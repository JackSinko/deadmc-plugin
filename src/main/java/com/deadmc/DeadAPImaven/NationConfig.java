package com.deadmc.DeadAPImaven;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class NationConfig extends YamlConfiguration {
	
	public static Map<String, NationConfig> configs = new HashMap<String, NationConfig>();
	
	public static NationConfig getConfig(String nationName) {
		synchronized(configs) { //'synchronized' allows only one thread to access configs at a time
			
			if(configs.containsKey(nationName))
				return configs.get(nationName);
			
			NationConfig config = new NationConfig(nationName);
			configs.put(nationName, config);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[NationConfig] " + ChatColor.RESET + "Added " + nationName + " - " + configs.size());
			
			return config;
			
		}
	}
	
	private File file = null;
	private Object saveLock = new Object();
	private String nationName;
	
	public NationConfig(String nationName) {
		super(); //'super' calls parent class constructor (YamlConfiguration)
		file = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "nations" + File.separator + nationName + ".yml");
		this.nationName = nationName;
		reload();
	}
	
	@SuppressWarnings("unused")
	private NationConfig() {
		nationName = null;
	}
	
	private void reload() {
		synchronized(saveLock) {
			try {
				load(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void save() {
		synchronized(saveLock) {
			try {
				save(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void discard() {
		synchronized(configs) {
			configs.remove(nationName);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[NationConfig] " + ChatColor.RESET + "Removed " + nationName + " - " + configs.size());
		}
	}
	
}