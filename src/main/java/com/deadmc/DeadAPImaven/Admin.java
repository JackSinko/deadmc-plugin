package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Chat.Leaderboard;
import com.deadmc.DeadAPImaven.Donator.Rank;
import com.deadmc.DeadAPImaven.Entities.LootRarity;
import com.deadmc.DeadAPImaven.Stats.Stat;
import com.deadmc.DeadAPImaven.Towns.Town;
import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.datatypes.skills.PrimarySkillType;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import github.scarsz.discordsrv.dependencies.jda.api.entities.MessageEmbed;
import github.scarsz.discordsrv.util.DiscordUtil;
import github.scarsz.discordsrv.util.WebhookUtil;
import io.papermc.lib.PaperLib;
import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Admin implements CommandExecutor {
	private static DeadMC plugin;
	
	public Admin(DeadMC p) {
		plugin = p;
	}
	
	public enum VoteReward {
		NOTHING,
		RARE,
		VERY_RARE
	}
	
	public enum Punishment {
		HACKING,
		BUG_ABUSE,
		SPAMMING,
		OFFENSIVE_LANGUAGE,
		PUBLIC_NUISANCE,
		FALSE_REPORTING,
		ALIAS_ABUSE,
		SHARING_LINKS
	}
	
	public enum StaffRank {
		JUNIOR_MOD,
		MODERATOR,
		SENIOR_MOD,
		ADMINISTRATOR,
		OWNER
	}
	
	public enum ActivityType {
		Report, //when a player makes a report
		Punish, //when a staff member makes a punishment
		Revert //when a staff member reverts a player punishment
	}
	
	public static HashMap<String, List<Integer>> lastThreeReports = new HashMap<String, List<Integer>>(); //player UUID, list of last reports
	public static List<String> spectating = new ArrayList<String>();
	public static List<String> vanished = new ArrayList<String>();
	public static List<String> nodamage = new ArrayList<String>();
	public static HashMap<String, Location> priorToSpectate = new HashMap<String, Location>(); //player to travel to, player attempting the travel\
	
	public static List<String> confirmResetChunks = new ArrayList<String>();
	
	public static String lbhead_type = null;
	public static int lbhead_place = 0;
	public static String lbhead_title = null;
	
	public static void exitSpectateMode(Player player) {
		player.sendMessage(ChatColor.YELLOW + "[Spectate] " + ChatColor.RED + "No longer spectating.");
		PaperLib.teleportAsync(player, priorToSpectate.get(player.getName()));
		if(!player.isOp()) {
			player.setFlying(false);
			player.setAllowFlight(false);
		}
		
		if(!vanished.contains(player.getName())) {
			for(Player online : Bukkit.getOnlinePlayers())
				online.showPlayer(plugin, player);
		}
		
		spectating.remove(player.getName());
		priorToSpectate.remove(player.getName());
	}
	
	public static void logBan(UUID uuid, Punishment reason, String dateOfSentence, int hoursServed) {
		if(hoursServed >= 1) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
			int banID = playerConfig.getString("BanHistory.Number of bans") == null ? 1 : playerConfig.getInt("BanHistory.Number of bans") + 1;
			
			playerConfig.set("BanHistory." + banID + ".Reason", reason.ordinal());
			playerConfig.set("BanHistory." + banID + ".HoursServed", hoursServed);
			playerConfig.set("BanHistory." + banID + ".Date", dateOfSentence);
			
			playerConfig.set("BanHistory.Number of bans", banID);
			playerConfig.save();
		}
	}
	
	public Boolean playerCanReport(UUID playerUUID, UUID playerToReportUUID, Punishment reason) {
		//for each int in list for player, timeSince > 1 hour, remove, get new list, loop all, if player reporting != same
		
		//Chat.broadcastMessage("Can player report?");
		
		//remove any old reports
		if(!lastThreeReports.containsKey(playerUUID.toString())) {
			//Chat.broadcastMessage("YES! player hasn't made any reports");
			return true;
		}
		List<Integer> lastReports = lastThreeReports.get(playerUUID.toString());
		//Chat.broadcastMessage(lastThreeReports.get(playerUUID.toString()).size() + " reports in the last hour");
		//Chat.broadcastMessage(lastThreeReports + "");
		for(int count = 0; count < lastReports.size(); count++) {
			int reportID = lastReports.get(count);
			
			ActivityType type = ActivityType.values()[DeadMC.ReportsFile.data().getInt("Activity." + reportID + ".Type")];
			int ID = DeadMC.ReportsFile.data().getInt("Activity." + reportID + ".ID");
			String timeOfReport = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Date/Time");
			
			//Chat.broadcastMessage("Time since report: " + DateCode.getTimeSince(timeOfReport));
			if(DateCode.getTimeSince(timeOfReport) > 60) {
				lastReports.remove(count);
			}
		}
		//update list
		lastThreeReports.remove(playerUUID.toString());
		if(lastReports.size() > 0) lastThreeReports.put(playerUUID.toString(), lastReports);
		else return true;
		
		if(lastReports.size() >= 3) return false;
		
		//check if player has reported this player in last hour
		for(int count = 0; count < lastReports.size(); count++) {
			int reportID = lastReports.get(count);
			ActivityType type = ActivityType.values()[DeadMC.ReportsFile.data().getInt("Activity." + reportID + ".Type")];
			int ID = DeadMC.ReportsFile.data().getInt("Activity." + reportID + ".ID");
			String playerReported = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed");
			Punishment reportReason = Punishment.values()[DeadMC.ReportsFile.data().getInt(type.toString() + "." + ID + ".Reason")];
			
			//Chat.broadcastMessage("");
			//Chat.broadcastMessage("Is this one about the player? " + playerReported.equalsIgnoreCase(playerToReportUUID.toString()));
			//Chat.broadcastMessage("Is the report reason the same? " + (reportReason == reason));
			if(playerReported.equalsIgnoreCase(playerToReportUUID.toString())
					&& reportReason == reason)
				return false;
		}
		return true;
	}
	
	//returns the activity ID
	public static int logActivity(ActivityType type, int id) {
		//log activity
		int activityID = DeadMC.ReportsFile.data().getInt("Activity.CurrentID") + 1;
		DeadMC.ReportsFile.data().set("Activity.CurrentID", activityID);
		DeadMC.ReportsFile.data().set("Activity." + activityID + ".Type", type.ordinal());
		DeadMC.ReportsFile.data().set("Activity." + activityID + ".ID", id);
		//keep only last 500 logs
		if(DeadMC.ReportsFile.data().getString("Activity." + (activityID - 500) + ".Type") != null) {
			DeadMC.ReportsFile.data().set("Activity." + (activityID - 500), null);
		}
		DeadMC.ReportsFile.save();
		return activityID;
	}
	
	boolean deleteDirectory(File directoryToBeDeleted) {
		File[] allContents = directoryToBeDeleted.listFiles();
		if (allContents != null) {
			for (File file : allContents) {
				deleteDirectory(file);
			}
		}
		return directoryToBeDeleted.delete();
	}
	
	@SuppressWarnings("unchecked")
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		Player player = null;
		if(sender instanceof Player) {
			player = (Player) sender;
		}
		
		// PLAYER EXPERIENCE SURVEY
		// send message every 14 days - unless set to ignore
		// LastSurveyVote - the date/time
		
		if(commandLabel.equalsIgnoreCase("gm")) {
			if(player.getWorld().getName().equalsIgnoreCase("revamp")) {
				if(player.getGameMode() == GameMode.CREATIVE)
					player.setGameMode(GameMode.SURVIVAL);
				else if(player.getGameMode() == GameMode.SURVIVAL)
					player.setGameMode(GameMode.CREATIVE);
			}
		}
		
		if(commandLabel.equalsIgnoreCase("yes") || commandLabel.equalsIgnoreCase("no")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(playerConfig.getString("LastSurveyVote") == null || DateCode.getDaysSince(playerConfig.getString("LastSurveyVote")) >= 14) {
				
				int percent = 100;
				if(commandLabel.equalsIgnoreCase("no")) percent = 0;
				
				Stats.Add(Stat.OSAT, percent);
				Stats.Add(Stat.RESPONSES);
				
				ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
				playerConfig.set("LastSurveyVote", DateCode.Encode(adelaideTime, true, false, false, true, false, false));
				if(commandLabel.equalsIgnoreCase("no")) playerConfig.set("SurveyVote", "no");
				if(commandLabel.equalsIgnoreCase("yes")) playerConfig.set("SurveyVote", "yes");
				playerConfig.save();
				
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.GREEN + "Thanks!");
				player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.WHITE + "Can you tell us why?");
				player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.GOLD + "/feedback <your comments here>");
				player.sendMessage("");
				
			} else {
				player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "You have already voted this fortnight. You can vote again in " + ChatColor.GOLD + (int) (14 - DateCode.getDaysSince(playerConfig.getString("LastSurveyVote"))) + ChatColor.WHITE + " days.");
			}
		}
		if(commandLabel.equalsIgnoreCase("feedback")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(playerConfig.getString("SurveyVote") != null) {
				if(args.length > 0) {
					
					int playtime = (int) Math.round(((player.getStatistic(Statistic.PLAY_ONE_MINUTE) / 20.0) / 60.0) / 60.0);
					String message = "(" + player.getName() + ", " + playtime + " hours) ";
					for(int count = 0; count < args.length; count++) {
						if(count == (args.length - 1)) message += args[count];
						else message += args[count] + " ";
					}
					
					String sentiment;
					if(playerConfig.getString("SurveyVote").equalsIgnoreCase("yes"))
						sentiment = "Positive";
					else sentiment = "Negative";
					
					List<String> weekComments = DeadMC.StatsFile.data().getStringList("Survey." + sentiment);
					weekComments.add(message);
					DeadMC.StatsFile.data().set("Survey." + sentiment, weekComments);
					DeadMC.StatsFile.save();
					
					player.sendMessage("");
					player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.GREEN + "Thanks for your feedback! :)");
					player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.WHITE + "We use this feedback to make the best possible player experience for all players.");
					player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.WHITE + "If you have any suggestions, feel free to let us know on our forums or our Discord:");
					player.sendMessage("");
					player.sendMessage(ChatColor.WHITE + "Forums: " + ChatColor.GOLD + "www.DeadMC.com/forum");
					player.sendMessage(ChatColor.WHITE + "Discord: " + ChatColor.GOLD + "discord.gg/CMpyUrs");
					player.sendMessage("");
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/feedback <your comments here>");
			} else
				player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.WHITE + "Do you " + ChatColor.ITALIC + "love" + ChatColor.WHITE + " playing DeadMC? " + ChatColor.GOLD + "/yes" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/no");
		}
		
		if(commandLabel.equalsIgnoreCase("comments")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(player.isOp() || playerConfig.getString("StaffRank") != null) {
				
				Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
					@Override
					public void run() {
						Player player = (Player) sender;
						
						List<String> positiveComments = DeadMC.StatsFile.data().getStringList("Survey.Positive");
						List<String> negativeComments = DeadMC.StatsFile.data().getStringList("Survey.Negative");
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "=======================================");
						player.sendMessage(ChatColor.YELLOW + "COMMENTS WEEK " + ChatColor.GOLD + DeadMC.StatsFile.data().getInt("CurrentWeek"));
						player.sendMessage("Total comments: " + ChatColor.BOLD + (int) (positiveComments.size() + negativeComments.size()));
						player.sendMessage("");
						
						if(positiveComments.size() > 0)
							player.sendMessage("Positive comments: " + ChatColor.GREEN + positiveComments.size());
						else player.sendMessage("Positive comments: 0");
						for(String comment : positiveComments) {
							player.sendMessage("  - " + comment);
						}
						
						player.sendMessage("");
						
						if(negativeComments.size() > 0)
							player.sendMessage("Negative comments: " + ChatColor.RED + negativeComments.size());
						else player.sendMessage("Negative comments: 0");
						for(String comment : negativeComments) {
							player.sendMessage("  - " + comment);
						}
						
						player.sendMessage(ChatColor.YELLOW + "=======================================");
						player.sendMessage("");
						
					}
				});
				
			}
		}
		
		if(commandLabel.equalsIgnoreCase("log")) {
			//run asynchronously
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					Player player = (Player) sender;
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					
					if(player.isOp() || playerConfig.getString("StaffRank") != null) {
						
						int logsPerPage = 3;
						// /log <page number>
						int pageNumber = 0;
						if(args.length > 0) {
							if(Chat.isInteger(args[0]))
								pageNumber = Integer.parseInt(args[0]);
							else {
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/log <page number>");
								return;
							}
						}
						
						int currentID = DeadMC.ReportsFile.data().getInt("Activity.CurrentID");
						int maxPages = (int) Math.ceil((double) (currentID / logsPerPage));
						
						if(pageNumber > maxPages) {
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Page " + pageNumber + " doesn't exist. (max page is " + maxPages + ")");
							return;
						}
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "================ DeadMC Staff Log ================");
						player.sendMessage("");
						
						int startNumber = currentID - (pageNumber * logsPerPage);
						for(int count = startNumber; count > (startNumber - logsPerPage); count--) {
							if(count < 1) break;
							
							if(DeadMC.ReportsFile.data().getString("Activity." + count + ".Type") == null) {
								player.sendMessage("  - This report has expired.");
								player.sendMessage("");
								continue;
							}
							ActivityType type = ActivityType.values()[DeadMC.ReportsFile.data().getInt("Activity." + count + ".Type")];
							int ID = DeadMC.ReportsFile.data().getInt("Activity." + count + ".ID");
							
							String timeOfReport = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Date/Time");
							String playerWhoReported = "CONSOLE";
							if(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed by") == null) {
								player.sendMessage("  - This report has expired.");
								player.sendMessage("");
								continue;
							} else if(!DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed by").equals("CONSOLE") && !DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed by").equals("EXPIRED"))
								playerWhoReported = Bukkit.getOfflinePlayer(UUID.fromString(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed by"))).getName();
							String playerReported = Bukkit.getOfflinePlayer(UUID.fromString(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed"))).getName();
							Punishment reason = Punishment.values()[DeadMC.ReportsFile.data().getInt(type.toString() + "." + ID + ".Reason")];
							String comments = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Comments");
							
							String typeColour = "" + ChatColor.RED;
							if(type == ActivityType.Revert)
								typeColour = "" + ChatColor.LIGHT_PURPLE;
							if(type == ActivityType.Report)
								typeColour = "" + ChatColor.AQUA;
							
							int timeSinceReported = DateCode.getTimeSince(timeOfReport);
							String timeSinceReportedString = timeSinceReported + "" + ChatColor.WHITE + " minutes";
							if(timeSinceReported > 60)
								timeSinceReportedString = Math.round(timeSinceReported / 60.0) + "" + ChatColor.WHITE + " hours";
							if(timeSinceReported > 2880)
								timeSinceReportedString = Math.round(timeSinceReported / 1440.0) + "" + ChatColor.WHITE + " days";
							
							player.sendMessage(DateCode.Decode(timeOfReport, DateCode.Value.DAY_OF_MONTH) + "/" + DateCode.Decode(timeOfReport, DateCode.Value.MONTH) + "/" + DateCode.Decode(timeOfReport, DateCode.Value.YEAR) + " (" + ChatColor.BOLD + timeSinceReportedString + " ago)");
							player.sendMessage("  - " + playerWhoReported + " " + typeColour + type.toString() + "ed " + ChatColor.GREEN + playerReported + ChatColor.WHITE + " for " + ChatColor.BOLD + reason.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + ".");
							
							int timePassed = DeadMC.ReportsFile.data().getInt(type.toString() + "." + ID + ".Time passed");
							String timePassedString = timePassed + "" + ChatColor.WHITE + " minutes";
							if(timePassed > 60)
								timePassedString = Math.round(timePassed / 60.0) + "" + ChatColor.WHITE + " hours";
							if(timePassed > 2880)
								timePassedString = Math.round(timePassed / 1440.0) + "" + ChatColor.WHITE + " days";
							
							if(type == ActivityType.Revert)
								player.sendMessage("    - Time passed: " + timePassedString);
							if(!comments.equalsIgnoreCase(""))
								player.sendMessage("    - Comments: " + ChatColor.ITALIC + comments);
							if(type == ActivityType.Punish)
								player.sendMessage("       Undo with: " + ChatColor.GOLD + "/unpunish " + ChatColor.BOLD + count);
							
							player.sendMessage("");
						}
						player.sendMessage(ChatColor.WHITE + "Displaying page " + ChatColor.BOLD + pageNumber + ChatColor.WHITE + " of " + maxPages + ".");
						player.sendMessage(ChatColor.YELLOW + "=================================================");
						player.sendMessage("");
						
					}
				}
			});
			
		}
		
		if(commandLabel.equalsIgnoreCase("unpunish")) {
			//run asynchronously
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					Player player = (Player) sender;
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					
					if(player.isOp() || playerConfig.getString("StaffRank") != null) {
						if(args.length < 2) {
							player.sendMessage("");
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Undo a punishment: " + ChatColor.GOLD + "/unpunish <reportID> <reason>");
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/log" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/lookup" + ChatColor.WHITE + " to find the activity ID.");
							player.sendMessage("");
						} else {
							
							if(Chat.isInteger(args[0])) {
								int activityID = Integer.parseInt(args[0]);
								
								if(DeadMC.ReportsFile.data().getString("Activity." + activityID + ".Type") != null) {
									ActivityType type = ActivityType.values()[DeadMC.ReportsFile.data().getInt("Activity." + activityID + ".Type")];
									int ID = DeadMC.ReportsFile.data().getInt("Activity." + activityID + ".ID");
									
									String timeOfReport = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Date/Time");
									//playerWhoReported will be null if console
									UUID playerWhoReported = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed by").equalsIgnoreCase("CONSOLE") ? null : UUID.fromString(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed by"));
									UUID playerReported = UUID.fromString(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed"));
									Punishment reason = Punishment.values()[DeadMC.ReportsFile.data().getInt(type.toString() + "." + ID + ".Reason")];
									String comments = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Comments");
									
									if(type == ActivityType.Punish) {
										
										if(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Reverted") == null) {
											
											if(!(sender instanceof Player)
													|| playerWhoReported == null
													|| playerWhoReported.toString().equalsIgnoreCase(player.getUniqueId().toString())
													|| playerConfig.getInt("StaffRank") >= StaffRank.SENIOR_MOD.ordinal()
													) { //was the console
												
												int count = 1;
												String revertComments = "";
												while(count < args.length) {
													revertComments = revertComments + args[count] + " ";
													count++;
												}
												
												OfflinePlayer reportedPlayer = Bukkit.getOfflinePlayer(playerReported);
												PlayerConfig banneeConfig = PlayerConfig.getConfig(reportedPlayer);
												
												for(Player staff : Bukkit.getOnlinePlayers()) {
													if(PlayerConfig.getConfig(staff.getUniqueId()).getString("StaffRank") != null) {
														staff.sendMessage("");
														staff.sendMessage(ChatColor.YELLOW + "================== Player Unpunished ==================");
														staff.sendMessage(ChatColor.GREEN + player.getName() + " unpunished " + reportedPlayer.getName() + " for " + reason.toString().toLowerCase().replace("_", " ") + ".");
														staff.sendMessage(ChatColor.WHITE + "Comments: " + revertComments);
														staff.sendMessage(ChatColor.YELLOW + "====================================================");
														staff.sendMessage("");
													}
												}
												
												int timePassedInMinutes = DateCode.getTimeSince(timeOfReport);
												String plural = "";
												if(timePassedInMinutes < 60) //less than an hour
													plural = timePassedInMinutes == 1 ? "1 minute" : timePassedInMinutes + " minutes";
												else if(timePassedInMinutes < 1440) //less than a day
													plural = timePassedInMinutes == 60 ? "1 hour" : (timePassedInMinutes / 60) + " hours";
												else if(timePassedInMinutes < 10080) //less than a week
													plural = timePassedInMinutes == 1440 ? "1 day" : (timePassedInMinutes / 1440) + " days";
												else if(timePassedInMinutes < 302400) //less than a month
													plural = timePassedInMinutes == 10080 ? "1 week" : (timePassedInMinutes / 10080) + " weeks";
												else
													plural = timePassedInMinutes == 302400 ? "1 month" : (timePassedInMinutes / 302400) + " months";
												
												player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You have undone " + ChatColor.AQUA + reportedPlayer.getName() + ChatColor.WHITE + "'s punishment for " + ChatColor.GOLD + reason.toString().toLowerCase().replace(" ", " ") + ChatColor.WHITE + " (" + plural + " passed).");
												
												//undo the chat offense
												if(banneeConfig.getString("Muted") != null) {
													int numberOfOffences = banneeConfig.getInt("MuteOffences");
													banneeConfig.set("MuteOffences", new Integer(numberOfOffences - 1));
													player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Removed 1 chat offence from player.");
												}
												
												//unmute
												banneeConfig.set("Muted", null);

												//unban
												if(banneeConfig.getString("Banned.Reason") != null) {
													int timeSinceBan = DateCode.getTimeSince(banneeConfig.getString("Banned.Start"));
													
													Admin.logBan(playerReported, Punishment.valueOf(banneeConfig.getString("Banned.Reason").replace(" ", "_")), banneeConfig.getString("Banned.Start"), timeSinceBan / 60);
													banneeConfig.set("Banned", null);
												}
												
												banneeConfig.save();
												
												int revertID = DeadMC.ReportsFile.data().getInt("Revert.CurrentID") + 1;
												DeadMC.ReportsFile.data().set("Revert.CurrentID", revertID);
												
												logActivity(ActivityType.Revert, revertID);
												
												DeadMC.ReportsFile.data().set("Revert." + revertID + ".Reverted by", player.getUniqueId().toString());
												DeadMC.ReportsFile.data().set("Revert." + revertID + ".Reverted", reportedPlayer.getUniqueId().toString());
												
												ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
												DeadMC.ReportsFile.data().set("Revert." + revertID + ".Date/Time", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
												DeadMC.ReportsFile.data().set("Revert." + revertID + ".Time passed", DateCode.getTimeSince(timeOfReport));
												DeadMC.ReportsFile.data().set("Revert." + revertID + ".Reason", reason.ordinal());
												DeadMC.ReportsFile.data().set("Revert." + revertID + ".Comments", revertComments);
												
												//keep only last 500 punishments
												if(DeadMC.ReportsFile.data().getString("Revert." + (revertID - 500) + ".Reverted") != null) {
													DeadMC.ReportsFile.data().set("Revert." + (revertID - 500), null);
												}
												DeadMC.ReportsFile.save();
												
												DeadMC.ReportsFile.data().set(type.toString() + "." + ID + ".Reverted", true);
												
												String nickName = playerConfig.getString("Name") != null ? playerConfig.getString("Name") : player.getName();
												WebhookUtil.deliverMessage(DiscordUtil.getJda().getTextChannelById(JDAListener.staffLogChannel), player, nickName, ":unlock: Reverted **" + reportedPlayer.getName() + "** for **" + reason.toString().toLowerCase().replace("_", " ") + "**.\nTime passed: " + plural + "\nComments: *" + revertComments + "*", (MessageEmbed) null);
												
												if(reportedPlayer.isOnline()) {
													reportedPlayer.getPlayer().sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You have been unpunished for " + reason.toString().toLowerCase().replace("_", " ") + ".");
												}
												
											} else
												player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "You can only undo punishments that you created.");
											
										} else
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "This has already been reverted.");
										
									} else
										player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[0] + " is not a punishment (it is a " + type.toString() + ").");
								} else
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[0] + " is not a valid Activity ID.");
							} else
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[0] + " is not a valid Activity ID.");
							
						}
					}
				}
			});
			
		}
		
		if(commandLabel.equalsIgnoreCase("punish") || commandLabel.equalsIgnoreCase("pun")) {
			//run asynchronously as we search all online players for nick names
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					if(sender instanceof Player) {
						Player player = (Player) sender;
						PlayerConfig playerConfig = PlayerConfig.getConfig(player);
						
						if(player.isOp() || playerConfig.getString("StaffRank") != null) {
							
							if(args.length <= 1) {
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "===================== Punishment =====================");
								player.sendMessage(ChatColor.WHITE + "Punish a player for breaking the rules.");
								player.sendMessage(ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/pun <player> <rule> (<time>) <evidence/notes>");
								player.sendMessage(ChatColor.WHITE + "Punishments:");
								player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Hacking" + ChatColor.WHITE + " (any client modifications that affect game play)");
								player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Bug abuse" + ChatColor.WHITE + " (not reporting a bug, and abusing it cosmetically or for gain)");
								player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Spamming" + ChatColor.WHITE + " (spamming chat and/or commands)");
								player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Language" + ChatColor.WHITE + " (offensive and or threatening language)");
								player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Advertising" + ChatColor.WHITE + " (sharing links or IP's to other servers)");
								player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Public nuisance" + ChatColor.WHITE + " (intending to cause harm to the server, or serious offense to players)");
								player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Alias abuse" + ChatColor.WHITE + " (using multiple accounts in a way that gives the player extra benefits)");
								player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " False reporting" + ChatColor.WHITE + " (reporting a player with inaccurate, misguiding, or biased information)");
								player.sendMessage(ChatColor.YELLOW + "====================================================");
								player.sendMessage("");
								return;
							}
							
							//time: 
							// 1hour
							// 1day
							// 2days
							// 1week
							// 2weeks
							// 1month
							// mustappeal
							
							String playerName = args[0];
							for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
								PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
								if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[0])) {
									//used nick name
									playerName = onlinePlayer.getName();
									break;
								}
							}
							UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
							
							if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
								if((!(sender instanceof Player)) || !uuid.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
									
									if(getPunishmentFromString(args[1]) != null) {
										Punishment punishment = getPunishmentFromString(args[1]);
										
										if(args.length == 2
												&& (punishment == Punishment.OFFENSIVE_LANGUAGE || punishment == Punishment.SPAMMING || punishment == Punishment.SHARING_LINKS)) {
											player.sendMessage("");
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "You are missing evidence/notes.");
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/pun <player> <rule> <evidence/notes>");
											player.sendMessage("");
											return;
										} else if(args.length == 3
												&& (punishment == Punishment.HACKING || punishment == Punishment.FALSE_REPORTING || punishment == Punishment.BUG_ABUSE || punishment == Punishment.PUBLIC_NUISANCE || punishment == Punishment.ALIAS_ABUSE)) {
											player.sendMessage("");
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "You are missing evidence/notes.");
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/pun <player> <rule> <time> <evidence/notes>");
											player.sendMessage("");
											return;
										}
										
										int punishmentTime = 0;
										
										//if other player is not a staff member - or this player is OP (to punish a staff member)
										PlayerConfig otherPlayerConfig = PlayerConfig.getConfig(uuid);
										if(otherPlayerConfig.getString("StaffRank") == null
												|| player.isOp()) {
											
											int count = punishment == Punishment.OFFENSIVE_LANGUAGE || punishment == Punishment.SPAMMING || punishment == Punishment.SHARING_LINKS ? 2 : 3; //start args count at 2
											String comments = "";
											while(count < args.length) {
												comments = count == (args.length - 1) ? comments + args[count] : comments + args[count] + " ";
												count++;
											}
											
											int punishID = DeadMC.ReportsFile.data().getInt("Punish.CurrentID") + 1;
											DeadMC.ReportsFile.data().set("Punish.CurrentID", punishID);
											
											int activityID = logActivity(ActivityType.Punish, punishID);
											
											if(punishment == Punishment.HACKING || punishment == Punishment.FALSE_REPORTING || punishment == Punishment.BUG_ABUSE || punishment == Punishment.PUBLIC_NUISANCE || punishment == Punishment.ALIAS_ABUSE) {

												int time = 0; //0 is permanent (default)
												
												String numbers = "";
												int i = 0;
												while(Chat.isInteger(String.valueOf(args[2].charAt(i)))) {
													numbers += args[2].charAt(i);
													//Chat.broadcastMessage("Numbers: " + numbers);
													i++;
												}
												
												if(((args[2].toLowerCase().contains("hour")
														|| args[2].toLowerCase().contains("day")
														|| args[2].toLowerCase().contains("week")
														|| args[2].toLowerCase().contains("month"))
														
														&& numbers.length() > 0)
														
														|| args[2].toLowerCase().contains("permanent")) {
													
													String bannedMessage = ChatColor.WHITE + "You have been " + ChatColor.RED + ChatColor.BOLD + "banned" + ChatColor.WHITE + " for " + ChatColor.BOLD + punishment.toString().replace("_", " ") + ChatColor.WHITE + "\nVisit " + ChatColor.GOLD + "www.DeadMC.com/forums " + ChatColor.WHITE + "to lodge an appeal. \n\n\"" + ChatColor.ITALIC + comments + ChatColor.WHITE + "\"\nPlease follow the rules.";
													if(args[2].toLowerCase().contains("hour"))
														time = Integer.parseInt(numbers);
													else if(args[2].toLowerCase().contains("day"))
														time = Integer.parseInt(numbers) * 24;
													else if(args[2].toLowerCase().contains("week"))
														time = Integer.parseInt(numbers) * 168;
													else if(args[2].toLowerCase().contains("month"))
														time = Integer.parseInt(numbers) * 730;
													
													
													if(time != 0) { //not permanent
														String plural = "";
														
														if(time < 24) //less than a day
															plural = time == 1 ? "1 hour" : time + " hours";
														else if(time < 168) //less than a week
															plural = time == 24 ? "1 day" : (time / 24) + " days";
														else if(time < 730) //less than a month
															plural = time == 168 ? "1 week" : (time / 168) + " weeks";
														else
															plural = time == 730 ? "1 month" : (time / 730) + " months";
														
														bannedMessage = "You have been banned " + ChatColor.RED + ChatColor.BOLD + plural + ChatColor.WHITE + " for " + ChatColor.BOLD + punishment.toString().replace("_", " ") + ChatColor.WHITE + ".\n\n\"" + ChatColor.ITALIC + comments + ChatColor.WHITE + "\"\nPlease follow the rules.";
													}
													final String finalMessage = bannedMessage;
													
													String banee = "AUTOMATIC";
													if(sender instanceof Player)
														banee = player.getName();
													
													ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
													otherPlayerConfig.set("Banned.Start", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
													if(time > 0)
														otherPlayerConfig.set("Banned.Time", time); //a null banned time means permanent
													otherPlayerConfig.set("Banned.Reason", punishment.toString().replace("_", " "));
													otherPlayerConfig.set("Banned.Comments", comments);
													otherPlayerConfig.set("Banned.Punished by", player.getUniqueId().toString());
													otherPlayerConfig.set("Banned.ActivityID", activityID);
													otherPlayerConfig.save();
													
													//check if any of the aliases are logged in:
													List<String> playersToCheck = new ArrayList<String>();
													if(otherPlayerConfig.getString("Aliases") != null)
														playersToCheck.addAll(otherPlayerConfig.getStringList("Aliases")); //add all aliases
													playersToCheck.add(uuid.toString()); //add the actual player
													
													for(String aliasUUID : playersToCheck) { //all aliases
														Bukkit.getScheduler().runTaskLater(plugin, () -> {
															if(Bukkit.getPlayer(UUID.fromString(aliasUUID)) != null) {
																Bukkit.getPlayer(UUID.fromString(aliasUUID)).kickPlayer(finalMessage);
															}
														}, 10L);
													}
													
													punishmentTime = time*60;
													
												} else {
													player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[2] + ChatColor.WHITE + " is not a valid time. Valid: (Xhour(s), Xday(s), Xweek(s), Xmonth(s), permanent)");
													return;
												}
												
											}
											
											int numberOfOffences = 0;
											if(punishment == Punishment.OFFENSIVE_LANGUAGE || punishment == Punishment.SPAMMING || punishment == Punishment.SHARING_LINKS) {
												//dmc <tempban/mute> <player> <time in minutes> <reason ID>
												if(otherPlayerConfig.getString("MuteOffences") != null) {
													numberOfOffences = otherPlayerConfig.getInt("MuteOffences");
												}
												numberOfOffences++;
												int muteTime = numberOfOffences * (60 * (numberOfOffences - 1)); //second offense is 2 hours, third is 6 hours, forth is 12 hours
												if(numberOfOffences == 1) muteTime = 30; //first offense is 30 minutes
												
												otherPlayerConfig.set("MuteOffences", numberOfOffences);
												
												ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
												otherPlayerConfig.set("Muted.Start", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
												otherPlayerConfig.set("Muted.Time", muteTime); //time in minutes
												otherPlayerConfig.set("Muted.Reason", punishment.ordinal());
												otherPlayerConfig.set("Muted.PunishID", punishID);
												
												otherPlayerConfig.save();
												
												punishmentTime = muteTime;
											}
											
											DeadMC.ReportsFile.data().set("Punish." + punishID + ".Punished by", player.getUniqueId().toString());
											DeadMC.ReportsFile.data().set("Punish." + punishID + ".Punished", uuid.toString());
											
											ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
											DeadMC.ReportsFile.data().set("Punish." + punishID + ".Date/Time", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
											DeadMC.ReportsFile.data().set("Punish." + punishID + ".Reason", punishment.ordinal());
											DeadMC.ReportsFile.data().set("Punish." + punishID + ".Comments", comments);
											//keep only last 500 punishments
											if(DeadMC.ReportsFile.data().getString("Punish." + (punishID - 500) + ".Punished") != null)
												DeadMC.ReportsFile.data().set("Punish." + (punishID - 500), null);
											DeadMC.ReportsFile.save();
											
											player.sendMessage("");
											player.sendMessage(ChatColor.YELLOW + "================== Punishment ==================");
											player.sendMessage(ChatColor.GREEN + "Successfully punished " + args[0] + " for " + punishment.toString().toLowerCase().replace("_", " ") + ".");
											
											if(numberOfOffences > 1)
												player.sendMessage(ChatColor.DARK_RED + "This is a repeat offence (x" + numberOfOffences + ").");
											if(args.length > 2)
												player.sendMessage(ChatColor.WHITE + "Comments: " + comments);
											
											player.sendMessage(ChatColor.WHITE + "A log of the activity has been created (" + ChatColor.GOLD + "/log" + ChatColor.WHITE + ") (ID " + activityID + ")");
											player.sendMessage(ChatColor.WHITE + "Undo this action with " + ChatColor.GOLD + "/unpunish " + ChatColor.BOLD + activityID);
											player.sendMessage(ChatColor.YELLOW + "==================================================");
											player.sendMessage("");
											
											String plural = "";
											if(punishmentTime < 60) //less than an hour
												plural = punishmentTime == 1 ? "1 minute" : punishmentTime + " minutes";
											else if(punishmentTime < 1440) //less than a day
												plural = punishmentTime == 60 ? "1 hour" : (punishmentTime / 60) + " hours";
											else if(punishmentTime < 10080) //less than a week
												plural = punishmentTime == 1440 ? "1 day" : (punishmentTime / 1440) + " days";
											else if(punishmentTime < 302400) //less than a month
												plural = punishmentTime == 10080 ? "1 week" : (punishmentTime / 10080) + " weeks";
											else
												plural = punishmentTime == 302400 ? "1 month" : (punishmentTime / 302400) + " months";
											
											Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + args[0] + " has been punished for " + punishment.toString().toLowerCase().replace("_", " ") + ".");
											String emoji = punishment == Admin.Punishment.OFFENSIVE_LANGUAGE || punishment == Admin.Punishment.SPAMMING || punishment == Admin.Punishment.SHARING_LINKS ? ":mute:" : ":judge:";
											String timeLine = punishmentTime > 0 ? "\nTime: " + plural : "";
											String nickName = playerConfig.getString("Name") != null ? playerConfig.getString("Name") : player.getName();
											WebhookUtil.deliverMessage(DiscordUtil.getJda().getTextChannelById(JDAListener.staffLogChannel), player, nickName, emoji + " Punished **" + playerName + "** for **" + punishment.toString().toLowerCase().replace("_", " ") + "**." + timeLine + "\nComments: *" + comments + "*\n> Undo with /unpunish **" + activityID + "**", (MessageEmbed) null);
										} else
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "Cannot punish another staff member.");
									} else
										player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " must be a valid RULE.");
								} else
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "You cannot punish yourself.");
							} else
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[0] + " is not an existing player.");
							
						}
					} else {
						//admin punishment:
						
						String playerName = args[0];
						for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
							PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
							if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[0])) {
								//used nick name
								playerName = onlinePlayer.getName();
								break;
							}
						}
						UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
						PlayerConfig otherPlayerConfig = PlayerConfig.getConfig(uuid);
						
						if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
							if(getPunishmentFromString(args[1]) != null) {
								Punishment punishment = getPunishmentFromString(args[1]);
								
								int punishID = DeadMC.ReportsFile.data().getInt("Punish.CurrentID") + 1;
								DeadMC.ReportsFile.data().set("Punish.CurrentID", punishID);
								
								int activityID = logActivity(ActivityType.Punish, punishID);
								
								if(!Chat.playerIsMuted(uuid)
										&& otherPlayerConfig.getString("Banned.Start") == null) {
									Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + args[0] + " has been punished for " + punishment.toString().toLowerCase().replace("_", " ") + ".");
								}
								
								int numberOfOffences = 0;
								int punishmentTime = 0;
								
								String emoji = "";
								String comments = "";
								String commentsDiscordFormat = "";
								
								if(punishment == Punishment.OFFENSIVE_LANGUAGE || punishment == Punishment.SPAMMING || punishment == Punishment.SHARING_LINKS) {
									emoji = ":mute:";
									//dmc <tempban/mute> <player> <time in minutes> <reason ID>
									if(otherPlayerConfig.getString("MuteOffences") != null) {
										numberOfOffences = otherPlayerConfig.getInt("MuteOffences");
									}
									numberOfOffences++;
									int muteTime = numberOfOffences * (120 * numberOfOffences);
									
									otherPlayerConfig.set("MuteOffences", numberOfOffences);
									
									ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
									otherPlayerConfig.set("Muted.Start", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
									otherPlayerConfig.set("Muted.Time", muteTime); //time in minutes
									otherPlayerConfig.set("Muted.Reason", punishment.ordinal());
									otherPlayerConfig.set("Muted.PunishID", punishID);
									
									otherPlayerConfig.save();
									punishmentTime = muteTime;
									
									String apostrophe = Chat.lastMessage.get(uuid.toString()).equalsIgnoreCase("sent message too fast") ? "" : "'";
									comments = ChatColor.ITALIC + "AUTOMATIC: " + apostrophe + Chat.lastMessage.get(uuid.toString()) + apostrophe;
									commentsDiscordFormat = "*AUTOMATIC: " + apostrophe + Chat.lastMessage.get(uuid.toString()) + apostrophe + "*";
								}
								if(punishment == Punishment.HACKING) {
									emoji = ":judge:";
									
									punishmentTime = 4320; //3 days
									
									comments = ChatColor.ITALIC + "AUTOMATIC: using " + args[3] + " hacks";
									commentsDiscordFormat = "*AUTOMATIC: using " + args[3] + " hacks*";

									String bannedMessage = "You have been banned " + ChatColor.RED + ChatColor.BOLD + "3 days" + ChatColor.WHITE + " for " + ChatColor.BOLD + punishment.toString().replace("_", " ") + ChatColor.WHITE + ".\n\n\"" + ChatColor.ITALIC + comments.replace("AUTOMATIC: ", "") + ChatColor.WHITE + "\"\nPlease follow the rules.";
									
									ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
									otherPlayerConfig.set("Banned.Start", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
									otherPlayerConfig.set("Banned.Time", 72); //3 days
									otherPlayerConfig.set("Banned.Reason", punishment.toString().replace("_", " "));
									otherPlayerConfig.set("Banned.Comments", comments);
									otherPlayerConfig.set("Banned.Punished by", "CONSOLE");
									otherPlayerConfig.set("Banned.ActivityID", activityID);
									otherPlayerConfig.save();
									
									//check if any of the aliases are logged in:
									List<String> playersToCheck = new ArrayList<String>();
									if(otherPlayerConfig.getString("Aliases") != null)
										playersToCheck.addAll(otherPlayerConfig.getStringList("Aliases")); //add all aliases
									playersToCheck.add(uuid.toString()); //add the actual player
									
									for(String aliasUUID : playersToCheck) { //all aliases
										Bukkit.getScheduler().runTaskLater(plugin, () -> {
											if(Bukkit.getPlayer(UUID.fromString(aliasUUID)) != null) {
												Bukkit.getPlayer(UUID.fromString(aliasUUID)).kickPlayer(bannedMessage);
											}
										}, 10L);
									}
								}
								
								DeadMC.ReportsFile.data().set("Punish." + punishID + ".Punished by", "CONSOLE");
								DeadMC.ReportsFile.data().set("Punish." + punishID + ".Punished", uuid.toString());
								
								ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
								DeadMC.ReportsFile.data().set("Punish." + punishID + ".Date/Time", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
								DeadMC.ReportsFile.data().set("Punish." + punishID + ".Reason", punishment.ordinal());
								DeadMC.ReportsFile.data().set("Punish." + punishID + ".Comments", comments);
								//keep only last 500 punishments
								if(DeadMC.ReportsFile.data().getString("Punish." + (punishID - 500) + ".Punished") != null)
									DeadMC.ReportsFile.data().set("Punish." + (punishID - 500), null);
								DeadMC.ReportsFile.save();

								String plural = "";
								if(punishmentTime < 60) //less than an hour
									plural = punishmentTime == 1 ? "1 minute" : punishmentTime + " minutes";
								else if(punishmentTime < 1440) //less than a day
									plural = punishmentTime == 60 ? "1 hour" : (punishmentTime / 60) + " hours";
								else if(punishmentTime < 10080) //less than a week
									plural = punishmentTime == 1440 ? "1 day" : (punishmentTime / 1440) + " days";
								else if(punishmentTime < 302400) //less than a month
									plural = punishmentTime == 10080 ? "1 week" : (punishmentTime / 10080) + " weeks";
								else
									plural = punishmentTime == 302400 ? "1 month" : (punishmentTime / 302400) + " months";
								
								String timeLine = punishmentTime > 0 ? "\nTime: " + plural : "";
								DiscordUtil.sendMessage(DiscordUtil.getJda().getTextChannelById(JDAListener.staffLogChannel), emoji + " Punished **" + playerName + "** for **" + punishment.toString().toLowerCase().replace("_", " ") + "**." + timeLine + "\nComments: " + commentsDiscordFormat + "\n> Undo with /unpunish **" + activityID + "**");
							}
						}
					}
				}
			});
		}
		
		if(commandLabel.equalsIgnoreCase("setname")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(player.isOp() || (playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.MODERATOR.ordinal())) {
				Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
					@Override
					public void run() {
						Player player = (Player) sender;
						
						if(args.length >= 2) {
							String playerName = args[0];
							for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
								PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
								if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[0])) {
									//used nick name
									playerName = onlinePlayer.getName();
									break;
								}
							}
							UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
							
							//setname <name> <new name>
							
							if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
								PlayerConfig otherConfig = PlayerConfig.getConfig(uuid);
								
								otherConfig.set("Name", args[1]);
								otherConfig.save();
								
								player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Changed name of " + Bukkit.getOfflinePlayer(playerName).getName() + " to " + args[1] + ".");
								
								//update placeholder:
								PlaceHolders.playersName.replace(uuid.toString(), args[1]);
								
							} else
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[0] + " is not an existing player.");
							
						} else
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "Use: /dmc playername <player name> <new name>");
					}
				});
			}
		}
		
		if(commandLabel.equalsIgnoreCase("vanish")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(player.isOp() || (playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.JUNIOR_MOD.ordinal())) {
				
				if(!vanished.contains(player.getName())) {
					player.sendMessage(ChatColor.YELLOW + "[/vanish] " + ChatColor.RED + "You are now invisible.");
					
					vanished.add(player.getName());
					for(Player online : Bukkit.getOnlinePlayers()) {
						online.hidePlayer(plugin, player);
					}
					
					Player finalPlayer = player;
					Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
						while(vanished.contains(finalPlayer.getName()) && finalPlayer != null) {
							Chat.sendTitleMessage(finalPlayer.getUniqueId(), ChatColor.YELLOW + "[/vanish] " + ChatColor.RED + "You are currently hidden.");
							Tools.sleepThread(2000);
						}
					});
				} else {
					player.sendMessage(ChatColor.YELLOW + "[/vanish] " + ChatColor.GREEN + "No longer invisible.");
					
					vanished.remove(player.getName());
					if(!spectating.contains(player.getName())) {
						for(Player online : Bukkit.getOnlinePlayers()) {
							online.showPlayer(plugin, player);
						}
					}
				}
				
			}
		}
		
		if(commandLabel.equalsIgnoreCase("spectate") || commandLabel.equalsIgnoreCase("spec")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(player.isOp() || playerConfig.getString("StaffRank") != null) {
				if(args.length > 0) {
					Player playerToSpectate = Bukkit.getPlayer(args[0]);
					if(playerToSpectate != null) {
						if(PlayerConfig.getConfig(playerToSpectate.getUniqueId()).getString("StaffRank") == null
								|| PlayerConfig.getConfig(playerToSpectate.getUniqueId()).getInt("StaffRank") < playerConfig.getInt("StaffRank")
								|| player.isOp()) {
							if(playerToSpectate.getWorld().getName().toLowerCase().contains("revamp")) {
								player.sendMessage(ChatColor.YELLOW + "[/spectate] " + ChatColor.RED + "Cannot spectate players that are in the revamp world.");
								return true;
							}
							
							if(!spectating.contains(player.getName())) {
								player.sendMessage(ChatColor.YELLOW + "[/spectate] " + ChatColor.GREEN + "Spectating " + Bukkit.getPlayer(args[0]).getName() + ". You are invisible.");
								priorToSpectate.put(player.getName(), player.getLocation());
								PaperLib.teleportAsync(player, Bukkit.getPlayer(args[0]).getLocation());
								
								player.setAllowFlight(true);
								player.setFlying(true);
								
								for(Player online : Bukkit.getOnlinePlayers()) {
									online.hidePlayer(plugin, player);
								}
								spectating.add(player.getName());
								
								Player finalPlayer = player;
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									while(spectating.contains(finalPlayer.getName()) && finalPlayer != null) {
										Chat.sendTitleMessage(finalPlayer.getUniqueId(), ChatColor.YELLOW + "[/spectate] " + ChatColor.RED + "You are currently hidden.");
										Tools.sleepThread(2000);
									}
								});
								
							} else exitSpectateMode(player);
						} else
							player.sendMessage(ChatColor.YELLOW + "[/spectate] " + ChatColor.RED + "You can't spectate staff of same rank or higher.");
					} else
						player.sendMessage(ChatColor.YELLOW + "[/spectate] " + ChatColor.RED + args[0] + " is not online.");
				} else if(spectating.contains(player.getName())) {
					exitSpectateMode(player);
				} else
					player.sendMessage(ChatColor.YELLOW + "[/spectate] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/spectate <player>");
			}
		}
		
		if(commandLabel.equalsIgnoreCase("report") || commandLabel.equalsIgnoreCase("r")) {
			//run asynchronously as we search all online players for nick names
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					Player player = (Player) sender;
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					
					if(args.length < 2) {
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "================== Report a Player ==================");
						player.sendMessage(ChatColor.WHITE + "Report a player for breaking the " + ChatColor.GOLD + "/rules" + ChatColor.WHITE + ".");
						player.sendMessage(ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/r <player> <rule> <evidence/notes>");
						player.sendMessage(ChatColor.WHITE + "Rules:");
						player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Hacking" + ChatColor.WHITE + " (any client modifications that affect game play)");
						player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Bug abuse" + ChatColor.WHITE + " (not reporting a bug, and abusing it cosmetically or for gain)");
						player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Spamming" + ChatColor.WHITE + " (spamming chat and/or commands)");
						player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Language" + ChatColor.WHITE + " (offensive and or threatening language)");
						player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Advertising" + ChatColor.WHITE + " (sharing links or IP's to other servers)");
						player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Public nuisance" + ChatColor.WHITE + " (intending to cause harm to the server, or serious offense to players)");
						player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " Alias abuse" + ChatColor.WHITE + " (using multiple accounts in a way that gives the player extra benefits)");
						player.sendMessage(ChatColor.WHITE + "  " + ChatColor.GOLD + " False reporting" + ChatColor.WHITE + " (reporting a player with inaccurate, misguiding, or biased information)");
						player.sendMessage(ChatColor.YELLOW + "===================================================");
						player.sendMessage("");
					} else {
						
						if(args.length > 2) { //report player ruleID <evidence>
							
							String playerName = args[0];
							for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
								PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
								if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[0])) {
									//used nick name
									playerName = onlinePlayer.getName();
									break;
								}
							}
							UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
							
							if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
								if(!uuid.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
									
									if(playerConfig.getString("StaffRank") == null || player.isOp()) {
										
										if(getPunishmentFromString(args[1]) != null) {
											Punishment punishment = getPunishmentFromString(args[1]);
											//only allowed 3 reports per hour
											//can't report someone for the SAME THING twice
											if(playerCanReport(player.getUniqueId(), uuid, punishment)) {
												
												int reportID = DeadMC.ReportsFile.data().getInt("Report.CurrentID") + 1;
												DeadMC.ReportsFile.data().set("Report.CurrentID", reportID);
												
												int count = 2;
												String comments = "";
												while(count < args.length) {
													comments = comments + args[count] + " ";
													count++;
												}
												
												logActivity(ActivityType.Report, reportID);
												
												DeadMC.ReportsFile.data().set("Report." + reportID + ".Reported by", player.getUniqueId().toString());
												DeadMC.ReportsFile.data().set("Report." + reportID + ".Reported", uuid.toString());
												
												ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
												DeadMC.ReportsFile.data().set("Report." + reportID + ".Date/Time", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
												DeadMC.ReportsFile.data().set("Report." + reportID + ".Reason", punishment.ordinal());
												DeadMC.ReportsFile.data().set("Report." + reportID + ".Comments", comments);
												
												//keep only last 500 reports
												if(DeadMC.ReportsFile.data().getString("Report." + (reportID - 500) + ".Reported") != null) {
													DeadMC.ReportsFile.data().set("Report." + (reportID - 500), null);
												}
												DeadMC.ReportsFile.save();
												
												player.sendMessage("");
												player.sendMessage(ChatColor.YELLOW + "================== Report Created ==================");
												player.sendMessage(ChatColor.GREEN + "Successfully reported " + args[0] + " for " + punishment.toString().toLowerCase().replace("_", " ") + ".");
												if(args.length > 2)
													player.sendMessage(ChatColor.WHITE + "Comments: " + comments);
												player.sendMessage(ChatColor.WHITE + "A staff member will review the report as soon as possible.");
												player.sendMessage(ChatColor.YELLOW + "==================================================");
												player.sendMessage("");
												
												for(Player staff : Bukkit.getOnlinePlayers()) {
													if(PlayerConfig.getConfig(staff.getUniqueId()).getString("StaffRank") != null) {
														staff.sendMessage("");
														staff.sendMessage(ChatColor.YELLOW + "================== Report Created ==================");
														staff.sendMessage(ChatColor.GREEN + player.getName() + " reported " + args[0] + " for " + punishment.toString().toLowerCase().replace("_", " ") + ".");
														if(args.length > 2)
															staff.sendMessage(ChatColor.WHITE + "Comments: " + comments);
														staff.sendMessage(ChatColor.WHITE + "Please review and action this.");
														staff.sendMessage(ChatColor.YELLOW + "==================================================");
														staff.sendMessage("");
													}
												}
												
												//add to players last reports
												List<Integer> lastReports = new ArrayList<Integer>();
												if(lastThreeReports.containsKey(player.getUniqueId().toString()))
													lastReports = lastThreeReports.get(player.getUniqueId().toString());
												int activityID = DeadMC.ReportsFile.data().getInt("Activity.CurrentID");
												lastReports.add(activityID);
												
												//Chat.broadcastMessage("Adding: " + lastReports);
												lastThreeReports.remove(player.getUniqueId().toString());
												lastThreeReports.put(player.getUniqueId().toString(), lastReports);
												//Chat.broadcastMessage("Last three reports: " + lastThreeReports);
												
											} else
												player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "You can only report 3 players per hour, and not the same player for the same thing.");
										} else
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " must be a valid RULE.");
										
									} else
										player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Staff cannot use this function. Use " + ChatColor.GOLD + "/punish" + ChatColor.WHITE + " instead.");
									
								} else
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "You cannot report yourself.");
							} else
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[0] + " is not an existing player.");
						} else {
							player.sendMessage("");
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Please add evidence/notes.");
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/r <player> <rule ID> <evidence/notes>");
							player.sendMessage("");
						}
						
					}
				}
			});
		}
		
		if(commandLabel.equalsIgnoreCase("dmc")) {

			if(args.length == 0) {
				player.sendMessage("Use /dmc <command>.");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("reset_nether")) {
				File directoryToBeDeleted = new File("world_nether");
				deleteDirectory(directoryToBeDeleted);
			}
			
			if(sender instanceof Player) {
				PlayerConfig staffConfig = PlayerConfig.getConfig(player);
				if(staffConfig.getString("StaffRank") == null || staffConfig.getInt("StaffRank") < StaffRank.MODERATOR.ordinal()) {
					return true;
				}
			}
			
			if(args[0].equalsIgnoreCase("bypass_alias_bans")) {
				//allows the player to bypass any bans (total override)
				
				//dmc reset_aliases <player>
				if(args.length == 2) {
					OfflinePlayer playerToReset = Bukkit.getOfflinePlayer(args[1]);
					UUID uuid = playerToReset.getUniqueId();
					if(!DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
						player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.YELLOW + args[1] + " is not an existing player.");
						return true;
					}
					
					PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
					playerConfig.set("Bypass_Alias_Bans", playerConfig.getString("Bypass_Alias_Bans") == null ? true : null);
					playerConfig.save();
					
					if(playerConfig.getString("Bypass_Alias_Bans") == null) {
						if(sender instanceof Player) {
							player.sendMessage(playerToReset.getName() + " no longer bypasses all aliases' bans.");
						} else {
							Chat.broadcastMessage(playerToReset.getName() + " no longer bypasses all aliases' bans.", true);
						}
					} else {
						if(sender instanceof Player) {
							player.sendMessage(playerToReset.getName() + " now bypasses all aliases' bans.");
						} else {
							Chat.broadcastMessage(playerToReset.getName() + " now bypasses all aliases' bans.", true);
						}
					}
					
				} else {
					if(sender instanceof Player) {
						player.sendMessage("Use /dmc bypass_alias_bans <player who will bypass their aliases' bans>. Note that their own bans will still stand.");
					} else {
						Chat.broadcastMessage("Use /dmc bypass_alias_bans <player who will bypass their aliases' bans>", true);
					}
				}
				
				return true;
			}
			
			if(args[0].equalsIgnoreCase("clear_player_config_logging")) {
				File directoryToBeDeleted = new File("player config logs");
				deleteDirectory(directoryToBeDeleted);
				Chat.broadcastMessage("Deleted all player config logs", true);
			}
			if(args[0].equalsIgnoreCase("toggle_player_config_logging")) {
				if(DeadMC.RestartFile.data().getString("LogPlayerConfigActions") == null) {
					DeadMC.RestartFile.data().set("LogPlayerConfigActions", true);
					DeadMC.RestartFile.save();
					
					Chat.broadcastMessage("Enabled player config logging", true);
				} else {
					DeadMC.RestartFile.data().set("LogPlayerConfigActions", null);
					DeadMC.RestartFile.save();
					
					Chat.broadcastMessage("Disabled player config logging", true);
				}
				return true;
			}
			
			if(args[0].equalsIgnoreCase("set_global_pos")) {
				//autotab shows existing global positions
				String posName = args[1];
				boolean alreadyExists = DeadMC.TravelFile.data().getString("Global Position." + posName) != null;
				
				if(alreadyExists) player.sendMessage("Overrwriting position for " + posName + ".");
				else player.sendMessage("Creating global position '" + posName + "'.");
				
				//overrwrite
				DeadMC.TravelFile.data().set("Global Position." + posName, player.getLocation());
				DeadMC.TravelFile.save();
			}
			
			if(args[0].equalsIgnoreCase("temp")) {
				//fix region heights
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
					Map<String, ProtectedRegion> wgRegionsWithID = regionManager.getRegions(); //region name, protected region
					List<ProtectedRegion> protectedRegions = new ArrayList<ProtectedRegion>();
					Bukkit.getScheduler().runTask(plugin, () -> {
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/world world");
					});
					Tools.sleepThread(1000);
					wgRegionsWithID.forEach((regionID, region) -> {
						Bukkit.getScheduler().runTask(plugin, () -> {
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "region select " + region.getId());
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/expand 500 down");
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/expand 500 up");
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "region redefine " + region.getId());
						});
						Tools.sleepThread(100);
					});
				});
				return true;
			}
			
			if(args[0].equalsIgnoreCase("recalc_shops")) {
				Shops.recalcAllShops();
			}
			
			if(args[0].equalsIgnoreCase("recalc_claimblocks")) {
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
					int broken = 0;
					for(String originalTownName : DeadMC.TownFile.data().getStringList("Active")) {
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						int actualClaimBlocks = 0;
						int initialClaimBlocks = townConfig.getInt("ClaimedBlocks");
						
						//get all regions
						List<String> mustRemoveRegions = new ArrayList<String>();
						for(String regionID : townConfig.getStringList("Regions")) {
							try {
								//get volume
								String regionName = originalTownName + "/" + regionID;
								ProtectedRegion region = regionManager.getRegion(regionName);
								if(region == null) mustRemoveRegions.add(regionID);
								if(region.getPriority() == 1) {
									actualClaimBlocks += region.volume() / 256;
								}
							} catch(Exception e) {
								Chat.logError(e);
							}
						}
						
						//remove broken regions:
						if(mustRemoveRegions.size() > 0) {
							Chat.broadcastMessage("Fixed " + mustRemoveRegions.size() + " regions in " + originalTownName + ".", true);
							List<String> regionsAfterRemovingBrokenOnes = townConfig.getStringList("Regions");
							for(String regionID : mustRemoveRegions) {
								regionsAfterRemovingBrokenOnes.remove(regionID);
							}
							townConfig.set("Regions", regionsAfterRemovingBrokenOnes);
							townConfig.save();
						}
						
						//save to town config
						if(initialClaimBlocks != actualClaimBlocks) {
							Chat.broadcastMessage("Fixed " + originalTownName + "'s claim blocks.", true);
							townConfig.set("ClaimedBlocks", actualClaimBlocks);
							townConfig.save();
							broken++;
						}
					}
					Chat.broadcastMessage(broken + " town's claim blocks were fixed.", true);
				});
			}
			
			if(args[0].equalsIgnoreCase("loghackingwarning")) {
				if(sender instanceof Player) {
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "This is a console only command.");
					return true;
				}

				UUID uuid = UUID.fromString(args[1]);
				HackingLevel hackingLevel = HackingLevel.values()[Integer.parseInt(args[2])];
				String detection = args[3];
				
				giveHackingPunishment(uuid, hackingLevel, detection);
				return true;
			}
			
			if(args[0].equalsIgnoreCase("transfer-account")) {
				//move wix's regions over, must be before moving yml files
				
				if(args.length == 1) {
					Chat.debug("The players cannot be online or loaded. Preferably run after server restart in maintenance. Checks first if both accounts exist on the server.");
					return true;
				}
				
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					
					//regions:
					OfflinePlayer oldPlayer = Bukkit.getOfflinePlayer(args[1]);
					OfflinePlayer newPlayer = Bukkit.getOfflinePlayer(args[2]);
					UUID oldUUID = oldPlayer.getUniqueId();
					UUID newUUID = newPlayer.getUniqueId();
					
					Chat.debug("Transferring " + oldUUID.toString() + " to " + newUUID.toString());

					if(!DeadMC.PlayerFile.data().getStringList("Active").contains(oldUUID.toString())) {
						Chat.debug("Cannot complete transfer! " + oldPlayer.getName() + " is not an active player.");
						return;
					}
					if(!DeadMC.PlayerFile.data().getStringList("Active").contains(newUUID.toString())) {
						Chat.debug("Cannot complete transfer! " + newPlayer.getName() + " is not an active player.");
						return;
					}
					if(Bukkit.getPlayer(oldUUID) != null || Bukkit.getPlayer(newUUID) != null) {
						Chat.debug("Cannot complete transfer! One or both of the players are online.");
						return;
					}

					//get list of WG regions
					RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
					Map<String, ProtectedRegion> wgRegionsWithID = regionManager.getRegions(); //region name, protected region
					List<ProtectedRegion> protectedRegions = new ArrayList<ProtectedRegion>();
					wgRegionsWithID.forEach((regionID, region) -> {
						//only include level 0 or 1 regions (dont need to check higher)
						if(region.getPriority() <= 1)
							protectedRegions.add(region);
					});
					
					//loop each protected region
					for(ProtectedRegion region : protectedRegions) {
						if(region.getOwners().getUniqueIds().contains(oldUUID)) {
							Bukkit.getScheduler().runTask(plugin, () -> Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "region removeowner -w world " + region.getId() + " " + oldUUID.toString()));
							Bukkit.getScheduler().runTask(plugin, () -> Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "region addowner -w world " + region.getId() + " " + newUUID.toString()));
							Tools.sleepThread(20);
						}
						if(region.getMembers().getUniqueIds().contains(oldUUID)) {
							Bukkit.getScheduler().runTask(plugin, () -> Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "region removemember -w world " + region.getId() + " " + oldUUID.toString()));
							Bukkit.getScheduler().runTask(plugin, () -> Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "region addmember -w world " + region.getId() + " " + newUUID.toString()));
							Tools.sleepThread(20);
						}
					}
					
					//town status:
					
					PlayerConfig oldConfig = PlayerConfig.getConfig(oldUUID);
					String originalTownName = oldConfig.getString("Town");
					if(oldConfig.getString("Town") != null) {
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						Chat.debug("Updating town (" + originalTownName + ")");
						Town.addMember(newUUID, originalTownName);
						
						if(townConfig.getString("Mayor").equalsIgnoreCase(oldUUID.toString())) {
							townConfig.set("Mayor", newUUID.toString());
						}
						if(townConfig.getStringList("CoMayors").contains(oldUUID.toString())) {
							List<String> coMayors = townConfig.getStringList("CoMayors");
							coMayors.remove(oldUUID.toString());
							coMayors.add(newUUID.toString());
							townConfig.set("CoMayors", coMayors);
						}
						townConfig.save();

						Town.removeMember(oldUUID);
					}
					
					//copy minecraft stats (world)
					Chat.debug("Copying world data");
					copyPlayerWorldData("world", oldUUID, newUUID);
					Chat.debug("Copying world nether data");
					copyPlayerWorldData("world_nether", oldUUID, newUUID);
					
					
					Chat.debug("Discarding player config");
					int donateRank = oldConfig.getInt("DonateRank");
					oldConfig.discard(true);
					
					//copy the player config
					Chat.debug("Copying player config");
					File oldPlayerConfig = new File(Bukkit.getWorldContainer(), "plugins" + File.separator + "DeadMC" + File.separator + "players" + File.separator + oldUUID.toString() + ".yml");
					File newPlayerConfig = new File(Bukkit.getWorldContainer(), "plugins" + File.separator + "DeadMC" + File.separator + "players" + File.separator + newUUID.toString() + ".yml");
					try {
						Files.move(oldPlayerConfig.toPath(), newPlayerConfig.toPath(), StandardCopyOption.REPLACE_EXISTING);
					} catch(IOException e) {
						Chat.logError("Error moving player config! " + e.getMessage());
					}
					
					//reload player configs
					
					
					//remove old player
					Chat.debug("Removing old player");
					List<String> activePlayers = DeadMC.PlayerFile.data().getStringList("Active");
					activePlayers.remove(oldUUID.toString());
					DeadMC.PlayerFile.data().set("Active", activePlayers);
					DeadMC.PlayerFile.save();
					
					PlayerConfig newConfig = PlayerConfig.getConfig(newUUID);
					
					//donate status:
					if(donateRank > 0) {
						Chat.debug("Updating donor rank");
						Bukkit.getScheduler().runTask(plugin, () -> Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "setrank " + newPlayer.getName() + " " + Rank.values()[donateRank].toString()));
						newConfig.set("DonateRank", donateRank);
						newConfig.save();
					}
					
					//mcmmo:
					Chat.debug("MCMMO:");
					for(PrimarySkillType skill : PrimarySkillType.values()) {
						Tools.sleepThread(1000);
						Bukkit.getScheduler().runTask(plugin, () -> {
							Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "mmoedit " + newPlayer.getName() + " " + skill.toString() + " " + ExperienceAPI.getLevelOffline(oldUUID, skill.toString()) + " -s");
							Chat.debug(" - " + newPlayer.getName() + " " + skill.toString() + " level: " + ExperienceAPI.getLevelOffline(newUUID, skill.toString()));
						});
					}
					for(PrimarySkillType skill : PrimarySkillType.values()) {
						Tools.sleepThread(1000);
						Bukkit.getScheduler().runTask(plugin, () -> {
							Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "mmoedit " + oldPlayer.getName() + " " + skill.toString() + " 0 -s");
							Chat.debug(" - " + oldPlayer.getName() + " " + skill.toString() + " level: " + ExperienceAPI.getLevelOffline(oldUUID, skill.toString()));
						});
					}
					
					Chat.debug("Leaderboard checks");
					for(Leaderboard leaderboard : Leaderboard.values()) {
						for(int count = 1; count <= 10; count++) {
							if(DeadMC.LeaderboardFile.data().getString(leaderboard.toString() + "." + count + ".UUID") != null
									&& DeadMC.LeaderboardFile.data().getString(leaderboard.toString() + "." + count + ".UUID").equalsIgnoreCase(oldPlayer.getUniqueId().toString())) {
								DeadMC.LeaderboardFile.data().set(leaderboard.toString() + "." + count + ".UUID", newPlayer.getUniqueId().toString());
								Chat.debug(" - Updated " + leaderboard.toString());
							}
						}
					}
					DeadMC.LeaderboardFile.save();

				});
				
				return true;
			}

			if(args[0].equalsIgnoreCase("unban")) {
				UUID othersUUID = Bukkit.getOfflinePlayer(args[1]).getUniqueId();
				PlayerConfig othersConfig = PlayerConfig.getConfig(othersUUID);
				if(othersConfig.getString("Coins") != null) {
					if(othersConfig.getString("Banned.Reason") != null) {
						int timeSinceBan = DateCode.getTimeSince(othersConfig.getString("Banned.Start"));
						
						Admin.logBan(othersUUID, Punishment.valueOf(othersConfig.getString("Banned.Reason").replace(" ", "_")), othersConfig.getString("Banned.Start"), timeSinceBan / 60);
					}
					othersConfig.set("Banned", null);
					othersConfig.set("Muted", null);
					othersConfig.save();
					Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + args[1] + " no longer has any punishments.", true);
					return true;
				} else
					Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " is not an existing player.", true);
			}
			
			if(args[0].equalsIgnoreCase("deny_discord_chat")) {
				DiscordSRVListener.allowChat(false);
				return true;
			}
			if(args[0].equalsIgnoreCase("allow_discord_chat")) {
				DiscordSRVListener.allowChat(true);
				return true;
			}
			
			if(args[0].equalsIgnoreCase("reset_aliases")) {
				
				//dmc reset_aliases <player>
				if(args.length > 1) {
					OfflinePlayer playerToReset = Bukkit.getOfflinePlayer(args[1]);
					UUID uuid = playerToReset.getUniqueId();
					if(!DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
						player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.YELLOW + args[1] + " is not an existing player.");
						return true;
					}
					
					Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
						@Override
						public void run() {
							Player player = (Player) sender;
							PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
							
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.YELLOW + "Clearing " + playerToReset.getName() + " aliases...");
							//clear each associated IP:
							int ips = 0;
							List<String> ipsToTurn = new ArrayList<String>();
							List<String> activeIPs = DeadMC.IpsFile.data().getStringList("Active");
							for(String ip : activeIPs) {
								IpConfig ipConfig = IpConfig.getConfig(ip);
								
								List<String> usersWithIp = ipConfig.getStringList("UUID");
								if(usersWithIp.contains(uuid.toString())) {
									if(usersWithIp.size() == 1) {
										//just turn ip
										ipsToTurn.add(ip);
									} else {
										usersWithIp.remove(uuid.toString());
										ipConfig.set("UUID", usersWithIp);
										ipConfig.save();
									}
									
									ips++;
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "  Cleared from " + ChatColor.GOLD + ip);
								}
							}
							
							//turn ips:
							if(ipsToTurn.size() > 0) {
								for(String ip : ipsToTurn) {
									File ipFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "ips" + File.separator + ip + ".yml");
									ipFile.delete();
									
									activeIPs.remove(ip);
								}
								DeadMC.IpsFile.data().set("Active", activeIPs);
								DeadMC.IpsFile.save();
							}
							
							//clear aliases:
							if(playerConfig.getString("Aliases") != null) {
								int aliases = playerConfig.getStringList("Aliases").size();
								playerConfig.set("Aliases", null);
								playerConfig.save();
								
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Cleared a total of " + ChatColor.GOLD + ChatColor.BOLD + aliases + ChatColor.WHITE + " aliases, and cleared the association with " + ChatColor.GOLD + ChatColor.BOLD + ips + ChatColor.WHITE + " ips.");
							}
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Done.");
							
						}
					});
				}
				
				return true;
			}
			
			if(args[0].equalsIgnoreCase("user-search")) {
				if(args.length == 1) {
					if(sender instanceof Player) player.sendMessage("To search users: /dmc user_search <keyword>.");
					else Chat.broadcastMessage("To search users: /dmc user_search <keyword>.", true);
					return true;
				}
				
				if(sender instanceof Player) player.sendMessage("Searching...");
				else Chat.broadcastMessage("Searching...", true);
				
				Player finalPlayer1 = player;
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					String keyword = args[1].toLowerCase();
					int results = 0;
					
					for(String uuidString : DeadMC.PlayerFile.data().getStringList("Active")) {
						UUID uuid = UUID.fromString(uuidString);
						PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
						
						String name = Bukkit.getOfflinePlayer(uuid).getName();
						if(name.toLowerCase().contains(keyword)) {
							//display
							if(sender instanceof Player) finalPlayer1.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + name + (playerConfig.getString("Name") != null ? ChatColor.BOLD + " | " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Name")) : "") + ChatColor.WHITE + ChatColor.BOLD + " | " + ChatColor.WHITE + uuidString);
							else Chat.broadcastMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + name + (playerConfig.getString("Name") != null ? ChatColor.BOLD + " | " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Name")) : "") + ChatColor.WHITE + ChatColor.BOLD + " | " + ChatColor.WHITE + uuidString);
							results++;
							continue;
						}
						
						//check nick name
						if(playerConfig.getString("Name") != null
						&& Chat.stripColor(playerConfig.getString("Name")).toLowerCase().contains(keyword)) {
							//display
							if(sender instanceof Player) finalPlayer1.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + name + (playerConfig.getString("Name") != null ? ChatColor.BOLD + " | " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Name")) : "") + ChatColor.WHITE + ChatColor.BOLD + " | " + ChatColor.WHITE + uuidString);
							else Chat.broadcastMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + name + (playerConfig.getString("Name") != null ? ChatColor.BOLD + " | " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Name")) : "") + ChatColor.WHITE + ChatColor.BOLD + " | " + ChatColor.WHITE + uuidString);
							results++;
						}
					}
					
					if(sender instanceof Player) finalPlayer1.sendMessage("Complete! (" + ChatColor.BOLD + results + ChatColor.WHITE + " results...)");
					else Chat.broadcastMessage("Complete! (" + ChatColor.BOLD + results + ChatColor.WHITE + " results...)", true);
				});
				
				return true;
			}
			
			if(args[0].equalsIgnoreCase("reloadconfig")) {
				//dmc reloadconfig <type> <name>
				
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					if(args[1].equalsIgnoreCase("yml")) {
						if(DeadMC.configs.containsKey(args[2])) {
							DeadMC.configs.get(args[2]).reload();
							Chat.broadcastMessage("Reloaded yml file " + args[2] + ".", true);
						} else {
							Chat.broadcastMessage("Invalid yml file '" + args[2] + "'.", true);
						}
					}
					if(args[1].equalsIgnoreCase("player")) {
						try {
							UUID uuid = UUID.fromString(args[2]);
							if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
								PlayerConfig.getConfig(uuid).discard(true);
								PlayerConfig.getConfig(uuid).save();
								Chat.broadcastMessage("Reloaded player file for " + Bukkit.getOfflinePlayer(uuid).getName() + ".", true);
							} else {
								Chat.broadcastMessage("No file found for UUID '" + args[2] + "'.", true);
							}
						} catch(IllegalArgumentException e) {
							//not a UUID, try find player name
							if(Bukkit.getOfflinePlayer(args[2]).hasPlayedBefore()) {
								UUID uuid = Bukkit.getOfflinePlayer(args[2]).getUniqueId();
								if(uuid != null) {
									if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
										//is a valid player
										PlayerConfig.getConfig(uuid).discard(true);
										PlayerConfig.getConfig(uuid).save();
										Chat.broadcastMessage("Reloaded player file for " + Bukkit.getOfflinePlayer(uuid).getName() + ".", true);
									}
								} else
									Chat.broadcastMessage("Cannot find UUID for '" + args[2] + "'.", true);
							} else Chat.broadcastMessage("Unknown player '" + args[2] + "'.", true);
						}
					}
					if(args[1].equalsIgnoreCase("town")) {
						String town = args[2];
						if(DeadMC.TownFile.data().getStringList("Active").contains(town)) {
							TownConfig.getConfig(town).discard();
							TownConfig.getConfig(town).save();
							Chat.broadcastMessage("Reloaded town file for " + town + ".", true);
						} else {
							Chat.broadcastMessage("Unknown town '" + args[2] + "'.", true);
						}
					}
					if(args[1].equalsIgnoreCase("travel")) {
						String tp = args[2];
						if(DeadMC.TravelFile.data().getStringList("Active").contains(tp)) {
							PublicTP.getConfig(tp).discard();
							PublicTP.getConfig(tp).save();
							Chat.broadcastMessage("Reloaded travel file for " + tp + ".", true);
						} else {
							Chat.broadcastMessage("Unknown travel point '" + args[2] + "'.", true);
						}
					}
					if(args[1].equalsIgnoreCase("shop")) {
						String town = args[2];
						String fileName = args[3];
						File file = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + town + File.separator + fileName);
						String regionName = town + "/" + fileName.replace(".yml", "");
						if(file.exists()) {
							RegionConfig.getConfig(regionName).discard();
							RegionConfig.getConfig(regionName).save();
							Chat.broadcastMessage("Reloaded travel file for " + regionName + ".", true);
						} else {
							Chat.broadcastMessage("Unknown file for shop '" + regionName + "'.", true);
						}
					}
				});
				
				return true;
			}
			if(args[0].equalsIgnoreCase("debug")) {
				String type = "";
				if(args.length == 2) {
					type = "." + args[1];
				}
				if(DeadMC.RestartFile.data().getString("DebugMessages" + type) != null) {
					DeadMC.RestartFile.data().set("DebugMessages" + type, null);
					Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Disabled debug messages.");
				} else {
					DeadMC.RestartFile.data().set("DebugMessages" + type, true);
					Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "Enabled debug messages.");
				}
				DeadMC.RestartFile.save();
				
			}
					if(args[0].equalsIgnoreCase("analyse_world") && player.isOp()) {
						
						//NOTICE:
						player.sendMessage(ChatColor.BOLD + "========================================");
						player.sendMessage(ChatColor.RED + "Must RESTART server BEFORE running to update regions last modified time.");
						player.sendMessage(ChatColor.WHITE + "Note, date modified in the region data directory INCLUDES when the region was last entered by a player. Not just when a block is modified.");
						player.sendMessage(ChatColor.RED + "Ensure no players are online when running.");
						player.sendMessage(ChatColor.WHITE + "To confirm deletion, use " + ChatColor.GOLD + "/dmc analyse_world confirm");
						player.sendMessage(ChatColor.BOLD + "========================================");
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
							@Override
							public void run() {
								Player player = (Player) sender;
								//keep list of SAFE regions
								List<RegionData> safe = new ArrayList<RegionData>();
								List<String> safeWithID = new ArrayList<String>();
								
								//get list of WG regions
								RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
								Map<String, ProtectedRegion> wgRegionsWithID = regionManager.getRegions(); //region name, protected region
								List<ProtectedRegion> protectedRegions = new ArrayList<ProtectedRegion>();
								wgRegionsWithID.forEach((regionID, region) -> {
									//only include level 0 or 1 regions (dont need to check higher)
									if(region.getPriority() <= 1)
										protectedRegions.add(region);
								});
								
								//loop each protected region
								for(ProtectedRegion wgRegion : protectedRegions) {
									//get the min and max corners
									List<BlockVector2> corners = wgRegion.getPoints();
									BlockVector2 minCorner = null;
									BlockVector2 maxCorner = null;
									for(BlockVector2 corner : corners) {
										int size = corner.getX() + corner.getZ();
										if(maxCorner == null || size > (maxCorner.getX() + maxCorner.getZ()))
											maxCorner = corner;
										if(minCorner == null || size < (minCorner.getX() + minCorner.getZ()))
											minCorner = corner;
									}
									
									//convert to chunk data
									int minChunkX = (int) Math.floor(minCorner.getX() / 16.0);
									int minChunkZ = (int) Math.floor(minCorner.getZ() / 16.0);
									int maxChunkX = (int) Math.floor(maxCorner.getX() / 16.0);
									int maxChunkZ = (int) Math.floor(maxCorner.getZ() / 16.0);
									
									//convert to region data
									int minRegionX = (int) Math.floor(minChunkX / 32.0);
									int minRegionZ = (int) Math.floor(minChunkZ / 32.0);
									int maxRegionX = (int) Math.floor(maxChunkX / 32.0);
									int maxRegionZ = (int) Math.floor(maxChunkZ / 32.0);
									
									RegionData minRegion = new RegionData(minRegionX, minRegionZ);
									RegionData maxRegion = new RegionData(maxRegionX, maxRegionZ);
									
									//add all the regions between
									for(int x = minRegion.x; x <= maxRegion.x; x++) {
										for(int z = minRegion.z; z <= maxRegion.z; z++) {
											//do not add if already in list
											String regionID = "r." + x + "." + z + ".mca";
											if(!safeWithID.contains(regionID)) {
												safe.add(new RegionData(x, z));
												safeWithID.add(regionID);
												Bukkit.getConsoleSender().sendMessage("Saving [" + x + ", " + z + "] for " + wgRegion.getId() + " (" + minCorner + " -> " + maxCorner + ")");
											} else {
												Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "(ignored-copy) Saving [" + x + ", " + z + "] for " + wgRegion.getId() + " (" + minCorner + " -> " + maxCorner + ")");
											}
										}
									}
									
									//delay so it doesn't stop:
									try {
										TimeUnit.MILLISECONDS.sleep(5);
									} catch(InterruptedException e) {
										e.printStackTrace();
									}
								}
								
								Bukkit.getConsoleSender().sendMessage("");
								Bukkit.getConsoleSender().sendMessage(ChatColor.BOLD + "===================================");
								Bukkit.getConsoleSender().sendMessage("");
								
								int notSafe = 0;
								long savedStorage = 0;
								long wastedStorage = 0;
								long oneDay = 86400000L;
								ZonedDateTime computerTime = ZonedDateTime.now(); //to adelaide time
								
								//now loop through all the region files in the world folder
								File regionDirectory = new File(Bukkit.getWorld("world").getWorldFolder() + File.separator + "region");
								Bukkit.getConsoleSender().sendMessage(regionDirectory.isDirectory() + " - " + regionDirectory.exists() + " - " + regionDirectory.getAbsolutePath());
								File[] regionFiles = regionDirectory.listFiles();
								for(File regionFile : regionFiles) {
									if(safeWithID.contains(regionFile.getName())) {
										Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + regionFile.getName() + " " + ChatColor.GREEN + "SAFE!" + ChatColor.WHITE + " (has region)");
										savedStorage += regionFile.length();
										continue;
									}
									
									long timePassed = (computerTime.toEpochSecond() * 1000) - regionFile.lastModified();
									if(timePassed < (oneDay * 7)) {
										Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + regionFile.getName() + " " + ChatColor.GREEN + "SAFE!" + ChatColor.WHITE + " (last accessed < 1 week ago)");
										savedStorage += regionFile.length();
										continue;
									}
									
									notSafe++;
									wastedStorage += regionFile.length();
									Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + regionFile.getName() + " " + ChatColor.RED + "Not safe" + ChatColor.WHITE + " (last accessed: " + Math.round(timePassed / (double) oneDay) + " days)");
									
									if(args.length > 1 && args[1].equalsIgnoreCase("confirm")) {
										regionFile.delete();
										
										File poiFile = new File(Bukkit.getWorld("world").getWorldFolder() + File.separator + "poi" + File.separator + regionFile.getName());
										if(poiFile.exists()) poiFile.delete();
									}
									
									//delay so it doesn't stop:
									try {
										TimeUnit.MILLISECONDS.sleep(5);
									} catch(InterruptedException e) {
										e.printStackTrace();
									}
								}
								
								player.sendMessage("");
								
								player.sendMessage(ChatColor.GREEN + "Finished! Exact results sent to console. Final results below:");
								
								//results here
								
								DecimalFormat df = new DecimalFormat("####0.00");
								double safeSize = savedStorage / 1073741824.0; //convert to GB
								double unsafeSize = wastedStorage / 1073741824.0; //convert to GB
								player.sendMessage("Safe files: " + safe.size() + " (" + ChatColor.BOLD + df.format(safeSize) + ChatColor.WHITE + "gb)");
								player.sendMessage("Not safe files: " + notSafe + " (" + ChatColor.BOLD + df.format(unsafeSize) + ChatColor.WHITE + "gb)");
								
							}
						});
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("create_flag_item")) {
						if(args.length == 1) {
							
							ItemStack item = player.getInventory().getItemInMainHand().clone();
							
							if(item.getType().toString().toLowerCase().contains("banner")) {
								
								ItemMeta meta = item.getItemMeta();
								List<String> lore = new ArrayList<String>();
								lore.add(ChatColor.GRAY + "This item will go back to the flag point on death or logout.");
								meta.setLore(lore);
								
								item.setItemMeta(meta);
								
								player.getInventory().setItemInMainHand(item);
								
								player.sendMessage("");
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Set " + ChatColor.BOLD + item.getType().toString().toLowerCase().replace("_", " ") + ChatColor.GREEN + " as flag item.");
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.BOLD + "PLACE THE BANNER TO SET THE RESPAWN POINT");
								player.sendMessage("");
								
							} else
								player.sendMessage(ChatColor.RED + "[DeadMC] Flag item must be a banner.");
						} else
							player.sendMessage(ChatColor.RED + "[DeadMC] Use /dmc create_flag_item to update the flag item.");
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("create_no_drop_item")) {
						if(args.length > 1) {
							String ID = args[1];
							
							ItemStack item = player.getInventory().getItemInMainHand().clone();
							
							ItemMeta meta = item.getItemMeta();
							List<String> lore = new ArrayList<String>();
							lore.add(ChatColor.GRAY + "This item doesn't drop on death.");
							meta.setLore(lore);
							
							item.setItemMeta(meta);
							
							player.getInventory().setItemInMainHand(item);
							
							DeadMC.DonatorFile.data().set("TownWars.NoDropItems." + ID, item);
							DeadMC.DonatorFile.save();
							
							player.sendMessage("");
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Set " + ChatColor.BOLD + item.getType().toString().toLowerCase().replace("_", " ") + ChatColor.GREEN + " as a no drop item.");
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Spawn this item with " + ChatColor.GOLD + "/dmc spawn_no_drop_item " + ID);
							player.sendMessage("");
							
						} else
							player.sendMessage(ChatColor.RED + "[DeadMC] Use /dmc create_no_drop_item <name ID> to save the item.");
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("spawn_no_drop_item")) {
						if(args.length > 1) {
							String ID = args[1];
							
							if(DeadMC.DonatorFile.data().getString("TownWars.NoDropItems." + ID) != null) {
								
								player.getInventory().addItem(DeadMC.DonatorFile.data().getItemStack("TownWars.NoDropItems." + ID).clone());
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Added " + ChatColor.BOLD + ID + ChatColor.GREEN + " to your inventory.");
								
							} else
								player.sendMessage(ChatColor.RED + "[DeadMC] That ID doesn't exist.");
						} else
							player.sendMessage(ChatColor.RED + "[DeadMC] Use /dmc create_no_drop_item <name ID> to save the item.");
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("villagers")) {
						
						if(args.length == 1) {
							
							Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
								@Override
								public void run() {
									
									FileConfiguration lootFile = DeadMC.LootFile.data();
									List<String> adjustedItems = new ArrayList<String>();
									if(lootFile.getString("Villagers.AdjustedItems") != null)
										adjustedItems = lootFile.getStringList("Villagers.AdjustedItems");
									
									Player player = (Player) sender;
									
									int emeraldCost = 0; //worked out later:
									
									player.sendMessage("");
									player.sendMessage("Emerald conversation rate: " + Economy.convertCoins(DeadMC.emeraldCost));
									player.sendMessage("Adjusted items for trading:");
									for(String material : adjustedItems) {
										
										String marketString = "NA";
										//loop all shops to see if market has shop
										FileConfiguration shops = DeadMC.ShopsFile.data();
										for(String shopCode : DeadMC.ShopsFile.data().getKeys(false)) {
											if(DeadMC.ShopsFile.data().getString(shopCode + ".Item") != null) {
												//shop is set up properly
												
												ItemStack shopItem = new ItemStack(shops.getItemStack(shopCode + ".Item")).clone();
												if(shopItem.getType().toString().equalsIgnoreCase(material)) { //is specified item
													int buyPrice = shops.getInt(shopCode + ".BuyPrice");
													marketString = ChatColor.BOLD + Economy.convertCoins(buyPrice);
													
													//using market price
													emeraldCost = (int) Math.ceil((double) buyPrice / (double) DeadMC.emeraldCost);
												}
											}
										}
										
										String adjustedString = "";
										if(lootFile.getString("Villagers." + material) != null) {
											//using adjusted price
											emeraldCost = (int) Math.ceil((double) lootFile.getInt("Villagers." + material) / (double) DeadMC.emeraldCost);
											adjustedString = ChatColor.DARK_PURPLE + "adjusted" + ChatColor.WHITE + ChatColor.ITALIC + " to " + ChatColor.BOLD + Economy.convertCoins(lootFile.getInt("Villagers." + material));
										}
										
										
										player.sendMessage("  " + ChatColor.BOLD + material + ChatColor.WHITE + ":");
										player.sendMessage("    - market price = " + marketString);
										player.sendMessage("    - " + adjustedString + " = " + ChatColor.GREEN + ChatColor.BOLD + emeraldCost + " emeralds");
									}
									
									//show list of items with conversions
									// DIAMOND: market price = (strike through)216 (adjusted:) 300 = 8 emeralds (ceil)
									// PUMPKIN: market price = NA (adjusted:) 90 = 3 emeralds (ceil)
								}
							});
						}
						if(args.length >= 4 && args[1].equalsIgnoreCase("set")) {
							
							Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
								@Override
								public void run() {
									
									FileConfiguration lootFile = DeadMC.LootFile.data();
									List<String> adjustedItems = new ArrayList<String>();
									if(lootFile.getString("Villagers.AdjustedItems") != null)
										adjustedItems = lootFile.getStringList("Villagers.AdjustedItems");
									
									Player player = (Player) sender;
									
									Boolean hasShop = false;
									if(args[3].equalsIgnoreCase("use_market_value")) {
										FileConfiguration shops = DeadMC.ShopsFile.data();
										for(String shopCode : DeadMC.ShopsFile.data().getKeys(false)) {
											if(DeadMC.ShopsFile.data().getString(shopCode + ".Item") != null) {
												//shop is set up properly
												
												ItemStack shopItem = new ItemStack(shops.getItemStack(shopCode + ".Item")).clone();
												if(shopItem.getType().toString().equalsIgnoreCase(args[2])) { //is specified item
													hasShop = true;
													break;
												}
											}
										}
									}
									
									//set conversion price with /dmc villagers set <item> <price>/USE MARKET VALUE
									
									if(Chat.isInteger(args[3])) {
										//set using adjusted price
										lootFile.set("Villagers." + args[2], Integer.parseInt(args[3]));
									} else if(args[3].equalsIgnoreCase("use_market_value")) {
										if(!hasShop) {
											player.sendMessage(args[2] + " does not have a market shop price.");
											return;
										}
										
										lootFile.set("Villagers." + args[2], null);
									}
									if(!adjustedItems.contains(args[2])) {
										adjustedItems.add(args[2]);
										lootFile.set("Villagers.AdjustedItems", adjustedItems);
									}
									DeadMC.LootFile.save();
									
									player.sendMessage(ChatColor.GREEN + "Adjusted price for " + args[2]);
								}
							});
						}
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("playername")) {
						Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
							@Override
							public void run() {
								if(args.length >= 3) {
									String playerName = args[1];
									for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
										PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
										if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[1])) {
											//used nick name
											playerName = onlinePlayer.getName();
											break;
										}
									}
									UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
									
									//dmc playername <name> <new name>
									
									if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
										PlayerConfig otherConfig = PlayerConfig.getConfig(uuid);
										
										otherConfig.set("Name", args[2]);
										otherConfig.save();
										
										Chat.debug(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Changed name of " + Bukkit.getOfflinePlayer(playerName).getName() + " to " + args[2] + ".");
										if(sender instanceof Player player) player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Changed name of " + Bukkit.getOfflinePlayer(playerName).getName() + " to " + args[2] + ".");
										
										//update placeholder:
										PlaceHolders.playersName.replace(uuid.toString(), args[2]);
										
									} else {
										Chat.debug(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " is not an existing player.");
										if(sender instanceof Player player) player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " is not an existing player.");
									}
									
								} else {
									Chat.debug(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "Use: /dmc playername <player name> <new name>");
									if(sender instanceof Player player) player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "Use: /dmc playername <player name> <new name>");
								}
								
							}
						});
					}
					
					if(args[0].equalsIgnoreCase("townname")) {
						Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
							@Override
							public void run() {
								if(args.length >= 3) {
									if(Town.townNameIsTaken(args[1])) {
										String originalTownName = Town.getOriginalTownName(Town.townNameCased(args[1]));
										String townDisplayName = Town.getTownDisplayName(originalTownName);
										TownConfig townConfig = TownConfig.getConfig(originalTownName);
										
										Chat.debug(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "You have renamed " + townDisplayName + " to " + args[2] + ".");
										if(sender instanceof Player player)
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "You have renamed " + townDisplayName + " to " + args[2] + ".");
										
										List<String> aliases = new ArrayList<String>();
										if(DeadMC.TownFile.data().getString("Aliases") != null)
											aliases = DeadMC.TownFile.data().getStringList("Aliases");
										if(townConfig.getString("Alias") != null) {
											DeadMC.TownFile.data().set("AliasOriginalName." + townDisplayName, null); //remove old alias
											aliases.remove(townConfig.getString("Alias")); //remove old alias
										}
										aliases.add(args[2]); //add new alias
										DeadMC.TownFile.data().set("Aliases", aliases);
										
										DeadMC.TownFile.data().set("AliasOriginalName." + args[2], originalTownName); //set new alias
										
										townConfig.set("Alias", args[2]);
										townConfig.save();
										DeadMC.TownFile.save();
										
									} else {
										Chat.debug(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " is not an existing town.");
										if(sender instanceof Player player)
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " is not an existing town.");
									}
									
								} else {
									Chat.debug(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "Use: /dmc townname <town name> <new name>");
									if(sender instanceof Player player) player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "Use: /dmc townname <town name> <new name>");
								}
							}
						});
					}
					
					if(args[0].equalsIgnoreCase("setcoins")) {
						PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
						if(othersConfig.getString("Coins") != null) {
							if(Chat.isInteger(args[2])) {
								othersConfig.set("Coins", Integer.parseInt(args[2]));
								othersConfig.save();
							} else
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[2] + " is not an integer (coins).");
						} else
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " is not an existing player.");
						return true;
					}
					
					
					if(args[0].equalsIgnoreCase("resetlocation")) {
						if(args.length == 2) {
							Player playerTp = Bukkit.getPlayer(args[1]);
							if(playerTp != null && playerTp.isOnline()) {
								player.sendMessage("Teleported.");
								PaperLib.teleportAsync(playerTp, Travel.getGlobalPosition("Longdale"));
							} else {
								PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
								othersConfig.set("ResetLocation", true);
								othersConfig.save();
								
								player.sendMessage("Will teleport on login.");
							}
						} else player.sendMessage("Use /dmc resetlocation <player>");
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("analyse_memory")) {
						Player finalPlayer = player;
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							int deadmcThreads = 0;
							for(Thread thread : Thread.getAllStackTraces().keySet()) {
								for(StackTraceElement stack : thread.getStackTrace()) {
									if(stack.getClassName().contains("DeadAPImaven")) {
										deadmcThreads++;
									}
								}
							}
							finalPlayer.sendMessage(Thread.activeCount() + " active threads (" + deadmcThreads + " are deadmc threads)");
							finalPlayer.sendMessage(Chat.loggedErrors.size() + " logged errors");
							finalPlayer.sendMessage(PublicTP.configs.size() + " PublicTP configs");
							finalPlayer.sendMessage(IpConfig.configs.size() + " IP configs");
							finalPlayer.sendMessage(PlayerConfig.configs.size() + " player configs");
							finalPlayer.sendMessage(RegionConfig.configs.size() + " region configs");
							finalPlayer.sendMessage(TownConfig.configs.size() + " town configs");
							finalPlayer.sendMessage(Bukkit.getWorld("world").getLoadedChunks().length + " chunks loaded");
							finalPlayer.sendMessage(Explosion.affectedBlocks.size() + " queued blocks for regen");
							Chat.debug(Explosion.affectedBlocks + "");
							finalPlayer.sendMessage(Explosion.delayedBlocks.size() + " delayed blocks for regen");
							Chat.debug(Explosion.delayedBlocks + "");
							finalPlayer.sendMessage(Explosion.twoBlockTallBlocks.size() + " two-block-tall blocks saved for regen");
							Chat.debug(Explosion.twoBlockTallBlocks + "");
							finalPlayer.sendMessage(Pets.allPetUUIDs.size() + " active pets");
							//not a valid measure with aikars flags
							DecimalFormat df = new DecimalFormat("####0.00");
							double memory = Runtime.getRuntime().freeMemory() / 1073741824.0; //convert to GB
							finalPlayer.sendMessage(df.format(memory) + "GB free memory");
						});
						return true;
					}
					
					if(args[0].equalsIgnoreCase("delete_region")) {
						//dmc delete_region <town> <land>
						if(Town.townNameIsTaken(args[1])) {
							String originalTownName = Town.getOriginalTownName(args[1]);
							
							RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
							
							String regionName = originalTownName.toLowerCase() + "/" + args[2].toLowerCase();
							ProtectedRegion region = regionManager.getRegion(regionName);
							
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							if(regionManager.hasRegion(regionName)) {
								
								ApplicableRegionSet regionsWithinRegion = regionManager.getApplicableRegions(region);
								if(Regions.highestPriorityLand(regionsWithinRegion.getRegions()) == region.getPriority()) {
									
									List<String> shops = new ArrayList<String>();
									File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionName + ".yml");
									if(regionFile.exists()) {
										RegionConfig regionConfig = RegionConfig.getConfig(regionName);
										shops = regionConfig.getStringList("Shops");
									}
									
									//give back claimed blocks (only for level 1 land)
									if(region.getPriority() == 1)
										townConfig.set("ClaimedBlocks", townConfig.getInt("ClaimedBlocks") - (region.volume() / 256));
									
									//update regions in town config
									List<String> regions = townConfig.getStringList("Regions");
									regions.remove(args[2].toLowerCase());
									townConfig.set("Regions", regions);
									townConfig.save();
									
									//delete the region
									Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg delete " + regionName + " -w world");
									
									//update shops
									if(shops.size() > 0) {
										//remove shops if no more regions left
										if(regionsWithinRegion.size() == 1) {
											//track number of shops player has:
											for(UUID owner : regionManager.getRegion(regionName).getOwners().getUniqueIds()) {
												PlayerConfig ownerConfig = PlayerConfig.getConfig(owner);
												int newAmount = ownerConfig.getInt("Shops") - shops.size();
												if(newAmount == 0)
													ownerConfig.set("Shops", null);
												else
													ownerConfig.set("Shops", newAmount);
												ownerConfig.save();
											}
											player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + ChatColor.BOLD + shops.size() + " shops " + ChatColor.RED + "removed!");
											//clear signs
											for(String shopCode : shops) {
												Location location = Shops.decodeCode(shopCode);
												Sign sign = (Sign) location.getBlock().getState();
												for(int line = 0; line <= 3; line++)
													sign.setLine(line, "");
												sign.update();
											}
											//remove region file:
											regionFile.delete();
											File townFolder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + Regions.getOriginalTownName(regionName).toLowerCase());
											if(townFolder.isDirectory() && townFolder.list().length == 0)
												townFolder.delete();
										} else {
											//else transfer to the next highest priority region
											for(String shopCode : shops) {
												Shops.copyShop(regionName, Regions.getHighestPriorityLand(Shops.decodeCode(shopCode)).getId(), shopCode);
											}
										}
									}
									
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Region " + ChatColor.YELLOW + args[2].toLowerCase() + ChatColor.GREEN + " in " + ChatColor.YELLOW + originalTownName + ChatColor.GREEN + " was deleted.");
									
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You cannot unclaim " + ChatColor.BOLD + args[1].toLowerCase() + ChatColor.WHITE + " because the following regions are within this region (with a higher priority):");
									for(ProtectedRegion r : regionManager.getApplicableRegions(region).getRegions())
										if(!r.getId().equalsIgnoreCase(regionName) && r.getPriority() > region.getPriority())
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + " - " + ChatColor.RED + Regions.getRegionID(r.getId()));
									player.sendMessage("");
								}
								
							} else
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + originalTownName + " doesn't own a region named " + ChatColor.YELLOW + args[2].toLowerCase() + ".");
							
							
						} else
							player.sendMessage(ChatColor.RED + "[DeadMC] '" + ChatColor.RED + args[1] + "' is not an existing town.");
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("delete_public_tp")) {
						if(Travel.publicNameIsTaken(args[1])) {
							String nameCased = Travel.publicNameCased(args[1]);
							if(nameCased != null) {
								
								PublicTP tpConfig = PublicTP.getConfig(nameCased);
								UUID ownerUUID = UUID.fromString(tpConfig.getString("Owner"));
								
								PlayerConfig ownerConfig = PlayerConfig.getConfig(ownerUUID);
								List<String> publicTravelPoints = ownerConfig.getStringList("Travel.Public");
								
								//remove from player file
								publicTravelPoints.remove(nameCased);
								ownerConfig.set("Travel.Public", publicTravelPoints);
								ownerConfig.save();
								
								//remove from global list:
								List<String> publicPoints = DeadMC.TravelFile.data().getStringList("Public");
								publicPoints.remove(nameCased);
								DeadMC.TravelFile.data().set("Public", publicPoints);
								DeadMC.TravelFile.save();
								
								//remove public point file:
								File publicFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "travel" + File.separator + nameCased + ".yml");
								publicFile.delete();
								
								if(sender instanceof Player) {
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "'" + nameCased + "' has been deleted.");
								} else {
									Chat.broadcastDiscord(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "'" + nameCased + "' has been deleted.", true);
								}
							}
						} else {
							if(sender instanceof Player) {
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " is not an existing point.");
							} else {
								Chat.broadcastDiscord(ChatColor.RED + "[DeadMC] " + ChatColor.RED + args[1] + " is not an existing point.", true);
							}
						}
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("setmayor")) {
						UUID newMayorUUID = Bukkit.getOfflinePlayer(args[2]).getUniqueId();
						PlayerConfig newMayorConfig = PlayerConfig.getConfig(newMayorUUID);
						String originalTownName = Town.getOriginalTownName(args[1]);
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						String oldMayorUUID = townConfig.getString("Mayor");
						
						if(newMayorConfig.getString("Town") != null && !(newMayorConfig.getString("Town").equalsIgnoreCase(args[1]))) {
							//kick from current town first
							sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Kicking player from old town first...");
							if(Town.Rank.values()[newMayorConfig.getInt("TownRank")] != Town.Rank.Mayor) {
								//just 'a member' in the town
								Town.removeMember(newMayorUUID);
								sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + " - Removed from town.");
							} else if(Town.Rank.values()[newMayorConfig.getInt("TownRank")] == Town.Rank.Mayor
									&& townConfig.getStringList("Members").size() == 1) {
								
								//last member in town, disband
								Town.disband(newMayorConfig.getString("Town"));
								sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + " - Disbanded town.");
							}
						}
						if(newMayorConfig.getString("Town") == null) {
							//add player to town
							sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Adding player to new town first...");
							Town.addMember(newMayorUUID, originalTownName);
							sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + " - Added player to the town.");
						}
						
						//set as mayor
						sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Setting " + Bukkit.getOfflinePlayer(newMayorUUID).getName() + " as mayor of " + originalTownName + "...");
						Town.setRank(newMayorUUID, Town.Rank.Mayor);
						sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + " - Set as mayor");
						
						//transfer regions
						sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Transferring regions to new mayor");
						
						for(String regionID : townConfig.getStringList("Regions")) {
							String regionName = originalTownName + "/" + regionID;
							RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
							ProtectedRegion region = regionManager.getRegion(regionName);
							
							if(region.getOwners().getUniqueIds().contains(UUID.fromString(oldMayorUUID))) {
								Bukkit.getScheduler().runTask(plugin, () -> {
									//remove membership OR ownership
									Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "region removeowner -w world " + region.getId() + " " + oldMayorUUID);
									//set mayor as new owner:
									Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "region addowner -w world " + region.getId() + " " + newMayorUUID.toString());
								});
							}
						}
						
						sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Done!");
						
						return true;
					}
					
					
					//dmc sethelper <player> (true)
					if(args[0].equalsIgnoreCase("sethelper")) {
						PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
						if(args.length == 2) {
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set tab.group.helper false");
							othersConfig.set("Helper", null);
							othersConfig.save();
						}
						if(args.length == 3) {
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set tab.group.helper true");
							othersConfig.set("Helper", true);
							othersConfig.save();
						}
						
						return true;
					}
					
					if(args[0].equalsIgnoreCase("setstaff")) {
						PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
						PlayerConfig staffConfig = sender instanceof Player ? PlayerConfig.getConfig(player) : null;
						if(args.length == 2) {
							if(sender instanceof Player && othersConfig.getInt("StaffRank") >= staffConfig.getInt("StaffRank") && !player.isOp()) {
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "Cannot demote players who have a higher or equal rank to you.");
								return true;
							}
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set aac.alerts false");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set aac.status false");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set aac.check false");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set tab.staff false");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set tab.group." + StaffRank.values()[othersConfig.getInt("StaffRank")].toString() + " false");
							othersConfig.set("StaffRank", null);
							sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Removed " + args[1] + " as staff.");
						} else {
							if(sender instanceof Player && StaffRank.valueOf(args[2]).ordinal() >= staffConfig.getInt("StaffRank") && !player.isOp()) {
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "You can only promote players to the rank below yours.");
								return true;
							}
							
							//remove helper if exists
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set tab.group.helper false");
							othersConfig.set("Helper", null);
							
							if(othersConfig.getString("StaffRank") != null)
								Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set tab.group." + StaffRank.values()[othersConfig.getInt("StaffRank")].toString() + " false");
							othersConfig.set("StaffRank", StaffRank.valueOf(args[2]).ordinal());
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set aac.alerts true");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set aac.status true");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set aac.check true");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set tab.staff true");
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "perm player " + args[1] + " set tab.group." + StaffRank.valueOf(args[2]).toString().toLowerCase() + " true");
							sender.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Set " + args[1] + " as " + StaffRank.valueOf(args[2]));
						}
						othersConfig.save();
						
						return true;
					}
			
			
			if(!(sender instanceof Player) || player.isOp()) {
				
				if(args[0].equalsIgnoreCase("cpu")) {
					int zombiesAroundPlayer = 0;
					for(Entity nearbyEntity : player.getLocation().getWorld().getNearbyEntities(player.getLocation(), 80, 80, 80)) {
						if(Entities.isZombie(nearbyEntity)) {
							zombiesAroundPlayer++;
						}
					}
					int zombiesInWorld = 0;
					int otherEntities = 0;
					HashMap<EntityType, Integer> types = new HashMap<EntityType, Integer>();
					for(Entity e : Bukkit.getWorld("world").getEntities()) {
						if(Entities.isZombie(e)) {
							zombiesInWorld++;
							Monster zombie = (Monster) e;
							Chat.debug("Zombie @ " + e.getLocation().getX() + ", " + e.getLocation().getY() + ", " + e.getLocation().getZ() + " - " + (zombie.getTarget() != null));
						}
						else if(e instanceof LivingEntity) {
							otherEntities++;
						}
						
						//types:
						if(types.containsKey(e.getType())) {
							types.replace(e.getType(), types.get(e.getType()) + 1);
						} else {
							types.put(e.getType(), 1);
						}
						
					}
					otherEntities -= Bukkit.getOnlinePlayers().size();
					player.sendMessage("Around you: " + zombiesAroundPlayer + " / " + zombiesInWorld + " zombies in the world (+" + otherEntities + " other entities)");
					for(EntityType type : EntityType.values()) {
						if(types.containsKey(type) && !Entities.isZombie(type) && type != EntityType.PLAYER) {
							player.sendMessage(type.toString() + ": " + types.get(type));
						}
					}
					
					return true;
					
				}
				
				if(args[0].equalsIgnoreCase("complete")) {
					TaskManager.completeTask(player.getUniqueId());
					return true;
				}
				
				if(args[0].equalsIgnoreCase("streak")) {
					player.sendMessage("A streak of " + args[1] + " gives " + TaskManager.GetStreakMultiplier(Integer.parseInt(args[1])) + "x ");
					return true;
				}
				
				if(args[0].equalsIgnoreCase("marketloc")) {
					DeadMC.ShopsFile.data().set("MarketLocation", player.getLocation());
					DeadMC.ShopsFile.save();
					return true;
				}
				
				if(args[0].equalsIgnoreCase("nodamage")) {
					if(nodamage.contains(player.getName())) {
						player.sendMessage("Receiving damage.");
						nodamage.remove(player.getName());
					} else {
						player.sendMessage("Invincible to damage.");
						nodamage.add(player.getName());
					}
					return true;
				}
				
				if(args[0].equalsIgnoreCase("checktodoublekeys")) {
					Player finalPlayer2 = player;
					Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
						OfflinePlayer playerToGive = Bukkit.getOfflinePlayer(args[1]);
						UUID uuid = playerToGive.getUniqueId();
						
						if(!DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString()))
							return;
						
						PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
						
						//check it has been more than 20 hours since last double up
						if(playerConfig.getString("TimeSinceVoteDoubleKeys") != null) {
							DecimalFormat df = new DecimalFormat("####0.00");
							double timeSinceLastDoubleKeys = (System.currentTimeMillis() - playerConfig.getLong("TimeSinceVoteDoubleKeys")) / 1000.0; //convert to seconds
							double timeInHours = timeSinceLastDoubleKeys / 60.0 / 60.0;
							if(timeInHours < 20) return;
						}
						
						//update
						playerConfig.set("TimeSinceVoteDoubleKeys", System.currentTimeMillis());
						playerConfig.save();
						
						if(finalPlayer2 != null) {
							finalPlayer2.sendMessage(ChatColor.YELLOW + "[" + ChatColor.GOLD + "/vote" + ChatColor.YELLOW + "] " + ChatColor.GREEN + "Thanks for voting on all the sites today! We have doubled your voting keys.");
						}
						
						//give player 3 voting keys
						Bukkit.getScheduler().runTask(plugin, () -> {
							for(int count = 0; count < 3; count++) {
								CustomItems.giveItemSafely(uuid, CustomItems.votingKey());
							}
						});
					});
					return true;
				}
				
				if(args[0].equalsIgnoreCase("addvote")) {
					Chat.debug("1");
					Stats.Add(Stat.VOTES);
					
					Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
						Chat.debug("2");
						OfflinePlayer playerToGive = Bukkit.getOfflinePlayer(args[1]);
						UUID uuid = playerToGive.getUniqueId();
						Chat.debug("3");
						if(!DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString()))
							return;
						Chat.debug("4");
						//give player voting key
						CustomItems.giveItemSafely(uuid, CustomItems.votingKey());
						Player onlinePlayer = Bukkit.getPlayer(uuid);
						if(onlinePlayer != null) onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_CHIME, 1, 1f);
						Chat.debug("5");
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								Chat.updateLeaderBoard(Leaderboard.VOTES, uuid);
								
								//reset if new month:
								
								String topvotervotes = "%VotingPlugin_GlobalMonthTotal%";
								topvotervotes = PlaceholderAPI.setPlaceholders(playerToGive, topvotervotes);
								int votes = topvotervotes == null || topvotervotes.equalsIgnoreCase("") ? 0 : Integer.parseInt(topvotervotes);
								
								int leaderboardVotes = DeadMC.LeaderboardFile.data().getInt(Leaderboard.VOTES_MONTH.toString() + "." + (int) 1 + ".Amount");
								
								if(votes < leaderboardVotes) {
									for(int count = 1; count <= 10; count++) {
										DeadMC.LeaderboardFile.data().set(Leaderboard.VOTES_MONTH.toString() + "." + count + ".Amount", 0);
										DeadMC.LeaderboardFile.data().set(Leaderboard.VOTES_MONTH.toString() + "." + count + ".UUID", null);
									}
									DeadMC.LeaderboardFile.save();
								}
								//end reset
								
								Chat.updateLeaderBoard(Leaderboard.VOTES_MONTH, uuid);
							}
						}, 60L); //wait 3 seconds to update leaderboard for vote to register
						
					});
					return true;
				}
				
				if(args[0].equalsIgnoreCase("ftime")) {
					//DEBUG
					Bukkit.getWorld("world").setFullTime(Integer.parseInt(args[1]));
					return true;
				}
				
				if(args[0].equalsIgnoreCase("spawning")) {
					Boolean spawning = DeadMC.DonatorFile.data().getString("Spawning") == null || DeadMC.DonatorFile.data().getBoolean("Spawning") ? true : false;
					DeadMC.DonatorFile.data().set("Spawning", !spawning);
					DeadMC.DonatorFile.save();
					Chat.broadcastMessage("Turned spawning " + (!spawning ? "on" : "off"));
					return true;
				}
				
				if(args[0].equalsIgnoreCase("kit")) {
					PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
					if(args.length == 3) {
						String kitName = args[2];
						if(args[1].equalsIgnoreCase("create")) {
							DeadMC.DonatorFile.data().set("Kits." + kitName, new ArrayList<ItemStack>());
						}
						if(args[1].equalsIgnoreCase("add")) {
							if(DeadMC.DonatorFile.data().getString("Kits." + kitName) != null) {
								List<ItemStack> itemsInKit = (List<ItemStack>) DeadMC.DonatorFile.data().getList("Kits." + kitName);
								itemsInKit.add(player.getInventory().getItem(player.getInventory().getHeldItemSlot()));
								DeadMC.DonatorFile.data().getList("Kits." + kitName, itemsInKit);
							} else player.sendMessage(kitName + " does not exist.");
						}
						DeadMC.DonatorFile.save();
					} else player.sendMessage("/dmc kit create/add <kit name>");
				}
				
				if(args[0].equalsIgnoreCase("mute")) {
					PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
					ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
					
					othersConfig.set("Muted.Start", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
					othersConfig.set("Muted.Time", Integer.parseInt(args[2])); //time in minutes
					othersConfig.set("Muted.Reason", Integer.parseInt(args[3]));
					othersConfig.save();
				}
				
				if(args[0].equalsIgnoreCase("givecoins")) {
					if(args[2].equalsIgnoreCase("random")) {
						//coins are from 0-150
						int coins = (int) (Math.random() * 240) + 10;
						Economy.giveCoins(Bukkit.getOfflinePlayer(args[1]).getUniqueId(), coins); // 0-250 (inclusive)
						Bukkit.getPlayer(args[1]).sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.GREEN + "You got " + ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.GREEN + "!");
					} else
						Economy.giveCoins(Bukkit.getOfflinePlayer(args[1]).getUniqueId(), Integer.parseInt(args[2]));
				}
				
				if(args[0].equalsIgnoreCase("votereward")) {
					Player playerToGive = Bukkit.getPlayer(args[1]);
					
					if(args[2].equalsIgnoreCase("2")) {
						int index = (int) (Math.random() * DeadMC.veryRareItems.size()); // 0-size (inclusive)
						ItemStack item = DeadMC.veryRareItems.get(index).clone();
						playerToGive.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.GREEN + "You got " + ChatColor.BOLD + item.getAmount() + ChatColor.GREEN + "x " + ChatColor.BOLD + item.getType().toString().toLowerCase().replace("_", " ") + ChatColor.GREEN + "!");
						//add stored (book) enchants:
						if(item.getItemMeta() instanceof EnchantmentStorageMeta) {
							for(Enchantment enchantment : ((EnchantmentStorageMeta) item.getItemMeta()).getStoredEnchants().keySet()) {
								String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
								playerToGive.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.WHITE + " - " + ChatColor.GREEN + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) item.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
							}
						}
						
						if(playerToGive != null) playerToGive.playSound(playerToGive.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1, 1f);
						playerToGive.getInventory().addItem(item);
						playerToGive.updateInventory();
					}
					if(args[2].equalsIgnoreCase("1")) {
						int index = (int) (Math.random() * DeadMC.rareItems.size()); // 0-size (inclusive)
						ItemStack item = DeadMC.rareItems.get(index).clone();
						playerToGive.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.GREEN + "You got " + ChatColor.BOLD + item.getAmount() + ChatColor.GREEN + "x " + ChatColor.BOLD + item.getType().toString().toLowerCase().replace("_", " ") + ChatColor.GREEN + "!");
						//add stored (book) enchants:
						if(item.getItemMeta() instanceof EnchantmentStorageMeta) {
							for(Enchantment enchantment : ((EnchantmentStorageMeta) item.getItemMeta()).getStoredEnchants().keySet()) {
								String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
								playerToGive.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.WHITE + " - " + ChatColor.GREEN + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) item.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
							}
						}
						playerToGive.getInventory().addItem(item);
						playerToGive.updateInventory();
					}
				}
				
				//dmc donatespecialitem {uuid} {item} {price}
				if(args[0].equalsIgnoreCase("donatespecialitem")) {
					UUID uuid = UUID.fromString(args[1]);
					int price = (int) Double.parseDouble(args[3].substring(1));
					//Stats.Add(Stat.SALES, price);
					
					String item = args[2];
					if(item.equalsIgnoreCase("ClimbingBoots")) {
						CustomItems.giveItemSafely(uuid, CustomItems.climbingBoots(true));
					}
					if(item.equalsIgnoreCase("QuickBowMulti")) {
						CustomItems.giveItemSafely(uuid, CustomItems.quickBow(CustomItems.QuickBowType.MULTI, true, 0));
					}
					if(item.equalsIgnoreCase("QuickBowSingle")) {
						CustomItems.giveItemSafely(uuid, CustomItems.quickBow(CustomItems.QuickBowType.SINGLE, true, 0));
					}
					if(item.equalsIgnoreCase("DestructorArrow")) {
						CustomItems.giveItemSafely(uuid, CustomItems.destructorArrow(true));
					}
					
					if(item.equalsIgnoreCase("Backpack")) {
						CustomItems.giveItemSafely(uuid, CustomItems.backpack());
					}
					
					if(item.equalsIgnoreCase("Quiver")) {
						CustomItems.giveItemSafely(uuid, CustomItems.createNewQuiver(CustomItems.QuiverTier.Donator));
					}
					
					if(!args[4].equalsIgnoreCase("null")) {
						
						Chat.broadcastMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "A MASSIVE THANK YOU to " + ChatColor.BOLD + Bukkit.getOfflinePlayer(uuid).getName() + ChatColor.WHITE + " for their support! Enjoy your new item :)");
						
						if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
							if(Bukkit.getPlayer(uuid) != null) {
								Bukkit.getPlayer(uuid).sendMessage("");
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "A MASSIVE THANK YOU - for supporting DeadMC and helping us grow bigger and better!");
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "We care a lot about you and your player experience. Please let us know if there is anything we can do to make it better :D");
								Bukkit.getPlayer(uuid).sendMessage("");
							}
						}
						
						return true;
						
					}
				}
				if(args[0].equalsIgnoreCase("donate")) {
					UUID uuid = UUID.fromString(args[1]);
					PlayerConfig othersConfig = PlayerConfig.getConfig(uuid);
					
					if(args[2].equalsIgnoreCase("10x-CRATE-KEYS")) {
						if(othersConfig.getString("ClaimedFreeCoins") == null) {
							
							//Stats.Add(Stat.WEBSITE_REGISTRATIONS);
							
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "crate givekey " + Bukkit.getOfflinePlayer(uuid).getName() + " mystery 10");
							othersConfig.set("ClaimedFreeCoins", true);
							
							Chat.broadcastMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GOLD + "A BIG THANKS to " + Bukkit.getOfflinePlayer(uuid).getName() + " for joining www.DeadMC.com! Enjoy the reward :)");
							if(Bukkit.getPlayer(uuid) != null) {
								Bukkit.getPlayer(uuid).sendMessage("");
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Thanks for registering on our website!");
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Consider also joining our " + ChatColor.BOLD + "Discord" + ChatColor.WHITE + "; it's the best way to get help and chat with other players :)");
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GOLD + "discord.gg/CMpyUrs");
								Bukkit.getPlayer(uuid).sendMessage("");
							}
							othersConfig.save();
							
						}
					} else {
						
						Chat.broadcastMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "A MASSIVE THANK YOU to " + ChatColor.BOLD + Bukkit.getOfflinePlayer(uuid).getName() + ChatColor.WHITE + " for their support! Enjoy your new rank :)");
						
						if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
							if(Bukkit.getPlayer(uuid) != null) {
								Bukkit.getPlayer(uuid).sendMessage("");
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "A MASSIVE THANK YOU - for supporting DeadMC and helping us grow bigger and better!");
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "We care a lot about you and your player experience. Please let us know if there is anything we can do to make it better :D");
								Bukkit.getPlayer(uuid).sendMessage("");
							}
							
							Bukkit.getServer().dispatchCommand(sender, "setrank " + Bukkit.getOfflinePlayer(uuid).getName() + " " + args[2]);
							othersConfig.set("DonateRank", Donator.getRankID(args[2]));
							othersConfig.save();
							Economy.giveCoins(uuid, DeadMC.DonatorFile.data().getInt(Donator.getRankID(args[2]) + ".Coins"));
						}
					}
				}
				if(args[0].equalsIgnoreCase("refund")) {
					DiscordSRVListener.sendPrivateMessage("464217924593385472", Bukkit.getOfflinePlayer(args[1]).getName() + " (" + Bukkit.getOfflinePlayer(args[1]).getUniqueId().toString() + ") just initiated a chargeback/refund!");
					PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
					Bukkit.getServer().dispatchCommand(sender, "setrank " + args[1] + " citizen");
					othersConfig.set("DonateRank", 0);
					othersConfig.save();
				}
				
				if(args[0].equalsIgnoreCase("tppos")) {
					PaperLib.teleportAsync(player, new Location(player.getWorld(), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3])));
				}
				
				if(args.length > 0) {
					
					//dmc setbuilder <player> (true)
					if(args[0].equalsIgnoreCase("lbhead")) {
						lbhead_type = args[1];
						lbhead_place = Integer.parseInt(args[2]);
						if(args.length == 4) lbhead_title = args[3];
						
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								lbhead_type = null;
								lbhead_title = null;
								lbhead_place = 0;
							}
						}, 300L);
						
						player.sendMessage("Click head to set leaderboard.");
					}
					
					//dmc setbuilder <player> (true)
					if(args[0].equalsIgnoreCase("setbuilder")) {
						PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
						if(args.length == 2) {
							othersConfig.set("Builder", null);
							othersConfig.save();
						}
						if(args.length == 3) {
							othersConfig.set("Builder", true);
							othersConfig.save();
						}
					}
					
					if(args[0].equalsIgnoreCase("setrank")) {
						PlayerConfig othersConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
						Bukkit.getServer().dispatchCommand(sender, "setrank " + args[1] + " " + Rank.values()[Integer.parseInt(args[2])].toString());
						othersConfig.set("DonateRank", Integer.parseInt(args[2]));
						othersConfig.save();
					}
					
					if(args[0].equalsIgnoreCase("loot") || args[0].equalsIgnoreCase("addloot")) {
						LootRarity rarity = LootRarity.values()[Integer.parseInt(args[1])];
						addLoot(rarity, player.getInventory().getItem(player.getInventory().getHeldItemSlot()).clone());
						player.sendMessage("Added.");
					}
					
				} else {
					player.sendMessage("/dmc setrank <player> <rank ID>");
					player.sendMessage("/dmc loot <0-3>(common-very rare)");
				}
				
			} else player.sendMessage("Need permission.");
		}
		
		return true;
	}
	
	private enum HackingLevel {
		POTENTIAL,
		LIKELY,
		HIGHLY_LIKELY,
		ABSOLUTE
	}
	
	private void giveHackingPunishment(UUID playerUUID, HackingLevel level, String detection) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			Player player = Bukkit.getPlayer(playerUUID);
			if(player == null) return; //player logged out
			PlayerConfig playerConfig = PlayerConfig.getConfig(playerUUID);
			String nickName = playerConfig.getString("Name") != null ? playerConfig.getString("Name") : player.getName();
			
			if(level == HackingLevel.POTENTIAL) {
			
			}
			if(level == HackingLevel.LIKELY) {
				Chat.broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "[!]" + ChatColor.WHITE + " " + ChatColor.translateAlternateColorCodes('&', nickName) + ChatColor.GRAY + " is likely using " + ChatColor.GOLD + detection, false, true);
			}
			if(level == HackingLevel.HIGHLY_LIKELY) {
				Chat.broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + "[!]" + ChatColor.WHITE + " " + ChatColor.translateAlternateColorCodes('&', nickName) + ChatColor.GRAY + " is highly likely using " + ChatColor.RED + detection, false, true);
			}
			if(level == HackingLevel.ABSOLUTE) {
				WebhookUtil.deliverMessage(DiscordUtil.getJda().getTextChannelById(JDAListener.warningsChannel), player, Chat.stripColor(nickName), ":detective: **" + Chat.stripColor(nickName) + "** was kicked for using " + detection + ".", (MessageEmbed) null);
				Chat.broadcastMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "[!]" + ChatColor.WHITE + " " + ChatColor.translateAlternateColorCodes('&', nickName) + ChatColor.GRAY + " is absolutely using " + ChatColor.DARK_RED + detection, false, true);
				Bukkit.getScheduler().runTask(plugin, () -> player.kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + "Kicked" + ChatColor.WHITE + " for hacking (" + detection + ").\n\nPlease follow the " + ChatColor.GOLD + "/rules" + ChatColor.WHITE + "."));
			}
			
		});
	}

	public static Punishment getPunishmentFromString(String string) {
		if(Chat.isInteger(string) && Integer.parseInt(string) <= Punishment.values().length && Integer.parseInt(string) >= 0) {
			return Punishment.values()[Integer.parseInt(string)];
		} else {
			try {
				return Punishment.valueOf(string.toUpperCase());
			} catch(IllegalArgumentException e) {
			}
		}
		return null; //return null if not a difficulty
	}
	
	@SuppressWarnings("unchecked")
	public void addLoot(LootRarity rarity, ItemStack itemstack) {
		List<ItemStack> items = new ArrayList<ItemStack>();
		if(DeadMC.LootFile.data().get(rarity.toString()) != null)
			items = (List<ItemStack>) DeadMC.LootFile.data().getList(rarity.toString());
		
		items.add(itemstack);
		DeadMC.LootFile.data().set(rarity.toString(), items);
		DeadMC.LootFile.save();
	}
	
	//remove data that wasn't cleaned properly:
	public static void RemoveNullData() {
		long nullFiles = 0;
		Restarter.writeToLog("Checking for null towns...");
		//towns:
		List<String> activeTowns = DeadMC.TownFile.data().getStringList("Active");
		// loop through all the town files in the towns folder
		File townsDirectory = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "towns");
		File[] townFiles = townsDirectory.listFiles();
		for(File townFile : townFiles) {
			if(!activeTowns.contains(townFile.getName().replace(".yml", ""))) {
				townFile.delete();
				nullFiles++;
				Restarter.writeToLog(" - Deleting " + townFile.getName() + " (doesn't exist in the town file)");
			}
		}
		
		Restarter.writeToLog("Checking for null players...");
		//players:
		List<String> activePlayers = DeadMC.PlayerFile.data().getStringList("Active");
		// loop through all the town files in the towns folder
		File playerDirectory = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "players");
		File[] playerFiles = playerDirectory.listFiles();
		for(File playerFile : playerFiles) {
			if(!activePlayers.contains(playerFile.getName().replace(".yml", ""))) {
				playerFile.delete();
				nullFiles++;
				Restarter.writeToLog(" - Deleting " + playerFile.getName() + " (doesn't exist in the players file)");
			}
		}
		List<String> updatedActivePlayers = new ArrayList<>(activePlayers);
		for(String uuidString : activePlayers) {
			UUID uuid = UUID.fromString(uuidString);
			File playerFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "players" + File.separator + uuidString + ".yml");
			if(!playerFile.exists()) {
				updatedActivePlayers.remove(uuidString);
				OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
				Chat.broadcastDiscord("Removing **" + player.getName() + "** (" + uuidString + ") from active players (PlayerConfig is missing).", true);
			} else {
				PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
				if(playerConfig.getString("DonateRank") == null) {
					updatedActivePlayers.remove(uuidString);
					OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
					Chat.broadcastDiscord("Removing **" + player.getName() + "** (" + uuidString + ") from active players (PlayerConfig is corrupt).", true);
				}
			}
		}
		
		//ips:
		Restarter.writeToLog("Checking for null IPs...");
		List<String> activeIPs = DeadMC.IpsFile.data().getStringList("Active");
		// loop through all the town files in the towns folder
		File ipDirectory = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "ips");
		File[] ipFiles = ipDirectory.listFiles();
		for(File ipFile : ipFiles) {
			if(!activeIPs.contains(ipFile.getName().replace(".yml", ""))) {
				ipFile.delete();
				nullFiles++;
				Restarter.writeToLog(" - Deleting " + ipFile.getName() + " (doesn't exist in the ips file)");
			}
		}
		
		//check for missed files in world folder
		// loop each file in playerdata
		File datDirectory = new File(Bukkit.getWorldContainer(), "world" + File.separator + "playerdata");
		File[] datFiles = datDirectory.listFiles();
		for(File datFile : datFiles) {
			if(updatedActivePlayers.contains(datFile.getName().replace(".dat", "").replace("_old", ""))) continue;
			datFile.delete();
			nullFiles++;
		}
		// loop each file in stats
		File statsDirectory = new File(Bukkit.getWorldContainer(), "world" + File.separator + "stats");
		File[] statFiles = statsDirectory.listFiles();
		for(File statFile : statFiles) {
			if(updatedActivePlayers.contains(statFile.getName().replace(".json", ""))) continue;
			statFile.delete();
			nullFiles++;
		}
		
		if(nullFiles > 0)
			Bukkit.getConsoleSender().sendMessage("[Data-Clean] " + ChatColor.RED + "" + nullFiles + " null files (town, player & ip) removed.");
		Restarter.writeToLog("Removed " + nullFiles + " null files");
		DeadMC.RestartFile.data().set("DataCleaner.Previous.NullFiles", nullFiles);
		DeadMC.RestartFile.save();
	}
	
	//remove town regions that don't have an owner
	public static void RemoveNullRegions() {
		Restarter.writeToLog("Checking for null regions...");
		
		//get list of WG regions
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
		Map<String, ProtectedRegion> wgRegionsWithID = regionManager.getRegions(); //region name, protected region
		List<ProtectedRegion> protectedRegions = new ArrayList<ProtectedRegion>();
		wgRegionsWithID.forEach((regionID, region) -> {
			protectedRegions.add(region);
		});
		
		//loop each protected region
		int nullRegions = 0;
		for(ProtectedRegion wgRegion : protectedRegions) {
			if(wgRegion.getPriority() != 0 && wgRegion.getId().contains("/")
					&& Regions.getOriginalTownName(wgRegion.getId()).equalsIgnoreCase("UNDEFINED")) {
				Restarter.writeToLog(" - Deleting " + wgRegion.getId() + " (the town doesn't exist anymore)");
				regionManager.removeRegion(wgRegion.getId());
				nullRegions++;
			}
		}
		
		if(nullRegions > 0)
			Bukkit.getConsoleSender().sendMessage("[Data-Clean] " + ChatColor.RED + "" + nullRegions + " regions removed with null town.");
		Restarter.writeToLog("Removed " + nullRegions + " null regions");
		DeadMC.RestartFile.data().set("DataCleaner.Previous.NullRegions", nullRegions);
		DeadMC.RestartFile.save();
	}
	
	public static void ClearOldChunks() {
		Restarter.writeToLog("Clearing unused chunks...");
		
		//keep list of SAFE regions
		List<RegionData> safe = new ArrayList<RegionData>();
		List<String> safeWithID = new ArrayList<String>();
		
		//get list of WG regions
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
		Map<String, ProtectedRegion> wgRegionsWithID = regionManager.getRegions(); //region name, protected region
		List<ProtectedRegion> protectedRegions = new ArrayList<ProtectedRegion>();
		wgRegionsWithID.forEach((regionID, region) -> {
			//only include level 0 or 1 regions (dont need to check higher)
			if(region.getPriority() <= 1)
				protectedRegions.add(region);
		});
		
		//loop each protected region
		for(ProtectedRegion wgRegion : protectedRegions) {
			//get the min and max corners
			
			List<BlockVector2> corners = wgRegion.getPoints();
			BlockVector2 minCorner = null;
			BlockVector2 maxCorner = null;
			for(BlockVector2 corner : corners) {
				int size = corner.getX() + corner.getZ();
				if(maxCorner == null || size > (maxCorner.getX() + maxCorner.getZ()))
					maxCorner = corner;
				if(minCorner == null || size < (minCorner.getX() + minCorner.getZ()))
					minCorner = corner;
			}

			//convert to chunk data
			int minChunkX = (int) Math.floor(minCorner.getX() / 16.0);
			int minChunkZ = (int) Math.floor(minCorner.getZ() / 16.0);
			int maxChunkX = (int) Math.floor(maxCorner.getX() / 16.0);
			int maxChunkZ = (int) Math.floor(maxCorner.getZ() / 16.0);
			
			//convert to region data
			int minRegionX = (int) Math.floor(minChunkX / 32.0);
			int minRegionZ = (int) Math.floor(minChunkZ / 32.0);
			int maxRegionX = (int) Math.floor(maxChunkX / 32.0);
			int maxRegionZ = (int) Math.floor(maxChunkZ / 32.0);
			
			RegionData minRegion = new RegionData(minRegionX, minRegionZ);
			RegionData maxRegion = new RegionData(maxRegionX, maxRegionZ);
			
			//add all the regions between
			for(int x = minRegion.x; x <= maxRegion.x; x++) {
				for(int z = minRegion.z; z <= maxRegion.z; z++) {
					//do not add if already in list
					String regionID = "r." + x + "." + z + ".mca";
					if(!safeWithID.contains(regionID)) {
						safe.add(new RegionData(x, z));
						safeWithID.add(regionID);
					}
				}
			}
		}
		
		int notSafe = 0;
		long savedStorage = 0;
		long wastedStorage = 0;
		long oneDay = 86400000L;
		ZonedDateTime computerTime = ZonedDateTime.now(); //to adelaide time
		
		//now loop through all the region files in the world folder
		File regionDirectory = new File(Bukkit.getWorld("world").getWorldFolder() + File.separator + "region");
		File[] regionFiles = regionDirectory.listFiles();
		for(File regionFile : regionFiles) {
			if(safeWithID.contains(regionFile.getName())) {
				savedStorage += regionFile.length();
				continue;
			}
			
			long timePassed = (computerTime.toEpochSecond() * 1000) - regionFile.lastModified();
			if(timePassed < (oneDay * 7)) {
				savedStorage += regionFile.length();
				continue;
			}
			
			notSafe++;
			wastedStorage += regionFile.length();
			
			Restarter.writeToLog(" - Removing " + regionFile.getName() + " (no regions within area, and " + (timePassed/oneDay) + " since last seen)");
			
			regionFile.delete();
			
			File poiFile = new File(Bukkit.getWorld("world").getWorldFolder() + File.separator + "poi" + File.separator + regionFile.getName());
			if(poiFile.exists()) poiFile.delete();
		}
		
		//results here
		
		DecimalFormat df = new DecimalFormat("####0.00");
		double safeSize = savedStorage / 1073741824.0; //convert to GB
		double unsafeSize = wastedStorage / 1073741824.0; //convert to GB
		
		Restarter.writeToLog("Removed " + notSafe + " old chunk regions.");
		
		Bukkit.getConsoleSender().sendMessage("");
		Bukkit.getConsoleSender().sendMessage("[Data-Clean] " + "Safe region files: " + safe.size() + " (" + df.format(safeSize) + "gb)");
		Bukkit.getConsoleSender().sendMessage("[Data-Clean] " + "Removed: " + ChatColor.GREEN + notSafe + ChatColor.WHITE + " (" + ChatColor.GREEN + df.format(unsafeSize) + "gb" + ChatColor.WHITE + ")");
		Bukkit.getConsoleSender().sendMessage("");
		
		DeadMC.RestartFile.data().set("DataCleaner.Previous.Chunks", notSafe + " (" + df.format(unsafeSize) + "gb)");
		DeadMC.RestartFile.save();
	}
	
	private void copyPlayerWorldData(String world, UUID oldUUID, UUID newUUID) {
		File oldStatFile = new File(Bukkit.getWorldContainer(), world + File.separator + "stats" + File.separator + oldUUID.toString() + ".json");
		File newStatFile = new File(Bukkit.getWorldContainer(), world + File.separator + "stats" + File.separator + newUUID.toString() + ".json");
		try {
			Files.move(oldStatFile.toPath(), newStatFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch(IOException e) {
			Chat.debug("Error moving stat file! " + e.getCause() + " " + e.getMessage());
		}
		
		File oldPlayerFile = new File(Bukkit.getWorldContainer(), world + File.separator + "playerdata" + File.separator + oldUUID.toString() + ".dat");
		File newPlayerFile = new File(Bukkit.getWorldContainer(), world + File.separator + "playerdata" + File.separator + newUUID.toString() + ".dat");
		try {
			Files.move(oldPlayerFile.toPath(), newPlayerFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch(IOException e) {
			Chat.debug("Error moving player file! " + e.getCause() + " " + e.getMessage());
		}
		
		File oldPlayerOldFile = new File(Bukkit.getWorldContainer(), world + File.separator + "playerdata" + File.separator + oldUUID.toString() + ".dat_old");
		File newPlayerOldFile = new File(Bukkit.getWorldContainer(), world + File.separator + "playerdata" + File.separator + newUUID.toString() + ".dat_old");
		try {
			Files.move(oldPlayerOldFile.toPath(), newPlayerOldFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch(IOException e) {
			Chat.debug("Error moving player old file! " + e.getCause() + " " + e.getMessage());
		}
	}
}

class RegionData {
	int x;
	int z;
	
	RegionData(int x, int z) {
		this.x = x;
		this.z = z;
	}
}
