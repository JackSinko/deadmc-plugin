package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Towns.Town;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.Permission;
import github.scarsz.discordsrv.dependencies.jda.api.entities.*;
import github.scarsz.discordsrv.dependencies.jda.api.events.guild.GuildUnavailableEvent;
import github.scarsz.discordsrv.dependencies.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import github.scarsz.discordsrv.dependencies.jda.api.events.interaction.SlashCommandEvent;
import github.scarsz.discordsrv.dependencies.jda.api.hooks.ListenerAdapter;
import github.scarsz.discordsrv.dependencies.jda.api.interactions.InteractionHook;
import github.scarsz.discordsrv.util.DiscordUtil;
import github.scarsz.discordsrv.util.WebhookUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class JDAListener extends ListenerAdapter {
	
	private final DeadMC plugin;
	
	public JDAListener(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static String staffLogChannel = "737090821601493073"; //punishment-logs
	public static String warningsChannel = "879235312038084650"; //warnings
	//public static String staffLogChannel = "861986694983319552"; //dev channel
	
	@Override
	public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
		VoiceChannel channel = event.getChannelLeft();
		//check if is a town voice chat
		if(channel.getName().contains("-town-voice")) {
			//check if there are 0 members left
			if(channel.getMembers().size() == 0) {
				//remove voice channel
				channel.delete().queue();
				String townDisplayName = Town.townNameCased(channel.getName().replace("-town-voice", ""));
				String originalTownName = Town.getOriginalTownName(townDisplayName);
				Town.sendMessage(originalTownName, "The voice channel has closed.");
			}
		}
		if(channel.getName().contains("-nation-voice")) {
			//check if there are 0 members left
			if(channel.getMembers().size() == 0) {
				//remove voice channel
				channel.delete().queue();
				String nationName = Nation.getNameCased(channel.getName().replace("-nation-voice", ""));
				Nation.sendMessage(nationName, "The voice channel has closed.");
			}
		}
	}
	
	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		Chat.debug("Slash command received!");
		
		String discordID = event.getMember().getId();

		if(event.getName().equalsIgnoreCase("punish")) {
			UUID uuid = DiscordSRV.getPlugin().getAccountLinkManager().getUuid(discordID);
			punishCommand(event, uuid, event.getOption("player").getAsString(), event.getOption("punishment").getAsString(), event.getOption("time") == null ? "permanent" : event.getOption("time").getAsString(), event.getOption("comments").getAsString());
		}
		
		if(event.getName().equalsIgnoreCase("unpunish")) {
			UUID uuid = DiscordSRV.getPlugin().getAccountLinkManager().getUuid(discordID);
			unpunishCommand(event, uuid, event.getOption("punish_id").getAsString(), event.getOption("reason").getAsString());
		}
		
		if(event.getName().equalsIgnoreCase("voice")) {
			UUID uuid = DiscordSRV.getPlugin().getAccountLinkManager().getUuid(discordID);
			voiceCommand(event, uuid, event.getChannel());
		}
		
		if(event.getName().equalsIgnoreCase("vanish")) {
			UUID uuid = DiscordSRV.getPlugin().getAccountLinkManager().getUuid(discordID);
			vanishCommand(event, uuid);
		}
	}
	
	public void vanishCommand(SlashCommandEvent event, UUID staffUUID) {
		Chat.debug("Vanish command executed!");
		event.deferReply(true).queue(); // Let the user know we received the command before doing anything else
		InteractionHook hook = event.getHook(); // This is a special webhook that allows you to send messages without having permissions in the channel and also allows ephemeral messages
		hook.setEphemeral(true); // All messages here will now be ephemeral implicitly
		
		if(staffUUID == null) {
			hook.sendMessage("You haven't linked your Discord account. Use **/discord link** in game to link.").queue();
			return; //user doesn't have account linked
		}
		
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(staffUUID);
		PlayerConfig playerConfig = PlayerConfig.getConfig(staffUUID);
		
		if(!offlinePlayer.isOp() && playerConfig.getString("StaffRank") == null) {
			hook.sendMessage("This is a staff command.").queue();
			return;
		}
		
		Player player = Bukkit.getPlayer(staffUUID);
		if(player != null) {
			hook.sendMessage("You are **" + (Admin.vanished.contains(player.getName()) ? "no longer" : "now") + "** vanished.").queue();
			Bukkit.dispatchCommand(player, "vanish");
		} else {
			if(Admin.vanished.contains(offlinePlayer.getName())) {
				hook.sendMessage("You are **no longer** vanished.").queue();
				Admin.vanished.remove(offlinePlayer.getName());
			} else {
				hook.sendMessage("You are **now** vanished.").queue();
				Admin.vanished.add(offlinePlayer.getName());
			}
		}
	}
	
	public void voiceCommand(SlashCommandEvent event, UUID playerUUID, MessageChannel channel) {
		Chat.debug("Voice command executed!");
		event.deferReply(true).queue(); // Let the user know we received the command before doing anything else
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		InteractionHook hook = event.getHook(); // This is a special webhook that allows you to send messages without having permissions in the channel and also allows ephemeral messages
		hook.setEphemeral(true); // All messages here will now be ephemeral implicitly
		
		if(playerUUID == null) {
			hook.sendMessage("You haven't linked your Discord account. Use **/discord link** in game to link.").queue();
			return; //user doesn't have account linked
		}
		
		//check if player is in a town chat
		if(channel.getName().contains("-town-chat")) {
			//player used it in town chat
			//guarentees player is in a town with a chat
			PlayerConfig playerConfig = PlayerConfig.getConfig(playerUUID);
			String originalTownName = playerConfig.getString("Town");
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			String townDisplayName = Town.getTownDisplayName(originalTownName);

			//check if a voice channel already exists:
			if(guild.getVoiceChannelsByName(townDisplayName.toLowerCase() + "-town-voice", false).size() > 0) {
				hook.sendMessage(townDisplayName + " already has a voice channel open.").queue();
				return;
			}
			
			String townRoleName = "Member of " + townDisplayName;
			Role townRole = guild.getRolesByName(townRoleName, false).get(0);
			
			long viewPermission = Permission.VIEW_CHANNEL.getRawValue();
			guild.createVoiceChannel(townDisplayName.toLowerCase() + "-town-voice", guild.getCategoryById("859961704456257546")) //assign to the 'Minecraft' category
					.addRolePermissionOverride(guild.getPublicRole().getIdLong(), 0, viewPermission) //deny viewing
					.addRolePermissionOverride(townRole.getIdLong(), viewPermission, 0).queue(); //allow viewing
			
			hook.sendMessage("You created the **" + townDisplayName.toLowerCase() + "-town-voice** channel!\nThe channel will be removed when there are no users left in the channel.").queue();
			
			String playerName = playerConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(playerUUID).getName() : Chat.stripColor(playerConfig.getString("Name"));
			Town.sendMessage(originalTownName, playerName + " has opened the " + townDisplayName + " voice channel.");
			
			//check if no users in after 5 minutes and remove
			Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
				if(guild.getVoiceChannelsByName(townDisplayName.toLowerCase() + "-town-voice", false).size() > 0) {
					VoiceChannel voiceChannel = guild.getVoiceChannelsByName(townDisplayName.toLowerCase() + "-town-voice", false).get(0);
					if(voiceChannel.getMembers().size() == 0) {
						voiceChannel.delete().queue();
						Town.sendMessage(originalTownName, "The voice channel has closed.");
					}
				}
			}, 6000L);
			
			return;
		} else if(channel.getName().contains("-nation-chat")) {
			//player used it in nation chat
			//guarentees player is in a nation with a chat
			PlayerConfig playerConfig = PlayerConfig.getConfig(playerUUID);
			String originalTownName = playerConfig.getString("Town");
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			String nationName = townConfig.getString("Nation");
			
			//check if a voice channel already exists:
			if(guild.getVoiceChannelsByName(nationName.toLowerCase() + "-nation-voice", false).size() > 0) {
				hook.sendMessage(nationName + " already has a voice channel open.").queue();
				return;
			}
			
			String nationRoleName = nationName + " nation";
			Role nationRole = guild.getRolesByName(nationRoleName, false).get(0);
			
			long viewPermission = Permission.VIEW_CHANNEL.getRawValue();
			guild.createVoiceChannel(nationName.toLowerCase() + "-nation-voice", guild.getCategoryById("859961704456257546")) //assign to the 'Minecraft' category
					.addRolePermissionOverride(guild.getPublicRole().getIdLong(), 0, viewPermission) //deny viewing
					.addRolePermissionOverride(nationRole.getIdLong(), viewPermission, 0).queue(); //allow viewing
			
			hook.sendMessage("You created the **" + nationName.toLowerCase() + "-nation-voice** channel!\nThe channel will be removed when there are no users left in the channel.").queue();
			
			String playerName = playerConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(playerUUID).getName() : Chat.stripColor(playerConfig.getString("Name"));
			Nation.sendMessage(nationName, playerName + " has opened the " + nationName + " voice channel.");
			
			//check if no users in after 5 minutes and remove
			Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
				if(guild.getVoiceChannelsByName(nationName.toLowerCase() + "-nation-voice", false).size() > 0) {
					VoiceChannel voiceChannel = guild.getVoiceChannelsByName(nationName.toLowerCase() + "-nation-voice", false).get(0);
					if(voiceChannel.getMembers().size() == 0) {
						voiceChannel.delete().queue();
						Nation.sendMessage(nationName, "The voice channel has closed.");
					}
				}
			}, 6000L);
			
			return;
		} else {
			hook.sendMessage(" > To create a *town* voice channel, use this command from your town-chat. \n > To create a *nation* voice channel, use this command from your nation-chat.").queue();
			return;
		}
		//check if player is in a nation chat
	}
	
	public void unpunishCommand(SlashCommandEvent event, UUID staffUUID, String punishIDstring, String comments) {
		Chat.debug("Unpunish command executed!");
		event.deferReply(true).queue(); // Let the user know we received the command before doing anything else
		InteractionHook hook = event.getHook(); // This is a special webhook that allows you to send messages without having permissions in the channel and also allows ephemeral messages
		hook.setEphemeral(true); // All messages here will now be ephemeral implicitly
		
		if(staffUUID == null) {
			hook.sendMessage("You haven't linked your Discord account. Use **/discord link** in game to link.").queue();
			return; //user doesn't have account linked
		}
		
		OfflinePlayer player = Bukkit.getOfflinePlayer(staffUUID);
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		if(!player.isOp() && playerConfig.getString("StaffRank") == null) {
			hook.sendMessage("This is a staff command.").queue();
			return;
		}
		
		if(!Chat.isInteger(punishIDstring) || DeadMC.ReportsFile.data().getString("Activity." + Integer.parseInt(punishIDstring) + ".Type") == null) {
			hook.sendMessage("'" + punishIDstring + "' is not a valid punishment ID.").queue();
			return;
		}
		int activityID = Integer.parseInt(punishIDstring);
		
		if(DeadMC.ReportsFile.data().getString("Activity." + activityID + ".Type") != null) {
			Admin.ActivityType type = Admin.ActivityType.values()[DeadMC.ReportsFile.data().getInt("Activity." + activityID + ".Type")];
			int ID = DeadMC.ReportsFile.data().getInt("Activity." + activityID + ".ID");
			
			String timeOfReport = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Date/Time");
			//playerWhoReported will be null if console
			UUID playerWhoReported = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed by").equalsIgnoreCase("CONSOLE") ? null : UUID.fromString(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed by"));
			UUID playerReported = UUID.fromString(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + "." + type.toString() + "ed"));
			Admin.Punishment reason = Admin.Punishment.values()[DeadMC.ReportsFile.data().getInt(type.toString() + "." + ID + ".Reason")];

			if(type == Admin.ActivityType.Punish) {
				
				if(DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Reverted") == null) {
					
					if(playerWhoReported == null
							|| playerWhoReported.toString().equalsIgnoreCase(player.getUniqueId().toString())
							|| playerConfig.getInt("StaffRank") >= Admin.StaffRank.SENIOR_MOD.ordinal()) {

						OfflinePlayer reportedPlayer = Bukkit.getOfflinePlayer(playerReported);
						PlayerConfig banneeConfig = PlayerConfig.getConfig(reportedPlayer);
						
						for(Player staff : Bukkit.getOnlinePlayers()) {
							if(PlayerConfig.getConfig(staff.getUniqueId()).getString("StaffRank") != null) {
								staff.sendMessage("");
								staff.sendMessage(ChatColor.YELLOW + "================== Player Unpunished ==================");
								staff.sendMessage(ChatColor.GREEN + player.getName() + " unpunished " + reportedPlayer.getName() + " for " + reason.toString().toLowerCase().replace("_", " ") + ".");
								staff.sendMessage(ChatColor.WHITE + "Comments: " + comments);
								staff.sendMessage(ChatColor.YELLOW + "====================================================");
								staff.sendMessage("");
							}
						}
						
						int timePassedInMinutes = DateCode.getTimeSince(timeOfReport);
						String plural = "";
						if(timePassedInMinutes < 60) //less than an hour
							plural = timePassedInMinutes == 1 ? "1 minute" : timePassedInMinutes + " minutes";
						else if(timePassedInMinutes < 1440) //less than a day
							plural = timePassedInMinutes == 60 ? "1 hour" : (timePassedInMinutes / 60) + " hours";
						else if(timePassedInMinutes < 10080) //less than a week
							plural = timePassedInMinutes == 1440 ? "1 day" : (timePassedInMinutes / 1440) + " days";
						else if(timePassedInMinutes < 302400) //less than a month
							plural = timePassedInMinutes == 10080 ? "1 week" : (timePassedInMinutes / 10080) + " weeks";
						else
							plural = timePassedInMinutes == 302400 ? "1 month" : (timePassedInMinutes / 302400) + " months";
						
						//undo the chat offense
						if(banneeConfig.getString("Muted") != null) {
							int numberOfOffences = banneeConfig.getInt("MuteOffences");
							banneeConfig.set("MuteOffences", new Integer(numberOfOffences - 1));
						}
						
						hook.sendMessage("You have undone **" + reportedPlayer.getName() + "**'s punishment for **" + reason.toString().toLowerCase().replace(" ", " ") + "** (" + plural + " passed).").queue();
						
						//unmute
						banneeConfig.set("Muted", null);
						
						//unban
						if(banneeConfig.getString("Banned.Reason") != null) {
							int timeSinceBan = DateCode.getTimeSince(banneeConfig.getString("Banned.Start"));
							
							Admin.logBan(playerReported, Admin.Punishment.valueOf(banneeConfig.getString("Banned.Reason").replace(" ", "_")), banneeConfig.getString("Banned.Start"), timeSinceBan / 60);
							banneeConfig.set("Banned", null);
						}
						
						banneeConfig.save();
						
						int revertID = DeadMC.ReportsFile.data().getInt("Revert.CurrentID") + 1;
						DeadMC.ReportsFile.data().set("Revert.CurrentID", revertID);
						
						Admin.logActivity(Admin.ActivityType.Revert, revertID);
						
						DeadMC.ReportsFile.data().set("Revert." + revertID + ".Reverted by", player.getUniqueId().toString());
						DeadMC.ReportsFile.data().set("Revert." + revertID + ".Reverted", reportedPlayer.getUniqueId().toString());
						
						ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
						DeadMC.ReportsFile.data().set("Revert." + revertID + ".Date/Time", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
						DeadMC.ReportsFile.data().set("Revert." + revertID + ".Time passed", DateCode.getTimeSince(timeOfReport));
						DeadMC.ReportsFile.data().set("Revert." + revertID + ".Reason", reason.ordinal());
						DeadMC.ReportsFile.data().set("Revert." + revertID + ".Comments", comments);
						
						//keep only last 500 punishments
						if(DeadMC.ReportsFile.data().getString("Revert." + (revertID - 500) + ".Reverted") != null) {
							DeadMC.ReportsFile.data().set("Revert." + (revertID - 500), null);
						}
						DeadMC.ReportsFile.save();
						
						DeadMC.ReportsFile.data().set(type.toString() + "." + ID + ".Reverted", true);
						
						String nickName = playerConfig.getString("Name") != null ? playerConfig.getString("Name") : player.getName();
						WebhookUtil.deliverMessage(DiscordUtil.getJda().getTextChannelById(JDAListener.staffLogChannel), player, nickName, ":unlock: Reverted **" + reportedPlayer.getName() + "** for **" + reason.toString().toLowerCase().replace("_", " ") + "**.\nTime passed: " + plural + "\nComments: *" + comments + "*", (MessageEmbed) null);
						
						if(reportedPlayer.isOnline()) {
							reportedPlayer.getPlayer().sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You have been unpunished for " + reason.toString().toLowerCase().replace("_", " ") + ".");
						}
						
					} else {
						hook.sendMessage("You can only undo punishments that you created.").queue();
					}
				} else {
					hook.sendMessage("This has already been reverted.").queue();
				}
			} else {
				hook.sendMessage("'" + activityID + "' is not a punishment (it is a " + type.toString() + ").").queue();
			}
		} else {
			hook.sendMessage("'" + punishIDstring + "' is not a valid Activity ID.").queue();
		}
	}
	
	public void punishCommand(SlashCommandEvent event, UUID staffUUID, String playerName, String punishmentString, String timeString, String comments) {
		Chat.debug("Punish command executed!");
		event.deferReply(true).queue(); // Let the user know we received the command before doing anything else
		InteractionHook hook = event.getHook(); // This is a special webhook that allows you to send messages without having permissions in the channel and also allows ephemeral messages
		hook.setEphemeral(true); // All messages here will now be ephemeral implicitly
		
		if(staffUUID == null) {
			hook.sendMessage("You haven't linked your Discord account. Use **/discord link** in game to link.").queue();
			return; //user doesn't have account linked
		}
		
		OfflinePlayer player = Bukkit.getOfflinePlayer(staffUUID);
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		if(!player.isOp() && playerConfig.getString("StaffRank") == null) {
			hook.sendMessage("This is a staff command.").queue();
			return;
		}

		for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
			if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(playerName)) {
				//used nick name
				playerName = onlinePlayer.getName();
				break;
			}
		}
		UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
		
		if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {

			if(Admin.getPunishmentFromString(punishmentString) != null) {
				Admin.Punishment punishment = Admin.getPunishmentFromString(punishmentString);

				//if other player is not a staff member - or this player is OP (to punish a staff member)
				PlayerConfig otherPlayerConfig = PlayerConfig.getConfig(uuid);
				if(otherPlayerConfig.getString("StaffRank") == null
						|| player.isOp()) {

					int punishID = DeadMC.ReportsFile.data().getInt("Punish.CurrentID") + 1;
					DeadMC.ReportsFile.data().set("Punish.CurrentID", punishID);
					
					int activityID = Admin.logActivity(Admin.ActivityType.Punish, punishID);
					
					int punishmentTime = 0;
					
					if(punishment == Admin.Punishment.HACKING || punishment == Admin.Punishment.FALSE_REPORTING || punishment == Admin.Punishment.BUG_ABUSE || punishment == Admin.Punishment.PUBLIC_NUISANCE || punishment == Admin.Punishment.ALIAS_ABUSE) {

						int time = 0;
						
						String numbers = "";
						int i = 0;
						while(Chat.isInteger(String.valueOf(timeString.charAt(i)))) {
							numbers += timeString.charAt(i);
							//Chat.broadcastMessage("Numbers: " + numbers);
							i++;
						}

						if(((timeString.toLowerCase().contains("hour")
								|| timeString.toLowerCase().contains("day")
								|| timeString.toLowerCase().contains("week")
								|| timeString.toLowerCase().contains("month"))
								
								&& numbers.length() > 0)
								
								|| timeString.toLowerCase().contains("permanent")) {
							
							String bannedMessage = ChatColor.WHITE + "You have been " + ChatColor.RED + ChatColor.BOLD + "banned" + ChatColor.WHITE + " for " + ChatColor.BOLD + punishment.toString().replace("_", " ") + ChatColor.WHITE + "\nVisit " + ChatColor.GOLD + "www.DeadMC.com/forums " + ChatColor.WHITE + "to lodge an appeal. \n\n\"" + ChatColor.ITALIC + comments + ChatColor.WHITE + "\"\nPlease follow the rules.";
							if(timeString.toLowerCase().contains("hour"))
								time = Integer.parseInt(numbers);
							else if(timeString.toLowerCase().contains("day"))
								time = Integer.parseInt(numbers) * 24;
							else if(timeString.toLowerCase().contains("week"))
								time = Integer.parseInt(numbers) * 168;
							else if(timeString.toLowerCase().contains("month"))
								time = Integer.parseInt(numbers) * 730;
							
							if(time != 0) { //not permanent
								String plural = "";
								
								if(time < 24) //less than a day
									plural = time == 1 ? "1 hour" : time + " hours";
								else if(time < 168) //less than a week
									plural = time == 24 ? "1 day" : (time / 24) + " days";
								else if(time < 730) //less than a month
									plural = time == 168 ? "1 week" : (time / 168) + " weeks";
								else
									plural = time == 730 ? "1 month" : (time / 730) + " months";
								
								bannedMessage = "You have been banned " + ChatColor.RED + ChatColor.BOLD + plural + ChatColor.WHITE + " for " + ChatColor.BOLD + punishment.toString().replace("_", " ") + ChatColor.WHITE + ".\n\n\"" + ChatColor.ITALIC + comments + ChatColor.WHITE + "\"\nPlease follow the rules.";
							}
							final String finalMessage = bannedMessage;

							String banee = player.getName();
							
							ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
							otherPlayerConfig.set("Banned.Start", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
							if(time > 0)
								otherPlayerConfig.set("Banned.Time", time); //a null banned time means permanent
							otherPlayerConfig.set("Banned.Reason", punishment.toString().replace("_", " "));
							otherPlayerConfig.set("Banned.Comments", comments);
							otherPlayerConfig.set("Banned.Punished by", player.getUniqueId().toString());
							otherPlayerConfig.set("Banned.ActivityID", activityID);
							otherPlayerConfig.save();
							
							//check if any of the aliases are logged in:
							List<String> playersToCheck = new ArrayList<String>();
							if(otherPlayerConfig.getString("Aliases") != null)
								playersToCheck.addAll(otherPlayerConfig.getStringList("Aliases")); //add all aliases
							playersToCheck.add(uuid.toString()); //add the actual player

							for(String aliasUUID : playersToCheck) { //all aliases
								if(Bukkit.getPlayer(UUID.fromString(aliasUUID)) != null) {
									
									Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
										@Override
										public void run() {
											Bukkit.getPlayer(UUID.fromString(aliasUUID)).kickPlayer(finalMessage);
										}
									}, 10L);
									
								}
							}
							
							punishmentTime = time*60;
							
						} else {
							hook.sendMessage("'" + timeString + "' is not a valid time. Valid: (Xhour(s), Xday(s), Xweek(s), Xmonth(s), permanent)").queue();
							return;
						}
						
					}
					
					int numberOfOffences = 0;
					if(punishment == Admin.Punishment.OFFENSIVE_LANGUAGE || punishment == Admin.Punishment.SPAMMING || punishment == Admin.Punishment.SHARING_LINKS) {
						//dmc <tempban/mute> <player> <time in minutes> <reason ID>
						if(otherPlayerConfig.getString("MuteOffences") != null) {
							numberOfOffences = otherPlayerConfig.getInt("MuteOffences");
						}
						numberOfOffences++;
						int muteTime = numberOfOffences * (60 * (numberOfOffences - 1)); //second offense is 2 hours, third is 6 hours, forth is 12 hours
						if(numberOfOffences == 1) muteTime = 30; //first offense is 30 minutes
						punishmentTime = muteTime;
						
						otherPlayerConfig.set("MuteOffences", numberOfOffences);
						
						ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
						otherPlayerConfig.set("Muted.Start", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
						otherPlayerConfig.set("Muted.Time", muteTime); //time in minutes
						otherPlayerConfig.set("Muted.Reason", punishment.ordinal());
						otherPlayerConfig.set("Muted.PunishID", punishID);
						
						otherPlayerConfig.save();
					}
					
					DeadMC.ReportsFile.data().set("Punish." + punishID + ".Punished by", player.getUniqueId().toString());
					DeadMC.ReportsFile.data().set("Punish." + punishID + ".Punished", uuid.toString());
					
					ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
					DeadMC.ReportsFile.data().set("Punish." + punishID + ".Date/Time", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
					DeadMC.ReportsFile.data().set("Punish." + punishID + ".Reason", punishment.ordinal());
					DeadMC.ReportsFile.data().set("Punish." + punishID + ".Comments", comments);
					//keep only last 500 punishments
					if(DeadMC.ReportsFile.data().getString("Punish." + (punishID - 500) + ".Punished") != null)
						DeadMC.ReportsFile.data().set("Punish." + (punishID - 500), null);
					DeadMC.ReportsFile.save();
					
					String plural = "";
					if(punishmentTime < 60) //less than an hour
						plural = punishmentTime == 1 ? "1 minute" : punishmentTime + " minutes";
					else if(punishmentTime < 1440) //less than a day
						plural = punishmentTime == 60 ? "1 hour" : (punishmentTime / 60) + " hours";
					else if(punishmentTime < 10080) //less than a week
						plural = punishmentTime == 1440 ? "1 day" : (punishmentTime / 1440) + " days";
					else if(punishmentTime < 302400) //less than a month
						plural = punishmentTime == 10080 ? "1 week" : (punishmentTime / 10080) + " weeks";
					else
						plural = punishmentTime == 302400 ? "1 month" : (punishmentTime / 302400) + " months";

					Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + playerName + " has been punished for " + punishment.toString().toLowerCase().replace("_", " ") + ".");
					hook.sendMessage("You punished **" + playerName + "** for " + punishmentString + ". Undo this action with **/unpunish " + activityID + "**.").queue();
					String emoji = punishment == Admin.Punishment.OFFENSIVE_LANGUAGE || punishment == Admin.Punishment.SPAMMING || punishment == Admin.Punishment.SHARING_LINKS ? ":mute:" : ":judge:";
					String timeLine = punishmentTime > 0 ? "\nTime: " + plural : "";
					String nickName = playerConfig.getString("Name") != null ? playerConfig.getString("Name") : player.getName();
					WebhookUtil.deliverMessage(DiscordUtil.getJda().getTextChannelById(staffLogChannel), player, nickName, emoji + " Punished **" + playerName + "** for **" + punishment.toString().toLowerCase().replace("_", " ") + "**." + timeLine + "\nComments: *" + comments + "*\n> Undo with /unpunish **" + activityID + "**", (MessageEmbed) null);
				} else {
					hook.sendMessage("Cannot punish another staff member.").queue();
				}
			} else {
				hook.sendMessage("Unknown rule '" + punishmentString + "'. (Try: **ALIAS_ABUSE**, **BUG_ABUSE**, **FALSE_REPORTING**, **HACKING**, **OFFENSIVE_LANGUAGE**, **PUBLIC_NUISANCE**, **SHARING_LINKS**, **SPAMMING**)").queue();
			}
		} else {
			hook.sendMessage("Unknown player '" + playerName + "'.").queue();
		}
	}
	
	@Override // we can use any of JDA's events through ListenerAdapter, just by overriding the methods
	public void onGuildUnavailable(@NotNull GuildUnavailableEvent event) {
		plugin.getLogger().severe("Oh no " + event.getGuild().getName() + " (Discord) went unavailable :(");
	}
}