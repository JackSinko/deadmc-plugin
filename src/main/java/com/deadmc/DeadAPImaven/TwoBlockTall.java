package com.deadmc.DeadAPImaven;

import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.type.Door;

public class TwoBlockTall {
	public BlockState topHalf;
	public BlockState bottomHalf;
	
	public TwoBlockTall(BlockState topHalf, BlockState bottomHalf) {
		this.bottomHalf = bottomHalf;
		this.topHalf = topHalf;
	}
	
	//returns if the block is the second block high
	public static boolean isTwoBlocksTall(BlockState state) {
		Material type = state.getType();
		return state.getType().toString().contains("_DOOR")
				|| type == Material.PEONY
				|| type == Material.TALL_GRASS
				|| type == Material.SUNFLOWER
				|| type == Material.ROSE_BUSH
				|| type == Material.LILAC
				|| type == Material.TALL_SEAGRASS
				|| type == Material.LARGE_FERN;
	}
	public static TwoBlockTall getTwoBlocksTall(BlockState state) {
		if(isTwoBlocksTall(state)) {
			try {
				//Chat.debug("Trying " + state.getY());
				if(state.getBlock().getRelative(BlockFace.UP).getType() == state.getType()) {
					//Chat.debug(" - is bottom half");
					return new TwoBlockTall(state.getBlock().getRelative(BlockFace.UP).getState(), state);
				}
				if(state.getBlock().getRelative(BlockFace.DOWN).getType() == state.getType()) {
					//Chat.debug(" - is top half");
					return new TwoBlockTall(state, state.getBlock().getRelative(BlockFace.DOWN).getState());
				}
			} catch(RuntimeException e) { Chat.debug("[Explosion] Could not get other block for " + state.getType()); }
		}
		return null; //returns null if not 2 blocks tall
	}

}
