package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Chat.Leaderboard;
import com.deadmc.DeadAPImaven.Pets.Pet;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import me.libraryaddict.disguise.disguisetypes.watchers.ZombieWatcher;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Entities implements Listener {
	private DeadMC plugin;
	
	public Entities(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public enum LootRarity {
		COMMON,
		UNCOMMON,
		RARE,
		VERY_RARE,
		SPECIAL
	}

	//VILLAGER NERF:
	
	//update old villagers:
	@EventHandler
	public void rightClickVillager(PlayerInteractEntityEvent event) {
		Entity entity = event.getRightClicked();
		Player player = event.getPlayer();
		if(entity instanceof Villager) {
			Villager villager = (Villager) entity;

			// REMOVE DISCOUNTS:
			
			//outdated in 1.17
//			CraftVillager craftVillager = (CraftVillager) entity;
//			EntityVillager v = craftVillager.getHandle();
//
//			// Gets the current reputation entries of the villager
//			Reputation reputation = v.fj();
//
//			UUID playerUniqueId = player.getUniqueId();
//
//			// Minor and major positive reputation types can only be received through curing
//			int minorPositive = reputation.a(playerUniqueId,
//					reputationType -> reputationType == ReputationType.MINOR_POSITIVE);
//			int majorPositive = reputation.a(playerUniqueId,
//					reputationType -> reputationType == ReputationType.MAJOR_POSITIVE);
//
//			// Having the current value at 0 means the player does not benefit from having cured the villager
//			if(minorPositive != 0 || majorPositive != 0) {
//				// This method adds to the current reputation, so we negate the current one to set it to 0
//				reputation.a(playerUniqueId, ReputationType.MINOR_POSITIVE, minorPositive * -1);
//				reputation.a(playerUniqueId, ReputationType.MAJOR_POSITIVE, majorPositive * -1);
//
//				//reopen menu:
//				Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
//					@Override
//					public void run() {
//						event.getPlayer().closeInventory();
//						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
//							@Override
//							public void run() {
//								event.getPlayer().openMerchant(villager, true);
//							}
//						}, 1L);
//					}
//				}, 1L);
//			}

			
			//REMOVE HotV
//
//			if(player.hasPotionEffect(PotionEffectType.HERO_OF_THE_VILLAGE)) {
//				player.removePotionEffect(PotionEffectType.HERO_OF_THE_VILLAGE);
//				player.removePotionEffect(PotionEffectType.HERO_OF_THE_VILLAGE);
//				//reopen menu:
//				Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
//					@Override
//					public void run() {
//						event.getPlayer().closeInventory();
//						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
//							@Override
//							public void run() {
//								event.getPlayer().openMerchant(villager, true);
//							}
//						}, 1L);
//					}
//				}, 1L);
//			}
			
			
			// ADJUST PRICES:
			
			List<MerchantRecipe> villagerRecipes = new ArrayList<>(villager.getRecipes());
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				FileConfiguration lootFile = DeadMC.LootFile.data();
				
				boolean reopenMenu = false;
				
				List<MerchantRecipe> recipes = new ArrayList<MerchantRecipe>();
				
				for(MerchantRecipe recipe : villagerRecipes) {
					
					//trading emeralds for etc.
					
					//emerald = 30
					//iron axe = 180
					// therefore should equal 180/30 = 6 emeralds (not 3)
					String ingredient = recipe.getIngredients().get(0).getType().toString();
					if(ingredient.equalsIgnoreCase("EMERALD")) {
						
						String result = recipe.getResult().getType().toString();
						int value = 0;
						
						if(lootFile.getString("Villagers." + result) != null) {
							
							//use adjusted price
							value = lootFile.getInt("Villagers." + result);
							
						} else {
							//attempt to use market price
							
							//loop all shops to see if market has shop
							FileConfiguration shops = DeadMC.ShopsFile.data();
							for(String shopCode : DeadMC.ShopsFile.data().getKeys(false)) {
								if(DeadMC.ShopsFile.data().getString(shopCode + ".Item") != null) {
									//shop is set up properly
									
									ItemStack shopItem = new ItemStack(shops.getItemStack(shopCode + ".Item")).clone();
									if(shopItem.getType().toString().equalsIgnoreCase(result)) { //is specified item
										value = shops.getInt(shopCode + ".BuyPrice");
										//using market price
									}
								}
							}
							
							if(value == 0) {
								recipes.add(recipe);
								continue; //return if no market price
							}
						}
						
						int emeraldCost = (int) Math.ceil((double) value / (double) DeadMC.emeraldCost);
						List<ItemStack> newIngredients = new ArrayList<ItemStack>();
						ItemStack item = recipe.getIngredients().get(0);
						if(emeraldCost == item.getAmount()) {
							recipes.add(recipe);
							continue;
						}
						item.setAmount(emeraldCost);
						newIngredients.add(item);
						for(int count = 1; count < recipe.getIngredients().size(); count++) {
							newIngredients.add(recipe.getIngredients().get(count));
						}
						recipe.setIngredients(newIngredients);
						recipe.setPriceMultiplier(2);
						
						recipes.add(recipe);
						
						Bukkit.getConsoleSender().sendMessage("[Villagers] Adjusted trade to " + emeraldCost + " emeralds for " + result + " (" + event.getPlayer().getName() + ")");
						
						reopenMenu = true;
					}
					
					//trading for 1 emerald
					if(recipe.getResult().getType().toString().equalsIgnoreCase("EMERALD")
							&& recipe.getResult().getAmount() == 1) {
						
						int value = 0;
						
						if(lootFile.getString("Villagers." + ingredient) != null) {
							
							//use adjusted price
							value = lootFile.getInt("Villagers." + ingredient);
							
						} else {
							//attempt to use market price
							
							//run async:
							
							//loop all shops to see if market has shop
							FileConfiguration shops = DeadMC.ShopsFile.data();
							for(String shopCode : DeadMC.ShopsFile.data().getKeys(false)) {
								if(DeadMC.ShopsFile.data().getString(shopCode + ".Item") != null) {
									//shop is set up properly
									
									ItemStack shopItem = new ItemStack(shops.getItemStack(shopCode + ".Item")).clone();
									if(shopItem.getType().toString().equalsIgnoreCase(ingredient)) { //is specified item
										value = shops.getInt(shopCode + ".BuyPrice");
										//using market price
									}
								}
							}
							
							if(value == 0) {
								recipes.add(recipe);
								continue; //return if no market price
							}
						}
						
						//set new amount:
						int itemCost = (int) Math.ceil((double) DeadMC.emeraldCost / (double) value);
						List<ItemStack> newIngredients = new ArrayList<ItemStack>();
						ItemStack item = recipe.getIngredients().get(0);
						if(itemCost == item.getAmount()) {
							recipes.add(recipe);
							continue;
						}
						item.setAmount(itemCost);
						newIngredients.add(item);
						for(int count = 1; count < recipe.getIngredients().size(); count++) {
							newIngredients.add(recipe.getIngredients().get(count));
						}
						recipe.setIngredients(newIngredients);
						recipe.setPriceMultiplier(2);
						
						recipes.add(recipe);
						
						Bukkit.getConsoleSender().sendMessage("[Villagers] Adjusted trade to " + itemCost + " " + ingredient + " for EMERALD (" + event.getPlayer().getName() + ")");
						
						reopenMenu = true;
					}
				}
				
				boolean finalReopenMenu = reopenMenu;
				Bukkit.getScheduler().runTask(plugin, () -> {
					villager.setRecipes(recipes);
					
					if(finalReopenMenu) {
						//reopen menu:
						Bukkit.getScheduler().runTaskLater(plugin, () -> {
							event.getPlayer().closeInventory();
							Bukkit.getScheduler().runTaskLater(plugin, () -> {
								event.getPlayer().openMerchant(villager, true);
							}, 1L);
						}, 1L);
					}
					
				});
			});
			
		}
	}
	
	@EventHandler
	public void onPlayerTakeDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player player) {
			if(Travel.travelEnabledPlayers.containsKey(player.getName())) {
				Travel.cancelTravel(player);
				player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Travel cancelled due to damage taken.");
			}
		}
	}
	
	public void setHelmet(LivingEntity entity) {
		EntityEquipment equipment = entity.getEquipment();
		equipment.setHelmet(new ItemStack(Material.LEATHER_HELMET));
		
		//for halloween:
		//if(entity.getType() == EntityType.ZOMBIE_VILLAGER)
			//equipment.setHelmet(new ItemStack(Material.SKELETON_SKULL));
		//else equipment.setHelmet(new ItemStack(Material.JACK_O_LANTERN));
	}
	
	public void setItemInHand(LivingEntity entity, ItemStack item) {
		EntityEquipment equipment = entity.getEquipment();
		int randomAmount = (int) (Math.random() * 3); // 0-2 (inclusive)
		
		if(randomAmount <= 1) equipment.setItemInMainHand(item);
		else equipment.setItemInOffHand(item);
	}
	
	public int getRandomRange() {
		//return -5 to 5
		int randomRange = (int) (Math.random() * 6); // 0-5 (inclusive)
		
		int chance = (int) (Math.random() * 2); // 50% chance of being negative
		if(chance == 1) randomRange = -randomRange;
		
		return randomRange;
	}
	
	public static EntityType getRandomZombieType() {
		EntityType randomZombieType = EntityType.ZOMBIE;
		int randomAmount = (int) (Math.random() * 100); // 0-99 (inclusive)
		
		if(randomAmount < 15)  //15% chance of husk
			randomZombieType = EntityType.HUSK;
		else if(randomAmount < 40) //25% chance of villager
			randomZombieType = EntityType.ZOMBIE_VILLAGER;
		else if(randomAmount < 50) //10% chance of drowned
			randomZombieType = EntityType.DROWNED;
		
		return randomZombieType;
	}
	
	public static List<EntityType> typesToRemoveIfUnclaimed = (List<EntityType>) Arrays.asList(
			EntityType.AXOLOTL,
			EntityType.BAT,
			EntityType.BEE,
			EntityType.BLAZE,
			EntityType.CAT,
			EntityType.CAVE_SPIDER,
			EntityType.CHICKEN,
			EntityType.COD,
			EntityType.COW,
			EntityType.DOLPHIN,
			EntityType.DONKEY,
			EntityType.FOX,
			EntityType.GLOW_SQUID,
			EntityType.GOAT,
			EntityType.HOGLIN,
			EntityType.HORSE,
			EntityType.MAGMA_CUBE,
			EntityType.MULE,
			EntityType.LLAMA,
			EntityType.MUSHROOM_COW,
			EntityType.OCELOT,
			EntityType.PARROT,
			EntityType.PIG,
			EntityType.GOAT,
			EntityType.PUFFERFISH,
			EntityType.RABBIT,
			EntityType.SALMON,
			EntityType.SHEEP,
			EntityType.SILVERFISH,
			EntityType.SKELETON,
			EntityType.SLIME,
			EntityType.SNOWMAN,
			EntityType.SQUID,
			EntityType.TROPICAL_FISH,
			EntityType.TURTLE,
			EntityType.WOLF
	);
	public static List<EntityType> typesToAlwaysRemove = (List<EntityType>) Arrays.asList(
			EntityType.ARROW,
			EntityType.EXPERIENCE_ORB
	);
	
	//this event doesn't work!
	@EventHandler
	public void onChunkUnload(ChunkUnloadEvent event) {
//
//		event.getChunk().setForceLoaded(true);
//		event.getChunk().load();
//
//		Entity[] entities = event.getChunk().getEntities();
//		if(entities.length > 0)
//			Chat.debug("[Unload] Found " + entities.length + " entities in chunk.", "chunk_despawning");
//
//		RegionManager adapter = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(event.getWorld()));
//
//		for(Entity entity : entities) {
//			Chat.debug(" checking " + entity.getType());
//			if(entity == null) {
//				Chat.debug("Entity is null!", "chunk_despawning");
//				continue;
//			}
//
//			//Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
//
//				Boolean isOldPet = DisguiseAPI.isDisguised(entity) && entity instanceof Wolf wolf && entity.isInvulnerable() && wolf.isAdult();
//				Boolean isOldDisguise = !DisguiseAPI.isDisguised(entity) && entity.getCustomName() != null && (entity.getCustomName().equalsIgnoreCase("Climber") || entity.getCustomName().equalsIgnoreCase("Bomber"));
//
//				EntityType type = entity.getType();
//				ApplicableRegionSet regionsAtLoc = adapter.getApplicableRegions(BukkitAdapter.asBlockVector(entity.getLocation()));
//				if(Entities.isZombie(type)
//						|| isOldPet
//						|| isOldDisguise
//						|| typesToAlwaysRemove.contains(type)
//						|| (typesToRemoveIfUnclaimed.contains(type) && regionsAtLoc.size() == 0 && entity.getCustomName() == null)) {
//
//					if(DeadMC.RestartFile.data().getString("DebugMessages.chunk_despawning") != null) {
//						String reason = "";
//						if(Entities.isZombie(type)) reason = " (is zombie)";
//						if(isOldPet) reason = ChatColor.RED + " (is broken pet)";
//						if(isOldDisguise) reason = ChatColor.RED + " (is broken disguise)";
//						if(typesToAlwaysRemove.contains(type)) reason = " (is always)";
//						if(typesToRemoveIfUnclaimed.contains(type) && regionsAtLoc.size() == 0 && entity.getCustomName() == null) reason = " (is always if unclaimed)";
//
//						String finalReason = reason;
//						Bukkit.getScheduler().runTask(plugin, () -> {
//							Chat.debug("[Despawn - Unload] " + entity.getType().toString() + finalReason);
//							entity.remove();
//						});
//					} else {
//						Bukkit.getScheduler().runTask(plugin, () -> {
//							entity.remove();
//						});
//					}
//
//				}
//			//});
//		}
//
//		event.getChunk().setForceLoaded(false);
//		Chat.broadcastMessage("Unloaded successfully? " + event.getChunk().unload(), false);
	}
	
	@EventHandler
	public void onChunkLoad(ChunkLoadEvent event) {

		RegionManager adapter = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(event.getWorld()));
		
		Entity[] entities = event.getChunk().getEntities();
//				if(entities.length > 0)
//					Chat.debug("[Load] Found " + entities.length + " entities in chunk.");
		for(Entity entity : entities) {
			if(entity == null) continue;
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				ApplicableRegionSet regionsAtLoc = adapter.getApplicableRegions(BukkitAdapter.asBlockVector(entity.getLocation()));
				
				Boolean isPeaceful = entity instanceof Animals || entity instanceof Ambient || entity instanceof Fish;
				
				if(((isPeaceful && regionsAtLoc.size() == 0) && entity.getCustomName() == null)) {
					//amount++;
					
					if(DeadMC.RestartFile.data().getString("DebugMessages.chunk_despawning") != null) {
						String reason = "";
						if(isPeaceful && regionsAtLoc.size() == 0) reason = " (is peaceful)";
						
						String finalReason = reason;
						Bukkit.getScheduler().runTask(plugin, () -> {
							//Chat.debug("[Despawn - Load] " + entity.getType().toString() + finalReason);
							entity.remove();
						});
					} else {
						Bukkit.getScheduler().runTask(plugin, () -> {
							entity.remove();
						});
					}
				}
				
			});
		}
	}
	
	//teleport player back if nether portal is blocked
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onNetherPortalTravel(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		if(event.getCause() == TeleportCause.NETHER_PORTAL) {
			
			Block firstPortal = event.getTo().getBlock();
			BlockFace oppositeDirection = null;
			
			List<BlockFace> directionsToTry = new ArrayList<>();
			directionsToTry.add(BlockFace.NORTH);
			directionsToTry.add(BlockFace.EAST);
			directionsToTry.add(BlockFace.SOUTH);
			directionsToTry.add(BlockFace.WEST);
			
			//get opposite block
			for(BlockFace direction : directionsToTry) {
				if(firstPortal.getRelative(direction).getType() == Material.NETHER_PORTAL) {
					oppositeDirection = direction;
					break;
				}
			}
			
			//get the opposite portal
			if(oppositeDirection == null) return;
			Block otherPortal = firstPortal.getRelative(oppositeDirection);
			
			//make sure it's bottom of portals
			while(firstPortal.getRelative(BlockFace.DOWN).getType() == Material.NETHER_PORTAL) {
				firstPortal = firstPortal.getRelative(BlockFace.DOWN);
				otherPortal = otherPortal.getRelative(BlockFace.DOWN);
			}
			
			List<Block> firstSideBlocks = new ArrayList<Block>();
			List<Block> otherSideBlocks = new ArrayList<Block>();
			if(oppositeDirection == BlockFace.NORTH || oppositeDirection == BlockFace.SOUTH) {
				//check east side first
				//check west side next
				//needs to have less 1 or less blocking
				
				//entire east side:
				firstSideBlocks.add(firstPortal.getRelative(BlockFace.EAST));
				firstSideBlocks.add(otherPortal.getRelative(BlockFace.EAST));
				firstSideBlocks.add(firstPortal.getRelative(BlockFace.EAST).getRelative(BlockFace.UP));
				firstSideBlocks.add(otherPortal.getRelative(BlockFace.EAST).getRelative(BlockFace.UP));
				firstSideBlocks.add(firstPortal.getRelative(BlockFace.EAST).getRelative(BlockFace.UP).getRelative(BlockFace.UP));
				firstSideBlocks.add(otherPortal.getRelative(BlockFace.EAST).getRelative(BlockFace.UP).getRelative(BlockFace.UP));
				//entire west side:
				otherSideBlocks.add(firstPortal.getRelative(BlockFace.WEST));
				otherSideBlocks.add(otherPortal.getRelative(BlockFace.WEST));
				otherSideBlocks.add(firstPortal.getRelative(BlockFace.WEST).getRelative(BlockFace.UP));
				otherSideBlocks.add(otherPortal.getRelative(BlockFace.WEST).getRelative(BlockFace.UP));
				otherSideBlocks.add(firstPortal.getRelative(BlockFace.WEST).getRelative(BlockFace.UP).getRelative(BlockFace.UP));
				otherSideBlocks.add(otherPortal.getRelative(BlockFace.WEST).getRelative(BlockFace.UP).getRelative(BlockFace.UP));
			}
			if(oppositeDirection == BlockFace.EAST || oppositeDirection == BlockFace.WEST) {
				//entire north side:
				firstSideBlocks.add(firstPortal.getRelative(BlockFace.NORTH));
				firstSideBlocks.add(otherPortal.getRelative(BlockFace.NORTH));
				firstSideBlocks.add(firstPortal.getRelative(BlockFace.NORTH).getRelative(BlockFace.UP));
				firstSideBlocks.add(otherPortal.getRelative(BlockFace.NORTH).getRelative(BlockFace.UP));
				firstSideBlocks.add(firstPortal.getRelative(BlockFace.NORTH).getRelative(BlockFace.UP).getRelative(BlockFace.UP));
				firstSideBlocks.add(otherPortal.getRelative(BlockFace.NORTH).getRelative(BlockFace.UP).getRelative(BlockFace.UP));
				//entire south side:
				otherSideBlocks.add(firstPortal.getRelative(BlockFace.SOUTH));
				otherSideBlocks.add(otherPortal.getRelative(BlockFace.SOUTH));
				otherSideBlocks.add(firstPortal.getRelative(BlockFace.SOUTH).getRelative(BlockFace.UP));
				otherSideBlocks.add(otherPortal.getRelative(BlockFace.SOUTH).getRelative(BlockFace.UP));
				otherSideBlocks.add(firstPortal.getRelative(BlockFace.SOUTH).getRelative(BlockFace.UP).getRelative(BlockFace.UP));
				otherSideBlocks.add(otherPortal.getRelative(BlockFace.SOUTH).getRelative(BlockFace.UP).getRelative(BlockFace.UP));
			}
			
			//check if blocked:
			
			String townOwnsLand = null;
			int blocksInFrontOfFirstSide = 0;
			for(Block block : firstSideBlocks) {
				//block.setType(Material.GREEN_STAINED_GLASS); //visualise blocks in front
				if(block.getType() != Material.AIR && block.getType() != Material.CAVE_AIR) {
					blocksInFrontOfFirstSide++;
					if(!Regions.playerOwnsLand(block.getLocation(), player.getUniqueId(), true)) {
						townOwnsLand = Regions.getOriginalTownName(Regions.getHighestPriorityLand(block.getLocation()).getId());
					}
				}
			}
			int blocksInFrontOfOtherSide = 0;
			for(Block block : otherSideBlocks) {
				//block.setType(Material.RED_STAINED_GLASS); //visualise blocks in front
				if(block.getType() != Material.AIR && block.getType() != Material.CAVE_AIR) {
					blocksInFrontOfOtherSide++;
					if(!Regions.playerOwnsLand(block.getLocation(), player.getUniqueId(), true)) {
						townOwnsLand = Regions.getOriginalTownName(Regions.getHighestPriorityLand(block.getLocation()).getId());
					}
				}
			}
			//Chat.broadcastMessage("First side: " + ChatColor.GREEN + blocksInFrontOfFirstSide + "blocks in front");
			//Chat.broadcastMessage("Other side: " + ChatColor.RED + blocksInFrontOfOtherSide + "blocks in front");
			if(blocksInFrontOfOtherSide > 1 && blocksInFrontOfFirstSide > 1
					&& townOwnsLand != null) {
				//is blocked
				Chat.sendBigTitle(player.getUniqueId(), "&4Portal blocked!", "&fSee the chat for info.");
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "This portal is blocked.");
				player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "It links to a portal owned by " + ChatColor.BOLD + townOwnsLand + ChatColor.WHITE + ".");
				player.sendMessage("");
				event.setCancelled(true);
			}
		}
	}
	
	int spawnAttempts = 0;
	int successful = 0;
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		
		//debugging;
//		spawnAttempts++;
//		long uptime = (System.currentTimeMillis() - Restarter.LAST_START_TIME) / 1000; // in milliseconds
//		Chat.debug("Spawn attempt (" + spawnAttempts + " in " + uptime + " seconds - " + successful + " successful)");

		event.getEntity().setNoDamageTicks(0);
		event.getEntity().setMaximumNoDamageTicks(0);
		
		//temp fix for 1.17
		//if(event.getEntityType() == EntityType.FOX) event.setCancelled(true);
		
		if(event.getEntityType() == EntityType.ARMOR_STAND
				|| event.getSpawnReason() == SpawnReason.EGG
				|| event.getSpawnReason() == SpawnReason.SPAWNER_EGG) return;
		
		if(event.getLocation().getWorld().getName().equalsIgnoreCase("revamp")
				|| DeadMC.DonatorFile.data().getString("Spawning") != null && !DeadMC.DonatorFile.data().getBoolean("Spawning")) {
			event.setCancelled(true);
			return;
		}
		
		EntityType type = event.getEntityType();
		
		//banned spawn types
		if(event.getSpawnReason() == SpawnReason.LIGHTNING
				|| event.getSpawnReason() == SpawnReason.SPAWNER
				|| event.getSpawnReason() == SpawnReason.TRAP
				|| event.getSpawnReason() == SpawnReason.NETHER_PORTAL
				|| type == EntityType.WITCH
				|| type == EntityType.TRADER_LLAMA
				|| type == EntityType.ENDERMITE
				|| type == EntityType.WANDERING_TRADER
				|| type == EntityType.TRADER_LLAMA
				|| type == EntityType.SLIME
				|| type == EntityType.SILVERFISH
				|| type == EntityType.GUARDIAN
				
				|| (event.getEntity().getWorld().getTime() > 12000
				&& (type == EntityType.SQUID
				|| type == EntityType.COD
				|| type == EntityType.PUFFERFISH
				|| type == EntityType.SALMON
				|| type == EntityType.TROPICAL_FISH
				|| type == EntityType.DOLPHIN
				|| type == EntityType.RABBIT))) {
			event.setCancelled(true);
			return;
		}

		if(event.getSpawnReason() == SpawnReason.DEFAULT
				|| event.getSpawnReason() == SpawnReason.NATURAL) {
			
			//any natural spawn type
			
			//if spawn position is too far away, try spawning
			
			Location location = event.getLocation();
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(location.getWorld()));
			boolean isInLongdale = location.getWorld().getName().equalsIgnoreCase("world") && regionManager.getRegions().get("longdale").contains(location.getBlockX(), location.getBlockY(), location.getBlockZ());
			
			//limit horde size:
			int hordeSize = isInLongdale ? 2 : BloodMoon.isBloodMoon ? 5 : 3;
			int hordeRadius = 8;
			Collection<Entity> nearbyEntities_hordeSize = event.getLocation().getWorld().getNearbyEntities(event.getLocation(), hordeRadius, 10.0, hordeRadius); //5 around, 5 down
			Collection<Entity> nearbyEntities_playerCheck = event.getLocation().getWorld().getNearbyEntities(event.getLocation(), 70, 70, 70);
			
			Boolean isAnimal = event.getEntity() instanceof Animals || event.getEntity() instanceof Ambient || event.getEntityType() == EntityType.RABBIT;
			Boolean isFish = event.getEntity() instanceof Fish || event.getEntityType() == EntityType.SQUID;
			
			Boolean isDay = event.getEntity().getWorld().getTime() < 12000;
			
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				Location spawnLocation = event.getLocation();
				
				long startTime = System.currentTimeMillis();
				
				//limit animal horde sizes:
				if(isAnimal && isDay) { //animals only spawn in day
					//Chat.broadcastMessage("Spawning " + event.getEntityType());
					
					int animalsAroundLimit = 8;
					Boolean playerNearby = false;
					int animalsNearby = 0;
					for(Entity entity : nearbyEntities_playerCheck) {
						if(isAnimal) {
							animalsNearby++;
						}
						if(entity instanceof Player)
							playerNearby = true;
					}
					if((animalsNearby + 1) > animalsAroundLimit
							|| !playerNearby) {
						Chat.debug("Cancelled animal spawn (too many) - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
						event.setCancelled(true);
						return;
					}
					//Chat.broadcastMessage("Spawned " + event.getEntity().getType() + ", " + (int)event.getLocation().distance(Bukkit.getPlayer("Thunder_Waffe").getLocation()) + " blocks away");
				}
				
				//limit fish horde sizes:
				if(isFish && isDay) { //fish only spawn in day
					//Chat.broadcastMessage("Spawning " + event.getEntityType());
					
					int fishAroundLimit = 8;
					Boolean playerNearby = false;
					int fishNearby = 0;
					for(Entity entity : nearbyEntities_playerCheck) {
						if(isFish) {
							fishNearby++;
						}
						if(entity instanceof Player)
							playerNearby = true;
					}
					if((fishNearby + 1) > fishAroundLimit
							|| !playerNearby) {
						Chat.debug("Cancelled fish spawn (too many) - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
						event.setCancelled(true);
						return;
					}
					//Chat.broadcastMessage("Spawned " + event.getEntity().getType() + ", " + (int)event.getLocation().distance(Bukkit.getPlayer("Thunder_Waffe").getLocation()) + " blocks away");
				}

				int zombiesNearby = 0;
				for(Entity entity : nearbyEntities_hordeSize) {
					if(isZombie(entity))
						zombiesNearby++;
				}
				if((zombiesNearby + 1) > hordeSize) {
					Chat.debug("Cancelled zombie spawn (horde limit reached) - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
					event.setCancelled(true);
					return;
				}
				
				int nearbyZombies = 0;
				
				for(Entity nearbyEntity : nearbyEntities_playerCheck) {
					if(isZombie(nearbyEntity) && nearbyEntity instanceof Monster monster && (monster.getTarget() != null || monster.getTicksLived() < 60)) {
						nearbyZombies++;
					}
				}
				
				//get list of players around spawn
				Boolean spawnIsNearPlayers = false;
				List<Player> nearbyPlayers = new ArrayList<Player>();
				
				for(Entity nearbyEntity : nearbyEntities_playerCheck) {
					if(nearbyEntity instanceof Player) {
						//if a player
						Player nearbyPlayer = (Player) nearbyEntity;
						if(!Admin.spectating.contains(nearbyPlayer.getName())
						&& !Admin.vanished.contains(nearbyPlayer.getName())) {
							nearbyPlayers.add(nearbyPlayer);
						}
					}
				}
				
				//limit the number of zombies around each player
				int playerMultiplier = BloodMoon.isBloodMoon ? (nearbyPlayers.size() * 6) : (nearbyPlayers.size() * 0);
				int zombieLimit = 10 + playerMultiplier;
				if(isInLongdale) zombieLimit = 6;
				
				if(nearbyZombies > zombieLimit) {
					Chat.debug("Cancelled zombie spawn (player limit reached) - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
					event.setCancelled(true);
					return;
				}
				
				double maxDistanceFromPlayerWhenUnderground = 10;
				double lowestPosWhenAboveGround = 52;
				double seaLevel = 62;
				
				double closestDistanceWhenSavingSpawn = 8;
				
				for(Player player : nearbyPlayers) {
					if(!player.getWorld().getName().equals(spawnLocation.getWorld().getName())) continue;
					double yDistanceBetween = Math.abs(player.getLocation().getY() - spawnLocation.getY());
					
					if((player.getLocation().getY() < seaLevel && yDistanceBetween <= maxDistanceFromPlayerWhenUnderground)
							|| player.getLocation().getY() > seaLevel && spawnLocation.getY() > lowestPosWhenAboveGround) {
						spawnIsNearPlayers = true;
						break;
					}
				}
				
				if(!spawnIsNearPlayers && isDay) {
					Chat.debug("Cancelled zombie spawn (too far away) - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
					event.setCancelled(true);
					return;
				}
				
				//if put at top, make sure the new limit isn't ignored
				if(!spawnIsNearPlayers) {
					//try putting on top
					//just try for closest player (first player)
					if(nearbyPlayers.size() > 0
							&& spawnLocation.getY() <= lowestPosWhenAboveGround
							&& nearbyPlayers.get(0).getLocation().getY() > seaLevel) {
						
						Player player = nearbyPlayers.get(0);
						spawnLocation = new Location(spawnLocation.getWorld(), spawnLocation.getX(), spawnLocation.getWorld().getHighestBlockYAt(spawnLocation), spawnLocation.getZ());
						
						double yDistanceBetween = Math.abs(player.getLocation().getY() - spawnLocation.getY());
						double distance = player.getLocation().distance(spawnLocation);
						
						if((player.getLocation().getY() < seaLevel && yDistanceBetween <= maxDistanceFromPlayerWhenUnderground)
								|| player.getLocation().getY() > seaLevel && spawnLocation.getY() > lowestPosWhenAboveGround) {
							if(distance >= closestDistanceWhenSavingSpawn) {
								//saved it!
								spawnIsNearPlayers = true;
								Chat.debug(" - SAVED THE SPAWN! - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
							} else {
								Chat.debug("Cancelled zombie spawn (too close when saving) - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
								event.setCancelled(true);
								return;
							}
						} else {
							Chat.debug("Cancelled zombie spawn (too far away after trying to save) - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
							event.setCancelled(true);
							return;
						}

					} else {
						Chat.debug("Cancelled zombie spawn (too far away) - " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ(), "spawning");
						event.setCancelled(true);
						return;
					}
				}
				
				Chat.debug("SUCCESS: " + spawnLocation.getX() + ", " + spawnLocation.getY() + ", " + spawnLocation.getZ() + " - Near players? " + spawnIsNearPlayers + " - Nearby zombies: " + nearbyZombies, "spawning");
				
				//hostile creatures
				if((type == EntityType.SKELETON
						|| type == EntityType.ZOMBIE
						|| type == EntityType.ZOMBIE_VILLAGER
						|| type == EntityType.ZOMBIFIED_PIGLIN
						|| type == EntityType.PIGLIN
						|| type == EntityType.SPIDER
						|| type == EntityType.CAVE_SPIDER
						|| type == EntityType.ENDERMAN
						|| type == EntityType.CREEPER
						|| type == EntityType.HUSK
						|| type == EntityType.DROWNED)
						
						//peaceful creatures - only cancel during night time
						|| (event.getEntity().getWorld().getTime() > 13000
						&& isAnimal)) {
					
					//spawn horde or boss?
					int chance = (int) (Math.random() * 100); // 0-99 (inclusive)
					int chanceOfClimbing = 7;
					if(BloodMoon.isBloodMoon) chanceOfClimbing = 20;
					
					long endTime = System.currentTimeMillis();
					Chat.debug("[Check spawn horde] Total SYNC time taken: " + (endTime - startTime) + " ms", "spawning");
					
					//pick depending on chance
					if(chance < chanceOfClimbing)
						spawnHorde(true, spawnLocation, nearbyPlayers, BloodMoon.isBloodMoon);
					else
						spawnHorde(false, spawnLocation, nearbyPlayers, BloodMoon.isBloodMoon);
				}
			});
			
			//remove the original entity:
			if(type == EntityType.CAVE_SPIDER
					|| type == EntityType.ENDERMAN
					|| type == EntityType.SPIDER
					|| type == EntityType.CREEPER
					|| type == EntityType.SKELETON
					|| (!isDay
					&& isAnimal)) {
				
				event.setCancelled(true);
			}
		}
	}
	
	//run async
	public void spawnHorde(Boolean climbing, Location spawnLocation, List<Player> nearbyPlayers, Boolean isBloodMoon) {
		int randomExtras = (int) (Math.random() * 2); // 0-1 (inclusive)
		int amountToSpawn = 1 + randomExtras;
		if(isBloodMoon) amountToSpawn++;
		
		//spawn zombies
		for(int count = 0; count < amountToSpawn; count++) {
			
			Tools.sleepThread((int) ((Math.random() * (500 - 100)) + 100)); //delay so zombies aren't on top of each other
			
			Boolean isCancelled = false;
			int numberOfChecks = 0;
			Location nonFinalLocation;
			do {
				//set random location
				
				numberOfChecks++;
				int randomX = getRandomRange();
				int randomZ = getRandomRange();
				nonFinalLocation = spawnLocation.add(randomX, 0, randomZ);
				int highestY = nonFinalLocation.getWorld().getHighestBlockAt(nonFinalLocation).getLocation().getBlockY() + 1;
				if(Math.abs(highestY - nonFinalLocation.getBlockY()) <= 8)
					nonFinalLocation.setY(highestY); //put entity on highest block, unless it's too far away
				
				//ensure it doesn't get into potential infinite loop:
				if(numberOfChecks > 40) { //is run async
					isCancelled = true;
					break;
				}
				
			} while(!(nonFinalLocation.getBlock().getType() == Material.WATER
					|| nonFinalLocation.getBlock().getType() == Material.AIR
					|| nonFinalLocation.getBlock().getType() == Material.CAVE_AIR
					|| nonFinalLocation.getBlock().getType() == Material.VOID_AIR
					|| nonFinalLocation.getBlock().getType() == Material.SNOW));
			
			if(isCancelled) break;
			
			//normal
			EntityType type = spawnLocation.getWorld().getName().equalsIgnoreCase("world_nether") ? EntityType.ZOMBIFIED_PIGLIN //spawn piglin in nether
					: getRandomZombieType();
			
			int playtimeMinutes = (int) Math.round(((nearbyPlayers.get(0).getStatistic(Statistic.PLAY_ONE_MINUTE) / 20.0) / 60.0) / 60.0);

			//the last one is the max chance of special. Eg. Tank's value = max chance
			int chance = (int) (Math.random() * 1000); // 0-999 (inclusive)
			int chanceOfBeing_Explosive = isBloodMoon ? 60 : 3;
			int chanceOfBeing_PyroBoss = isBloodMoon ? 0 : 0;
			int chanceOfBeing_FlyingBoss = isBloodMoon ? playtimeMinutes > 120 ? 100 : 70 : 0;
			int chanceOfBeing_Skeleton = isBloodMoon ? playtimeMinutes > 120 ? 140 : 80 : 6;
			int chanceOfBeing_Tank = isBloodMoon ? playtimeMinutes > 120 ? 160 : 0 : 0;
			int chanceOfBeing_Poisonous = isBloodMoon ? playtimeMinutes > 120 ? 190 : 90 : 8;
			int chanceOfBeing_BeastBoss = isBloodMoon ? 0 : 0;
			int chanceOfBeing_SpookySpider = isBloodMoon ? 0 : 0;
			
			final Location location = nonFinalLocation;
			ApplicableRegionSet regionsAtLoc = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(spawnLocation.getWorld())).getApplicableRegions(BukkitAdapter.asBlockVector(spawnLocation));
			Bukkit.getScheduler().runTask(plugin, () -> {
				long startTime = System.currentTimeMillis();
				
				//for halloween:
				//if(regionsAtLoc.size() == 0) {
				//spawnLocation.getWorld().spawnEntity(spawnLocation, EntityType.BAT);
				//}
				
				//can't run async:
				
				if(chance < chanceOfBeing_Explosive) {
					spawnExplosiveZombie(location, nearbyPlayers);
					long endTime = System.currentTimeMillis();
					Chat.debug("[Spawn bomber] Total SYNC time taken: " + (endTime - startTime) + " ms", "spawning");
				} else if(chance < chanceOfBeing_PyroBoss)
					spawnPyroBoss(location, nearbyPlayers);
				else if(chance < chanceOfBeing_FlyingBoss) {
					spawnFlyingBoss(location, nearbyPlayers);
					long endTime = System.currentTimeMillis();
					Chat.debug("[Spawn phantom] Total SYNC time taken: " + (endTime - startTime) + " ms", "spawning");
				} else if(chance < chanceOfBeing_Skeleton) {
					spawnSkeletonZombie(location, nearbyPlayers, isBloodMoon);
					long endTime = System.currentTimeMillis();
					Chat.debug("[Spawn destructor] Total SYNC time taken: " + (endTime - startTime) + " ms", "spawning");
				} else if(chance < chanceOfBeing_Tank) {
					spawnTankZombie(location, nearbyPlayers);
					long endTime = System.currentTimeMillis();
					Chat.debug("[Spawn tank] Total SYNC time taken: " + (endTime - startTime) + " ms", "spawning");
				} else if(chance < chanceOfBeing_Poisonous) {
					spawnPoisonZombie(location, nearbyPlayers);
				} else if(chance < chanceOfBeing_BeastBoss)
					spawnBeastBoss(location, nearbyPlayers);
				else if(chance < chanceOfBeing_SpookySpider)
					spawnSpookySpider(location, nearbyPlayers);
				else {
					//not special
					
					LivingEntity entity;
					if(climbing) {
						entity = spawnClimbingZombie(type, location, nearbyPlayers, isBloodMoon);
						long endTime = System.currentTimeMillis();
						Chat.debug("[Spawn climbing zombie] Total SYNC time taken: " + (endTime - startTime) + " ms", "spawning");
					} else {
						entity = (LivingEntity) location.getWorld().spawnEntity(location, type);
						long endTime = System.currentTimeMillis();
						Chat.debug("[Spawn normal zombie] Total SYNC time taken: " + (endTime - startTime) + " ms", "spawning");
					}
					
					//set baby zombies health
					if(entity instanceof Ageable ageable) {
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							Zombie zombie = (Zombie) entity;
							if(zombie.isBaby())
								zombie.setHealth(5); //zombie has 20 health - normal diamond sword does 1 hit kill
							
							//set items
							int chanceOfDualWeapons = (int) (Math.random() * 100); // 0-9 (inclusive)
							setRandomItemsInHand(entity);
							if(chanceOfDualWeapons < 30) //30%
								setRandomItemsInHand(entity);
							//set helmet
							int chanceOfHelmet = (int) (Math.random() * 100); // 0-99 (inclusive)
							if(chanceOfHelmet < 15) setHelmet(entity);
							
							//set target
							Player nearestPlayer = null;
							for(Player player : nearbyPlayers) {
								if(!player.getWorld().getName().equals(location.getWorld().getName())) continue;
								if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
										&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
									nearestPlayer = player;
							}
							if(nearestPlayer != null && !zombie.getLocation().getWorld().getName().equalsIgnoreCase("world_nether")) {
								zombie.setTarget(nearestPlayer);
							}
						});
					}
					
				}
			});
		}
		
	}
	
	public LivingEntity spawnClimbingZombie(EntityType type, Location location, List<Player> nearbyPlayers, Boolean isBloodMoon) {
		
		Spider spider = (Spider) location.getWorld().spawnEntity(location, EntityType.SPIDER);
		LivingEntity climbingZombie = (LivingEntity) spider;

		MobDisguise mobDisguise = new MobDisguise(DisguiseType.getType(type));
		mobDisguise.setReplaceSounds(true);
		spider.setSilent(true);
		DisguiseAPI.disguiseToAll(spider, mobDisguise);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				spider.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(40);
				if(isBloodMoon)
					spider.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.26);
				else spider.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.23);
				
				Player nearestPlayer = null;
				for(Player player : nearbyPlayers) {
					try {
						if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
								&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
							nearestPlayer = player;
					} catch(IllegalArgumentException e) {
						// Cannot measure distance if the player is in a different world
					}
				}
				if(nearestPlayer != null) {
					spider.setTarget(nearestPlayer);
				}
				
				EntityEquipment equipment = climbingZombie.getEquipment();
				//equipment.setBoots(new ItemStack(Material.LEATHER_BOOTS)); //climbing boots
				
				climbingZombie.setCustomName("Climber");
			}
		});
		
		return climbingZombie;
	}
	
	public void spawnExplosiveZombie(Location location, List<Player> nearbyPlayers) {
		Creeper creeper = (Creeper) location.getWorld().spawnEntity(location, EntityType.CREEPER);
		
		MobDisguise mobDisguise = new MobDisguise(DisguiseType.getType(getRandomZombieType()));
		mobDisguise.setReplaceSounds(true);
		ZombieWatcher watcher = (ZombieWatcher) mobDisguise.getWatcher();
		watcher.setBaby(true);
		DisguiseAPI.disguiseToAll(creeper, mobDisguise);
		
		creeper.setMaxHealth(1);
		creeper.setHealth(1);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				creeper.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(40);
				creeper.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.345);
				
				Player nearestPlayer = null;
				for(Player player : nearbyPlayers) {
					if(!player.getWorld().getName().equals(location.getWorld().getName())) continue;
					if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
							&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
						nearestPlayer = player;
				}
				if(nearestPlayer != null) {
					creeper.setTarget(nearestPlayer);
				}
				
				LivingEntity explosiveZombie = (LivingEntity) creeper;
				EntityEquipment equipment = explosiveZombie.getEquipment();
				equipment.setHelmet(new ItemStack(Material.TNT));
				
				creeper.setFireTicks(Integer.MAX_VALUE);
				
				creeper.setCustomName("Bomber");
			}
		});
	}
	
	public void spawnPoisonZombie(Location location, List<Player> nearbyPlayers) {
		Creeper creeper = (Creeper) location.getWorld().spawnEntity(location, EntityType.CREEPER);
		
		MobDisguise mobDisguise = new MobDisguise(DisguiseType.HUSK);
		ZombieWatcher watcher = (ZombieWatcher) mobDisguise.getWatcher();
		mobDisguise.setReplaceSounds(true);
		watcher.setBaby(true);
		DisguiseAPI.disguiseToAll(creeper, mobDisguise);
		
		creeper.setMaxHealth(1);
		creeper.setHealth(1);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				creeper.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(40);
				creeper.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(0.345);
				
				Player nearestPlayer = null;
				for(Player player : nearbyPlayers) {
					if(!player.getWorld().getName().equals(location.getWorld().getName())) continue;
					if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
							&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
						nearestPlayer = player;
				}
				if(nearestPlayer != null) {
					creeper.setTarget(nearestPlayer);
				}
				
				LivingEntity explosiveZombie = (LivingEntity) creeper;
				EntityEquipment equipment = explosiveZombie.getEquipment();
				equipment.setHelmet(new ItemStack(Material.CHISELED_DEEPSLATE));
				
				ItemStack potion = new ItemStack(Material.SPLASH_POTION);
				PotionMeta meta = (PotionMeta) potion.getItemMeta();
				meta.setBasePotionData(new PotionData(PotionType.POISON));
				potion.setItemMeta(meta);
				equipment.setItemInMainHand(potion);
				equipment.setItemInOffHand(potion.clone());
				
				creeper.setCustomName("Blinding Bomber");
			}
		});
	}
	
	public void spawnSkeletonZombie(Location location, List<Player> nearbyPlayers, Boolean isBloodMoon) {
		Skeleton skeleton = (Skeleton) location.getWorld().spawnEntity(location, EntityType.SKELETON);
		MobDisguise mobDisguise = new MobDisguise(DisguiseType.STRAY);
		DisguiseAPI.disguiseToAll(skeleton, mobDisguise);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				skeleton.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(40);
				
				Player nearestPlayer = null;
				for(Player player : nearbyPlayers) {
					if(!player.getWorld().getName().equals(location.getWorld().getName())) continue;
					if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
							&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
						nearestPlayer = player;
				}
				if(nearestPlayer != null) {
					skeleton.setTarget(nearestPlayer);
				}
				
				skeleton.setCustomName("Destructor");
				
				skeleton.getEquipment().getItemInMainHand().addEnchantment(Enchantment.ARROW_KNOCKBACK, 2);
				
				if(isBloodMoon) skeleton.getEquipment().setHelmet(new ItemStack(Material.CAMPFIRE));
				
			}
		});
	}
	
	public void spawnPyroBoss(Location location, List<Player> nearbyPlayers) {
		Zombie zombie = (Zombie) location.getWorld().spawnEntity(location, getRandomZombieType());
		zombie.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(40);
		
		zombie.setFireTicks(Integer.MAX_VALUE);
		
		zombie.setCustomName("PYRO");
	}
	
	public void spawnFlyingBoss(Location location, List<Player> nearbyPlayers) {
		Ghast ghast = (Ghast) location.getWorld().spawnEntity(location.add(new Location(location.getWorld(), 0, 6, 0)), EntityType.GHAST);
		
		MobDisguise mobDisguise = new MobDisguise(DisguiseType.PHANTOM);
		DisguiseAPI.disguiseToAll(ghast, mobDisguise);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				Player nearestPlayer = null;
				for(Player player : nearbyPlayers) {
					if(!player.getWorld().getName().equals(location.getWorld().getName())) continue;
					if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
							&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
						nearestPlayer = player;
				}
				if(nearestPlayer != null) {
					ghast.setTarget(nearestPlayer);
				}
			}
		});
	}
	
	public void spawnBeastBoss(Location location, List<Player> nearbyPlayers) {
		Ravager ravager = (Ravager) location.getWorld().spawnEntity(location, EntityType.RAVAGER);
		Skeleton zombie = (Skeleton) location.getWorld().spawnEntity(location, EntityType.SKELETON);
		ravager.setPassenger(zombie);
		
		ravager.setCustomName("Zombie Beast");
		zombie.setCustomName("Zombie Beast");
		
		Player nearestPlayer = null;
		for(Player player : nearbyPlayers) {
			if(!player.getWorld().getName().equals(location.getWorld().getName())) continue;
			if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
					&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
				nearestPlayer = player;
		}
		if(nearestPlayer != null) {
			ravager.setTarget(nearestPlayer);
			zombie.setTarget(nearestPlayer);
		}
		
		zombie.setMaxHealth(100.0);
		zombie.setHealth(100.0);
		EntityEquipment equipment = zombie.getEquipment();
		equipment.setItemInMainHand(new ItemStack(Material.BOW));
		equipment.setHelmet(new ItemStack(Material.NETHERITE_HELMET));
		equipment.setChestplate(new ItemStack(Material.NETHERITE_CHESTPLATE));
		equipment.setLeggings(new ItemStack(Material.NETHERITE_LEGGINGS));
		equipment.setBoots(new ItemStack(Material.NETHERITE_BOOTS));
		equipment.setItemInOffHand(new ItemStack(Material.SHIELD));
		
		MobDisguise zombieDisguise = new MobDisguise(DisguiseType.ZOMBIE);
		DisguiseAPI.disguiseToAll(zombie, zombieDisguise);
	}
	
	@EventHandler
	public void skeletonShootBow(EntityShootBowEvent event) {
		Entity entity = event.getEntity();
		if(entity.getCustomName() != null && entity.getCustomName().equalsIgnoreCase("Zombie Beast")) {
			Fireball fireball = entity.getWorld().spawn(event.getProjectile().getLocation(), Fireball.class);
			event.setProjectile(fireball);
		}
	}
	
	public void spawnSpookySpider(Location location, List<Player> nearbyPlayers) {
		//spawn halloween zombie
		CaveSpider spider = (CaveSpider) location.getWorld().spawnEntity(location, EntityType.CAVE_SPIDER);
		spider.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(5);
		
		EntityType type = location.getWorld().getName().contains("nether") ? EntityType.ZOMBIFIED_PIGLIN : Entities.getRandomZombieType();
		Zombie zombie = (Zombie) location.getWorld().spawnEntity(location, type);
		zombie.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(5);
		zombie.setBaby();
		spider.setPassenger(zombie);
		
		Player nearestPlayer = null;
		for(Player player : nearbyPlayers) {
			if(!player.getWorld().getName().equals(location.getWorld().getName())) continue;
			if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
					&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
				nearestPlayer = player;
		}
		if(nearestPlayer != null) {
			spider.setTarget(nearestPlayer);
			zombie.setTarget(nearestPlayer);
		}
		
		spider.setCustomName("Spooky Spider");
		zombie.setCustomName("Spooky Spider");
		
		EntityEquipment equipment = zombie.getEquipment();
		equipment.setItemInMainHand(new ItemStack(Material.PUMPKIN));
		equipment.setItemInOffHand(new ItemStack(Material.PUMPKIN));
		equipment.setHelmet(new ItemStack(Material.CARVED_PUMPKIN));
	}
	
	public void spawnTankZombie(Location location, List<Player> nearbyPlayers) {
		Spider zombie = (Spider) location.getWorld().spawnEntity(location, EntityType.SPIDER);
		MobDisguise zombieDisguise = new MobDisguise(DisguiseType.STRAY);
		DisguiseAPI.disguiseToAll(zombie, zombieDisguise);
		
		zombie.setMaxHealth(150.0);
		zombie.setHealth(150.0);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				ItemStack sword = new ItemStack(Material.IRON_SWORD);
				sword.addEnchantment(Enchantment.KNOCKBACK, 2);
				
				EntityEquipment equipment = zombie.getEquipment();
				equipment.setItemInMainHand(sword);
				equipment.setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
				equipment.setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
				equipment.setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
				equipment.setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
				equipment.setItemInOffHand(new ItemStack(Material.SHIELD));
				
				zombie.setCustomName("Tank");
				
				Player nearestPlayer = null;
				for(Player player : nearbyPlayers) {
					if(!player.getWorld().getName().equals(location.getWorld().getName())) continue;
					if((!(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())))
							&& (nearestPlayer == null || location.distance(player.getLocation()) > location.distance(nearestPlayer.getLocation())))
						nearestPlayer = player;
				}
				if(nearestPlayer != null) {
					zombie.setTarget(nearestPlayer);
				}
			}
		});
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		//stop zombies from burning
		Entity entity = event.getEntity();
		if(isZombie(entity) && (event.getCause() == DamageCause.FIRE_TICK || event.getCause() == DamageCause.FIRE) && (BloodMoon.isBloodMoon || entity.getCustomName() != null))
			event.setCancelled(true);
		if(isZombie(entity) && event.getCause() == DamageCause.BLOCK_EXPLOSION)
			event.setCancelled(true); //skeleton/creeper explosions dont damage zombies
		
		if(entity instanceof Player && Admin.nodamage.contains(((Player) entity).getName()))
			event.setCancelled(true);
	}
	
	public static List<String> shotDestructorArrow = new ArrayList<String>();
	public static List<String> shotBloodMoonArrow = new ArrayList<String>();
	
	@EventHandler
	public void onShootProjectile(EntityShootBowEvent event) {
		if(isZombie(event.getEntity()) && BloodMoon.isBloodMoon) {
			event.getProjectile().setFireTicks(Integer.MAX_VALUE);
		}
	}
	
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			
			if(event.getEntity().getShooter() instanceof Entity) {
				Entity shooter = (Entity) event.getEntity().getShooter();
				
				Boolean bloodMoon = false;
				Boolean shotByPlayer = false;
				if(shooter instanceof Player player) {
					if(Explosion.isExplodingProjectile(event.getEntityType())) {
						if(shotBloodMoonArrow.contains(player.getName())) {
							bloodMoon = true;
							shotByPlayer = true;

						} else if(shotDestructorArrow.contains(player.getName())) {
							if(event.getEntity() instanceof Arrow) {
								//blow up - dont destroy unowned land
								shotByPlayer = true;
							}
							
						}
					}
					shotBloodMoonArrow.remove(shooter.getName()); //remove either way
					shotDestructorArrow.remove(shooter.getName()); //remove either way
				}
				
				if(isZombie(shooter)
						|| shotByPlayer) {
					
					//check shooter has custom tag (block breaker etc.)
					
					Location location = event.getHitBlock() != null ? event.getHitBlock().getLocation() :  event.getHitEntity().getLocation(); //initialise empty
					
					//disable in market
					if(location.getWorld().getName().equalsIgnoreCase("world")) {
						RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(location.getWorld()));
						if(regionManager.getRegions().get("market").contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())) {
							Bukkit.getScheduler().runTask(plugin, () -> event.getEntity().remove());
							return;
						}
					}
					//disable for unowned land
					if(shotByPlayer) {
						Player player = (Player) shooter;
						if(!Regions.playerCanBuild(player, location, true)) {
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.DARK_RED + "[Destructor Arrow] " + ChatColor.RED + "These arrows don't work here!");
							Bukkit.getScheduler().runTask(plugin, () -> event.getEntity().remove());
							return;
						}
					}
					
					//do explosion
					if(!shotByPlayer) bloodMoon = BloodMoon.isBloodMoon;
					if(event.getHitBlock() != null || event.getHitEntity() != null) {
						Bukkit.getScheduler().runTask(plugin, () -> shooter.getWorld().playSound(location, Sound.BLOCK_SNOW_BREAK, 3, 1));
						Explosion.explode(location, bloodMoon, bloodMoon && event.getHitEntity() == null? 1 : 0, 100);
						if(bloodMoon && event.getHitEntity() != null && !event.getHitEntity().isInvulnerable())
							event.getHitEntity().setFireTicks(60);
					}
					Bukkit.getScheduler().runTask(plugin, () -> event.getEntity().remove());
				}
			}
		});
	}
	
	//halloween only:
	@EventHandler
	public void SpookySpiderDeath(EntityDeathEvent event) {
		if(event.getEntityType() == EntityType.CAVE_SPIDER && event.getEntity().getKiller() instanceof Player) {
			Player killer = event.getEntity().getKiller();
			Location location = killer.getLocation();
			
			int chance = DeadMC.PetsFile.data().getInt("Pets." + Pet.Spooky_Spider.toString() + ".Chance");
			int random = (int) (Math.random() * chance) + 1; // 1-chance (inclusive)
			if(random == chance) { //1 in X
				Pet pet = Pet.Spooky_Spider;
				if(location.getWorld().getName().contains("nether")) pet = Pet.Spooky_Nether_Spider;
				
				Pets.Obtain(killer, pet);
			}
			
		}
	}
	
	//bomber explosions
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event) {
		event.setCancelled(true);
		
		if(event.getEntity().getCustomName() != null && event.getEntity().getCustomName().contains("Blinding")) {
			ItemStack itemStack = new ItemStack(Material.SPLASH_POTION);
			PotionMeta potionMeta = (PotionMeta) itemStack.getItemMeta();

			potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 0), true);

			itemStack.setItemMeta(potionMeta);

			ThrownPotion thrownPotion = (ThrownPotion) event.getLocation().getWorld().spawnEntity(event.getLocation(), EntityType.SPLASH_POTION);
			thrownPotion.setItem(itemStack);
			return;
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			Location location = event.getLocation();
			//convert to BlockStates
			List<BlockState> states = new ArrayList<BlockState>();
			for(Block block : event.blockList()) {
				try {
					states.add(block.getState());
				} catch(RuntimeException e) {
				}
			}
			//get surrounding blocks and add if obsidian
			List<BlockState> finalStates = new ArrayList<BlockState>();
			for(BlockState state : states) {
				finalStates.add(state);
				try {
					Block block = state.getBlock().getRelative(BlockFace.NORTH);
					if(block.getType() == Material.OBSIDIAN) finalStates.add(block.getState());
				} catch(RuntimeException e) {
				}
				try {
					Block block = state.getBlock().getRelative(BlockFace.EAST);
					if(block.getType() == Material.OBSIDIAN) finalStates.add(block.getState());
				} catch(RuntimeException e) {
				}
				try {
					Block block = state.getBlock().getRelative(BlockFace.SOUTH);
					if(block.getType() == Material.OBSIDIAN) finalStates.add(block.getState());
				} catch(RuntimeException e) {
				}
				try {
					Block block = state.getBlock().getRelative(BlockFace.WEST);
					if(block.getType() == Material.OBSIDIAN) finalStates.add(block.getState());
				} catch(RuntimeException e) {
				}
				try {
					Block block = state.getBlock().getRelative(BlockFace.UP);
					if(block.getType() == Material.OBSIDIAN) finalStates.add(block.getState());
				} catch(RuntimeException e) {
				}
				try {
					Block block = state.getBlock().getRelative(BlockFace.DOWN);
					if(block.getType() == Material.OBSIDIAN) finalStates.add(block.getState());
				} catch(RuntimeException e) {
				}
			}
			Explosion.explode(location, finalStates, 3, 100);
		});
	}
	
	@EventHandler
	public void onEntityDamageEntity(EntityDamageByEntityEvent event) {
		//stop zombies damaging each other
		Entity entity = event.getEntity();
		
		if(event.getCause() == DamageCause.PROJECTILE) {
			Projectile projectile = (Projectile) event.getDamager();
			if(projectile.getShooter() instanceof Entity) {
				Entity shooter = (Entity) projectile.getShooter();
				if(isZombie(shooter) && isZombie(entity)) {
					event.setCancelled(true);
				}
			}
		}
		if(event.getCause() == DamageCause.ENTITY_EXPLOSION) {
			//creepers, ghasts dont blow up other zombies
			if(((isZombie(event.getDamager()) || entity.getType() == EntityType.GHAST) && (isZombie(entity) || entity.getType() == EntityType.RAVAGER))
			|| (event.getEntity().getCustomName() != null && event.getEntity().getCustomName().contains("Blinding"))) {
				event.setCancelled(true);
			}
		}
		
	}
	
	@EventHandler
	public void entityTargetEntityEvent(EntityTargetLivingEntityEvent event) {
		//stop entities targeting each other
		Entity entity = event.getEntity();
		
		//stop zombies targeting other zombies:
		if(isZombie(event.getEntity()) //zombie is targeting
				&& (event.getTarget() == null || (isZombie(event.getTarget()) || event.getTarget().getType() == EntityType.VILLAGER || event.getTarget().getType() == EntityType.PILLAGER))) {
			event.setCancelled(true);
			return;
		}
		
		//pets can't target anything
		if(entity.isInvulnerable()) {
			event.setCancelled(true);
			return;
		}
		
		//disable entity target with disguise/spectate
		if(event.getTarget() instanceof Player) {
			Player player = (Player) event.getTarget();
			if(isZombie(event.getEntity())) {
				if(Donator.disguised.contains(player.getName()) || Admin.spectating.contains(player.getName())) {
					event.setCancelled(true);
					return;
				}
			}
		}
		
	}
	
	public static Boolean isZombie(Entity entity) {
		return (isZombie(entity.getType())
				|| (entity.getType() == EntityType.IRON_GOLEM && DisguiseAPI.isDisguised(entity)));
	}
	public static Boolean isZombie(EntityType type) {
		return (type == EntityType.ZOMBIFIED_PIGLIN
				|| type == EntityType.ZOMBIE
				|| type == EntityType.ZOMBIE_VILLAGER
				|| type == EntityType.HUSK
				|| type == EntityType.DROWNED
				|| type == EntityType.SPIDER //climbing zombie
				|| type == EntityType.CREEPER //explosive zombie
				|| type == EntityType.SKELETON //shooting zombie
				|| type == EntityType.STRAY
				|| type == EntityType.PHANTOM //flying zombie
				|| type == EntityType.GHAST
				|| type == EntityType.CAVE_SPIDER); //halloween zombie
	}
	
	public void setRandomItemsInHand(LivingEntity zombie) {
		int randomAmount = (int) (Math.random() * 16); // 0-11 (inclusive)
		for(int count = 0; count < 1; count++) {
			if(randomAmount == 0)
				setItemInHand(zombie, new ItemStack(Material.BONE));
			if(randomAmount == 1)
				setItemInHand(zombie, new ItemStack(Material.ROTTEN_FLESH));
			if(randomAmount == 2)
				setItemInHand(zombie, new ItemStack(Material.LEVER));
			if(randomAmount == 3)
				setItemInHand(zombie, new ItemStack(Material.WOODEN_PICKAXE));
			if(randomAmount == 4)
				setItemInHand(zombie, new ItemStack(Material.WOODEN_HOE));
			if(randomAmount == 5)
				setItemInHand(zombie, new ItemStack(Material.STICK));
			if(randomAmount == 6)
				setItemInHand(zombie, new ItemStack(Material.WOODEN_AXE));
			if(randomAmount == 7)
				setItemInHand(zombie, new ItemStack(Material.STONE_SHOVEL));
			if(randomAmount == 8)
				setItemInHand(zombie, new ItemStack(Material.WOODEN_SHOVEL));
			if(randomAmount == 9)
				setItemInHand(zombie, new ItemStack(Material.LEAD));
			if(randomAmount == 10)
				setItemInHand(zombie, new ItemStack(Material.COBWEB));
		}
		
		//christmas event 2019:
		//EntityEquipment equipment = zombie.getEquipment();
		//equipment.setHelmet(new ItemStack(Material.RED_MUSHROOM_BLOCK));
		
	}
	
	@EventHandler
	public void onKillZombie(EntityDeathEvent event) {
		Entity entity = event.getEntity();
		if(isZombie(entity)) {
			event.getDrops().clear(); //don't drop anything if player didn't kill it
			
			if(entity instanceof Creeper creeper) {
				creeper.explode();
			}
			
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					//get player who last hit zombie
					if(entity.getLastDamageCause() instanceof EntityDamageByEntityEvent damageEvent) {
						
						Player playerNonFinal = null;
						//killed by projectile:
						if(damageEvent.getCause() == DamageCause.PROJECTILE) {
							Projectile projectile = (Projectile) damageEvent.getDamager();
							if(projectile.getShooter() instanceof Player) { //shooter is a player
								playerNonFinal = (Player) projectile.getShooter();
							}
						}
						//killed by player:
						if(damageEvent.getDamager() instanceof Player) {
							playerNonFinal = (Player) damageEvent.getDamager();
						}
						Player player = playerNonFinal;
						
						if(player != null) {
							
							if(BloodMoon.isBloodMoon) {
								PlayerConfig playerConfig = PlayerConfig.getConfig(player);
								
								//add total blood moon kills
								int numberOfKills = 0;
								if(playerConfig.getString("BloodMoonKills") != null)
									numberOfKills = playerConfig.getInt("BloodMoonKills");
								numberOfKills++;
								playerConfig.set("BloodMoonKills", numberOfKills);
								
								if(numberOfKills % 1000 == 0) {
									Bukkit.getScheduler().runTask(plugin, new Runnable() {
										@Override
										public void run() {
											player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1f, 8f);
										}
									});
									DecimalFormat formatter = new DecimalFormat("#,###");
									Chat.broadcastMessage(ChatColor.YELLOW + "[Achievement] " + ChatColor.AQUA + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " has killed " + ChatColor.BOLD + formatter.format(numberOfKills) + ChatColor.DARK_RED + ChatColor.BOLD + " Blood Moon" + ChatColor.WHITE + " zombies!");
								}
								
								int bloodMoonKills = 0;
								if(DeadMC.LeaderboardFile.data().getString("CurrentTotalKills") != null)
									bloodMoonKills = DeadMC.LeaderboardFile.data().getInt("CurrentTotalKills");
								DeadMC.LeaderboardFile.data().set("CurrentTotalKills", bloodMoonKills + 1);
								
								//set current kills:
								long fullTime = Bukkit.getWorld("world").getFullTime();
								long lastParticipated = 0;
								if(playerConfig.getString("BM_LastParticipated") != null) {
									lastParticipated = playerConfig.getLong("BM_LastParticipated");
								}
								
								if(Math.abs((fullTime - lastParticipated)) > 12000) {
									//reset kills
									playerConfig.set("BM_CurrentKills", 1);
									playerConfig.set("BM_LastParticipated", fullTime); //update last participated
								} else
									playerConfig.set("BM_CurrentKills", playerConfig.getInt("BM_CurrentKills") + 1);
								Bukkit.getScheduler().runTask(plugin, () -> playerConfig.save());
								
								//add placeholder data to memory
								if(!PlaceHolders.player_BMKills.containsKey(player.getUniqueId().toString()))
									PlaceHolders.player_BMKills.put(player.getUniqueId().toString(), playerConfig.getInt("BM_CurrentKills"));
								else
									PlaceHolders.player_BMKills.replace(player.getUniqueId().toString(), playerConfig.getInt("BM_CurrentKills"));
								
								Chat.updateLeaderBoard(Leaderboard.BMKILLS, player.getUniqueId());
								Chat.updateLeaderBoard(Leaderboard.CURRENTBLOODMOON, player.getUniqueId());
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.AQUA + "" + ChatColor.BOLD + "" + playerConfig.getInt("BM_CurrentKills") + ChatColor.DARK_RED + ChatColor.BOLD + " Blood Moon" + ChatColor.WHITE + " kills tonight.");
							}
							
							if(!Regions.regionIsAtLocation("longdale", player.getLocation())) { //player is not standing in spawn protection
								ItemStack itemDrop = ZombieDrop(player, (LivingEntity) entity);
								event.getDrops().clear();
								if(itemDrop != null) {
									Bukkit.getScheduler().runTask(plugin, () -> {
										try {
											player.getWorld().dropItemNaturally(entity.getLocation(), itemDrop);
										} catch (Exception e) {
											Chat.logError("Error dropping item " + itemDrop.getType()  + "" + (itemDrop.getItemMeta().hasDisplayName() ? " (" + itemDrop.getItemMeta().getDisplayName() + ")" : "") + "! " + e.getMessage());
										}
									});
								}
								
								//pets:
								if(entity.getType() == EntityType.ZOMBIE
										|| entity.getType() == EntityType.HUSK
										|| entity.getType() == EntityType.DROWNED
										|| entity.getType() == EntityType.ZOMBIE_VILLAGER) {
									String petName = entity.getType().toString().substring(0, 1) + entity.getType().toString().toLowerCase().substring(1);
									if(entity.getType() == EntityType.ZOMBIE_VILLAGER)
										petName = "Zombie_Villager";
									
									int chance = DeadMC.PetsFile.data().getInt("Pets." + petName + ".Chance");
									int random = (int) (Math.random() * chance) + 1; // 1-chance (inclusive)
									if(random == chance) { //1 in X
										Pet pet = Pet.valueOf(petName);
										Bukkit.getScheduler().runTask(plugin, new Runnable() {
											@Override
											public void run() {
												Pets.Obtain(player, pet);
											}
										});
									}
								}
								
							} else {
								player.sendMessage("");
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Zombies don't drop loot in Longdale!");
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Travel to the wilderness with " + ChatColor.GOLD + "/travel" + ".");
								player.sendMessage("");
							}
						}
						
					}
				}
			});
		}
	}
	
	private ItemStack ZombieDrop(Player player, LivingEntity entity) {
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		int numberOfKills = playerConfig.getInt("Kills") + 1;
		playerConfig.set("Kills", numberOfKills);
		Bukkit.getScheduler().runTask(plugin, () -> playerConfig.save());
		
		Boolean isTank = entity.getCustomName() != null && entity.getCustomName().equalsIgnoreCase("Tank");
		Boolean isPhantom = entity.getType() == EntityType.PHANTOM || (DisguiseAPI.isDisguised(entity) && DisguiseAPI.getDisguise(entity).getType() == DisguiseType.PHANTOM);
		Boolean isClimber = entity.getCustomName() != null && entity.getCustomName().equalsIgnoreCase("Climber");
		Boolean isDestructor = entity.getCustomName() != null && entity.getCustomName().equalsIgnoreCase("Destructor");
		Boolean isBomber = entity.getCustomName() != null && (entity.getCustomName().equalsIgnoreCase("Bomber") || entity.getCustomName().equalsIgnoreCase("Blinding Bomber"));
		Boolean isHoldingTrident = entity.getEquipment() != null && entity.getEquipment().getItemInMainHand() != null && entity.getEquipment().getItemInMainHand().getType() == Material.TRIDENT;
		
		//give coins
		int coinReward = 1;
		if(isTank) coinReward = 10;
		if(isPhantom) coinReward = 5;
		if(isClimber || isDestructor || isBomber || isHoldingTrident) coinReward = 2;
		Economy.giveCoins(player.getUniqueId(), coinReward);
		
		//total server kill count:
		FileConfiguration leaderboard = DeadMC.LeaderboardFile.data();
		int totalServerKills = leaderboard.getString("TotalServerKills") == null ? 0 : leaderboard.getInt("TotalServerKills") + 1;
		leaderboard.set("TotalServerKills", totalServerKills);
		DecimalFormat formatter = new DecimalFormat("#,###");
		PlaceHolders.totalZombieKills = formatter.format(totalServerKills);
		
		Bukkit.getScheduler().runTask(plugin, () -> DeadMC.LeaderboardFile.save());

		if(!BloodMoon.isBloodMoon) {
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GOLD + "" + ChatColor.BOLD + "" + formatter.format(numberOfKills) + ChatColor.WHITE + " zombies killed.");
		}
		
		if(numberOfKills % 1000 == 0) {
			Bukkit.getScheduler().runTask(plugin, new Runnable() {
				@Override
				public void run() {
					player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1f, 8f);
				}
			});
			Chat.broadcastMessage(ChatColor.YELLOW + "[Achievement] " + ChatColor.AQUA + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " has killed " + ChatColor.GOLD + ChatColor.BOLD + formatter.format(numberOfKills) + ChatColor.WHITE + " zombies!");
		}
		Chat.updateLeaderBoard(Leaderboard.KILLS, player.getUniqueId());
		
		ItemStack itemDrop = null;
		LootRarity rarity = null;
		
		double random = (double) (Math.random() * 1000);
		if(isClimber && random >= 998) { //1 in 500
			itemDrop = CustomItems.climbingBoots(false);
			rarity = LootRarity.SPECIAL;
		} else if(isDestructor && random >= 666) { //1 in 3
			itemDrop = CustomItems.destructorArrow(BloodMoon.isBloodMoon);
			rarity = LootRarity.SPECIAL;
		} else {
			
			double tableChance = (double) (Math.random() * 1000); // 0-999 (inclusive)
			if(tableChance >= 998) { //0.2% chance == very rare - // 997.5 - 999 = 0.25%
				//Chat.broadcastMessage("Hit: " + tableChance);
				int index = (int) (Math.random() * DeadMC.veryRareItems.size()); // 0-size (inclusive)
				itemDrop = (ItemStack) DeadMC.veryRareItems.get(index).clone();
				//Chat.broadcastMessage("" + index + " / " + DeadMC.veryRareItems.size());
				//Bukkit.getConsoleSender().sendMessage("" + DeadMC.veryRareItems);
				rarity = LootRarity.VERY_RARE;
				player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1f, 8f);
			} else if(tableChance >= 982.5) { //1.5% chance == rare
				int index = (int) (Math.random() * DeadMC.rareItems.size());
				//Chat.broadcastMessage("RARE: " + index + " / " + rareItems.size());
				itemDrop = (ItemStack) DeadMC.rareItems.get(index).clone();
				rarity = LootRarity.RARE;
			} else if(tableChance >= 882.5) { //10% chance == uncommon
				int index = (int) (Math.random() * DeadMC.uncommonItems.size());
				itemDrop = (ItemStack) DeadMC.uncommonItems.get(index).clone();
				rarity = LootRarity.UNCOMMON;
			} else if(tableChance >= 732.5) { //15% chance == common
				int index = (int) (Math.random() * DeadMC.commonItems.size());
				itemDrop = (ItemStack) DeadMC.commonItems.get(index).clone();
				rarity = LootRarity.COMMON;
			}
			
		}
		
		if(itemDrop != null) { //something dropped
			
			if(itemDrop.getItemMeta().hasLore()) rarity = LootRarity.SPECIAL;
			
			String rarityColour = "";
			if(rarity == LootRarity.UNCOMMON) rarityColour = ChatColor.YELLOW + "";
			if(rarity == LootRarity.RARE) rarityColour = ChatColor.RED + "";
			if(rarity == LootRarity.VERY_RARE) rarityColour = ChatColor.DARK_RED + "";
			if(rarity == LootRarity.SPECIAL) rarityColour = ChatColor.DARK_PURPLE + "";
			
			String itemName = itemDrop.getType().toString().toLowerCase().replace("_", " ");
			if(itemDrop.getItemMeta().getLore() != null) itemName = itemDrop.getItemMeta().getDisplayName();
			
			//send title if uncommon or better
			if(rarity != LootRarity.COMMON)
				Chat.sendTitleMessage(player.getUniqueId(), rarityColour + "[" + rarity.toString().replace("_", " ") + "] Dropped " + itemDrop.getAmount() + "x " + itemName);
			else
				Chat.sendTitleMessage(player.getUniqueId(), "Dropped " + itemDrop.getAmount() + "x " + itemName);
			//send chat message if rare or better
			if(rarity.ordinal() >= LootRarity.RARE.ordinal())
				player.sendMessage(rarityColour + "[" + rarity.toString().replace("_", " ") + "] Dropped " + itemDrop.getAmount() + "x " + itemName);
			
			return itemDrop;
		}
		
		return null;
	}
	
}
