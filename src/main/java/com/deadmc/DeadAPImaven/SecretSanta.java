package com.deadmc.DeadAPImaven;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SecretSanta implements Listener, CommandExecutor {
	private DeadMC plugin;
	
	public SecretSanta(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public Map<String, ItemStack> confirmGift = new HashMap<String, ItemStack>(); //player
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {

		Player player = null;
		if(sender instanceof Player)
			player = (Player) sender;

		if(commandLabel.equalsIgnoreCase("gift")) {

			if(args.length == 0) {
				player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Hold an item in your hand and use " + ChatColor.GOLD + "/gift <player> <note>" + ChatColor.WHITE + ".");
				return true;
			}

			ZonedDateTime chicagoTime = ZonedDateTime.now(ZoneId.of("America/Chicago"));
			ZonedDateTime christmasDay = ZonedDateTime.of(2021, 12, 25, 0, 0, 0, 0, ZoneId.of("America/Chicago"));

			if(chicagoTime.isAfter(christmasDay)) {
				player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Gifting is over for 2021.");
				
				//with tree up:
				//player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Gifting is over for 2021, but you can still access your presents at Longdale.");
				return true;
			}

			ItemStack itemInHand = player.getInventory().getItemInMainHand();

			//run asynchronously as we search all online players for nick names
			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					Player player = (Player) sender;

					String playerName = args[0];
					for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
						if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[0])) {
							//used nick name
							playerName = onlinePlayer.getName();
							break;
						}
					}
					UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();

					PlayerConfig receiverConfig = PlayerConfig.getConfig(uuid);

					if(receiverConfig.getString("Coins") != null) { //player exists

						if(itemInHand.hasItemMeta() && !itemInHand.getItemMeta().hasLore() && itemInHand.getItemMeta().hasDisplayName() && itemInHand.getItemMeta().getDisplayName().contains("Backpack")) {
							player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "You cannot gift this item.");
							return;
						}
						if(uuid.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
							player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "Maybe gift this to somebody else...");
							return;
						}
						if(itemInHand.getType() == Material.AIR) {
							player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Hold an item in your hand that you would like to give.");
							return;
						}

						if(confirmGift.containsKey(player.getName())
								&& confirmGift.get(player.getName()).getAmount() == itemInHand.getAmount() && confirmGift.get(player.getName()).getType() == itemInHand.getType()) {

							List<ItemStack> gifts = new ArrayList<ItemStack>();
							if(receiverConfig.getString("Gifts") != null)
								gifts = (List<ItemStack>) receiverConfig.get("Gifts");

							ItemStack gift = itemInHand.clone();
							if(args.length > 1) {

								int count = 1;
								String comments = "";
								while(count < args.length) {
									String space = count == (args.length - 1) ? "" : " ";
									comments += args[count] + space;
									count++;
								}

								ItemMeta meta = gift.getItemMeta();
								List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<String>();
								lore.add(ChatColor.YELLOW + "Gift note: " + ChatColor.WHITE + comments);
								meta.setLore(lore);
								gift.setItemMeta(meta);
							}

							gifts.add(gift);
							receiverConfig.set("Gifts", gifts);
							receiverConfig.save();

							player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));

							player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Gifted " + itemInHand.getAmount() + "x " + itemInHand.getType().toString().toLowerCase().replace("_", " ") + " to " + playerName + "!");

							int chance = DeadMC.PetsFile.data().getInt("Pets." + Pets.Pet.Snow_Golem.toString() + ".Chance");
							int random = (int) (Math.random() * chance) + 1; // 1-chance (inclusive)
							if(random == chance) { //1 in X
								Bukkit.getScheduler().runTask(plugin, () -> Pets.Obtain(player, Pets.Pet.Snow_Golem));
							}

						} else {

							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "================= " + ChatColor.RED + ChatColor.BOLD + "Secret" + ChatColor.GREEN + ChatColor.BOLD + " Santa" + ChatColor.YELLOW + " =================");
							player.sendMessage(ChatColor.WHITE + "Are you sure you want to gift " + ChatColor.GOLD + ChatColor.BOLD + itemInHand.getAmount() + ChatColor.GOLD + "x " + (itemInHand.getItemMeta().hasLore() ? itemInHand.getItemMeta().getDisplayName() : itemInHand.getType().toString().toLowerCase().replace("_", " ")) + ChatColor.WHITE + "?");
							if(args.length == 1) {
								player.sendMessage(ChatColor.WHITE + " > Add a note with " + ChatColor.GOLD + "/gift " + args[0] + ChatColor.BOLD + " <note>");
							}
							player.sendMessage(ChatColor.WHITE + "  > " + ChatColor.ITALIC + " Use the command again to confirm");
							player.sendMessage(ChatColor.YELLOW + "=================================================");
							player.sendMessage("");

							confirmGift.put(player.getName(), itemInHand);

						}

					} else {
						player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "'" + playerName + "' is not an existing player name.");
					}

				}
			});

		}

		return true;
	}

	@EventHandler
	public void onSantaInventoryClick(InventoryClickEvent event) {
//		if(event.getWhoClicked() instanceof Player) {
//			Player player = (Player) event.getWhoClicked();
//			Inventory inventory = event.getInventory();
//
//			if(event.getView().getTitle().contains("from DeadMC!") && (event.getClickedInventory() == event.getView().getTopInventory() || event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
//				if((event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY && event.getClickedInventory() == event.getView().getBottomInventory())
//						|| inventory.getItem(event.getRawSlot()) == null
//						|| inventory.getItem(event.getRawSlot()).getType() == Material.AIR) { //cant place items in inventory
//					event.setCancelled(true);
//				} else {
//					//remove from gifts list
//					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
//					List<ItemStack> gifts = new ArrayList<ItemStack>();
//					if(playerConfig.getString("Gifts") != null)
//						gifts = (List<ItemStack>) playerConfig.get("Gifts");
//					gifts.remove(inventory.getItem(event.getRawSlot()));
//
//					playerConfig.set("Gifts", gifts);
//					playerConfig.save();
//				}
//			}
//
//		}
	}
//
//	@EventHandler
//	public void onSantaInventoryClick(InventoryDragEvent event) {
//		if(event.getWhoClicked() instanceof Player player && event.getView().getTitle().contains("from DeadMC!")) {
//			Inventory inventory = event.getView().getTopInventory();
//
//			for(int slot : event.getRawSlots()) {
//				if(slot < 54) {
//					event.setCancelled(true);
//					return;
//				}
//			}
//		}
//	}
//
//	@EventHandler
//	private void onClickChristmasChest(PlayerInteractEvent event) {
//		Player player = event.getPlayer();
//		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
//			Block block = event.getClickedBlock();
//			if(block.getType() == Material.ENDER_CHEST) {
//				String locationCode = LocationCode.Encode(block.getLocation()).replace(".", "").replace("|", "");
//
//				//admin first:
//				if(player.isOp() && player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_AXE) {
//
//
//					if(DeadMC.SantaFile.data().getStringList("Chests").contains(locationCode)) {
//						//remove chest
//						List<String> chestLocations = DeadMC.SantaFile.data().getStringList("Chests");
//						chestLocations.remove(locationCode);
//						DeadMC.SantaFile.data().set("Chests", chestLocations);
//
//						//remove hologram
//						Bukkit.dispatchCommand(player, "hd delete " + locationCode);
//
//						player.sendMessage(ChatColor.RED + "Removed secret santa chest.");
//					} else {
//						//add chest
//						List<String> chestLocations = DeadMC.SantaFile.data().getStringList("Chests");
//						chestLocations.add(locationCode);
//						DeadMC.SantaFile.data().set("Chests", chestLocations);
//
//						//create hologram - requires HolographicExtension for placeholder support
//						Bukkit.dispatchCommand(player, "hd create " + locationCode + " &6Secret Santa!");
//						Bukkit.dispatchCommand(player, "hd addline " + locationCode + " &2/gift <receiver> <note>");
//						Bukkit.dispatchCommand(player, "hd addline " + locationCode + " %deadmc_secretsanta_presents%");
//						Bukkit.dispatchCommand(player, "hd addline " + locationCode + " %deadmc_secretsanta_time%");
//
//						player.sendMessage(ChatColor.GREEN + "Added secret santa chest.");
//					}
//					DeadMC.SantaFile.save();
//
//					event.setCancelled(true);
//					return;
//				}
//
//				//player:
//				if(DeadMC.SantaFile.data().getStringList("Chests").contains(locationCode)) {
//					//is a santa chest
//					ZonedDateTime chicagoTime = ZonedDateTime.now(ZoneId.of("America/Chicago"));
//					ZonedDateTime christmasDay = ZonedDateTime.of(2021, 12, 25, 0, 0, 0, 0, ZoneId.of("America/Chicago"));
//
//					if(chicagoTime.isAfter(christmasDay) || player.isOp()) {
//						//is past christmas
//
//						PlayerConfig playerConfig = PlayerConfig.getConfig(player);
//						List<ItemStack> gifts = new ArrayList<ItemStack>();
//						if(playerConfig.getString("Gifts") != null)
//							gifts = (List<ItemStack>) playerConfig.get("Gifts");
//
//						Inventory inventory = Bukkit.createInventory(null, 54, ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Happy " + ChatColor.DARK_RED + ChatColor.BOLD + "Holidays " + ChatColor.DARK_GRAY + ChatColor.ITALIC + "from DeadMC!");
//						//show all on first page
//						for(int count = 0; count <= 53; count++) {
//							if(count < gifts.size())
//								inventory.setItem(count, gifts.get(count).clone());
//						}
//						player.openInventory(inventory);
//
//					} else {
//						PlayerConfig receiverConfig = PlayerConfig.getConfig(player.getUniqueId());
//
//						//is before christmas eve
//						List<ItemStack> gifts = new ArrayList<ItemStack>();
//						if(receiverConfig.getString("Gifts") != null)
//							gifts = (List<ItemStack>) receiverConfig.get("Gifts");
//
//						int days = (int) ChronoUnit.DAYS.between(chicagoTime, christmasDay);
//						int dayHours = days * 24;
//						int hours = (int) ChronoUnit.HOURS.between(chicagoTime, christmasDay) - dayHours;
//
//						player.sendMessage("");
//						player.sendMessage(ChatColor.YELLOW + "================= " + ChatColor.RED + ChatColor.BOLD + "Secret" + ChatColor.GREEN + ChatColor.BOLD + " Santa" + ChatColor.YELLOW + " =================");
//						player.sendMessage(ChatColor.WHITE + "You have " + ChatColor.GOLD + ChatColor.BOLD + gifts.size() + ChatColor.WHITE + " presents waiting from your Secret Santas!");
//						player.sendMessage(ChatColor.WHITE + "You can open these in " + ChatColor.BOLD + days + " days, " + hours + " hours");
//						player.sendMessage("");
//						player.sendMessage(ChatColor.WHITE + "Be a Secret Santa and give gifts with:");
//						player.sendMessage(ChatColor.WHITE + " > " + ChatColor.GOLD + "/gift <receiver> <note>");
//						player.sendMessage(ChatColor.YELLOW + "=================================================");
//						player.sendMessage("");
//					}
//					event.setCancelled(true);
//
//				}
//
//			}
//		}
//	}
	
}
