package com.deadmc.DeadAPImaven.Towns;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.deadmc.DeadAPImaven.*;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;

import net.wesjd.anvilgui.AnvilGUI;

public class TownGUI implements Listener {
	
	// Get reference to main class
	private static DeadMC plugin;
	
	public TownGUI(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	private enum Icon {
		SET_TP,
		SET_OPEN,
		
		BACK
	}
	
	private static ItemStack getIcon(Player player, Icon icon) {
		ItemStack finalIcon = null;
		String displayName = null;
		List<String> lore = new ArrayList<String>();
		
		if(icon == Icon.BACK) {
			finalIcon = new ItemStack(Material.BARRIER);
			displayName = ChatColor.DARK_RED + "" + ChatColor.BOLD + "Cancel";
		}
		if(icon == Icon.SET_TP) {
			finalIcon = new ItemStack(Material.WRITABLE_BOOK);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Set TP";
			lore.add(ChatColor.WHITE + "Update the town travel point.");
		}
		if(icon == Icon.SET_OPEN) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			TownConfig townConfig = TownConfig.getConfig(playerConfig.getString("Town"));
			
			finalIcon = new ItemStack(townConfig.getBoolean("Travel.Public") ? Material.HOPPER_MINECART : Material.CHEST_MINECART);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Accessibility";
			if(townConfig.getBoolean("Travel.Public"))
				lore.add(ChatColor.WHITE + "Your TP is currently " + ChatColor.GREEN + "public" + ChatColor.WHITE + ".");
			else
				lore.add(ChatColor.WHITE + "Your TP is currently " + ChatColor.RED + "private" + ChatColor.WHITE + ".");
			lore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Left click to make private.");
			lore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Right click to make public.");
		}
		
		if(displayName != null || lore.size() > 0) {
			ItemMeta meta = finalIcon.getItemMeta();
			meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			meta.setDisplayName(displayName);
			meta.setLore(lore);
			finalIcon.setItemMeta(meta);
			return finalIcon;
		} else return null;
	}
	
	public static ItemStack makeIcon(Material material, String title, List<String> lore) {
		ItemStack finalIcon = new ItemStack(material);
		return makeIcon(finalIcon, title, lore);
	}
	
	//be sure to CLONE the itemstack first
	public static ItemStack makeIcon(ItemStack itemStack, String title, List<String> lore) {
		if(itemStack.getType().toString().contains("BANNER")) {
			//use the banner meta instead of normal meta:
			BannerMeta bannerMeta = (BannerMeta) itemStack.getItemMeta();
			
			bannerMeta.setDisplayName(title);
			bannerMeta.setLore(lore);
			bannerMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			itemStack.setItemMeta(bannerMeta);
			return itemStack;
		}
		ItemMeta meta = itemStack.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.setDisplayName(title);
		meta.setLore(lore);
		itemStack.setItemMeta(meta);
		return itemStack;
	}

	public static void openTownMenu(Player player) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		String originalTownName = playerConfig.getString("Town"); //can be null if not in town
		boolean isInTown = originalTownName != null;
		
		Inventory inventory = Bukkit.createInventory(null, isInTown ? 54 : 9, ChatColor.BOLD + (isInTown ? Town.getTownDisplayName(originalTownName) : "Town") + ChatColor.DARK_GRAY + (isInTown ? ": Manage" : ": Create or Join"));
		
		if(!isInTown) {
			//if not in town, show "create a town", "join a town"
			inventory.setItem(3, makeIcon(Material.WRITABLE_BOOK, ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Create " + ChatColor.YELLOW + ChatColor.BOLD + "<<", new ArrayList<String>(Arrays.asList(ChatColor.WHITE + "Create a town.", ChatColor.GRAY + " > Opens the creator menu."))));
			inventory.setItem(5, makeIcon(Material.BOOKSHELF, ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Join " + ChatColor.YELLOW + ChatColor.BOLD + "<<", new ArrayList<String>(Arrays.asList(ChatColor.WHITE + "Lookup and join existing towns.", ChatColor.GRAY + " > Opens the lookup menu."))));
		} else {
			boolean isMayor = playerConfig.getInt("TownRank") >= Town.Rank.Co_Mayor.ordinal();
			
			List<String> townsRanked = Town.getTownsRanked();
			int rank = townsRanked.indexOf(originalTownName) + 1;
			
			Town.Rank playerRank = Town.Rank.values()[playerConfig.getInt("TownRank")];
			String playerRankString = "a " + ChatColor.BOLD + "citizen" + ChatColor.WHITE + ". " + Town.getTownRankPrefix(player.getUniqueId());
			if(playerRank == Town.Rank.Mayor) playerRankString = "the " + ChatColor.BOLD + "Mayor" + ChatColor.WHITE + ". " + Town.getTownRankPrefix(player.getUniqueId());
			if(playerRank == Town.Rank.Co_Mayor) playerRankString = "a " + ChatColor.BOLD + "Co-Mayor" + ChatColor.WHITE + ". "  + Town.getTownRankPrefix(player.getUniqueId());
			if(playerRank == Town.Rank.Veteran) playerRankString = "a " + ChatColor.BOLD + "Veteran" + ChatColor.WHITE + " member. " + Town.getTownRankPrefix(player.getUniqueId());
			if(playerRank == Town.Rank.Respected) playerRankString = "a " + ChatColor.BOLD + "Respected" + ChatColor.WHITE + " member. " + Town.getTownRankPrefix(player.getUniqueId());
			
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			String reigningSinceString = DateCode.Decode(townConfig.getString("CreationDate"), DateCode.Value.DAY_OF_MONTH) + "/" + DateCode.Decode(townConfig.getString("CreationDate"), DateCode.Value.MONTH) + "/" + DateCode.Decode(townConfig.getString("CreationDate"), DateCode.Value.YEAR) + " (" + Town.getDaysOld(originalTownName) + " days old)";
			
			inventory.setItem(4, makeIcon(Town.getBanner(originalTownName).clone(), ChatColor.YELLOW + "Town rank: " + ChatColor.GOLD + ChatColor.BOLD + "#" + rank + ChatColor.WHITE + " out of " + townsRanked.size(), new ArrayList<String>(Arrays.asList(ChatColor.YELLOW + "Reigning since: " + ChatColor.WHITE + reigningSinceString, ChatColor.WHITE + "You are " + playerRankString, isMayor ? ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Click to modify the banner." : ""))));
			inventory.setItem(8, makeIcon(Material.BOOKSHELF, ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Lookup " + ChatColor.YELLOW + ChatColor.BOLD + "<<", new ArrayList<String>(Arrays.asList(ChatColor.WHITE + "Lookup other towns.", ChatColor.GRAY + " > Opens the lookup menu."))));
		}

		Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
	}
	
	public static void openTPMenu(Player player, String previousMenu, boolean openedAsync) {
		
		//TASK
		if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), Task.TaskStep.OPEN_TOWN_TRAVEL_MENU))
			TaskManager.stepTask(player.getUniqueId());
		
		Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.BOLD + "Town" + ChatColor.DARK_GRAY + ": Travel Point");
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		String originalTownName = playerConfig.getString("Town");
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		ItemStack setItem = getIcon(player, Icon.SET_TP);
		inventory.setItem(8, setItem);
		
		//only show modifying items if town has a TP:
		if(townConfig.getString("Travel.LocationCode") != null) {
			ItemStack setOpen = getIcon(player, Icon.SET_OPEN);
			
			ItemStack setFee = new ItemStack(Material.GOLD_INGOT);
			ItemMeta feemeta = setFee.getItemMeta();
			feemeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Set Fee");
			List<String> feelore = feemeta.getLore() == null ? new ArrayList<String>() : feemeta.getLore();
			feelore.add(ChatColor.WHITE + "Set the fee to travel.");
			boolean hasFee = townConfig.getString("Travel.Fee") != null && townConfig.getInt("Travel.Fee") > 0;
			if(hasFee) {
				feelore.add(ChatColor.GRAY + " > Current: " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Travel.Fee")));
				feelore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Right click to remove.");
			}
			feemeta.setLore(feelore);
			setFee.setItemMeta(feemeta);
			
			ItemStack setBanner = Town.getBanner(originalTownName).clone();
			ItemMeta bannerMeta = setBanner.getItemMeta();
			bannerMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Set Banner");
			List<String> bannerLore = new ArrayList<String>();
			bannerLore.add(ChatColor.WHITE + "Set the town's banner.");
			bannerLore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " To update the banner, use:");
			bannerLore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GOLD + " /town set banner");
			bannerMeta.setLore(bannerLore);
			setBanner.setItemMeta(bannerMeta);
			
			inventory.setItem(3, setOpen);
			inventory.setItem(4, setBanner);
			inventory.setItem(5, setFee);
		}
		
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = backmeta.getLore() == null ? new ArrayList<String>() : backmeta.getLore();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + previousMenu + ChatColor.GRAY + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		if(openedAsync) {
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
		} else {
			player.openInventory(inventory);
		}
	}
	
	public static void openSetTravelFeeMenu(Player player) {
		
		//if opening anvil menu, set player XP to 0 first or they can take the item.
		if(TravelGUI.resetXP.containsKey(player.getName())) TravelGUI.resetXP.remove(player.getName());
		TravelGUI.resetXP.put(player.getName(), player.getLevel());
		player.setLevel(0);
		
		new AnvilGUI.Builder()
				.onClose(p3 -> {
					//give original XP back
					if(TravelGUI.resetXP.containsKey(player.getName())) {
						player.setLevel(TravelGUI.resetXP.get(player.getName()));
						TravelGUI.resetXP.remove(player.getName());
					}
				})
				
				.onComplete((p3, fee) -> { //called when the inventory output slot is clicked
					if(!Chat.isInteger(fee) || Integer.parseInt(fee) < 0) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Enter a positive number.");
						return AnvilGUI.Response.text("Enter number.");
					}
					
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					TownConfig townConfig = TownConfig.getConfig(playerConfig.getString("Town"));
					int coins = Integer.parseInt(fee);
					
					if(coins == 0) townConfig.set("Travel.Fee", null);
					else townConfig.set("Travel.Fee", coins);
					townConfig.save();
					
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Travel fee set to " + ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.GREEN + ".");
					
					openTPMenu(player, "Quick Menu", false);
					
					return AnvilGUI.Response.close();
				})
				.text("0") //starting text
				.itemLeft(new ItemStack(Material.NAME_TAG))
				.onLeftInputClick(p3 -> Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Click the output slot to continue."))
				
				.title(ChatColor.DARK_GRAY + "Set: " + ChatColor.RESET + ChatColor.BOLD + "Fee" + ChatColor.RESET + " ?") //set the title of the GUI
				.plugin(plugin)
				.open(player);
	}
	
	private void openCreateTownMenu(Player player) {
		
		//if opening anvil menu, set player XP to 0 first or they can take the item.
		if(TravelGUI.resetXP.containsKey(player.getName())) TravelGUI.resetXP.remove(player.getName());
		TravelGUI.resetXP.put(player.getName(), player.getLevel());
		player.setLevel(0);
		
		new AnvilGUI.Builder()
				.onClose(p3 -> {
					//give original XP back
					if(TravelGUI.resetXP.containsKey(player.getName())) {
						player.setLevel(TravelGUI.resetXP.get(player.getName()));
						TravelGUI.resetXP.remove(player.getName());
					}
				})
				
				.onComplete((p3, name) -> { //called when the inventory output slot is clicked
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					if(playerConfig.getString("Town") != null) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.RED + "[Town] You are already in a town!");
						return AnvilGUI.Response.close();
					}
					
					if(name.contains(" ")) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Cannot contain spaces.");
						return AnvilGUI.Response.text("Contains spaces!");
					}
					
					try {
						ProtectedCuboidRegion region = new ProtectedCuboidRegion(name, BlockVector3.ONE, BlockVector3.ONE);
					} catch(IllegalArgumentException e) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Cannot contain special characters.");
						return AnvilGUI.Response.text("Invalid characters!");
					}
					
					if(name.contains("fuck")
							|| name.contains("cunt")
							|| name.contains("nigger")
							|| name.contains("nigga")
							|| name.contains("faggot")) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Name cannot contain inappropriate language.");
						return AnvilGUI.Response.text("Inappropriate.");
					}
					
					String nonCased = name.toLowerCase();
					if(Town.townNameIsTaken(name) || Travel.publicNameIsTaken(name)) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + ChatColor.RED + "That name is already taken.");
						return AnvilGUI.Response.text("Name taken.");
					}
					
					if(name.length() > 17) {
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Town name cannot be more than 17 characters.");
						return AnvilGUI.Response.text("Too long.");
					}
					
					//check it doesn't equal any online players names
					for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						if(onlinePlayer.getName().equalsIgnoreCase(name)) {
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "The town name must be unique. Pick another name.");
							return AnvilGUI.Response.text("Pick again.");
						}
					}
					
					Town.create(player.getUniqueId(), name);
					
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Successfully created " + name + ".");
					
					//TASK
					if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), Task.TaskStep.CREATE_A_TOWN))
						TaskManager.stepTask(player.getUniqueId());
					
					//update placeholder
					PlaceHolders.player_TownString1.remove(player.getUniqueId().toString());
					PlaceHolders.player_TownString2.remove(player.getUniqueId().toString());
					PlaceHolders.player_TownRank.remove(player.getUniqueId().toString());
					PlaceHolders.player_TownName.remove(player.getUniqueId().toString());
					
					int totalTowns = DeadMC.TownFile.data().getStringList("Active").size();
					DecimalFormat formatter = new DecimalFormat("#,###");
					PlaceHolders.totalTowns = formatter.format(totalTowns);
					
					//update leaderboard prefixs
					Chat.updateUserLeaderboardPrefix(player.getUniqueId());
					
					openTownMenu(player);
					
					return AnvilGUI.Response.close();
				})
				.text("Longdale") //starting text
				.itemLeft(new ItemStack(Material.NAME_TAG))
				.onLeftInputClick(p3 -> Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Click the output slot to continue."))
				
				.title(ChatColor.DARK_GRAY + "Set town name?") //set the title of the GUI
				.plugin(plugin)
				.open(player);
	}
	
	@EventHandler
	private void townGUIclick(InventoryClickEvent event) {
		if(event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();

			if(event.getView().getTitle().contains("Town")) {
				
				if(event.getCurrentItem() == null || event.getCurrentItem().getItemMeta() == null || event.getCurrentItem().getItemMeta().getLore() == null || event.getCurrentItem().getItemMeta().getLore().size() == 0 || !event.getCurrentItem().getItemMeta().hasDisplayName()) {
					//didn't click a custom item
					event.setCancelled(true);
					return;
				}
				
				Inventory inventory = event.getClickedInventory();
				String name = event.getCurrentItem().getItemMeta().getDisplayName();
				
				//per item here:
				
				if(name.toLowerCase().contains("cancel")) {
					String description = event.getCurrentItem().getItemMeta().getLore().get(0);
					
					if(description.toLowerCase().contains("quick menu")) {
						TravelGUI.openQuickMenu(player);
					}
					if(description.toLowerCase().contains("town menu")) {
						openTownMenu(player);
					}
				}
				
				if(event.getView().getTitle().contains("Create or Join")) {
					if(name.contains("Create")) {
						openCreateTownMenu(player);
					}
				}
				
				if(event.getView().getTitle().contains("Travel Point")) {
					if(name.contains("Set Banner")) {
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "To update the town banner, use:");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GOLD + "/town set banner");
						player.sendMessage("");
					}
					if(name.contains("Set TP")) {
						RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
						if(regionManager.getApplicableRegions(BlockVector3.at(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())).size() > 0
								&& !Regions.playerCanBuild(player, player.getLocation(), true)) {
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "You do not have rights to this land.");
							if(!Tools.playerIsJava(player)) {
								Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
							}
							return;
						}
						
						TravelGUI.openConfirmMenu(player, TravelGUI.Icon.Confirm_set_town_point, "Manage TP", true);
					}
					if(name.contains("Set Fee")) {
						
						if(event.isLeftClick()) {
							openSetTravelFeeMenu(player);
						}
						
						if(event.isRightClick()) {
							PlayerConfig playerConfig = PlayerConfig.getConfig(player);
							TownConfig townConfig = TownConfig.getConfig(playerConfig.getString("Town"));
							
							if(townConfig.getString("Travel.Fee") != null) {
								townConfig.set("Travel.Fee", null);
								townConfig.save();
								
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Travel fee removed!");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Travel fee removed!");
								
								//just update the fee item
								ItemStack setFee = event.getCurrentItem();
								ItemMeta feemeta = setFee.getItemMeta();
								List<String> feelore = new ArrayList<String>();
								feelore.add(ChatColor.WHITE + "Set the fee to travel.");
								feemeta.setLore(feelore);
								setFee.setItemMeta(feemeta);
							}
						}
					}
					if(name.contains("Accessibility")) {
						PlayerConfig playerConfig = PlayerConfig.getConfig(player);
						TownConfig townConfig = TownConfig.getConfig(playerConfig.getString("Town"));
						if(townConfig.getString("Travel.Public") == null || townConfig.getBoolean("Travel.Public") != event.isRightClick()) {
							townConfig.set("Travel.Public", event.isRightClick());
							townConfig.save();
							
							//update item:
							ItemStack setOpen = getIcon(player, Icon.SET_OPEN);
							inventory.setItem(3, setOpen);
							
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + (event.isRightClick() ? ChatColor.GREEN : ChatColor.RED) + "Set to " + ChatColor.BOLD + (event.isRightClick() ? "public" : "private") + ".");
							
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Set the travel point to " + ChatColor.BOLD + (event.isRightClick() ? "public" : "private") + ".");
							player.sendMessage(event.isRightClick() ? ChatColor.DARK_RED + "[WARNING] " + ChatColor.RED + "Any player can access this travel point." : ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Only members of " + Town.getTownDisplayName(playerConfig.getString("Town") + " can use the travel point."));
							player.sendMessage("");
						}
					}
				}
				
				event.setCancelled(true);
			}
			
		}
	}
	
	@EventHandler
	public void travelGUIdrag(InventoryDragEvent event) {
		if(event.getWhoClicked() instanceof Player) {
			
			if(event.getView().getTitle().contains("Town")) {
				event.setCancelled(true);
			}
			
		}
	}
	
}
