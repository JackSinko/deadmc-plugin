package com.deadmc.DeadAPImaven.Towns;

import com.deadmc.DeadAPImaven.*;
import com.deadmc.DeadAPImaven.Task.TaskStep;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.Permission;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Role;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import github.scarsz.discordsrv.util.DiscordUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.awt.*;
import java.io.File;
import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.*;

public class Town implements CommandExecutor {
	private static DeadMC plugin;
	
	public Town(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	List<String> confirmLeave = new ArrayList<String>();
	Map<String, String> confirmNameChange = new HashMap<String, String>();
	List<String> confirmMayor = new ArrayList<String>();
	Map<String, String> invites = new HashMap<String, String>(); // playerName, townName
	
	public enum RecruitStatus {
		OPEN,
		INVITE_ONLY,
		CLOSED
	}
	
	public enum Rank {
		Citizen,
		Respected,
		Veteran,
		Co_Mayor,
		Mayor,
	}
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		final Player player = (Player) sender;
		
		try {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		if(commandLabel.equalsIgnoreCase("town") || commandLabel.equalsIgnoreCase("t") || commandLabel.equalsIgnoreCase("towns") || commandLabel.equalsIgnoreCase("factions") || commandLabel.equalsIgnoreCase("towny")) {
			
			if(player.isOp()) {
				Chat.sendTitleMessage(player.getUniqueId(), "Opening for OP only, in dev mode.");

				if(args.length == 0) {
					Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> TownGUI.openTownMenu(player));
				}

			}
			
			if(args.length == 0) {
				
				//TASK
				if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.USE_TOWN_COMMAND))
					TaskManager.stepTask(player.getUniqueId());
					
				// /town
				
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + "================== Town Commands ==================");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Create or Join a town:");
				player.sendMessage(ChatColor.GOLD + "/town create" + ChatColor.WHITE + " - Create a town.");
				player.sendMessage(ChatColor.GOLD + "/town join" + ChatColor.WHITE + " - Join a town.");
				player.sendMessage(ChatColor.GOLD + "/town list" + ChatColor.WHITE + " - A ranking list of all towns.");
				player.sendMessage(ChatColor.GOLD + "/town lookup <town>" + ChatColor.WHITE + " - Lookup town stats.");
				
				if(playerConfig.getString("Town") != null) {
					String townDisplayName = getTownDisplayName(player.getUniqueId());
					
					player.sendMessage("");
					player.sendMessage(ChatColor.AQUA + "You are a " + Rank.values()[playerConfig.getInt("TownRank")].toString().replace("_", " ") + " of " + townDisplayName + ".");
					player.sendMessage(ChatColor.GOLD + "/town leave" + ChatColor.WHITE + " - Leave " + townDisplayName + ".");
					player.sendMessage(ChatColor.GOLD + "/tc <message>" + ChatColor.WHITE + " - Town chat.");
					if(playerConfig.getInt("TownRank") >= Rank.Mayor.ordinal()) {
						player.sendMessage(ChatColor.GOLD + "/town set name" + ChatColor.WHITE + " - Rename " + townDisplayName + " for 1000 coins.");
						player.sendMessage(ChatColor.GOLD + "/town set description" + ChatColor.WHITE + " - A brief description of your town.");
						player.sendMessage(ChatColor.GOLD + "/town set banner" + ChatColor.WHITE + " - Assign a custom town banner.");
					}
					player.sendMessage("");
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Economy:");
					player.sendMessage(ChatColor.GOLD + "/town funds add" + ChatColor.WHITE + " - Deposit funds to " + townDisplayName + ".");
					if(playerConfig.getInt("TownRank") >= Rank.Mayor.ordinal()) {
						player.sendMessage(ChatColor.GOLD + "/town funds withdraw" + ChatColor.WHITE + " - Withdraw funds from " + townDisplayName + ".");
						player.sendMessage(ChatColor.GOLD + "/town set tax" + ChatColor.WHITE + " - Set a tax for members to pay every 12 hours.");
					}
					if(playerConfig.getInt("TownRank") >= Rank.Veteran.ordinal()) {
						player.sendMessage("");
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Members:");
					}
					if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal())
						player.sendMessage(ChatColor.GOLD + "/town upgrade member-limit" + ChatColor.WHITE + " - Upgrade the member limit.");
					if(playerConfig.getInt("TownRank") >= Rank.Mayor.ordinal())
						player.sendMessage(ChatColor.GOLD + "/town set recruiting" + ChatColor.WHITE + " - Manage how players join " + townDisplayName + ".");
					if(playerConfig.getInt("TownRank") >= Rank.Veteran.ordinal())
						player.sendMessage(ChatColor.GOLD + "/town invite" + ChatColor.WHITE + " - Send join invitation to players.");
					if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal())
						player.sendMessage(ChatColor.GOLD + "/town banish" + ChatColor.WHITE + " - Kick a player from " + townDisplayName + ".");
					if(playerConfig.getInt("TownRank") >= Rank.Mayor.ordinal())
						player.sendMessage(ChatColor.GOLD + "/town rank" + ChatColor.WHITE + " - Rank members of " + townDisplayName + ".");
					if(playerConfig.getInt("TownRank") >= Rank.Mayor.ordinal()) {
						player.sendMessage("");
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Travel:");
						player.sendMessage(ChatColor.GOLD + "/town set travel-fee" + ChatColor.WHITE + " - Set a fee for travelling to " + townDisplayName + ".");
						player.sendMessage(ChatColor.GOLD + "/town set travel-point" + ChatColor.WHITE + " - Set the town travel location.");
					}
					player.sendMessage("");
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Land:");
					player.sendMessage(ChatColor.GOLD + "/land sell" + ChatColor.WHITE + " - Sell land to town members.");
					if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal())
						player.sendMessage(ChatColor.GOLD + "/town upgrade claim-limit" + ChatColor.WHITE + " - Upgrade the claim limit.");
					player.sendMessage(ChatColor.AQUA + "For land management commands, use:");
					player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "  /land help");
				}
				player.sendMessage(ChatColor.YELLOW + "==================================================");
				player.sendMessage("");
				
				//TASK
				//if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.USE_TOWN_COMMAND)) TaskManager.stepTask(player.getUniqueId());
				
				return true;
			}
			
			if(args[0].equalsIgnoreCase("list")) {
				
				Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
					@Override
					public void run() {
						
						int townsPerPage = 10;
						int pageNumber = 0;
						if(args.length > 1 && Chat.isInteger(args[1])) {
							pageNumber = Integer.parseInt(args[1]) - 1;
						} else {
							if(args.length > 1) {
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town list <page number>");
								return;
							}
						}
						
						List<String> townsRanked = getTownsRanked();
						
						int maxTownToInclude = (townsPerPage + (pageNumber * townsPerPage)) - 1; //start at page 0
						int minTownToInclude = pageNumber * townsPerPage;
						//0 - 9 
						// 10 - 19
						
						int numberOfPages = (int) Math.ceil(new Double(townsRanked.size() / townsPerPage));
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder("List of active towns", 46) + " List of active towns " + Chat.getTitlePlaceholder("List of active towns", 46));
						player.sendMessage(ChatColor.WHITE + "There are " + ChatColor.GOLD + ChatColor.BOLD + townsRanked.size() + ChatColor.WHITE + " active DeadMC towns!");
						player.sendMessage("");
						if(pageNumber > numberOfPages)
							player.sendMessage(ChatColor.RED + "   Page #" + (pageNumber + 1) + " does not exist.");
						int rank = 1;
						for(int count = minTownToInclude; count <= maxTownToInclude; count++) {
							if(townsRanked.size() <= count) break;
							String originalTownName = townsRanked.get(count);
							int actualRank = rank + (pageNumber * townsPerPage);
							
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							List<String> members = townConfig.getStringList("Members");
							
							String recruitStatus = ChatColor.GREEN + "OPEN";
							RecruitStatus status = RecruitStatus.values()[townConfig.getInt("Recruiting")];
							if(status == RecruitStatus.OPEN
									&& (townConfig.getInt("Limit.Members") - townConfig.getStringList("Members").size()) == 0)
								recruitStatus = ChatColor.RED + "MAX MEMBERS";
							if(status == RecruitStatus.CLOSED)
								recruitStatus = ChatColor.RED + "CLOSED";
							if(status == RecruitStatus.INVITE_ONLY)
								recruitStatus = ChatColor.YELLOW + "INVITE-ONLY";
							
							String townDisplayName = getTownDisplayName(originalTownName);
							
							player.sendMessage("" + ChatColor.WHITE + ChatColor.BOLD + actualRank + ChatColor.WHITE + ". " + ChatColor.GOLD + ChatColor.BOLD + townDisplayName + ChatColor.WHITE + " | " + ChatColor.BOLD + getDaysOld(originalTownName) + ChatColor.WHITE + " days old | " + recruitStatus + ChatColor.WHITE + " | " + members.size() + ChatColor.WHITE + "/" + townConfig.getInt("Limit.Members") + " members");
							rank++;
						}
						player.sendMessage("");
						player.sendMessage(ChatColor.WHITE + "Displaying page " + (pageNumber + 1) + ChatColor.WHITE + " of " + (numberOfPages + 1) + ".");
						player.sendMessage(ChatColor.WHITE + "Lookup a town with: " + ChatColor.GOLD + "/town lookup <town>");
						player.sendMessage(ChatColor.YELLOW + "==============================================");
						player.sendMessage("");
					}
				});
				
			} else if(args[0].equalsIgnoreCase("create")) {
				if(args.length == 1) {
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town create <town name>");
					return true;
				} else if(args.length > 2) {
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "The town name cannot contain spaces. Try a name like: " + ChatColor.GOLD + "/town create MyTown");
					return true;
				}
					
					if(playerConfig.getString("Town") == null) {
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
							@Override
							public void run() {
								
								String townName = args[1];
								String nonCased = townName.toLowerCase();
								if(!townNameIsTaken(townName) && !Travel.publicNameIsTaken(townName)) {
									if(townName.length() <= 17) {
										
										//if player has selection
										try {
											ProtectedCuboidRegion region = new ProtectedCuboidRegion(townName, BlockVector3.ONE, BlockVector3.ONE);
										} catch(IllegalArgumentException e) {
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "The town name cannot contain special characters.");
											return;
										}
											
											if(!nonCased.contains("fuck")
													&& !nonCased.contains("cunt")
													&& !nonCased.contains("nigger")
													&& !nonCased.contains("nigga")
													&& !nonCased.contains("faggot")) {
												
												//check it doesn't equal any online players names
												for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
													if(onlinePlayer.getName().equalsIgnoreCase(townName)) {
														player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "The town name must be unique. Pick another name.");
														return;
													}
												}
												
												Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
													@Override
													public void run() {
														Town.create(player.getUniqueId(), townName);
														
														player.sendMessage("");
														player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Successfully created " + townName + ".");
														player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town" + ChatColor.WHITE + " for a list of commands.");
														player.sendMessage("");
														
														//TASK
														if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.CREATE_A_TOWN))
															TaskManager.stepTask(player.getUniqueId());
														
														//update placeholder
														PlaceHolders.player_TownString1.remove(player.getUniqueId().toString());
														PlaceHolders.player_TownString2.remove(player.getUniqueId().toString());
														PlaceHolders.player_TownRank.remove(player.getUniqueId().toString());
														PlaceHolders.player_TownName.remove(player.getUniqueId().toString());
														
														int totalTowns = DeadMC.TownFile.data().getStringList("Active").size();
														DecimalFormat formatter = new DecimalFormat("#,###");
														PlaceHolders.totalTowns = formatter.format(totalTowns);
														
														//update leaderboard prefixs
														Chat.updateUserLeaderboardPrefix(player.getUniqueId());
														
													}
												}, 0L);
											} else
												player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "The town name cannot contain offensive language.");
									} else
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Town name cannot be more than 17 characters.");
								} else
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "The name '" + townName + "' has been claimed. Try another name.");
								
							}
						});
					} else {
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "You can't create a town if you are a member of one!");
						//TASK
						if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.CREATE_A_TOWN))
							TaskManager.stepTask(player.getUniqueId());
					}

			} else if(args[0].equalsIgnoreCase("join")) {
				if(args.length == 2) {
					
					if(playerConfig.getString("Town") == null) {
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
							@Override
							public void run() {
								if(DeadMC.TownFile.data().getStringList("Active").contains(getOriginalTownName(townNameCased(args[1])))) {
									String originalTownName = getOriginalTownName(townNameCased(args[1]));
									String townDisplayName = getTownDisplayName(originalTownName);
									TownConfig townConfig = TownConfig.getConfig(originalTownName);
									
									RecruitStatus recruitStatus = RecruitStatus.values()[townConfig.getInt("Recruiting")];
									
									//if town is open OR player has invitation
									if(recruitStatus == RecruitStatus.OPEN
											|| (recruitStatus == RecruitStatus.INVITE_ONLY && (invites.containsKey(player.getName()) && invites.get(player.getName()).equals(originalTownName)))) {
										
										if(townConfig.getStringList("Members").size() < townConfig.getInt("Limit.Members")) {
											
											if(invites.containsKey(player.getName()))
												invites.remove(player.getName());
											
											Town.sendMessage(originalTownName, ChatColor.GREEN + "Welcome " + townDisplayName + "'s newest member: " + ChatColor.BOLD + player.getName(), false, false);
											Town.sendMessage(originalTownName, ChatColor.GREEN + "" + ChatColor.BOLD + player.getName() + ChatColor.GREEN + " joined " + townDisplayName + "!", false, true);
											
											Town.addMember(player.getUniqueId(), originalTownName);
											
											player.sendMessage("");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "You have joined " + townDisplayName + "!");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use the " + ChatColor.AQUA + "Charter Boat" + ChatColor.WHITE + " at " + ChatColor.GOLD + "/longdale" + ChatColor.WHITE + " to visit your new town.");
											player.sendMessage("");
											
											//update placeholder
											PlaceHolders.player_TownString1.remove(player.getUniqueId().toString());
											PlaceHolders.player_TownString2.remove(player.getUniqueId().toString());
											PlaceHolders.player_TownRank.remove(player.getUniqueId().toString());
											PlaceHolders.player_TownName.remove(player.getUniqueId().toString());
											//update leaderboard prefixs
											Chat.updateUserLeaderboardPrefix(player.getUniqueId());
											
										} else
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + townDisplayName + " needs to upgrade their member limit.");
										
									} else
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + townDisplayName + " are not recruiting at this time.");
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "There is no town named '" + args[1] + "'.");
								
							}
						});
						
					} else
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "You can't join a town if you are a member of one!");
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town join <town name>");
				
			} else if(args[0].equalsIgnoreCase("leave")) {
				if(args.length == 1) {
					
					if(playerConfig.getString("Town") != null) {
						String originalTownName = playerConfig.getString("Town");
						String townDisplayName = getTownDisplayName(player.getUniqueId());
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						
						if(Rank.values()[playerConfig.getInt("TownRank")] != Rank.Mayor) {
							
							//just 'another' member in the town
							
							if(confirmLeave.contains(player.getName())) {
								Town.removeMember(player.getUniqueId());
								
								Town.sendMessage(originalTownName, ChatColor.WHITE + "" + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " has left " + townDisplayName + ".", false, false);
								Town.sendMessage(originalTownName, ChatColor.WHITE + "" + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " left " + townDisplayName + ".", false, true);
								
								confirmLeave.remove(player.getName());
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You have left " + townDisplayName + ".");
								
								//update placeholder
								PlaceHolders.player_TownString1.remove(player.getUniqueId().toString());
								PlaceHolders.player_TownString2.remove(player.getUniqueId().toString());
								PlaceHolders.player_TownRank.remove(player.getUniqueId().toString());
								PlaceHolders.player_TownName.remove(player.getUniqueId().toString());
								//update leaderboard prefixs
								Chat.updateUserLeaderboardPrefix(player.getUniqueId());
								
							} else {
								
								confirmLeave.add(player.getName());
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Are you sure you want to leave " + townDisplayName + "?");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/town leave" + ChatColor.WHITE + " again to confirm.");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Confirmation session will expire in 15 seconds");
								player.sendMessage("");
								
								Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
									@Override
									public void run() {
										Player player = null;
										if((sender instanceof Player)) {
											player = (Player) sender;
										}
										
										if(confirmLeave.contains(player.getName())) {
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Confirmation session expired.");
											confirmLeave.remove(player.getName());
										}
										
									}
								}, 300L);
								
							}
							
							
						} else if(Rank.values()[playerConfig.getInt("TownRank")] == Rank.Mayor
								&& townConfig.getStringList("Members").size() == 1) {
							
							//last member in town, disband
							
							if(confirmLeave.contains(player.getName())) {
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + townDisplayName + " has been disbanded!");
								Town.disband(originalTownName);
								confirmLeave.remove(player.getName());
								
								//update placeholder
								PlaceHolders.player_TownString1.remove(player.getUniqueId().toString());
								PlaceHolders.player_TownString2.remove(player.getUniqueId().toString());
								PlaceHolders.player_TownRank.remove(player.getUniqueId().toString());
								PlaceHolders.player_TownName.remove(player.getUniqueId().toString());
								//update leaderboard prefixs
								Chat.updateUserLeaderboardPrefix(player.getUniqueId());
								
							} else {
								
								confirmLeave.add(player.getName());
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Are you sure you want to disband " + townDisplayName + "?");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/town leave" + ChatColor.WHITE + " again to confirm.");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Confirmation session will expire in 15 seconds");
								player.sendMessage("");
								
								Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
									@Override
									public void run() {
										Player player = null;
										if((sender instanceof Player)) {
											player = (Player) sender;
										}
										
										if(confirmLeave.contains(player.getName())) {
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Confirmation session expired.");
											confirmLeave.remove(player.getName());
										}
										
									}
								}, 300L);
								
							}
							
						} else {
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "The mayor cannot abandon their town members!");
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Give mayorship to a member before leaving:");
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GOLD + "/town rank <player> mayor");
							player.sendMessage("");
						}
						
					} else
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You are not a member of a town.");
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town leave");
				
			} else if(args[0].equalsIgnoreCase("invite")) {
				if(args.length == 2 && (args[1].equalsIgnoreCase("accept") || args[1].equalsIgnoreCase("decline"))) {
					
					if(invites.containsKey(player.getName())) {
						
						if(args[1].equalsIgnoreCase("accept")) {
							
							Bukkit.getServer().dispatchCommand(player, "town join " + invites.get(player.getName()));
							
						}
						
						if(args[1].equalsIgnoreCase("decline")) {
							
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Invitation from " + invites.get(player.getName()) + " declined.");
							invites.remove(player.getName());
							
						}
						
					} else
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "There are no active invitations to respond to.");
					
				} else if(playerConfig.getString("Town") != null && playerConfig.getInt("TownRank") >= Rank.Veteran.ordinal()) {
					String originalTownName = playerConfig.getString("Town");
					String townDisplayName = getTownDisplayName(player.getUniqueId());
					TownConfig townConfig = TownConfig.getConfig(originalTownName);
					
					if(args.length == 2) {
						
						String invitee = args[1];
						UUID inviteeUUID = Bukkit.getOfflinePlayer(invitee).getUniqueId();
						Player playerToInvite = Bukkit.getPlayer(invitee);
						
						if(RecruitStatus.values()[townConfig.getInt("Recruiting")] != RecruitStatus.CLOSED) {
							if(DeadMC.PlayerFile.data().getStringList("Active").contains(inviteeUUID.toString())) {
								if(PlayerConfig.getConfig(inviteeUUID).getString("Town") == null) {
									if(invites.get(invitee) == null) {
										if(playerToInvite != null) {
											
											invites.put(invitee, originalTownName);
											
											player.sendMessage("");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Sent join invitation to " + playerToInvite.getName() + ".");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "They have 15 minutes to accept or decline.");
											player.sendMessage("");
											
											playerToInvite.sendMessage("");
											playerToInvite.sendMessage(ChatColor.YELLOW + "========== " + ChatColor.BOLD + "Invitation to join town" + ChatColor.YELLOW + " ==========");
											playerToInvite.sendMessage(ChatColor.GREEN + "The town '" + ChatColor.BOLD + townDisplayName + ChatColor.GREEN + "' has sent you an invitation to join!");
											playerToInvite.sendMessage(ChatColor.WHITE + "Lookup the town's info with " + ChatColor.GOLD + "/town lookup " + townDisplayName + ChatColor.WHITE + ".");
											playerToInvite.sendMessage(ChatColor.WHITE + "To accept, use: " + ChatColor.GOLD + "/town invite " + ChatColor.GREEN + "accept");
											playerToInvite.sendMessage(ChatColor.WHITE + "To decline, use: " + ChatColor.GOLD + "/town invite " + ChatColor.RED + "decline");
											playerToInvite.sendMessage(ChatColor.YELLOW + "===========================================");
											playerToInvite.sendMessage("");
											
											Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
												@Override
												public void run() {
													Player player = null;
													if((sender instanceof Player)) {
														player = (Player) sender;
													}
													
													if(invites.containsKey(player.getName())) {
														if(playerToInvite != null)
															playerToInvite.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Invitation from " + invites.get(invitee) + " expired.");
														if(player != null)
															player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Invitation to " + invitee + " expired.");
														invites.remove(invitee);
													}
													
												}
											}, 18000L); // 15 minutes
											
										} else
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + invitee + " must be online to receive an invite.");
									} else
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + invitee + " has an open invitation from a town. They must first accept or decline the invite before receiving others.");
								} else
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + invitee + " is already a member of a town.");
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "'" + invitee + "' is not a valid player.");
						} else
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Cannot invite players as " + townDisplayName + "'s recruitment status is closed.");
					} else
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town invite <player>");
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You must be a town veteran or higher to do this.");
				
			} else if(args[0].equalsIgnoreCase("rank")) {
				if(playerConfig.getString("Town") != null
						&& playerConfig.getInt("TownRank") == Rank.Mayor.ordinal()) {
					String originalTownName = playerConfig.getString("Town");
					String townDisplayName = getTownDisplayName(player.getUniqueId());
					TownConfig townConfig = TownConfig.getConfig(originalTownName);
					
					if(args.length == 3) {
						
						String rankee = args[1];
						UUID rankeeUUID = Bukkit.getOfflinePlayer(rankee).getUniqueId();
						
						String rank = args[2].toLowerCase();
						
						if(townConfig.getStringList("Members").contains(rankeeUUID.toString())) {
							
							if(!rankee.equalsIgnoreCase(player.getName())) {
								
								if(rank.equalsIgnoreCase("citizen"))
									setRank(rankeeUUID, Rank.Citizen);
								else if(rank.equalsIgnoreCase("respected"))
									setRank(rankeeUUID, Rank.Respected);
								else if(rank.equalsIgnoreCase("veteran"))
									setRank(rankeeUUID, Rank.Veteran);
								else if(rank.equalsIgnoreCase("co-mayor"))
									setRank(rankeeUUID, Rank.Co_Mayor);
								else if(rank.equalsIgnoreCase("mayor")) {
									
									if(confirmMayor.contains(player.getName())) {
										setRank(rankeeUUID, Rank.Mayor);
										confirmMayor.remove(player.getName());
									} else {
										confirmMayor.add(player.getName());
										player.sendMessage("");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Are you sure you want to make " + rankee + " mayor of " + townDisplayName + "?");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/town rank " + rankee + " mayor" + ChatColor.WHITE + " again to confirm.");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Confirmation session will expire in 15 seconds");
										player.sendMessage("");
										
										Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
											@Override
											public void run() {
												Player player = null;
												if((sender instanceof Player)) {
													player = (Player) sender;
												}
												
												if(confirmMayor.contains(player.getName())) {
													player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Confirmation session expired.");
													confirmMayor.remove(player.getName());
												}
												
											}
										}, 300L);
									}
									
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + rank + ChatColor.WHITE + " is not a rank!");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Ranking options:");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Citizen");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Respected");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Veteran");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Co-Mayor");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Mayor");
									player.sendMessage("");
								}
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "You can't change your own rank.");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + rankee + ChatColor.WHITE + " is not a member of your town.");
						
					} else {
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town rank <player> " + ChatColor.AQUA + "<rank>");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Ranking options:");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Citizen" + ChatColor.WHITE + " (default)");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Respected" + ChatColor.WHITE + " (cosmetic)");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Veteran");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "     -> Send member join invitations");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Co-Mayor");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "     -> Claim wild land");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "     -> Unclaim (delete) wild regions");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "Mayor");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "     -> All control of town");
						player.sendMessage("");
						
					}
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You must be a town " + ChatColor.AQUA + "mayor" + ChatColor.WHITE + " to do this.");
				
			} else if(args[0].equalsIgnoreCase("funds") || args[0].equalsIgnoreCase("fund")) {
				if(args.length > 1) {
					
					if(args[1].equalsIgnoreCase("add")) {
						
						if(playerConfig.getString("Town") != null) {
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = getTownDisplayName(player.getUniqueId());
							
							if(args.length > 2 && Chat.isInteger(args[2]) && Integer.parseInt(args[2]) > 0) {
								int coins = Integer.parseInt(args[2]);
								
								if(coins <= playerConfig.getInt("Coins")) {
									
									Town.addFunds(originalTownName, player, coins);
									Economy.takeCoins(player.getUniqueId(), coins);
									playerConfig.set("TownFundsAdded", playerConfig.getInt("TownFundsAdded") + coins);
									playerConfig.save();
									
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Added " + ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.GREEN + " to " + townDisplayName + "'s funds.");
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You don't have " + ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.WHITE + "!");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town funds add <coins>");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You must be in a town to do this.");
						
					} else if(args[1].equalsIgnoreCase("withdraw")) {
						
						if(playerConfig.getInt("TownRank") == Rank.Mayor.ordinal()) {
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = getTownDisplayName(player.getUniqueId());
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							if(args.length > 2 && Chat.isInteger(args[2]) && Integer.parseInt(args[2]) >= 0) {
								int coins = Integer.parseInt(args[2]);
								
								if(coins <= townConfig.getInt("Funds")) {
									
									Town.takeFunds(originalTownName, coins);
									Economy.giveCoins(UUID.fromString(townConfig.getString("Mayor")), coins);
									
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Withdrawn " + ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.GREEN + " from town funds.");
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You don't have " + ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.WHITE + " in your town funds!");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town funds withdraw <coins>");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You must be a town " + ChatColor.AQUA + "mayor" + ChatColor.WHITE + " to do this.");
						
					} else if(DeadMC.TownFile.data().getStringList("Active").contains(args[1]))
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + args[1] + " has " + ChatColor.GOLD + Economy.convertCoins(DeadMC.TownFile.data().getInt(args[1] + ".Funds")) + ChatColor.WHITE + " in their town funds.");
					
					else
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "'" + args[1] + "' is not a valid town.");
					
				} else if(playerConfig.getString("Town") != null)
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Your town has " + ChatColor.GOLD + Economy.convertCoins(TownConfig.getConfig(playerConfig.getString("Town")).getInt("Funds")) + ChatColor.WHITE + " in it's funds.");
				
				else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town funds <town>");
				
			} else if(args[0].equalsIgnoreCase("set")) {
				if(playerConfig.getString("Town") != null
						&& playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
					String originalTownName = playerConfig.getString("Town");
					String townDisplayName = getTownDisplayName(player.getUniqueId());
					TownConfig townConfig = TownConfig.getConfig(originalTownName);
					
					if(args.length > 1 && (args[1].equalsIgnoreCase("description") || args[1].equalsIgnoreCase("banner") || args[1].equalsIgnoreCase("recruiting") || args[1].equalsIgnoreCase("tax") || args[1].equalsIgnoreCase("travel-fee") || args[1].equalsIgnoreCase("travel-point") || args[1].equalsIgnoreCase("name"))) {
						
						if(args[1].equalsIgnoreCase("banner")) {
							if(args.length == 2) {
								if(player.getInventory().getItemInMainHand().getType().toString().toLowerCase().contains("banner") && !player.getInventory().getItemInMainHand().getType().toString().toLowerCase().contains("pattern")) {
									setBanner(originalTownName, player.getInventory().getItemInMainHand().clone());
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Updated your town banner!");
								} else {
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use this command while holding a banner.");
								}
							} else {
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/town set banner" + ChatColor.WHITE + ".");
							}
						}
						
						if(args[1].equalsIgnoreCase("name")) {
							
							if(args.length > 2) {
								
								//knight+ can change name for 1000 coins
								if(playerConfig.getInt("DonateRank") >= Donator.Rank.Knight.ordinal()) {
									
									int funds = townConfig.getInt("Funds");
									int cost = 1000;
									
									String newTownName = args[2];
									
									if(funds >= cost) {
										
										if(confirmNameChange.containsKey(player.getName())) {
											if(confirmNameChange.get(player.getName()).equals(newTownName)) {
												
												String nonCased = newTownName.toLowerCase();
												if(!townNameIsTaken(newTownName) && DeadMC.TravelFile.data().getString(newTownName + ".LocationCode") == null) {
													if(newTownName.length() < 22) {
														
														try {
															ProtectedCuboidRegion region = new ProtectedCuboidRegion(newTownName, BlockVector3.ONE, BlockVector3.ONE);
														} catch(IllegalArgumentException e) {
															player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "The town name cannot contain special characters.");
															return true;
														}
														
														if(!newTownName.contains(".") && !newTownName.contains("/") && !newTownName.contains("'\'") && !newTownName.contains(":") && newTownName != null) {
															
															if(!nonCased.contains("fuck")
																	&& !nonCased.contains("cunt")
																	&& !nonCased.contains("nigger")
																	&& !nonCased.contains("nigga")
																	&& !nonCased.contains("faggot")) {
																
																confirmNameChange.remove(player.getName());
																player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "You have renamed " + townDisplayName + " to " + newTownName + ".");
																
																//rename discord
																Guild guild = DiscordSRV.getPlugin().getMainGuild();
																if(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).size() > 0) {
																	//has discord channel
																	Town.renameDiscordTown(townDisplayName, newTownName);
																}
																
																List<String> aliases = new ArrayList<String>();
																if(DeadMC.TownFile.data().getString("Aliases") != null)
																	aliases = DeadMC.TownFile.data().getStringList("Aliases");
																if(townConfig.getString("Alias") != null) {
																	DeadMC.TownFile.data().set("AliasOriginalName." + townDisplayName, null); //remove old alias
																	aliases.remove(townConfig.getString("Alias")); //remove old alias
																}
																aliases.add(newTownName); //add new alias
																DeadMC.TownFile.data().set("Aliases", aliases);
																
																DeadMC.TownFile.data().set("AliasOriginalName." + newTownName, originalTownName); //set new alias
																
																townConfig.set("Alias", newTownName);
																townConfig.save();
																DeadMC.TownFile.save();
																
																Town.takeFunds(originalTownName, cost);
																
																//update placeholder
																PlaceHolders.player_TownString1.remove(player.getUniqueId().toString());
																PlaceHolders.player_TownName.remove(player.getUniqueId().toString());
																//update leaderboard prefixs
																Chat.updateUserLeaderboardPrefix(player.getUniqueId());
																
															} else
																player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "The town name cannot contain offensive language.");
														} else
															player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "The town name cannot contain special characters.");
													} else
														player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Town name cannot be more than 21 characters.");
												} else
													player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "The name '" + newTownName + "' has been claimed. Try another name.");
												
												
											} else
												player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Please wait for the confirmation session to expire.");
										} else {
											
											confirmNameChange.put(player.getName(), newTownName);
											player.sendMessage("");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Are you sure you want to rename " + townDisplayName + " to:");
											player.sendMessage(ChatColor.YELLOW + "[Town]   - " + ChatColor.AQUA + newTownName);
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.DARK_RED + ChatColor.BOLD + "COST: " + ChatColor.GOLD + Economy.convertCoins(cost));
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/town set name " + newTownName + ChatColor.WHITE + " again to confirm.");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Confirmation session will expire in 30 seconds");
											player.sendMessage("");
											
											Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
												@Override
												public void run() {
													Player player = null;
													if((sender instanceof Player)) {
														player = (Player) sender;
													}
													
													if(confirmNameChange.containsKey(player.getName())) {
														player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Confirmation session expired.");
														confirmNameChange.remove(player.getName());
													}
													
												}
											}, 300L);
											
										}
										
									} else {
										player.sendMessage("");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + townDisplayName + " doesn't have enough funds!");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GOLD + Economy.convertCoins(funds) + ChatColor.WHITE + " / " + ChatColor.GOLD + Economy.convertCoins(cost));
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town funds add <coins>");
										player.sendMessage("");
									}
									
								} else
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town set name <new name>");
							
						}
						
						if(args[1].equalsIgnoreCase("description")) {
							
							if(args.length > 2) {
								
								int count = 3;
								String description = "";
								while(count <= args.length) {
									description = description + args[count - 1] + " ";
									count++;
								}
								description = description.substring(0, description.length() - 1); //remove the last space
								
								if(description.length() <= 128) {
									
									List<TextChannel> channels = DiscordSRV.getPlugin().getMainGuild().getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false);
									if(channels.size() > 0) {
										channels.get(0).getManager().setTopic(description).queue();
									}
									
									townConfig.set("Description", description);
									townConfig.save();
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + townDisplayName + "'s description has been updated.");
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Description must be less than 128 characters.");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town set description <description>");
							
						}
						
						if(args[1].equalsIgnoreCase("tax")) {
							
							if(args.length > 2 && Chat.isInteger(args[2]) && Integer.parseInt(args[2]) >= 0) {
								
								int coins = Integer.parseInt(args[2]);
								
								townConfig.set("Tax", coins);
								townConfig.save();
								
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + townDisplayName + "'s tax is now " + ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.WHITE + ".");
								
							} else {
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town set tax <coins>");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Tax is charged to town members after logging in after every 12 hours.");
							}
							
						}
						
						if(args[1].equalsIgnoreCase("travel-fee")) {
							TownGUI.openSetTravelFeeMenu(player);
						}
						if(args[1].equalsIgnoreCase("travel-point")) {
							TravelGUI.openConfirmMenu(player, TravelGUI.Icon.Confirm_set_town_point, "Manage TP", true);
						}
						
						if(args[1].equalsIgnoreCase("recruiting")) {
							
							if(args.length > 2) {
								
								if(args[2].equalsIgnoreCase("public") || args[2].equalsIgnoreCase("open")) {
									townConfig.set("Recruiting", RecruitStatus.OPEN.ordinal());
									townConfig.save();
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Any player may now join " + townDisplayName + " if the member-limit hasn't been reached.");
								} else if(args[2].equalsIgnoreCase("invite") || args[2].equalsIgnoreCase("invite-only")) {
									townConfig.set("Recruiting", RecruitStatus.INVITE_ONLY.ordinal());
									townConfig.save();
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.YELLOW + "Only players who are sent an invitation may join " + townDisplayName + ".");
								} else if(args[2].equalsIgnoreCase("private") || args[2].equalsIgnoreCase("closed")) {
									townConfig.set("Recruiting", RecruitStatus.CLOSED.ordinal());
									townConfig.save();
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Nobody can now join " + townDisplayName + ".");
								} else
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town set recruiting " + ChatColor.AQUA + "public" + ChatColor.WHITE + "/" + ChatColor.AQUA + "invite-only" + ChatColor.WHITE + "/" + ChatColor.AQUA + "private");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town set recruiting " + ChatColor.AQUA + "public" + ChatColor.WHITE + "/" + ChatColor.AQUA + "invite-only" + ChatColor.WHITE + "/" + ChatColor.AQUA + "private");
							
						}
						
					} else {
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town set " + ChatColor.AQUA + "<one of these:>");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "name" + ChatColor.WHITE + ": rename your town for 1000 coins.");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "description" + ChatColor.WHITE + ": a brief description of your town.");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "travel-point" + ChatColor.WHITE + ": the 'home/spawn' location when players travel to your town.");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "travel-fee" + ChatColor.WHITE + ": set a fee that players must pay when riding the travel boat to your town.");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "recruiting" + ChatColor.WHITE + ": control how and if players can join your town.");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "tax" + ChatColor.WHITE + ": set a tax that " + townDisplayName + "'s members should pay. Only members with less than 50% of owed tax paid are banishable.");
						player.sendMessage("");
					}
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You must be a town " + ChatColor.AQUA + "mayor" + ChatColor.WHITE + " to do this.");
				
			} else if(args[0].equalsIgnoreCase("banish") || args[0].equalsIgnoreCase("kick")) {
				String originalTownName = playerConfig.getString("Town");
				String townDisplayName = getTownDisplayName(player.getUniqueId());
				TownConfig townConfig = TownConfig.getConfig(originalTownName);
				
				//to stop mayors from kicking loyal members, you can only kick players who:
				// - have 0 funds added
				// - has been inactive for more than 3 days (72 hours)
				// - has a tax ratio of less than 50%
				
				if(args.length > 1) {
					if(playerConfig.getString("Town") != null
							&& playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
						String member = args[1];
						UUID memberUUID = Bukkit.getOfflinePlayer(member).getUniqueId();
						
						if(townConfig.getStringList("Members").contains(memberUUID.toString())) {
							PlayerConfig memberConfig = PlayerConfig.getConfig(memberUUID);
							
							if(!Bukkit.getOfflinePlayer(member).getName().equalsIgnoreCase(player.getName())) {
								if(memberConfig.getInt("TownRank") < Rank.Co_Mayor.ordinal()) {
									
									int taxPercentage = 0;
									if(memberConfig.getInt("TownFundsOwed") > 0)
										taxPercentage = (memberConfig.getInt("TownFundsAdded") / memberConfig.getInt("TownFundsOwed")) * 100;
									if(memberConfig.getInt("TownFundsOwed") == 0)
										taxPercentage = 100;
									
									
									if(memberConfig.getInt("TownFundsAdded") <= 0
											|| DateCode.getTimeSince(memberConfig.getString("LastLogin")) > 4320
											|| taxPercentage < 50) {
										
										removeMember(memberUUID);
										if(Bukkit.getPlayer(memberUUID) != null) {
											Bukkit.getPlayer(memberUUID).sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.BOLD + "You have been banished from " + townDisplayName + ".");
											
											//update placeholder
											PlaceHolders.player_TownString1.remove(memberUUID.toString());
											PlaceHolders.player_TownString2.remove(player.getUniqueId().toString());
											PlaceHolders.player_TownRank.remove(player.getUniqueId().toString());
											PlaceHolders.player_TownName.remove(player.getUniqueId().toString());
											//update leaderboard prefixs
											Chat.updateUserLeaderboardPrefix(player.getUniqueId());
										}
										
										Town.sendMessage(originalTownName, ChatColor.WHITE + "" + ChatColor.BOLD + Bukkit.getOfflinePlayer(memberUUID).getName() + ChatColor.WHITE + " has been banished from " + townDisplayName + ".", false, false);
										Town.sendMessage(originalTownName, ChatColor.WHITE + "" + ChatColor.BOLD + Bukkit.getOfflinePlayer(memberUUID).getName() + ChatColor.WHITE + " left " + townDisplayName + ".", false, true);
										
									} else {
										player.sendMessage("");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + member + " cannot be banished as they have member-protection.");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "To stop mayors from banishing loyal members, you can only banish members who:");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + " - Have 0 funds added");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + " - Have been inactive for more than 3 days");
										player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + " - Have a tax paid ratio below 50%");
										player.sendMessage("");
									}
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You cannot banish a mayor.");
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You cannot banish yourself. Try " + ChatColor.GOLD + "/town leave" + ChatColor.WHITE + ".");
						} else
							player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + member + " is not a member of " + townDisplayName + ".");
					} else
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You must be a town " + ChatColor.AQUA + "mayor" + ChatColor.WHITE + " to do this.");
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/town banish <member>" + ChatColor.WHITE + " to kick a player from " + townDisplayName + ".");
				
			} else if(args[0].equalsIgnoreCase("upgrade")) {
				
				if(playerConfig.getString("Town") != null
						&& playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
					String originalTownName = playerConfig.getString("Town");
					String townDisplayName = getTownDisplayName(player.getUniqueId());
					TownConfig townConfig = TownConfig.getConfig(originalTownName);
					
					int costForMemberUpgrade = 50 * townConfig.getInt("Limit.Members"); // +half bar for every member (to get 4 member limit, costs 1.5 bars)
					int costForClaimUpgrade = 2 * (((townConfig.getInt("Limit.Claim") + 1) * (townConfig.getInt("Limit.Claim") + 1))
							- (townConfig.getInt("Limit.Claim") * (townConfig.getInt("Limit.Claim")))); //2 coins for every block added (eg. from 49 to 50 blocks = 99 extra blocks (2 bars)
					
					if(args.length == 2 && (args[1].equalsIgnoreCase("member-limit") || args[1].equalsIgnoreCase("claim-limit"))) {
						
						if(args[1].equalsIgnoreCase("member-limit")) {
							
							int members = townConfig.getInt("Limit.Members");
							
							if((playerConfig.getInt("DonateRank") == Donator.Rank.Citizen.ordinal()
									&& members < 5) || playerConfig.getInt("DonateRank") > Donator.Rank.Citizen.ordinal()) {
								
								boolean canUseFunds = townConfig.getInt("Funds") >= costForMemberUpgrade;
								boolean canUseCoins = playerConfig.getInt("Coins") >= costForMemberUpgrade;
								if(canUseFunds || canUseCoins) {
									
									if(canUseFunds) {
										//use funds first if available
										sendMessage(originalTownName, new String(ChatColor.GOLD + Economy.convertCoins(costForMemberUpgrade) + ChatColor.RED + " deducted from " + townDisplayName + "'s funds."), true, true);
										townConfig.set("Funds", new Integer(townConfig.getInt("Funds") - costForMemberUpgrade));
									} else {
										Economy.takeCoins(player.getUniqueId(), costForMemberUpgrade);
										playerConfig.set("TownFundsAdded", playerConfig.getInt("TownFundsAdded") + costForMemberUpgrade);
										playerConfig.save();
									}
									
									townConfig.set("Limit.Members", new Integer(members + 1));
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Upgraded member limit to " + (members + 1) + " members (cost " + ChatColor.GOLD + Economy.convertCoins(costForMemberUpgrade) + ChatColor.GREEN + ").");
									townConfig.save();
									
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + townDisplayName + " doesn't have enough funds! " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Funds")) + ChatColor.WHITE + " / " + ChatColor.GOLD + Economy.convertCoins(costForMemberUpgrade));
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town funds add <coins>");
									player.sendMessage("");
								}
							} else {
								player.sendMessage("");
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You've reached the " + Donator.getPrefix(player.getUniqueId()) + ChatColor.WHITE + " member limit.");
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to upgrade.");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Or use " + ChatColor.GOLD + "/town members " + townDisplayName + ChatColor.WHITE + " to manage members.");
								player.sendMessage("");
							}
							
						}
						
						if(args[1].equalsIgnoreCase("claim-limit")) {
							
							int current = townConfig.getInt("Limit.Claim");
							int newClaimLimit = current + 1;
							int claimLimit = DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.Claim");
							if(newClaimLimit < claimLimit
									|| playerConfig.getString("StaffRank") != null) {
								
								boolean canUseFunds = townConfig.getInt("Funds") >= costForClaimUpgrade;
								boolean canUseCoins = playerConfig.getInt("Coins") >= costForClaimUpgrade;
								if(canUseFunds || canUseCoins) {
									
									if(canUseFunds) {
										//use funds first if available
										sendMessage(originalTownName, new String(ChatColor.GOLD + Economy.convertCoins(costForClaimUpgrade) + ChatColor.RED + " deducted from " + townDisplayName + "'s funds."), true, true);
										townConfig.set("Funds", new Integer(townConfig.getInt("Funds") - costForClaimUpgrade));
									} else {
										Economy.takeCoins(player.getUniqueId(), costForClaimUpgrade);
										playerConfig.set("TownFundsAdded", playerConfig.getInt("TownFundsAdded") + costForClaimUpgrade);
										playerConfig.save();
									}
									
									townConfig.set("Limit.Claim", current + 1);
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Upgraded land claim limit to " + newClaimLimit * newClaimLimit + " blocks (cost " + ChatColor.GOLD + Economy.convertCoins(costForClaimUpgrade) + ChatColor.GREEN + ").");
									townConfig.save();
									
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + townDisplayName + " doesn't have enough funds! " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Funds")) + ChatColor.WHITE + " / " + ChatColor.GOLD + Economy.convertCoins(costForClaimUpgrade));
									player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town funds add <coins>");
									player.sendMessage("");
								}
							} else {
								player.sendMessage("");
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You've reached the " + Donator.getPrefix(player.getUniqueId()) + ChatColor.WHITE + " claim limit.");
								player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to upgrade.");
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Or use " + ChatColor.GOLD + "/land info " + townDisplayName + ChatColor.WHITE + " to manage land.");
								player.sendMessage("");
							}
						}
						
					} else {
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town upgrade " + ChatColor.AQUA + "member-limit" + ChatColor.GOLD + " OR " + ChatColor.AQUA + "claim-limit");
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  Cost to upgrade " + ChatColor.AQUA + "member-limit" + ChatColor.WHITE + " (" + ChatColor.BOLD + townConfig.getInt("Limit.Members") + ChatColor.WHITE + " members): " + ChatColor.GOLD + Economy.convertCoins(costForMemberUpgrade));
						player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "  Cost to upgrade " + ChatColor.AQUA + "claim-limit" + ChatColor.WHITE + " (" + ChatColor.BOLD + townConfig.getInt("Limit.Claim") + ChatColor.WHITE + "x" + ChatColor.BOLD + townConfig.getInt("Limit.Claim") + ChatColor.WHITE + " blocks): " + ChatColor.GOLD + Economy.convertCoins(costForClaimUpgrade));
						player.sendMessage("");
					}
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You must be a town " + ChatColor.AQUA + "mayor" + ChatColor.WHITE + " to do this.");
				
			} else if(args[0].equalsIgnoreCase("members")) {
				if(args.length > 1) {
					
					Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
						@Override
						public void run() {
							if(townNameIsTaken(args[1])) {
								String originalTownName = getOriginalTownName(townNameCased(args[1]));
								String townDisplayName = getTownDisplayName(originalTownName);
								TownConfig townConfig = TownConfig.getConfig(originalTownName);
								
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(townDisplayName, 46) + " " + townDisplayName + " " + Chat.getTitlePlaceholder(townDisplayName, 46));
								player.sendMessage("Members in " + townDisplayName + ":");
								for(String member : townConfig.getStringList("Members")) {
									PlayerConfig memberConfig = PlayerConfig.getConfig(UUID.fromString(member));
									int townRank = memberConfig.getInt("TownRank");
									
									String rank = ChatColor.WHITE + "(" + ChatColor.BOLD + Rank.values()[townRank].toString() + ChatColor.WHITE + ")";
									if(townRank == Rank.Citizen.ordinal())
										rank = "";
									player.sendMessage(" - " + ChatColor.AQUA + Bukkit.getOfflinePlayer(UUID.fromString(member)).getName() + ChatColor.WHITE + " (has added " + ChatColor.GOLD + Economy.convertCoins(memberConfig.getInt("TownFundsAdded")) + ChatColor.WHITE + ") " + rank);
								}
								player.sendMessage("");
								player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/lookup " + ChatColor.AQUA + "<player>" + ChatColor.WHITE + " to lookup a player!");
								player.sendMessage(ChatColor.YELLOW + "=================================================");
								player.sendMessage("");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "'" + args[1] + "' is not an existing town.");
						}
					});
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town members <town>");
				
			} else if(args[0].equalsIgnoreCase("lookup") || args[0].equalsIgnoreCase("info")) {
				if(args.length > 1) {
					
					Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
						@Override
						public void run() {
							if(townNameIsTaken(args[1])) {
								String originalTownName = getOriginalTownName(townNameCased(args[1]));
								String townDisplayName = getTownDisplayName(originalTownName);
								TownConfig townConfig = TownConfig.getConfig(originalTownName);
								
								int rank = getTownsRanked().indexOf(originalTownName) + 1;
								
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(townDisplayName, 46) + " " + townDisplayName + " " + Chat.getTitlePlaceholder(townDisplayName, 46));
								player.sendMessage(ChatColor.YELLOW + "Description: " + ChatColor.WHITE + townConfig.getString("Description"));
								player.sendMessage(ChatColor.YELLOW + "Reigning since: " + ChatColor.WHITE + DateCode.Decode(townConfig.getString("CreationDate"), DateCode.Value.DAY_OF_MONTH) + "/" + DateCode.Decode(townConfig.getString("CreationDate"), DateCode.Value.MONTH) + "/" + DateCode.Decode(townConfig.getString("CreationDate"), DateCode.Value.YEAR) + " (" + getDaysOld(originalTownName) + " days old)");
								player.sendMessage(ChatColor.YELLOW + "Recruiting: " + ChatColor.WHITE + RecruitStatus.values()[townConfig.getInt("Recruiting")].toString().replace("_", " "));
								player.sendMessage(ChatColor.YELLOW + "Town rank: " + ChatColor.GOLD + "#" + ChatColor.BOLD + rank + ChatColor.WHITE + " out of " + DeadMC.TownFile.data().getStringList("Active").size() + " (" + ChatColor.GOLD + "/town list" + ChatColor.WHITE + ")");
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "Mayor: " + ChatColor.WHITE + Bukkit.getOfflinePlayer(UUID.fromString(townConfig.getString("Mayor"))).getName());
								
								List<String> coMayors = townConfig.getStringList("CoMayors");
								if(coMayors.size() > 0)
									player.sendMessage(ChatColor.YELLOW + "Co Mayors:");
								for(int count = 0; count < coMayors.size(); count++)
									player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.WHITE + Bukkit.getOfflinePlayer(UUID.fromString(coMayors.get(count))).getName());
								
								List<String> members = townConfig.getStringList("Members");
								player.sendMessage(ChatColor.YELLOW + "Number of members: " + ChatColor.WHITE + members.size() + "/" + townConfig.getInt("Limit.Members"));
								player.sendMessage("Use " + ChatColor.GOLD + "/town members " + townDisplayName + ChatColor.WHITE + " for a list of members.");
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "Claimed land: " + ChatColor.WHITE + townConfig.getInt("ClaimedBlocks") + "/" + (townConfig.getInt("Limit.Claim") * townConfig.getInt("Limit.Claim")) + " blocks");
								player.sendMessage("Use " + ChatColor.GOLD + "/land info " + townDisplayName + ChatColor.WHITE + " for a list of regions.");
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "Current funds: " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Funds")));
								player.sendMessage(ChatColor.YELLOW + "Tax per 12 hours: " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Tax")));
								
								boolean hasFee = townConfig.getString("Travel.Fee") != null && townConfig.getInt("Travel.Fee") > 0;
								if(hasFee)
									player.sendMessage(ChatColor.YELLOW + "Travel fee: " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Travel.Fee")));
								if(townConfig.getString("Travel.Public") != null && townConfig.getBoolean("Travel.Public") == true)
									player.sendMessage(ChatColor.YELLOW + "Travel point: " + ChatColor.WHITE + "PUBLIC");
								else if(townConfig.getString("Travel.Public") != null && townConfig.getBoolean("Travel.Public") == false)
									player.sendMessage(ChatColor.YELLOW + "Travel point: " + ChatColor.WHITE + "PRIVATE");
								else
									player.sendMessage(ChatColor.YELLOW + "Travel point: " + ChatColor.WHITE + "NA (not set)");
								player.sendMessage(ChatColor.YELLOW + "=================================================");
								player.sendMessage("");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "'" + args[1] + "' is not an existing town.");
						}
					});
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town lookup <town>");
				
			} else {
				player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Unknown command? Use: " + ChatColor.GOLD + "/town");
			}
			
		}
		
		} catch(Exception e) {
			Chat.logError(e, player);
		}
		
		return true;
	}
	
	public static void create(UUID uuid, String townName) {
		
		TownConfig townConfig = TownConfig.getConfig(townName);
		
		ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		townConfig.set("CreationDate", DateCode.Encode(adelaideTime, true, true, true, true, false, false));
		townConfig.set("Description", "A DeadMC town.");
		townConfig.set("Recruiting", RecruitStatus.OPEN.ordinal());
		townConfig.set("Limit.Members", 1);
		townConfig.set("Limit.Claim", 10);
		townConfig.set("ClaimedBlocks", 0);
		townConfig.set("Mayor", uuid.toString());
		townConfig.set("CoMayors", new ArrayList<String>());
		townConfig.set("Funds", 0);
		townConfig.set("Tax", 0);
		townConfig.set("CurrentLandID", 0);
		townConfig.set("Regions", new ArrayList<String>());
		townConfig.save();
		
		List<String> activeTowns = DeadMC.TownFile.data().getStringList("Active");
		activeTowns.add(townName);
		DeadMC.TownFile.data().set("Active", activeTowns);
		DeadMC.TownFile.save();
		
		addMember(uuid, townName);
		
	}
	
	public static String getOriginalTownName(String townName) {
		if(DeadMC.TownFile.data().getStringList("Active").contains(townName)) {
			return townName;
		} else {
			return DeadMC.TownFile.data().getString("AliasOriginalName." + townName);
		}
	}
	
	public static String getTownDisplayName(UUID uuid) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		String originalTownName = playerConfig.getString("Town");
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		if(townConfig.getString("Alias") != null) {
			return townConfig.getString("Alias");
		}
		return originalTownName;
	}
	
	public static String getTownDisplayName(String townName) {
		if(DeadMC.TownFile.data().getStringList("Active").contains(townName)) {
			TownConfig townConfig = TownConfig.getConfig(townName);
			if(townConfig.getString("Alias") != null) {
				return townConfig.getString("Alias");
			}
		}
		return townName;
	}
	
	public static void setBanner(String originalTownName, ItemStack banner) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		
		townConfig.set("Banner", banner);
		townConfig.save();
	}
	
	public static ItemStack getBanner(String originalTownName) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		if(townConfig.getString("Banner") == null || !townConfig.getItemStack("Banner").getType().toString().toLowerCase().contains("banner")) {
			//set random coloured banner
			
			Random r = new Random();
			int randomColour = r.nextInt(DyeColor.values().length);
			DyeColor randomDye = DyeColor.values()[randomColour];
			
			ItemStack banner = new ItemStack(Material.valueOf(randomDye.toString() + "_BANNER"));

			townConfig.set("Banner", banner);
			townConfig.save();
			
			return banner;
		} else {
			return townConfig.getItemStack("Banner");
		}
	}
	
	public static boolean townNameIsTaken(String townName) {
		for(String alias : DeadMC.TownFile.data().getStringList("Aliases")) {
			if(alias.equalsIgnoreCase(townName)) {
				return true;
			}
		}
		for(String town : DeadMC.TownFile.data().getStringList("Active")) {
			if(town.equalsIgnoreCase(townName)) {
				return true;
			}
		}
		return false;
	}
	
	public static String townNameCased(String townName) {
		for(String alias : DeadMC.TownFile.data().getStringList("Aliases")) {
			if(alias.equalsIgnoreCase(townName)) {
				return alias;
			}
		}
		for(String town : DeadMC.TownFile.data().getStringList("Active")) {
			if(town.equalsIgnoreCase(townName)) {
				return town;
			}
		}
		return null;
	}
	
	public static void disband(String originalTownName) {
		
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		String townDisplayName = getTownDisplayName(originalTownName);
		
		//if town is a member of a nation
		String nationName = townConfig.getString("Nation");
		if(nationName != null) {
			NationConfig nationConfig = NationConfig.getConfig(nationName);
			if(nationConfig.getString("Capital").equalsIgnoreCase(originalTownName)) {
				//is the capital, disband the nation
				Nation.disband(nationName);
			} else {
				//remove from the nation
				Nation.removeTown(nationName, originalTownName);
			}
		}
		
		//delete the town discord if exists
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		if(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).size() > 0) {
			deleteDiscordChannel(originalTownName);
		}
		
		Restarter.writeToLog(" - Disbanding town '" + originalTownName + "'");

		//delete all regions
		List<String> regions = townConfig.getStringList("Regions");
		for(String regionID : regions) {
			
			String regionName = originalTownName + "/" + regionID;
			//remove all shops:
			List<String> shops = new ArrayList<String>();
			File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionName + ".yml");
			RegionConfig regionConfig = null;
			if(regionFile.exists()) {
				regionConfig = RegionConfig.getConfig(regionName);
				shops = regionConfig.getStringList("Shops");
			}
			if(shops.size() > 0) {
				for(String shopCode : shops) {
					//clear signs
					Bukkit.getScheduler().runTaskLater(plugin, () -> {
						Location location = Shops.decodeCode(shopCode);
						Sign sign = (Sign) location.getBlock().getState();
						for(int line = 0; line <= 3; line++)
							sign.setLine(line, "");
						sign.update();
					}, 0L);
				}
			}
			//track number of shops player has:
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			if(regionManager.getRegion(regionName) != null) {
				for(UUID owner : regionManager.getRegion(regionName).getOwners().getUniqueIds()) {
					PlayerConfig playerConfig = PlayerConfig.getConfig(owner);
					int newAmount = playerConfig.getInt("Shops") - shops.size();
					if(newAmount == 0) playerConfig.set("Shops", null);
					else playerConfig.set("Shops", newAmount);
					playerConfig.save();
				}
			}
			//remove region file:
			if(regionConfig != null) regionConfig.discard();
			regionFile.delete();
			
			Restarter.writeToLog("   - Removing region '" + regionName + "'");
			
			//delete region:
			regionManager.removeRegion(regionName);
		}
		//clear all shop files:
		File townFolder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + originalTownName.toLowerCase());
		if(townFolder.isDirectory())
			townFolder.delete();
		
		List<String> members = townConfig.getStringList("Members");
		for(String member : members) {
			PlayerConfig memberConfig = PlayerConfig.getConfig(UUID.fromString(member));
			memberConfig.set("Town", null);
			memberConfig.set("TownRank", null);
			memberConfig.set("TownFundsAdded", null);
			memberConfig.set("TownFundsOwed", null);
			memberConfig.set("LastTax", null);
			memberConfig.save();
		}
		
		//remove alias name
		if(townConfig.getString("Alias") != null) {
			List<String> aliases = new ArrayList<String>();
			if(DeadMC.TownFile.data().getString("Aliases") != null)
				aliases = DeadMC.TownFile.data().getStringList("Aliases");
			DeadMC.TownFile.data().set("AliasOriginalName." + originalTownName, null); //remove old alias
			aliases.remove(townConfig.getString("Alias")); //remove old alias
			DeadMC.TownFile.data().set("Aliases", aliases);
		}
		
		File townFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "towns" + File.separator + originalTownName + ".yml");
		townConfig.discard();
		townFile.delete();
		List<String> activeTowns = DeadMC.TownFile.data().getStringList("Active");
		activeTowns.remove(originalTownName);
		DeadMC.TownFile.data().set("Active", activeTowns);
		DeadMC.TownFile.save();
		
		int totalTowns = activeTowns.size();
		DecimalFormat formatter = new DecimalFormat("#,###");
		PlaceHolders.totalTowns = formatter.format(totalTowns);
	}
	
	public static void addMember(UUID uuid, String townName) {
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		TownConfig townConfig = TownConfig.getConfig(townName);
		String townDisplayName = getTownDisplayName(townName);
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		
		List<String> members = new ArrayList<String>();
		if(townConfig.getStringList("Members").size() != 0) {
			members = townConfig.getStringList("Members");
			playerConfig.set("TownRank", 0);
		} else playerConfig.set("TownRank", Rank.Mayor.ordinal());
		
		members.add(uuid.toString());
		townConfig.set("Members", members);
		playerConfig.set("Town", townName);
		playerConfig.set("TownFundsAdded", 0);
		playerConfig.set("TownFundsOwed", 0);
		
		ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		playerConfig.set("LastTax", DateCode.Encode(adelaideTime, true, false, false, true, true, true));
		
		townConfig.save();
		playerConfig.save();
		
		//add to town discord if linked
		String discordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(uuid);
		if(discordID != null) {
			String townRoleName = "Member of " + townDisplayName;
			Role townRole = guild.getRolesByName(townRoleName, false).size() == 0 ? null : guild.getRolesByName(townRoleName, false).get(0);
			if(townRole != null) {
				DiscordUtil.addRolesToMember(DiscordUtil.getMemberById(discordID), townRole);
			}
			
			//remove from nation discord if exists
			String nationName = townConfig.getString("Nation");
			if(nationName != null) {
				//is a member of a nation
				String nationRoleName = nationName + " nation";
				Role nationRole = guild.getRolesByName(nationRoleName, false).size() == 0 ? null : guild.getRolesByName(nationRoleName, false).get(0);
				if(nationRole != null) {
					DiscordUtil.addRolesToMember(DiscordUtil.getMemberById(discordID), nationRole);
				}
			}
		}
	}
	
	public static Boolean isMember(String town, UUID uuid) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		if(playerConfig == null) return false;
		if(playerConfig.getString("Town") == null) return false;
		
		String townName = playerConfig.getString("Town");
		TownConfig townConfig = TownConfig.getConfig(townName);
		List<String> members = townConfig.getStringList("Members");
		
		for(String memberUUID : members) {
			if(memberUUID.equalsIgnoreCase(uuid.toString())) {
				return true;
			}
		}
		return false;
	}
	
	public static void removeMember(UUID uuid) {
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
		String originalTownName = playerConfig.getString("Town");
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		String townDisplayName = getTownDisplayName(originalTownName);
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		List<String> members = townConfig.getStringList("Members");
		
		if(playerConfig.getInt("TownRank") == Rank.Mayor.ordinal()) {
			
			//get member next in command
			UUID nextInCommand = null;
			members.remove(uuid.toString()); //remove mayor
			if(members.size() != 0) {
				
				for(String member : members) {
					UUID memberUUID = UUID.fromString(member);
					
					if(nextInCommand == null) {
						nextInCommand = memberUUID;
						break; //set first member as initially highest;
					}
					
					if(PlayerConfig.getConfig(memberUUID).getInt("TownRank") > PlayerConfig.getConfig(nextInCommand).getInt("TownRank")) {
						nextInCommand = memberUUID;
						break;
					}
				}
				
			} else {
				disband(originalTownName);
				return;
			}
			
			//set next in command as mayor
			setRank(nextInCommand, Rank.Mayor);
		}
		
		if(townConfig.getStringList("CoMayors").contains(uuid.toString())) {
			List<String> coMayors = townConfig.getStringList("CoMayors");
			coMayors.remove(uuid.toString());
			townConfig.set("CoMayors", coMayors);
			townConfig.save();
		}
		
		String newMayorUUID = townConfig.getString("Mayor");
		for(String regionID : townConfig.getStringList("Regions")) {
			String regionName = originalTownName + "/" + regionID;
			if(regionManager.hasRegion(regionName)) { //check if region still exists
				ProtectedRegion region = regionManager.getRegion(regionName);
				
				if(region.getOwners().getUniqueIds().contains(uuid)) {
					//player was the owner
					//remove ownership
					DefaultDomain owners = region.getOwners();
					owners.removePlayer(uuid);
					
					//set mayor as new owner:
					owners.addPlayer(UUID.fromString(newMayorUUID));
					
					region.setOwners(owners);
				}
				
				if(region.getMembers().getUniqueIds().contains(uuid)) {
					//remove as member
					DefaultDomain regionmembers = region.getMembers();
					regionmembers.removePlayer(uuid);
					region.setMembers(regionmembers);
				}
				
			}
		}
		
		members.remove(uuid.toString());
		townConfig.set("Members", members);
		
		playerConfig.set("Town", null);
		playerConfig.set("TownRank", null);
		playerConfig.set("TownFundsAdded", null);
		playerConfig.set("TownFundsOwed", null);
		playerConfig.set("LastTax", null);
		
		townConfig.save();
		playerConfig.save();
		
		//check if discord linked
		String discordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(uuid);
		if(discordID != null) {
			//remove from town discord
			String townRoleName = "Member of " + townDisplayName;
			Role townRole = guild.getRolesByName(townRoleName, false).size() == 0 ? null : guild.getRolesByName(townRoleName, false).get(0);
			if(townRole != null) {
				DiscordUtil.removeRolesFromMember(DiscordUtil.getMemberById(discordID), townRole);
				
				//check if still qualifies
				if(!qualifiesForDiscordChannel(originalTownName)) {
					deleteDiscordChannel(originalTownName);
				}
			}
			
			//remove from nation discord if exists
			String nationName = townConfig.getString("Nation");
			if(nationName != null) {
				//is a member of a nation
				String nationRoleName = nationName + " nation";
				Role nationRole = guild.getRolesByName(nationRoleName, false).size() == 0 ? null : guild.getRolesByName(nationRoleName, false).get(0);
				if(nationRole != null) {
					DiscordUtil.removeRolesFromMember(DiscordUtil.getMemberById(discordID), nationRole);
				}
			}
		}
	}
	
	public static void setRank(UUID uuid, Rank rank) {
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		String townName = playerConfig.getString("Town");
		TownConfig townConfig = TownConfig.getConfig(townName);
		
		String name = Bukkit.getOfflinePlayer(uuid).getName();
		if(rank.ordinal() > playerConfig.getInt("TownRank")) {
			sendMessage(townName, ChatColor.GREEN + "" + ChatColor.BOLD + name + ChatColor.GREEN + " has been promoted to " + rank.toString().replace("_", " ") + ".", false, false);
			sendMessage(townName, ChatColor.GREEN + "" + ChatColor.BOLD + name + ChatColor.GREEN + " was promoted.", false, true);
		} else {
			sendMessage(townName, ChatColor.WHITE + "" + ChatColor.BOLD + name + ChatColor.WHITE + " has been demoted to " + rank.toString().replace("_", "") + ".", false, false);
			sendMessage(townName, ChatColor.WHITE + "" + ChatColor.BOLD + name + ChatColor.WHITE + " was demoted.", false, true);
		}
		
		//CO-MAYOR:
		//removing:
		List<String> co_mayors = townConfig.getStringList("CoMayors");
		if(playerConfig.getInt("TownRank") == Rank.Co_Mayor.ordinal())
			co_mayors.remove(uuid.toString());
		else if(rank == Rank.Co_Mayor) co_mayors.add(uuid.toString()); //adding
		townConfig.set("CoMayors", co_mayors);
		
		//update new rank
		playerConfig.set("TownRank", rank.ordinal());
		
		//MAYOR:
		if(rank == Rank.Mayor) {
			if(townConfig.getString("Mayor") != null) {
				UUID oldMayor = UUID.fromString(townConfig.getString("Mayor"));
				if(oldMayor != null)
					setRank(oldMayor, Rank.Co_Mayor);
			}
			
			townConfig.set("Mayor", uuid.toString());
		}
		
		//update placeholder
		PlaceHolders.player_TownString1.remove(uuid.toString());
		PlaceHolders.player_TownRank.remove(uuid.toString());
		//update leaderboard prefixs
		Chat.updateUserLeaderboardPrefix(uuid);
		
		playerConfig.save();
		townConfig.save();
	}
	
	public static void addFunds(String originalTownName, Player player, int coins) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		
		sendMessage(originalTownName, ChatColor.GREEN + player.getName() + " added " + ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.GREEN + " to " + getTownDisplayName(originalTownName) + "'s funds.", false, true);
		
		townConfig.set("Funds", townConfig.getInt("Funds") + coins);
		townConfig.save();
		
		//update placeholder
		PlaceHolders.player_TownString2.remove(player.getUniqueId().toString());
	}
	
	public static void addFunds(String originalTownName, int coins) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		
		sendMessage(originalTownName, new String(ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.GREEN + " was added to " + getTownDisplayName(originalTownName) + "'s funds."), true, true);
		
		townConfig.set("Funds", townConfig.getInt("Funds") + coins);
		townConfig.save();
	}
	
	public static void takeFunds(String originalTownName, int coins) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		
		sendMessage(originalTownName, new String(ChatColor.GOLD + Economy.convertCoins(coins) + ChatColor.RED + " deducted from " + getTownDisplayName(originalTownName) + "'s funds."), true, true);
		
		townConfig.set("Funds", new Integer(townConfig.getInt("Funds") - coins));
		townConfig.save();
	}
	
	public static void sendMessage(String originalTownName, String message) {
		sendMessage(originalTownName, message, false, false);
	}
	
	public static void sendMessage(String originalTownName, String message, Boolean mayorsOnly, Boolean subtitle) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		String townDisplayName = getTownDisplayName(originalTownName);
		
		for(String memberUUID : townConfig.getStringList("Members")) {
			Player member = Bukkit.getPlayer(UUID.fromString(memberUUID));
			if(member != null) {
				if(!mayorsOnly || (mayorsOnly && PlayerConfig.getConfig(member.getUniqueId()).getInt("TownRank") >= Rank.Co_Mayor.ordinal())) {
					if(!subtitle)
						member.sendMessage(ChatColor.YELLOW + "[Town] " + message);
					else {
						Bukkit.getScheduler().runTask(plugin, new Runnable() {
							@Override
							public void run() {
								Chat.sendTitleMessage(member.getUniqueId(), message);
							}
						});
					}
				}
			}
		}
		
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		if(!subtitle && !mayorsOnly && guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).size() > 0) {
			//send to discord
			DiscordUtil.sendMessage(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).get(0), message);
		}
	}
	
	public static double getScore(String originalTownName) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		
		Double score = 0.0;
		score += getDaysOld(originalTownName) * 0.02;
		score += townConfig.getStringList("Members").size();
		score += Math.sqrt(townConfig.getInt("ClaimedBlocks")) * 0.1;
		
		return score;
	}
	
	public static List<UUID> getMayors(String townName) {
		List<UUID> mayors = new ArrayList<UUID>();
		
		List<String> members = TownConfig.getConfig(townName).getStringList("Members");
		
		for(String memberUUID : members) {
			if(PlayerConfig.getConfig(UUID.fromString(memberUUID)).getInt("TownRank") >= Rank.Co_Mayor.ordinal())
				mayors.add(UUID.fromString(memberUUID));
		}
		
		return mayors;
	}
	
	public static String getTownRankPrefix(UUID uuid) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		String townPrefix = new String(ChatColor.GRAY + "[" + ChatColor.WHITE + "C" + ChatColor.GRAY + "] ");
		if(playerConfig.getString("Town") == null) townPrefix = new String(""); //no town
		
		else if(Town.Rank.values()[playerConfig.getInt("TownRank")] == Town.Rank.Mayor
				|| Town.Rank.values()[playerConfig.getInt("TownRank")] == Town.Rank.Co_Mayor)
			townPrefix = new String(ChatColor.GRAY + "[" + ChatColor.RED + "M" + ChatColor.GRAY + "] ");
		
		else if(Town.Rank.values()[playerConfig.getInt("TownRank")] == Town.Rank.Veteran)
			townPrefix = new String(ChatColor.GRAY + "[" + ChatColor.LIGHT_PURPLE + "V" + ChatColor.GRAY + "] ");
		
		else if(Town.Rank.values()[playerConfig.getInt("TownRank")] == Town.Rank.Respected)
			townPrefix = new String(ChatColor.GRAY + "[" + ChatColor.YELLOW + "R" + ChatColor.GRAY + "] ");
		
		return townPrefix;
	}
	
	private static long timeWhenLastSorted = 0;
	private static List<String> townsSortedSaved = new ArrayList<String>();
	//gets the towns sorted by score
	public static List<String> getTownsRanked() {
		long timeSinceLastSorted = System.currentTimeMillis() - timeWhenLastSorted;
		long timeBetweenSorts = 300000; //5 minutes
		//only calculate every X minutes
		if(timeSinceLastSorted > timeBetweenSorts) {
			//sort the towns based on score (highest to lowest)
			if(townsSortedSaved.size() == 0) townsSortedSaved = new ArrayList<String>(DeadMC.TownFile.data().getStringList("Active"));
			Collections.sort(townsSortedSaved, new Comparator<String>() {
				@Override
				public int compare(String town1, String town2) {
					return Double.compare(Town.getScore(town2), Town.getScore(town1));
				}
			});
			timeWhenLastSorted = System.currentTimeMillis();
		}
		return townsSortedSaved;
	}
	
	public static int getDaysOld(String townName) {
		TownConfig townConfig = TownConfig.getConfig(townName);
		if(townConfig.getString("CreationDate") == null) return 0; //town is null, eg. if testing
		
		ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		
		int creationDate_Year = DateCode.Decode(townConfig.getString("CreationDate"), DateCode.Value.YEAR);
		int creationDate_DayOfYear = DateCode.Decode(townConfig.getString("CreationDate"), DateCode.Value.DAY_OF_YEAR);
		
		int currentTimeDay;
		if(adelaideTime.getYear() > creationDate_Year)
			currentTimeDay = (365 * (adelaideTime.getYear() - creationDate_Year) + adelaideTime.getDayOfYear());
		else currentTimeDay = adelaideTime.getDayOfYear();
		
		int dayDifference = currentTimeDay - creationDate_DayOfYear;
		
		return dayDifference;
	}
	
	public static boolean qualifiesForDiscordChannel(String originalTownName) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		String townDisplayName = getTownDisplayName(originalTownName);
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		UUID mayorUUID = UUID.fromString(townConfig.getString("Mayor"));
		PlayerConfig mayorConfig = PlayerConfig.getConfig(mayorUUID);
		
		//get discord ID of mayor. If null, return false
		String mayorDiscordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(mayorUUID);
		if(mayorDiscordID == null) {
			//mayor not on discord
			return false;
		}
		
		int activePlayersNeeded = 2; //at least 2 players needed
		int activePlayers = 0;
		//search all town members
		for(String member : townConfig.getStringList("Members")) {
			//an active player is a player whose last login is not more than 7 days ago
			PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(member));
			if(playerConfig.getString("LastLogin") != null) {
				int daysSinceLastSeen = DateCode.getDaysSince(playerConfig.getString("LastLogin"));
				if(daysSinceLastSeen < 7.0) {
					activePlayers++;
					if(activePlayers >= activePlayersNeeded) break;
				}
			}
		}
		//also search ally town members
		
		if(activePlayers < activePlayersNeeded) {
			//not enough active players
			return false;
		}
		
		return true;
	}
	
	public static void turnDiscordChannels() {
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		
		for(String originalTownName : DeadMC.TownFile.data().getStringList("Active")) {
			String townDisplayName = getTownDisplayName(originalTownName);
			if(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).size() > 0
				&& !qualifiesForDiscordChannel(originalTownName)) {
				//no longer qualifies
				deleteDiscordChannel(originalTownName);
			}
		}
	}
	
	public static void createDiscordChannel(String originalTownName) {
		Chat.debug("Creating discord channel for " + originalTownName);
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		String townDisplayName = getTownDisplayName(originalTownName);
		
		//create the role
		String townRoleName = "Member of " + townDisplayName;
		guild.createRole().setName(townRoleName).setColor(Color.CYAN).setPermissions(Permission.USE_SLASH_COMMANDS).complete();
		Role townRole = guild.getRolesByName(townRoleName, false).get(0);
		
		//add roles to each member
		for(String member : townConfig.getStringList("Members")) {
			String discordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(UUID.fromString(member));
			if(discordID != null) {
				DiscordUtil.addRolesToMember(DiscordUtil.getMemberById(discordID), townRole);
			}
		}
		
		//create the channel with role permissions
		long viewPermission = Permission.VIEW_CHANNEL.getRawValue();
		guild.createTextChannel(townDisplayName.toLowerCase() + "-town-chat", guild.getCategoryById("859961704456257546")) //assign to the 'Minecraft' category
				.addRolePermissionOverride(guild.getPublicRole().getIdLong(), 0, viewPermission) //deny viewing
				.addRolePermissionOverride(townRole.getIdLong(), viewPermission, 0) //allow viewing
				.setTopic(townConfig.getString("Description")).complete(); //set the description

		//send intro message to all members
		DiscordUtil.sendMessage(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).get(0), "Welcome to the official " + townDisplayName + " town chat! :smiley:\n\nHere is where you and your town members can communicate.\nThe channel keeps history of your in-game chats, and allows you the ability to chat to your town remotely through Discord.\n\n> :loud_sound: You can also create a private voice channel at any time with **/voice**.");
	}
	
	public static void deleteDiscordChannel(String originalTownName) {
		Chat.debug("Deleting " + originalTownName + "'s discord channel");
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		String townDisplayName = getTownDisplayName(originalTownName);
		
		//delete the role
		String townRoleName = "Member of " + townDisplayName;
		Role townRole = guild.getRolesByName(townRoleName, false).get(0);
		townRole.delete().queue();
		
		//delete the channel
		guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).get(0).delete().queue();
	}
	
	public static void renameDiscordTown(String oldTownName, String newTownName) {
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		
		//rename the role
		String oldRoleName = "Member of " + oldTownName;
		Role townRole = guild.getRolesByName(oldRoleName, false).get(0);
		
		String newRoleName = "Member of " + newTownName;
		townRole.getManager().setName(newRoleName).queue();
		
		//rename the channel
		guild.getTextChannelsByName(oldTownName.toLowerCase() + "-town-chat", false).get(0).getManager().setName(newTownName.toLowerCase() + "-town-chat").queue();
	}
}