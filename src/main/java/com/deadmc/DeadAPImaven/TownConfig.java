package com.deadmc.DeadAPImaven;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class TownConfig extends YamlConfiguration {
	
	public static Map<String, TownConfig> configs = new HashMap<String, TownConfig>();
	
	public static TownConfig getConfig(String originalTownName) {
		synchronized(configs) { //'synchronized' allows only one thread to access configs at a time
			
			if(configs.containsKey(originalTownName))
				return configs.get(originalTownName);

			TownConfig config = new TownConfig(originalTownName);
			configs.put(originalTownName, config);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[TownConfig] " + ChatColor.RESET + "Added " + originalTownName + " - " + configs.size());
			
			return config;
			
		}
	}
	
	private File file = null;
	private Object saveLock = new Object();
	private String originalTownName;
	
	public TownConfig(String originalTownName) {
		super(); //'super' calls parent class constructor (YamlConfiguration)
		file = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "towns" + File.separator + originalTownName + ".yml");
		this.originalTownName = originalTownName;
		reload();
	}
	
	@SuppressWarnings("unused")
	private TownConfig() {
		originalTownName = null;
	}
	
	private void reload() {
		synchronized(saveLock) {
			try {
				load(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void save() {
		synchronized(saveLock) {
			try {
				save(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void discard() {
		synchronized(configs) {
			configs.remove(originalTownName);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[TownConfig] " + ChatColor.RESET + "Removed " + originalTownName + " - " + configs.size());
		}
	}
	
}
