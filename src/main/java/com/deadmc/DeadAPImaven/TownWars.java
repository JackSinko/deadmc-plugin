package com.deadmc.DeadAPImaven;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.data.Rotatable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import com.deadmc.DeadAPImaven.Admin.StaffRank;

public class TownWars implements Listener {
	private DeadMC plugin;
	public TownWars(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClickHelmetSlot(InventoryClickEvent event) {
		if(event.getWhoClicked() instanceof Player) {
	    	Player player = (Player) event.getWhoClicked();
	    	Inventory inventory = event.getClickedInventory();
	    	
	    	if(inventory == player.getInventory() 
	    			&& event.getSlot() == 39
	    			&& inventory.getItem(39) != null) { //cliked helmet slot
	    		ItemStack item = inventory.getItem(39);
	    		ItemMeta meta = item.getItemMeta();

	    		if(meta.getLore() != null
	    				&& meta.getLore().contains(ChatColor.GRAY + "This item will go back to the flag point on death or logout.")) {
	    			//is flag banner
	    			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					if(playerConfig.getString("StaffRank") == null || playerConfig.getInt("StaffRank") < StaffRank.ADMINISTRATOR.ordinal()) {
						event.setCancelled(true);
					}
	    		}
	    	}
		}
	}
	@EventHandler
	public void onDragHelmetSlot(InventoryDragEvent event) {
		if(event.getWhoClicked() instanceof Player) {
	    	Player player = (Player) event.getWhoClicked();
	    	Inventory inventory = event.getInventory();

	    	if(inventory == player.getInventory() 
	    			&& event.getRawSlots().contains(39)) { //cliked helmet slot
	    		ItemStack item = inventory.getItem(39);
	    		ItemMeta meta = item.getItemMeta();
	    		
	    		if(meta.getLore() != null
	    				&& meta.getLore().contains(ChatColor.GRAY + "This item will go back to the flag point on death or logout.")) {
	    			//is flag banner
	    			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					if(playerConfig.getString("StaffRank") == null || playerConfig.getInt("StaffRank") < StaffRank.ADMINISTRATOR.ordinal()) {
						event.setCancelled(true);
					}
	    		}
	    	}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		
		//TOWN WARS BANNER:
		
		if(block.getType().toString().toLowerCase().contains("banner")) {
			ItemMeta meta = player.getInventory().getItemInMainHand().getItemMeta();
			if(meta.getLore() != null && meta.getLore().contains(ChatColor.GRAY + "This item will go back to the flag point on death or logout.")) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				if(playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal()) {
					Banner banner = (Banner) block.getState();
					
					DeadMC.DonatorFile.data().set("TownWars.Flag.Location", LocationCode.Encode(block.getLocation()));
					DeadMC.DonatorFile.data().set("TownWars.Flag.BlockData", banner.getBlockData().getAsString()); //for rotation
					DeadMC.DonatorFile.data().set("TownWars.Flag.Patterns", banner.getPatterns()); //for rotation
					DeadMC.DonatorFile.save();
					
					player.sendMessage("");
					player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "Set new flag spawn location/rotation.");
					player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "The flag will spawn here if a player dies or logs out with the flag in their inventory.");
					player.sendMessage("");
				} else {
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.RED + "You cannot place the flag.");
					event.setCancelled(true);
				}
			}
		}		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event) {
		
		if(!event.isCancelled()) { //cant break blocks on claimed land for task
			Player player = event.getPlayer();
			Block block = event.getBlock();
			Location location = block.getLocation();
			//break flag!
			if(DeadMC.DonatorFile.data().getString("TownWars.Flag.Location") == null) return;
			
			Location flagLoc = LocationCode.Decode(DeadMC.DonatorFile.data().getString("TownWars.Flag.Location"));
			if(location.getBlockX() == flagLoc.getBlockX()
					&& location.getBlockY() == flagLoc.getBlockY()
					&& location.getBlockZ() == flagLoc.getBlockZ()) {

				ItemStack item = (ItemStack) block.getDrops().toArray()[0];
				
				block.setType(Material.AIR);
				
				PlayerInventory inventory = player.getInventory();
				if(inventory.getHelmet() != null && inventory.getHelmet().getItemMeta() != null) {
					ItemStack helmet = inventory.getHelmet();
					ItemMeta helmetMeta = helmet.getItemMeta();
					if(helmetMeta.getLore() != null && helmetMeta.getLore().contains(ChatColor.GRAY + "This item will go back to the flag point on death or logout.")) {
						//already has flag
						event.setCancelled(true);
						return;
					}
				}
				
				ItemMeta meta = item.getItemMeta();
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.GRAY + "This item will go back to the flag point on death or logout.");
				meta.setLore(lore);
				
				item.setItemMeta(meta);
				
				//put helmet in inventory, if full drop on ground
				if(player.getInventory().getHelmet() != null && player.getInventory().getHelmet().getType() != Material.AIR)
	                player.getInventory().addItem(player.getInventory().getHelmet());
	            
				//set the hat:
				player.getInventory().setHelmet(item);
				
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town Wars] " + ChatColor.WHITE + ChatColor.BOLD + "You have the flag!");
				
				event.setCancelled(true);
			}
		}
	}

}
