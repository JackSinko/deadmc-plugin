package com.deadmc.DeadAPImaven;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.bukkit.Bukkit;

public class DateCode {

	public enum Value {
		YEAR,
		MONTH,
		DAY_OF_MONTH,
		DAY_OF_YEAR,
		HOUR,
		MINUTE
	};
	
	public static String Encode(ZonedDateTime date, Boolean year, Boolean month, Boolean dayOfMonth, Boolean dayOfYear, Boolean hour, Boolean minute) {
		String code = "";
		if(year) code = code + date.getYear() + "-"; else code = code + "-";
		if(month) code = code + date.getMonthValue() + "-"; else code = code + "-";
		if(dayOfMonth) code = code + date.getDayOfMonth() + "-"; else code = code + "-";
		if(dayOfYear) code = code + date.getDayOfYear() + "-"; else code = code + "-";
		if(hour) code = code + date.getHour() + "-"; else code = code + "-";
		if(minute) code = code + date.getMinute() + "-"; else code = code + "-";
		return code;
	}
	
	public static int Decode(String code, Value value) {
		int numberOfDash = 0;
		int valueStart = -1;
		int valueEnd = -1;
		for(int count = 0; count < code.length(); count++) {
			
			if(numberOfDash == value.ordinal() && valueStart == -1)
				valueStart = count;
			
			if(code.charAt(count) == '-')
				numberOfDash++;
			
			if(numberOfDash > value.ordinal() && valueEnd == -1) {
				valueEnd = count-1; break;
			}

		}
		
		return Integer.parseInt(code.substring(valueStart, valueEnd + 1));
	}
	
	/**
	 * returns in minutes
	 * @param dateCode requires: year, day of year, hour and minutes
	 */
	public static int getTimeSince(String dateCode) {
		
		ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		
		int year = DateCode.Decode(dateCode, DateCode.Value.YEAR);
		int dayOfYear = DateCode.Decode(dateCode, DateCode.Value.DAY_OF_YEAR);
		int hour = DateCode.Decode(dateCode, DateCode.Value.HOUR);
		int minute = DateCode.Decode(dateCode, DateCode.Value.MINUTE);
		
		int currentTimeDay;
		if(currentTime.getYear() > year) currentTimeDay = (365 * (currentTime.getYear() - year) + currentTime.getDayOfYear());
		else currentTimeDay = currentTime.getDayOfYear();

		int dayDifference = currentTimeDay - dayOfYear;
		
		int differenceInMinutes;
		if(dayDifference == 0) differenceInMinutes = ((currentTime.getHour()*60) + currentTime.getMinute()) - ((hour*60) + minute);
		else if(dayDifference == 1) differenceInMinutes = (((25 - hour - 1)*60) - minute) + ((currentTime.getHour()*60) + currentTime.getMinute());
		else differenceInMinutes = (((25 - hour - 1)*60) - minute) + (((currentTimeDay - dayOfYear) - 2)*1440) + ((currentTime.getHour()*60) + currentTime.getMinute());
		
		return differenceInMinutes;
	}
	
	public static int getDaysSince(String dateCode) {
		ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		
		int year = DateCode.Decode(dateCode, DateCode.Value.YEAR);
		int dayOfYear = DateCode.Decode(dateCode, DateCode.Value.DAY_OF_YEAR);
		
		int currentTimeDay;
		if(currentTime.getYear() > year) {
			//Chat.broadcastMessage("Days since = 365 * " + (currentTime.getYear() - year) + " + " + currentTime.getDayOfYear() + " - " + dayOfYear + " = " + ((365 * (currentTime.getYear() - year) + currentTime.getDayOfYear()) - dayOfYear));
			currentTimeDay = (365 * (currentTime.getYear() - year) + currentTime.getDayOfYear());
		}
		else currentTimeDay = currentTime.getDayOfYear();

		return currentTimeDay - dayOfYear;
	}
	
	
}
