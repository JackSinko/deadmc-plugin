package com.deadmc.DeadAPImaven;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;

import java.util.*;

public class BetterBackpacks implements Listener {
	private static DeadMC plugin;
	public BetterBackpacks(DeadMC p) {
		plugin = p;
	}
	
//	Map<Player, ItemStack> openshulkers = new HashMap<>();
//	Map<Player, Boolean> fromhand = new HashMap<>();
//	Map<UUID, Inventory> openinventories = new HashMap<>();
//	Map<Player, Inventory> opencontainer = new HashMap<>();
//	private Map<Player, Long> pvp_timer = new HashMap<>();
//
//	/*
//Saves the shulker on inventory drag if its open
// */
//	@EventHandler
//	public void onInventoryDrag(InventoryDragEvent event) {
//		if (event.getWhoClicked() instanceof Player) {
//			Player player = (Player) event.getWhoClicked();
//			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
//				if (!saveShulker(player, event.getView().getTitle())) {
//					event.setCancelled(true);
//				}
//			}, 1);
//		}
//	}
//
//	@EventHandler
//	public void onInventoryMoveItem(InventoryMoveItemEvent event) {
//		List<Player> closeInventories = new ArrayList<>();
//		for (Player p : openshulkers.keySet()) {
//			if (openshulkers.get(p).equals(event.getItem())) {
//				closeInventories.add(p);
//			}
//		}
//		for (Player p : closeInventories) {
//			if (event.getInitiator().getLocation().distance(p.getLocation()) < 6) {
//				p.closeInventory();
//			}
//		}
//	}
//
//	/*
//	Opens the shulker if its not in a weird inventory, then saves it
//	 */
//	@EventHandler
//	public void onInventoryClick(InventoryClickEvent event) {
//		if (event.isCancelled()) {
//			return;
//		}
//
//		Player player = (Player) event.getWhoClicked();
//
//		if (openshulkers.containsKey(player)) {
//			if (openshulkers.get(player).getType() == Material.AIR) {
//				event.setCancelled(true);
//				player.closeInventory();
//				return;
//			}
//		}
//
//		if (checkIfOpen(event.getCurrentItem())) { //cancels the event if the player is trying to remove an open shulker
//			if (event.getClick() != ClickType.RIGHT) {
//				event.setCancelled(true);
//				return;
//			}
//		}
//
//		if (event.getWhoClicked() instanceof Player && event.getClickedInventory() != null) {
//			if (event.getCurrentItem() != null && (openshulkers.containsKey(player) && event.getCurrentItem().equals(openshulkers.get(player)))) {
//				event.setCancelled(true);
//				return;
//			}
//
//			if (event.getClickedInventory() != null && (event.getClickedInventory().getType() == InventoryType.CHEST)) {
//				if (!canopeninchests || (canopeninchests && !player.hasPermission("shulkerpacks.open_in_chests"))) {
//					return;
//				}
//			}
//
//			String typeStr = event.getClickedInventory().getType().toString();
//			InventoryType type = event.getClickedInventory().getType();
//			if (typeStr.equals("WORKBENCH") || typeStr.equals("ANVIL") || typeStr.equals("BEACON") || typeStr.equals("MERCHANT") || typeStr.equals("ENCHANTING") ||
//					typeStr.equals("GRINDSTONE") || typeStr.equals("CARTOGRAPHY") || typeStr.equals("LOOM") || typeStr.equals("STONECUTTER")) {
//				return;
//			}
//
//			if (type == InventoryType.CRAFTING && event.getRawSlot() >= 1 && event.getRawSlot() <= 4) {
//				return;
//			}
//
//			if ((player.getInventory() == event.getClickedInventory()) {
//				if (!canopenininventory || (canopenininventory && !player.hasPermission("shulkerpacks.open_in_inventory"))) {
//					return;
//				}
//			}
//
//			if (event.getSlotType() == InventoryType.SlotType.RESULT) {
//				return;
//			}
//
//			if(event.getClickedInventory() != null && event.getClickedInventory().getHolder() != null && event.getClickedInventory().getHolder().getClass().toString().endsWith(".CraftBarrel") && !canopeninbarrels) {
//				return;
//			}
//
//			if (!canopeninenderchest && type == InventoryType.ENDER_CHEST) {
//				return;
//			}
//
//			if (!shiftclicktoopen || event.isShiftClick()) {
//				if (event.isRightClick() && openInventoryIfShulker(event.getCurrentItem(), player)) {
//					fromhand.remove(player);
//					event.setCancelled(true);
//					return;
//				}
//			}
//
//			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
//				if (!saveShulker(player, event.getView().getTitle())) {
//					event.setCancelled(true);
//				}
//			}, 1);
//		}
//	}
//
//	// Deals with multiple people opening the same shulker
//	private boolean checkIfOpen(ItemStack shulker) {
//		for (ItemStack i : openshulkers.values()) {
//			if (i.equals(shulker)) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/*
//	Saves the shulker if its open, then removes the current open shulker from the player data
//	 */
//	@EventHandler
//	public void onInventoryClose(InventoryCloseEvent event) {
//		if (event.getPlayer() instanceof Player) {
//			Player player = (Player) event.getPlayer();
//			if (saveShulker(player, player.getOpenInventory().getTitle())) {
//				player.playSound(player.getLocation(), Sound.BLOCK_SHULKER_BOX_CLOSE, 0.5f, 1);
//			}
//			openshulkers.remove(player);
//		}
//	}
//
//	/*
//	Opens the shulker if the air was clicked with one
//	 */
//	@EventHandler
//	public void onClickAir(PlayerInteractEvent event) {
//		Player player = event.getPlayer();
//		if (event.getClickedBlock() == null || event.getClickedBlock().getType() == Material.AIR) {
//			if (event.getAction() == Action.RIGHT_CLICK_AIR) {
//				ItemStack item = event.getItem();
//				openInventoryIfShulker(item, event.getPlayer());
//				fromhand.put(player, true);
//			}
//		}
//	}
//
//	@EventHandler
//	public void onPlayerHit(EntityDamageByEntityEvent event) {
//		if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
//			setPvpTimer((Player) event.getDamager());
//			setPvpTimer((Player) event.getEntity());
//		}
//	}
//
//	/*
//	Saves the shulker data in the itemmeta
//	 */
//	public boolean saveShulker(Player player, String title) {
//		try {
//			if (openshulkers.containsKey(player)) {
//				if (title.equals(defaultname) || (openshulkers.get(player).hasItemMeta() &&
//						openshulkers.get(player).getItemMeta().hasDisplayName() &&
//						(openshulkers.get(player).getItemMeta().getDisplayName().equals(title)))) {
//					ItemStack item = openshulkers.get(player);
//					if (item != null) {
//						BlockStateMeta meta = (BlockStateMeta) item.getItemMeta();
//						ShulkerBox shulker = (ShulkerBox) meta.getBlockState();
//						shulker.getInventory().setContents(openinventories.get(player.getUniqueId()).getContents());
//						meta.setBlockState(shulker);
//						item.setItemMeta(meta);
//						openshulkers.put(player, item);
//						updateAllInventories(openshulkers.get(player));
//						return true;
//					}
//				}
//			}
//		} catch (Exception e) {
//			openshulkers.remove(player);
//			player.closeInventory();
//			return false;
//		}
//		return false;
//	}
//
//	private void updateAllInventories(ItemStack item) {
//		for (Player p : openshulkers.keySet()) {
//			if (openshulkers.get(p).equals(item)) {
//				BlockStateMeta meta = (BlockStateMeta) item.getItemMeta();
//				ShulkerBox shulker = (ShulkerBox) meta.getBlockState();
//				p.getOpenInventory().getTopInventory().setContents(shulker.getInventory().getContents());
//				p.updateInventory();
//			}
//		}
//	}
//
//	/*
//	Opens the shulker inventory with the contents of the shulker
//	 */
//	public boolean openInventoryIfShulker(ItemStack item, Player player) {
//		if (getPvpTimer(player)) {
//			player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "You cannot open shulkers in combat!");
//			return false;
//		}
//
//		if (player.hasPermission("shulkerpacks.use")) {
//			if (item != null) {
//				if (item.getAmount() == 1 && item.getType().toString().contains("SHULKER")) {
//					if (item.getItemMeta() instanceof BlockStateMeta) {
//						BlockStateMeta meta = (BlockStateMeta) item.getItemMeta();
//						if (meta != null && meta.getBlockState() instanceof ShulkerBox) {
//							ShulkerBox shulker = (ShulkerBox) meta.getBlockState();
//							Inventory inv;
//							if (meta.hasDisplayName()) {
//								inv = Bukkit.createInventory(new ShulkerHolder(), InventoryType.SHULKER_BOX, meta.getDisplayName());
//							} else {
//								inv = Bukkit.createInventory(new ShulkerHolder(), InventoryType.SHULKER_BOX, main.defaultname);
//							}
//							inv.setContents(shulker.getInventory().getContents());
//
//							main.opencontainer.put(player, player.getOpenInventory().getTopInventory());
//
//							player.openInventory(inv);
//							player.playSound(player.getLocation(), Sound.BLOCK_SHULKER_BOX_OPEN, main.volume, 1);
//							main.openshulkers.put(player, item);
//							main.openinventories.put(player.getUniqueId(), player.getOpenInventory().getTopInventory());
//							return true;
//						}
//					}
//				}
//			}
//		}
//		return false;
//	}
//
//	void checkIfValid() {
//		Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
//			@Override
//			public void run() {
//				for (Player p : main.openshulkers.keySet()) {
//					if (main.openshulkers.get(p).getType() == Material.AIR) {
//						p.closeInventory();
//					}
//					if (main.opencontainer.containsKey(p)) {
//						if (main.opencontainer.get(p).getLocation() != null) {
//							if (main.opencontainer.get(p).getLocation().distance(p.getLocation()) > 6) {
//								p.closeInventory();
//							}
//						}
//					}
//				}
//			}
//		}, 1L, 1L);
//	}
}
