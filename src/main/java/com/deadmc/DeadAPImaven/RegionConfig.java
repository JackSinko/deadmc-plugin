package com.deadmc.DeadAPImaven;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class RegionConfig extends YamlConfiguration {
	
	public static Map<String, RegionConfig> configs = new HashMap<String, RegionConfig>();
	
	public static RegionConfig getConfig(String regionID) {
		synchronized(configs) { //'synchronized' allows only one thread to access configs at a time
			
			if(configs.containsKey(regionID))
				return configs.get(regionID);

			RegionConfig config = new RegionConfig(regionID);
			configs.put(regionID, config);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[RegionConfig] " + ChatColor.RESET + "Added " + regionID + " - " + configs.size());
			
			//Bukkit.getConsoleSender().sendMessage("" + Thread.currentThread().getStackTrace()[1]);
			
			return config;
			
		}
	}
	
	private File file = null;
	private Object saveLock = new Object();
	private String regionID;
	
	public RegionConfig(String regionID) {
		super(); //'super' calls parent class constructor (YamlConfiguration)
		file = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionID + ".yml");
		this.regionID = regionID;
		reload();
	}
	
	@SuppressWarnings("unused")
	private RegionConfig() {
		regionID = null;
	}
	
	private void reload() {
		synchronized(saveLock) {
			try {
				load(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void save() {
		synchronized(saveLock) {
			try {
				save(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void discard() {
		synchronized(configs) {
			configs.remove(regionID);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[RegionConfig] " + ChatColor.RESET + "Removed " + regionID + " - " + configs.size());
		}
	}
	
}
