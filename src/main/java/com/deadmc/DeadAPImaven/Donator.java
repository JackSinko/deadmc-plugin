package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Admin.StaffRank;
import com.deadmc.DeadAPImaven.Towns.Town;
import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import io.papermc.lib.PaperLib;
import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MiscDisguise;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import me.libraryaddict.disguise.disguisetypes.PlayerDisguise;
import net.craftcitizen.imagemaps.ImagePlaceEvent;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.data.BlockData;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

public class Donator implements Listener, CommandExecutor {
	private DeadMC plugin;
	
	public Donator(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public enum Rank {
		Citizen,
		Squire,
		Knight,
		Baron,
		Royal,
		Emperor
	}

	public static List<String> disguised = new ArrayList<String>();
	public static List<String> coolingDown = new ArrayList<String>();
	
	public static List<String> invseeing = new ArrayList<String>();
	
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		Player player = null;
		if(sender instanceof Player)
			player = (Player) sender;
		
		if(commandLabel.equalsIgnoreCase("workbench") || commandLabel.equalsIgnoreCase("wb")) {
			if(canUseCommand(player.getUniqueId(), "workbench") || player.isOp()) {
				player.openWorkbench(null, true);
			} else
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
		}
		
		if(commandLabel.equalsIgnoreCase("invsee")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(canUseCommand(player.getUniqueId(), "invsee") || player.isOp() || playerConfig.getString("StaffRank") != null) {
				
				if(args.length > 0) {
					if(Bukkit.getPlayer(args[0]) != null) {
						player.openInventory(Bukkit.getPlayer(args[0]).getInventory());
						invseeing.add(player.getName());
					} else
						player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + args[0] + " must be online.");
				} else
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/invsee <player>");
				
			} else
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
		}
		
		if(commandLabel.equalsIgnoreCase("hat")) {
			if(!canUseCommand(player.getUniqueId(), "hat") && !player.isOp()) {
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
				return true;
			}
			
			if(player.getInventory().getItemInMainHand() == null
					|| player.getInventory().getItemInMainHand().getType() == Material.AIR){
				player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Put an item in your hand to wear it.");
				return true;
			}
			
			ItemStack helmet = player.getInventory().getHelmet();
			if(helmet != null && helmet.getItemMeta().getLore() != null
					&& helmet.getItemMeta().getLore().contains(ChatColor.GRAY + "This item will go back to the flag point on death or logout.")) {
				//is flag banner
				player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "Cannot remove flag from head.");
				return true;
			}
			
			player.getInventory().setHelmet(player.getInventory().getItemInMainHand());
			player.getInventory().remove(player.getInventory().getItemInMainHand());
			
			if(helmet != null) {
				player.getInventory().addItem(helmet);
			}
			
			player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Nice new hat!");
		}
		
		if(commandLabel.equalsIgnoreCase("condense")) {
			
		}
		
		if(commandLabel.equalsIgnoreCase("kit")) {
			
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(args.length > 0) {
				String kitName = args[0];
				if(DeadMC.DonatorFile.data().getString("Kits." + kitName) != null) {
					if(Donator.canUseCommand(player.getUniqueId(), "kit " + kitName)) {
						
						int cooldown = DeadMC.DonatorFile.data().getInt("KitCooldowns." + kitName);
						if(playerConfig.getString("LastUsedKit." + kitName) == null || DateCode.getTimeSince(playerConfig.getString("LastUsedKit." + kitName)) > cooldown) {
							
							ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
							playerConfig.set("LastUsedKit." + kitName, DateCode.Encode(adelaideTime, true, true, true, true, true, true));
							playerConfig.save();
							
							List<ItemStack> itemsInKit = (List<ItemStack>) DeadMC.DonatorFile.data().getList("Kits." + kitName);
							for(ItemStack item : itemsInKit) {
								if(item.getType() == Material.APPLE)
									item.setAmount(64);
								player.getInventory().addItem(item);
							}
							player.updateInventory();
							
							player.sendMessage(ChatColor.YELLOW + "[Kits] " + ChatColor.GREEN + "Kit '" + kitName + "' was added to your inventory.");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Kits] " + ChatColor.WHITE + "Kit cooling down: " + ChatColor.BOLD + (cooldown - DateCode.getTimeSince(playerConfig.getString("LastUsedKit." + kitName))) + ChatColor.WHITE + " minutes remaining");
						
					} else
						player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
				} else
					player.sendMessage(ChatColor.YELLOW + "[Kits] " + ChatColor.WHITE + "The kit '" + kitName + "' doesn't exist.");
			} else
				player.sendMessage(ChatColor.YELLOW + "[Kits] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/kit <kit name>");
		}
		
		//if(commandLabel.equalsIgnoreCase("santa")) {
		//if(playerConfig.getInt("DonateRank") >= Rank.Squire.ordinal()) {
		//Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "disguiseplayer " + player.getName() + " player santa");
		//player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GREEN + "You are now Santa! Happy Holidays!");
		//} else player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
		//}
		
		if(commandLabel.equalsIgnoreCase("disguise") || commandLabel.equalsIgnoreCase("dis") || commandLabel.equalsIgnoreCase("d")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			List<String> disguises = new ArrayList<String>();
			String disguiseString = " - ";
			if(playerConfig.getInt("DonateRank") >= Rank.Emperor.ordinal()) {
				for(EntityType type : EntityType.values()) {
					if(type != EntityType.UNKNOWN && type != EntityType.LIGHTNING) { //banned types
						disguiseString += ChatColor.YELLOW + "" + type.toString() + ChatColor.WHITE + ", ";
						disguises.add(type.toString().toUpperCase());
					}
				}
			} else if(playerConfig.getInt("DonateRank") >= Rank.Baron.ordinal()) {
				for(String type : DeadMC.DonatorFile.data().getStringList(playerConfig.getInt("DonateRank") + ".Upgrades.Disguises")) {
					disguiseString += ChatColor.YELLOW + "" + type.toUpperCase() + ChatColor.WHITE + ", ";
					disguises.add(type.toUpperCase());
				}
			} else {
				disguiseString += ChatColor.RED + "Looks like you don't have any disguises!";
			}
			
			if(args.length == 0) {
				
				if(DisguiseAPI.isDisguised(player)) {
					DisguiseAPI.undisguiseToAll(player);
					player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Your disguise has been removed!");
					return true;
				}
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + "=================== Disguise ===================");
				player.sendMessage(ChatColor.WHITE + "A list of disguises you can use:");
				player.sendMessage(disguiseString);
				player.sendMessage("");
				if(playerConfig.getInt("DonateRank") >= Rank.Squire.ordinal())
					player.sendMessage(ChatColor.WHITE + " - Looking for the ZOMBIE disguise? Use " + ChatColor.GOLD + "/zombie" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/z" + ChatColor.WHITE + ".");
				if(playerConfig.getInt("DonateRank") != Rank.Emperor.ordinal())
					player.sendMessage(ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock more disguises.");
				player.sendMessage(ChatColor.YELLOW + "==============================================");
				player.sendMessage("");
				
				return true;
			}
			
			if(args[0].equalsIgnoreCase("unknown") || args[0].equalsIgnoreCase("lightning")) {
				player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Sorry, you can't disguise as that.");
				return true;
			}
			
			try {
				if(disguises.contains(args[0].toUpperCase())
						|| playerConfig.getInt("DonateRank") == Rank.Emperor.ordinal()) { //player can access this disguise
					EntityType type = EntityType.valueOf(args[0].toUpperCase());
					MobDisguise mobDisguise = new MobDisguise(DisguiseType.getType(type));
					DisguiseAPI.disguiseToAll(player, mobDisguise);
					player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.GREEN + "You disguise as a " + ChatColor.BOLD + type.toString().replace("_", " ") + ChatColor.GREEN + "!");
					return true;
				}
			} catch(IllegalArgumentException e) {
				try {
					if(disguises.contains(args[0].toUpperCase())
							|| playerConfig.getInt("DonateRank") == Rank.Emperor.ordinal()) { //player can access this disguise
						EntityType type = EntityType.valueOf(args[0].toString().toUpperCase());
						MiscDisguise mobDisguise = new MiscDisguise(DisguiseType.getType(type));
						DisguiseAPI.disguiseToAll(player, mobDisguise);
						player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.GREEN + "You disguise as a " + ChatColor.BOLD + type.toString().replace("_", " ") + ChatColor.GREEN + "!");
						return true;
					}
				} catch(IllegalArgumentException e2) {
					try {
						if(playerConfig.getInt("DonateRank") == Rank.Emperor.ordinal()) {
							PlayerDisguise playerDisguise = new PlayerDisguise(args[0]);
							DisguiseAPI.disguiseToAll(player, playerDisguise);
							player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.GREEN + "You disguise as player '" + ChatColor.BOLD + args[0] + ChatColor.GREEN + "'!");
							return true;
						}
					} catch(IllegalArgumentException e3) {
					}
				}
			}
			player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "No access to disguise " + ChatColor.BOLD + args[0] + ChatColor.RED + ".");
			if(playerConfig.getInt("DonateRank") != Rank.Emperor.ordinal())
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock more disguises.");
			
		}
		
		if(commandLabel.equalsIgnoreCase("zombie") || commandLabel.equalsIgnoreCase("z")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(playerConfig.getInt("DonateRank") >= Rank.Squire.ordinal()) {
				
				if(disguised.contains(player.getName())) {
					setDisguised(player.getUniqueId(), false);
					player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Your disguise has been removed!");
				} else {
					if(!coolingDown.contains(player.getName()))
						setDisguised(player.getUniqueId(), true);
					else
						player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Your disguise is not yet ready to use again.");
				}
				
			} else
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
			
		}
		
		if(commandLabel.equalsIgnoreCase("fly")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(playerConfig.getInt("DonateRank") >= Rank.Royal.ordinal()) {
				if(player.getAllowFlight()) {
					player.sendMessage(ChatColor.YELLOW + "[Fly] " + ChatColor.RED + "Flying mode disabled.");
					player.setFlying(false);
					player.setAllowFlight(false);
				} else {
					if(Regions.playerCanBuild(player, player.getLocation(), false)) {
						player.sendMessage(ChatColor.YELLOW + "[Fly] " + ChatColor.GREEN + "Flying mode enabled!");
						player.sendMessage(ChatColor.YELLOW + "[Fly] " + ChatColor.RED + ChatColor.BOLD + "NOTE: " + ChatColor.WHITE + "you can only fly on land you have rights to.");
						player.setAllowFlight(true);
						player.setFlying(true);
					} else
						player.sendMessage(ChatColor.YELLOW + "[Fly] " + ChatColor.RED + "You can only fly on land you have rights to.");
				}
			} else
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
		}
		
		if(commandLabel.equalsIgnoreCase("name")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(canUseCommand(player.getUniqueId(), "name") || player.isOp()) {
				if(args.length > 0) {
					String name = args[0];
					String nonCased = name.toLowerCase();
					String nameNoColor = Chat.stripColor(name);
					
					if(!DeadMC.PlayerFile.data().getStringList("Active").contains(name) && (!name.equalsIgnoreCase("Snicko") || player.isOp())) {
						if(nameNoColor.length() < 17) {
							if(!nonCased.contains("fuck")
									&& !nonCased.contains("cunt")
									&& !nonCased.contains("nigger")
									&& !nonCased.contains("nigga")
									&& !nonCased.contains("faggot")) {
								
								playerConfig.set("Name", name);
								//update placeholder:
								PlaceHolders.playersName.replace(player.getUniqueId().toString(), name);
								//update leaderboard prefixs
								Chat.updateUserLeaderboardPrefix(player.getUniqueId());
								
								if(playerConfig.getInt("DonateRank") == Rank.Emperor.ordinal())
									player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Name changed to " + ChatColor.translateAlternateColorCodes('&', name) + ".");
								else
									player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Name changed to " + name + ".");
								
								//update actual username for DiscordSRV
								player.setDisplayName(name);
								Chat.debug("Player display name is " + player.getDisplayName());
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Name cannot contain offensive language.");
						} else
							player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Name must be less than 17 characters.");
					} else
						player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Cannot use name of an existing player.");
				} else {
					playerConfig.set("Name", null);
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Custom name removed.");
					
					//update placeholder:
					PlaceHolders.playersName.replace(player.getUniqueId().toString(), player.getName());
					//update leaderboard prefixs
					Chat.updateUserLeaderboardPrefix(player.getUniqueId());
				}
				playerConfig.save();
			} else
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
		}
		
		if(commandLabel.equalsIgnoreCase("royal") || commandLabel.equalsIgnoreCase("king") || commandLabel.equalsIgnoreCase("queen")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(playerConfig.getInt("DonateRank") == Rank.Royal.ordinal()) {
				
				String prefix = commandLabel.toUpperCase().substring(0, 1) + commandLabel.toLowerCase().substring(1, commandLabel.length());
				
				playerConfig.set("SpecialPrefix", prefix);
				playerConfig.save();
				
				player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Updated prefix to " + prefix + ".");
				
			} else
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
		}
		
		//set a custom rank prefix
		if(commandLabel.equalsIgnoreCase("prefix")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(playerConfig.getInt("DonateRank") >= Rank.Emperor.ordinal()) {
				
				if(args.length == 0) {
					//remove prefix
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Custom prefix successfully removed!");
					playerConfig.set("SpecialPrefix", null);
					playerConfig.save();
					return true;
				}
				
				String prefix = args[0];
				String nonCased = prefix.toLowerCase();
				String prefixNoColor = Chat.stripColor(prefix);
				
				if(!nonCased.contains("mod")
						&& !nonCased.contains("staff")
						&& !nonCased.contains("admin")
						&& !nonCased.contains("snicko")
						&& !nonCased.contains("helper")
						&& !nonCased.contains("builder")) {
					if(prefixNoColor.length() < 11) {
						if(!nonCased.contains("fuck")
								&& !nonCased.contains("cunt")
								&& !nonCased.contains("nigger")
								&& !nonCased.contains("nigga")
								&& !nonCased.contains("faggot")) {
							
							playerConfig.set("SpecialPrefix", prefix);
							playerConfig.save();
							
							player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Prefix changed to " + ChatColor.translateAlternateColorCodes('&', prefix) + ChatColor.GREEN + ".");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Prefix cannot contain offensive language.");
					} else
						player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Prefix must be less than 11 characters.");
				} else
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Cannot use a prefix containing staff roles.");
				
			} else
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
		}
		
		if(commandLabel.equalsIgnoreCase("deathpoint")) {
			if(deathPoints.containsKey(player.getName())) {
				
				player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Travelling to your death point...");
				player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 160, 500000, false, false));
				
				Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
					@Override
					public void run() {
						Player player = (Player) sender;
						
						player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 60, 500000, false, false));
						PaperLib.teleportAsync(player, deathPoints.get(player.getName()));
						deathPoints.remove(player.getName());
						
					}
				}, 60L);
				Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
					@Override
					public void run() {
						
						Player player = (Player) sender;
						player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "After a long journey, you arrive at your death point.");
						
					}
				}, 120L);
			} else
				player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You don't have a saved death point.");
		}
		
		if(commandLabel.equalsIgnoreCase("banner") || commandLabel.equalsIgnoreCase("banners")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);

			Rank rank = Donator.Rank.values()[playerConfig.getInt("DonateRank")];
			if(rank == Rank.Emperor || player.isOp()) {
				if(args.length == 0) {
					player.sendMessage("");
					player.sendMessage(ChatColor.YELLOW + "================= Banners ==================");
					player.sendMessage(ChatColor.WHITE + "Banners are here! You can upload images to host sites like " + ChatColor.GOLD + "https://imgbb.com" + ChatColor.WHITE + " and display them in DeadMC!");
					player.sendMessage(ChatColor.RED + "Please note that you will be punished for uploading any offensive or non-family-friendly content.");
					player.sendMessage("");
					player.sendMessage(ChatColor.GOLD + "/banner list" + ChatColor.WHITE + " - A list of your banners.");
					player.sendMessage(ChatColor.GOLD + "/banner create <name> <URL>" + ChatColor.WHITE + " - Create or update a banner.");
					player.sendMessage(ChatColor.GOLD + "/banner place <name>" + ChatColor.WHITE + " - Place a banner.");
					player.sendMessage(ChatColor.YELLOW + "===========================================");
					player.sendMessage("");
				} else {
					if(args[0].equalsIgnoreCase("list")) {
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "================= Banners ==================");
						List<String> banners = new ArrayList<String>();
						if(playerConfig.getString("Banners") != null)
							banners = playerConfig.getStringList("Banners");

						if(banners.size() == 0) //no banners
							player.sendMessage(" - " + ChatColor.RED + "You don't have any banners");
						else {
							for(String name : banners) {
								player.sendMessage(" - " + ChatColor.YELLOW + name);
							}
						}
						//create a banner with
						//place a banner with
						player.sendMessage(ChatColor.WHITE + "Create a banner with: " + ChatColor.GOLD + "/banner create <name>" + ChatColor.WHITE + ".");
						player.sendMessage(ChatColor.WHITE + "Place a banner with: " + ChatColor.GOLD + "/banner place <name>" + ChatColor.WHITE + ".");
						player.sendMessage(ChatColor.YELLOW + "===========================================");
						player.sendMessage("");
					} else if(args[0].equalsIgnoreCase("create")) {
						if(args.length >= 3) { //banner create <name> <URL>

							List<String> banners = new ArrayList<String>();
							if(playerConfig.getString("Banners") != null)
								banners = playerConfig.getStringList("Banners");

							String name = player.getUniqueId() + "_" + args[1];
							String URL = args[2];

							//if player has a banner already with that name, it will update it
							player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "Downloading banner...");

							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "perm player " + player.getName() + " set imagemaps.download true");
							Bukkit.dispatchCommand(sender, "imagemap download " + name + " " + URL);
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "perm player " + player.getName() + " set imagemaps.download false");

							//reload it
							if(banners.contains(args[1])) {
								player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "Banner will be updated in 10 seconds.");
								Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
									@Override
									public void run() {
										Player player = (Player) sender;
										Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "perm player " + player.getName() + " set imagemaps.reload true");
										Bukkit.dispatchCommand(sender, "imagemap reload " + name);
										Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "perm player " + player.getName() + " set imagemaps.reload false");
									}
								}, 200L);

							}

							if(!banners.contains(args[1])) {
								banners.add(args[1]);
								playerConfig.set("Banners", banners);
								playerConfig.save();
							}

						} else {
							player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/banner create <name> <URL>" + ChatColor.WHITE + ".");
							player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "For a list of your banners use " + ChatColor.GOLD + "/banner list" + ChatColor.WHITE + ".");
						}
					} else if(args[0].equalsIgnoreCase("place")) {
						if(args.length >= 2) { //banner place <name>
							String name = player.getUniqueId() + "_" + args[1];

							List<String> banners = new ArrayList<String>();
							if(playerConfig.getString("Banners") != null)
								banners = playerConfig.getStringList("Banners");
							//if list of banners contains name
							if(banners.contains(args[1])) {
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "perm player " + player.getName() + " set imagemaps.place true");
								Bukkit.dispatchCommand(sender, "imagemap place " + name + " false true");
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "perm player " + player.getName() + " set imagemaps.place false");
							} else {
								player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "You don't have a banner named " + ChatColor.GOLD + args[1] + ChatColor.WHITE + ".");
								player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "For a list of your banners, use " + ChatColor.GOLD + "/banner list" + ChatColor.WHITE + ".");
							}
						} else {
							player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/banner place <name>" + ChatColor.WHITE + ".");
							player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "For a list of your banners, use " + ChatColor.GOLD + "/banner list" + ChatColor.WHITE + ".");
						}
					} else {
						//unknown command
						player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "Unknown command '" + args[0] + "'.");
						player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/banner" + ChatColor.WHITE + " for help.");
					}
				}
			} else
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
		}

		return true;
	}
	
	@EventHandler
	public void PlaceBanner(ImagePlaceEvent event) {
		Block block = event.getBlock();
		
		Player player = event.getPlayer();
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
		ApplicableRegionSet regionsAtLocation = regionManager.getApplicableRegions(BlockVector3.at(block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ()));
		ProtectedRegion highestPriority = Regions.getHighestPriorityLand(block.getLocation());
		Boolean ownsLand = highestPriority != null
				&& regionManager.getRegion(highestPriority.getId()).getOwners().getUniqueIds().contains(player.getUniqueId());
		
		//only place if admin, or player owns the land
		if((playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal())
				|| (regionsAtLocation.size() > 0
				&& ownsLand)) {
			
			//player owns the land
			
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Banners] " + ChatColor.GREEN + "Placed banner!");
			player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.GREEN + "Banner placed! " + ChatColor.WHITE + ChatColor.BOLD + "Right click" + ChatColor.WHITE + " the banner to remove it.");
			
			
		} else {
			//player doesn't own the land
			player.sendMessage(ChatColor.YELLOW + "[Banners] " + ChatColor.RED + "You do not " + ChatColor.BOLD + "own" + ChatColor.RED + " this land!");
			event.setCancelled(true);
		}
	}
	
	//remove image map in survival mode
	@EventHandler
	public void RemoveBanner(PlayerInteractEntityEvent event) {
		Player player = event.getPlayer();
		Entity entity = event.getRightClicked();
		
		if(entity instanceof ItemFrame) {
			ItemFrame itemFrame = (ItemFrame) entity;
			if(itemFrame.isFixed()) {
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
				ApplicableRegionSet regionsAtLocation = regionManager.getApplicableRegions(BlockVector3.at(itemFrame.getLocation().getX(), itemFrame.getLocation().getY(), itemFrame.getLocation().getZ()));
				ProtectedRegion highestPriority = Regions.getHighestPriorityLand(player.getLocation());
				Boolean ownsLand = highestPriority != null
						&& regionManager.getRegion(highestPriority.getId()).getOwners().getUniqueIds().contains(player.getUniqueId());
				
				//only remove if admin, no regions at location, or player owns the land
				if((playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal())
						|| regionsAtLocation.size() == 0
						|| ownsLand) {
					
					itemFrame.remove();
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Banners] " + ChatColor.RED + "Removed banner!");
					
				}
			}
		}
		
	}
	
	public static Rank getRank(UUID uuid) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		if(playerConfig.getString("DonateRank") != null)
			return Rank.values()[playerConfig.getInt("DonateRank")];
		else return null;
	}
	
	public static int getRankID(String item) {
		for(int count = 0; count <= Rank.values().length; count++) {
			if(Rank.values()[count].toString().equalsIgnoreCase(item)) return count;
		}
		return 0;
	}
	
	public static void setRank(UUID uuid, Rank rank) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		playerConfig.set("DonateRank", rank.ordinal());
		playerConfig.save();
	}
	
	public static String getPrefix(UUID uuid) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		if(playerConfig.getString("Builder") != null) {
			return new String(ChatColor.GRAY + "[" + ChatColor.LIGHT_PURPLE + "Builder" + ChatColor.GRAY + "]");
		}
		if(playerConfig.getString("Helper") != null) {
			return new String(ChatColor.GRAY + "[" + ChatColor.GOLD + "Helper" + ChatColor.GRAY + "]");
		}
		
		if(playerConfig.getString("StaffRank") != null) {
			StaffRank staff = StaffRank.values()[playerConfig.getInt("StaffRank")];
			if(staff == StaffRank.JUNIOR_MOD)
				return new String(ChatColor.GRAY + "[" + ChatColor.WHITE + "Junior " + ChatColor.DARK_GREEN + "Mod" + ChatColor.GRAY + "]");
			if(staff == StaffRank.MODERATOR)
				return new String(ChatColor.GRAY + "[" + ChatColor.DARK_GREEN + "Moderator" + ChatColor.GRAY + "]");
			if(staff == StaffRank.SENIOR_MOD)
				return new String(ChatColor.GRAY + "[" + ChatColor.GOLD + "Senior " + ChatColor.DARK_GREEN + "Mod" + ChatColor.GRAY + "]");
			if(staff == StaffRank.ADMINISTRATOR)
				return new String(ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Admin" + ChatColor.GRAY + "]");
			if(staff == StaffRank.OWNER)
				return new String(ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Owner" + ChatColor.GRAY + "]");
		}
		
		Rank rank = Donator.Rank.values()[playerConfig.getInt("DonateRank")];
		if(rank == Rank.Citizen)
			return new String(ChatColor.GRAY + "[" + rank.toString() + ChatColor.GRAY + "]");
		if(rank == Rank.Squire)
			return new String(ChatColor.GRAY + "[" + ChatColor.YELLOW + rank.toString() + ChatColor.GRAY + "]");
		if(rank == Rank.Knight)
			return new String(ChatColor.GRAY + "[" + ChatColor.AQUA + rank.toString() + ChatColor.GRAY + "]");
		if(rank == Rank.Baron)
			return new String(ChatColor.GRAY + "[" + ChatColor.GREEN + rank.toString() + ChatColor.GRAY + "]");
		if(rank == Rank.Royal) {
			if(playerConfig.getString("SpecialPrefix") != null)
				return new String(ChatColor.GRAY + "[" + ChatColor.DARK_PURPLE + ChatColor.BOLD + playerConfig.getString("SpecialPrefix") + ChatColor.GRAY + "]");
			return new String(ChatColor.GRAY + "[" + ChatColor.DARK_PURPLE + ChatColor.BOLD + rank.toString() + ChatColor.GRAY + "]");
		}
		if(rank == Rank.Emperor) {
			if(playerConfig.getString("SpecialPrefix") != null)
				return new String(ChatColor.GRAY + "[" + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', playerConfig.getString("SpecialPrefix")) + ChatColor.GRAY + "]");
			return new String(ChatColor.GRAY + "[" + ChatColor.GOLD + ChatColor.BOLD + rank.toString() + ChatColor.GRAY + "]");
		}
		
		return null;
	}
	
	public static boolean canUseCommand(UUID uuid, String command) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		if(Bukkit.getOfflinePlayer(uuid).isOp()) return true;
		return DeadMC.DonatorFile.data().getStringList(playerConfig.getInt("DonateRank") + ".Commands").contains(command);
	}
	
	private Map<String, List<ItemStack>> itemsToKeepOnRespawn = new HashMap<String, List<ItemStack>>(); //player items
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		if(itemsToKeepOnRespawn.containsKey(player.getName())) {
			
			for(ItemStack item : itemsToKeepOnRespawn.get(player.getName()))
				player.getInventory().addItem(item);
			
			itemsToKeepOnRespawn.remove(player.getName());
		}
	}
	
	public static Map<String, Location> deathPoints = new HashMap<String, Location>();
	
	@EventHandler
	public void createDeathPoint(PlayerDeathEvent event) {
		Player player = event.getEntity();
		PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
		
		PlaceHolders.totalDeaths++;
		
		//helmet contains flag?
		PlayerInventory inventory = player.getInventory();
		if(inventory.getHelmet() != null && inventory.getHelmet().getItemMeta() != null) {
			ItemStack helmet = player.getInventory().getHelmet();
			ItemMeta helmetMeta = helmet.getItemMeta();
			if(helmetMeta.getLore() != null && helmetMeta.getLore().contains(ChatColor.GRAY + "This item will go back to the flag point on death or logout.")) {
				//place back at flag spawn point
				player.getInventory().setHelmet(new ItemStack(Material.AIR));
				player.updateInventory();
				
				event.getDrops().remove(helmet);
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town Wars] " + ChatColor.RED + "The flag has returned to the spawn point.");
				
				Location location = LocationCode.Decode(DeadMC.DonatorFile.data().getString("TownWars.Flag.Location"));
				BlockData blockData = Bukkit.getServer().createBlockData(DeadMC.DonatorFile.data().getString("TownWars.Flag.BlockData"));
				List<Pattern> patterns = (List<Pattern>) DeadMC.DonatorFile.data().getList("TownWars.Flag.Patterns");
				
				Banner banner = (Banner) Bukkit.getWorld("world").getBlockAt(location).getState();
				banner.setPatterns(patterns);
				banner.setBlockData(blockData);
				banner.update();
			}
		}

		//player has newbie protection
		if(playerConfig.getString("FreeDeathPointTravel") != null
				|| playerConfig.getString("FreeInventorySaveOnDeath") != null) {
			
			if(playerConfig.getString("FreeInventorySaveOnDeath") != null) {
				playerConfig.set("FreeInventorySaveOnDeath", playerConfig.getInt("FreeInventorySaveOnDeath") == 1 ? null : playerConfig.getInt("FreeInventorySaveOnDeath") - 1);
				playerConfig.save();
				
				event.setKeepInventory(true);
				event.getDrops().clear();
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + "[Death] " + ChatColor.GREEN + "Your items were saved!");
			}
			
			player.sendMessage("");
			player.sendMessage(ChatColor.YELLOW + "=============================================");
			player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Oh dear! You died! " + ChatColor.RESET + "But don't stress, you still have some newbie benefits enabled:");
			player.sendMessage("");
			if(playerConfig.getString("FreeInventorySaveOnDeath") != null)
				player.sendMessage(ChatColor.WHITE + "   > " + ChatColor.BOLD + playerConfig.getInt("FreeDeathPointTravel") + " free inventory saves on death");
			if(playerConfig.getString("FreeInventorySaveOnDeath") != null && playerConfig.getInt("FreeInventorySaveOnDeath") == 1)
				player.sendMessage(ChatColor.WHITE + "     > " + ChatColor.RED + "Be sure to claim land to store valuables! " + ChatColor.GOLD + ChatColor.BOLD + "/land claim");
			if(playerConfig.getString("FreeDeathPointTravel") != null)
				player.sendMessage(ChatColor.WHITE + "   > " + ChatColor.BOLD + playerConfig.getInt("FreeDeathPointTravel") + " free travels to your death point" + ChatColor.WHITE + " (" + ChatColor.GOLD + "/deathpoint)");
			if(playerConfig.getString("FreeDeathPointTravel") != null && playerConfig.getInt("FreeDeathPointTravel") < 3)
				player.sendMessage(ChatColor.WHITE + "     > " + ChatColor.RED + "Remember to set a travel point! " + ChatColor.GOLD + ChatColor.BOLD + "/travel");
			player.sendMessage(ChatColor.YELLOW + "=============================================");
			player.sendMessage("");
			
		}
		
		Rank rank = getRank(player.getUniqueId());
		if(rank != null && rank.ordinal() >= Rank.Squire.ordinal()) {
			int savesLimit = DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.SaveInventories");
			
			ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
			if(playerConfig.getString("LastSaveDay") == null) {
				playerConfig.set("LastSaveDay", adelaideTime.getDayOfYear());
				playerConfig.save();
			}
			int lastSaveDay = playerConfig.getInt("LastSaveDay");
			
			if(lastSaveDay != adelaideTime.getDayOfYear() || playerConfig.getString("InvenSaves") == null) {
				playerConfig.set("InvenSaves", 0);
				playerConfig.save();
			}
			
			int invenSaves = playerConfig.getInt("InvenSaves");
			if(invenSaves < savesLimit) {
				event.setKeepInventory(true);
				event.getDrops().clear();
				
				playerConfig.set("InvenSaves", invenSaves + 1);
				playerConfig.set("LastSaveDay", adelaideTime.getDayOfYear());
				playerConfig.save();
				invenSaves = playerConfig.getInt("InvenSaves");
				
				player.sendMessage(ChatColor.YELLOW + "[Death] " + ChatColor.GREEN + "Your items were saved! " + ChatColor.WHITE + "(" + ChatColor.GOLD + invenSaves + ChatColor.WHITE + " / " + ChatColor.GOLD + savesLimit + ChatColor.WHITE + " used today)");
			} else
				player.sendMessage(ChatColor.YELLOW + "[Death] " + ChatColor.RED + "Your items weren't saved! " + ChatColor.WHITE + "(" + ChatColor.GOLD + invenSaves + ChatColor.WHITE + " / " + ChatColor.GOLD + savesLimit + ChatColor.WHITE + " used today)");
			
		}
		
		if(playerConfig.getString("FreeDeathPointTravel") != null
				|| getRank(player.getUniqueId()).ordinal() >= Rank.Baron.ordinal()) {
			
			if(playerConfig.getString("FreeDeathPointTravel") != null) {
				int deathPointsLeft = playerConfig.getInt("FreeDeathPointTravel") - 1;
				
				if(deathPointsLeft <= 0) playerConfig.set("FreeDeathPointTravel", null);
				else playerConfig.set("FreeDeathPointTravel", deathPointsLeft);
				playerConfig.save();
			}
			
			player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Your death point has been saved! " + ChatColor.GOLD + "/deathpoint");
			deathPoints.put(player.getName(), player.getLocation());
			
			Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {
				@Override
				public void run() {
					if(deathPoints.containsKey(player.getName())) {
						deathPoints.remove(player.getName());
						
						player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Your saved death point has expired.");
					}
				}
			}, 18000L); // 15 minute
			
		}
		
		if(!event.getKeepInventory()) {
			//keep 'dont drop' items:
			List<ItemStack> dontDropItems = new ArrayList<ItemStack>();
			for(ItemStack item : event.getDrops()) {
				ItemMeta meta = item.getItemMeta();
				if(meta.getLore() != null && meta.getLore().contains(ChatColor.GRAY + "This item doesn't drop on death.")) {
					dontDropItems.add(item);
				}
			}
			event.getDrops().removeAll(dontDropItems);
			itemsToKeepOnRespawn.put(player.getName(), dontDropItems);
		}
		
		if(player.getWorld().getName().equalsIgnoreCase("revamp")) {
			event.setKeepInventory(false);
		}
	}
	
	@EventHandler
	public void onPlayerTakeDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player player) {
			
			if(Players.loginProtection.contains(player.getName())) {
				if(event.getCause() == DamageCause.SUFFOCATION) {
					if(player.getWorld().getName().contains("nether")) {
						PaperLib.teleportAsync(player, Travel.getGlobalPosition("Longdale"));
					} else {
						PaperLib.teleportAsync(player, player.getWorld().getHighestBlockAt(player.getLocation()).getLocation());
					}
				}
				event.setCancelled(true);
			}
			if(event.getCause() == DamageCause.SUFFOCATION
					&& (Regions.regionIsAtLocation("market", player.getLocation()) || Regions.regionIsAtLocation("longdale", player.getLocation()))) {
				//disable in market
				PaperLib.teleportAsync(player, player.getWorld().getHighestBlockAt(player.getLocation()).getLocation());
				event.setCancelled(true);
			}
			if(event.getCause() == DamageCause.DROWNING
					&& Regions.regionIsAtLocation("longdale", player.getLocation())) {
				event.setCancelled(true);
			}
			
			if(player.getAllowFlight() && !player.isOp()) {
				player.sendMessage(ChatColor.YELLOW + "[Fly] " + ChatColor.RED + "You were damaged - Flying mode disabled.");
				player.setFlying(false);
				player.setAllowFlight(false);
			}
			
			if(disguised.contains(player.getName())) {
				player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Your disguise was revealed!");
				setDisguised(player.getUniqueId(), false);
			}
			
			if(Admin.spectating.contains(player.getName())) {
				event.setCancelled(true);
			}
			
			//remove donator disguise (not zombie)
			if(DisguiseAPI.isDisguised(player)) {
				DisguiseAPI.undisguiseToAll(player);
				player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Your disguise was revealed!");
			}
			
		}
	}
	
	@EventHandler
	public void damageEntityInDisguise(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Player) {
			Player player = (Player) event.getDamager();
			
			if(disguised.contains(player.getName())) {
				
				if(player.getAllowFlight() && !player.isOp()) {
					player.sendMessage(ChatColor.YELLOW + "[Fly] " + ChatColor.RED + "You damaged an entity - Flying mode disabled.");
					player.setFlying(false);
					player.setAllowFlight(false);
				}
				
				player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Your disguise was revealed!");
				setDisguised(player.getUniqueId(), false);
				
			}
			
			//remove donator disguise (not zombie)
			if(DisguiseAPI.isDisguised(player)) {
				DisguiseAPI.undisguiseToAll(player);
				player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Your disguise was revealed!");
			}
			
		}
	}
	
	@EventHandler
	public void onHitPlayer(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Player && Admin.spectating.contains(event.getDamager().getName())) {
			Chat.sendTitleMessage(((Player) event.getDamager()).getUniqueId(), ChatColor.YELLOW + "[Spectate] " + ChatColor.WHITE + "Cannot interact while spectating.");
			event.setCancelled(true);
			return;
		}
	}
	
	@EventHandler
	public void InteractWithArmorStand(PlayerInteractAtEntityEvent event) {
		
		Player player = event.getPlayer();
		
		//shared to also setup leaderboard head locations:
		if(player.isOp() && Admin.lbhead_type != null) {
			FileConfiguration leaderboard = DeadMC.LeaderboardFile.data();
			UUID leader = UUID.fromString(leaderboard.getString(Admin.lbhead_type + "." + Admin.lbhead_place + ".UUID"));
			Entity entity = event.getRightClicked();
			
			if(entity.getType() != EntityType.ARMOR_STAND) {
				player.sendMessage("This isn't an armorstand.");
				return;
			}
			
			ArmorStand armorStand = (ArmorStand) entity;
			armorStand.setSmall(true);
			armorStand.setArms(true);
			armorStand.setBasePlate(false);
			//armorStand.setBodyPose(0);
			//set head pose
			
			//create leaderboard head:
			leaderboard.set(Admin.lbhead_type + "." + Admin.lbhead_place + ".HeadLoc", LocationCode.Encode(entity.getLocation(), true));
			
			ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
			SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
			skullMeta.setOwningPlayer(Bukkit.getOfflinePlayer(leader));
			skull.setItemMeta(skullMeta);
			armorStand.getEquipment().setHelmet(skull);
			armorStand.getEquipment().setItemInOffHand(new ItemStack(Material.SHIELD));
			
			final Hologram hologram = HologramsAPI.createHologram(plugin, entity.getLocation().add(0, 1.9, 0));
			PlayerConfig leaderConfig = PlayerConfig.getConfig(leader);
			String townRank = leaderConfig.getString("Town") == null ? "" : Town.getTownRankPrefix(leader);
			String townName = leaderConfig.getString("Town") == null ? "" : Town.getTownDisplayName(leader);
			String name = leaderConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(leader).getName() : leaderConfig.getString("Name");
			
			String place = "";
			if(Admin.lbhead_place == 1) {
				place = ChatColor.AQUA + "" + ChatColor.BOLD + "1st";
				armorStand.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				armorStand.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				armorStand.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
			}
			if(Admin.lbhead_place == 2) {
				place = ChatColor.GOLD + "" + ChatColor.BOLD + "2nd";
				armorStand.getEquipment().setChestplate(new ItemStack(Material.GOLDEN_CHESTPLATE));
				armorStand.getEquipment().setLeggings(new ItemStack(Material.GOLDEN_LEGGINGS));
				armorStand.getEquipment().setBoots(new ItemStack(Material.GOLDEN_BOOTS));
			}
			if(Admin.lbhead_place == 3) {
				place = ChatColor.WHITE + "" + ChatColor.BOLD + "3rd";
				armorStand.getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
				armorStand.getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
				armorStand.getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS));
			}
			
			hologram.appendTextLine(ChatColor.AQUA + "" + ChatColor.BOLD + place);
			if(leaderConfig.getString("Town") != null) hologram.appendTextLine(townRank + townName);
			hologram.appendTextLine(name);
			
			//create leaderboard title
			if(Admin.lbhead_place == 1) {
				Location blockBehind = entity.getWorld().getBlockAt(entity.getLocation()).getRelative(armorStand.getFacing().getOppositeFace()).getLocation();
				final Hologram title = HologramsAPI.createHologram(plugin, blockBehind.add(0.5, 5.5, 0.5));
				
				if(Admin.lbhead_title != null) {
					title.appendTextLine(ChatColor.GOLD + "" + ChatColor.ITALIC + "Top 10 - " + ChatColor.GOLD + ChatColor.BOLD + Admin.lbhead_title.replace("_", " "));
					leaderboard.set(Admin.lbhead_type + "." + Admin.lbhead_place + ".HologramTitle", Admin.lbhead_title);
				} else
					title.appendTextLine(ChatColor.GOLD + "" + ChatColor.ITALIC + "Top 10 - " + ChatColor.GOLD + ChatColor.BOLD + "set title");
				
				title.appendTextLine("Use " + ChatColor.GOLD + ChatColor.BOLD + "/lb " + Admin.lbhead_type.toLowerCase());
				title.appendTextLine(ChatColor.YELLOW + "=============================");
				for(int count = 1; count <= 10; count++) {
					if(leaderboard.getString(Admin.lbhead_type + "." + count + ".UUID") != null) {
						OfflinePlayer playerInPos = Bukkit.getOfflinePlayer(UUID.fromString(leaderboard.getString(Admin.lbhead_type + "." + count + ".UUID")));
						PlayerConfig posConfig = PlayerConfig.getConfig(playerInPos);
						String playerInPosName = playerInPos.getName();
						if(posConfig.getString("Name") != null)
							playerInPosName = posConfig.getString("Name");
						
						String amountString = "";
						if(Admin.lbhead_type.equalsIgnoreCase("coins"))
							amountString = Economy.convertCoins(leaderboard.getInt(Admin.lbhead_type + "." + count + ".Amount"));
						DecimalFormat formatter = new DecimalFormat("#,###");
						if(Admin.lbhead_type.equalsIgnoreCase("kills") || Admin.lbhead_type.equalsIgnoreCase("bmkills"))
							amountString = formatter.format(leaderboard.getInt(Admin.lbhead_type + "." + count + ".Amount")) + " zombies";
						if(Admin.lbhead_type.equalsIgnoreCase("playtime"))
							amountString = Math.round(leaderboard.getInt(Admin.lbhead_type + "." + count + ".Amount") / 60.0) + " hours";
						if(Admin.lbhead_type.equalsIgnoreCase("tasks"))
							amountString = leaderboard.getInt(Admin.lbhead_type + "." + count + ".Amount") + " tasks";
						if(Admin.lbhead_type.equalsIgnoreCase("streak"))
							amountString = leaderboard.getInt(Admin.lbhead_type + "." + count + ".Amount") + " tasks";
						if(Admin.lbhead_type.equalsIgnoreCase("bmleader"))
							amountString = leaderboard.getInt(Admin.lbhead_type + "." + count + ".Amount") + " nights";
						if(Admin.lbhead_type.equalsIgnoreCase("welcome"))
							amountString = leaderboard.getInt(Admin.lbhead_type + "." + count + ".Amount") + " players";
						if(Admin.lbhead_type.equalsIgnoreCase("votes") || Admin.lbhead_type.equalsIgnoreCase("votes_month"))
							amountString = leaderboard.getInt(Admin.lbhead_type + "." + count + ".Amount") + " votes";
						
						String number = ChatColor.GRAY + "" + count + ". ";
						if(count == 1)
							number = ChatColor.AQUA + "" + ChatColor.BOLD + count + ChatColor.GRAY + ". ";
						if(count == 2)
							number = ChatColor.GOLD + "" + ChatColor.BOLD + count + ChatColor.GRAY + ". ";
						if(count == 3)
							number = ChatColor.WHITE + "" + ChatColor.BOLD + count + ChatColor.GRAY + ". ";
						
						title.appendTextLine(number + ChatColor.WHITE + Chat.stripColor(playerInPosName) + " (" + ChatColor.GOLD + amountString + ChatColor.WHITE + ")");
					}
				}
				title.appendTextLine(ChatColor.YELLOW + "=============================");
			}
			
			//reset:
			Admin.lbhead_place = 0;
			Admin.lbhead_type = null;
			Admin.lbhead_title = null;
			
			DeadMC.LeaderboardFile.save();
			
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onOpenChestSilently(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
			if(Admin.spectating.contains(player.getName()) || Admin.vanished.contains(player.getName())) {
				if(event.getClickedBlock().getState() instanceof Chest) {
					Chest chest = (Chest) event.getClickedBlock().getState();
					Donator.invseeing.add(player.getName());
					
					Inventory inventory = Bukkit.createInventory(null, chest.getBlockInventory().getSize());
					inventory.setContents(chest.getBlockInventory().getContents());
					player.openInventory(inventory);
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Spectate] " + ChatColor.WHITE + "NOTE: Chest doesn't update if player is using it.");
					event.setCancelled(true);
				} else if(!Admin.vanished.contains(player.getName())) {
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Spectate] " + ChatColor.WHITE + "Cannot interact while spectating.");
					event.setCancelled(true);
					return;
				}
			}
			
		}
		
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		HumanEntity player = event.getPlayer();
		if(invseeing.contains(player.getName())) {
			
			Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
				@Override
				public void run() {
					if(event.getPlayer().getOpenInventory().getType() == InventoryType.CRAFTING)
						invseeing.remove(player.getName()); //wait to check if opening shulker box
				}
			}, 2L);
			
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		HumanEntity player = event.getWhoClicked();
		if(invseeing.contains(player.getName())) {
			if(!player.isOp())
				event.setCancelled(true);
		}
	}
	
	public void setDisguised(UUID uuid, Boolean disguise) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		long disguiseCooldown = DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.DisguiseCooldown");
		long disguiseLimit = DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.DisguiseLimit");
		
		Player player = Bukkit.getPlayer(uuid);
		
		if(disguise) {
			player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.GREEN + "You wear your zombie disguise ...");
			disguised.add(player.getName());
			
			MobDisguise mobDisguise = new MobDisguise(DisguiseType.getType(Entities.getRandomZombieType()));
			DisguiseAPI.disguiseToAll(player, mobDisguise);
			
			long cooldownWarning = disguiseLimit - 100;
			Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
				@Override
				public void run() {
					player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.WHITE + "Your disguise is wearing off!");
				}
			}, cooldownWarning);
			Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
				@Override
				public void run() {
					setDisguised(uuid, false);
					player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.RED + "Your disguise has worn off!");
				}
			}, disguiseLimit);
			
		} else {
			DisguiseAPI.undisguiseToAll(player);
			disguised.remove(player.getName());
			coolingDown.add(player.getName());
			
			Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
				@Override
				public void run() {
					coolingDown.remove(player.getName());
					player.sendMessage(ChatColor.YELLOW + "[Disguise] " + ChatColor.WHITE + "Your disguise is ready to use again.");
				}
			}, disguiseCooldown);
		}
		
	}
	
}
