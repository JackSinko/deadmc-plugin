package com.deadmc.DeadAPImaven;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.apache.commons.lang3.EnumUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Lectern;
import org.bukkit.block.data.type.Door;
import org.bukkit.block.data.type.Fence;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.lang.reflect.Field;
import java.util.*;


public class CustomItems implements Listener, CommandExecutor {
	private static DeadMC plugin;
	public CustomItems(DeadMC p) {
		plugin = p;
	}
	private int ticksToDealDamage = 200;
	private Map<String, Integer> damageTicks = new HashMap<String, Integer>();
	
	public enum CustomItem {
		BACKPACK,
		CLIMBING_BOOTS,
		DESTRUCTOR_ARROW,
		DESTRUCTOR_ARROW_BLOOD_MOON,
		QUICK_BOW_SINGLE_SHOT,
		QUICK_BOW_MULTI_SHOT,
		QUIVER_BASIC,
		QUIVER_STANDARD,
		QUIVER_EXTRA,
		QUIVER_ADVANCED,
		QUIVER_DONATOR,
		VOTING_KEY
	}
	
	//returns null if not a custom item
	public static String getCustomItemName(ItemStack itemStack) {
		if(itemStack.hasItemMeta() && itemStack.getItemMeta().getLore() != null && itemStack.getItemMeta().getLore().size() > 0) {
			String customItemName = ChatColor.stripColor(itemStack.getItemMeta().getDisplayName().toUpperCase().replace("(", "").replace(")", "").replace(" ", "_").replace("_DONATOR", ""));
			//Chat.debug(customItemName);
			if(EnumUtils.isValidEnum(CustomItems.CustomItem.class, customItemName)) {
				return customItemName;
			}
		}
		return null;
	}
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		if(sender instanceof Player player) {
			
			if(player.isOp() && commandLabel.equalsIgnoreCase("giveitem")) {
				CustomItem item = CustomItem.valueOf(args[0]);
				if(item == CustomItem.BACKPACK) {
					player.getInventory().addItem(CustomItems.backpack());
				}
				if(item == CustomItem.CLIMBING_BOOTS) {
					player.getInventory().addItem(climbingBoots(args.length >= 2 && args[1].equalsIgnoreCase("true")));
				}
				if(item == CustomItem.DESTRUCTOR_ARROW) {
					player.getInventory().addItem(destructorArrow(false));
				}
				if(item == CustomItem.DESTRUCTOR_ARROW_BLOOD_MOON) {
					player.getInventory().addItem(destructorArrow(true));
				}
				if(item == CustomItem.QUICK_BOW_SINGLE_SHOT) {
					player.getInventory().addItem(quickBow(QuickBowType.SINGLE, args.length >= 2 && args[1].equalsIgnoreCase("true"), args.length >= 3 ? Integer.parseInt(args[2]) : 0));
				}
				if(item == CustomItem.QUICK_BOW_MULTI_SHOT) {
					player.getInventory().addItem(quickBow(QuickBowType.MULTI, args.length >= 2 && args[1].equalsIgnoreCase("true"), args.length >= 3 ? Integer.parseInt(args[2]) : 0));
				}
				if(item.toString().contains("QUIVER")) {
					String tierUppercase = args[0].replace("QUIVER_", "");
					player.getInventory().addItem(createNewQuiver(QuiverTier.valueOf(tierUppercase.substring(0,1) + tierUppercase.substring(1,tierUppercase.length()).toLowerCase())));
				}
				if(item == CustomItem.VOTING_KEY) {
					player.getInventory().addItem(votingKey());
				}
			}
			
		}
		
		return true;
	}
	
	public static void giveItemSafely(UUID uuid, ItemStack itemStack) {
		Player player = Bukkit.getPlayer(uuid);

		if(player == null || Shops.getItemsCanFit(player, itemStack) == 0) {
			//wait until the player is online to give the item
			PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
			List<ItemStack> itemsToAdd = playerConfig.getString("ItemsToAdd") == null ? new ArrayList<ItemStack>() : (List<ItemStack>) playerConfig.getList("ItemsToAdd");
			itemsToAdd.add(itemStack.clone());
			playerConfig.set("ItemsToAdd", itemsToAdd);
			playerConfig.save();
			
			if(player != null) player.sendMessage(ChatColor.RED + "[DeadMC] Tried giving custom items, although your inventory is full. Empty your inventory and relog to retrieve the items.");
			
			Chat.debug(uuid.toString() + " could not take custom items that were given. Will add on next reload.");
			return;
		}
		
		player.getInventory().addItem(itemStack);
	}
	
	public static ItemStack getCustomItem(CustomItem item, boolean donator) {
		if(item == CustomItem.BACKPACK) {
			return backpack();
		}
		if(item == CustomItem.CLIMBING_BOOTS) {
			Material material = Material.LEATHER_BOOTS;
			ItemStack boots = new ItemStack(material);

			return boots;
		}
		if(item == CustomItem.DESTRUCTOR_ARROW) {
			return destructorArrow(false);
		}
		if(item == CustomItem.DESTRUCTOR_ARROW_BLOOD_MOON) {
			return destructorArrow(true);
		}
		if(item == CustomItem.QUICK_BOW_SINGLE_SHOT) {
			return quickBow(QuickBowType.SINGLE, donator, 0);
		}
		if(item == CustomItem.QUICK_BOW_MULTI_SHOT) {
			return quickBow(QuickBowType.MULTI, donator, 0);
		}
		if(item.toString().contains("QUIVER")) {
			String tierUppercase = item.toString().replace("QUIVER_", "");
			QuiverTier tier = QuiverTier.valueOf(tierUppercase.substring(0,1) + tierUppercase.toLowerCase().substring(1,tierUppercase.length()));
			return createNewQuiver(tier);
		}
		if(item == CustomItem.VOTING_KEY) {
			return votingKey();
		}
		return null;
	}
	
	public static ItemStack backpack() {
		ItemStack backpack = new ItemStack(Material.GRAY_SHULKER_BOX);
		ItemMeta meta = backpack.getItemMeta();
		meta.setDisplayName(ChatColor.DARK_PURPLE + "Backpack" + ChatColor.GRAY + " (Donator)");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Right click this item to open.");
		meta.setLore(lore);
		backpack.setItemMeta(meta);
		return backpack;
	}
	
	public static ItemStack destructorArrow(Boolean bloodmoon) {
		Material material = Material.TIPPED_ARROW;
		ItemStack arrows = new ItemStack(material);
		
		PotionMeta meta = (PotionMeta) arrows.getItemMeta();
		List<String> lore = new ArrayList<String>();
		if(bloodmoon) {
			meta.setBasePotionData(new PotionData(PotionType.INSTANT_DAMAGE));
			meta.setDisplayName(ChatColor.RED + "Destructor Arrow" + ChatColor.GRAY + " (" + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.GRAY + ")");
			lore.add(ChatColor.GRAY + " > Blows up on hit!");
			lore.add(ChatColor.GRAY + " > Spreads fire");
		}
		else {
			meta.setBasePotionData(new PotionData(PotionType.STRENGTH));
			meta.setDisplayName(ChatColor.RED + "Destructor Arrow");
			lore.add(ChatColor.GRAY + " > Blows up on hit!");
		}
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		
		meta.setLore(lore);
		
		int amount = (int) (Math.random() * 6) + 2; // 2-6 (inclusive)
		arrows.setAmount(amount);
		
		arrows.setItemMeta(meta);
		
		return arrows;
	}
	
	public static ItemStack climbingBoots(boolean donator) {
		Material material = Material.LEATHER_BOOTS;
		ItemStack boots = new ItemStack(material);
		ItemMeta meta = boots.getItemMeta();
		
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "These boots look like they were from a Climber...");
		
		int fallingLevelChance = (int) (Math.random() * 100); // 0-99 (inclusive)
		String rarityString = "";
		if(donator) {
			rarityString = " (Donator)";
			lore.add("This is a donator item, and therefore never degrades.");
		} else {
			int durability = (int)(Math.random() * boots.getType().getMaxDurability())+1; // 1-100 (inclusive)
			((Damageable)meta).setDamage(durability);
			
			if(fallingLevelChance == 99) { //1%
				rarityString = " (Very Rare)";
			} else if(fallingLevelChance >= 89) { //10%
				rarityString = " (Rare)";
			} else if(fallingLevelChance >= 74) { //20%
				rarityString = " (Uncommon)";
			}
		}

		meta.setDisplayName(ChatColor.DARK_PURPLE + "Climbing Boots" + ChatColor.GRAY + rarityString);
		meta.setLore(lore);
		boots.setItemMeta(meta);
		
		if(donator) {
			boots.addEnchantment(Enchantment.PROTECTION_FALL, 4);
		} else {
			if(fallingLevelChance == 99) { //1%
				boots.addEnchantment(Enchantment.PROTECTION_FALL, 4);
			} else if(fallingLevelChance >= 89) { //10%
				boots.addEnchantment(Enchantment.PROTECTION_FALL, 3);
			} else if(fallingLevelChance >= 74) { //20%
				boots.addEnchantment(Enchantment.PROTECTION_FALL, 2);
			} else boots.addEnchantment(Enchantment.PROTECTION_FALL, 1);
		}
		
		return boots;
	}

	public enum QuickBowType {
		SINGLE,
		MULTI
	}
	public static ItemStack quickBow(QuickBowType type, boolean donator, int durability) {
		Material material = Material.CROSSBOW;
		ItemStack bow = new ItemStack(material);
		
		ItemMeta meta = bow.getItemMeta();
		meta.setDisplayName(ChatColor.DARK_RED + "Quick Bow" + ChatColor.GRAY + ( " (" + type.toString().toLowerCase() + " shot)") + (donator ?  ChatColor.GRAY + " (Donator)" : ""));
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "This looks like a powerful weapon!");
		lore.add(ChatColor.GRAY + " > Auto reloading functionality.");
		if(type == QuickBowType.SINGLE)
			lore.add(ChatColor.GRAY + " > Single shot: Good accuracy");
		if(type == QuickBowType.MULTI)
			lore.add(ChatColor.GRAY + " > Multi shot: Poor accuracy");
		if(donator) lore.add("This is a donator item, and therefore never degrades.");
		meta.setLore(lore);
		
		((Damageable)meta).setDamage(durability);
		
		bow.setItemMeta(meta);
		
		return bow;
	}
	
	public static boolean isQuickBow(ItemStack item) {
		return item != null && item.getType() == Material.CROSSBOW && item.getItemMeta().hasLore() && item.getItemMeta().getLore().size() > 0 && item.getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.GRAY + "This looks like a powerful weapon!");
	}
	
	public static ItemStack votingKey() {
		ItemStack key = new ItemStack(Material.TRIPWIRE_HOOK);
		ItemMeta meta = key.getItemMeta();
		meta.setDisplayName(ChatColor.DARK_PURPLE + "Voting Key");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "" + ChatColor.ITALIC + "Thanks so much for voting!");
		meta.setLore(lore);
		key.setItemMeta(meta);
		return key;
	}
	
	//the lore and displayname carries over
	public static void openHelpBook(Player player) {
		
		boolean playerIsJava = Tools.playerIsJava(player);
		
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta bookMeta = (BookMeta) book.getItemMeta();

		//create a page
		BaseComponent[] page = new ComponentBuilder()
				
				.append("         * * *\n").color(net.md_5.bungee.api.ChatColor.DARK_RED).bold(true)
				.append("       DEAD").color(net.md_5.bungee.api.ChatColor.DARK_RED).bold(true)
				.append("MC\n").color(net.md_5.bungee.api.ChatColor.DARK_GRAY).bold(true)
				.append("         * * *\n\n").color(net.md_5.bungee.api.ChatColor.DARK_RED).bold(true)
				
				//run command
				.append(new ComponentBuilder("Click me")
				.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "tp"))
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Go to the spigot website!").create()))
						.bold(false).underlined(false).create())
				
				//end by creating the page
				.create();
		
		//create a page
		BaseComponent[] page2 = new ComponentBuilder("Some more text on a new page").create();
		
		//add the page to the meta
		bookMeta.spigot().addPage(page);
		bookMeta.spigot().addPage(page2);
		
		bookMeta.setTitle("Interactive Book");
		bookMeta.setAuthor("DeadMC");
		
		book.setItemMeta(bookMeta);
		
		if(playerIsJava) {
			player.openBook(book);
		} else {
			player.sendMessage(ChatColor.YELLOW + "[Help] " + ChatColor.WHITE + "Book added to inventory.");
			player.getInventory().addItem(book);
		}
	}
	
	
	//tier 1: holds 9 * 64 = 576 - starting
	//tier 2: holds 18 * 64 = 1152 - rare - picks up directly into quiver
	//tier 3: holds 27 * 64 = 1728 - very rare - picks up directly into quiver - 20% chance of arrow magneticing back into quiver
	//tier 4: holds 54 * 64 = 3456 (donator) - picks up directly into quiver = 50% chance of arrow magneticing back into quiver
	
	public enum QuiverTier {
		Basic,
		Standard,
		Extra,
		Advanced,
		Donator
	}
	
	private static ArrayList<Integer> quiverInventorySizes = new ArrayList<Integer>(Arrays.asList(
			5, 9, 18, 27, 54));
	private static ArrayList<Integer> quiverRetractionChances = new ArrayList<Integer>(Arrays.asList(
			0, 10, 20, 30, 50));
	
	//the lore and displayname carries over
	public static ItemStack createNewQuiver(QuiverTier tier) {
		
		ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
		ItemMeta meta = head.getItemMeta();

		meta.setDisplayName(ChatColor.DARK_PURPLE + "Quiver" + ChatColor.GRAY + " (" + tier.toString() + ")");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "This looks useful to store arrows.");
		lore.add(ChatColor.GRAY + " > Arrows inside: " + ChatColor.BOLD + "0");
		lore.add(ChatColor.GRAY + " > Max storage: " + ChatColor.BOLD + (quiverInventorySizes.get(tier.ordinal()) * 64));
		lore.add(ChatColor.GRAY + " > Picks up arrows directly into quiver.");
		if(quiverRetractionChances.get(tier.ordinal()) > 0) lore.add(ChatColor.GRAY + " > " + ChatColor.BOLD + quiverRetractionChances.get(tier.ordinal()) + "%" + ChatColor.GRAY + " chance arrows will retract back when shot.");
		if(tier == QuiverTier.Donator) lore.add("This is a donator item, and therefore never degrades.");
		meta.setLore(lore);
		
		head.setItemMeta(meta);
		
		setQuiverIcon(head, true);
		
		return head;
	}
	
	//events:

	@EventHandler
	private void onClickVotingChest(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block block = event.getClickedBlock();
			if(block.getType() == Material.ENDER_CHEST) {
				String locationCode = LocationCode.Encode(block.getLocation()).replace(".", "").replace("|", "");

				//admin first:
				if(player.isOp() && player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_AXE) {

					if(DeadMC.RestartFile.data().getStringList("VoteChests").contains(locationCode)) {
						//remove chest
						List<String> chestLocations = DeadMC.RestartFile.data().getStringList("VoteChests");
						chestLocations.remove(locationCode);
						DeadMC.RestartFile.data().set("VoteChests", chestLocations);

						//remove hologram
						Bukkit.dispatchCommand(player, "hd delete " + locationCode);

						player.sendMessage(ChatColor.RED + "Removed voting chest.");
					} else {
						//add chest
						List<String> chestLocations = DeadMC.RestartFile.data().getStringList("VoteChests");
						chestLocations.add(locationCode);
						DeadMC.RestartFile.data().set("VoteChests", chestLocations);

						//create hologram - requires HolographicExtension for placeholder support
						Bukkit.dispatchCommand(player, "hd create " + locationCode + " &2&k_ &5&lVoting Crate &2&k_");
						Bukkit.dispatchCommand(player, "hd addline " + locationCode + " &6&l/vote&f daily for keys!");

						player.sendMessage(ChatColor.GREEN + "Added voting chest.");
					}
					DeadMC.RestartFile.save();

					event.setCancelled(true);
					return;
				}

				//player:
				if(DeadMC.RestartFile.data().getStringList("VoteChests").contains(locationCode)) {
					//is a voting chest
					event.setCancelled(true);
					
					if(Shops.getPlayersStacks(player, votingKey()).size() == 0) {
						player.sendMessage(ChatColor.YELLOW + "[Voting] " + ChatColor.WHITE + "You don't have any " + ChatColor.DARK_PURPLE + "Voting Keys" + ChatColor.WHITE+ ".");
						return;
					}
					
					if(player.getInventory().firstEmpty() == -1) {
						player.sendMessage(ChatColor.YELLOW + "[Voting] " + ChatColor.WHITE + "You don't have any free space in your inventory.");
						return;
					}
					
					player.getInventory().removeItem(votingKey());
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "dmc givecoins " + player.getName() + " random");
					
					//random reward:
					int reward = 0;
					int random = (int) (Math.random() * 100) + 1; // 1-100 (inclusive)
					if(random < 10) reward = 2; //very rare
					else if(random < 50) reward = 1; //rare
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "dmc votereward " + player.getName() + " " + reward);
					
					player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_CHIME, 1, 1f);
					
					player.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, event.getClickedBlock().getLocation() ,8, 0.2F, 0F, 0.2F);
					Firework f = player.getWorld().spawn(event.getClickedBlock().getLocation().add(0.5, 1, 0.5), Firework.class);
					FireworkMeta fm = f.getFireworkMeta();
					fm.addEffect(FireworkEffect.builder()
							.flicker(true)
							.trail(true)
							.with(FireworkEffect.Type.BALL_LARGE)
							.withColor(Color.PURPLE).build());
					fm.setPower((int) 0.5);
					f.setFireworkMeta(fm);
				}

			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void clickQuiverEvent(InventoryClickEvent event) {
		ItemStack item = event.getCurrentItem();

		if(event.getWhoClicked() instanceof Player player) {
			if(itemsInSlot17.containsKey(player.getName())) restoreQuiverSlot(player, true);
			
			//check if clicked a quiver
			if(isQuiver(item)) {
				if(player.getItemOnCursor() != null && !player.getItemOnCursor().getType().isAir()) {
					//add the arrows
					if(player.getItemOnCursor().getType() == Material.ARROW) {
						addArrowsToQuiver(player, item, player.getItemOnCursor());
						if(openedQuivers.containsKey(player.getName())) {
							openQuiver(player, item); //reopen
						}
						event.setCancelled(true);
						return;
					}
					if(event.isRightClick()) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.RED + "The quiver can only fit " + ChatColor.BOLD + "ordinary arrows" + ChatColor.WHITE + ".");
						event.setCancelled(true);
						return;
					}
				}
				
				//open the quiver
				if(event.isRightClick() && !openedQuivers.containsKey(player.getName())) {
					openQuiver(player, item);
					event.setCancelled(true);
					return;
				}
			}
			
			if(event.getView().getTitle().contains("Quiver")) {
				if(openedQuivers.containsKey(player.getName())) {
					
					if(((event.getCurrentItem() == null || event.getCurrentItem().getType() != Material.ARROW) && (event.getCursor() == null || event.getCursor().getType() != Material.ARROW))
							|| event.getAction() == InventoryAction.SWAP_WITH_CURSOR || event.getAction() == InventoryAction.HOTBAR_MOVE_AND_READD) {
						if((event.getCurrentItem() != null && event.getCurrentItem().getType() != Material.AIR) || (event.getCursor() != null && event.getCursor().getType() != Material.AIR)) Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.RED + "The quiver can only fit " + ChatColor.BOLD + "ordinary arrows" + ChatColor.WHITE + ".");
						event.setCancelled(true);
						return;
					}
					
					//moving arrows
					updateQuiverAfterMovingArrows(player, event.getView().getTopInventory());
				} else {
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.RED + "This is not your quiver.");
					event.setCancelled(true);
					return;
				}
			}
		}
	}
	
	@EventHandler
	private void dragItemsInQuiver(InventoryDragEvent event) {
		if(event.getWhoClicked() instanceof Player player) {
			if(event.getView().getTitle().contains("Quiver")) {
				if(openedQuivers.containsKey(player.getName())) {
					updateQuiverAfterMovingArrows(player, event.getView().getTopInventory());
				}
			}
			if(itemsInSlot17.containsKey(player.getName())) restoreQuiverSlot(player, true);
		}
	}
	
	private void updateQuiverAfterMovingArrows(Player player, Inventory inventory) {
		ItemStack quiver = openedQuivers.get(player.getName());
		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			int difference = setQuiverStock(quiver, getTotalArrowsInInventory(inventory));
			if(difference != 0)
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.WHITE + (difference > 0 ? "Added " : "Removed ") + Math.abs(difference) + " arrows. (" + ChatColor.BOLD + getQuiverStock(quiver) + ChatColor.WHITE + " / " + getQuiverMaxStock(quiver) + ")");
		}, 1L);
	}
	
	private static int getTotalArrowsInInventory(Inventory inventory) {
		int stock = 0;
		for(ItemStack item : inventory.getContents()) {
			if(item != null && item.getType() == Material.ARROW) {
				stock += item.getAmount();
			}
		}
		return stock;
	}

	public static HashMap<String, ItemStack> openedQuivers = new HashMap<String, ItemStack>(); //player name, quiver item
	
	public static void openQuiver(Player player, ItemStack quiver) {
		if(Donator.invseeing.contains(player.getName())) {
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.RED + "Cannot open while invseeing.");
			return;
		}
		
		QuiverTier tier = getQuiverTier(quiver);
		
		int inventorySize = (int) (9*(Math.ceil(quiverInventorySizes.get(tier.ordinal()) / (double)9)));
		Inventory inventory = Bukkit.createInventory(null, inventorySize, quiver.getItemMeta().getDisplayName());
		int blockedSlots = inventorySize - quiverInventorySizes.get(tier.ordinal());
		boolean lastWasLeft = false;
		int left = 0;
		int right = 8;
		for(int count = 0; count < blockedSlots; count++) {
			if(lastWasLeft) {
				//do right
				inventory.setItem(right, TravelGUI.getIcon(player, TravelGUI.Icon.HEADER_LINE));
				right--;
				lastWasLeft = false;
			} else {
				//do left
				inventory.setItem(left, TravelGUI.getIcon(player, TravelGUI.Icon.HEADER_LINE));
				left++;
				lastWasLeft = true;
			}
		}
		
		Bukkit.getScheduler().runTask(plugin, () -> {
			player.openInventory(inventory);
			if(openedQuivers.containsKey(player.getName())) {
				//this should never happen, but in case it could happen, just keep reference of the most recently opened quiver
				openedQuivers.replace(player.getName(), quiver);
			} else openedQuivers.put(player.getName(), quiver);
		}); //can't open inventory on same tick as we close the current inventory first
		Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.WHITE + "Opened quiver.");
		
		int totalStock = getQuiverStock(quiver);
		int added = 0;
		while(added < totalStock) {
			int amountToAdd = totalStock - added;
			if(amountToAdd > Material.ARROW.getMaxStackSize())
				amountToAdd = Material.ARROW.getMaxStackSize();
			
			inventory.addItem(new ItemStack(Material.ARROW, amountToAdd));
			added += amountToAdd;
		}
	}
	
	@EventHandler
	public void onQuiverDropEvent(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
		ItemStack item = event.getItemDrop().getItemStack();
		if(openedQuivers.containsKey(player.getName()) && item.hasItemMeta() && item.getItemMeta().hasLore() && openedQuivers.get(player.getName()).getItemMeta().getLore().equals(item.getItemMeta().getLore())) {
			//if player drops the quiver while open, close the inventory
			Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.WHITE + "Closed quiver.");
		}
		if(itemsInSlot17.containsKey(player.getName())) restoreQuiverSlot(player, true);
	}
	
	//check if it works with invsee
	
	@EventHandler
	public void onQuiverInventoryClose(InventoryCloseEvent event) {
		if(event.getPlayer() instanceof Player player) {
			if(openedQuivers.containsKey(player.getName())) {
				openedQuivers.remove(player.getName());
				Bukkit.getScheduler().runTaskLater(plugin, () -> {
					if(!openedQuivers.containsKey(player.getName())) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.WHITE + "Closed quiver.");
					}
				}, 1L);
			}
		}
	}
	
	public static boolean isQuiver(ItemStack item) {
		return item != null && item.getType() == Material.PLAYER_HEAD && item.getItemMeta().hasLore() && item.getItemMeta().getLore().size() > 0 && item.getItemMeta().getLore().get(0).equalsIgnoreCase(ChatColor.GRAY + "This looks useful to store arrows.");
	}
	
	public static QuiverTier getQuiverTier(ItemStack quiver) {
		int maxStock = getQuiverMaxStock(quiver);
		return QuiverTier.values()[quiverInventorySizes.indexOf(maxStock/Material.ARROW.getMaxStackSize())];
	}
	
	public static int getQuiverStock(ItemStack quiver) {
		String stripped = ChatColor.stripColor(quiver.getItemMeta().getLore().get(1));
		return Integer.parseInt(stripped.substring(18));
	}
	
	public static int getQuiverMaxStock(ItemStack quiver) {
		String stripped = ChatColor.stripColor(quiver.getItemMeta().getLore().get(2));
		return Integer.parseInt(stripped.substring(16));
	}
	
	public static int getQuiverRetraction(ItemStack quiver) {
		QuiverTier tier = getQuiverTier(quiver);
		return quiverRetractionChances.get(tier.ordinal());
	}
	
	public static int setQuiverStock(ItemStack quiver, int newStock) {
		int oldStock = getQuiverStock(quiver);
		
		ItemMeta meta = quiver.getItemMeta();
		List<String> lore = meta.getLore();
		lore.set(1, ChatColor.GRAY + " > Arrows inside: " + ChatColor.BOLD + newStock);
		meta.setLore(lore);
		quiver.setItemMeta(meta);
		
		if(newStock == 0 && oldStock != 0) {
			//quiver became empty
			setQuiverIcon(quiver, true);
		}
		if(newStock != 0 && oldStock == 0) {
			//update icon to no longer empty
			setQuiverIcon(quiver, false);
		}
		
		int difference = newStock - oldStock;
		return difference;
	}
	
	public static void setQuiverIcon(ItemStack quiver, boolean empty) {
		SkullMeta meta = (SkullMeta) quiver.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		byte[] encodedData = Base64.getEncoder().encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", empty ? "http://textures.minecraft.net/texture/5e5dfa4478acca48a8371eeedc5a2f569896ac987c06eee77a6a327eb02cc136" : "http://textures.minecraft.net/texture/68392651a1a4cbb5982b13ace0829a75b98c11e13b68868fe349aea24014ffbd").getBytes());
		profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
		Field profileField = null;
		try {
			profileField = meta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(meta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		quiver.setItemMeta(meta);
	}
	
	//leaves remaining in the stack
	public static void addArrowsToQuiver(Player player, ItemStack quiver, ItemStack arrows) {
		int remaining = addArrowsToQuiver(player, quiver, arrows.getAmount());
		arrows.setAmount(remaining); //take the arrows
	}
	
	//returns the remaining amount
	public static int addArrowsToQuiver(Player player, ItemStack quiver, int amountToAdd) {
		//add the arrows amount to the quiver stock
		//remove the arrows
		
		QuiverTier tier = getQuiverTier(quiver);
		int previousStock = getQuiverStock(quiver);
		int maxStock = getQuiverMaxStock(quiver);
		int freeSpace = maxStock - previousStock;
		
		if(freeSpace == 0) {
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.RED + "Full!");
			return amountToAdd;
		}
		
		int amountAdded = amountToAdd > freeSpace ? freeSpace : amountToAdd;

		int newStock = previousStock + amountAdded;
		setQuiverStock(quiver, newStock);

		Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.WHITE + "Added " + amountAdded + " arrows. (" + ChatColor.BOLD + newStock + ChatColor.WHITE + " / " + maxStock + ")");
		int remaining = amountToAdd - amountAdded;
		return remaining;
	}
	
	//leaves remaining in the stack
	public static void addArrowsToQuivers(Player player, List<ItemStack> quivers, ItemStack arrows) {
		int remaining = addArrowsToQuivers(player, quivers, arrows.getAmount());
		arrows.setAmount(remaining);
	}
	
	//returns the remaining amount
	public static int addArrowsToQuivers(Player player, List<ItemStack> quivers, int amountToAdd) {
		int amountNeededToAdd = amountToAdd;
		for(ItemStack quiver : quivers) {
			if(amountNeededToAdd == 0) return 0;
			amountNeededToAdd = addArrowsToQuiver(player, quiver, amountNeededToAdd);
		}
		return amountNeededToAdd;
	}
	
	public static void takeArrowsFromQuiver(Player player, ItemStack quiver, int amount) {
		QuiverTier tier = getQuiverTier(quiver);
		int previousStock = getQuiverStock(quiver);
		int maxStock = getQuiverMaxStock(quiver);
		int freeSpace = maxStock - previousStock;

		int newStock = previousStock - amount;
		if(newStock < 0) newStock = 0;
		setQuiverStock(quiver, newStock);
		
		if(previousStock != 0 && newStock == 0) {
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.RED + ChatColor.BOLD + "Empty!");
		} else {
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.WHITE + "Taken " + amount + " arrows. (" + ChatColor.BOLD + newStock + ChatColor.WHITE + " / " + maxStock + ")");
		}
	}
	
	public static HashMap<String, ItemStack> itemsInSlot17 = new HashMap<String, ItemStack>();
	public static HashMap<String, ItemStack> quiversInUse = new HashMap<String, ItemStack>();
	
	@EventHandler
	public void onClickWithQuiver(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		ItemStack item = event.getItem();
		if(((event.getAction() == Action.RIGHT_CLICK_BLOCK && !event.getClickedBlock().getType().toString().contains("SIGN") && !event.getClickedBlock().getType().toString().contains("CHEST")) || event.getAction() == Action.RIGHT_CLICK_AIR)
				&& isQuiver(item)) {
			openQuiver(player, item);
			event.setCancelled(true);
			return;
		}
		
		if((event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)
		&& item != null && (item.getType() == Material.BOW || item.getType() == Material.CROSSBOW) && (!isQuickBow(item) || !((CrossbowMeta)item.getItemMeta()).hasChargedProjectiles())) {
			ItemStack quiver = canUseQuiver(player, item);
			if(quiver != null) {
				ItemStack itemInSlot = player.getInventory().getItem(17);
				
				//save the item to a hashmap - can be null
				if(itemsInSlot17.containsKey(player.getName()))
					restoreQuiverSlot(player, true); // if for some reason the last item hasn't been restored yet, restore it first - can only have one quiver arrow in use at a time
				itemsInSlot17.put(player.getName(), itemInSlot);
				quiversInUse.put(player.getName(), quiver);
				
				player.getInventory().setItem(17, new ItemStack(Material.ARROW, 1));
				takeArrowsFromQuiver(player, quiver, 1);
			}
		}
	}
	
	@EventHandler
	private void arrowRetractToQuiver(ProjectileHitEvent event) {
		if(event.getEntity().getShooter() instanceof Player player) {
			if(event.getEntity() instanceof Arrow arrow) {
				if(arrow.getPickupStatus() == Arrow.PickupStatus.ALLOWED) { //don't include multi shot arrows etc.
					//get the quiver with the highest retraction in inventory
					ItemStack quiverWithHighestRetraction = null;
					int highestRetraction = 0;
					for(ItemStack quiver : getPlayersQuivers(player)) {
						int previousStock = getQuiverStock(quiver);
						int maxStock = getQuiverMaxStock(quiver);
						int freeSpace = maxStock - previousStock;
						
						if(freeSpace > 0) {
							int retraction = getQuiverRetraction(quiver);
							if(quiverWithHighestRetraction == null || retraction > highestRetraction) {
								quiverWithHighestRetraction = quiver;
								highestRetraction = retraction;
							}
						}
					}
					
					if(highestRetraction > 0) {
						int retractionChance = (int) (Math.random() * 100) + 1; // 0-99 (inclusive)
						if(retractionChance <= highestRetraction) {
							ItemStack finalQuiverWithHighestRetraction = quiverWithHighestRetraction;
							Bukkit.getScheduler().runTaskLater(plugin, () -> {
								if(event.getEntity() != null) {
									addArrowsToQuiver(player, finalQuiverWithHighestRetraction, 1);
									int newStock = getQuiverStock(finalQuiverWithHighestRetraction);
									int maxStock = getQuiverMaxStock(finalQuiverWithHighestRetraction);
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.WHITE + "Retracted 1 arrow. (" + ChatColor.BOLD + newStock + ChatColor.WHITE + " / " + maxStock + ")");
									event.getEntity().remove();
									player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
								}
							}, 20L);
						}
					}
				}
			}
		}
	}
	
	//2. PICK UP ARROWS DIRECTLY INTO QUIVER (if enough space):
	
	public static void pickupItemsWithFullInventory() {
		Bukkit.getScheduler().runTaskTimer(plugin, () -> {
			for(Player player : Bukkit.getOnlinePlayers()) {
				boolean inventoryIsFull = player.getInventory().firstEmpty() == -1;
				if(inventoryIsFull) {
					List<Entity> nearbyEntities = player.getNearbyEntities(1.02, 1.02, 1.02);
					for(Entity entity : nearbyEntities) {
						if(entity instanceof Item item
						&& item.getItemStack().getType() == Material.ARROW) {
							//try pickup:
							List<ItemStack> quivers = getPlayersQuivers(player);
							if(quivers.size() > 0) {
								int oldAmount = item.getItemStack().getAmount();
								addArrowsToQuivers(player, quivers, item.getItemStack());
								int newAmount = item.getItemStack().getAmount();
								if(newAmount != oldAmount) {
									player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
								}
							}
						}
						if(entity instanceof Arrow arrow
						&& arrow.getTicksLived() > 20) {
							List<ItemStack> quivers = getPlayersQuivers(player);
							if(quivers.size() > 0) {
								int remaining = addArrowsToQuivers(player, quivers, 1);
								if(remaining == 0) {
									player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
									arrow.remove();
								}
							}
						}
					}
				}
			}
		}, 20L, 20L);
	}
	
	@EventHandler
	private void pickupArrowToQuiver(PlayerPickupArrowEvent event) {
		event.setCancelled(false);
		Player player = event.getPlayer();
		List<ItemStack> quivers = getPlayersQuivers(player);
		if(quivers.size() > 0) {
			int remainingAfterAddingToQuivers = addArrowsToQuivers(player, quivers, 1);
			if(remainingAfterAddingToQuivers == 0) {
				event.setCancelled(true);
				event.getArrow().remove();
				player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
			}
		}
	}
	
	//test with backpacks and shulkers
	//test with shops
	//
	
	@EventHandler
	private void pickupArrowItemStackToQuiver(EntityPickupItemEvent event) {
		ItemStack item = event.getItem().getItemStack();
		if(event.getEntity() instanceof Player player
		&& item.getType() == Material.ARROW) {
			List<ItemStack> quivers = getPlayersQuivers(player);
			if(quivers.size() > 0) {
				addArrowsToQuivers(player, quivers, item);
				if(item.getAmount() == 0) {
					event.getItem().remove();
					player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
					event.setCancelled(true);
				} else {
					//allow to pickup naturally
				}
			}
		}
	}
	
	@EventHandler
	private void playerDisconnectWithSavedQuiverSlot(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if(itemsInSlot17.containsKey(player.getName())) restoreQuiverSlot(player, true);
	}
	
	@EventHandler
	private void playerSwapItemHeldWithSavedQuiverSlot(PlayerItemHeldEvent event) {
		Player player = event.getPlayer();
		if(itemsInSlot17.containsKey(player.getName())) restoreQuiverSlot(player, true);
	}
	
	public static void restoreQuiverSlot(Player player, boolean canReturnArrowToQuiver) {
		try {
			//check first that the player is online,
			//the player has a quiver arrow out (items in slot contains player)

			if(player.getInventory().getItem(17) != null && player.getInventory().getItem(17).getType() == Material.ARROW && canReturnArrowToQuiver) {
				//give back the arrow
				ItemStack quiver = quiversInUse.get(player.getName());
				addArrowsToQuiver(player, quiver, 1);
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Quiver] " + ChatColor.WHITE + "Returned 1 arrow. (" + ChatColor.BOLD + getQuiverStock(quiver) + ChatColor.WHITE + " / " + getQuiverMaxStock(quiver) + ")");
			}
			player.getInventory().setItem(17, itemsInSlot17.get(player.getName()));
			
			itemsInSlot17.remove(player.getName());
			quiversInUse.remove(player.getName());
		} catch(Exception e) {
			Chat.logError(e);
		}
	}
	
	//check if quiver arrow should be returned:
	@EventHandler
	private void onShootQuiverArrow(EntityShootBowEvent event) {
		if(event.getEntity() instanceof Player player) {
			if(itemsInSlot17.containsKey(player.getName())) {
				restoreQuiverSlot(player, false);
			}
		}
	}
	
	private static List<ItemStack> getPlayersQuivers(Player player) {
		List<ItemStack> quivers = new ArrayList<ItemStack>();
		for(ItemStack item : player.getInventory().getContents()) {
			if(item == null) continue;
			if(isQuiver(item)) {
				quivers.add(item);
			}
		}
		return quivers;
	}
	
	//returns the quiver to use - null if can't use
	private static ItemStack canUseQuiver(Player player, ItemStack bow) {
		//if there's a quiver in the inven,
		//and the quiver has more than 1 arrow
		//and no arrows in the inven,
		
		boolean isCrossbow = bow.getType() == Material.CROSSBOW;
		if(isCrossbow) {
			CrossbowMeta crossbowMeta = (CrossbowMeta) bow.getItemMeta();
			if(player.getInventory().getItemInOffHand().getType() == Material.FIREWORK_ROCKET
				|| (!isQuickBow(bow) && crossbowMeta.hasChargedProjectiles())) { //normal crossbow
				return null;
			}
		}
		
		ItemStack hasQuiverWithArrows = null;
		for(ItemStack item : player.getInventory().getContents()) {
			if(item == null) continue;
			if(item.getType().toString().contains("ARROW")) {
				return null;
			}
			if(isQuiver(item) && getQuiverStock(item) > 0) {
				hasQuiverWithArrows = item;
			}
		}
		//must wait until the loop is over to check if no arrows in inven
		return hasQuiverWithArrows;
	}

	//special boots:
	@EventHandler
	public void playerMoveEvent(PlayerMoveEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				Player player = event.getPlayer();
				
				//CLIMBING BOOTS:
				if((player.getEquipment().getBoots() != null
						&& player.getEquipment().getBoots().getItemMeta().getDisplayName() != null
						&& player.getEquipment().getBoots().getItemMeta().getDisplayName().contains("Climbing Boots"))
						&& (player.getEquipment().getBoots().getItemMeta().getLore() != null && player.getEquipment().getBoots().getItemMeta().getLore().size() > 0
						&& player.getEquipment().getBoots().getItemMeta().getLore().contains(ChatColor.GRAY + "These boots look like they were from a Climber..."))) {
					Block blockInFrontOfEntity = player.getLocation().getBlock().getRelative(player.getFacing());
					
					if(blockInFrontOfEntity.getType().isSolid() && (blockInFrontOfEntity.getType() != Material.SNOW
							&& !(blockInFrontOfEntity.getBlockData() instanceof Slab) && !(blockInFrontOfEntity.getBlockData() instanceof Stairs) && !(blockInFrontOfEntity.getBlockData() instanceof Fence) && !(blockInFrontOfEntity.getBlockData() instanceof Door))) {
						
						Bukkit.getScheduler().runTask(plugin, new Runnable() {
							@Override
							public void run() {
								player.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, Integer.MAX_VALUE, 3, false, false, false));
							}
						});
						//do damage to boots?
						if(player.getEquipment().getBoots() != null && !player.getEquipment().getBoots().getItemMeta().getLore().contains("This is a donator item, and therefore never degrades.")) {
							if(!damageTicks.containsKey(player.getName()))
								damageTicks.put(player.getName(), 1);
							else
								damageTicks.replace(player.getName(), damageTicks.get(player.getName()) + 1);
							
							if(damageTicks.get(player.getName()) > ticksToDealDamage) {
								
								damageTicks.replace(player.getName(), 0);
								
								Damageable meta = (Damageable) player.getEquipment().getBoots().getItemMeta();
								int newDamage = meta.getDamage() + 1;
								double maxDurability = player.getEquipment().getBoots().getType().getMaxDurability();
								int warningPercent = 90;
								int damagePercent = (int) ((double) (newDamage / maxDurability) * 100);
								if(warningPercent == damagePercent)
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Item] " + ChatColor.WHITE + "Your Climbing Boots have " + ChatColor.BOLD + "10%" + ChatColor.WHITE + " durability!");
								if(newDamage >= maxDurability) {
									Bukkit.getScheduler().runTask(plugin, new Runnable() {
										@Override
										public void run() {
											player.getEquipment().setBoots(new ItemStack(Material.AIR));
											player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1f, 8f);
										}
									});
									player.sendMessage(ChatColor.YELLOW + "[Item] " + ChatColor.RED + "Your Climbing Boots broke from wear!");
								} else {
									meta.setDamage(newDamage);
									Bukkit.getScheduler().runTask(plugin, new Runnable() {
										@Override
										public void run() {
											player.getEquipment().getBoots().setItemMeta((ItemMeta) meta);
										}
									});
								}
								
							}
						}
						
					} else if(player.hasPotionEffect(PotionEffectType.LEVITATION)) {
						Bukkit.getScheduler().runTask(plugin, new Runnable() {
							@Override
							public void run() {
								player.removePotionEffect(PotionEffectType.LEVITATION);
							}
						});
					}
				} else if(player.hasPotionEffect(PotionEffectType.LEVITATION)) {
					Bukkit.getScheduler().runTask(plugin, new Runnable() {
						@Override
						public void run() {
							player.removePotionEffect(PotionEffectType.LEVITATION);
						}
					});
				}
			}
		});
	}
	
	@EventHandler
	private void onItemDamage(PlayerItemDamageEvent event) {
		if(event.getItem().getItemMeta().getLore() != null && event.getItem().getItemMeta().getLore().contains("This is a donator item, and therefore never degrades."))
			event.setCancelled(true);
	}

	//instant bow:
	@EventHandler
	private void onShootBow(EntityShootBowEvent event) {
		if(event.getEntity() instanceof Player player
		&& event.getProjectile() instanceof Arrow arrow) {

			if(event.getBow().hasItemMeta() && event.getBow().getItemMeta().getDisplayName().contains("Quick Bow")
			&& event.getBow().getItemMeta().getLore().contains(ChatColor.GRAY + "This looks like a powerful weapon!")) {
				if(event.getConsumable().getAmount() != 0) {
					
					List<ItemStack> projectilesInBow = new ArrayList<ItemStack>();
					projectilesInBow.add(event.getConsumable());
					CrossbowMeta meta = (CrossbowMeta) event.getBow().getItemMeta();
					meta.setChargedProjectiles(projectilesInBow); //update the item meta on reload to set charged

					//shot destructor arrow with instant bow?
					List<String> lore = event.getConsumable().getItemMeta().getLore() != null ? event.getConsumable().getItemMeta().getLore() : new ArrayList<String>();
					if(lore.contains(ChatColor.GRAY + " > Spreads fire")) {
						Bukkit.getScheduler().runTask(plugin, () -> Entities.shotBloodMoonArrow.add(player.getName()));
					} else if(lore.contains(ChatColor.GRAY + " > Blows up on hit!")) {
						Bukkit.getScheduler().runTask(plugin, () -> Entities.shotDestructorArrow.add(player.getName()));
					}
					
					long reloadDelay = 1L;
					if(meta.getLore().contains(ChatColor.GRAY + " > Single shot: Good accuracy")) {
						//launch an arrow at full speed:
						Arrow spawnedArrow = player.getLocation().getWorld().spawnArrow(arrow.getLocation(), player.getLocation().getDirection(), 4, 1);
						if(arrow.hasCustomEffects())
							spawnedArrow.addCustomEffect(arrow.getCustomEffects().get(0), true);
						if(arrow.getBasePotionData().getType() != PotionType.UNCRAFTABLE) spawnedArrow.setBasePotionData(arrow.getBasePotionData());
						spawnedArrow.setShooter(player);
						
						spawnedArrow.setBounce(false);
					}
					if(meta.getLore().contains(ChatColor.GRAY + " > Multi shot: Poor accuracy")) {
						reloadDelay = 16L;
						//launch multiple arrows at full speed:
						Arrow spawnedArrow = player.getLocation().getWorld().spawnArrow(arrow.getLocation(), player.getLocation().getDirection(), 4, 15);
						spawnedArrow.setKnockbackStrength(2);
						if(arrow.hasCustomEffects())
							spawnedArrow.addCustomEffect(arrow.getCustomEffects().get(0), true);
						if(arrow.getBasePotionData().getType() != PotionType.UNCRAFTABLE) spawnedArrow.setBasePotionData(arrow.getBasePotionData());
						spawnedArrow.setShooter(player);
						spawnedArrow.setBounce(false);
						
						double yDifference = arrow.getLocation().getY() - player.getLocation().getY();
						
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								Arrow spawnedArrow = player.getLocation().getWorld().spawnArrow(player.getLocation().add(0,yDifference,0), player.getLocation().getDirection(), 4, 15);
								spawnedArrow.setKnockbackStrength(2);
								if(arrow.hasCustomEffects())
									spawnedArrow.addCustomEffect(arrow.getCustomEffects().get(0), true);
								if(arrow.getBasePotionData().getType() != PotionType.UNCRAFTABLE) spawnedArrow.setBasePotionData(arrow.getBasePotionData());
								spawnedArrow.setShooter(player);
								spawnedArrow.setBounce(false);
								spawnedArrow.setPickupStatus(AbstractArrow.PickupStatus.DISALLOWED);
								
								if(lore.contains(ChatColor.GRAY + " > Spreads fire")) {
									Entities.shotBloodMoonArrow.add(player.getName());
								} else if(lore.contains(ChatColor.GRAY + " > Blows up on hit!")) {
									Entities.shotDestructorArrow.add(player.getName());
								}
							}
						},2L);
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								Arrow spawnedArrow = player.getLocation().getWorld().spawnArrow(player.getLocation().add(0,yDifference,0), player.getLocation().getDirection(), 4, 15);
								spawnedArrow.setKnockbackStrength(2);
								if(arrow.hasCustomEffects())
									spawnedArrow.addCustomEffect(arrow.getCustomEffects().get(0), true);
								if(arrow.getBasePotionData().getType() != PotionType.UNCRAFTABLE) spawnedArrow.setBasePotionData(arrow.getBasePotionData());
								spawnedArrow.setShooter(player);
								spawnedArrow.setBounce(false);
								spawnedArrow.setPickupStatus(AbstractArrow.PickupStatus.DISALLOWED);
								
								if(lore.contains(ChatColor.GRAY + " > Spreads fire")) {
									Entities.shotBloodMoonArrow.add(player.getName());
								} else if(lore.contains(ChatColor.GRAY + " > Blows up on hit!")) {
									Entities.shotDestructorArrow.add(player.getName());
								}
							}
						}, 4L);
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								Arrow spawnedArrow = player.getLocation().getWorld().spawnArrow(player.getLocation().add(0,yDifference,0), player.getLocation().getDirection(), 4, 15);
								spawnedArrow.setKnockbackStrength(2);
								if(arrow.hasCustomEffects())
									spawnedArrow.addCustomEffect(arrow.getCustomEffects().get(0), true);
								if(arrow.getBasePotionData().getType() != PotionType.UNCRAFTABLE) spawnedArrow.setBasePotionData(arrow.getBasePotionData());
								spawnedArrow.setShooter(player);
								spawnedArrow.setBounce(false);
								spawnedArrow.setPickupStatus(AbstractArrow.PickupStatus.DISALLOWED);
								
								if(lore.contains(ChatColor.GRAY + " > Spreads fire")) {
									Entities.shotBloodMoonArrow.add(player.getName());
								} else if(lore.contains(ChatColor.GRAY + " > Blows up on hit!")) {
									Entities.shotDestructorArrow.add(player.getName());
								}
							}
						}, 6L);
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								Arrow spawnedArrow = player.getLocation().getWorld().spawnArrow(player.getLocation().add(0,yDifference,0), player.getLocation().getDirection(), 4, 15);
								spawnedArrow.setKnockbackStrength(2);
								if(arrow.hasCustomEffects())
									spawnedArrow.addCustomEffect(arrow.getCustomEffects().get(0), true);
								if(arrow.getBasePotionData().getType() != PotionType.UNCRAFTABLE) spawnedArrow.setBasePotionData(arrow.getBasePotionData());
								spawnedArrow.setShooter(player);
								spawnedArrow.setBounce(false);
								spawnedArrow.setPickupStatus(AbstractArrow.PickupStatus.DISALLOWED);
								
								if(lore.contains(ChatColor.GRAY + " > Spreads fire")) {
									Entities.shotBloodMoonArrow.add(player.getName());
								} else if(lore.contains(ChatColor.GRAY + " > Blows up on hit!")) {
									Entities.shotDestructorArrow.add(player.getName());
								}
							}
						}, 8L);
					}
					
					//do damage to bow?
					if(!meta.getLore().contains("This is a donator item, and therefore never degrades.")) {
						
						Damageable damageable = (Damageable) meta;
						int newDamage = meta.getLore().contains(ChatColor.GRAY + " > Single shot: Good accuracy") ? damageable.getDamage()+1 : damageable.getDamage()+2;
						double maxDurability = event.getBow().getType().getMaxDurability();
						int warningPercent = 90;
						int damagePercent = (int) ((double)(newDamage/maxDurability)*100);
						if(warningPercent == damagePercent)
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Item] " + ChatColor.WHITE + "Your " + event.getBow().getItemMeta().getDisplayName()  + ChatColor.WHITE + " has " + ChatColor.BOLD + "10%" + ChatColor.WHITE + " durability!");
						if(newDamage >= maxDurability) {
							player.sendMessage(ChatColor.YELLOW + "[Item] " + ChatColor.RED + "Your " + event.getBow().getItemMeta().getDisplayName()  + ChatColor.RED + " broke from wear!");
							damageable.setDamage(newDamage);
							event.getBow().setItemMeta((ItemMeta)meta);
							Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
								@Override
								public void run() {
									player.getInventory().setItem(event.getHand(), new ItemStack(Material.AIR));
									player.updateInventory();
								}
							}, 1L);
							player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1f, 8f);
						} else {
							damageable.setDamage(newDamage);
							event.getBow().setItemMeta((ItemMeta)meta);
						}

					}
					
					event.setCancelled(true);
					
					//reload the crossbow:
					Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
						@Override
						public void run() {
							Damageable damageable = (Damageable) event.getBow().getItemMeta();
							if(event.getBow().getType() == Material.AIR || damageable.getDamage() >= event.getBow().getType().getMaxDurability()) {
								event.getBow().setType(Material.AIR);
								player.updateInventory();
								return;
							}
							
							if(player.getInventory().containsAtLeast(event.getConsumable(), 1)) {
								event.getBow().setItemMeta(meta);
								if(player.getGameMode() != GameMode.CREATIVE)
									player.getInventory().removeItem(event.getConsumable());
							} else {
								ItemStack quiver = canUseQuiver(player, event.getBow());
								if(event.getConsumable().getType() == Material.ARROW && quiver != null) {
									//can take from quiver
									event.getBow().setItemMeta(meta);
									takeArrowsFromQuiver(player, quiver, 1);
								} else {
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Item] " + ChatColor.WHITE + "Your " + event.getBow().getItemMeta().getDisplayName() + ChatColor.WHITE + " has run out of ammunition.");
								}
							}
						}
					}, reloadDelay);
				}
			} else {
				List<String> lore = event.getConsumable().getItemMeta().getLore() == null ? new ArrayList<String>() : event.getConsumable().getItemMeta().getLore();
				if(lore.contains(ChatColor.GRAY + " > Spreads fire")) {
					Bukkit.getScheduler().runTask(plugin, () -> Entities.shotBloodMoonArrow.add(player.getName()));
				} else if(lore.contains(ChatColor.GRAY + " > Blows up on hit!")) {
					Bukkit.getScheduler().runTask(plugin, () -> Entities.shotDestructorArrow.add(player.getName()));
				}
			}
		}
	}
	
	@EventHandler
	public void addCustomItemIntoInventory(InventoryClickEvent event) {
		Inventory inventory = event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY ? event.getInventory() : event.getClickedInventory();
		ItemStack item = event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY ? event.getCurrentItem() : event.getCursor();

		if(event.getWhoClicked() instanceof Player player
				&& inventory != null
				&& (inventory.getType() == InventoryType.ANVIL || inventory.getType() == InventoryType.ENCHANTING || inventory.getType() == InventoryType.GRINDSTONE)
				&& item.hasItemMeta()
				&& item.getItemMeta().hasLore()) {
			
			List<String> lore = item.getItemMeta().getLore();
			if(lore.contains(ChatColor.GRAY + "This looks like a powerful weapon!") //quick bow
					|| lore.contains(ChatColor.GRAY + "These boots look like they were from a Climber...")) { //climbing boots
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Item] " + ChatColor.RED + "This item cannot be modified.");
				event.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void dragCustomItemIntoInventory(InventoryDragEvent event) {
		Inventory inventory = event.getView().getTopInventory();
		Map<Integer, ItemStack> items = event.getNewItems();
		
		if(event.getWhoClicked() instanceof  Player player
		&& inventory != null
		&& ((inventory.getType() == InventoryType.ANVIL && (event.getRawSlots().contains(0) || event.getRawSlots().contains(1)))
		|| (inventory.getType() == InventoryType.ENCHANTING && (event.getRawSlots().contains(0)))
		|| (inventory.getType() == InventoryType.GRINDSTONE && (event.getRawSlots().contains(0) || event.getRawSlots().contains(1))))) {
		
			for(int itemSlot : items.keySet()) {
				ItemStack item = items.get(itemSlot);
				if(item.hasItemMeta()
				&& item.getItemMeta().hasLore()) {
					List<String> lore = item.getItemMeta().getLore();
					if(lore.contains(ChatColor.GRAY + "This looks like a powerful weapon!") //quick bow
							|| lore.contains(ChatColor.GRAY + "These boots look like they were from a Climber...")) { //climbing boots
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Item] " + ChatColor.RED + "This item cannot be modified.");
						event.setCancelled(true);
					}
				}
			}
		}
	}
}
