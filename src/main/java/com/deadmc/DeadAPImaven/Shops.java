package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Shop.ShopType;
import com.deadmc.DeadAPImaven.Task.TaskStep;
import com.deadmc.DeadAPImaven.Towns.Town;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import static java.util.stream.Collectors.toMap;

public class Shops implements Listener, CommandExecutor {
	private static DeadMC plugin;
	
	public Shops(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static Map<String, ItemStack> setItem = new HashMap<String, ItemStack>(); //player, itemstack
	public static Map<String, Integer> buyPrice = new HashMap<String, Integer>(); //player, price
	public static Map<String, Integer> sellPrice = new HashMap<String, Integer>(); //player, price
	
	public static Map<String, String> confirmSell = new HashMap<String, String>(); //player, shopCode
	public static Map<String, Integer> sellAmount = new HashMap<String, Integer>(); //player, price
	
	public static Map<String, String> confirmBuy = new HashMap<String, String>(); //player, shopCode
	public static Map<String, Integer> buyAmount = new HashMap<String, Integer>(); //player, price
	
	public static List<Shop> getShopsWithStock(String itemFinderName, boolean showPointsWithPassword, boolean showPointsWithFee) {
		//a list of points that have the item stocked:
		List<Shop> pointsWithStock = new ArrayList<Shop>();
		
		String enchant = null;
		if(itemFinderName.length() > 8 && itemFinderName.contains("ENCHANT:")) {
			String enchantmentString = itemFinderName.substring(8);
			try {
				if(Enchantment.getByKey(NamespacedKey.minecraft(enchantmentString)) != null)
					enchant = enchantmentString; //wont be null if enchantment exists
			} catch(IllegalArgumentException e) {
			}
		}
		final String finalEnchant = enchant;
		
		
		//the customItemName is the name to check for in the item display name (using .contains)
		String customItemName = null;
		if(itemFinderName.length() > 8 && itemFinderName.contains("SPECIAL:")) {
			String customItemString = itemFinderName.substring(8).toUpperCase();
			try {
				if(CustomItems.CustomItem.valueOf(customItemString) != null) {
					customItemName = customItemString;
				}
			} catch(IllegalArgumentException e) {
			}
		}
		final String finalCustomItemName = customItemName;

		if(finalCustomItemName == null && enchant == null && Material.matchMaterial(itemFinderName) == null) {
			//not a known item!
			Chat.debug(itemFinderName + " is not a known item!");
			return null;
		}
		
		//get all travel points:
		List<String> points = DeadMC.TravelFile.data().getStringList("Public");
		
		for(String point : points) {
			
			//Bukkit.broadcastMessage("Searching " + point + "...");
			
			//check if it has shops:
			
			PublicTP tpConfig = PublicTP.getConfig(point);
			if(tpConfig.getString("LocationCode") == null) continue;
			Location tpLocation = LocationCode.Decode(tpConfig.getString("LocationCode"));
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			ApplicableRegionSet regionsAtLoc = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(tpLocation));
			
			ProtectedRegion lowestPriorityRegion = null;
			for(ProtectedRegion region : regionsAtLoc) {
				if(region.getPriority() == 1) {
					lowestPriorityRegion = region;
				}
			}
			
			if(lowestPriorityRegion == null) continue; //skip if not on region
			
			//Bukkit.broadcastMessage("  has regions");
			
			ApplicableRegionSet regionsWithinRegion = regionManager.getApplicableRegions(lowestPriorityRegion);
			for(ProtectedRegion region : regionsWithinRegion) {
				//for each region within region
				//check if has shops
				
				File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + region.getId() + ".yml");
				if(regionFile.exists()) {
					RegionConfig regionConfig = RegionConfig.getConfig(region.getId());
					List<String> shops = regionConfig.getStringList("Shops");
					
					//Bukkit.broadcastMessage("  -> " + region.getId() + " has shops");
					
					//check if it has any shops with the specified item:
					for(String shopCode : shops) {
						if(regionConfig.getString(shopCode + ".Item") != null) { //has item assigned?
							ItemStack shopItem = new ItemStack(regionConfig.getItemStack(shopCode + ".Item")).clone();
							
							//does it have an enchant?
							Boolean isEnchanted = false;
							
							if(finalEnchant != null) {
								//is the item enchanted?
								for(Enchantment type : shopItem.getEnchantments().keySet()) {
									//do the enchants match?
									if(type.getKey().toString().substring(10).equalsIgnoreCase(finalEnchant)) {
										isEnchanted = true;
									}
								}
								//or is it an enchanted book?
								if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
									for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
										//holds enchant?
										if(enchantment.getKey().toString().substring(10).equalsIgnoreCase(finalEnchant)) {
											isEnchanted = true;
										}
									}
								}
							}
							
							String itemName = shopItem.getType().toString();
							//is it a custom item?
							//Bukkit.getConsoleSender().sendMessage("Checking " + itemName + " - " + ChatColor.stripColor(shopItem.getItemMeta().getDisplayName().replace("(", "").replace(")", "").replace(" ", "_")).toUpperCase() + " = " + finalCustomItemName);
							
							if(((finalCustomItemName == null && itemName.equalsIgnoreCase(itemFinderName)) //item name matches
									//item is custom and the name matches:
									|| (finalCustomItemName != null && shopItem.getItemMeta().hasLore() && ChatColor.stripColor(shopItem.getItemMeta().getDisplayName().replace("(", "").replace(")", "").replace(" ", "_")).toUpperCase().contains(finalCustomItemName))
									|| isEnchanted) //is enchanted
									&& regionConfig.getString(shopCode + ".BuyPrice") != null) { //item is specified item?
								
								//Bukkit.broadcastMessage(region.getId() + " has a shop with a " + itemFinderName);
								
								//does it have stock?
								Block block = Shops.decodeCode(shopCode).getBlock();
								//get total stock available:
								int totalStock = Shops.getTotalStock(regionConfig, shopCode);
								if(totalStock > 0) {
									//Bukkit.broadcastMessage("  > With " + totalStock + " stocked!");
									int sellPrice = 0;
									if(regionConfig.getString(shopCode + ".SellPrice") != null)
										sellPrice = regionConfig.getInt(shopCode + ".SellPrice");
									
									boolean isDonatorItem = finalCustomItemName != null && shopItem.getItemMeta().hasLore() && shopItem.getItemMeta().getDisplayName().toUpperCase().contains("DONATOR");
									Shop shop = new Shop(ShopType.PLAYER, block.getLocation(), totalStock, point, regionConfig.getInt(shopCode + ".BuyPrice"), sellPrice, shopItem.getType(), isDonatorItem);
									//check that location isn't already in list (eg. multiple public points in the region)
									Boolean shopAlreadyExists = false;
									for(Shop existing : pointsWithStock) {
										if(existing.location.equals(shop.location)) {
											shopAlreadyExists = true;
											break;
										}
									}
									if(!shopAlreadyExists) {
										if(!showPointsWithPassword && tpConfig.getString("Pass") != null) continue;
										if(!showPointsWithFee && tpConfig.getString("Cost") != null) continue;
										pointsWithStock.add(shop);
									}
								}
								
							}
							
						}
					}
					
				}
			}
			
		}
		
		//add market ones:
		
		FileConfiguration shops = DeadMC.ShopsFile.data();
		
		//loop all shops in shops file
		for(String shopCode : shops.getKeys(false)) {
			if(shops.getString(shopCode + ".Item") != null) {
				//shop is set up properly
				
				//no enchanted items or custom items in market, ignore special checks
				
				ItemStack shopItem = new ItemStack(shops.getItemStack(shopCode + ".Item")).clone();
				if(shopItem.getType().toString().equalsIgnoreCase(itemFinderName)) { //is specified item
					int buyPrice = shops.getInt(shopCode + ".BuyPrice");
					int sellPrice = 0;
					if(shops.getString(shopCode + ".SellPrice") != null)
						sellPrice = shops.getInt(shopCode + ".SellPrice");
					
					Location location = decodeCode(shopCode);
					String regionName = Regions.getHighestPriorityLand(location).getId();
					String shopName = regionName.toLowerCase().substring(0, 1).toUpperCase() + regionName.toLowerCase().substring(1, regionName.length());
					
					pointsWithStock.add(new Shop(ShopType.MARKET, location, Integer.MAX_VALUE, "The Longdale Market", buyPrice, sellPrice, shopName));
				}
			}
		}
		
		return pointsWithStock;
	}
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		try {
		final Player player = (Player) sender;
		
		if(commandLabel.equalsIgnoreCase("find")
				|| commandLabel.equalsIgnoreCase("f")) {
			
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				
				if(args.length == 0) {
					String itemFinderName = TravelGUI.getItemFinderNameFromItem(player.getInventory().getItemInMainHand());
					if(itemFinderName != null) {
						TravelGUI.openItemFinderResultsMenu(player, itemFinderName);
					} else {
						TravelGUI.openItemFinderMenu(player);
					}
					return;
				}
				
				//check if is an actual item, if not, open the keyword search menu
				String actualItemName = "";
				boolean keywordIsContainedInOtherItems = false;
				for(String itemFinderName : TravelGUI.getItemFinderItemNames()) {
					if(itemFinderName.equalsIgnoreCase(args[0])) {
						//is an actual item finder item
						actualItemName = itemFinderName;
					} else if(itemFinderName.toLowerCase().contains(args[0].toLowerCase())) {
						keywordIsContainedInOtherItems = true;
					}
				}
				
				if(!keywordIsContainedInOtherItems && !actualItemName.equals("")) {
					int uses = DeadMC.MarketFile.data().getString("ItemFinder." + actualItemName) == null ? 0 : DeadMC.MarketFile.data().getInt("ItemFinder." + actualItemName);
					DeadMC.MarketFile.data().set("ItemFinder." + actualItemName, uses + 1);
					DeadMC.MarketFile.save();
					
					TravelGUI.openItemFinderResultsMenu(player, actualItemName);
					return;
				}
				
				//else try searching
				TravelGUI.openItemFinderMenu(player, args[0], true);
				
			});
			
			return true;
		}
		
		if(commandLabel.equalsIgnoreCase("sell")) {
			
			int amount = 0;
			if(args.length == 0)
				amount = 1;
			else {
				if(Chat.isInteger(args[0])) {
					amount = Integer.parseInt(args[0]);
					if(amount < 1) {
						//cannot be 0 or negative
						player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/sell <amount>" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/sell all" + ChatColor.WHITE + ".");
						return true;
					}
				} else if(args[0].equalsIgnoreCase("all")) {
					amount = 0;
				} else {
					player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/sell <amount>" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/sell all" + ChatColor.WHITE + ".");
					return true;
				}
			}
			if(sellAmount.containsKey(player.getName())) {
				sellAmount.replace(player.getName(), amount); //replace so there's not more than 1
			} else sellAmount.put(player.getName(), amount);
			confirmSell.remove(player.getName());
			
			player.sendMessage("");
			if(amount == 0)
				player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + "Click a " + ChatColor.BOLD + "[Shop] sign" + ChatColor.WHITE + " to sell " + ChatColor.AQUA + ChatColor.BOLD + "all" + ChatColor.WHITE + " of an item.");
			else
				player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + "Click a " + ChatColor.BOLD + "[Shop] sign" + ChatColor.WHITE + " to sell " + ChatColor.AQUA + ChatColor.BOLD + amount + "x" + ChatColor.WHITE + " of an item.");
			if(amount == 1)
				player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/sell <amount>" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/sell all" + ChatColor.WHITE + " to sell more.");
			player.sendMessage("");
			
		}
		
		if(commandLabel.equalsIgnoreCase("buy")) {
			
			int amount = 0;
			if(args.length == 0)
				amount = 1;
			else {
				if(Chat.isInteger(args[0])) {
					amount = Integer.parseInt(args[0]);
					if(amount < 1) {
						//cannot be 0 or negative
						player.sendMessage(ChatColor.YELLOW + "[Buy] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/buy <amount>" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/buy all" + ChatColor.WHITE + ".");
						return true;
					}
				} else if(args[0].equalsIgnoreCase("all")) {
					amount = 0;
				} else {
					player.sendMessage(ChatColor.YELLOW + "[Buy] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/buy <amount>" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/buy all" + ChatColor.WHITE + ".");
					return true;
				}
			}
			if(buyAmount.containsKey(player.getName())) {
				buyAmount.replace(player.getName(), amount); //replace so there's not more than 1
			} else buyAmount.put(player.getName(), amount);
			confirmBuy.remove(player.getName());
			
			player.sendMessage("");
			if(amount == 0)
				player.sendMessage(ChatColor.YELLOW + "[Buy] " + ChatColor.WHITE + "Click a " + ChatColor.BOLD + "[Shop] sign" + ChatColor.WHITE + " to buy " + ChatColor.AQUA + ChatColor.BOLD + "all" + ChatColor.WHITE + " of an item.");
			else
				player.sendMessage(ChatColor.YELLOW + "[Buy] " + ChatColor.WHITE + "Click a " + ChatColor.BOLD + "[Shop] sign" + ChatColor.WHITE + " to buy " + ChatColor.AQUA + ChatColor.BOLD + amount + "x" + ChatColor.WHITE + " of an item.");
			if(amount == 1)
				player.sendMessage(ChatColor.YELLOW + "[Buy] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/buy <amount>" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/buy all" + ChatColor.WHITE + " to buy more.");
			player.sendMessage("");
			
		}
		
		if(commandLabel.equalsIgnoreCase("shop") || commandLabel.equalsIgnoreCase("shops") || commandLabel.equalsIgnoreCase("s")) {
			
			if(args.length == 0) {
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + "=================== Shop Commands ===================");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Manage a shop");
				player.sendMessage(ChatColor.GOLD + "/shop item" + ChatColor.WHITE + " - Assign the item in your hand to a shop.");
				player.sendMessage(ChatColor.GOLD + "/shop buyprice" + ChatColor.WHITE + " - Set the price to " + ChatColor.AQUA + "buy" + ChatColor.WHITE + " from your shop.");
				player.sendMessage(ChatColor.GOLD + "/shop sellprice" + ChatColor.WHITE + " - Set the price to " + ChatColor.AQUA + "sell" + ChatColor.WHITE + " to your shop.");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "To " + ChatColor.BOLD + "create" + ChatColor.GOLD + " a shop: " + ChatColor.WHITE + "Place a sign on claimed land, writee " + ChatColor.AQUA + "[shop]" + ChatColor.WHITE + " on the first line.");
				player.sendMessage(ChatColor.YELLOW + "====================================================");
				player.sendMessage("");
				
				//need at least 5 shops in region to enable shopping cart system
				
				return true;
			}
			
			if(args[0].equalsIgnoreCase("item")) {
				ItemStack itemInHand = player.getInventory().getItemInMainHand().clone();
				itemInHand.setAmount(1);
				
				if(itemInHand == null || itemInHand.getType() == Material.AIR) {
					player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.WHITE + "Select an item in your " + ChatColor.BOLD + "hand" + ChatColor.WHITE + " and use " + ChatColor.GOLD + "/s item" + ChatColor.WHITE + ".");
					return true;
				}
				ItemMeta meta = itemInHand.getItemMeta();
				if(!meta.hasLore() && meta.hasDisplayName() && meta.getDisplayName().contains("Backpack")) {
					//can't sell the donator rank backpack
					player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "You cannot sell this item.");
					return true;
				}
				
				//15 seconds to click sign
				setItem.put(player.getName(), itemInHand);
				Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
					@Override
					public void run() {
						if(setItem.containsKey(player.getName())) {
							setItem.remove(player.getName());
						}
					}
				}, 300L);
				
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + "================= Assign Item =================");
				player.sendMessage(ChatColor.WHITE + "Click a " + ChatColor.BOLD + "shop sign" + ChatColor.WHITE + " to assign the following item: ");
				String itemName = meta.hasLore() && meta.hasDisplayName() ? meta.getDisplayName() : itemInHand.getType().toString().toLowerCase().replace("_", " ");
				player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.AQUA + ChatColor.BOLD + itemName.substring(0, 1).toUpperCase() + itemName.substring(1, itemName.length()));
				//add item enchants:
				for(Enchantment enchantment : itemInHand.getEnchantments().keySet()) {
					String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
					player.sendMessage(ChatColor.WHITE + "    - " + ChatColor.DARK_AQUA + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + itemInHand.getEnchantmentLevel(enchantment) + ChatColor.WHITE + ")");
				}
				//add stored (book) enchants:
				if(itemInHand.getItemMeta() instanceof EnchantmentStorageMeta) {
					for(Enchantment enchantment : ((EnchantmentStorageMeta) itemInHand.getItemMeta()).getStoredEnchants().keySet()) {
						String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
						player.sendMessage(ChatColor.WHITE + "    - " + ChatColor.DARK_AQUA + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) itemInHand.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
					}
				}
				player.sendMessage(ChatColor.YELLOW + "=============================================");
				player.sendMessage("");
				
			} else if(args[0].equalsIgnoreCase("buyprice")) {
				if(args.length >= 2 && Chat.isInteger(args[1])) {
					int price = Integer.parseInt(args[1]);
					
					if(price > 0) {
						
						//15 seconds to click sign
						buyPrice.put(player.getName(), price);
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								if(buyPrice.containsKey(player.getName())) {
									buyPrice.remove(player.getName());
								}
							}
						}, 400L);
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "================= Buy Price =================");
						player.sendMessage(ChatColor.WHITE + "Click a " + ChatColor.BOLD + "shop sign" + ChatColor.WHITE + " to set the price to buy to:");
						player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(price));
						player.sendMessage(ChatColor.YELLOW + "===========================================");
						player.sendMessage("");
						
					} else
						player.sendMessage(ChatColor.YELLOW + "[Shop] " + ChatColor.WHITE + "The price must be greater than zero.");
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Shop] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/s buyprice " + ChatColor.AQUA + "<price>" + ChatColor.WHITE + " to set a price to buy.");
			} else if(args[0].equalsIgnoreCase("sellprice")) {
				if(args.length >= 2 && Chat.isInteger(args[1])) {
					int price = Integer.parseInt(args[1]);
					
					if(price >= 0) { //set to 0 to disable selling
						
						//15 seconds to click sign
						sellPrice.put(player.getName(), price);
						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								if(sellPrice.containsKey(player.getName())) {
									sellPrice.remove(player.getName());
								}
							}
						}, 300L);
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "================= Sell Price =================");
						if(price == 0) {
							player.sendMessage(ChatColor.WHITE + "Click a " + ChatColor.BOLD + "shop sign" + ChatColor.WHITE + " to disable buying from players.");
						} else {
							player.sendMessage(ChatColor.WHITE + "Click a " + ChatColor.BOLD + "shop sign" + ChatColor.WHITE + " to set the price to sell to:");
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(price));
						}
						player.sendMessage(ChatColor.YELLOW + "===========================================");
						player.sendMessage("");
						
					}
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[Shop] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/s sellprice " + ChatColor.AQUA + "<price>" + ChatColor.WHITE + " to set a price to sell.");
			} else {
				
				//unknown command
				player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.WHITE + "Unknown command '" + args[0] + "'.");
				player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/shop" + ChatColor.WHITE + " for help.");
				
			}
			
		}
		
		} catch(Exception e) {
			Chat.logError(e, sender instanceof Player player ? player : null);
		}
		
		return true;
	}
	
	//citizens = 25 shops
	//squire = 35 shops
	//knight = 50 shops
	//baron = 100 shops
	//royal = unlimited shops
	
	@EventHandler
	public void onSignUpdate(SignChangeEvent event) {
		Player player = event.getPlayer();
		
		//create admin shop
		if(event.getLine(0).equalsIgnoreCase("[m]") || event.getLine(0).equalsIgnoreCase("[market]")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(player.isOp() || playerConfig.getInt("StaffRank") > 2) {
				
				if(!(event.getBlock().getBlockData() instanceof WallSign)) {
					//is a normal sign
					player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "Shop signs can only be placed on walls.");
					event.setCancelled(true);
					return;
				}
				
				if(!(event.getBlock().getLocation().getWorld().getName().equalsIgnoreCase("world"))) {
					//is a normal sign
					player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "You can only create shops in the overworld.");
					event.setCancelled(true);
					return;
				}
				
				event.setLine(0, "[Market]");
				event.setLine(1, ChatColor.DARK_RED + "Out of Order");
				event.setLine(2, "");
				event.setLine(3, "Click for help");
				Sign sign = (Sign) event.getBlock().getState();
				sign.update();
				
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Market] " + ChatColor.GREEN + "Created market shop!");
				
				FileConfiguration shopConfig = DeadMC.ShopsFile.data();
				
				//track number of shops player has:
				if(shopConfig.getString("Shops") == null) shopConfig.set("Shops", 1);
				else shopConfig.set("Shops", shopConfig.getInt("Shops") + 1);
				DeadMC.ShopsFile.save();
				
				//track shop location in region
				String shopCode = convertToCode(event.getBlock().getLocation());
				String regionName = Regions.getHighestPriorityLand(event.getBlock().getLocation()).getId();
				RegionConfig regionConfig = RegionConfig.getConfig(regionName);
				
				List<String> shopsInRegion = new ArrayList<String>();
				if(regionConfig.getString("Shops") != null)
					shopsInRegion = regionConfig.getStringList("Shops");
				if(!shopsInRegion.contains(shopCode)) {
					shopsInRegion.add(shopCode);
				}
				regionConfig.set("Shops", shopsInRegion);
				regionConfig.save();
				
			} else {
				player.sendMessage(ChatColor.RED + "[DeadMC] You cannot create a market shop.");
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Use [Shop] to create a shop.");
				event.setCancelled(true);
			}
		}
		
		//create player shop
		if(event.getLine(0).equalsIgnoreCase("[Shop]")) {
			if(Regions.playerOwnsLand(event.getBlock().getLocation(), player.getUniqueId(), false)) {
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				int shopLimit = DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.Shops");
				
				if(playerConfig.getString("Shops") == null || new Integer(playerConfig.getInt("Shops") + 1) <= shopLimit) {
					
					if(!(event.getBlock().getBlockData() instanceof WallSign)) {
						//is a normal sign
						player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "Shop signs can only be placed on walls.");
						event.setCancelled(true);
						return;
					}
					
					if(!(event.getBlock().getLocation().getWorld().getName().equalsIgnoreCase("world"))) {
						//is a normal sign
						player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "You can only create shops in the overworld.");
						event.setCancelled(true);
						return;
					}
					
					event.setLine(0, "[Shop]");
					event.setLine(1, ChatColor.DARK_RED + "Out of Order");
					event.setLine(2, "");
					event.setLine(3, "Click for help");
					Sign sign = (Sign) event.getBlock().getState();
					sign.update();
					
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Shops] " + ChatColor.GREEN + "Created shop!");
					
					//track number of shops player has:
					if(playerConfig.getString("Shops") == null) playerConfig.set("Shops", 1);
					else playerConfig.set("Shops", playerConfig.getInt("Shops") + 1);
					playerConfig.save();
					
					//track shop location in region
					String shopCode = convertToCode(event.getBlock().getLocation());
					String regionName = Regions.getHighestPriorityLand(event.getBlock().getLocation()).getId();
					RegionConfig regionConfig = RegionConfig.getConfig(regionName);
					
					List<String> shopsInRegion = new ArrayList<String>();
					if(regionConfig.getString("Shops") != null)
						shopsInRegion = regionConfig.getStringList("Shops");
					if(!shopsInRegion.contains(shopCode)) {
						shopsInRegion.add(shopCode);
					}
					regionConfig.set("Shops", shopsInRegion);
					regionConfig.save();
					
				} else {
					//limit reach
					player.sendMessage("");
					player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You've reached your " + ChatColor.AQUA + "shops" + ChatColor.WHITE + " limit (" + shopLimit + ").");
					player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to upgrade.");
					player.sendMessage("");
					event.setCancelled(true);
				}
			} else {
				player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "You must " + ChatColor.BOLD + "own" + ChatColor.RED + " the land to create a shop.");
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onSignBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		
		if(event.getBlock().getState() instanceof Sign) {
			Sign sign = (Sign) event.getBlock().getState();
			
			//player shops
			if(sign.getLine(0).equalsIgnoreCase("[Shop]")) {
				if(!event.isCancelled()) {
					if(Regions.playerOwnsLand(sign.getLocation(), player.getUniqueId(), false)) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "Removed shop!");
						
						PlayerConfig playerConfig = PlayerConfig.getConfig(player);
						//track number of shops player has:
						int newAmount = playerConfig.getInt("Shops") - 1;
						if(newAmount == 0) playerConfig.set("Shops", null);
						else playerConfig.set("Shops", newAmount);
						playerConfig.save();
						
						//track shop location in region
						String shopCode = convertToCode(event.getBlock().getLocation());
						String regionName = Regions.getHighestPriorityLand(event.getBlock().getLocation()).getId();
						removeShop(regionName, shopCode);
						
					} else {
						OfflinePlayer shopOwner = Regions.getOwnerAtLocation(sign.getLocation());
						player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "Only " + shopOwner.getName() + " can remove this shop!");
						event.setCancelled(true);
					}
				}
			}
			
			//admin shops:
			if(sign.getLine(0).equalsIgnoreCase("[Market]")) {
				if(!event.isCancelled()) {
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					if(player.isOp() || playerConfig.getInt("StaffRank") > 2) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "Removed shop!");
						
						//track number of shops player has:
						int newAmount = playerConfig.getInt("Shops") - 1;
						if(newAmount == 0) playerConfig.set("Shops", null);
						else playerConfig.set("Shops", newAmount);
						playerConfig.save();
						
						//track shop location in region
						String shopCode = convertToCode(event.getBlock().getLocation());
						String regionName = Regions.getHighestPriorityLand(event.getBlock().getLocation()).getId();
						removeShop(regionName, shopCode);
					} else {
						OfflinePlayer shopOwner = Regions.getOwnerAtLocation(sign.getLocation());
						player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "Only " + shopOwner.getName() + " can remove this shop!");
						event.setCancelled(true);
					}
				}
			}
		}
	}
	
	public static void removeShop(String regionName, String shopCode) {
		RegionConfig regionConfig = RegionConfig.getConfig(regionName);
		
		List<String> shopsInRegion = regionConfig.getStringList("Shops");
		shopsInRegion.remove(shopCode);
		if(shopsInRegion.size() == 0) {
			//last shop in file, remove:
			File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionName + ".yml");
			regionFile.delete();
			File townFolder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + Regions.getOriginalTownName(regionName));
			if(townFolder.isDirectory() && townFolder.list().length == 0)
				townFolder.delete();
		} else {
			//not last shop in file, update:
			regionConfig.set(shopCode, null);
			regionConfig.set("Shops", shopsInRegion);
			regionConfig.save();
		}
	}
	
	public static void copyShop(String oldRegionName, String newRegionName, String shopCode) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				RegionConfig oldConfig = RegionConfig.getConfig(oldRegionName);
				RegionConfig newConfig = RegionConfig.getConfig(newRegionName);
				
				//copy the shop to new region file
				List<String> shopsInRegion = newConfig.getStringList("Shops");
				if(!shopsInRegion.contains(shopCode)) {
					shopsInRegion.add(shopCode);
				}
				newConfig.set("Shops", shopsInRegion);
				if(oldConfig.getString(shopCode + ".Item") != null)
					newConfig.set(shopCode + ".Item", oldConfig.getItemStack(shopCode + ".Item"));
				if(oldConfig.getString(shopCode + ".BuyPrice") != null)
					newConfig.set(shopCode + ".BuyPrice", oldConfig.getInt(shopCode + ".BuyPrice"));
				if(oldConfig.getString(shopCode + ".SellPrice") != null)
					newConfig.set(shopCode + ".SellPrice", oldConfig.getInt(shopCode + ".SellPrice"));
				if(oldConfig.getString(shopCode + ".TotalStock") != null)
					newConfig.set(shopCode + ".TotalStock", oldConfig.getInt(shopCode + ".TotalStock"));
				newConfig.save();
				
				//remove the shop from old region file
				removeShop(oldRegionName, shopCode);
		});
	}
	
	public static void recalcAllShops() {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			//1) LOOP ALL REGION CONFIGS
			int brokensInRegionConfigs = 0;
			int duplicatesInRegionConfigs = 0;
			
			File shopsFolder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops");
			File[] files = shopsFolder.listFiles();
			for(int town = 0; town < files.length; town++) { //all town folders
				if(!files[town].isFile()) { //ensure it is a town folder
					String townName = files[town].getName();
					File townFolder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + files[town].getName());
					File[] regionFiles = townFolder.listFiles();
					
					for(File regionFile : regionFiles) { //check each region file
						String regionName = regionFile.getName().replace(".yml", "");
						RegionConfig regionConfig = RegionConfig.getConfig(townName + "/" + regionName); //convert the file to RegionConfig
						
						if(regionConfig.getString("Shops") == null)
							continue; //can remove the file
						
						//2) CHECK AND REMOVE DUPLICATES
						List<String> listWithDuplicates = regionConfig.getStringList("Shops");
						List<String> listWithoutDuplicates = new ArrayList<>(
								new HashSet<>(listWithDuplicates));
						
						int duplicates = listWithDuplicates.size() - listWithoutDuplicates.size();
						if(duplicates > 0) {
							duplicatesInRegionConfigs++;
							Chat.broadcastMessage("Found " + duplicates + " duplicates in " + townName + "/" + regionName + ".", true);
						}
						
						int brokenShops = 0;
						for(String shopCode : listWithDuplicates) { //use the duplicate list to avoid removing in loop
							//check if there's a sign still at the location, if not remove
							
							Location location = Shops.decodeCode(shopCode);
							if(!location.getBlock().getType().toString().contains("WALL_SIGN")) {
								//shop has been removed
								brokenShops++;
								brokensInRegionConfigs++;
								
								regionConfig.set(shopCode, null);
								listWithoutDuplicates.remove(shopCode);
							}
						}
						
						if(brokenShops > 0)
							Chat.broadcastMessage("Removed " + brokenShops + " broken shops in " + townName + "/" + regionName + ".", true);
						
						regionConfig.set("Shops", listWithoutDuplicates);
						regionConfig.save();
					}
				}
			}
			
			//3) RECALCULATE PLAYER LIMITS
			
			int inaccuratePlayers = 0;
			
			//create HashMap<UUID, Integer> - player uuid, how many shops they have
			//loop all regions, make sure priority is > 0, check if there's a region config with shops list, get the region owner, add the shops list count to the player hashmap
			
			HashMap<UUID, Integer> shopsOwnedByPlayers = new HashMap<UUID, Integer>();
			
			//get list of WG regions
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			Map<String, ProtectedRegion> wgRegionsWithID = regionManager.getRegions(); //region name, protected region
			List<ProtectedRegion> protectedRegions = new ArrayList<ProtectedRegion>();
			wgRegionsWithID.forEach((regionID, region) -> {
				if(region.getPriority() > 0)
					protectedRegions.add(region);
			});
			
			//loop each protected region
			for(ProtectedRegion region : protectedRegions) {
				//check if region config exists before creating
				File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + region.getId() + ".yml");
				if(regionFile.exists()) {
					RegionConfig regionConfig = RegionConfig.getConfig(region.getId()); //convert the file to RegionConfig
					if(regionConfig.getString("Shops") == null) continue;
					
					int shopsInRegion = regionConfig.getStringList("Shops").size();
					for(UUID owner : regionManager.getRegion(region.getId()).getOwners().getUniqueIds()) {
						if(shopsOwnedByPlayers.containsKey(owner)) shopsOwnedByPlayers.replace(owner, shopsOwnedByPlayers.get(owner) + shopsInRegion);
						else shopsOwnedByPlayers.put(owner, shopsInRegion);
						break; //only do for first owner
					}
					
				}
			}
			
			//loop all players
			List<String> activePlayers = DeadMC.PlayerFile.data().getStringList("Active");
			for(String uuidString : activePlayers) {
				UUID uuid = UUID.fromString(uuidString);
				PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
				int accurate = shopsOwnedByPlayers.containsKey(uuid) ? shopsOwnedByPlayers.get(uuid) : 0;
				int current = playerConfig.getString("Shops") == null ? 0 : playerConfig.getInt("Shops");

				if(accurate != current) {
					Chat.broadcastMessage("Fixed broken shop count for " + Bukkit.getOfflinePlayer(uuid).getName(), true);
					inaccuratePlayers++;
					
					if(accurate == 0) playerConfig.set("Shops", null);
					else playerConfig.set("Shops", accurate);
					playerConfig.save();
				}
				
			}
			
			Tools.sleepThread(1000);
			Chat.broadcastMessage("Finished...", true);
			Chat.broadcastMessage("Removed " + duplicatesInRegionConfigs + " duplicates in region configs.", true);
			Chat.broadcastMessage("Removed " + brokensInRegionConfigs + " broken shops in region configs.", true);
			Chat.broadcastMessage("Fixed " + inaccuratePlayers + " inaccurate player shop counts.", true);
		});
	}
	
	@EventHandler
	public void playerOpenChest(PlayerInteractEvent event) {
		//just sends a message, therefore run async
		
		Player player = event.getPlayer();
		
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK
				|| event.getAction() == Action.LEFT_CLICK_BLOCK) {
			Block block = event.getClickedBlock();
			if(block.getState() instanceof Chest) {
				
				List<Sign> connectedShops = new ArrayList<Sign>();
				
				InventoryHolder holder = ((Chest) block.getState()).getInventory().getHolder();
				if(holder instanceof DoubleChest) {
					
					DoubleChest doubleChest = ((DoubleChest) holder);
					Chest leftChest = (Chest) doubleChest.getLeftSide();
					Chest rightChest = (Chest) doubleChest.getRightSide();
					
					connectedShops.addAll(getConnectedShops(leftChest.getBlock()));
					connectedShops.addAll(getConnectedShops(rightChest.getBlock()));
					
				} else {
					//single chest
					connectedShops = getConnectedShops(block);
				}
				
				Boolean playerOwnsAllShops = true;
				String otherOwnersName = "";
				for(Sign sign : connectedShops) {
					OfflinePlayer shopOwner = Regions.getOwnerAtLocation(sign.getLocation());
					if(shopOwner != null && shopOwner.getUniqueId() != player.getUniqueId()) {
						playerOwnsAllShops = false;
						otherOwnersName = shopOwner.getName();
						break;
					}
				}
				
				if(!playerOwnsAllShops) {
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Shops] " + ChatColor.RED + ChatColor.BOLD + otherOwnersName + ChatColor.RED + " owns this shop.");
				}
			}
		}
	}
	
	@EventHandler
	public void playerClickSign(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		
		try {
			if(event.getAction() == Action.RIGHT_CLICK_BLOCK
					|| event.getAction() == Action.LEFT_CLICK_BLOCK) {
				Block block = event.getClickedBlock();
				if(block.getState() instanceof Sign) {
					final Sign sign = (Sign) block.getState();
					
					//market shops:
					if(sign.getLine(0).equalsIgnoreCase("[Market]")) {
						
						FileConfiguration shops = DeadMC.ShopsFile.data();
						String shopCode = convertToCode(block.getLocation());
						
						if(shops.getString(shopCode + ".Item") == null
								|| shops.getString(shopCode + ".BuyPrice") == null) {
							//shop is no longer in order
							sign.setLine(1, ChatColor.DARK_RED + "Out of Order");
							sign.update();
						}
						
						//out of order:
						
						if(!player.isOp() && PlayerConfig.getConfig(player).getInt("StaffRank") <= 2) {
							if(sign.getLine(1).equalsIgnoreCase(ChatColor.DARK_RED + "Out of Order")) {
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "This shop is " + ChatColor.BOLD + "out of order" + ChatColor.WHITE + ".");
								player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.WHITE + "Please contact an administrator for help.");
								player.sendMessage("");
								return;
							}
						} else {
							
							if(setItem.containsKey(player.getName())) {
								ItemStack item = setItem.get(player.getName());
								ItemMeta meta = item.getItemMeta();
								String itemName = "";
								if(meta.hasLore()) {
									//special item
									itemName = meta.getDisplayName();
								} else {
									if(item.getEnchantments().keySet().size() > 0)
										itemName = ChatColor.AQUA + "(e)";
									itemName += ChatColor.WHITE + item.getType().toString().toUpperCase().replace("_", " ");
								}
								
								shops.set(shopCode + ".Item", item);
								DeadMC.ShopsFile.save();
								setItem.remove(player.getName());
								
								player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.GREEN + "Assigned " + ChatColor.BOLD + itemName + ChatColor.GREEN + " to the shop!");
								sign.setLine(2, "" + itemName);
								sign.update();
								
								return;
							}
							
							if(shops.getString(shopCode + ".Item") == null) {
								//item hasn't been set
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "================= Shop Help =================");
								player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "This shop is NOT functioning properly!");
								player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "This shop has no " + ChatColor.BOLD + "item" + ChatColor.RED + " configured.");
								player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/s item" + ChatColor.WHITE + " to set the item.");
								player.sendMessage(ChatColor.YELLOW + "============================================");
								player.sendMessage("");
								return;
							}
							if(sellPrice.containsKey(player.getName())) {
								int price = sellPrice.get(player.getName());
								
								shops.set(shopCode + ".SellPrice", price);
								DeadMC.ShopsFile.save();
								sellPrice.remove(player.getName());
								
								player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.GREEN + "Assigned new " + ChatColor.BOLD + "sell price" + ChatColor.GREEN + " to the shop!");
								
								return;
							}
							if(buyPrice.containsKey(player.getName())) {
								int price = buyPrice.get(player.getName());
								
								shops.set(shopCode + ".BuyPrice", price);
								DeadMC.ShopsFile.save();
								buyPrice.remove(player.getName());
								
								player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.GREEN + "Assigned new " + ChatColor.BOLD + "buy price" + ChatColor.GREEN + " to the shop!");
								
								return;
							}
							if(shops.getString(shopCode + ".BuyPrice") == null) {
								//buy price hasn't been set
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "================ Shop Help ================");
								player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Your shop is NOT functioning properly!");
								player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "This shop has no " + ChatColor.BOLD + "buy price" + ChatColor.RED + " configured.");
								player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/s buyprice" + ChatColor.WHITE + " to set the price.");
								player.sendMessage(ChatColor.YELLOW + "==========================================");
								player.sendMessage("");
								return;
							}
							
							//all is good
							sign.setLine(1, ChatColor.DARK_GREEN + "Stocked");
							sign.update();
							
						}
						
						//is functioning:
						
						ItemStack shopItem = new ItemStack(shops.getItemStack(shopCode + ".Item")).clone();
						String itemName = shopItem.getItemMeta().hasLore() ? shopItem.getItemMeta().getDisplayName() : shopItem.getType().toString().replace("_", " ");
						int buyPrice = shops.getInt(shopCode + ".BuyPrice");
						int sellPrice = 0;
						if(shops.getString(shopCode + ".SellPrice") != null)
							sellPrice = shops.getInt(shopCode + ".SellPrice");
						
						//get all the shopItem's in players inven
						int amountInInven = 0;
						List<ItemStack> playerStacks = getPlayersStacks(player, shopItem);
						List<ItemStack> playersDamagedStacks = getPlayersStacksDamaged(playerStacks);
						List<ItemStack> playersNonDamagedStacks = new ArrayList<ItemStack>();
						int playersNonDamagedItems = 0;
						for(ItemStack stack : playerStacks) {
							amountInInven += stack.getAmount();
							if(!playersDamagedStacks.contains(stack)) { //get non damaged items
								playersNonDamagedItems += stack.getAmount();
								playersNonDamagedStacks.add(stack);
							}
						}
						
						String regionName = Regions.getHighestPriorityLand(block.getLocation()).getId();
						String shopName = regionName.toLowerCase().substring(0, 1).toUpperCase() + regionName.toLowerCase().substring(1, regionName.length());
						
						//player is selling:
						if(sellAmount.containsKey(player.getName())) {
							if(sellPrice > 0) {
								
								//get amount:
								int amountPotential = sellAmount.get(player.getName());
								if(amountPotential == 0) //player wants to sell ALL
									amountPotential = amountInInven;
								
								//player has none
								if(amountInInven == 0) {
									player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.RED + "You do not have " + ChatColor.BOLD + itemName + ChatColor.RED + " to sell.");
									//add item enchants:
									for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
										String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
										player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + " - " + ChatColor.RED + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.RED + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.RED + ")");
									}
									//add stored (book) enchants:
									if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
										for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
											String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
											player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + " - " + ChatColor.RED + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.RED + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.RED + ")");
										}
									}
									confirmSell.remove(player.getName());
									sellAmount.remove(player.getName());
									return;
								}
								if(amountPotential > amountInInven) {
									amountPotential = amountInInven;
								}
								
								int deduction = 0;
								int numberOfDamaged = amountPotential - playersNonDamagedItems;
								int averagePercent = 0;
								if(amountPotential > playersNonDamagedItems) {
									//player is selling damaged items
									
									for(int count = 0; count < numberOfDamaged; count++) {
										ItemStack stack = playersDamagedStacks.get(count);
										Damageable damage = (Damageable) stack.getItemMeta();
										double maxDurability = stack.getType().getMaxDurability();
										int damagedPercent = 100 - (int) ((double) (damage.getDamage() / maxDurability) * 100);
										
										deduction += ((double) (100.0 - damagedPercent) / 100.0) * sellPrice;
										
										averagePercent += damagedPercent;
									}
									averagePercent /= numberOfDamaged; //get average
								}
								
								int cost = (amountPotential * sellPrice) - deduction;
								if(cost < 0) {
									player.sendMessage(ChatColor.RED + "[DeadMC] The total cost exceeds the maximum price.");
									return;
								}
								
								//if the shopCode matches the confirmation
								if(confirmSell.containsKey(player.getName())
										&& shopCode.equalsIgnoreCase(confirmSell.get(player.getName()))) {
									confirmSell.remove(player.getName());
									sellAmount.remove(player.getName());
									
									//take out items:
									int itemsTaken = 0;
									
									//get all non damaged items first:
									for(int count = 0; count < playersNonDamagedStacks.size(); count++) { //loop through all non damaged stacks
										
										int itemsStillNeeded = amountPotential - itemsTaken; //how many items do we still need to get
										if(itemsStillNeeded <= 0) break;
										
										ItemStack stack = playersNonDamagedStacks.get(count);
										if(stack.getAmount() <= itemsStillNeeded) {
											//can take whole stack
											itemsTaken += stack.getAmount();
											stack.setAmount(0); //remove the stack
										} else {
											//leaving left overs
											ItemStack newStack = stack.clone();
											newStack.setAmount(itemsStillNeeded); //only add what can fit from stack
											itemsTaken += newStack.getAmount();
											stack.setAmount(stack.getAmount() - itemsStillNeeded); //update the leftovers
										}
									}
									//then do damaged items:
									//NOTE: currently there are no damageable items that can hold more than 1 item in a stack. This is only implemented in case a damageable stackable item comes into the game.
									for(int count = 0; count < numberOfDamaged; count++) {
										
										int itemsStillNeeded = amountPotential - itemsTaken; //how many items do we still need to get
										if(itemsStillNeeded <= 0) break;
										
										ItemStack stack = playersDamagedStacks.get(count);
										if(stack.getAmount() <= itemsStillNeeded) {
											//can take whole stack
											itemsTaken += stack.getAmount();
											stack.setAmount(0); //remove the stack
										} else {
											//leaving left overs
											ItemStack newStack = stack.clone();
											newStack.setAmount(itemsStillNeeded); //only add what can fit from stack
											itemsTaken += newStack.getAmount();
											stack.setAmount(stack.getAmount() - itemsStillNeeded); //update the leftovers
										}
									}
									
									Economy.giveCoins(player.getUniqueId(), cost);
									
									player.sendMessage(ChatColor.YELLOW + "[Shop] " + ChatColor.GREEN + "Sold " + ChatColor.BOLD + amountPotential + "x" + ChatColor.GREEN + " " + itemName + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(cost) + ChatColor.GREEN + ".");
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GREEN + "Sold " + ChatColor.BOLD + amountPotential + "x" + ChatColor.GREEN + " " + itemName + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(cost) + ChatColor.GREEN + ".");
									
									//TASK
									if(itemName.toLowerCase().contains("log") && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.SELL_LOGS))
										TaskManager.stepTask(player.getUniqueId());
									
								} else {
									
									confirmSell.remove(player.getName()); //reset if player clicked a different sign
									
									player.sendMessage("");
									String title = shopName;
									player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
									
									//add discount for damaged items
									// - 1 item is damaged (60%)
									// - 3 items are damaged (average 55%)
									//   - you will be discounted (-X coins)
									//if player is attempting to sell more than non-damaged items
									
									if(numberOfDamaged > 0) {
										player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + numberOfDamaged + "x" + ChatColor.RED + " items are " + ChatColor.BOLD + "damaged" + ChatColor.RED + " (average " + ChatColor.BOLD + averagePercent + ChatColor.RED + "%)");
										player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "Deducted " + ChatColor.GOLD + Economy.convertCoins(deduction));
										player.sendMessage("");
									}
									
									player.sendMessage(ChatColor.WHITE + "Sell " + ChatColor.BOLD + amountPotential + "x" + ChatColor.WHITE + " " + ChatColor.AQUA + itemName + ChatColor.WHITE + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins((amountPotential * sellPrice) - deduction) + ChatColor.WHITE + "?");
									//add item enchants:
									for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
										String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
										player.sendMessage(ChatColor.WHITE + " - " + ChatColor.WHITE + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.WHITE + ")");
									}
									//add stored (book) enchants:
									if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
										for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
											String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
											player.sendMessage(ChatColor.WHITE + " - " + ChatColor.WHITE + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
										}
									}
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Click the sign again to confirm.");
									player.sendMessage(ChatColor.YELLOW + "==========================================");
									player.sendMessage("");
									
									confirmSell.put(player.getName(), shopCode);
									
									Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
										@Override
										public void run() {
											if(confirmSell.containsKey(player.getName())) {
												confirmSell.remove(player.getName());
											}
										}
									}, 400L); //20 seconds
								}
								
							} else {
								sellAmount.remove(player.getName());
								confirmSell.remove(player.getName());
								player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.RED + "This shop does not buy from players.");
							}
							return;
						}
						
						//player is buying:
						if(buyAmount.containsKey(player.getName())) {
							
							int getItemsCanFit = getItemsCanFit(player, shopItem); // how many items can the player take before becoming full
							if(getItemsCanFit == 0) {
								player.sendMessage("");
								String title = shopName;
								player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
								player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "Your inventory cannot fit any more items!");
								player.sendMessage(ChatColor.YELLOW + "==========================================");
								player.sendMessage("");
								confirmBuy.remove(player.getName());
								buyAmount.remove(player.getName());
								return;
							}
							//get amount:
							int amountPotential = buyAmount.get(player.getName());
							if(amountPotential == 0) //player wants to buy ALL
								amountPotential = getItemsCanFit;
							
							int amountCanFit = amountPotential; //the original amount the player can fit
							if(amountPotential > getItemsCanFit) {
								//if player is trying to buy more than their inventory can fit
								amountPotential = getItemsCanFit;
							}
							
							PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
							int coins = playerConfig.getInt("Coins");
							int cost = (amountPotential * buyPrice);
							if(cost < 0) {
								player.sendMessage(ChatColor.RED + "[DeadMC] The total cost exceeds the maximum price.");
								return;
							}
							
							int itemsCanAfford = (int) Math.floor(coins / buyPrice);
							//cant afford that many
							if(coins < cost) {
								
								player.sendMessage("");
								String title = shopName;
								player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
								if(itemsCanAfford == 0)
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "You can't afford this!");
								else
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "You can only afford " + ChatColor.BOLD + itemsCanAfford + ChatColor.RED + " items!");
								player.sendMessage(ChatColor.YELLOW + "==========================================");
								player.sendMessage("");
								confirmBuy.remove(player.getName());
								buyAmount.remove(player.getName());
								return;
							}
							
							//if the shopCode matches the confirmation
							if(confirmBuy.containsKey(player.getName())
									&& shopCode.equalsIgnoreCase(confirmBuy.get(player.getName()))) {
								confirmBuy.remove(player.getName());
								buyAmount.remove(player.getName());
								
								//take out items:
								int itemsTaken = 0;
								
								//put items in players inven
								ItemStack stack = shops.getItemStack(shopCode + ".Item");
								while(itemsTaken < amountPotential) {
									int itemsNeeded = amountPotential - itemsTaken;
									ItemStack newStack = stack.clone();
									if(itemsNeeded < newStack.getMaxStackSize()) {
										//items needed is not a full stack
										newStack.setAmount(itemsNeeded);
									} else {
										newStack.setAmount(newStack.getMaxStackSize());
									}
									itemsTaken += newStack.getAmount();
									player.getInventory().addItem(newStack); //add stack to player
								}
								
								Economy.takeCoins(player.getUniqueId(), cost);
								
								player.sendMessage(ChatColor.YELLOW + "[Shop] " + ChatColor.GREEN + "Bought " + ChatColor.BOLD + amountPotential + "x" + ChatColor.GREEN + " " + itemName + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(cost) + ChatColor.GREEN + ".");
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GREEN + "Bought " + ChatColor.BOLD + amountPotential + "x" + ChatColor.GREEN + " " + itemName + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(cost) + ChatColor.GREEN + ".");
								
								//TASK
								if(itemName.toLowerCase().contains("sword") && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.BUY_SWORD))
									TaskManager.stepTask(player.getUniqueId());
								
							} else {
								
								confirmBuy.remove(player.getName()); //reset if player clicked a different sign
								
								player.sendMessage("");
								String title = shopName;
								player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
								
								player.sendMessage(ChatColor.WHITE + "Buy " + ChatColor.BOLD + amountPotential + "x" + ChatColor.WHITE + " " + ChatColor.AQUA + itemName + ChatColor.WHITE + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins((amountPotential * buyPrice)) + ChatColor.WHITE + "?");
								//add item enchants:
								for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
									String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
									player.sendMessage(ChatColor.WHITE + " - " + ChatColor.WHITE + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.WHITE + ")");
								}
								//add stored (book) enchants:
								if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
									for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
										String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
										player.sendMessage(ChatColor.WHITE + " - " + ChatColor.WHITE + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
									}
								}
								player.sendMessage("");
								if(getItemsCanFit >= amountCanFit)
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + "Your inventory can fit " + ChatColor.GREEN + ChatColor.BOLD + "all" + ChatColor.WHITE + " of these items.");
								else
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "Your inventory can only fit " + ChatColor.BOLD + getItemsCanFit + ChatColor.RED + " of these items.");
								
								player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Click the sign again to confirm.");
								player.sendMessage(ChatColor.YELLOW + "==========================================");
								player.sendMessage("");
								
								confirmBuy.put(player.getName(), shopCode);
								
								Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
									@Override
									public void run() {
										if(confirmBuy.containsKey(player.getName())) {
											confirmBuy.remove(player.getName());
										}
									}
								}, 400L); //20 seconds
							}
							
							return;
						}
						
						//start message: ======================================================
						String title = shopName;
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
						
						//Buy price: 500 | Sell price: 200
						// > Diamond sword
						//   - Sharpness 5
						player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.AQUA + ChatColor.BOLD + itemName.substring(0, 1).toUpperCase() + itemName.substring(1, itemName.length()));
						//add item enchants:
						for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
							String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
							player.sendMessage(ChatColor.WHITE + "    - " + ChatColor.DARK_AQUA + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.WHITE + ")");
						}
						//add stored (book) enchants:
						if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
							for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
								String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
								player.sendMessage(ChatColor.WHITE + "    - " + ChatColor.DARK_AQUA + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
							}
						}
						player.sendMessage("");
						
						player.sendMessage(ChatColor.WHITE + "Price to " + ChatColor.BOLD + "BUY" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(buyPrice));
						player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.GOLD + ChatColor.BOLD + "/buy");
						if(sellPrice != 0) {
							player.sendMessage(ChatColor.WHITE + "Price to " + ChatColor.BOLD + "SELL" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(sellPrice));
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.GOLD + ChatColor.BOLD + "/sell");
							if(amountInInven > 0) {
								player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "   > " + ChatColor.WHITE + "You have " + ChatColor.WHITE + ChatColor.BOLD + "x" + amountInInven + ChatColor.WHITE + "");
							}
						}
						
						player.sendMessage(ChatColor.YELLOW + "==========================================");
						player.sendMessage("");
						
						//TASK
						if(itemName.toLowerCase().contains("log") && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.CLICK_LOG_SALE_SIGN))
							TaskManager.stepTask(player.getUniqueId());
						
						//TASK
						if(itemName.toLowerCase().contains("sword") && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.CLICK_IRON_SWORD_SALE_SIGN))
							TaskManager.stepTask(player.getUniqueId());
						
						
						return;
					}
					
					//player shops
					if(sign.getLine(0).equalsIgnoreCase("[Shop]")) {
						
						OfflinePlayer shopOwner = Regions.getOwnerAtLocation(sign.getLocation());
						//shop has no owner:
						if(shopOwner == null) {
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "This shop had no owner.");
							player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.WHITE + "The shop has been removed.");
							player.sendMessage("");
							for(int count = 0; count <= 3; count++)
								sign.setLine(count, "");
							sign.update(true);
							
							return;
						}
						
						//if town doesn't exist anymore:
						ProtectedRegion highestPriorityLand = Regions.getHighestPriorityLand(event.getClickedBlock().getLocation());
						if(!Town.townNameIsTaken(Regions.getOriginalTownName(highestPriorityLand.getId()))) {
							//town doesn't exist anymore
							player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "This shop is no longer active.");
							sign.setLine(0, "[Shop]");
							sign.setLine(1, ChatColor.DARK_RED + "Out of Order");
							sign.setLine(2, "");
							sign.setLine(3, " - not active - ");
							sign.update();
							
							return;
						}
						
						List<Chest> connectedChests = getConnectedChests(block);
						List<Chest> chestsNotOwned = getChestsNotOwned(connectedChests, shopOwner.getUniqueId());
						
						String shopCode = convertToCode(block.getLocation());
						RegionConfig regionConfig = RegionConfig.getConfig(highestPriorityLand.getId());
						
						if(chestsNotOwned.size() > 0 || connectedChests.size() == 0
								|| regionConfig.getString(shopCode + ".Item") == null
								|| regionConfig.getString(shopCode + ".BuyPrice") == null) {
							//shop is no longer in order
							sign.setLine(1, ChatColor.DARK_RED + "Out of Order");
							sign.update();
						}
						
						Boolean memberOfLand = Regions.playerCanBuild(player, event.getClickedBlock().getLocation(), false)
								&& !Regions.playerOwnsLand(event.getClickedBlock().getLocation(), player.getUniqueId(), false);
						
						if(!Regions.playerOwnsLand(event.getClickedBlock().getLocation(), player.getUniqueId(), false)
								&& !memberOfLand) {
							//player doesn't own land
							
							if(sign.getLine(1).equalsIgnoreCase(ChatColor.DARK_RED + "Out of Order")) {
								//out of order:
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "This shop is " + ChatColor.BOLD + "out of order" + ChatColor.WHITE + ".");
								player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.WHITE + "'" + ChatColor.BOLD + shopOwner.getName() + ChatColor.WHITE + "' is the shop owner.");
								player.sendMessage("");
								return;
							} else {
								//is functioning:
								
								ItemStack shopItem = new ItemStack(regionConfig.getItemStack(shopCode + ".Item")).clone();
								String itemName = shopItem.getItemMeta().hasLore() ? shopItem.getItemMeta().getDisplayName() : shopItem.getType().toString().replace("_", " ");
								HashMap<Chest, List<ItemStack>> stockStacks = getStockStacks(connectedChests, shopItem);
								List<ItemStack> damagedStock = getDamagedItems(connectedChests, stockStacks, shopItem); //includes non damaged (100%) items
								//get total stock available:
								int totalStock = getTotalStock(regionConfig, shopCode);
								
								int buyPrice = regionConfig.getInt(shopCode + ".BuyPrice");
								int sellPrice = 0;
								if(regionConfig.getString(shopCode + ".SellPrice") != null)
									sellPrice = regionConfig.getInt(shopCode + ".SellPrice");
								
								//get all the shopItem's in players inven
								int amountInInven = 0;
								List<ItemStack> playerStacks = getPlayersStacks(player, shopItem);
								List<ItemStack> playersDamagedStacks = getPlayersStacksDamaged(playerStacks);
								List<ItemStack> playersNonDamagedStacks = new ArrayList<ItemStack>();
								int playersNonDamagedItems = 0;
								for(ItemStack stack : playerStacks) {
									amountInInven += stack.getAmount();
									if(!playersDamagedStacks.contains(stack)) { //get non damaged items
										playersNonDamagedItems += stack.getAmount();
										playersNonDamagedStacks.add(stack);
									}
								}
								
								//player is selling:
								if(sellAmount.containsKey(player.getName())) {
									if(sellPrice > 0) {
										
										//get amount:
										int amountPotential = sellAmount.get(player.getName());
										if(amountPotential == 0) //player wants to sell ALL
											amountPotential = amountInInven;
										
										//player has none
										if(amountInInven == 0) {
											player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.RED + "You do not have " + ChatColor.BOLD + itemName + ChatColor.RED + " to sell.");
											//add item enchants:
											for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
												String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
												player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + " - " + ChatColor.RED + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.RED + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.RED + ")");
											}
											//add stored (book) enchants:
											if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
												for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
													String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
													player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.WHITE + " - " + ChatColor.RED + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.RED + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.RED + ")");
												}
											}
											confirmSell.remove(player.getName());
											sellAmount.remove(player.getName());
											return;
										}
										if(amountPotential > amountInInven) {
											amountPotential = amountInInven;
										}
										
										int getItemsCanFit = getItemsCanFit(shopCode, shopItem); // how many items can the shop take before becoming full
										
										if(getItemsCanFit == 0) {
											player.sendMessage("");
											String title = shopOwner.getName() + "'s Shop";
											player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
											player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "This shop cannot fit any more stock!");
											player.sendMessage(ChatColor.YELLOW + "==========================================");
											player.sendMessage("");
											
											confirmSell.remove(player.getName());
											sellAmount.remove(player.getName());
											return;
										}
										if(amountPotential > getItemsCanFit) {
											//if player is trying to sell more than the shop can fit
											amountPotential = getItemsCanFit;
										}
										
										int deduction = 0;
										int numberOfDamaged = amountPotential - playersNonDamagedItems;
										int averagePercent = 0;
										if(amountPotential > playersNonDamagedItems) {
											//player is selling damaged items
											
											for(int count = 0; count < numberOfDamaged; count++) {
												ItemStack stack = playersDamagedStacks.get(count);
												Damageable damage = (Damageable) stack.getItemMeta();
												double maxDurability = stack.getType().getMaxDurability();
												int damagedPercent = 100 - (int) ((double) (damage.getDamage() / maxDurability) * 100);
												
												deduction += ((double) (100.0 - damagedPercent) / 100.0) * sellPrice;
												
												averagePercent += damagedPercent;
											}
											averagePercent /= numberOfDamaged; //get average
										}
										
										PlayerConfig playerConfig = PlayerConfig.getConfig(shopOwner.getUniqueId());
										int coins = playerConfig.getInt("Coins");
										int cost = (amountPotential * sellPrice) - deduction;
										if(cost < 0) {
											player.sendMessage(ChatColor.RED + "[DeadMC] The total cost exceeds the maximum price.");
											return;
										}
										int itemsCanAfford = (int) Math.floor(coins / sellPrice);
										//cant afford that many
										if(coins < cost) {
											
											player.sendMessage("");
											String title = shopOwner.getName() + "'s Shop";
											player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
											if(itemsCanAfford == 0)
												player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + shopOwner.getName() + " can't afford to buy any more items!");
											else
												player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + shopOwner.getName() + " can only afford to buy " + ChatColor.BOLD + itemsCanAfford + ChatColor.RED + " items!");
											player.sendMessage(ChatColor.YELLOW + "==========================================");
											player.sendMessage("");
											confirmSell.remove(player.getName());
											sellAmount.remove(player.getName());
											return;
										}
										
										//if the shopCode matches the confirmation
										if(confirmSell.containsKey(player.getName())
												&& shopCode.equalsIgnoreCase(confirmSell.get(player.getName()))) {
											confirmSell.remove(player.getName());
											sellAmount.remove(player.getName());
											
											//take out items:
											int itemsTaken = 0;
											
											//get all non damaged items first:
											//Chat.broadcastMessage("Initial amount = " + amountPotential);
											for(int count = 0; count < playersNonDamagedStacks.size(); count++) { //loop through all non damaged stacks
												
												int itemsStillNeeded = amountPotential - itemsTaken; //how many items do we still need to get
												//Chat.broadcastMessage("Items still needed = " + itemsStillNeeded);
												if(itemsStillNeeded <= 0)
													break;
												
												ItemStack stack = playersNonDamagedStacks.get(count);
												//Chat.broadcastMessage("Trying stack " + count + "(x" + stack.getAmount() + ")");
												
												if(stack.getAmount() <= itemsStillNeeded) { //stack = 4, items needed = 16
													//can take whole stack as all items are needed
													
													//Chat.broadcastMessage(" > Taking entire stack as all items are needed");
													
													//if chest is full, remaining items will be returned to the player
													HashMap<Integer, ItemStack> remainingStack = addStackToShop(shopCode, stack.clone()); //attempts to add the stack, and then returns any remaining
													int remaining = remainingStack.containsKey(0) ? remainingStack.get(0).getAmount() : 0; //only feeding 1 stack, so always in index 0
													//Chat.broadcastMessage("    stack = x" + stack.getAmount() + " | remaining = " + remaining);
													
													itemsTaken += (stack.getAmount() - remaining);
													
													//Chat.broadcastMessage("   > Taken: x" + (stack.getAmount() - remaining) + " - Remaining: " + remainingStack + " - total x" + itemsTaken);
													
													stack.setAmount(remaining); //remove the stack
													
												} else {
													//leaving left overs
													
													//Chat.broadcastMessage(" > Taking part of the stack as not entire stack is needed");
													ItemStack newStack = stack.clone();
													newStack.setAmount(itemsStillNeeded); //only add what can fit from stack
													itemsTaken += newStack.getAmount();
													addStackToShop(shopCode, newStack);
													stack.setAmount(stack.getAmount() - itemsStillNeeded); //update the leftovers
												}
											}
	
											//then do damaged items:
											//NOTE: currently there are no damageable items that can hold more than 1 item in a stack. This is only implemented in case another stackable item comes into the game.
											for(int count = 0; count < numberOfDamaged; count++) {
												
												int itemsStillNeeded = amountPotential - itemsTaken; //how many items do we still need to get
												if(itemsStillNeeded <= 0)
													break;
												
												ItemStack stack = playersDamagedStacks.get(count);
												if(stack.getAmount() <= itemsStillNeeded) {
													//can take whole stack
													itemsTaken += stack.getAmount();
													addStackToShop(shopCode, stack.clone());
													stack.setAmount(0); //remove the stack
												} else {
													//leaving left overs
													ItemStack newStack = stack.clone();
													newStack.setAmount(itemsStillNeeded); //only add what can fit from stack
													itemsTaken += newStack.getAmount();
													addStackToShop(shopCode, newStack);
													stack.setAmount(stack.getAmount() - itemsStillNeeded); //update the leftovers
												}
											}
											
											//temporary bug fix if items taken is too much
											if(itemsTaken != amountPotential) {
												//bug only happens with non-damageable items so cost never has a deduction
												cost = itemsTaken * sellPrice;
												player.sendMessage(net.md_5.bungee.api.ChatColor.RED + "[DeadMC] " + org.bukkit.ChatColor.RED + "Woops! There was an error with the transaction, and not all items could be sold. " + net.md_5.bungee.api.ChatColor.WHITE + "An administrator has been notified.");
												if(cost < 0) {
													player.sendMessage(ChatColor.RED + "[DeadMC] The total cost exceeds the maximum price.");
													return;
												}
											}
											
											//update stats
											int totalSpendings = 0;
											if(regionConfig.getString(shopCode + ".Spendings") != null)
												totalSpendings = regionConfig.getInt(shopCode + ".Spendings");
											regionConfig.set(shopCode + ".Spendings", totalSpendings + cost);
											int unitsBought = 0;
											if(regionConfig.getString(shopCode + ".UnitsBought") != null)
												unitsBought = regionConfig.getInt(shopCode + ".UnitsBought");
											regionConfig.set(shopCode + ".UnitsBought", unitsBought + itemsTaken);
											
											String transactionString = player.getName() + ChatColor.BOLD + " sold " + ChatColor.WHITE + itemsTaken + "x " + itemName.toLowerCase() + ". " + ChatColor.RED + ChatColor.BOLD + "-" + ChatColor.GOLD + Economy.convertCoins(cost);
											
											//add transaction history
											for(int count = 1; count <= 4; count++) {
												//get empty transaction slot, else get the first one.
												if(count == 4) {
													//set 2 as 3
													regionConfig.set(shopCode + ".LastTransactions" + 1 + ".String", regionConfig.getString(shopCode + ".LastTransactions" + 2 + ".String"));
													regionConfig.set(shopCode + ".LastTransactions" + 1 + ".Time", regionConfig.getString(shopCode + ".LastTransactions" + 2 + ".Time"));
													
													//set 1 as 2
													regionConfig.set(shopCode + ".LastTransactions" + 2 + ".String", regionConfig.getString(shopCode + ".LastTransactions" + 3 + ".String"));
													regionConfig.set(shopCode + ".LastTransactions" + 2 + ".Time", regionConfig.getString(shopCode + ".LastTransactions" + 3 + ".Time"));
													
													//put in 1
													String discountString = "";
													if(deduction > 0) {
														discountString = ChatColor.ITALIC + "Discounted " + ChatColor.GOLD + ChatColor.ITALIC + "-" + Economy.convertCoins(deduction) + ChatColor.WHITE + ChatColor.ITALIC + " for " + numberOfDamaged + " damaged items";
														regionConfig.set(shopCode + ".LastTransactions" + 3 + ".Discount", discountString);
													}
													
													regionConfig.set(shopCode + ".LastTransactions" + 3 + ".String",
															transactionString);
													
													//set time of transaction
													ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
													regionConfig.set(shopCode + ".LastTransactions" + 3 + ".Time",
															DateCode.Encode(adelaideTime, true, false, false, true, true, true));
													
													regionConfig.save();
													break;
												}
												if(regionConfig.getString(shopCode + ".LastTransactions" + count + ".String") == null) { //just put in
													
													String discountString = "";
													if(deduction > 0) {
														discountString = ChatColor.ITALIC + "Discounted " + ChatColor.GOLD + ChatColor.ITALIC + "-" + Economy.convertCoins(deduction) + ChatColor.WHITE + ChatColor.ITALIC + " for " + numberOfDamaged + " damaged items";
														regionConfig.set(shopCode + ".LastTransactions" + count + ".Discount", discountString);
													}
													
													regionConfig.set(shopCode + ".LastTransactions" + count + ".String",
															transactionString);
													
													//set time of transaction
													ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
													regionConfig.set(shopCode + ".LastTransactions" + count + ".Time",
															DateCode.Encode(adelaideTime, true, false, false, true, true, true));
													
													regionConfig.save();
													
													break; //dont try next free spot
												}
											}
											
											Economy.takeCoins(shopOwner.getUniqueId(), cost);
											Economy.giveCoins(player.getUniqueId(), cost);
											
											if(Bukkit.getPlayer(shopOwner.getUniqueId()) != null) { //player is online
												Chat.sendTitleMessage(shopOwner.getUniqueId(), transactionString);
												Bukkit.getPlayer(shopOwner.getUniqueId()).playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_DIDGERIDOO, .3f, 2f);
											} else {
												//player not online
												PlayerConfig ownerConfig = PlayerConfig.getConfig(shopOwner);
												List<String> transactionsWhileOffline = new ArrayList<String>();
												if(ownerConfig.getString("ShopTransactionsWhileOffline") != null) {
													transactionsWhileOffline = ownerConfig.getStringList("ShopTransactionsWhileOffline");
												}
												transactionsWhileOffline.add(transactionString);
												ownerConfig.set("ShopTransactionsWhileOffline", transactionsWhileOffline);
												ownerConfig.save();
											}
											
											player.sendMessage(ChatColor.YELLOW + "[Shop] " + ChatColor.GREEN + "Sold " + ChatColor.BOLD + itemsTaken + "x" + ChatColor.GREEN + " " + itemName + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(cost) + ChatColor.GREEN + ".");
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GREEN + "Sold " + ChatColor.BOLD + itemsTaken + "x" + ChatColor.GREEN + " " + itemName + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(cost) + ChatColor.GREEN + ".");
											
											updateStock(regionConfig, shopCode, sign, getTotalStock(connectedChests, getStockStacks(connectedChests, shopItem), shopItem), null);
										} else {
											
											confirmSell.remove(player.getName()); //reset if player clicked a different sign
											
											player.sendMessage("");
											String title = shopOwner.getName() + "'s Shop";
											player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
											
											//add discount for damaged items
											// - 1 item is damaged (60%)
											// - 3 items are damaged (average 55%)
											//   - you will be discounted (-X coins)
											//if player is attempting to sell more than non-damaged items
											
											if(numberOfDamaged > 0) {
												player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + numberOfDamaged + "x" + ChatColor.RED + " items are " + ChatColor.BOLD + "damaged" + ChatColor.RED + " (average " + ChatColor.BOLD + averagePercent + ChatColor.RED + "%)");
												player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "Deducted " + ChatColor.GOLD + Economy.convertCoins(deduction));
												player.sendMessage("");
											}
											
											player.sendMessage(ChatColor.WHITE + "Sell " + ChatColor.BOLD + amountPotential + "x" + ChatColor.WHITE + " " + ChatColor.AQUA + itemName + ChatColor.WHITE + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins((amountPotential * sellPrice) - deduction) + ChatColor.WHITE + "?");
											//add item enchants:
											for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
												String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
												player.sendMessage(ChatColor.WHITE + " - " + ChatColor.WHITE + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.WHITE + ")");
											}
											//add stored (book) enchants:
											if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
												for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
													String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
													player.sendMessage(ChatColor.WHITE + " - " + ChatColor.WHITE + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
												}
											}
											player.sendMessage("");
											player.sendMessage(ChatColor.WHITE + "This shop can fit " + ChatColor.BOLD + getItemsCanFit + ChatColor.WHITE + " more stock.");
											player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Click the sign again to confirm.");
											player.sendMessage(ChatColor.YELLOW + "==========================================");
											player.sendMessage("");
											
											confirmSell.put(player.getName(), shopCode);
											
											Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
												@Override
												public void run() {
													if(confirmSell.containsKey(player.getName())) {
														confirmSell.remove(player.getName());
													}
												}
											}, 400L); //20 seconds
										}
										
									} else {
										sellAmount.remove(player.getName());
										confirmSell.remove(player.getName());
										player.sendMessage(ChatColor.YELLOW + "[Sell] " + ChatColor.RED + "This shop does not buy from players.");
									}
									return;
								}
								
								//player is buying:
								if(buyAmount.containsKey(player.getName())) {
									
									int amountInShop = 0;
									//get all the shopItem's in shops inven
									List<ItemStack> allStock = new ArrayList<ItemStack>();
									List<ItemStack> damagedStacks = getDamagedItems(connectedChests, stockStacks, shopItem);
									List<ItemStack> nonDamagedStacks = new ArrayList<ItemStack>();

									int nonDamagedItems = 0;
									for(Chest chest : connectedChests) {
										allStock.addAll(stockStacks.get(chest)); //get all item stacks in all chests
									}
									for(ItemStack stack : allStock) {
										amountInShop += stack.getAmount();
										if(!damagedStacks.contains(stack)) { //get non damaged items
											nonDamagedItems += stack.getAmount();
											nonDamagedStacks.add(stack);
										}
									}
									
									//get amount:
									int amountPotential = buyAmount.get(player.getName());
									if(amountPotential == 0) //player wants to buy ALL
										amountPotential = amountInShop;
									
									//shop has none
									if(totalStock == 0) {
										player.sendMessage(ChatColor.YELLOW + "[Buy] " + ChatColor.RED + "This shop is " + ChatColor.BOLD + "Out of Stock" + ChatColor.RED + "!");
										player.sendMessage(ChatColor.YELLOW + "[Shop] " + ChatColor.WHITE + "'" + ChatColor.BOLD + shopOwner.getName() + ChatColor.WHITE + "' is the shop owner.");
										confirmBuy.remove(player.getName());
										buyAmount.remove(player.getName());
										return;
									}
									if(amountPotential > amountInShop) {
										amountPotential = amountInShop;
									}
									
									int getItemsCanFit = getItemsCanFit(player, shopItem); // how many items can the player take before becoming full
									if(getItemsCanFit == 0) {
										player.sendMessage("");
										String title = shopOwner.getName() + "'s Shop";
										player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
										player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "Your inventory cannot fit any more items!");
										player.sendMessage(ChatColor.YELLOW + "==========================================");
										player.sendMessage("");
										confirmBuy.remove(player.getName());
										buyAmount.remove(player.getName());
										return;
									}
									int amountCanFit = amountPotential; //the original amount the player can fit
									if(amountPotential > getItemsCanFit) {
										//if player is trying to buy more than their inventory can fit
										amountPotential = getItemsCanFit;
									}
									
									int deduction = 0;
									int numberOfDamaged = amountPotential - nonDamagedItems;
									int averagePercent = 0;
									if(amountPotential > nonDamagedItems) {
										//player is buying damaged items
										
										for(int count = 0; count < numberOfDamaged; count++) {
											ItemStack stack = damagedStacks.get(count);
											Damageable damage = (Damageable) stack.getItemMeta();
											double maxDurability = stack.getType().getMaxDurability();
											int damagedPercent = 100 - (int) ((double) (damage.getDamage() / maxDurability) * 100);
											
											deduction += ((double) (100.0 - damagedPercent) / 100.0) * buyPrice;
											
											averagePercent += damagedPercent;
										}
										averagePercent /= numberOfDamaged; //get average
									}
									
									PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
									int coins = playerConfig.getInt("Coins");
									int cost = (amountPotential * buyPrice) - deduction;
									if(cost < 0) {
										player.sendMessage(ChatColor.RED + "[DeadMC] The total cost exceeds the maximum price.");
										return;
									}
									
									int itemsCanAfford = (int) Math.floor(coins / buyPrice);
									//cant afford that many
									if(coins < cost) {
										
										player.sendMessage("");
										String title = shopOwner.getName() + "'s Shop";
										player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
										if(itemsCanAfford == 0)
											player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "You can't afford this!");
										else
											player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "You can only afford " + ChatColor.BOLD + itemsCanAfford + ChatColor.RED + " items!");
										player.sendMessage(ChatColor.YELLOW + "==========================================");
										player.sendMessage("");
										confirmBuy.remove(player.getName());
										buyAmount.remove(player.getName());
										return;
									}
									
									//if the shopCode matches the confirmation
									if(confirmBuy.containsKey(player.getName())
											&& shopCode.equalsIgnoreCase(confirmBuy.get(player.getName()))) {
										confirmBuy.remove(player.getName());
										buyAmount.remove(player.getName());
										
										//take out items:
										int itemsTaken = 0;
										
										//get all non damaged items first:
										for(int count = 0; count < nonDamagedStacks.size(); count++) { //loop through all non damaged stacks
											
											int itemsStillNeeded = amountPotential - itemsTaken; //how many items do we still need to get
											if(itemsStillNeeded <= 0) break;
											
											ItemStack stack = nonDamagedStacks.get(count);
											if(stack.getAmount() <= itemsStillNeeded) {
												//can take whole stack
												itemsTaken += stack.getAmount();
												player.getInventory().addItem(stack.clone()); //add stack to player
												stack.setAmount(0); //remove the stack
											} else {
												//leaving left overs
												ItemStack newStack = stack.clone();
												newStack.setAmount(itemsStillNeeded); //only add what can fit from stack
												itemsTaken += newStack.getAmount();
												player.getInventory().addItem(newStack); //add stack to player
												stack.setAmount(stack.getAmount() - itemsStillNeeded); //update the leftovers
											}
											
										}
										//then do damaged items:
										//NOTE: currently there are no damageable items that can hold more than 1 item in a stack. This is only implemented in case another stackable item comes into the game.
										for(int count = 0; count < numberOfDamaged; count++) {
											
											int itemsStillNeeded = amountPotential - itemsTaken; //how many items do we still need to get
											if(itemsStillNeeded <= 0) break;
											
											ItemStack stack = damagedStacks.get(count);
											if(stack.getAmount() <= itemsStillNeeded) {
												//can take whole stack
												player.getInventory().addItem(stack.clone()); //add stack to player
												itemsTaken += stack.getAmount();
												stack.setAmount(0); //remove the stack
											} else {
												//leaving left overs
												ItemStack newStack = stack.clone();
												newStack.setAmount(itemsStillNeeded); //only add what can fit from stack
												itemsTaken += newStack.getAmount();
												player.getInventory().addItem(newStack); //add stack to player
												stack.setAmount(stack.getAmount() - itemsStillNeeded); //update the leftovers
											}
										}
										
										//temporary bug fix if items taken is too much
										if(itemsTaken != amountPotential) {
											//bug only happens with non-damageable items so cost never has a deduction
											cost = itemsTaken * sellPrice;
											player.sendMessage(net.md_5.bungee.api.ChatColor.RED + "[DeadMC] " + org.bukkit.ChatColor.RED + "Woops! There was an error with the transaction, and not all items could be bought. " + net.md_5.bungee.api.ChatColor.WHITE + "An administrator has been notified.");
											if(cost < 0) {
												player.sendMessage(ChatColor.RED + "[DeadMC] The total cost exceeds the maximum price.");
												return;
											}
										}
										
										//update stats
										int totalSales = 0;
										if(regionConfig.getString(shopCode + ".Sales") != null)
											totalSales = regionConfig.getInt(shopCode + ".Sales");
										regionConfig.set(shopCode + ".Sales", totalSales + cost);
										int unitsSold = 0;
										if(regionConfig.getString(shopCode + ".UnitsSold") != null)
											unitsSold = regionConfig.getInt(shopCode + ".UnitsSold");
										regionConfig.set(shopCode + ".UnitsSold", unitsSold + itemsTaken);
										
										String transactionString = player.getName() + ChatColor.BOLD + " bought " + ChatColor.WHITE + itemsTaken + "x " + itemName.toLowerCase() + ". " + ChatColor.GREEN + ChatColor.BOLD + "+" + ChatColor.GOLD + Economy.convertCoins(cost);
										
										//add transaction history/
										for(int count = 1; count <= 4; count++) {
											//get empty transaction slot, else get the first one.
											if(count == 4) {
												//set 2 as 3
												regionConfig.set(shopCode + ".LastTransactions" + 1 + ".String", regionConfig.getString(shopCode + ".LastTransactions" + 2 + ".String"));
												regionConfig.set(shopCode + ".LastTransactions" + 1 + ".Discount", regionConfig.getString(shopCode + ".LastTransactions" + 2 + ".Discount"));
												regionConfig.set(shopCode + ".LastTransactions" + 1 + ".Time", regionConfig.getString(shopCode + ".LastTransactions" + 2 + ".Time"));
												
												//set 1 as 2
												regionConfig.set(shopCode + ".LastTransactions" + 2 + ".String", regionConfig.getString(shopCode + ".LastTransactions" + 3 + ".String"));
												regionConfig.set(shopCode + ".LastTransactions" + 2 + ".Discount", regionConfig.getString(shopCode + ".LastTransactions" + 3 + ".Discount"));
												regionConfig.set(shopCode + ".LastTransactions" + 2 + ".Time", regionConfig.getString(shopCode + ".LastTransactions" + 3 + ".Time"));
												
												//put in 1
												String discountString = "";
												if(deduction > 0) {
													discountString = ChatColor.ITALIC + "Discounted " + ChatColor.GOLD + ChatColor.ITALIC + "-" + Economy.convertCoins(deduction) + ChatColor.WHITE + ChatColor.ITALIC + " for " + numberOfDamaged + " damaged items";
													regionConfig.set(shopCode + ".LastTransactions" + 3 + ".Discount", discountString);
												}
												
												regionConfig.set(shopCode + ".LastTransactions" + 3 + ".String",
														transactionString);
												
												//set time of transaction
												ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
												regionConfig.set(shopCode + ".LastTransactions" + 3 + ".Time",
														DateCode.Encode(adelaideTime, true, false, false, true, true, true));
												
												regionConfig.save();
												break;
											}
											if(regionConfig.getString(shopCode + ".LastTransactions" + count + ".String") == null) { //just put in
												
												String discountString = "";
												if(deduction > 0) {
													discountString = ChatColor.ITALIC + "Discounted " + ChatColor.GOLD + ChatColor.ITALIC + "-" + Economy.convertCoins(deduction) + ChatColor.WHITE + ChatColor.ITALIC + " for " + numberOfDamaged + " damaged items";
													regionConfig.set(shopCode + ".LastTransactions" + count + ".Discount", discountString);
												}
												
												regionConfig.set(shopCode + ".LastTransactions" + count + ".String",
														transactionString);
												
												//set time of transaction
												ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
												regionConfig.set(shopCode + ".LastTransactions" + count + ".Time",
														DateCode.Encode(adelaideTime, true, false, false, true, true, true));
												
												regionConfig.save();
												
												break; //dont try next free spot
											}
										}
										
										Economy.giveCoins(shopOwner.getUniqueId(), cost);
										Economy.takeCoins(player.getUniqueId(), cost);
										
										if(Bukkit.getPlayer(shopOwner.getUniqueId()) != null) { //player is online
											Chat.sendTitleMessage(shopOwner.getUniqueId(), transactionString);
											Bukkit.getPlayer(shopOwner.getUniqueId()).playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_CHIME, .3f, 2f);
										} else {
											//player not online
											PlayerConfig ownerConfig = PlayerConfig.getConfig(shopOwner);
											List<String> transactionsWhileOffline = new ArrayList<String>();
											if(ownerConfig.getString("ShopTransactionsWhileOffline") != null) {
												transactionsWhileOffline = ownerConfig.getStringList("ShopTransactionsWhileOffline");
											}
											transactionsWhileOffline.add(transactionString);
											ownerConfig.set("ShopTransactionsWhileOffline", transactionsWhileOffline);
											ownerConfig.save();
										}
										
										player.sendMessage(ChatColor.YELLOW + "[Shop] " + ChatColor.GREEN + "Bought " + ChatColor.BOLD + itemsTaken + "x" + ChatColor.GREEN + " " + itemName + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(cost) + ChatColor.GREEN + ".");
										Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GREEN + "Bought " + ChatColor.BOLD + itemsTaken + "x" + ChatColor.GREEN + " " + itemName + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(cost) + ChatColor.GREEN + ".");
										
										updateStock(regionConfig, shopCode, sign, getTotalStock(connectedChests, getStockStacks(connectedChests, shopItem), shopItem), null);
										
									} else {
										
										confirmBuy.remove(player.getName()); //reset if player clicked a different sign
										
										player.sendMessage("");
										String title = shopOwner.getName() + "'s Shop";
										player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
										
										//add discount for damaged items
										// - 1 item is damaged (60%)
										// - 3 items are damaged (average 55%)
										//   - you will be discounted (-X coins)
										//if player is attempting to sell more than non-damaged items
										
										if(numberOfDamaged > 0) {
											player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + numberOfDamaged + "x" + ChatColor.RED + " items are " + ChatColor.BOLD + "damaged" + ChatColor.RED + " (average " + ChatColor.BOLD + averagePercent + ChatColor.RED + "%)");
											player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "Deducted " + ChatColor.GOLD + Economy.convertCoins(deduction));
											player.sendMessage("");
										}
										
										player.sendMessage(ChatColor.WHITE + "Buy " + ChatColor.BOLD + amountPotential + "x" + ChatColor.WHITE + " " + ChatColor.AQUA + itemName + ChatColor.WHITE + " for " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins((amountPotential * buyPrice) - deduction) + ChatColor.WHITE + "?");
										//add item enchants:
										for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
											String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
											player.sendMessage(ChatColor.WHITE + " - " + ChatColor.WHITE + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.WHITE + ")");
										}
										//add stored (book) enchants:
										if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
											for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
												String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
												player.sendMessage(ChatColor.WHITE + " - " + ChatColor.WHITE + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
											}
										}
										player.sendMessage("");
										if(getItemsCanFit >= amountCanFit)
											player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + "Your inventory can fit " + ChatColor.GREEN + ChatColor.BOLD + "all" + ChatColor.WHITE + " of these items.");
										else
											player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "Your inventory can only fit " + ChatColor.BOLD + getItemsCanFit + ChatColor.RED + " of these items.");
										player.sendMessage(ChatColor.WHITE + "The shop has " + ChatColor.BOLD + totalStock + ChatColor.WHITE + " total stock.");
										
										player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Click the sign again to confirm.");
										player.sendMessage(ChatColor.YELLOW + "==========================================");
										player.sendMessage("");
										
										confirmBuy.put(player.getName(), shopCode);
										
										Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
											@Override
											public void run() {
												if(confirmBuy.containsKey(player.getName())) {
													confirmBuy.remove(player.getName());
												}
											}
										}, 400L); //20 seconds
									}
									
									return;
								}
								
								//start message: ======================================================
								String title = shopOwner.getName() + "'s Shop";
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(title, 40) + " " + title + " " + Chat.getTitlePlaceholder(title, 40));
								
								//Buy price: 500 | Sell price: 200
								// > Diamond sword
								//   - Sharpness 5
								player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.AQUA + ChatColor.BOLD + itemName.substring(0, 1).toUpperCase() + itemName.substring(1, itemName.length()));
								//add item enchants:
								for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
									String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
									player.sendMessage(ChatColor.WHITE + "    - " + ChatColor.DARK_AQUA + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.WHITE + ")");
								}
								//add stored (book) enchants:
								if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
									for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
										String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
										player.sendMessage(ChatColor.WHITE + "    - " + ChatColor.DARK_AQUA + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
									}
								}
								player.sendMessage("");
								
								if(totalStock > shopItem.getMaxStackSize())
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.GREEN + "Stock: " + ChatColor.GREEN + ChatColor.BOLD + totalStock + ChatColor.GREEN);
								else if(totalStock != 0)
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + ChatColor.ITALIC + "Only " + ChatColor.RED + ChatColor.BOLD + totalStock + ChatColor.RED + ChatColor.ITALIC + " left!");
								else
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + "Stock: " + ChatColor.RED + ChatColor.BOLD + "out of stock");
								if(damagedStock.size() > 0) {
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "   > " + ChatColor.RED + "Damaged: " + ChatColor.RED + ChatColor.BOLD + damagedStock.size());
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "   > " + ChatColor.WHITE + "These will be sold last at a discount");
								}
								player.sendMessage("");
								
								player.sendMessage(ChatColor.WHITE + "Price to " + ChatColor.BOLD + "BUY" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(buyPrice));
								player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.GOLD + ChatColor.BOLD + "/buy");
								if(sellPrice != 0) {
									player.sendMessage(ChatColor.WHITE + "Price to " + ChatColor.BOLD + "SELL" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(sellPrice));
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.GOLD + ChatColor.BOLD + "/sell");
									if(amountInInven > 0) {
										player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "   > " + ChatColor.WHITE + "You have " + ChatColor.WHITE + ChatColor.BOLD + "x" + amountInInven + ChatColor.WHITE + "");
									}
								}
								
								player.sendMessage(ChatColor.YELLOW + "==========================================");
								player.sendMessage("");
								
								return;
							}
							
						}
						
						if(memberOfLand)
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Land Share] " + ChatColor.RED + "You cannot buy and sell to this shop.");
						
						//player.sendMessage(connectedChests.size() + " chests connected");
						if(connectedChests.size() == 0) {
							//no chest connected
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "================ Shop Help ================");
							player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Your shop is NOT functioning properly!");
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "This shop doesn't have a chest connected.");
							player.sendMessage(ChatColor.WHITE + "Place a chest either:");
							player.sendMessage(ChatColor.WHITE + " - Directly " + ChatColor.BOLD + "under" + ChatColor.WHITE + " the sign, or");
							player.sendMessage(ChatColor.WHITE + " - " + ChatColor.BOLD + "Behind the block" + ChatColor.WHITE + " the sign is attached to");
							player.sendMessage(ChatColor.YELLOW + "==========================================");
							player.sendMessage("");
							return;
						}
						
						//player.sendMessage(chestsNotOwned.size() + " chests connected that aren't owned");
						if(chestsNotOwned.size() > 0) {
							//chests don't have enough stock
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "================ Shop Help ================");
							player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Your shop is NOT functioning properly!");
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "There are " + ChatColor.BOLD + chestsNotOwned.size() + ChatColor.RED + " connected chests on land you don't own.");
							for(Chest chest : chestsNotOwned)
								player.sendMessage(ChatColor.WHITE + " - Chest @ (" + chest.getLocation().getBlockX() + ", " + chest.getLocation().getBlockY() + ", " + chest.getLocation().getBlockZ() + ")");
							player.sendMessage(ChatColor.WHITE + "These chests must be on your land.");
							player.sendMessage(ChatColor.YELLOW + "==========================================");
							player.sendMessage("");
							return;
						}
						
						if(setItem.containsKey(player.getName()) && !memberOfLand) {
							ItemStack item = setItem.get(player.getName());
							ItemMeta meta = item.getItemMeta();
							String itemName = "";
							if(meta.hasLore()) {
								//special item
								itemName = meta.getDisplayName();
							} else {
								if(item.getEnchantments().keySet().size() > 0)
									itemName = ChatColor.AQUA + "(e)";
								itemName += ChatColor.WHITE + item.getType().toString().toUpperCase().replace("_", " ");
							}
							
							regionConfig.set(shopCode + ".Item", item);
							regionConfig.save();
							setItem.remove(player.getName());
							
							player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.GREEN + "Assigned " + ChatColor.BOLD + itemName + ChatColor.GREEN + " to the shop!");
							sign.setLine(2, "" + itemName);
							sign.update();
							
							if(regionConfig.getString(shopCode + ".BuyPrice") != null)
								updateStock(regionConfig, shopCode, sign, getTotalStock(connectedChests, getStockStacks(connectedChests, item), item), null);
							
							return;
						}
						
						if(regionConfig.getString(shopCode + ".Item") == null) {
							//item hasn't been set
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "================ Shop Help ================");
							player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Your shop is NOT functioning properly!");
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "This shop has no " + ChatColor.BOLD + "item" + ChatColor.RED + " configured.");
							player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/s item" + ChatColor.WHITE + " to set the item.");
							player.sendMessage(ChatColor.YELLOW + "==========================================");
							player.sendMessage("");
							return;
						}
						ItemStack shopItem = new ItemStack(regionConfig.getItemStack(shopCode + ".Item")).clone();
						HashMap<Chest, List<ItemStack>> stockStacks = getStockStacks(connectedChests, shopItem);
						//get total stock available:
						int totalStock = getTotalStock(regionConfig, shopCode);
						List<ItemStack> damagedStock = getDamagedItems(connectedChests, stockStacks, shopItem);
						
						if(sellPrice.containsKey(player.getName()) && !memberOfLand) {
							int price = sellPrice.get(player.getName());
							
							regionConfig.set(shopCode + ".SellPrice", price);
							regionConfig.save();
							sellPrice.remove(player.getName());
							
							player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.GREEN + "Assigned new " + ChatColor.BOLD + "sell price" + ChatColor.GREEN + " to the shop!");
							
							return;
						}
						if(buyPrice.containsKey(player.getName()) && !memberOfLand) {
							int price = buyPrice.get(player.getName());
							
							regionConfig.set(shopCode + ".BuyPrice", price);
							regionConfig.save();
							buyPrice.remove(player.getName());
							
							player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.GREEN + "Assigned new " + ChatColor.BOLD + "buy price" + ChatColor.GREEN + " to the shop!");
							
							if(regionConfig.getString(shopCode + ".Item") != null)
								updateStock(regionConfig, shopCode, sign, totalStock, null);
							
							return;
						}
						if(regionConfig.getString(shopCode + ".BuyPrice") == null) {
							//buy price hasn't been set
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "================ Shop Help ================");
							player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Your shop is NOT functioning properly!");
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + "This shop has no " + ChatColor.BOLD + "buy price" + ChatColor.RED + " configured.");
							player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/s buyprice" + ChatColor.WHITE + " to set the price.");
							player.sendMessage(ChatColor.YELLOW + "==========================================");
							player.sendMessage("");
							return;
						}
						
						//ALL FUNCTIONING:
						if(sign.getLine(1).equalsIgnoreCase(ChatColor.DARK_RED + "Out of Order")) { //fix sign if it says out of order
							updateStock(regionConfig, shopCode, sign, totalStock, null);
						}
						
						int buyPrice = regionConfig.getInt(shopCode + ".BuyPrice");
						int sellPrice = 0;
						if(regionConfig.getString(shopCode + ".SellPrice") != null)
							sellPrice = regionConfig.getInt(shopCode + ".SellPrice");
						int totalSales = 0;
						if(regionConfig.getString(shopCode + ".Sales") != null)
							totalSales = regionConfig.getInt(shopCode + ".Sales");
						int unitsSold = 0;
						if(regionConfig.getString(shopCode + ".UnitsSold") != null)
							unitsSold = regionConfig.getInt(shopCode + ".UnitsSold");
						int totalSpendings = 0;
						if(regionConfig.getString(shopCode + ".Spendings") != null)
							totalSpendings = regionConfig.getInt(shopCode + ".Spendings");
						int unitsBought = 0;
						if(regionConfig.getString(shopCode + ".UnitsBought") != null)
							unitsBought = regionConfig.getInt(shopCode + ".UnitsBought");
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "================ Shop Help ================");
						//Buy price: 500 | Sell price: 200
						// > Diamond sword
						//   - Sharpness 5
						String itemName = shopItem.getItemMeta().hasLore() ? shopItem.getItemMeta().getDisplayName() : shopItem.getType().toString().toLowerCase().replace("_", " ");
						player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.AQUA + ChatColor.BOLD + itemName.substring(0, 1).toUpperCase() + itemName.substring(1, itemName.length()));
						//add item enchants:
						for(Enchantment enchantment : shopItem.getEnchantments().keySet()) {
							String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
							player.sendMessage(ChatColor.WHITE + "    - " + ChatColor.DARK_AQUA + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + shopItem.getEnchantmentLevel(enchantment) + ChatColor.WHITE + ")");
						}
						//add stored (book) enchants:
						if(shopItem.getItemMeta() instanceof EnchantmentStorageMeta) {
							for(Enchantment enchantment : ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants().keySet()) {
								String enchantmentName = enchantment.getKey().toString().replace("minecraft:", "").replace("_", " ");
								player.sendMessage(ChatColor.WHITE + "    - " + ChatColor.DARK_AQUA + ChatColor.BOLD + enchantmentName.substring(0, 1).toUpperCase() + enchantmentName.substring(1, enchantmentName.length()) + ChatColor.WHITE + " (Level " + ChatColor.BOLD + ((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchantLevel(enchantment) + ChatColor.WHITE + ")");
							}
						}
						
						if(totalStock > shopItem.getMaxStackSize())
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.GREEN + "Stock: " + ChatColor.GREEN + ChatColor.BOLD + totalStock + ChatColor.GREEN);
						else if(totalStock != 0)
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.RED + ChatColor.ITALIC + "Only " + ChatColor.RED + ChatColor.BOLD + totalStock + ChatColor.RED + ChatColor.ITALIC + " left!");
						else
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + "Stock: " + ChatColor.RED + ChatColor.BOLD + "out of stock");
						if(damagedStock.size() > 0) {
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "   > " + ChatColor.RED + "Damaged: " + ChatColor.RED + ChatColor.BOLD + damagedStock.size());
							player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "   > " + ChatColor.WHITE + "These will be sold last at a discount");
						}
						int getItemsCanFit = getItemsCanFit(shopCode, shopItem); // how many items can the shop take before becoming full
						player.sendMessage(ChatColor.WHITE + "This shop can fit " + ChatColor.BOLD + getItemsCanFit + ChatColor.WHITE + " more stock.");
						player.sendMessage("");
						
						if(sellPrice != 0) {
							player.sendMessage(ChatColor.WHITE + "Price to " + ChatColor.BOLD + "BUY" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(buyPrice) + ChatColor.WHITE + " | " + "Price to " + ChatColor.BOLD + "SELL" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(sellPrice));
						} else {
							player.sendMessage(ChatColor.WHITE + "Price to " + ChatColor.BOLD + "BUY" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(buyPrice));
						}
						
						player.sendMessage("");
						
						player.sendMessage(ChatColor.BOLD + "STATISTICS");
						player.sendMessage(ChatColor.WHITE + "Total sales: " + ChatColor.GREEN + ChatColor.BOLD + "+" + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(totalSales) + ChatColor.WHITE + " (" + ChatColor.AQUA + unitsSold + ChatColor.WHITE + " items)");
						if(regionConfig.getString(shopCode + ".Spendings") != null)
							player.sendMessage(ChatColor.WHITE + "Total spendings: " + ChatColor.RED + ChatColor.BOLD + "-" + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(totalSpendings) + ChatColor.WHITE + " (" + ChatColor.AQUA + unitsBought + ChatColor.WHITE + " items)");
						
						player.sendMessage("");
						player.sendMessage(ChatColor.BOLD + "Last 3 transactions:");
						if(regionConfig.getString(shopCode + ".LastTransactions1.String") == null) {
							player.sendMessage(ChatColor.WHITE + " - " + ChatColor.RED + "No transactions yet!");
						} else {
							for(int count = 3; count >= 1; count--) {
								if(regionConfig.getString(shopCode + ".LastTransactions" + count + ".String") != null) {
									int timeSince = DateCode.getTimeSince(regionConfig.getString(shopCode + ".LastTransactions" + count + ".Time"));
									String timeSinceString = timeSince + " minutes";
									if(timeSince > 60)
										timeSinceString = Math.round(timeSince / 60.0) + " hours";
									if(timeSince > 2880)
										timeSinceString = Math.round(timeSince / 1440.0) + " days";
									
									player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + regionConfig.getString(shopCode + ".LastTransactions" + count + ".String"));
									if(regionConfig.getString(shopCode + ".LastTransactions" + count + ".Discount") != null)
										player.sendMessage(ChatColor.WHITE + "    > " + regionConfig.getString(shopCode + ".LastTransactions" + count + ".Discount"));
									player.sendMessage(ChatColor.WHITE + "    > " + ChatColor.BOLD + timeSinceString + ChatColor.WHITE + " ago");
								}
							}
						}
						
						//player.sendMessage(ChatColor.WHITE + " - Creeper bought 5 diamond sword's for 1250 coins. (Discounted 178 coins for damaged items) (12 minutes ago)");
						//player.sendMessage(ChatColor.WHITE + " - NGLAS sold 1 diamond sword for 200 coins. (2 days ago)");
						//Your shop is functioning properly! (green)
						//Your shop is out of stock! (red)
						//Selling item:
						// > Diamond sword
						//   - Sharpness 5
						//
						//Stock: 0 (<= maxStackSize = yellow etc.)
						//Of which X are damaged (if > 0)
						//Damaged items will be sold at a discount (if > 0)
						//
						//Buy price:
						//Sell price: (if sell price exists)
						//
						//Total sales: 150 coins
						//Last 3 transactions:
						// - Creeper bought 5 diamond sword's for 1250 coins. (Discounted 178 coins for damaged items) (12 minutes ago)
						// - Dia25 sold 1 diamond sword for 200 coins. (2 days ago)
						player.sendMessage(ChatColor.YELLOW + "==========================================");
						player.sendMessage("");
						
					}
				}
			}
		} catch(Exception e) {
			event.setCancelled(true);
			Chat.logError(e, player);
		}
	}
	
	//returns any remaining items
	public HashMap<Integer, ItemStack> addStackToShop(String shopCode, ItemStack stack) {
		List<Chest> connectedShops = getConnectedChests(decodeCode(shopCode).getBlock());
		
		HashMap<Integer, ItemStack> leftOvers = new HashMap<Integer, ItemStack>();
		
		int chest = 0;
		do {
			if(chest >= connectedShops.size()) {
				//cant fit any more! return remaining amount:
				return leftOvers;
			}
			
			Inventory inventory = connectedShops.get(chest).getInventory();
			leftOvers = inventory.addItem(stack); //add item to chest
			chest++;
		} while(leftOvers.size() != 0);
		
		return leftOvers; //successfully added all stock - return empty hashmap
		
	}
	
	public static int getItemsCanFit(Player player, ItemStack stack) {
		Inventory inventory = player.getInventory();
		
		int totalAmountFree = 0;
		
		for(Integer slot : inventory.all(stack.getType()).keySet()) {
			//get all the item stacks in the chest - these = maxStackSize - getAmount()
			totalAmountFree += stack.getMaxStackSize() - inventory.getItem(slot).getAmount();
		}
		//loop each slot
		for(int slotCount = 0; slotCount <= 35; slotCount++) {
			if(inventory.getItem(slotCount) == null || inventory.getItem(slotCount).getType() == Material.AIR) {
				//if empty, can add a full stack
				totalAmountFree += stack.getMaxStackSize();
			}
		}
		
		return totalAmountFree;
	}
	
	public int getItemsCanFit(String shopCode, ItemStack stack) {
		List<Chest> connectedShops = getConnectedChests(decodeCode(shopCode).getBlock());
		
		int totalAmountFree = 0;
		
		for(Chest chest : connectedShops) { //loop each connected chest
			Inventory inventory = chest.getInventory();

			for(Integer slot : inventory.all(stack.getType()).keySet()) {
				//get all the item stacks in the chest - these = maxStackSize - getAmount()
				totalAmountFree += stack.getMaxStackSize() - inventory.getItem(slot).getAmount();
			}
			//loop each slot
			for(int slotCount = 0; slotCount < inventory.getContents().length; slotCount++) {
				if(inventory.getItem(slotCount) == null || inventory.getItem(slotCount).getType() == Material.AIR) {
					//if empty, can add a full stack
					totalAmountFree += stack.getMaxStackSize();
				}
			}
			
		}
		
		return totalAmountFree;
	}
	
	@EventHandler(priority =  EventPriority.HIGHEST)
	public void hopperMoveItemFromShop(InventoryMoveItemEvent event) {
		
		if(event.getSource() != null && event.getSource().getType() == InventoryType.CHEST) {
			Block sourceBlock = event.getSource().getLocation().getBlock();
			//moving out of chest, check if it's a shop and cancel
			
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			ApplicableRegionSet regionsAtLoc = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(sourceBlock.getLocation()));
			
			if(regionsAtLoc.size() > 0) {
				List<Sign> connectedShops = getConnectedShops(sourceBlock);
				if(connectedShops.size() > 0) {
					event.setCancelled(true);
				}
			}
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(event.getDestination() != null && event.getSource() != null
					&& event.getDestination().getType() == InventoryType.CHEST
					&& event.getSource().getType() == InventoryType.HOPPER) {
				Block sourceBlock = event.getSource().getLocation().getBlock();
				//moving into chest from hopper, if it's a shop, update the stock
				RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
				ApplicableRegionSet regionsAtLoc = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(sourceBlock.getLocation()));
				
				if(regionsAtLoc.size() > 0 && event.getDestination().getLocation().getBlock() != null) {
					Block destinationBlock = event.getDestination().getLocation().getBlock();
					Bukkit.getScheduler().runTask(plugin, () -> {
						List<Sign> connectedShops = getConnectedShops(destinationBlock);
						
						Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
							for(Sign sign : connectedShops) {
								ProtectedRegion highestPriorityLand = Regions.getHighestPriorityLand(sign.getLocation());
								if(highestPriorityLand == null) continue;
								
								File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + highestPriorityLand.getId() + ".yml");
								if(regionFile.exists()) {
									RegionConfig regionConfig = RegionConfig.getConfig(highestPriorityLand.getId());
									String shopCode = convertToCode(sign.getLocation());
									Bukkit.getScheduler().runTask(plugin, () -> {
										List<Chest> connectedChests = getConnectedChests(sign.getBlock());
										ItemStack shopItem = regionConfig.getItemStack(shopCode + ".Item").clone();
										updateStock(regionConfig, shopCode, sign, getTotalStock(connectedChests, getStockStacks(connectedChests, shopItem), shopItem), null);
									});
								}
							}
						}, 25L); //hopper transfer rate is 24 ticks, so wait until all transferred
					});
				}
			}
		});
	}
	@EventHandler
	public void onChestBreak(BlockBreakEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();
		
		if(block.getState() instanceof Chest) {
			
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			ApplicableRegionSet regionsAtLoc = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(block.getLocation()));
			if(regionsAtLoc.size() > 0) {
				
				List<Sign> connectedShops = getConnectedShops(block);
				
				for(Sign sign : connectedShops) {
					ProtectedRegion highestPriorityLand = Regions.getHighestPriorityLand(sign.getLocation());
					if(highestPriorityLand == null) continue;
					
					File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + highestPriorityLand.getId() + ".yml");
					if(regionFile.exists()) {
						RegionConfig regionConfig = RegionConfig.getConfig(highestPriorityLand.getId());
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "You removed a chest connected to a shop!");
						
						Bukkit.getScheduler().runTaskLater(plugin, () -> {
							String shopCode = convertToCode(sign.getLocation());
							ItemStack shopItem = regionConfig.getItemStack(shopCode + ".Item").clone();
							List<Chest> connectedChests = getConnectedChests(sign.getBlock());
							updateStock(regionConfig, shopCode, sign, getTotalStock(connectedChests, getStockStacks(connectedChests, shopItem), shopItem), player);
							
							if(connectedChests.size() == 1) { //no more connected shops
								sign.setLine(1, ChatColor.DARK_RED + "Out of Order");
								sign.update();
							}
						}, 5L);

					}
				}
				
			}
		} else {
			
			//if block has sign attached
			//and sign is a shop sign
			//if player is not the owner
			//cancel
			
			if(!(block.getState() instanceof Sign)) {
				List<Block> blocksToTry = new ArrayList<Block>();
				blocksToTry.add(block.getRelative(BlockFace.NORTH));
				blocksToTry.add(block.getRelative(BlockFace.EAST));
				blocksToTry.add(block.getRelative(BlockFace.SOUTH));
				blocksToTry.add(block.getRelative(BlockFace.WEST));
				for(Block blockToTry : blocksToTry) {
					
					//if surrounding block is a wall sign
					if(blockToTry.getBlockData() instanceof WallSign) {
						Sign sign = (Sign) blockToTry.getState();
						//and if the block behind the sign is the broken block
						BlockFace oppositeFace = ((Directional) blockToTry.getBlockData()).getFacing();
						Block blockBehind = block.getRelative(oppositeFace);
						if(blockBehind.equals(blockToTry)) {
							
							//therefore the sign depends on the block
							if(sign.getLine(0).equalsIgnoreCase("[Shop]")) {
								OfflinePlayer shopOwner = Regions.getOwnerAtLocation(sign.getLocation());
								if(shopOwner != null && shopOwner.getUniqueId() != player.getUniqueId()) {
									player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.RED + "You cannot break this block as it is a part of " + ChatColor.BOLD + shopOwner.getName() + ChatColor.RED + "'s shop.");
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "================ Shop Help ================");
									player.sendMessage(ChatColor.RED + "Your shop depends on this block.");
									player.sendMessage(ChatColor.WHITE + "Destroy the shop first to remove this block.");
									player.sendMessage(ChatColor.YELLOW + "==========================================");
									player.sendMessage("");
								}
								event.setCancelled(true);
								return;
							}
							
						}
						
					}
					
				}
			}
			
		}
	}
	
	@EventHandler
	public void onChestPlace(BlockPlaceEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();
		
		//if placed chest that is the only chest to a shop,
		if(block.getState() instanceof Chest) {
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			ApplicableRegionSet regionsAtLoc = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(block.getLocation()));
			if(regionsAtLoc.size() > 0) {
				
				List<Sign> connectedShops = getConnectedShops(block);
				
				for(Sign sign : connectedShops) {
					ProtectedRegion highestPriorityLandAtChest = Regions.getHighestPriorityLand(sign.getLocation()); //the highest priority land at the sign
					if(highestPriorityLandAtChest == null) continue;
					File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + highestPriorityLandAtChest.getId() + ".yml");
					if(regionFile.exists()) {
						RegionConfig regionConfig = RegionConfig.getConfig(highestPriorityLandAtChest.getId());
						
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Shops] " + ChatColor.GREEN + "Chest connected to shop!");
						if(getConnectedChests(sign.getBlock()).size() == 1) {
							
							String shopCode = convertToCode(sign.getLocation());
							if(regionConfig.getString(shopCode + ".Item") != null
									|| regionConfig.getString(shopCode + ".BuyPrice") != null) {
								//change to out of stock
								sign.setLine(1, ChatColor.DARK_RED + "Out of Stock");
								sign.update();
							}
							
						}
					}
				}
				
			}
		}
	}
	
	
	//just gets the reference for how many are in the chest
	public static int getTotalStock(RegionConfig regionConfig, String shopCode) {
		if(regionConfig.getString(shopCode + ".TotalStock") != null)
			return regionConfig.getInt(shopCode + ".TotalStock");
		return 0;
	}
	//actually searches the chests for items
	public static int getTotalStock(List<Chest> connectedChests, HashMap<Chest, List<ItemStack>> stockStacks, ItemStack shopItem) {
		int totalStock = 0;
		for(Chest chest : connectedChests) {
			int chestsStock = 0;
			for(ItemStack stack : stockStacks.get(chest)) {
				chestsStock += stack.getAmount();
			}
			totalStock += chestsStock;
			//player.sendMessage("Chest has " + chestsStock + " items.");
		}
		//player.sendMessage("Shop has " + totalStock + " total stock.");
		//player.sendMessage("Of which " + damagedItems.size() + " are damaged.");
		
		return totalStock;
	}
	
	//not including 100% items
	public static List<ItemStack> getDamagedItems(List<Chest> connectedChests, HashMap<Chest, List<ItemStack>> stockStacks, ItemStack shopItem) {
		
		if(shopItem.getItemMeta() instanceof Damageable && shopItem.getType() != Material.PLAYER_HEAD) {
			HashMap<ItemStack, Integer> damagedItemsNotInOrder = new HashMap<ItemStack, Integer>();
			
			for(Chest chest : connectedChests) {
				for(ItemStack stack : stockStacks.get(chest)) {
					//get damaged items:
					ItemMeta meta = stack.getItemMeta();
					if(meta instanceof Damageable) {
						
						Damageable damage = (Damageable) meta;
						double maxDurability = stack.getType().getMaxDurability();
						
						int damagedPercent = 100 - (int) ((double) (damage.getDamage() / maxDurability) * 100);
						//player.sendMessage(damagedPercent + "%");
						
						if(damagedPercent < 100)
							damagedItemsNotInOrder.put(stack, damagedPercent);
					}
				}
			}
			
			//sort in order of damage
			Map<ItemStack, Integer> sorted = damagedItemsNotInOrder.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(
					toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2, LinkedHashMap::new));
			Object[] inorder = sorted.keySet().toArray();
			
			List<ItemStack> damagedItemsInOrder = new ArrayList<ItemStack>();
			for(Object object : inorder) {
				ItemStack stack = (ItemStack) object;
				damagedItemsInOrder.add(stack);
			}
			
			return damagedItemsInOrder;
		} else return new ArrayList<ItemStack>();
	}
	
	public void updateStock(RegionConfig regionConfig, String shopCode, Sign sign, int totalStock, Player playerToNotify) {
		//Chat.broadcastMessage("Stock: " + totalStock + " - Item: " + shopItem.getType());
		
		ItemStack shopItem = regionConfig.getItemStack(shopCode + ".Item").clone();
		
		if(playerToNotify != null)
			Chat.sendTitleMessage(playerToNotify.getUniqueId(), ChatColor.YELLOW + "[Shop] " + ChatColor.WHITE + "Stock: " + ChatColor.BOLD + totalStock);
		if(totalStock == 0) {
			if(!sign.getLine(1).equalsIgnoreCase(ChatColor.DARK_RED + "Out of Stock")) {
				sign.setLine(1, ChatColor.DARK_RED + "Out of Stock");
				sign.update();
			}
		} else if(totalStock <= shopItem.getMaxStackSize()) {
			if(!sign.getLine(1).equalsIgnoreCase(ChatColor.GOLD + "Low Stock")) {
				sign.setLine(1, ChatColor.GOLD + "Low Stock");
				sign.update();
			}
		} else {
			if(!sign.getLine(1).equalsIgnoreCase(ChatColor.DARK_GREEN + "Stocked")) {
				sign.setLine(1, ChatColor.DARK_GREEN + "Stocked");
				sign.update();
			}
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			regionConfig.set(shopCode + ".TotalStock", totalStock);
			regionConfig.save();
		});
	}
	
	@EventHandler
	public void onClickChestInRevampWorld(PlayerInteractEvent event) {
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.getClickedBlock().getLocation().getWorld().getName().equalsIgnoreCase("revamp")) {
				Block block = event.getClickedBlock().getLocation().getBlock();
				
				if(block.getType() == Material.ENDER_CHEST) {
					event.getPlayer().sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "Cannot access ender chests in this area.");
					event.setCancelled(true);
				}
			}
		}
	}
	
	//update shops when adding stock to chests:
	@EventHandler
	public void onClickShopInventory(InventoryClickEvent event) {
		if(event.getClickedInventory() != null) { //check its not null
			if(event.getInventory() != null && event.getInventory().getLocation() != null) {
				Block block = event.getInventory().getLocation().getBlock();
				
				//Chat.broadcastMessage("Click inventory: " + block.getBlockData());
				
				if(block.getType() == Material.CHEST) { //chest the block is a chest
					//Chat.broadcastMessage("Is a chest");
					
					List<Block> blocksToTry = new ArrayList<Block>();
					InventoryHolder holder = ((Chest) block.getState()).getInventory().getHolder();
					if(holder instanceof DoubleChest) {
						DoubleChest doubleChest = ((DoubleChest) holder);
						blocksToTry.add(((Chest) doubleChest.getLeftSide()).getBlock());
						blocksToTry.add(((Chest) doubleChest.getRightSide()).getBlock());
					} else {
						//single chest
						blocksToTry.add(block);
					}
					
					for(Block blockToTry : blocksToTry) {
						
						List<Sign> connectedShops = getConnectedShops(blockToTry);
						for(Sign sign : connectedShops) {
							//Chat.broadcastMessage("Updating...");
							
							ProtectedRegion highestPriorityLand = Regions.getHighestPriorityLand(sign.getLocation());
							RegionConfig regionConfig = RegionConfig.getConfig(highestPriorityLand.getId());
							
							String shopCode = convertToCode(sign.getLocation());
							if(regionConfig.getString(shopCode + ".BuyPrice") != null) { //don't update if not setup
								//wait until after the event to update the stock:
								Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
									@Override
									public void run() {
										List<Chest> connectedChests = getConnectedChests(sign.getBlock());
										ItemStack shopItem = new ItemStack(regionConfig.getItemStack(shopCode + ".Item")).clone();
										
										Player player = null;
										if(event.getWhoClicked() instanceof Player)
											player = (Player) event.getWhoClicked();
										updateStock(regionConfig, shopCode, sign, getTotalStock(connectedChests, getStockStacks(connectedChests, shopItem), shopItem), player);
										//Chat.broadcastMessage("Updated!");
									}
								}, 1L);
							}
							
						}
						
					}
				}
				
			}
		}
	}
	
	@EventHandler
	public void onDragShopInventory(InventoryDragEvent event) {
		if(event.getInventory() != null && event.getInventory().getLocation() != null) {
			Block block = event.getInventory().getLocation().getBlock();
			
			//Chat.broadcastMessage("Click inventory: " + block.getBlockData());
			
			if(block.getType() == Material.CHEST) { //chest the block is a chest
				//Chat.broadcastMessage("Is a chest");
				
				List<Block> blocksToTry = new ArrayList<Block>();
				InventoryHolder holder = ((Chest) block.getState()).getInventory().getHolder();
				if(holder instanceof DoubleChest) {
					DoubleChest doubleChest = ((DoubleChest) holder);
					blocksToTry.add(((Chest) doubleChest.getLeftSide()).getBlock());
					blocksToTry.add(((Chest) doubleChest.getRightSide()).getBlock());
				} else {
					//single chest
					blocksToTry.add(block);
				}
				
				for(Block blockToTry : blocksToTry) {
					
					List<Sign> connectedShops = getConnectedShops(blockToTry);
					for(Sign sign : connectedShops) {
						//Chat.broadcastMessage("Updating...");
						
						ProtectedRegion highestPriorityLand = Regions.getHighestPriorityLand(sign.getLocation());
						RegionConfig regionConfig = RegionConfig.getConfig(highestPriorityLand.getId());
						
						String shopCode = convertToCode(sign.getLocation());
						if(regionConfig.getString(shopCode + ".BuyPrice") != null) { //don't update if not setup
							//wait until after the event to update the stock:
							Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
								@Override
								public void run() {
									List<Chest> connectedChests = getConnectedChests(sign.getBlock());
									ItemStack shopItem = new ItemStack(regionConfig.getItemStack(shopCode + ".Item")).clone();
									
									Player player = null;
									if(event.getWhoClicked() instanceof Player)
										player = (Player) event.getWhoClicked();
									updateStock(regionConfig, shopCode, sign, getTotalStock(connectedChests, getStockStacks(connectedChests, shopItem), shopItem), player);
									//Chat.broadcastMessage("Updated!");
								}
							}, 1L);
						}
						
					}
					
				}
			}
			
		}
		
	}
	
	//use with a chest to find what shops it is used for
	public List<Sign> getConnectedShops(Block block) {
		List<Sign> connectedShops = new ArrayList<Sign>();
		
		List<Block> blocksToTry = new ArrayList<Block>();
		blocksToTry.add(block.getRelative(BlockFace.UP));
		blocksToTry.add(block.getRelative(BlockFace.NORTH).getRelative(BlockFace.NORTH));
		blocksToTry.add(block.getRelative(BlockFace.EAST).getRelative(BlockFace.EAST));
		blocksToTry.add(block.getRelative(BlockFace.SOUTH).getRelative(BlockFace.SOUTH));
		blocksToTry.add(block.getRelative(BlockFace.WEST).getRelative(BlockFace.WEST));
		
		for(Block blockToTry : blocksToTry) {
			if(blockToTry.getBlockData() instanceof WallSign) {
				Sign sign = (Sign) blockToTry.getState();
				if(sign.getLine(0).equalsIgnoreCase("[shop]")
						&& getConnectedChests(blockToTry).contains((Chest) block.getState())) {
					connectedShops.add(sign);
				}
			}
		}
		
		return connectedShops;
	}
	
	public static String convertToCode(Location location) {
		return new String(location.getBlockX() + "," + location.getBlockY() + "," + location.getBlockZ());
	}
	
	public static Location decodeCode(String string) {
		
		int x = 0;
		int y = 0;
		int z = 0;
		
		for(int coord = 0; coord < 3; coord++) {
			int numberOfCommas = 0;
			int valueStart = -1;
			int valueEnd = -1;
			
			for(int count = 0; count < string.length(); count++) {
				
				if(numberOfCommas == coord && valueStart == -1)
					valueStart = count;
				
				if(string.charAt(count) == ',')
					numberOfCommas++;
				
				if((numberOfCommas > coord || (numberOfCommas == 2 && count == (string.length() - 1)))
						&& valueEnd == -1) {
					valueEnd = count - 1;
					if(coord == 0) x = Integer.parseInt(string.substring(valueStart, valueEnd + 1));
					if(coord == 1) y = Integer.parseInt(string.substring(valueStart, valueEnd + 1));
					if(coord == 2) z = Integer.parseInt(string.substring(valueStart, valueEnd + 2));
					break;
				}
				
			}
		}
		
		return new Location(Bukkit.getWorld("world"), x, y, z);
	}
	
	public static List<Chest> getConnectedChests(Block block) {
		List<Chest> connectedChests = new ArrayList<Chest>();
		
		List<Block> blocksToTry = new ArrayList<Block>();
		
		if(block.getBlockData() instanceof WallSign) {
			BlockFace oppositeFace = ((Directional) block.getBlockData()).getFacing().getOppositeFace();
			Block blockTwoBehind = block.getRelative(oppositeFace).getRelative(oppositeFace);
			
			blocksToTry.add(blockTwoBehind);
			blocksToTry.add(block.getRelative(BlockFace.DOWN));
		}
		
		for(Block blockToTry : blocksToTry) {
			if(blockToTry.getType() == Material.CHEST)
				connectedChests.add((Chest) blockToTry.getState());
		}
		
		return connectedChests;
	}
	
	//get the item stacks in players inventory
	public static List<ItemStack> getPlayersStacks(Player player, ItemStack shopItem) {
		List<ItemStack> stacksInInven = new ArrayList<ItemStack>();
		
		for(Integer slot : player.getInventory().all(shopItem.getType()).keySet()) {
			//loop each item stack with material
			ItemStack stackAtSlot = player.getInventory().getItem(slot);
			ItemMeta meta = stackAtSlot.getItemMeta();

			//has same enchants
			if(stackAtSlot.getEnchantments().equals(shopItem.getEnchantments())
					&& (!shopItem.getItemMeta().hasLore() || (meta.hasLore() && (meta.getLore().equals(shopItem.getItemMeta().getLore())  //or the shop item isn't custom, OR if the lore matches
					|| (CustomItems.isQuiver(shopItem) && CustomItems.isQuiver(stackAtSlot) && CustomItems.getQuiverTier(shopItem) == CustomItems.getQuiverTier(stackAtSlot))))) // OR if the item is a quiver and has the same tier
					&& (!(stackAtSlot.getItemMeta() instanceof EnchantmentStorageMeta) || ((EnchantmentStorageMeta) stackAtSlot.getItemMeta()).getStoredEnchants().equals(((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants()))) {
				//add to list
				stacksInInven.add(stackAtSlot);
			}
		}
		
		return stacksInInven;
	}
	
	//not including 100% items
	public List<ItemStack> getPlayersStacksDamaged(List<ItemStack> stacksInInven) {
		HashMap<ItemStack, Integer> damagedItemsNotInOrder = new HashMap<ItemStack, Integer>();
		
		for(ItemStack stack : stacksInInven) {
			//get damaged items:
			ItemMeta meta = stack.getItemMeta();
			if(meta instanceof Damageable) {
				
				Damageable damage = (Damageable) meta;
				double maxDurability = stack.getType().getMaxDurability();
				
				int damagedPercent = 100 - (int) ((double) (damage.getDamage() / maxDurability) * 100);
				//player.sendMessage(damagedPercent + "%");
				
				if(damagedPercent < 100)
					damagedItemsNotInOrder.put(stack, damagedPercent);
			}
		}
		
		
		//sort in order of damage
		Map<ItemStack, Integer> sorted = damagedItemsNotInOrder.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(
				toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2, LinkedHashMap::new));
		Object[] inorder = sorted.keySet().toArray();
		
		List<ItemStack> damagedItemsInOrder = new ArrayList<ItemStack>();
		for(Object object : inorder) {
			ItemStack stack = (ItemStack) object;
			damagedItemsInOrder.add(stack);
		}
		
		return damagedItemsInOrder;
	}
	
	//get the shopItem stacks in each chest
	public static HashMap<Chest, List<ItemStack>> getStockStacks(List<Chest> connectedChests, ItemStack shopItem) {
		HashMap<Chest, List<ItemStack>> stock = new HashMap<Chest, List<ItemStack>>();
		
		for(Chest chest : connectedChests) {
			//loop all chests with stock
			
			List<ItemStack> itemsInChest = new ArrayList<ItemStack>(); //the shopItem stacks in the chest
			
			for(Integer slot : chest.getInventory().all(shopItem.getType()).keySet()) {
				//loop each item stack with material
				ItemStack stackAtSlot = chest.getInventory().getItem(slot);
				ItemMeta meta = stackAtSlot.getItemMeta();
				
				//has same enchants? and if custom item, does it also have custom features (lore) ?
				if(stackAtSlot.getEnchantments().equals(shopItem.getEnchantments())
						&& (!shopItem.getItemMeta().hasLore() || (meta.hasLore() && (meta.getLore().equals(shopItem.getItemMeta().getLore())  //or the shop item isn't custom, OR if the lore matches
						|| (CustomItems.isQuiver(shopItem) && CustomItems.isQuiver(stackAtSlot) && CustomItems.getQuiverTier(shopItem) == CustomItems.getQuiverTier(stackAtSlot))))) // OR if the item is a quiver and has the same tier
						&& (!(meta instanceof EnchantmentStorageMeta) || ((EnchantmentStorageMeta) meta).getStoredEnchants().equals(((EnchantmentStorageMeta) shopItem.getItemMeta()).getStoredEnchants()))) {
					//add to list
					itemsInChest.add(stackAtSlot);
				}
			}
			
			stock.put(chest, itemsInChest);
		}
		
		return stock;
	}
	
	//loop the connected chests and find any that the player doesn't own
	public List<Chest> getChestsNotOwned(List<Chest> chests, UUID owner) {
		List<Chest> chestsNotOwned = new ArrayList<Chest>();
		
		for(Chest chestToTry : chests) {
			
			InventoryHolder holder = chestToTry.getInventory().getHolder();
			if(holder instanceof DoubleChest) {
				DoubleChest doubleChest = ((DoubleChest) holder);
				Chest leftChest = (Chest) doubleChest.getLeftSide();
				Chest rightChest = (Chest) doubleChest.getRightSide();
				
				//check left chest
				if(!Regions.playerOwnsLand(leftChest.getLocation(), owner, false))
					chestsNotOwned.add(leftChest);
				//check right chest
				if(!Regions.playerOwnsLand(rightChest.getLocation(), owner, false))
					chestsNotOwned.add(rightChest);
				
			} else {
				//single chest
				if(!Regions.playerOwnsLand(chestToTry.getLocation(), owner, false))
					chestsNotOwned.add(chestToTry);
			}
			
		}
		
		return chestsNotOwned;
	}
	
}
