package com.deadmc.DeadAPImaven;

import org.bukkit.ChatColor;

public class ShopData {
	public int shops = 0;
	public int stocked = 0;
	public String percent = "0%";
	
	public ShopData(int shops, int stocked) {
		int percentStocked = (int) ((double)((double)stocked/(double)shops) * 100);
		percent = ChatColor.GREEN + "" + percentStocked + "%";
		if(percentStocked <= 70 && percentStocked > 40) //less than 3.5 in 5
			percent = ChatColor.YELLOW + "" + percentStocked + "%";
		if(percentStocked <= 40) //less than 2 in 5
			percent = ChatColor.RED + "" + percentStocked + "%";
		this.shops = shops;
		this.stocked = stocked;
	}
	
}
