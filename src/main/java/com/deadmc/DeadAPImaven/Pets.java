package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Donator.Rank;
import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.Disguise;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import me.libraryaddict.disguise.disguisetypes.watchers.SheepWatcher;
import me.libraryaddict.disguise.disguisetypes.watchers.SnowmanWatcher;
import me.libraryaddict.disguise.disguisetypes.watchers.ZombieWatcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.entity.Cat.Type;
import org.bukkit.entity.Parrot.Variant;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class Pets implements CommandExecutor, Listener {
	private static DeadMC plugin;
	
	public Pets(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static List<String> allPetUUIDs = new ArrayList<String>();
	
	@EventHandler
	private void parrotLeftShoulder(CreatureSpawnEvent event) {
		try {
			Entity entity = event.getEntity();
			if(event.getSpawnReason() == SpawnReason.SHOULDER_ENTITY
					&& entity instanceof Parrot
					&& entity.isInvulnerable()) {
				
				Collection<Entity> nearbyPlayers = event.getLocation().getWorld().getNearbyEntities(event.getLocation(), 3, 3, 3); //1 around
				Player player = null;
				for(Entity e : nearbyPlayers) {
					if(e instanceof Player) {
						player = (Player) e;
					}
				}
				
				//remove reference
				if(player != null) { //was an actual pet parrot
					//update pet reference
					List<String> currentPetsUUIDS = new ArrayList<String>();
					currentPetsUUIDS.add(entity.getUniqueId().toString());
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					playerConfig.set("Pets.Active.Reference", currentPetsUUIDS);
					playerConfig.save();
				}
			}
		} catch(Exception e) {
			Chat.logError(e);
		}
	}
	
	public enum Pet {
		//special:
		Spooky_Spider,
		Spooky_Nether_Spider,
		
		//zombies:
		Zombie,
		Husk,
		Drowned,
		Zombie_Villager,
//		BloodMooner, //on fire
//		Destructor,
//		Bomber,
//		Phantom,
//		Tank,
//		Zombie_Piglin,
//		
//		//rare:
//		Wither,
//		Blaze,
//		Strider,
//		Magma_Cube,
//		Bee,

//from breeding:
		
		Chicken,
		Pig,
		Cow,
		Sheep,

//donator:
		
		//baron:
		Cat,
		Wolf,
		Parrot,

//		//royal
//		Donkey,
//		Fox,
//		Horse,
//		Mule,
//		
//		//emperor:
//		Iron_Golem,
//		Turtle,
//		Polar_Bear,
//		Panda,
//		Silverfish,
//		Ravager,
//		Mooshroom,
//      Mini-Player

//      Holiday specials:
        Snow_Golem
//      Bat
	}
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		Player player = null;
		if(sender instanceof Player)
			player = (Player) sender;
		
		if(commandLabel.equalsIgnoreCase("pet") || commandLabel.equalsIgnoreCase("pets")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			//despawn pets: (/pet)
			if(args.length == 0) {
				if(playerConfig.getString("Pets.Active.Reference") != null) {
					//despawn current pets
					DespawnPets(player, true);
					
					int ordinal = playerConfig.getInt("Pets.Active.Ordinal");
					Pet pet = Pet.values()[ordinal];
					String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
					String petName = playerConfig.getString("Pets." + pet.ordinal() + ".Name") != null ? playerConfig.getString("Pets." + pet.ordinal() + ".Name") : entityNick;
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Pet] " + ChatColor.RED + ChatColor.BOLD + ChatColor.translateAlternateColorCodes('&', petName) + ChatColor.RED + " went home.");
					
					playerConfig.set("Pets.Active", null);
					playerConfig.save();
					
					return true;
				}
			}
			
			//show pets (/pet (<page>))
			
			int petsPerPage = 4;
			int pageNumber = 0;
			if(args.length > 0 && Chat.isInteger(args[0])) {
				pageNumber = Integer.parseInt(args[0]) - 1;
			}
			
			int maxPetsToInclude = (petsPerPage + (pageNumber * petsPerPage)) - 1; //start at page 0
			int minPetsToInclude = pageNumber * petsPerPage;
			//0 - 9 
			// 10 - 19
			
			//get pets on page
			List<Pet> petsOnPage = new ArrayList<Pet>();
			int count = 0;
			for(Pet pet : Pet.values()) {
				if(count >= minPetsToInclude && count <= maxPetsToInclude) {
					petsOnPage.add(pet);
				}
				if(count > maxPetsToInclude) break;
				count++;
			}
			
			int numberOfPages = (int) Math.ceil((double) (Pet.values().length / petsPerPage));
			
			player.sendMessage("");
			player.sendMessage(ChatColor.YELLOW + "================== " + ChatColor.BOLD + "DeadMC Pets" + ChatColor.YELLOW + " ==================");
			player.sendMessage(ChatColor.WHITE + "You have obtained " + ChatColor.BOLD + Pets.getObtainedPets(playerConfig).size() + ChatColor.WHITE + " of " + ChatColor.BOLD + Pet.values().length + ChatColor.WHITE + " pets.");
			player.sendMessage("");
			if(pageNumber > numberOfPages) {
				player.sendMessage(ChatColor.RED + "   Page #" + (pageNumber + 1) + " does not exist.");
				player.sendMessage("");
			}
			for(Pet pet : petsOnPage) {
				Boolean obtained = playerConfig.getString("Pets." + pet.ordinal() + ".Obtained") != null;
				ChatColor color = obtained ? ChatColor.GREEN : ChatColor.RED; //if obtained or not
				String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
				player.sendMessage(color + "" + ChatColor.BOLD + entityNick + ChatColor.WHITE + ": " + DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Description"));
				if(obtained) {
					if(playerConfig.getString("Pets." + pet.ordinal() + ".Name") != null) {
						player.sendMessage("  - Name: " + ChatColor.AQUA + ChatColor.ITALIC + ChatColor.BOLD + ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Pets." + pet.ordinal() + ".Name")));
					} else {
						player.sendMessage("  - Use " + ChatColor.GOLD + "/pet setname" + ChatColor.WHITE + " to give a name.");
					}
				}
			}
			player.sendMessage("");
			player.sendMessage(ChatColor.WHITE + "Displaying page " + (pageNumber + 1) + ChatColor.WHITE + " of " + (numberOfPages + 1) + " (use " + ChatColor.GOLD + "/pets <page>" + ChatColor.WHITE + ")");
			player.sendMessage(" > Use " + ChatColor.GOLD + "/pet <name>" + ChatColor.WHITE + " to activate your pet.");
			player.sendMessage(" > Use " + ChatColor.GOLD + "/pet" + ChatColor.WHITE + " to deactivate your pet.");
			// 0 / 20 pets obtained
			// Use /pet <page> for next page
			player.sendMessage(ChatColor.YELLOW + "=================================================");
			player.sendMessage("");
			
			if(pageNumber > 0) return true;
			
			//set name and spawn:
			if(args.length > 0) {
				
				if(args[0].equalsIgnoreCase("setname")) {
					if(args.length >= 3) { //pet setname <pet> <name>
						Pet pet = getPet(args[1], playerConfig);
						if(pet != null) {
							String name = args[2];
							if(!(name.toLowerCase().contains("fuck")
									|| name.toLowerCase().contains("cunt")
									|| name.toLowerCase().contains("pussy")
									|| name.toLowerCase().contains("nigga")
									|| name.toLowerCase().contains("nigger"))) {
								if(name.length() > 15) {
									player.sendMessage(ChatColor.YELLOW + "[Pets] " + ChatColor.RED + "Name must be less than 16 characters.");
									return true;
								}
								
								String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
								
								Boolean obtained = playerConfig.getString("Pets." + pet.ordinal() + ".Obtained") != null;
								if(obtained) {
									
									//change the name
									playerConfig.set("Pets." + pet.ordinal() + ".Name", name);
									playerConfig.save();
									
									//if it is the current pet:
									if(playerConfig.getString("Pets.Active.Reference") != null
											&& pet.ordinal() == playerConfig.getInt("Pets.Active.Ordinal")) {
										//update the name
										List<String> current = playerConfig.getStringList("Pets.Active.Reference");
										if(current != null) {
											for(String entityUUID : current) {
												Entity entity = Bukkit.getEntity(UUID.fromString(entityUUID));
												if(playerConfig.getInt("DonateRank") >= Rank.Baron.ordinal()) {
													entity.setCustomName(ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Pets." + pet.ordinal() + ".Name")));
												} else
													entity.setCustomName(playerConfig.getString("Pets." + pet.ordinal() + ".Name"));
												entity.setCustomNameVisible(true);
											}
										}
									}
									
									if(playerConfig.getInt("DonateRank") >= Rank.Baron.ordinal()) {
										player.sendMessage(ChatColor.YELLOW + "[Pets] " + ChatColor.GREEN + "Set the name for " + entityNick + " to " + ChatColor.BOLD + ChatColor.translateAlternateColorCodes('&', args[2]) + ChatColor.GREEN + ".");
									} else {
										player.sendMessage(ChatColor.YELLOW + "[Pets] " + ChatColor.GREEN + "Set the name for " + entityNick + " to " + ChatColor.BOLD + args[2] + ChatColor.GREEN + ".");
									}
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Pets] " + ChatColor.RED + "You have not obtained the " + entityNick + " pet.");
							} else
								player.sendMessage(ChatColor.YELLOW + "[Pets] " + ChatColor.RED + "Name cannot contain offensive language.");
						} else
							player.sendMessage(ChatColor.YELLOW + "[Pet] " + ChatColor.RED + "Unknown pet '" + args[0] + "'.");
					} else
						player.sendMessage(ChatColor.YELLOW + "[Pets] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/pet setname <old name> <new name>" + ChatColor.WHITE + ".");
					
					return true; //break
				}
				
				Pet pet = getPet(args[0], playerConfig);
				if(pet != null) {
					String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
					
					Boolean obtained = playerConfig.getString("Pets." + pet.ordinal() + ".Obtained") != null;
					if(obtained) { //if pet is unlocked
						if(DeadMC.PetsFile.data().getString("Pets." + pet.ordinal() + ".RankNeeded") == null
								|| playerConfig.getInt("DonateRank") >= DeadMC.PetsFile.data().getInt("Pets." + pet.ordinal() + ".RankNeeded")) {
							Spawn(player, pet);
							String petName = playerConfig.getString("Pets." + pet.ordinal() + ".Name") != null ? playerConfig.getString("Pets." + pet.ordinal() + ".Name") : entityNick;
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Pet] " + ChatColor.GREEN + "Placed down " + ChatColor.BOLD + ChatColor.translateAlternateColorCodes('&', petName) + ChatColor.GREEN + ".");
						} else
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this pet.");
					} else
						player.sendMessage(ChatColor.YELLOW + "[Pet] " + ChatColor.RED + "You have not obtained the " + entityNick + " pet.");
				} else
					player.sendMessage(ChatColor.YELLOW + "[Pet] " + ChatColor.RED + "Unknown pet '" + args[0] + "'.");
				
			}
			
		}
		
		return true;
	}
	
	//gets a pet from either the actual pet string, or nick name
	private Pet getPet(String nameToTry, PlayerConfig playerConfig) {
		for(Pet pet : Pet.values()) {
			
			String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString();
			if(entityNick.equalsIgnoreCase(nameToTry)
					|| (playerConfig.getString("Pets." + pet.ordinal() + ".Name") != null
					&& Chat.stripColor(playerConfig.getString("Pets." + pet.ordinal() + ".Name")).equalsIgnoreCase(nameToTry)))
				return pet;
		}
		return null;
	}
	
	public static void Obtain(Player player, Pet pet) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		if(playerConfig.getString("Pets." + pet.ordinal() + ".Obtained") != null) return; //can only obtain once
		
		String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
		Chat.broadcastMessage(ChatColor.YELLOW + "[Achievement] " + ChatColor.AQUA + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " obtained the " + ChatColor.BOLD + entityNick + ChatColor.WHITE + " pet!");
		
		if(pet == Pet.Spooky_Spider || pet == Pet.Spooky_Nether_Spider) {
			playerConfig.set("Pets." + pet.ordinal() + ".Helmet", new ItemStack(Material.JACK_O_LANTERN, 1));
			playerConfig.set("Pets." + pet.ordinal() + ".OffHand", new ItemStack(Material.PUMPKIN, 1));
			playerConfig.set("Pets." + pet.ordinal() + ".MainHand", new ItemStack(Material.PUMPKIN, 1));
		}
		if(pet == Pet.Sheep || pet == Pet.Wolf || pet == Pet.Cat || pet == Pet.Parrot) {
			playerConfig.set("Pets." + pet.ordinal() + ".Wool", new ItemStack(Material.WHITE_WOOL, 1));
		}
		if(pet == Pet.Snow_Golem) {
			playerConfig.set("Pets." + pet.ordinal() + ".Wool", new ItemStack(Material.CARVED_PUMPKIN, 1));
		}
		playerConfig.set("Pets." + pet.ordinal() + ".Obtained", true);
		playerConfig.save();
		
		Spawn(player, pet);
		player.sendMessage("");
		player.sendMessage(ChatColor.YELLOW + "[Pet] " + ChatColor.GREEN + "You have obtained the " + ChatColor.BOLD + entityNick + ChatColor.GREEN + " pet!");
		player.sendMessage(ChatColor.YELLOW + "[Pet] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/pet" + ChatColor.WHITE + " to view your pets.");
		player.sendMessage("");
	}
	
	public static void DespawnPets(Player player, boolean removeReference) {
		try {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(playerConfig.getString("Pets.Active.Reference") != null) {
				//1 pet can consist of multiple entities:
				List<String> currentPetsUUIDS = playerConfig.getStringList("Pets.Active.Reference");
				if(currentPetsUUIDS != null) {
					for(String entityUUID : currentPetsUUIDS) {
						Bukkit.getScheduler().runTask(plugin, () -> {
							try {
								Entity entity = Bukkit.getEntity(UUID.fromString(entityUUID));
								if(playerConfig.getString("Pets.Active.IsParrot") != null) {
									player.setShoulderEntityLeft(null);
									player.setShoulderEntityRight(null);
								}
								allPetUUIDs.remove(entityUUID);
								if(entity != null) {
									entity.remove();
								}
							} catch(Exception e) {
								Chat.logError(e);
							}
						});
					}
				}
				if(removeReference) {
					playerConfig.set("Pets.Active.IsParrot", null);
					playerConfig.set("Pets.Active.Reference", null);
					playerConfig.save();
				}
			}
		} catch(Exception e) {
			Chat.logError(e);
		}
	}
	
	public static void Spawn(Player player, Pet pet) {
		if(Admin.spectating.contains(player.getName())
		|| Admin.vanished.contains(player.getName())) {
			return;
		}
		
		try {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
			//despawn current pets
			DespawnPets(player, true);
	
			List<String> currentPetsUUIDS = new ArrayList<String>();
			
			if(pet == Pet.Spooky_Spider || pet == Pet.Spooky_Nether_Spider) {
				
				Wolf wolf = (Wolf) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
				
				MobDisguise mobDisguise = new MobDisguise(DisguiseType.CAVE_SPIDER);
				DisguiseAPI.disguiseToAll(wolf, mobDisguise);
				
				Chicken zombie = (Chicken) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.CHICKEN);
				EntityType type = pet == Pet.Spooky_Nether_Spider ? EntityType.ZOMBIFIED_PIGLIN : EntityType.HUSK;
				MobDisguise mobDisguise2 = new MobDisguise(DisguiseType.getType(type));
				ZombieWatcher watcher = (ZombieWatcher) mobDisguise2.getWatcher();
				watcher.setBaby(true);
				DisguiseAPI.disguiseToAll(zombie, mobDisguise2);
				
				wolf.setPassenger(zombie);
				wolf.setOwner(player);
				
				zombie.setCustomName("Spooky Spider Pet");
				zombie.setCustomNameVisible(true);
				
				wolf.setInvulnerable(true);
				zombie.setInvulnerable(true);
				
				wolf.setRemoveWhenFarAway(false);
				zombie.setRemoveWhenFarAway(false);
				
				zombie.setCollidable(false);
				wolf.setCollidable(false);
				
				currentPetsUUIDS.add(wolf.getUniqueId().toString());
				currentPetsUUIDS.add(zombie.getUniqueId().toString());
				allPetUUIDs.add(wolf.getUniqueId().toString());
				allPetUUIDs.add(zombie.getUniqueId().toString());
			}
			
			if(pet == Pet.Zombie
					|| pet == Pet.Husk
					|| pet == Pet.Drowned
					|| pet == Pet.Zombie_Villager) {
				EntityType type = EntityType.valueOf(pet.toString().toUpperCase());
				Wolf wolf = (Wolf) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
				
				MobDisguise mobDisguise = new MobDisguise(DisguiseType.getType(type));
				mobDisguise.setReplaceSounds(true);
				ZombieWatcher watcher = (ZombieWatcher) mobDisguise.getWatcher();
				watcher.setBaby(true);
				DisguiseAPI.disguiseToAll(wolf, mobDisguise);
				
				wolf.setOwner(player);
				String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
				wolf.setCustomName("Pet " + entityNick);
				wolf.setCustomNameVisible(true);
				
				wolf.setInvulnerable(true);
				wolf.setRemoveWhenFarAway(false);
				wolf.setCollidable(false);
				wolf.setCanPickupItems(false);
				
				currentPetsUUIDS.add(wolf.getUniqueId().toString());
				allPetUUIDs.add(wolf.getUniqueId().toString());
			}
			
			if(pet == Pet.Snow_Golem) {
				EntityType type = EntityType.SNOWMAN;
				Wolf wolf = (Wolf) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
				
				MobDisguise mobDisguise = new MobDisguise(DisguiseType.SNOWMAN);
				mobDisguise.setReplaceSounds(true);
				SnowmanWatcher snowmanWatcher = (SnowmanWatcher) mobDisguise.getWatcher();
				snowmanWatcher.setDerp(playerConfig.getString("Pets." + pet.ordinal() + ".Wool") == null || playerConfig.getItemStack("Pets." + pet.ordinal() + ".Wool").getType() != Material.CARVED_PUMPKIN);
				DisguiseAPI.disguiseToAll(wolf, mobDisguise);
				
				wolf.setOwner(player);
				String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
				wolf.setCustomName("Pet " + entityNick);
				wolf.setCustomNameVisible(true);
				
				wolf.setInvulnerable(true);
				wolf.setRemoveWhenFarAway(false);
				wolf.setCollidable(false);
				wolf.setCanPickupItems(false);
				
				currentPetsUUIDS.add(wolf.getUniqueId().toString());
				allPetUUIDs.add(wolf.getUniqueId().toString());
			}
			
			if(pet == Pet.Cat) {
				//spawn as itself
				Cat entity = (Cat) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.CAT);
				
				entity.setBaby();
				entity.setOwner(player);
				
				String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
				entity.setCustomName("Pet " + entityNick);
				entity.setCustomNameVisible(true);
				
				entity.setInvulnerable(true);
				entity.setRemoveWhenFarAway(false);
				entity.setCollidable(false);
				entity.setBreed(false);
				entity.setAgeLock(true);
				entity.setCanPickupItems(false);
				
				currentPetsUUIDS.add(entity.getUniqueId().toString());
				allPetUUIDs.add(entity.getUniqueId().toString());
			}
			if(pet == Pet.Wolf) {
				//spawn as itself
				Wolf entity = (Wolf) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
				
				entity.setBaby();
				entity.setOwner(player);
				
				String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
				entity.setCustomName("Pet " + entityNick);
				entity.setCustomNameVisible(true);
				
				entity.setInvulnerable(true);
				entity.setRemoveWhenFarAway(false);
				entity.setCollidable(false);
				entity.setBreed(false);
				entity.setAgeLock(true);
				entity.setCanPickupItems(false);
				
				currentPetsUUIDS.add(entity.getUniqueId().toString());
				allPetUUIDs.add(entity.getUniqueId().toString());
			}
			if(pet == Pet.Parrot) {
				//spawn as itself
				EntityType type = EntityType.valueOf(pet.toString().toUpperCase());
				Parrot entity = (Parrot) player.getLocation().getWorld().spawnEntity(player.getLocation(), type);
				
				entity.setOwner(player);
				
				String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
				entity.setCustomName("Pet " + entityNick);
				entity.setCustomNameVisible(true);
				
				entity.setInvulnerable(true);
				entity.setRemoveWhenFarAway(false);
				entity.setCollidable(false);
				entity.setBreed(false);
				entity.setCanPickupItems(false);
				
				playerConfig.set("Pets.Active.IsParrot", true);
				
				currentPetsUUIDS.add(entity.getUniqueId().toString());
				allPetUUIDs.add(entity.getUniqueId().toString());
			}
			
			if(pet == Pet.Chicken
					|| pet == Pet.Pig
					|| pet == Pet.Cow
					|| pet == Pet.Sheep) {
				EntityType type = EntityType.valueOf(pet.toString().toUpperCase());
				Wolf wolf = (Wolf) player.getLocation().getWorld().spawnEntity(player.getLocation(), EntityType.WOLF);
				
				MobDisguise mobDisguise = new MobDisguise(DisguiseType.getType(type), false);
				mobDisguise.setReplaceSounds(true);
				DisguiseAPI.disguiseToAll(wolf, mobDisguise);
				
				wolf.setOwner(player);
				
				String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
				wolf.setCustomName("Pet " + entityNick);
				wolf.setCustomNameVisible(true);
				
				wolf.setInvulnerable(true);
				wolf.setRemoveWhenFarAway(false);
				wolf.setCollidable(false);
				wolf.setBreed(false);
				wolf.setCanPickupItems(false);
				
				currentPetsUUIDS.add(wolf.getUniqueId().toString());
				allPetUUIDs.add(wolf.getUniqueId().toString());
			}
			
			//set as active
			playerConfig.set("Pets.Active.Reference", currentPetsUUIDS);
			playerConfig.set("Pets.Active.Ordinal", pet.ordinal());
			playerConfig.save();
			
			if(currentPetsUUIDS != null) {
				for(String entityUUID : currentPetsUUIDS) {
					Entity entity = Bukkit.getEntity(UUID.fromString(entityUUID));
					if(entity == null) {
						Chat.logError("Tried modifying a pet that doesn't exist. **" + player.getName() + "**, **" + pet.toString() + "**");
						continue;
					}
					if(playerConfig.getString("Pets." + pet.ordinal() + ".Name") != null) {
						//set names:
						if(playerConfig.getInt("DonateRank") >= Rank.Baron.ordinal()) {
							entity.setCustomName(ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Pets." + pet.ordinal() + ".Name")));
						} else
							entity.setCustomName(playerConfig.getString("Pets." + pet.ordinal() + ".Name"));
					}
					//set equipment:
					if(entity instanceof LivingEntity) {
						EntityEquipment equipment = ((LivingEntity) entity).getEquipment();
						if(playerConfig.getString("Pets." + pet.ordinal() + ".Helmet") != null)
							equipment.setHelmet(playerConfig.getItemStack("Pets." + pet.ordinal() + ".Helmet").clone());
						else equipment.setHelmet(new ItemStack(Material.AIR, 1));
						if(playerConfig.getString("Pets." + pet.ordinal() + ".OffHand") != null)
							equipment.setItemInOffHand(playerConfig.getItemStack("Pets." + pet.ordinal() + ".OffHand").clone());
						else equipment.setItemInOffHand(new ItemStack(Material.AIR, 1));
						if(playerConfig.getString("Pets." + pet.ordinal() + ".MainHand") != null)
							equipment.setItemInMainHand(playerConfig.getItemStack("Pets." + pet.ordinal() + ".MainHand").clone());
						else equipment.setItemInMainHand(new ItemStack(Material.AIR, 1));
						if(playerConfig.getString("Pets." + pet.ordinal() + ".Chestplate") != null)
							equipment.setChestplate(playerConfig.getItemStack("Pets." + pet.ordinal() + ".Chestplate").clone());
						else equipment.setChestplate(new ItemStack(Material.AIR, 1));
						if(playerConfig.getString("Pets." + pet.ordinal() + ".Pants") != null)
							equipment.setLeggings(playerConfig.getItemStack("Pets." + pet.ordinal() + ".Pants").clone());
						else equipment.setLeggings(new ItemStack(Material.AIR, 1));
						if(playerConfig.getString("Pets." + pet.ordinal() + ".Boots") != null)
							equipment.setBoots(playerConfig.getItemStack("Pets." + pet.ordinal() + ".Boots").clone());
						else equipment.setBoots(new ItemStack(Material.AIR, 1));
						
						//sheep wool:
						if(DisguiseAPI.isDisguised(entity)) {
							Disguise disguise = DisguiseAPI.getDisguise(entity);
							if(disguise.getType() == DisguiseType.SHEEP) {
								if(playerConfig.getString("Pets." + pet.ordinal() + ".Wool") != null) {
									SheepWatcher watcher = (SheepWatcher) disguise.getWatcher();
									watcher.setColor(DyeColor.getByWoolData(playerConfig.getItemStack("Pets." + pet.ordinal() + ".Wool").clone().getData().getData()));
									watcher.setSheared(false);
								} else {
									SheepWatcher watcher = (SheepWatcher) disguise.getWatcher();
									watcher.setColor(DyeColor.WHITE);
									watcher.setSheared(true);
								}
							}
						}
						
						//wolf collar:
						if(!DisguiseAPI.isDisguised(entity) && entity.getType() == EntityType.WOLF) {
							if(playerConfig.getString("Pets." + pet.ordinal() + ".Wool") != null) {
								Wolf wolf = (Wolf) entity;
								wolf.setCollarColor(DyeColor.getByWoolData(playerConfig.getItemStack("Pets." + pet.ordinal() + ".Wool").clone().getData().getData()));
							} else {
								Wolf wolf = (Wolf) entity;
								wolf.setCollarColor(DyeColor.GRAY);
							}
						}
						
						//cat collar and fur:
						if(!DisguiseAPI.isDisguised(entity) && entity.getType() == EntityType.CAT) {
							Cat cat = (Cat) entity;
							if(playerConfig.getString("Pets." + pet.ordinal() + ".Wool") != null) {
								cat.setCollarColor(DyeColor.getByWoolData(playerConfig.getItemStack("Pets." + pet.ordinal() + ".Wool").clone().getData().getData()));
							} else {
								cat.setCollarColor(DyeColor.WHITE);
							}
							
							if(playerConfig.getString("Pets." + pet.ordinal() + ".Variant") != null) {
								cat.setCatType(Type.valueOf(playerConfig.getString("Pets." + pet.ordinal() + ".Variant")));
							} else {
								cat.setCatType(Type.WHITE);
							}
						}
						
						//parrot colour:
						if(!DisguiseAPI.isDisguised(entity) && entity.getType() == EntityType.PARROT) {
							try {
								Parrot parrot = (Parrot) entity;
								DyeColor dye = DyeColor.getByWoolData(playerConfig.getItemStack("Pets." + pet.ordinal() + ".Wool").clone().getData().getData());
								parrot.setVariant(Variant.valueOf(dye.toString()));
							} catch(Exception e) {
								Parrot parrot = (Parrot) entity;
								parrot.setVariant(Variant.GRAY);
							}
						}
						
					}
				}
			}
			
		} catch(Exception e) {
			Chat.logError(e);
		}
	}
	
	//spawn halloween treats from pet:
	public static List<Material> halloweenTreats = new ArrayList<Material>();
	
	@EventHandler
	public void spookySpiderDropTreats(ItemSpawnEvent event) {
		Item test = event.getEntity();
		ItemStack is = test.getItemStack();
		
		if(is.getType() == Material.EGG) {
			
			List<Entity> nearbyEntities = test.getNearbyEntities(0.01D, 0.3D, 0.01D);
			
			for(Entity entity : nearbyEntities) {
				if(entity instanceof Chicken chicken) {
					if(DisguiseAPI.isDisguised(chicken)) { //is a pet
						event.setCancelled(true);
						int index = (int) (Math.random() * halloweenTreats.size()); // 0-size (inclusive)
						ItemStack drop = (ItemStack) new ItemStack(halloweenTreats.get(index), 1).clone();
						event.getEntity().getWorld().dropItem(event.getLocation(), drop);
					}
				}
			}
			
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		//despawn current pets
		DespawnPets(player, true);
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
			@Override
			public void run() {
				Player player = event.getPlayer();
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				if(playerConfig.getString("Pets.Active.Ordinal") != null)
					Spawn(player, Pet.values()[playerConfig.getInt("Pets.Active.Ordinal")]);
			}
		}, 20L);
	}
	
	//on travel, respawn the pet
	public static void teleportPet(Player player, Pet pet) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			if(!Admin.spectating.contains(player.getName()))
				Spawn(player, pet);
		}, 40L);
	}
	
	final private int woolSlot = 4;
	final private int helmetSlot = 4;
	final private int offHandSlot = 12;
	final private int chestplateSlot = 13;
	final private int mainHandSlot = 14;
	final private int pantsSlot = 22;
	final private int bootsSlot = 31;
	
	//dress pets:
	@EventHandler
	public void openPetEquipmentMenu(PlayerInteractEntityEvent event) {
		try {
			Player player = event.getPlayer();
			Entity entity = event.getRightClicked();
			if(entity.isInvulnerable()) {
				if(!allPetUUIDs.contains(entity.getUniqueId().toString())
						&& (entity.getType() == EntityType.WOLF || entity.getType() == EntityType.CAT || entity.getType() == EntityType.PARROT)) {
					player.sendMessage(ChatColor.RED + "[DeadMC] Woops! That pet was bugged. You can right click bugged pets to remove them. " + ChatColor.WHITE + "(We have fixed the duplicating issue now, so please post on Discord in #bug-reports if you notice anything strange happening!)");
					entity.remove();
					return;
				}

				if(entity.isInvulnerable() && entity instanceof Sittable sittable && DisguiseAPI.isDisguised(entity)) {
					sittable.setSitting(false);
					event.setCancelled(true);
				}
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				if(playerConfig.getInt("DonateRank") >= Rank.Squire.ordinal()) {
					List<String> currentPetsUUIDS = playerConfig.getStringList("Pets.Active.Reference");
					if(playerConfig.getString("Pets.Active.Reference") != null && currentPetsUUIDS.contains(entity.getUniqueId().toString())) {
						//is players pet
						int ordinal = playerConfig.getInt("Pets.Active.Ordinal");
						
						Pet pet = Pet.values()[ordinal];
						String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
						String petName = playerConfig.getString("Pets." + pet.ordinal() + ".Name") != null ? playerConfig.getString("Pets." + pet.ordinal() + ".Name") : "Pet " + entityNick;
						
						//not customisable pets:
						if(pet == Pet.Chicken
								|| pet == Pet.Cow
								|| pet == Pet.Pig) {
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Pets] " + ChatColor.RED + petName + " isn't customisable :(");
							return;
						}
						
						//wool:
						if(pet == Pet.Sheep
								|| pet == Pet.Wolf
								|| pet == Pet.Cat
								|| pet == Pet.Parrot
								|| pet == Pet.Snow_Golem) {
							String adjustable = "Wool";
							if(pet == Pet.Wolf || pet == Pet.Cat) adjustable = "Collar";
							if(pet == Pet.Parrot) adjustable = "Dye";
							if(pet == Pet.Snow_Golem) adjustable = "Pumpkin";
							
							Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.BOLD + ChatColor.translateAlternateColorCodes('&', petName) + ChatColor.RESET + ChatColor.BOLD + "'s " + adjustable);
							for(int count = 0; count < 9; count++) {
								//add items to inven
								if(count != woolSlot) {
									//add blocked slot
									ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE, 1);
									ItemMeta meta = item.getItemMeta();
									item.setItemMeta(meta);
									inventory.setItem(count, item);
								}
							}
							if(playerConfig.getString("Pets." + ordinal + ".Wool") != null) {
								inventory.setItem(woolSlot, playerConfig.getItemStack("Pets." + ordinal + ".Wool").clone());
							}
							if(entity instanceof Sittable sittable) {
								sittable.setSitting(true);
							}
							player.openInventory(inventory);
							return;
						}
						
						//any pets with armour:
						Inventory inventory = Bukkit.createInventory(null, 36, ChatColor.BOLD + ChatColor.translateAlternateColorCodes('&', petName) + ChatColor.RESET + ChatColor.BOLD + "'s Equipment");
						List<Integer> slotsWithArmour = new ArrayList<Integer>();
						slotsWithArmour.add(helmetSlot); //helmet
						slotsWithArmour.add(offHandSlot); //off hand
						slotsWithArmour.add(chestplateSlot); //chestplate
						slotsWithArmour.add(mainHandSlot); //main hand
						slotsWithArmour.add(pantsSlot); //pants
						slotsWithArmour.add(bootsSlot); //boots
						for(int count = 0; count < 36; count++) {
							//add items to inven
							if(!slotsWithArmour.contains(count)) {
								//add blocked slot
								ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE, 1);
								ItemMeta meta = item.getItemMeta();
								item.setItemMeta(meta);
								inventory.setItem(count, item);
							}
						}
						if(playerConfig.getString("Pets." + ordinal + ".Helmet") != null) {
							inventory.setItem(helmetSlot, playerConfig.getItemStack("Pets." + ordinal + ".Helmet").clone());
						}
						if(playerConfig.getString("Pets." + ordinal + ".OffHand") != null) {
							inventory.setItem(offHandSlot, playerConfig.getItemStack("Pets." + ordinal + ".OffHand").clone());
						}
						if(playerConfig.getString("Pets." + ordinal + ".Chestplate") != null) {
							inventory.setItem(chestplateSlot, playerConfig.getItemStack("Pets." + ordinal + ".Chestplate").clone());
						}
						if(playerConfig.getString("Pets." + ordinal + ".MainHand") != null) {
							inventory.setItem(mainHandSlot, playerConfig.getItemStack("Pets." + ordinal + ".MainHand").clone());
						}
						if(playerConfig.getString("Pets." + ordinal + ".Pants") != null) {
							inventory.setItem(pantsSlot, playerConfig.getItemStack("Pets." + ordinal + ".Pants").clone());
						}
						if(playerConfig.getString("Pets." + ordinal + ".Boots") != null) {
							inventory.setItem(bootsSlot, playerConfig.getItemStack("Pets." + ordinal + ".Boots").clone());
						}
						player.openInventory(inventory);
						if(entity instanceof Sittable sittable) {
							sittable.setSitting(true);
						}
					} else
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Pets] " + ChatColor.RED + "This is not your pet.");
				} else
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Pets] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks " + ChatColor.RESET + "" + ChatColor.WHITE + "to customise your pet.");
			}
		} catch(Exception e) {
			Chat.logError(e, event.getPlayer());
		}
	}
	
	@EventHandler
	public void onPetInventoryClick(InventoryClickEvent event) {
		if(event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			try {
				Inventory inventory = event.getInventory();
				
				List<Integer> slotsWithArmour = new ArrayList<Integer>();
				slotsWithArmour.add(helmetSlot); //helmet
				slotsWithArmour.add(offHandSlot); //off hand
				slotsWithArmour.add(chestplateSlot); //chestplate
				slotsWithArmour.add(mainHandSlot); //main hand
				slotsWithArmour.add(pantsSlot); //pants
				slotsWithArmour.add(bootsSlot); //boots
				
				if((event.getView().getTitle().contains("'s Wool") || event.getView().getTitle().contains("'s Collar") || event.getView().getTitle().contains("'s Dye") || event.getView().getTitle().contains("'s Pumpkin"))
						&& event.getClickedInventory() == event.getView().getTopInventory() && inventory.getSize() == 9) {
					if(event.getRawSlot() != 4) { //is not an armour slot
						event.setCancelled(true);
					}
				}
				
				if(event.getView().getTitle().contains("'s Equipment") && event.getClickedInventory() == event.getView().getTopInventory() && inventory.getSize() == 36) {
					//is an armour menu
					if(!slotsWithArmour.contains(event.getRawSlot())) { //is not an armour slot
						event.setCancelled(true);
					}
				}
			} catch(Exception e) {
				Chat.logError(e, player);
			}
		}
	}
	
	// Check for clicks on slots
	@EventHandler
	public void closePetEquipmentMenu(InventoryCloseEvent event) {
		if(event.getPlayer() instanceof Player) {
			Player player = (Player) event.getPlayer();
			try {
				Inventory inventory = event.getInventory();
				
				if((event.getView().getTitle().contains("'s Wool") || event.getView().getTitle().contains("'s Collar") || event.getView().getTitle().contains("'s Dye") || event.getView().getTitle().contains("'s Pumpkin"))
						&& inventory == event.getView().getTopInventory() && inventory.getSize() == 9) {
					//set sheeps wool:
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					List<String> currentPetsUUIDS = playerConfig.getStringList("Pets.Active.Reference");
					for(String petUUID : currentPetsUUIDS) {
						Entity pet = Bukkit.getEntity(UUID.fromString(petUUID));
						if(pet instanceof LivingEntity) {
							
							//sheep wool:
							if(DisguiseAPI.isDisguised(pet)) {
								Disguise disguise = DisguiseAPI.getDisguise(pet);
								if(disguise != null && disguise.getType() == DisguiseType.SHEEP) {
									SheepWatcher watcher = (SheepWatcher) disguise.getWatcher();
									if(inventory.getItem(woolSlot) != null) {
										watcher.setColor(DyeColor.getByWoolData(inventory.getItem(woolSlot).getData().getData()));
										watcher.setSheared(false);
									} else {
										watcher.setColor(DyeColor.WHITE);
										watcher.setSheared(true);
									}
								}
							}
							
							//wolf collar:
							if(!DisguiseAPI.isDisguised(pet) && pet.getType() == EntityType.WOLF) {
								if(inventory.getItem(woolSlot) != null) {
									Wolf wolf = (Wolf) pet;
									wolf.setCollarColor(DyeColor.getByWoolData(inventory.getItem(woolSlot).getData().getData()));
								} else {
									Wolf wolf = (Wolf) pet;
									wolf.setCollarColor(DyeColor.GRAY);
								}
							}
							
							//cat collar:
							if(!DisguiseAPI.isDisguised(pet) && pet.getType() == EntityType.CAT) {
								if(inventory.getItem(woolSlot) != null) {
									Cat cat = (Cat) pet;
									cat.setCollarColor(DyeColor.getByWoolData(inventory.getItem(woolSlot).getData().getData()));
								} else {
									Cat cat = (Cat) pet;
									cat.setCollarColor(DyeColor.GRAY);
								}
							}
							
							//snowman pumpkin
							if(DisguiseAPI.isDisguised(pet)) {
								Disguise disguise = DisguiseAPI.getDisguise(pet);
								if(disguise != null && disguise.getType() == DisguiseType.SNOWMAN) {
									SnowmanWatcher watcher = (SnowmanWatcher) disguise.getWatcher();
									watcher.setDerp(inventory.getItem(woolSlot) == null || inventory.getItem(woolSlot).getType() != Material.CARVED_PUMPKIN);
								}
							}
							
							//parrot colour:
							if(!DisguiseAPI.isDisguised(pet) && pet.getType() == EntityType.PARROT) {
								if(inventory.getItem(woolSlot) != null) {
									Parrot parrot = (Parrot) pet;
									DyeColor dye = DyeColor.getByWoolData(inventory.getItem(woolSlot).getData().getData());
									try {
										parrot.setVariant(Variant.valueOf(dye.toString()));
									} catch(IllegalArgumentException e) {
										//colour doesn't exist
										Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Pets] " + ChatColor.RED + dye.toString() + " dye is not supported. Try another colour.");
										player.getWorld().dropItem(pet.getLocation(), inventory.getItem(woolSlot));
										return;
									}
								} else {
									Parrot parrot = (Parrot) pet;
									parrot.setVariant(Variant.GRAY);
								}
							}
							
							if(pet instanceof Sittable sittable) {
								sittable.setSitting(false);
							}
						}
					}
					int ordinal = playerConfig.getInt("Pets.Active.Ordinal");
					playerConfig.set("Pets." + ordinal + ".Wool", inventory.getItem(woolSlot));
					playerConfig.save();
				}
				
				if(event.getView().getTitle().contains("'s Equipment") && inventory == event.getView().getTopInventory() && inventory.getSize() == 36) {
					//is an armour menu
					
					//set pets equipment:
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					List<String> currentPetsUUIDS = playerConfig.getStringList("Pets.Active.Reference");
					for(String petUUID : currentPetsUUIDS) {
						Entity pet = Bukkit.getEntity(UUID.fromString(petUUID));
						if(pet instanceof LivingEntity) {
							EntityEquipment equipment = ((LivingEntity) pet).getEquipment();
							if(inventory.getItem(helmetSlot) != null)
								equipment.setHelmet(inventory.getItem(helmetSlot).clone());
							else
								equipment.setHelmet(new ItemStack(Material.AIR, 1));
							if(inventory.getItem(offHandSlot) != null)
								equipment.setItemInOffHand(inventory.getItem(offHandSlot).clone());
							else
								equipment.setItemInOffHand(new ItemStack(Material.AIR, 1));
							if(inventory.getItem(chestplateSlot) != null)
								equipment.setChestplate(inventory.getItem(chestplateSlot).clone());
							else
								equipment.setChestplate(new ItemStack(Material.AIR, 1));
							if(inventory.getItem(mainHandSlot) != null)
								equipment.setItemInMainHand(inventory.getItem(mainHandSlot).clone());
							else
								equipment.setItemInMainHand(new ItemStack(Material.AIR, 1));
							if(inventory.getItem(pantsSlot) != null)
								equipment.setLeggings(inventory.getItem(pantsSlot).clone());
							else
								equipment.setLeggings(new ItemStack(Material.AIR, 1));
							if(inventory.getItem(bootsSlot) != null)
								equipment.setBoots(inventory.getItem(bootsSlot).clone());
							else equipment.setBoots(new ItemStack(Material.AIR, 1));
							
							if(pet instanceof Sittable sittable) {
								sittable.setSitting(false);
							}
						}
					}
					int ordinal = playerConfig.getInt("Pets.Active.Ordinal");
					playerConfig.set("Pets." + ordinal + ".Helmet", inventory.getItem(helmetSlot));
					playerConfig.set("Pets." + ordinal + ".OffHand", inventory.getItem(offHandSlot));
					playerConfig.set("Pets." + ordinal + ".Chestplate", inventory.getItem(chestplateSlot));
					playerConfig.set("Pets." + ordinal + ".MainHand", inventory.getItem(mainHandSlot));
					playerConfig.set("Pets." + ordinal + ".Pants", inventory.getItem(pantsSlot));
					playerConfig.set("Pets." + ordinal + ".Boots", inventory.getItem(bootsSlot));
					playerConfig.save();
				}
				
			} catch(Exception e) {
				Chat.logError(e, player);
			}
		}
	}
	
	public static List<Pet> getObtainedPets(PlayerConfig playerConfig) {
		List<Pet> pets = new ArrayList<Pet>();
		for(Pet pet : Pet.values()) {
			if(playerConfig.getString("Pets." + pet.ordinal() + ".Obtained") != null) {
				pets.add(pet);
			}
		}
		return pets;
	}
}
