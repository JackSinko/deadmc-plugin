package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Chat.Leaderboard;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.entities.MessageEmbed;
import github.scarsz.discordsrv.util.DiscordUtil;
import github.scarsz.discordsrv.util.WebhookUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.UUID;

public class BloodMoon implements Listener {
	private static DeadMC plugin;
	public BloodMoon(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static int lengthOfDay = 24000;
	public static int nightStartTime = 13000;
	private static int daysBetweenBloodMoon = 6;
	
	//checks in placeholder every 1000ms
	public static Boolean isBloodMoon = false;
	public static Boolean isBloodMoon() {
		isBloodMoon = timeUntilBloodMoon() == 0;
		return isBloodMoon;
	}
	
	public static int bloodMoonProgress() {
		if(isBloodMoon) {
			int time = (int) Bukkit.getWorld("world").getTime();
			int progressLeft = (int) (((double)(23000-time)/10000.0) * 100);
			return progressLeft;
		}
		
		int timeBetweenEvent = lengthOfDay * daysBetweenBloodMoon;
		int progressLeft = (int) (timeUntilBloodMoon()/(double)(timeBetweenEvent-10000.0) * 100);

		return progressLeft;
	}
	public static String timeLeftInNight() {
		int time = (int) Bukkit.getWorld("world").getTime();
		if(time > 12000 && time < 22000) {
			int hours = (24000-time)/1000;
			return new String("" + ChatColor.BOLD + hours + ChatColor.WHITE + " hours");
		} else if (time >= 22000) {
			int minutes = (int) Math.ceil((23000-time)/16.667f);
			return new String("" + ChatColor.BOLD + minutes + ChatColor.WHITE + " minutes");
		}
		return "Not Night"; //not night
	}

	public static BloodMoonNotification lastNotification = null;
	public enum BloodMoonNotification {
		TWELVE_HOUR_WARNING,
		ONE_HOUR_WARNING,
		BLOODMOON_START,
		BLOODMOON_END
	}
	public static int timeUntilBloodMoon() {
		int timeBetweenEvent = lengthOfDay * daysBetweenBloodMoon;
		
		int fullTime = (int) Bukkit.getWorld("world").getFullTime();
		
		int firstNight = timeBetweenEvent + nightStartTime;
		int numberOfBloodMoonsPassed = 0;
		int nextBloodMoonStartTime = firstNight;
		if(fullTime > firstNight) {
			numberOfBloodMoonsPassed = (fullTime-nightStartTime) / timeBetweenEvent;
			nextBloodMoonStartTime = ((numberOfBloodMoonsPassed+1) * timeBetweenEvent) + nightStartTime;
		}
			
		int timeInRelationToEvent = (int)(fullTime % timeBetweenEvent);
		if(timeInRelationToEvent >= nightStartTime && timeInRelationToEvent < (lengthOfDay-1000/*don't include sunrise*/) && fullTime > 24000) //don't do on first day
			return 0;
		
		int timeUntilBloodMoon = nextBloodMoonStartTime-fullTime;
		
		if(fullTime > 25000) {
			if(timeUntilBloodMoon < 12050 && timeUntilBloodMoon > 12000
					&& (lastNotification == null || lastNotification != BloodMoonNotification.TWELVE_HOUR_WARNING)) {
				lastNotification = BloodMoonNotification.TWELVE_HOUR_WARNING;
				message(12000, "The " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " is tonight. Prepare yourselves!", ChatColor.WHITE + "The " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " is 12 hours away.");
			}
			if(timeUntilBloodMoon < 1050 && timeUntilBloodMoon > 1000
					&& (lastNotification == null || lastNotification != BloodMoonNotification.ONE_HOUR_WARNING)) {
				lastNotification = BloodMoonNotification.ONE_HOUR_WARNING;
				message(1000, "As the sun falls, as so the " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " rises...", ChatColor.WHITE + "The " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " is almost here!");
			}
			if(timeUntilBloodMoon < 30 && timeUntilBloodMoon > 0
					&& (lastNotification == null || lastNotification != BloodMoonNotification.BLOODMOON_START)) {
				lastNotification = BloodMoonNotification.BLOODMOON_START;
				
				//reset leaderboard and stats
				DeadMC.LeaderboardFile.data().set("CURRENTBLOODMOON", null);
				DeadMC.LeaderboardFile.data().set("CurrentTotalKills", 0);
				Bukkit.getScheduler().runTask(plugin, () -> DeadMC.LeaderboardFile.save());
				message(50, "The " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " is here! Save our souls!", ChatColor.WHITE + "The " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " is here!");
				for(Player player : Bukkit.getServer().getOnlinePlayers()) {
					Bukkit.getScheduler().runTask(plugin, () -> {
						player.playSound(player.getLocation(), Sound.AMBIENT_CAVE, 1, 1f);
						player.playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_AMBIENT, 1, 1f);
						Chat.sendBigTitle(player.getUniqueId(), "&fThe &4&lBlood Moon &fis here.", "&fSave our souls!");
					});
				}
				//reset leaderboard
				DeadMC.LeaderboardFile.data().set("CURRENTBLOODMOON", null);
				DeadMC.LeaderboardFile.save();
				//reset placeholder data
				PlaceHolders.player_BMKills.clear();
			}
			
			if(timeUntilBloodMoon < (timeBetweenEvent-10000) && timeUntilBloodMoon > (timeBetweenEvent-10050)
					&& (lastNotification == null || lastNotification != BloodMoonNotification.BLOODMOON_END)) {
				lastNotification = BloodMoonNotification.BLOODMOON_END;
				onBloodMoonEnd();
			}
			
		}
		
		return timeUntilBloodMoon;
	}
	
	private static void onBloodMoonEnd() {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			int timeBetweenEvent = lengthOfDay * daysBetweenBloodMoon;
			
			Chat.broadcastMessage("", false, false, false);
			Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "The " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " is over!", false, false, false);
			
			for(Player player : Bukkit.getServer().getOnlinePlayers()) {
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.WHITE + "The " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " has fallen...");
			}
			
			//anounce leader:
			String leaderName = "Nobody";
			UUID leaderUUID = null;
			if(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 1 + ".UUID") != null) {
				leaderUUID = UUID.fromString(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 1 + ".UUID"));
				OfflinePlayer leader = Bukkit.getOfflinePlayer(leaderUUID);
				leaderName = leader.getName();
				PlayerConfig leaderConfig = PlayerConfig.getConfig(leader);
				if(leaderConfig.getString("Name") != null) leaderName = leaderConfig.getString("Name");
			}
			leaderName = ChatColor.translateAlternateColorCodes('&', leaderName);
			for(Player p : Bukkit.getServer().getOnlinePlayers()) {
				Bukkit.getScheduler().runTask(plugin, () -> p.playSound(p.getLocation(), Sound.AMBIENT_CAVE, 1, 1f));
				if(leaderUUID == null) {
					Chat.sendBigTitle(p.getUniqueId(), "&fThe &4&lBlood Moon &fis over!", "&fThere was no leader.");
				} else if(p.getUniqueId().toString().equalsIgnoreCase(leaderUUID.toString())) {
					Chat.sendBigTitle(p.getUniqueId(), "&fThe &4&lBlood Moon &fis over!", "&b&lYou &fwere the leader!");
				} else {
					Chat.sendBigTitle(p.getUniqueId(), "&fThe &4&lBlood Moon &fis over!", "&b&l" + leaderName + " &fwas the leader.");
				}
			}
			
			//calculate rewards:
			int totalKills = DeadMC.LeaderboardFile.data().getInt("CurrentTotalKills");
			int top3kills = DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 1 + ".Amount") + DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 2 + ".Amount") + DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 3 + ".Amount");
			double multiplier = 1 + (totalKills > 0 ? (totalKills - top3kills) / totalKills : 0);
			Chat.debug("Multiplier: " + top3kills + " / " + totalKills + " = " + multiplier);
			int endLoot = (int) (totalKills * multiplier);
			if(endLoot < 0) endLoot = 0;
			
			//top 3:
			for(int count = 1; count <= 3; count++) {
				if(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + (int)count + ".UUID") == null) {
					break;
				}
				UUID uuid = UUID.fromString(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + (int)count + ".UUID"));
				Chat.debug(count + ": " + uuid.toString());
				int reward = 0;
				if(count == 1) reward = endLoot;
				else if(count == 2) reward = endLoot / 2;
				else if(count == 3) reward = endLoot / 4;
				
				Economy.giveCoins(uuid, reward);
				
				String name = Bukkit.getOfflinePlayer(uuid).getName();
				PlayerConfig leaderConfig = PlayerConfig.getConfig(uuid);
				if(leaderConfig.getString("Name") != null) name = ChatColor.translateAlternateColorCodes('&', leaderConfig.getString("Name"));
				
				Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.GOLD + ChatColor.BOLD + count + "." + ChatColor.WHITE + " " + ChatColor.BOLD + name + ChatColor.WHITE + " (" + ChatColor.BOLD + ChatColor.AQUA + DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + count + ".Amount") + " kills" + ChatColor.WHITE + " - " + ChatColor.GOLD + Economy.convertCoins(reward) + ChatColor.WHITE + ")", false, false, false);
			}
			
			//other participants:
			for(int count = 4; count <= 10; count++) {
				if(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + count + ".UUID") == null) {
					break;
				}
				UUID uuid = UUID.fromString(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + count + ".UUID"));
				int reward = (int) (DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + count + ".Amount") * multiplier);
				if(reward == 0) continue;
				
				Economy.giveCoins(uuid, reward);
				Player player = Bukkit.getPlayer(uuid);
				if(player != null) {
					player.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.WHITE + "You earned an extra " + Economy.convertCoins(reward) + " for your " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " efforts.");
				}
			}
			
			//add leader stat
			if(leaderUUID != null) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(leaderUUID);
				int leaderTimes = 1;
				if(playerConfig.getString("BloodMoonLeader") != null)
					leaderTimes += playerConfig.getInt("BloodMoonLeader");
				playerConfig.set("BloodMoonLeader", leaderTimes);
				playerConfig.save();
				Chat.updateLeaderBoard(Leaderboard.BMLEADER, leaderUUID);
				
				String times = "" + leaderTimes;
				String lastCharacter = times.substring(times.length() - 1, times.length()); //23 = 3
				String timeString = "th";
				if(lastCharacter.equalsIgnoreCase("1")) timeString = "st";
				if(lastCharacter.equalsIgnoreCase("2")) timeString = "nd";
				if(lastCharacter.equalsIgnoreCase("3")) timeString = "rd";
				
				Chat.broadcastDiscord(":bloodmoon: **The BloodMoon is over!** " + ChatColor.stripColor(leaderName) + " was the leader for the " + leaderTimes + timeString + " time."); //send the message to discord
				
				Player onlinePlayer = Bukkit.getPlayer(leaderUUID);
				if(onlinePlayer != null) {
					onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1f, 8f);
					onlinePlayer.sendMessage(ChatColor.RED + "[DeadMC] " + "You were the " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.RED + " leader!");
					Chat.sendTitleMessage(leaderUUID, ChatColor.WHITE + "You were the " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " leader!");
				}
				Chat.broadcastMessage("", false, false, false);
				Chat.broadcastMessage(ChatColor.YELLOW + "[Achievement] " + ChatColor.AQUA + ChatColor.BOLD + leaderName + ChatColor.WHITE + " became the " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " leader for the " + ChatColor.GOLD + ChatColor.BOLD + leaderTimes + ChatColor.GOLD + timeString + ChatColor.WHITE + " time!", false, false, false);
			}
		});
	}
	
	static int lastMessage = 0;
	private static void message(int fullTimeSent, String string, String title) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			if(lastMessage != fullTimeSent) {
				
				lastMessage = fullTimeSent;
				
				if(fullTimeSent == 12000)
					Chat.broadcastDiscord(":clock3: **The BloodMoon is 12 hours away.**"); //send the message to discord
				if(fullTimeSent == 1000)
					Chat.broadcastDiscord(":waxing_crescent_moon: **The BloodMoon is almost here!**"); //send the message to discord
				if(fullTimeSent == 50)
					Chat.broadcastDiscord(":bloodmoon: **The BloodMoon is here!**"); //send the message to discord
				
				Chat.broadcastMessage("", false, false, false);
				Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + string, false, false, false);
				for(Player player : Bukkit.getServer().getOnlinePlayers()) {
					Chat.sendTitleMessage(player.getUniqueId(), title);
				}
				
				Chat.broadcastMessage("", false, false, false);
			}
		});
	}

	public static String timeUntilBloodMoonFormatted() {
		int timeUntilBloodMoon = timeUntilBloodMoon();
		if(timeUntilBloodMoon() == 0) return new String("");

		int days = (int) Math.floor(timeUntilBloodMoon/24000);
		int hours = (int) Math.ceil((timeUntilBloodMoon-(days*24000))/1000) + 1;
		if(timeUntilBloodMoon > 24000) //more than a day
			return new String("" + ChatColor.BOLD + (days+1) + ChatColor.WHITE + " days");
		if(timeUntilBloodMoon > 1000)
			return new String("" + ChatColor.BOLD + hours + ChatColor.WHITE + " hours");
		int minutes = (int) Math.ceil((double)(timeUntilBloodMoon/1000.0)*60.0);
		return new String("" + ChatColor.BOLD + minutes + ChatColor.WHITE + " minutes");
	}

}
