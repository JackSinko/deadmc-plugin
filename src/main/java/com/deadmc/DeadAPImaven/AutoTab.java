package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Admin.Punishment;
import com.deadmc.DeadAPImaven.Admin.StaffRank;
import com.deadmc.DeadAPImaven.Chat.Leaderboard;
import com.deadmc.DeadAPImaven.CustomItems.CustomItem;
import com.deadmc.DeadAPImaven.Pets.Pet;
import com.deadmc.DeadAPImaven.TaskManager.Difficulty;
import com.deadmc.DeadAPImaven.TaskManager.TaskType;
import com.deadmc.DeadAPImaven.Towns.Town;
import com.deadmc.DeadAPImaven.Towns.Town.Rank;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Breedable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AutoTab implements TabCompleter {
	private DeadMC plugin;
	
	public AutoTab(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		
		Player player = null;
		if(sender instanceof Player)
			player = (Player) sender;
		
		if(command.getName().equalsIgnoreCase("giveitem") && player.isOp()) {
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				for(CustomItem item : CustomItem.values()) {
					if(item.toString().contains(args[0].toUpperCase()))
						names.add(item.toString());
				}
				return names;
			}
			if(args.length == 2) {
				List<String> names = new ArrayList<String>();
				names.add("donator? true or false");
				names.add("true");
				names.add("false");
				return names;
			}
			if(args.length == 3) {
				List<String> names = new ArrayList<String>();
				names.add("<damage>");
				return names;
			}
		}
		
		//task create new 0 Coal_Miner 2 <list of blocks, contains>
		if(command.getName().equalsIgnoreCase("task")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			// /task
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				names.add("list");
				names.add("new");
				
				if(playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal()) {
					names.add("create");
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				return applicableNames;
			}
			
			// LIST:
			
			if(args.length == 2 && args[0].equalsIgnoreCase("list")) {
				List<String> names = new ArrayList<String>();
				names.add("<page>");
				return names;
			}
			if(args.length == 3 && args[0].equalsIgnoreCase("list")) {
				List<String> names = new ArrayList<String>();
				names.add("all");
				for(TaskType type : TaskType.values()) {
					names.add(type.toString());
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[2].length()) {
						if(args[2].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[2].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				return applicableNames;
			}
			if(args.length == 4 && args[0].equalsIgnoreCase("list")) {
				List<String> names = new ArrayList<String>();
				for(Difficulty difficulty : Difficulty.values()) {
					names.add(difficulty.toString());
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[3].length()) {
						if(args[3].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[3].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				return applicableNames;
			}
			
			
			// task creator
			if(args.length > 1 &&
					(args[0].equalsIgnoreCase("c") || args[0].equalsIgnoreCase("create"))
					&& (playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal())) {
				
				if(args.length == 2) {
					List<String> names = new ArrayList<String>();
					names.add("new");
					names.add("<ID>");
					return names;
				}
				
				if(args.length == 3) {
					List<String> names = new ArrayList<String>();
					for(Difficulty difficulty : Difficulty.values()) {
						names.add(difficulty.toString());
					}
					
					List<String> applicableNames = new ArrayList<String>();
					for(String name : names) {
						if(name.length() > args[2].length()) {
							if(args[2].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[2].length()))) { //use to have precise start
								applicableNames.add(name);
							}
						}
					}
					return applicableNames;
				}
				
				if(args.length == 4) {
					List<String> names = new ArrayList<String>();
					names.add("<task name>");
					return names;
				}
				
				if(args.length == 5) {
					List<String> names = new ArrayList<String>();
					for(TaskType type : TaskType.values()) {
						names.add(type.toString());
					}
					
					List<String> applicableNames = new ArrayList<String>();
					for(String name : names) {
						if(name.length() > args[4].length()) {
							if(args[4].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[4].length()))) { //use to have precise start
								applicableNames.add(name);
							}
						}
					}
					return applicableNames;
				}
				
				//what
				if(args.length == 6) {
					if(args[4].equalsIgnoreCase("" + TaskType.KILL.toString())) {
						List<String> applicableNames = new ArrayList<String>();
						for(EntityType entity : EntityType.values()) {
							if(entity.isAlive()) {
								if(entity.toString().toLowerCase().contains(args[args.length - 1].toLowerCase())) {
									applicableNames.add(entity.toString());
								}
							}
						}
						return applicableNames;
					}
					
					if(args[4].equalsIgnoreCase("" + TaskType.BREED.toString())) {
						List<String> applicableNames = new ArrayList<String>();
						for(EntityType type : EntityType.values()) {
							
							if(type.isSpawnable() && type.isAlive()) {
								Entity entity = player.getWorld().spawnEntity(player.getLocation(), type);
								if(entity instanceof Breedable) {
									if(entity.toString().toLowerCase().contains(args[args.length - 1].toLowerCase())) {
										applicableNames.add(type.toString());
									}
								}
								entity.remove();
							}
						}
						return applicableNames;
					}
					
					if(args[4].equalsIgnoreCase("" + TaskType.BREAK.toString())
							|| args[4].equalsIgnoreCase("" + TaskType.PLACE.ordinal())) {
						
						List<String> blocks = new ArrayList<String>();
						for(Material block : Material.values()) {
							if(block.isBlock()) {
								blocks.add(block.toString());
							}
						}
						
						List<String> applicableNames = new ArrayList<String>();
						for(String block : blocks) {
							if(block.toLowerCase().contains(args[args.length - 1].toLowerCase())) {
								applicableNames.add(block);
							}
						}
						return applicableNames;
					}
					
					if(args[4].equalsIgnoreCase("" + TaskType.HARVEST.toString())) {
						
						TaskType type = TaskType.valueOf(args[4].toUpperCase());
						List<String> whats = DeadMC.TaskFile.data().getStringList("What." + type.ordinal());
						
						List<String> applicableNames = new ArrayList<String>();
						for(String what : whats) {
							if(what.toLowerCase().contains(args[args.length - 1].toLowerCase())) {
								applicableNames.add(what);
							}
						}
						return applicableNames;
					}
					
				}
				
				if(args.length == 7) {
					List<String> names = new ArrayList<String>();
					names.add("<amount>");
					return names;
				}
				
				//using
				if(args.length == 8) {
					
					List<String> items = new ArrayList<String>();
					items.add(".");
					for(Material item : Material.values()) {
						if(item.isItem()) {
							items.add(item.toString());
						}
					}
					
					List<String> applicableNames = new ArrayList<String>();
					for(String item : items) {
						if(item.toLowerCase().contains(args[args.length - 1].toLowerCase())) {
							applicableNames.add(item);
						}
					}
					return applicableNames;
				}
				
				//step description
				if(args.length == 9) {
					//auto generate
					TaskType type = TaskType.valueOf(args[4].toUpperCase());
					String typeString = type.toString().substring(0, 1).toUpperCase() + type.toString().substring(1, type.toString().length()).toLowerCase();
					
					String using = null;
					if(!args[7].equalsIgnoreCase("."))
						using = args[7];
					String usingString = "";
					if(using != null)
						usingString = " using a " + using.toLowerCase().replace("_", " ");
					
					int amount = Integer.parseInt(args[6]);
					String plural = "";
					if(amount > 1) plural = "s";
					
					//use item in hand for place
					String what = null;
					if(type == TaskType.PLACE || type == TaskType.BREAK || type == TaskType.HARVEST) {
						if(args[5].equalsIgnoreCase(".")) {
							what = player.getInventory().getItemInMainHand().getType().toString().toLowerCase().replace("_", " ");
						} else {
							what = args[5];
						}
					} else if(type == TaskType.KILL || type == TaskType.BREED) {
						if(args[5].equalsIgnoreCase(".")) {
							what = "entities";
						} else {
							what = args[5];
						}
					} else {
						List<String> whats = DeadMC.TaskFile.data().getStringList("What." + type.ordinal());
						what = whats.get(Integer.parseInt(args[5]));
					}
					String whatString = what.toLowerCase() + plural;
					
					List<String> stepDescription = new ArrayList<String>();
					stepDescription.add(new String(typeString + " " + amount + " " + whatString + usingString).replace(" ", "_"));
					
					return stepDescription;
				}
				
				if(args.length == 10) {
					List<String> names = new ArrayList<String>();
					names.add("<tips>");
					names.add(".");
					return names;
				}
			}
			
		}
		
		if(command.getName().equalsIgnoreCase("gift")) {
			if(args.length > 1) {
				List<String> names = new ArrayList<String>();
				names.add("<note>");
				return names;
			}
		}
		
		//land <commands player can use>
		if(command.getName().equalsIgnoreCase("land")) {
			
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				names.add("help");
				names.add("info");
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				if(playerConfig.getString("Town") != null) {
					names.add("define");
					names.add("unclaim");
					names.add("claimtool");
					names.add("share");
					names.add("unshare");
					names.add("sell");
					names.add("buy");
					names.add("spawning");
					
					if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
						names.add("claim");
					}
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			if(args.length == 3 && args[0].equalsIgnoreCase("sell")) {
				List<String> names = new ArrayList<String>();
				names.add("<price>");
				return names;
			}
			
		}
		
		if(command.getName().equalsIgnoreCase("nation") || command.getName().equalsIgnoreCase("n")) {
			List<String> options = new ArrayList<String>();
			
			if(args.length == 1) {
				
				options.add("list");
				options.add("lookup");
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
				String originalTownName = playerConfig.getString("Town");
				if(originalTownName != null) {
					TownConfig townConfig = TownConfig.getConfig(originalTownName);
					String townDisplayName = Town.getTownDisplayName(originalTownName);
					boolean isMayor = playerConfig.getInt("TownRank") >= Town.Rank.Mayor.ordinal();
					
					String nationName = townConfig.getString("Nation");
					if(nationName != null) {
						//is a part of a nation
						NationConfig nationConfig = NationConfig.getConfig(nationName);

						if(isMayor) {
							options.add("leave");

							boolean isCapitalTown = nationConfig.getString("Capital").equalsIgnoreCase(originalTownName);
							if(isCapitalTown) {
								options.add("invite");
								options.add("banish");
								options.add("set");
							}
						}
					} else {
						//not in a nation
						if(isMayor) {
							options.add("create");
							options.add("join");
						}
					}
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String option : options) {
					if(option.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(option.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
							applicableNames.add(option);
						}
					}
				}
				
				return applicableNames;
			}
			
			//args greater than 1
			
			if(args[0].equalsIgnoreCase("create")) {
				if(args.length == 2) {
					options.add("<nation name>");
				}
			}
			
			if(args[0].equalsIgnoreCase("list")) {
				if(args.length == 2) {
					options.add("<page number>");
				}
			}
			
			if(args[0].equalsIgnoreCase("lookup")) {
				if(args.length == 2) {
					//show all nations
					options.addAll(DeadMC.NationFile.data().getStringList("Active"));
				}
			}
			
			if(args[0].equalsIgnoreCase("join")) {
				if(args.length == 2) {
					//get nations that match
					for(String nation : DeadMC.NationFile.data().getStringList("Active")) {
						if(nation.toLowerCase().contains(args[1].toLowerCase())) {
							options.add(nation);
						}
					}
				}
			}
			
			if((args[0].equalsIgnoreCase("banish") && args.length == 2)
				|| (args.length == 3 && args[0].equalsIgnoreCase("set") && args[1].equalsIgnoreCase("capital"))) {
				String nationName = Nation.isLeader(player.getUniqueId());
				if(nationName != null) {
					//get towns in the nation
					NationConfig nationConfig = NationConfig.getConfig(nationName);
					for(String townInNation : nationConfig.getStringList("Towns")) {
						String townDisplayName = Town.getTownDisplayName(townInNation);
						options.add(townDisplayName);
					}
				}
			}
			
			if(args.length == 2 && args[0].equalsIgnoreCase("invite")) {
				//show town names
				List<String> aliases = DeadMC.TownFile.data().getString("Aliases") != null ? DeadMC.TownFile.data().getStringList("Aliases") : new ArrayList<String>();
				for(String town : DeadMC.TownFile.data().getStringList("Active")) {
					if(DeadMC.TownFile.data().getString("AliasOriginalName." + Town.townNameCased(args[1])) == null
							&& town.length() > args[1].length()
							&& args[1].toLowerCase().equalsIgnoreCase(town.toLowerCase().substring(0, args[1].length())))
						options.add(town);
				}
				for(String townAlias : aliases) {
					if(townAlias.length() > args[1].length()
							&& args[1].toLowerCase().equalsIgnoreCase(townAlias.toLowerCase().substring(0, args[1].length())))
						options.add(townAlias);
				}
			}
			
			if(args[0].equalsIgnoreCase("set")) {
				if(args.length == 2) {
					options.add("recruiting");
					options.add("capital");
					options.add("description");
				}
				if(args.length == 3 && args[1].equalsIgnoreCase("recruiting")) {
					options.add("public");
					options.add("invite-only");
					options.add("closed");
				}
				if(args.length >= 3 && args[1].equalsIgnoreCase("description")) {
					options.add("<description>");
				}
			}
			
			//return blank otherwise
			return options;
		}
		
		//town members <town>
		//town lookup / join <town>
		//land lookup <town>
		if((command.getName().equalsIgnoreCase("town") || command.getName().equalsIgnoreCase("land")
				|| command.getName().equalsIgnoreCase("t") || command.getName().equalsIgnoreCase("towns") || command.getName().equalsIgnoreCase("f") || command.getName().equalsIgnoreCase("factions") || command.getName().equalsIgnoreCase("towny"))
				&& args.length == 2
				&& (args[0].equalsIgnoreCase("lookup") || args[0].equalsIgnoreCase("members") || args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("join"))) {
			
			List<String> applicableNames = new ArrayList<String>();
			List<String> aliases =  DeadMC.TownFile.data().getString("Aliases") != null ? DeadMC.TownFile.data().getStringList("Aliases") : new ArrayList<String>();
			
			//add all towns & aliases:
			for(String town : DeadMC.TownFile.data().getStringList("Active")) {
				boolean hasAlias = !Town.getTownDisplayName(town).equals(town);
				if(!hasAlias
						&& town.length() > args[1].length()
						&& args[1].toLowerCase().equalsIgnoreCase(town.toLowerCase().substring(0, args[1].length())))
					applicableNames.add(town);
			}
			for(String townAlias : aliases) {
				if(townAlias.length() > args[1].length()
						&& args[1].toLowerCase().equalsIgnoreCase(townAlias.toLowerCase().substring(0, args[1].length())))
					applicableNames.add(townAlias);
			}
			return applicableNames;
		}
		
		if(command.getName().equalsIgnoreCase("town") && args.length >= 3 && args[1].equalsIgnoreCase("travel-point") && args[0].equalsIgnoreCase("set")) {
			if(args.length == 3) {
				List<String> names = new ArrayList<String>();
				names.add("public");
				names.add("private");
				
				return names;
			}
		}
		
		if(command.getName().equalsIgnoreCase("town") && args.length >= 2 && args[0].equalsIgnoreCase("create")) {
			List<String> names = new ArrayList<String>();
			names.add("<town name>");
			
			return names;
		}
		
		//land info ... <town's regions> 
		if(command.getName().equalsIgnoreCase("land")
				&& args.length == 3
				&& (args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("lookup"))) {
			if(Town.townNameIsTaken(args[1])) {
				
				String townName = Town.getOriginalTownName(args[1]);
				TownConfig townConfig = TownConfig.getConfig(townName);
				
				List<String> regions = townConfig.getStringList("Regions");
				
				List<String> applicableNames = new ArrayList<String>();
				for(String regionID : regions) {
					if(regionID.toLowerCase().contains(args[2].toLowerCase())) {
						applicableNames.add(regionID);
					}
				}
				
				return applicableNames;
			}
		}
		
		//land unclaim / share / unshare / sell / spawning <region>
		if(command.getName().equalsIgnoreCase("land")
				&& args.length == 2
				&& (args[0].equalsIgnoreCase("unclaim") || args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("share") || args[0].equalsIgnoreCase("unshare") || args[0].equalsIgnoreCase("sell") || args[0].equalsIgnoreCase("spawning"))
				&& PlayerConfig.getConfig(player).getString("Town") != null) {
			
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			
			String originalTownName = Town.getOriginalTownName(PlayerConfig.getConfig(player).getString("Town"));
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			
			List<String> regions = townConfig.getStringList("Regions");
			
			List<String> regionsPlayerOwns = new ArrayList<String>();
			for(String regionID : regions) {
				String regionName = originalTownName + "/" + regionID;
				ProtectedRegion region = regionManager.getRegion(regionName);
				
				if(region != null) {
					for(UUID owner : region.getOwners().getUniqueIds()) {
						if(owner.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
							regionsPlayerOwns.add(regionID);
							break;
						}
					}
				}
				
			}
			
			List<String> applicableNames = new ArrayList<String>();
			for(String regionID : regionsPlayerOwns) {
				if(regionID.toLowerCase().contains(args[1].toLowerCase())) {
					applicableNames.add(regionID);
				}
			}
			return applicableNames;
			
		}
		
		if(command.getName().equalsIgnoreCase("land")
				&& args.length == 3
				&& args[0].equalsIgnoreCase("spawning")) {
			
			List<String> names = new ArrayList<String>();
			names.add("on");
			names.add("off");
			
			return names;
		}
		
		if(command.getName().equalsIgnoreCase("land")
				&& args.length == 3
				&& args[0].equalsIgnoreCase("share")
				&& PlayerConfig.getConfig(player).getString("Town") != null) {
			
			List<String> names = new ArrayList<String>();
			for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
				if(!onlinePlayer.getName().equalsIgnoreCase(player.getName())) {
					names.add(onlinePlayer.getName()); //add normal names
					PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
					if(onlineConfig.getString("Name") != null) {
						names.add(Chat.stripColor(onlineConfig.getString("Name"))); //add nick names if exist
					}
				}
			}
			TownConfig townConfig = TownConfig.getConfig(PlayerConfig.getConfig(player).getString("Town"));
			for(String uuid : townConfig.getStringList("Members")) {
				if(!uuid.equalsIgnoreCase(player.getUniqueId().toString())) {
					names.add(Bukkit.getOfflinePlayer(UUID.fromString(uuid)).getName()); //add normal names
					PlayerConfig memberConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
					if(memberConfig.getString("Name") != null) {
						names.add(Chat.stripColor(memberConfig.getString("Name"))); //add nick names if exist
					}
				}
			}
			
			List<String> applicableNames = new ArrayList<String>();
			for(String name : names) {
				if(name.toLowerCase().contains(args[args.length - 1].toLowerCase())) {
					applicableNames.add(name);
				}
			}
			
			return applicableNames;
		}
		
		if(command.getName().equalsIgnoreCase("land")
				&& args.length == 3
				&& args[0].equalsIgnoreCase("unshare")) {
			
			List<String> applicableNames = new ArrayList<String>();
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(playerConfig.getString("Town") != null) {
				
				String originalTownName = playerConfig.getString("Town");
				String townDisplayName = Town.getTownDisplayName(originalTownName);
				
				String regionName = originalTownName + "/" + args[1].toLowerCase();
				
				if(regionManager.hasRegion(regionName) && regionManager.getRegion(regionName).getOwners().getUniqueIds().contains(player.getUniqueId())) {
					DefaultDomain members = regionManager.getRegion(regionName).getMembers();
					
					List<String> names = new ArrayList<String>();
					for(UUID member : members.getUniqueIds()) {
						String name = Bukkit.getOfflinePlayer(member).getName();
						if(!member.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
							names.add(name); //add normal names
							PlayerConfig onlineConfig = PlayerConfig.getConfig(member);
							if(onlineConfig.getString("Name") != null) {
								names.add(Chat.stripColor(onlineConfig.getString("Name"))); //add nick names if exist
							}
						}
					}
					
					for(String name : names) {
						if(name.toLowerCase().contains(args[args.length - 1].toLowerCase())) {
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
		}
		
		
		//land buy <sale ID>
		if(command.getName().equalsIgnoreCase("land")
				&& args.length == 2
				&& (args[0].equalsIgnoreCase("buy"))
				&& PlayerConfig.getConfig(player).getString("Town") != null) {
			
			List<String> applicableSaleIDs = new ArrayList<String>();
			for(int count = 1; count <= DeadMC.RegionFile.data().getInt("SignIDcount"); count++) {
				String regionID = DeadMC.RegionFile.data().getString(count + ".Region");
				if(regionID != null) {
					String originalTownName = Regions.getOriginalTownName(regionID);
					if(originalTownName != null && originalTownName.equalsIgnoreCase(PlayerConfig.getConfig(player).getString("Town")))
						applicableSaleIDs.add("" + count);
					
				}
			}
			
			return applicableSaleIDs;
			
		}
		
		//land unshare/share .region. <town members>
		if(command.getName().equalsIgnoreCase("land")
				&& ((args.length == 2 && args[0].equalsIgnoreCase("unshare"))
				|| (args.length == 3 && args[0].equalsIgnoreCase("share")))
				&& PlayerConfig.getConfig(player).getString("Town") != null) {
			
			List<String> playersInTown = new ArrayList<String>();
			
			for(String uuid : TownConfig.getConfig(PlayerConfig.getConfig(player).getString("Town")).getStringList("Members")) {
				String playerInTown = Bukkit.getOfflinePlayer(UUID.fromString(uuid)).getName();
				if(!playerInTown.equalsIgnoreCase(player.getName()))
					playersInTown.add(playerInTown);
			}
			
			List<String> applicableNames = new ArrayList<String>();
			for(String playerInTown : playersInTown) {
				if(playerInTown.toLowerCase().contains(args[args.length - 1].toLowerCase())) {
					applicableNames.add(playerInTown);
				}
			}
			return applicableNames;
			
		}
		
		if(command.getName().equalsIgnoreCase("town")) {
			
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			//town <commands player can use>
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				names.add("list");
				names.add("lookup");
				names.add("info");
				names.add("members");
				
				if(playerConfig.getString("Town") != null) {
					names.add("leave");
					names.add("funds");
					
					if(playerConfig.getInt("TownRank") >= Rank.Veteran.ordinal()) {
						names.add("invite");
					}
					
					if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
						names.add("upgrade");
						names.add("banish");
						names.add("set");
						names.add("rank");
					}
					
				} else {
					names.add("create");
					names.add("join");
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			//town funds <add/withdraw>
			if(args.length == 2
					&& args[0].equalsIgnoreCase("funds")) {
				
				List<String> names = new ArrayList<String>();
				names.add("add");
				if(playerConfig.getInt("TownRank") >= Rank.Mayor.ordinal())
					names.add("withdraw");
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[1].length()) {
						if(args[1].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[1].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			//town rank <player> <rank>
			if(args.length == 3
					&& args[0].equalsIgnoreCase("rank")) {
				
				List<String> names = new ArrayList<String>();
				for(Town.Rank rank : Town.Rank.values())
					names.add(rank.toString().replace("_", "-"));
				
				return names;
			}
			
			//town set <name, description, travel-point, travel-fee, recruiting, tax>
			if(args.length == 2
					&& args[0].equalsIgnoreCase("set")) {
				
				List<String> names = new ArrayList<String>();
				if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
					names.add("name");
					names.add("description");
					names.add("travel-point");
					names.add("banner");
					names.add("travel-fee");
					names.add("recruiting");
					names.add("tax");
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[1].length()) {
						if(args[1].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[1].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			//town set recruiting <public,private,invite-only>
			if(args.length == 3
					&& args[1].equalsIgnoreCase("recruiting")) {
				
				List<String> names = new ArrayList<String>();
				if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
					names.add("public");
					names.add("private");
					names.add("invite-only");
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[2].length()) {
						if(args[2].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[2].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
				
			}
			
			//town upgrade <claim-limit, member-limit>
			if(args.length == 2
					&& args[0].equalsIgnoreCase("upgrade")) {
				
				List<String> names = new ArrayList<String>();
				if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
					names.add("claim-limit");
					names.add("member-limit");
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[1].length()) {
						if(args[1].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[1].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			//town rank <player> <citizen,respected,veteran,co-mayor,mayor>
			if(args.length == 2 && args[0].equalsIgnoreCase("rank")
					&& PlayerConfig.getConfig(player).getString("Town") != null) {
				
				List<String> playersInTown = new ArrayList<String>();
				
				for(String uuid : TownConfig.getConfig(PlayerConfig.getConfig(player).getString("Town")).getStringList("Members")) {
					String playerInTown = Bukkit.getOfflinePlayer(UUID.fromString(uuid)).getName();
					if(!playerInTown.equalsIgnoreCase(player.getName()))
						playersInTown.add(playerInTown);
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String playerInTown : playersInTown) {
					if(playerInTown.toLowerCase().contains(args[args.length - 1].toLowerCase())) {
						applicableNames.add(playerInTown);
					}
				}
				return applicableNames;
				
			}
			
		}
		
		//cart <commands player can use>
//		if(command.getName().equalsIgnoreCase("cart")) {
//			
//			if(args.length == 1) {
//				List<String> names = new ArrayList<String>();
//				names.add("add");
//				names.add("list");
//				names.add("view");
//				names.add("remove");
//				names.add("empty");
//				
//				List<String> applicableNames = new ArrayList<String>();
//				for(String name : names) {
//					if(name.length() > args[0].length()) {
//						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
//							applicableNames.add(name);
//						}
//					}
//				}
//				
//				return applicableNames;
//			}
//			
//			//cart view / empty / remove <cart ID in use>
//			if(args.length == 2
//					&& (args[0].equalsIgnoreCase("view")
//					|| args[0].equalsIgnoreCase("empty")
//					|| args[0].equalsIgnoreCase("remove"))) {
//				
//				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
//				
//				if(playerConfig.getStringList("Carts.Shops") == null) return null;
//				List<String> carts = playerConfig.getStringList("Carts.Shops");
//				return carts;
//				
//			}
//			
//			//cart remove .Cart. <product IDs in cart>
//			if(args.length == 3
//					&& (args[0].equalsIgnoreCase("remove"))) {
//				
//				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
//				
//				if(playerConfig.getStringList("Carts.Shops." + args[1] + ".Products") == null) return null;
//				List<String> products = playerConfig.getStringList("Carts." + args[1] + ".Products");
//				return products;
//			}
//			
//			//cart remove .Cart. .Product. all
//			if(args.length == 4
//					&& (args[0].equalsIgnoreCase("remove"))) {
//				
//				List<String> names = new ArrayList<String>();
//				names.add("1");
//				names.add("all");
//				
//				return names;
//			}
//			
//		}
		
		if(command.getName().equalsIgnoreCase("sell")
				|| command.getName().equalsIgnoreCase("buy")) {
			//sell/buy <1/all>
			if(args.length == 1) {
				
				List<String> names = new ArrayList<String>();
				names.add("<amount>");
				names.add("all");
				
				return names;
			}
		}
		
		//shop <commands player can use>
		if((command.getName().equalsIgnoreCase("shop")
				|| command.getName().equalsIgnoreCase("s")
				|| command.getName().equalsIgnoreCase("shops"))) {
			
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				names.add("item");
				names.add("buyprice");
				names.add("sellprice");
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			if(args.length == 2
					&& (args[0].equalsIgnoreCase("buyprice") || args[0].equalsIgnoreCase("sellprice"))) {
				List<String> names = new ArrayList<String>();
				names.add("<price>");
				
				return names;
			}
			if(args.length == 2
					&& args[0].equalsIgnoreCase("item")) {
				List<String> names = new ArrayList<String>();
				names.add("");
				
				return names;
			}
		}
		
		//banner <commands player can use>
		if(command.getName().equalsIgnoreCase("banner")
				|| command.getName().equalsIgnoreCase("banners")) {
			
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				names.add("list");
				names.add("create");
				names.add("place");

				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}

				return applicableNames;
			}

			//banner place/create <existing banner names>
			if(args.length == 2
					&& (args[0].equalsIgnoreCase("create")
					|| args[0].equalsIgnoreCase("place"))) {

				PlayerConfig playerConfig = PlayerConfig.getConfig(player);

				List<String> banners = new ArrayList<String>();
				if(playerConfig.getString("Banners") != null)
					banners = playerConfig.getStringList("Banners");

				List<String> applicableNames = new ArrayList<String>();
				for(String name : banners) {
					if(name.toLowerCase().contains(args[1].toLowerCase())) {
						applicableNames.add(name);
					}
				}

				return applicableNames;
			}
		}
		
		//kit <commands player can use>
		if(command.getName().equalsIgnoreCase("kit")
				&& args.length == 1) {
			
			List<String> names = new ArrayList<String>();
			names.add("food");
			names.add("squire-protection");
			names.add("knight-protection");
			names.add("baron-protection");
			names.add("royal-protection");
			names.add("emperor-protection");
			
			List<String> applicableNames = new ArrayList<String>();
			for(String name : names) {
				if(name.length() > args[0].length()) {
					if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
						applicableNames.add(name);
					}
				}
			}
			
			return applicableNames;
		}
		
		//leaderboard <commands player can use>
		if((command.getName().equalsIgnoreCase("leaderboards")
				|| command.getName().equalsIgnoreCase("leaderboard")
				|| command.getName().equalsIgnoreCase("lb"))
				&& args.length == 1) {
			
			List<String> names = new ArrayList<String>();
			names.add("coins");
			names.add("kills");
			names.add("playtime");
			names.add("skills");
			names.add("towns");
			names.add("tasks");
			names.add("streak");
			names.add("bmleader");
			names.add("bmkills");
			names.add("welcome");
			names.add("votes");
			names.add("votes_month");
			
			List<String> applicableNames = new ArrayList<String>();
			for(String name : names) {
				if(name.length() > args[0].length()) {
					if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
						applicableNames.add(name);
					}
				}
			}
			
			return applicableNames;
		}
		
		if(command.getName().equalsIgnoreCase("restarter")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(player.isOp() || (playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal())) {
				if(args.length == 1) {
					List<String> names = new ArrayList<String>();
					names.add("force-restart");
					names.add("force-maintenance");
					names.add("cancel");
					return names;
				}
				if(args.length == 2) {
					List<String> names = new ArrayList<String>();
					names.add("<warning time in seconds>");
					return names;
				}
				if(args.length == 3) {
					List<String> names = new ArrayList<String>();
					names.add("<type here to backup first>");
					return names;
				}
			}
		}
		
		//travel <private + town + public + player names>
		if(command.getName().equalsIgnoreCase("travel") || command.getName().equalsIgnoreCase("sethome") || command.getName().equalsIgnoreCase("home") || command.getName().equalsIgnoreCase("warp") || command.getName().equalsIgnoreCase("tp") || command.getName().equalsIgnoreCase("teleport")) {
			
			if(args.length >= 2) {
				//travel to player
				String playerName = args[0];
				for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
					PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
					if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[0])) {
						//used nick name
						playerName = onlinePlayer.getName();
						break;
					}
				}
				if(Bukkit.getPlayerExact(playerName) != null) {
					List<String> names = new ArrayList<String>();
					names.add("player");
					return names;
				}
			}
			
			if(args.length >= 2
					&& args[0].contains("<private")) {
				List<String> names = new ArrayList<String>();
				names.add("Press BACKSPACE to see private points");
				return names;
			}
			if(args.length >= 2
					&& args[0].contains("<public")) {
				List<String> names = new ArrayList<String>();
				names.add("Press BACKSPACE to see public points");
				return names;
			}
			if(args.length >= 2
					&& args[0].contains("<player")) {
				List<String> names = new ArrayList<String>();
				names.add("Press BACKSPACE to see players");
				return names;
			}
			
			//travel
			if(args.length == 1) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				List<String> travelPoints = new ArrayList<String>();
				travelPoints.add("accept");
				travelPoints.add("decline");
				travelPoints.add("menu");
				travelPoints.add("create");
				travelPoints.add("delete");
				travelPoints.add("list");
				if(playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal())
					travelPoints.add("pos");
				
				List<String> applicableNames = new ArrayList<String>();
				
				if(args[0].length() == 0) {
					
					travelPoints.add("<private_point>");
					travelPoints.add("<public_point>");
					travelPoints.add("<player>");
					
				} else {
					
					if(args[0].contains("<private")) {
						//just show private points
						List<String> privateTravelPoints = new ArrayList<String>();
						if(playerConfig.getStringList("Travel.Private") != null)
							privateTravelPoints = playerConfig.getStringList("Travel.Private");
						return privateTravelPoints;
					}
					if(args[0].contains("<public")) {
						//just show private points
						List<String> privateTravelPoints = new ArrayList<String>();
						privateTravelPoints.addAll(DeadMC.TravelFile.data().getStringList("Public"));
						return privateTravelPoints;
					}
					if(args[0].contains("<player")) {
						//just show players
						List<String> privateTravelPoints = new ArrayList<String>();
						for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
							if(!onlinePlayer.getName().equalsIgnoreCase(player.getName())) {
								privateTravelPoints.add(onlinePlayer.getName()); //add normal names
								PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
								if(onlineConfig.getString("Name") != null) {
									privateTravelPoints.add(Chat.stripColor(onlineConfig.getString("Name"))); //add nick names if exist
								}
							}
						}
						return privateTravelPoints;
					}
					
					
					List<String> privateTravelPoints = new ArrayList<String>();
					if(playerConfig.getStringList("Travel.Private") != null)
						privateTravelPoints = playerConfig.getStringList("Travel.Private");
					
					travelPoints.addAll(privateTravelPoints);
					travelPoints.addAll(DeadMC.TravelFile.data().getStringList("Public"));
					
					for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						if(!onlinePlayer.getName().equalsIgnoreCase(player.getName())) {
							travelPoints.add(onlinePlayer.getName()); //add normal names
							String recordedName = PlaceHolders.playersName.get(onlinePlayer.getUniqueId().toString());
							if(recordedName != null && !recordedName.equalsIgnoreCase(onlinePlayer.getName())) {
								travelPoints.add(Chat.stripColor(recordedName)); //add nick names if exist
							}
						}
					}
					
					//add all towns & aliases:
					List<String> aliases = new ArrayList<String>();
					if(DeadMC.TownFile.data().getString("Aliases") != null)
						aliases = DeadMC.TownFile.data().getStringList("Aliases");
					for(String town : DeadMC.TownFile.data().getStringList("Active")) {
						if(DeadMC.TownFile.data().getString("AliasOriginalName." + Town.townNameCased(args[0])) == null
								&& town.length() > args[0].length()
								&& args[0].toLowerCase().equalsIgnoreCase(town.toLowerCase().substring(0, args[0].length())))
							applicableNames.add(town);
					}
					for(String townAlias : aliases) {
						if(townAlias.length() > args[0].length()
								&& args[0].toLowerCase().equalsIgnoreCase(townAlias.toLowerCase().substring(0, args[0].length())))
							applicableNames.add(townAlias);
					}
					
				}
				
				for(String name : travelPoints) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) {
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			//travel create <private/public>
			if(args.length > 1 && args[0].equalsIgnoreCase("create")) {
				List<String> names = new ArrayList<String>();
				return names;
			}
			
			if(args.length == 2 && args[0].equalsIgnoreCase("pos")) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				if(playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal()
						|| player.getWorld().getName().equalsIgnoreCase("revamp")) {
					List<String> names = new ArrayList<String>();
					names.add("<x>");
					return names;
				}
			}
			if(args.length == 3 && args[0].equalsIgnoreCase("pos")) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				if(playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal()
						|| player.getWorld().getName().equalsIgnoreCase("revamp")) {
					List<String> names = new ArrayList<String>();
					names.add("<z>");
					return names;
				}
			}
			
			//travel delete <point>
			if(args.length == 2 && args[0].equalsIgnoreCase("delete")) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				List<String> travelPoints = new ArrayList<String>();
				
				List<String> privateTravelPoints = new ArrayList<String>();
				if(playerConfig.getString("Travel.Private") != null)
					privateTravelPoints = playerConfig.getStringList("Travel.Private");
				List<String> publicTravelPoints = new ArrayList<String>();
				if(playerConfig.getString("Travel.Public") != null)
					publicTravelPoints = playerConfig.getStringList("Travel.Public");
				
				travelPoints.addAll(privateTravelPoints);
				travelPoints.addAll(publicTravelPoints);
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : travelPoints) {
					if(name.toLowerCase().contains(args[1].toLowerCase())) {
						applicableNames.add(name);
					}
				}
				
				return applicableNames;
			}
			
		}
		
		if(command.getName().equalsIgnoreCase("report") ||
				command.getName().equalsIgnoreCase("punish")) {
			
			if(args.length == 2) {
				List<String> names = new ArrayList<String>();
				for(Punishment punishment : Punishment.values())
					names.add(punishment.toString().toUpperCase());
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[1].length()) {
						if(args[1].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[1].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			if(args.length > 1) {
				String punishment = args[1];
				if(punishment.equalsIgnoreCase(Punishment.HACKING.toString()) || punishment.equalsIgnoreCase(Punishment.FALSE_REPORTING.toString()) || punishment.equalsIgnoreCase(Punishment.BUG_ABUSE.toString()) || punishment.equalsIgnoreCase(Punishment.PUBLIC_NUISANCE.toString()) || punishment.equalsIgnoreCase(Punishment.ALIAS_ABUSE.toString())) {
					
					if(args.length == 3) {
						List<String> names = new ArrayList<String>();
						names.add("1hour");
						names.add("12hours");
						names.add("1day");
						names.add("2days");
						names.add("1week");
						names.add("2weeks");
						names.add("1month");
						names.add("2months");
						names.add(".PERMANENT_(Must_appeal_on_website)");
						return names;
					}
					
					if(args.length >= 4) {
						List<String> names = new ArrayList<String>();
						names.add("<evidence/notes>");
						return names;
					}
					
				} else if(args.length >= 3) {
					List<String> names = new ArrayList<String>();
					names.add("<evidence/notes>");
					return names;
				}
			}
			
		}
		
		if(command.getName().equalsIgnoreCase("backup")
				&& args.length == 1) {
			List<String> names = new ArrayList<String>();
			for(Backup.BackupType type : Backup.BackupType.values())
				names.add(type.toString());
			return names;
		}
		
		if(command.getName().equalsIgnoreCase("find")
				&& args.length == 1) {
			
			List<String> applicableNames = new ArrayList<String>();
			for(String itemFinderItemName : TravelGUI.getItemFinderItemNames()) {
				if(itemFinderItemName.toLowerCase().contains(args[0].toLowerCase())) {
					applicableNames.add(itemFinderItemName);
				}
			}
			
			applicableNames.add("<keyword>");
			return applicableNames;
		}
		
		if(command.getName().equalsIgnoreCase("disguise")
				|| command.getName().equalsIgnoreCase("dis")) {
			if(args.length == 1) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				List<String> names = new ArrayList<String>();
				if(playerConfig.getInt("DonateRank") >= Donator.Rank.Emperor.ordinal()) {
					for(EntityType type : EntityType.values()) {
						if(type != EntityType.UNKNOWN && type != EntityType.LIGHTNING) { //banned types
							names.add(type.toString().toUpperCase());
						}
					}
					for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						if(!onlinePlayer.getName().equalsIgnoreCase(player.getName())) {
							names.add(onlinePlayer.getName());
						}
					}
				} else if(playerConfig.getInt("DonateRank") >= Donator.Rank.Baron.ordinal()) {
					for(String type : DeadMC.DonatorFile.data().getStringList(playerConfig.getInt("DonateRank") + ".Upgrades.Disguises")) {
						names.add(type.toUpperCase());
					}
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) {
							applicableNames.add(name);
						}
					}
				}
				if(playerConfig.getInt("DonateRank") >= Donator.Rank.Emperor.ordinal())
					applicableNames.add("<player>");
				return applicableNames;
			}
		}
		
		if(command.getName().equalsIgnoreCase("pet")
				|| command.getName().equalsIgnoreCase("pets")) {
			
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(args.length == 1
					|| (args.length == 2 && args[0].equalsIgnoreCase("setname"))) {
				List<String> names = new ArrayList<String>();
				if(args.length == 1) names.add("setname");
				for(Pet pet : Pets.Pet.values()) {
					if(playerConfig.getString("Pets." + pet.ordinal() + ".Obtained") != null) {
						String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString();
						names.add(entityNick); //add actual name
						if(playerConfig.getString("Pets." + pet.ordinal() + ".Name") != null) {
							names.add(Chat.stripColor(playerConfig.getString("Pets." + pet.ordinal() + ".Name"))); //add the nick name
						}
					}
				}
				if(args.length == 1) {
					List<String> applicableNames = new ArrayList<String>();
					for(String name : names) {
						if(name.length() > args[0].length()) {
							if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) {
								applicableNames.add(name);
							}
						}
					}
					return applicableNames;
				} else return names;
			}
			
			if(args.length == 3 && args[0].equalsIgnoreCase("setname")) {
				List<String> names = new ArrayList<String>();
				names.add("<name>");
				return names;
			}
			
		}
		
		if(command.getName().equalsIgnoreCase("dmc")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			//senior mod commands:
			// /delete_public_tp
			// delete_region
			// delete_town
			// set helper
			// townname
			
			
			
			// /dmc
			if(player.isOp() || (playerConfig.getString("StaffRank") != null)) {
				if(args.length == 1) {
					List<String> names = new ArrayList<String>();
					if(playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal()) {
						names.add("playername");
						names.add("lbhead");
						names.add("villagers");
						names.add("tppos");
						names.add("setcoins");
						names.add("reset_aliases");
						names.add("create_no_drop_item");
						names.add("spawn_no_drop_item");
						names.add("create_flag_item");
						names.add("analyse_world");
						names.add("analyse_memory");
						names.add("debug");
						names.add("reloadconfig");
						names.add("reset_nether");
						names.add("allow_discord_chat");
						names.add("deny_discord_chat");
						names.add("transfer-account");
						names.add("recalc_claimblocks");
						names.add("recalc_shops");
						names.add("unban");
						names.add("setmayor");
						names.add("set_global_pos");
					}
					if(playerConfig.getInt("StaffRank") >= StaffRank.SENIOR_MOD.ordinal()) {
						names.add("sethelper");
						names.add("delete_public_tp");
						names.add("delete_region");
						names.add("resetlocation");
						names.add("setstaff");
					}
					if(playerConfig.getInt("StaffRank") >= StaffRank.MODERATOR.ordinal()) {
						names.add("user-search");
						names.add("bypass_alias_bans");
						names.add("townname");
					}
					
					List<String> applicableNames = new ArrayList<String>();
					for(String name : names) {
						if(name.length() > args[0].length()) {
							if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) {
								applicableNames.add(name);
							}
						}
					}
					return applicableNames;
				}
				
				if(args[0].equalsIgnoreCase("transfer-account")) {
					List<String> applicableNames = new ArrayList<String>();
					if(args.length == 2) {
						applicableNames.add("<old player>");
					}
					if(args.length == 3) {
						applicableNames.add("<new player>");
					}
					return applicableNames;
				}
				
				if(args[0].equalsIgnoreCase("reloadconfig")) {
					if(args.length == 2) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("town");
						applicableNames.add("player");
						applicableNames.add("travel");
						applicableNames.add("shop");
						applicableNames.add("yml");
						
						return applicableNames;
					}
					if(args.length >= 3) {
						if(args[1].equalsIgnoreCase("town")) {
							//list all town files
							List<String> applicableNames = new ArrayList<String>();
							//add all towns & aliases:
							List<String> aliases = new ArrayList<String>();
							if(DeadMC.TownFile.data().getString("Aliases") != null)
								aliases = DeadMC.TownFile.data().getStringList("Aliases");
							for(String town : DeadMC.TownFile.data().getStringList("Active")) {
								if(DeadMC.TownFile.data().getString("AliasOriginalName." + Town.townNameCased(args[2])) == null
										&& town.length() > args[2].length()
										&& args[2].toLowerCase().equalsIgnoreCase(town.toLowerCase().substring(0, args[2].length())))
									applicableNames.add(town);
							}
							for(String townAlias : aliases) {
								if(townAlias.length() > args[2].length()
										&& args[2].toLowerCase().equalsIgnoreCase(townAlias.toLowerCase().substring(0, args[2].length())))
									applicableNames.add(townAlias);
							}
							return applicableNames;
						}
						if(args[1].equalsIgnoreCase("player")) {
							List<String> applicableNames = new ArrayList<String>();
							//add all UUID's
							for(String uuid : DeadMC.PlayerFile.data().getStringList("Active")) {
								if(args[2].length() <= uuid.length() && args[2].toLowerCase().equalsIgnoreCase(uuid.toLowerCase().substring(0, args[2].length())))
									applicableNames.add(uuid);
							}

							//add all online player names
							for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
								if(args[2].length() <= onlinePlayer.getName().length() && args[2].toLowerCase().equalsIgnoreCase(onlinePlayer.getName().toLowerCase().substring(0, args[2].length())))
									applicableNames.add(onlinePlayer.getName());
							}
							
							applicableNames.add("<player name>");
							return applicableNames;
						}
						if(args[1].equalsIgnoreCase("travel")) {
							List<String> applicableNames = new ArrayList<String>();
							//add all public point files
							for(String tp : DeadMC.TravelFile.data().getStringList("Public")) {
								if(args[2].length() <= tp.length() && args[2].toLowerCase().equalsIgnoreCase(tp.toLowerCase().substring(0, args[2].length())))
									applicableNames.add(tp);
							}
							return applicableNames;
						}
						if(args[1].equalsIgnoreCase("shop")) {
							List<String> applicableNames = new ArrayList<String>();
							
							if(args.length == 3) {
								for(String town : DeadMC.TownFile.data().getStringList("Active")) {
									File folder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + town);
									if(folder.isDirectory() && town.length() > args[2].length() && args[2].toLowerCase().equalsIgnoreCase(town.toLowerCase().substring(0, args[2].length())))
										applicableNames.add(town);
								}
								return applicableNames;
							}
							
							if(args.length == 4) {
								String town = args[2];
								File[] allContents = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + town).listFiles();
								for(File file : allContents) {
									if(file.isDirectory()) continue;
									if(args[3].length() <= file.getName().length() && args[3].toLowerCase().equalsIgnoreCase(file.getName().toLowerCase().substring(0, args[3].length())))
										applicableNames.add(file.getName());
								}
								return applicableNames;
							}
						}
						if(args[1].equalsIgnoreCase("yml")) {
							List<String> applicableNames = new ArrayList<String>();
							File[] allContents = Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder().listFiles();
							for(File file : allContents) {
								if(file.isDirectory()) continue;
								if(args[2].length() <= file.getName().length() && args[2].toLowerCase().equalsIgnoreCase(file.getName().toLowerCase().substring(0, args[2].length())))
									applicableNames.add(file.getName());
							}
							return applicableNames;
						}
					}
				}
				
				if(args[0].equalsIgnoreCase("debug")) {
					//dmc debug <type>
					if(args.length == 2) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("spawning");
						applicableNames.add("explosion");
						applicableNames.add("restarter");
						applicableNames.add("discord");
						applicableNames.add("lbhead");
						applicableNames.add("chat");
						applicableNames.add("chunk_despawning");
						
						return applicableNames;
					}
				}
				
				if(args[0].equalsIgnoreCase("create_no_drop_item")
						|| args[0].equalsIgnoreCase("spawn_no_drop_item")) {
					//dmc create_no_drop_item <name ID>
					if(args.length == 2) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("<name ID>");
						
						return applicableNames;
					}
				}
				
				if(args[0].equalsIgnoreCase("delete_region")) {
					//dmc delete_region <town> <land>
					if(args.length == 2) {
						
						List<String> applicableNames = new ArrayList<String>();
						//add all towns & aliases:
						List<String> aliases = new ArrayList<String>();
						if(DeadMC.TownFile.data().getString("Aliases") != null)
							aliases = DeadMC.TownFile.data().getStringList("Aliases");
						for(String town : DeadMC.TownFile.data().getStringList("Active")) {
							//TownConfig townConfig = TownConfig.getConfig(town);
							if(/*townConfig.getString("Alias") == null &&*/ town.length() > args[1].length()
									&& args[1].toLowerCase().equalsIgnoreCase(town.toLowerCase().substring(0, args[1].length())))
								applicableNames.add(town);
						}
						for(String townAlias : aliases) {
							if(townAlias.length() > args[1].length()
									&& args[1].toLowerCase().equalsIgnoreCase(townAlias.toLowerCase().substring(0, args[1].length())))
								applicableNames.add(townAlias);
						}
						return applicableNames;
						
					}
					
					if(args.length == 3) {
						String originalTownName = Town.getOriginalTownName(args[1]);
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						
						List<String> regions = townConfig.getStringList("Regions");
						List<String> applicableNames = new ArrayList<String>();
						for(String regionID : regions) {
							if(regionID.length() > args[2].length()) {
								if(args[2].toLowerCase().equalsIgnoreCase(regionID.toLowerCase().substring(0, args[2].length()))) {
									applicableNames.add(regionID);
								}
							}
						}
						return applicableNames;
					}
				}
				
				if(args[0].equalsIgnoreCase("delete_public_tp")) {
					if(args.length == 2) {
						
						List<String> publicPoints = DeadMC.TravelFile.data().getStringList("Public");
						List<String> applicableNames = new ArrayList<String>();
						for(String name : publicPoints) {
							if(name.length() > args[1].length()) {
								if(name.contains(args[1].toLowerCase())) {
									applicableNames.add(name);
								}
							}
						}
						return applicableNames;
					}
				}
				
				if(args[0].equalsIgnoreCase("setstaff")) {
					if(args.length == 3) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("<leave blank to strip rank>");
						applicableNames.add("JUNIOR_MOD");
						applicableNames.add("MODERATOR");
						applicableNames.add("SENIOR_MOD");
						applicableNames.add("ADMINISTRATOR");
						return applicableNames;
					}
				}
				if(args[0].equalsIgnoreCase("sethelper")) {
					if(args.length == 3) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("<leave blank to strip helper rank>");
						applicableNames.add("<type anything to set as helper>");
						return applicableNames;
					}
				}
				
				if(args[0].equalsIgnoreCase("villagers")) {
					if(args.length == 2) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("set");
						return applicableNames;
					}
					if(args.length == 3) {
						List<String> blocks = new ArrayList<String>();
						for(Material block : Material.values()) {
							blocks.add(block.toString());
						}
						
						List<String> applicableNames = new ArrayList<String>();
						for(String block : blocks) {
							if(block.toLowerCase().contains(args[2].toLowerCase())) {
								applicableNames.add(block);
							}
						}
						return applicableNames;
					}
					if(args.length == 4) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("<type to set a market COIN value>");
						applicableNames.add("use_market_value");
						return applicableNames;
					}
				}
				
				if(args[0].equalsIgnoreCase("lbhead")) {
					if(args.length == 2) {
						List<String> applicableNames = new ArrayList<String>();
						for(Leaderboard leaderboard : Leaderboard.values()) {
							if(leaderboard.toString().toLowerCase().contains(args[1].toLowerCase())) {
								applicableNames.add(leaderboard.toString());
							}
						}
						return applicableNames;
					}
					if(args.length == 3) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("<place 1-10>");
						return applicableNames;
					}
					if(args.length == 4) {
						List<String> applicableNames = new ArrayList<String>();
						applicableNames.add("<title - eg. Zombie Killers>");
						return applicableNames;
					}
				}
				
				if(args.length == 2 && args[0].equalsIgnoreCase("townname")) {
					List<String> towns = DeadMC.TownFile.data().getStringList("Active");
					
					List<String> applicableNames = new ArrayList<String>();
					for(String town : towns) {
						String displayName = Town.getTownDisplayName(town);
						if(displayName.toLowerCase().contains(args[1].toLowerCase())) {
							applicableNames.add(displayName);
						}
					}
					return applicableNames;
				}
				if(args.length == 2 && (args[0].equalsIgnoreCase("setmayor") || args[0].equalsIgnoreCase("setcoins"))) {
					List<String> towns = DeadMC.TownFile.data().getStringList("Active");
					
					List<String> applicableNames = new ArrayList<String>();
					for(String town : towns) {
						if(town.toLowerCase().contains(args[1].toLowerCase())) {
							applicableNames.add(town);
						}
					}
					return applicableNames;
				}
				if(args.length == 2 && args[0].equalsIgnoreCase("playername")) {
					List<String> names = new ArrayList<String>();
					for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						if(!onlinePlayer.getName().equalsIgnoreCase(player.getName())) {
							names.add(onlinePlayer.getName()); //add normal names
							PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
							if(onlineConfig.getString("Name") != null) {
								names.add(Chat.stripColor(onlineConfig.getString("Name"))); //add nick names if exist
							}
						}
					}
					
					List<String> applicableNames = new ArrayList<String>();
					for(String name : names) {
						if(name.length() > args[1].length()) {
							if(args[1].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[1].length()))) {
								applicableNames.add(name);
							}
						}
					}
					
					return applicableNames;
				}
				
				if(args.length == 3) {
					List<String> names = new ArrayList<String>();
					if(args[0].equalsIgnoreCase("playername")
							|| args[0].equalsIgnoreCase("townname")) {
						names.add("<new name>");
					}
					if(args[0].equalsIgnoreCase("setmayor")) {
						names.add("<new mayor>");
					}
					if(args[0].equalsIgnoreCase("setcoins")) {
						names.add("<new amount>");
					}
					return names;
				}
				
			}
			
		}
		
		if(command.getName().equalsIgnoreCase("unpunish")) {
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				names.add("<ID>");
				return names;
			}
			if(args.length == 2) {
				List<String> names = new ArrayList<String>();
				names.add("<reason>");
				names.add("was a mistake");
				return names;
			}
		}
		
		if(command.getName().equalsIgnoreCase("unignore")) {
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
				List<String> ignoredUUIDs = new ArrayList<String>();
				if(playerConfig.getString("Ignored") != null)
					ignoredUUIDs = playerConfig.getStringList("Ignored");
				
				for(String uuid : ignoredUUIDs) {
					names.add(Bukkit.getOfflinePlayer(UUID.fromString(uuid)).getName());
					PlayerConfig ignoredConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
					if(ignoredConfig.getString("Name") != null)
						names.add(Chat.stripColor(ignoredConfig.getString("Name")));
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
		}
		
		if(command.getName().equalsIgnoreCase("lookup") ||
				command.getName().equalsIgnoreCase("setname") ||
				command.getName().equalsIgnoreCase("ignore") ||
				command.getName().equalsIgnoreCase("report") ||
				command.getName().equalsIgnoreCase("punish") ||
				command.getName().equalsIgnoreCase("message") || command.getName().equalsIgnoreCase("r") || command.getName().equalsIgnoreCase("sc") || command.getName().equalsIgnoreCase("tc") || command.getName().equalsIgnoreCase("msg") || command.getName().equalsIgnoreCase("w") || command.getName().equalsIgnoreCase("m") || command.getName().equalsIgnoreCase("mail")) {
			
			// /mail
			// and /lookup
			
			if((alias.equalsIgnoreCase("r")
					|| alias.equalsIgnoreCase("sc")
					|| alias.equalsIgnoreCase("tc"))
					&& args.length > 1) {
				List<String> names = new ArrayList<String>();
				names.add("<message>");
				return names;
			}
			
			if(args.length == 1) {
				List<String> names = new ArrayList<String>();
				if(command.getName().equalsIgnoreCase("lookup")) {
					names.add("<player>");
					names.add("<keyword>");
				}
				for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
					if(!onlinePlayer.getName().equalsIgnoreCase(player.getName())) {
						names.add(onlinePlayer.getName()); //add normal names
						String recordedName = PlaceHolders.playersName.get(onlinePlayer.getUniqueId().toString());
						if(recordedName != null && !recordedName.equalsIgnoreCase(onlinePlayer.getName())) {
							names.add(Chat.stripColor(recordedName)); //add nick names if exist
						}
					}
				}
				
				List<String> applicableNames = new ArrayList<String>();
				for(String name : names) {
					if(name.length() > args[0].length()) {
						if(args[0].toLowerCase().equalsIgnoreCase(name.toLowerCase().substring(0, args[0].length()))) { //use to have precise start
							applicableNames.add(name);
						}
					}
				}
				
				return applicableNames;
			}
			
			if(args.length == 2
					&& command.getName().equalsIgnoreCase("setname")) {
				List<String> names = new ArrayList<String>();
				names.add("<new name>");
				return names;
			}
		}
		
		
		return null;
	}
	
	
}
