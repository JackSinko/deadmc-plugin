package com.deadmc.DeadAPImaven;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.deadmc.DeadAPImaven.Task.TaskStep;

public class Market {
	private DeadMC plugin;
	public Market(DeadMC plugin) {
		this.plugin = plugin;
	}
//	
//	public Map<String, Integer> confirmCheckout = new HashMap<String, Integer>(); //playerName, shopID
//	public static Map<String, ArrayList<Integer>> marketInfoSignsClicked = new HashMap<String, ArrayList<Integer>>(); //playerName, shopID
//	public List<String> confirmSell = new ArrayList<String>(); //playerName
//	
//	public enum MarketShop {
//		Lumberjack,
//		Bakery,
//		Music_Store,
//		Building_Supplies,
//		Mining_Stall,
//		Seed_Stall,
//		Free_Range_Farm,
//		Redstone_Specialist,
//		Blacksmith
//	};
//	
//	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
//		
//		Player player = null;
//		if(sender instanceof Player)
//			player = (Player) sender;
//		
//		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
//		
//	    if(commandLabel.equalsIgnoreCase("se")) {
//
//	    	// creates the product ID for item in hand
//	    	
//    		if(!player.isOp()) {
//    			player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "Developer only command.");
//    			return false;
//    		}
//    		
//    		if(args.length == 0) {
//    			player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "/s <shop ID> <product ID> <cost to buy> <cost to sell>");
//    			return false;
//    		}
//	    
//    		DeadMC.MarketFile.data().set(Integer.parseInt(args[0]) + "." + Integer.parseInt(args[1]) + ".Material", player.getInventory().getItemInMainHand().getType().toString());
//    		DeadMC.MarketFile.data().set(Integer.parseInt(args[0]) + "." + Integer.parseInt(args[1]) + ".Buy", Integer.parseInt(args[2]));
//    		DeadMC.MarketFile.data().set(Integer.parseInt(args[0]) + "." + Integer.parseInt(args[1]) + ".Sell", Integer.parseInt(args[3]));
//    		DeadMC.MarketFile.save();
//    		
//    		player.sendMessage(ChatColor.GOLD + "[DeadMC] " + ChatColor.WHITE + "Created product.");
//	    	
//	    }
//	    
//	    
//		if(commandLabel.equalsIgnoreCase("sell")) { //sell 
//	    	if(args.length == 3) {
//	    		
//	    		if(!confirmSell.contains(player.getName())) {
//	        		if(Chat.isInteger(args[0]) == true && Chat.isInteger(args[1]) == true && ((Chat.isInteger(args[2]) == true && Integer.parseInt(args[2]) >= 0) || args[2].equalsIgnoreCase("all")) ) {
//	        			
//	        			int shopID = Integer.parseInt(args[0]);
//	        			int productID = Integer.parseInt(args[1]);
//	        			
//						Material material = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//						int sellPrice = DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Sell");
//						
//	        			int amount = 0;
//	        			if(args[2].equalsIgnoreCase("all")) {
//	        			    for(ItemStack stack : player.getInventory().getContents()) {
//	        			    	if(stack != null) {
//	        			    		ItemStack newStack = new ItemStack(material, stack.getAmount());
//	        			    		if(stack.equals(newStack)) {
//	        			    			amount += stack.getAmount();
//	        			    		}
//	        			    	}
//	        			    }
//	        			} else {
//	        				amount = Integer.parseInt(args[2]);
//	        			}
//						
//	        			if(DeadMC.MarketFile.data().getString("" + shopID) != null) {
//	        				if(DeadMC.MarketFile.data().getString(shopID + "." + productID) != null) {
//	        					
//	        					if(marketInfoSignsClicked.get(player.getName()) != null && marketInfoSignsClicked.get(player.getName()).contains((Object)shopID)) {
//	        						
//		        					ItemStack itemstack = new ItemStack(material, amount);
//		        					int totalSalePrice = new Integer(sellPrice * amount);
//		        					if(player.getInventory().containsAtLeast(itemstack, amount)) {
//
//			    						confirmSell.add(player.getName());
//			    						playerConfig.set("Temp.shopID", shopID);
//			    						playerConfig.set("Temp.productID", productID);
//			    						playerConfig.set("Temp.amount", amount);
//			    						playerConfig.save();
//    			    					
//		        						player.sendMessage("");
//		        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Sell " + ChatColor.GOLD + amount + ChatColor.WHITE + " x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " for " + ChatColor.GOLD + Economy.convertCoins(totalSalePrice) + ChatColor.WHITE + "?");
//			    				    	player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Type " + ChatColor.GOLD + ChatColor.BOLD + "/sell confirm" + ChatColor.WHITE + " to continue.");
//			    						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "The confirmation session will expire in 15 seconds.");
//			    						player.sendMessage("");
//			    						
//			    						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
//			    							@Override
//			    							public void run() {
//			    							    Player player = null; if((sender instanceof Player)) { player = (Player)sender; }
//			    							    
//			    							    if(confirmSell.contains(player.getName())) {
//			    							    	player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.RED + "Confirmation session expired.");
//			    							    	confirmSell.remove(player.getName());
//			    							    	playerConfig.set("Temp", null);
//			    							    	playerConfig.save();
//			    							    }
//			    							    
//			    							}
//			    						}, 300L);
//		        						
//		        					} else {
//		        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "You don't have " + ChatColor.GOLD + amount + ChatColor.WHITE + " x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " in your inventory therefore you cannot sell this.");
//		        					}
//		        					
//	        					} else {
//	        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "To sell to the " + MarketShop.values()[shopID-1].toString().replace("_", " ") + " market shop, " + ChatColor.ITALIC + "first" + ChatColor.WHITE + " find and click a " + ChatColor.AQUA + "[Market]" + ChatColor.WHITE + " sign with shop ID " + ChatColor.GOLD + shopID + ChatColor.WHITE + ".");
//	        					}
//	        					
//	        				} else {
//	        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "This shop doesn't have a product with ID " + productID + ".");
//	        				}
//	        			} else {
//	        				player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "There is no market shop with ID " + shopID + ".");
//	        			}
//	        		} else {
//	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Each variable can only contain numbers (or all for amount). Use:");
//	            		player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.GOLD + "/sell <shop ID> <product ID> <amount/all>");
//	        		}
//	    		} else {
//	    			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.RED + "Please wait for the confirmation session to expire.");
//	    		}
//	    		
//	    	} else { //args length != 3
//	    		
//	    		if(args.length == 1) {
//	    			if(args[0].equalsIgnoreCase("confirm")) {
//	    				if(confirmSell.contains(player.getName())) {
//	    					
//	    					int shopID = playerConfig.getInt("Temp.shopID");
//	    					int productID = playerConfig.getInt("Temp.productID");
//	    					int amount = playerConfig.getInt("Temp.amount");
//	    					
//	    					Material material = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//							int sellPrice = DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Sell");
//							
//	    					ItemStack itemstack = new ItemStack(material, amount);
//	    					
//	        				if(player.getInventory().containsAtLeast(itemstack, amount)) {
//
//	        					int totalSalePrice = sellPrice * amount;
//	        					
//	        					player.getInventory().removeItem(itemstack); player.updateInventory();
//	        					Economy.giveCoins(player.getUniqueId(), totalSalePrice);
//	        					
//	        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Successfully sold " + ChatColor.GOLD + amount + ChatColor.WHITE + " x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " for " + ChatColor.GOLD + Economy.convertCoins(totalSalePrice) + ChatColor.WHITE + ".");
//	        					
//	        					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GREEN + "Sold " + ChatColor.GOLD + amount + ChatColor.GREEN + " x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.GREEN + " for " + ChatColor.GOLD + Economy.convertCoins(totalSalePrice));
//	        					
//	        					confirmSell.remove(player.getName());
//	        					playerConfig.set("Temp", null);
//	        					playerConfig.save();
//	        					
//	        					//TASK
//	        					if(shopID == 1 && (productID > 6 && productID < 13) && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.SELL_LOGS))
//	        						TaskManager.stepTask(player.getUniqueId());
//	    						
//	    					} else {
//	    						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "You don't have " + ChatColor.GOLD + amount + ChatColor.WHITE + " x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " in your inventory therefore you cannot sell this.");
//	    					}
//	        				
//	    				} else {
//	    					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.RED + "There is nothing to confirm.");
//	    				}
//	    			} else {
//	            		player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/sell <shop ID> <product ID> <amount/all>");
//	    			}
//	    		} else {
//	    			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/sell <shop ID> <product ID> <amount/all>");
//	    		}
//	    	}
//	    }
//		
//		
//		
//		// cart
//        // cart list - all shops with non empty carts
//        // cart add <shop id> <product id> <amount>
//        // cart remove item <shop id> <product id> <amount>
//        // cart empty <shop id>/all
//        // visit a checkout sign to purchase items in a cart
//        
//        if(commandLabel.equalsIgnoreCase("cart")) {
//        	if(args.length == 0) { // '/cart'
//        	
//        		player.sendMessage("");
//        		player.sendMessage(ChatColor.YELLOW + "=============== Shopping Cart ===============");
//        		player.sendMessage(ChatColor.GOLD + "/cart add" + ChatColor.WHITE + " - Add an item.");
//        		player.sendMessage(ChatColor.GOLD + "/cart remove" + ChatColor.WHITE + " - Remove an item.");
//        		player.sendMessage(ChatColor.GOLD + "/cart list" + ChatColor.WHITE + " - A list of your active shopping carts.");
//        		player.sendMessage(ChatColor.GOLD + "/cart view" + ChatColor.WHITE + " - View the items in a shopping cart.");
//        		player.sendMessage(ChatColor.GOLD + "/cart empty" + ChatColor.WHITE + " - Clear all items in a shopping cart.");
//        		player.sendMessage(ChatColor.WHITE + "Visit a shop " + ChatColor.AQUA + "[Checkout]" + ChatColor.WHITE + " sign to buy the items in a cart.");
//        		player.sendMessage(ChatColor.YELLOW + "===========================================");
//        		player.sendMessage("");
//        		
//        	} else if(args.length >= 1) {
//        		
//        		if(args[0].equalsIgnoreCase("add")) {
//        			if(args.length == 4) {
//    	        		if(Chat.isInteger(args[1]) == true && Chat.isInteger(args[2]) == true && (Chat.isInteger(args[3]) == true && Integer.parseInt(args[3]) >= 0)) {
//    	        			
//    	        			int shopID = Integer.parseInt(args[1]);
//    	        			int productID = Integer.parseInt(args[2]);
//    	        			int amount = Integer.parseInt(args[3]);
//    	        			Material material = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//    	        			int buyPrice = DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Buy");
//    	        			
//    	        			int totalBuyPrice = buyPrice * amount;
//    	        			
//    	        			if(DeadMC.MarketFile.data().getString("" + shopID) != null) {
//    	        				if(DeadMC.MarketFile.data().getString(shopID + "." + productID) != null) {
//    	        					
//    	        					addItemToCart(player.getUniqueId(), shopID, productID, amount);
//    	        					player.sendMessage("");
//    	        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.GREEN + "Added " + ChatColor.GOLD + amount + ChatColor.WHITE + " x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " (Costs " + ChatColor.GOLD + Economy.convertCoins(totalBuyPrice) + ChatColor.WHITE + ").");
//    	        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/cart view " + ChatColor.GREEN + shopID + ChatColor.WHITE + " to view your " + MarketShop.values()[shopID-1].toString().replace("_", " ") + " cart.");
//    	        					player.sendMessage("");
//    	        					
//    	        					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GREEN + "Added " + ChatColor.GOLD + amount + ChatColor.WHITE + " x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " (Costs " + ChatColor.GOLD + Economy.convertCoins(totalBuyPrice) + ChatColor.WHITE + ")");
//    	        					
//    	        					//TASK
//    	        					if(shopID == 9 && productID == 9 && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.ADD_IRON_SWORD_TO_CART))
//    	        						TaskManager.stepTask(player.getUniqueId());
//    	        					
//    	        				} else {
//    	        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "This shop doesn't have a product with ID " + productID + ".");
//    	        				}
//    	        			} else {
//    	        				player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "There is no market shop with ID " + shopID + ".");
//    	        			}
//    	        		} else {
//    	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/cart add <shop ID> <product ID> <amount>");
//    	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Where shop ID, product ID and amount are numbers only.");
//    	        		}
//        			} else {
//	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/cart add <shop ID> <product ID> <amount>");
//        			}
//        		} else
//        		
//        		if(args[0].equalsIgnoreCase("list")) {
//        			if(args.length == 1) { // '/cart list'
//        				
//        				if(playerConfig.getIntegerList("Carts") != null) { //player has at least 1 cart
//        					
//        					List<Integer> shops = playerConfig.getIntegerList("Carts.Shops");
//        					
//        					player.sendMessage("");
//        	        		player.sendMessage(ChatColor.YELLOW + "========== List of your shopping carts ==========");
//        	        		player.sendMessage("You have " + ChatColor.GOLD + shops.size() + ChatColor.WHITE + " shopping carts.");
//        	        		player.sendMessage("");
//    	        			for(int shopID : shops) {
//    	        				List<Integer> products = playerConfig.getIntegerList("Carts." + shopID + ".Products");
//    	        				player.sendMessage(ChatColor.BOLD + MarketShop.values()[shopID-1].toString().replace("_", " ") + ChatColor.WHITE + " (Shop ID: " + ChatColor.GREEN + shopID + ChatColor.WHITE + "):");
//    	        				player.sendMessage("    Items in cart: " + ChatColor.GOLD + products.size());
//    	        				player.sendMessage("    Cost of cart: " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(getCostOfCart(player.getUniqueId(), shopID)));
//        	        			player.sendMessage("");
//    	        			}
//    	        			player.sendMessage("Use " + ChatColor.GOLD + "/cart view " + ChatColor.GREEN + "<shopID>" + ChatColor.WHITE + " to view cart details.");
//        	        		player.sendMessage(ChatColor.YELLOW + "=============================================");
//        	        		player.sendMessage("");
//        	        		
//        				} else {
//        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "You currently have no carts in use.");
//        				}
//        			} else {
//	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/cart list" + ChatColor.WHITE + " to view your carts.");
//        			}
//        		} else
//        		
//        		if(args[0].equalsIgnoreCase("view")) {
//        			if(args.length == 2) {
//        				
//        				if(playerConfig.getIntegerList("Carts") != null) { //player has at least 1 cart
//        					List<Integer> shopList = playerConfig.getIntegerList("Carts.Shops");
//        					if(Chat.isInteger(args[1]) == true) {
//            					int shopID = Integer.parseInt(args[1]);
//            					if(shopList.contains((Object)shopID)) {
//            						
//            						List<Integer> products = playerConfig.getIntegerList("Carts." + shopID + ".Products");
//            						
//                					player.sendMessage("");
//                	        		player.sendMessage(ChatColor.YELLOW + "========== Shopping cart for " + MarketShop.values()[shopID-1].toString().replace("_", " ") + " ==========");
//                	        		player.sendMessage(ChatColor.WHITE + "You have " + ChatColor.GOLD + products.size() + ChatColor.WHITE + " products in this cart (ShopID = " + ChatColor.GREEN + shopID + ChatColor.WHITE + ").");
//                	        		for(int productID : products) {
//                						int amount = playerConfig.getInt("Carts." + shopID + "." + productID);
//                						Material material = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//                						
//                						player.sendMessage("");
//                						player.sendMessage(ChatColor.WHITE + "- Product ID: " + ChatColor.GREEN + productID);
//                						String eachCost = "";
//                						if(amount > 1) eachCost = new String(" (" + ChatColor.GOLD + Economy.convertCoins(DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Buy")) + ChatColor.WHITE + " each)");
//                						player.sendMessage(ChatColor.WHITE + "     " + ChatColor.BOLD + amount + ChatColor.WHITE + "x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " = " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Buy") * amount) + ChatColor.WHITE + eachCost);
//            	        			}
//                	        		player.sendMessage("");
//                	        		player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/cart" + ChatColor.WHITE + " for help managing the cart.");
//                	        		player.sendMessage(ChatColor.WHITE + "The total cost of this cart is " + ChatColor.GOLD + Economy.convertCoins(getCostOfCart(player.getUniqueId(), shopID)) + ChatColor.WHITE + ".");
//                	        		player.sendMessage(ChatColor.WHITE + "Visit the " + MarketShop.values()[shopID-1].toString().replace("_", " ") + " " + ChatColor.AQUA + "[Checkout]" + ChatColor.WHITE + " sign to buy these items.");
//                	        		player.sendMessage(ChatColor.YELLOW + "==============================================");
//                	        		player.sendMessage("");
//                	        		
//            					} else {
//                 					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "The cart for this shop is empty.");
//                 				}
//        					} else {
//        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "'" + args[1] + "' is not a shop ID! This must be a number.");
//        					}
//        				} else {
//        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "You currently have no carts in use!");
//        				}
//        			} else {
//        				player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/cart view <shop ID>" + ChatColor.WHITE + " to view cart details.");
//        			}
//        		} else
//        			
//        		if(args[0].equalsIgnoreCase("remove")) { //cart remove <shop id> <product id> <amount>/all
//        			if(args.length == 4) {
//        				
//    	        		if(Chat.isInteger(args[1]) == true && Chat.isInteger(args[2]) == true) {
//    	        			if(Chat.isInteger(args[3]) == true || args[3].equalsIgnoreCase("all")) {
//    	        				
//	    	        			int shopID = Integer.parseInt(args[1]);
//	    	        			int productID = Integer.parseInt(args[2]);
//	    	        			
//	    	        			if(DeadMC.MarketFile.data().getString("" + shopID) != null) {
//	    	        				if(DeadMC.MarketFile.data().getString(shopID + "." + productID) != null) {
//	    	        					if(playerConfig.getString("Carts.Shops") != null) {
//	    	        						
//	    	        						if(playerConfig.getString("Carts." + shopID) != null) {
//		    	        						
//	    	        							if(playerConfig.getString("Carts." + shopID + "." + productID) != null) {
//			    	        						
//	    	        								int amountToRemove;
//	    	        								if(args[3].equalsIgnoreCase("all"))
//	    	        									amountToRemove = playerConfig.getInt("Carts." + shopID + "." + productID);
//	    	        								else
//	    	        									amountToRemove = Integer.parseInt(args[3]);
//				    	    	        			if(amountToRemove > playerConfig.getInt("Carts." + shopID + "." + productID))
//				    	    	        				amountToRemove = playerConfig.getInt("Carts." + shopID + "." + productID);
//
//				    	    	        			Material material = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//				    	    	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Removed " + ChatColor.GOLD + amountToRemove + ChatColor.WHITE + " x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " from your cart.");
//		    	    	        					
//				    	    	        			
//				    	    	        			//update file & lists
//				    	    	        			if(amountToRemove == playerConfig.getInt("Carts." + shopID + "." + productID)) {
//				    	    	        				//if removing all:
//				    	    	        				
//				    	    	        				playerConfig.set("Carts." + shopID + "." + productID, null);
//				    	    	        				
//				    	    	        				// update product list
//				    	    	        				List<Integer> shops = playerConfig.getIntegerList("Carts.Shops");
//				    	    	        				List<Integer> products = playerConfig.getIntegerList("Carts." + shopID + ".Products");
//				    	    	        				
//				    	    	        				products.remove((Object)productID);
//				    									if(products.size() == 0) {
//				    										//if no more products
//				    										if((shops.size()-1) == 0) {
//				    											//if no more active carts
//				    											playerConfig.set("Carts", null);
//				    										} else {
//				    											playerConfig.set("Carts." + shopID, null);
//				    											shops.remove((Object)shopID);
//				    											playerConfig.set("Carts.Shops", shops);
//				    										}
//				    									} else playerConfig.set("Carts." + shopID + ".Products", products);
//				    									
//				    	    	        			} else playerConfig.set("Carts." + shopID + "." + productID, playerConfig.getInt("Carts." + shopID + "." + productID) - amountToRemove);
//				    	    	        			
//				    	    	        			playerConfig.save();
//				    	    	        			
//				    	    	        			
//			    	        					} else {
//			    	        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "That product is not in your cart.");
//			    	        					}
//		    	        					} else {
//		    	        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Your cart for this shop is empty.");
//		    	        					}
//	    	        					} else {
//	    	        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "You don't have any active carts.");
//	    	        					}
//	    	        				} else {
//	    	        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "This shop doesn't have a product with ID " + productID + ".");
//	    	        				}
//	    	        			} else {
//	    	        				player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "There is no market shop with ID " + shopID + ".");
//	    	        			}
//    	        			} else {
//        	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: "+ ChatColor.GOLD + "/cart remove <shop ID> <product ID> <amount>/all");
//        	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Where shop ID, product ID and amount are numbers only.");
//    	        			}
//    	        		} else {
//    	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: "+ ChatColor.GOLD + "/cart remove <shop ID> <product ID> <amount>/all");
//    	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Where shop ID, product ID and amount are numbers only.");
//    	        		}
//        			} else {
//        				player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: "+ ChatColor.GOLD + "/cart remove <shop ID> <product ID> <amount>/all");
//        			}
//        		} else
//        			
//        		if(args[0].equalsIgnoreCase("empty") || args[0].equalsIgnoreCase("clear")) { //cart empty <shop id>
//        			if(args.length == 2) {
//        				
//    	        		if(Chat.isInteger(args[1]) == true) {
//    	        			int shopID = Integer.parseInt(args[1]);
//    	        			
//    	        			if(DeadMC.MarketFile.data().getString("" + shopID) != null) {
//    	        				if(playerConfig.getString("Carts") != null) {
//    	        					
//    	        					if(playerConfig.getString("Carts." + shopID) != null) {
//    	        						
//    	        						playerConfig.set("Carts." + shopID, null);
//	    	        					
//    	        						List<Integer> shops = playerConfig.getIntegerList("Carts.Shops");
//    	        						if((shops.size()-1) == 0) {
//											//if no more active carts
//    	        							playerConfig.set("Carts", null);
//										} else {
//											playerConfig.set("Carts." + shopID, null);
//											shops.remove((Object)shopID);
//											playerConfig.set("Carts.Shops", shops);
//										}
//    	        						
//    	        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "This shopping cart is now empty.");
//    	        						playerConfig.save();
//	    	        					
//    	        					} else {
//    	        						player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Your cart for this shop is already empty.");
//    	        					}
//    	        				} else {
//    	        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Your cart for this shop is already empty.");
//    	        				}
//            				} else { 
//            					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "There is no market shop with ID " + shopID + ".");
//            				}
//    	        		} else {
//    	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: "+ ChatColor.GOLD + "/cart empty <shop ID>");
//    	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Where shop ID can only be a number.");
//    	        		}
//        			} else {
//	        			player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Use: "+ ChatColor.GOLD + "/cart empty <shop ID>");
//        			}
//        		} else {
//        			
//        			//unknown command
//        			player.sendMessage(ChatColor.YELLOW + "[Cart] " + ChatColor.WHITE + "Unknown command '" + args[0] + "'.");
//        			player.sendMessage(ChatColor.YELLOW + "[Cart] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/cart" + ChatColor.WHITE + " for help.");
//        			
//        		}
//        	}
//        	
//        }
//	    
//		return true;		
//	}
//	
//	@EventHandler
//	public void onMarketSignCreate(SignChangeEvent event) {
//		Player player = event.getPlayer();
//		
//		if((event.getLine(0).equalsIgnoreCase("[Market]") || event.getLine(0).equalsIgnoreCase("[Checkout]")) && !player.isOp()) {
//			player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Players aren't permitted to create market shops. Create a chest shop with " + ChatColor.GOLD + "/shop" + ChatColor.WHITE + ".");
//			event.setCancelled(true);
//			return;
//		}
//		
//		if(event.getLine(0).equalsIgnoreCase("[Market]")) {
//			int shopID = Integer.parseInt(event.getLine(1));
//			int productID = Integer.parseInt(event.getLine(2));
//			event.setLine(0, "[Market]");
//			event.setLine(1, "Shop ID: " + shopID);
//			event.setLine(2, "Product ID: " + productID);
//			event.setLine(3, "CLICK FOR INFO");
//		}
//		
//		if(event.getLine(0).equalsIgnoreCase("[Checkout]")) {
//			int shopID = Integer.parseInt(event.getLine(1));
//			event.setLine(0, "[Checkout]");
//			event.setLine(1, "Click here to");
//			event.setLine(2, "purchase items.");
//			event.setLine(3, "Shop ID: " + shopID);
//		}
//		
//	}
//	
//	@EventHandler
//	public void onMarketSignClick(final PlayerInteractEvent event) {
//		Player player = event.getPlayer();
//		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
//		
//		if(event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
//			if(event.getClickedBlock().getState() instanceof Sign) {
//				Sign sign = (Sign) event.getClickedBlock().getState();
//				
//				
//				if(sign.getLine(0).equalsIgnoreCase("[Checkout]")) {
//					int shopID = Integer.parseInt(sign.getLine(3).substring(9));
//					
//					int costOfCart = getCostOfCart(player.getUniqueId(), shopID);
//					
//					if(confirmCheckout.containsKey(player.getName())) {
//
//						if(shopID == confirmCheckout.get(player.getName())) {
//							
//							if(Economy.getCoins(player.getUniqueId()) >= costOfCart) {
//								if(Players.getNumberOfFreeInventorySlots(player) >= getNumberOfFreeSlotsRequiredForCartCheckout(player.getUniqueId(), shopID)) {
//									
//									addCartToInventory(player, shopID);
//    	        					Economy.takeCoins(player.getUniqueId(), costOfCart);
//									
//									List<Integer> shops = playerConfig.getIntegerList("Carts.Shops");
//									playerConfig.set("Carts." + shopID, null);
//    	        					
//									
//									//SUCCESS
//									player.sendMessage("");
//									player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.GREEN + "Successfully checked out.");
//									player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "This shopping cart is now empty.");
//									player.sendMessage("");
//									
//
//    	        					//TASK
//    	        					if(shopID == 9 && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.PURCHASE_IRON_SWORD))
//    	        						TaskManager.stepTask(player.getUniqueId());
//									
//    	        					
//									shops.remove((Object)shopID);
//									if(shops.size() == 0)
//										playerConfig.set("Carts", null);
//									else
//										playerConfig.set("Carts.Shops", shops);
//									playerConfig.save();
//    	        					
//									confirmCheckout.remove(player.getName());
//									
//									
//								} else {
//									player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "You don't enough space in your inventory to hold this. You need " + ChatColor.GOLD + new Integer(getNumberOfFreeSlotsRequiredForCartCheckout(player.getUniqueId(), shopID) - Players.getNumberOfFreeInventorySlots(player)) + ChatColor.WHITE + " more empty slots to hold the items in your cart.");
//								}
//							} else {
//								player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "You don't have enough coins! You have " + ChatColor.GOLD + Economy.convertCoins(Economy.getCoins(player.getUniqueId())) + ChatColor.WHITE + " / " + ChatColor.GOLD + Economy.convertCoins(costOfCart) + ChatColor.WHITE + ".");
//							}
//							
//						} else {
//							player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Please wait for the confirmation session to expire.");
//						}
//						
//					} else {
//						
//						List<Integer> shops = playerConfig.getIntegerList("Carts.Shops");
//        				if(shops != null && shops.contains((Object)shopID)) {
//        						
//    						List<Integer> products = playerConfig.getIntegerList("Carts." + shopID + ".Products");
//    						
//        					player.sendMessage("");
//        	        		player.sendMessage(ChatColor.YELLOW + "========== Shopping cart for " + MarketShop.values()[shopID-1].toString().replace("_", " ") + " ==========");
//        	        		for(int productID : products) {
//        						int amount = playerConfig.getInt("Carts." + shopID + "." + productID);
//        						Material material = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//        						player.sendMessage(ChatColor.WHITE + "- Product ID: " + ChatColor.GREEN + productID);
//        						String eachCost = "";
//        						if(amount > 1) eachCost = new String(" (" + ChatColor.GOLD + Economy.convertCoins(DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Buy")) + ChatColor.WHITE + " each)");
//        						player.sendMessage(ChatColor.WHITE + "     " + ChatColor.BOLD + amount + ChatColor.WHITE + "x " + ChatColor.AQUA + ChatColor.BOLD + material.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " = " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Buy") * amount) + ChatColor.WHITE + eachCost);
//    	        			}
//        	        		player.sendMessage("");
//        	        		player.sendMessage(ChatColor.WHITE + "The total cost of this cart is " + ChatColor.GOLD + Economy.convertCoins(getCostOfCart(player.getUniqueId(), shopID)) + ChatColor.WHITE + ".");
//        	        		player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Click the " + ChatColor.AQUA + ChatColor.BOLD + "[Checkout]" + ChatColor.RED + ChatColor.BOLD + " sign again to confirm purchase.");
//        	        		player.sendMessage("The confirmation session will expire in 15 seconds.");
//        	        		player.sendMessage(ChatColor.YELLOW + "==============================================");
//        	        		player.sendMessage("");
//        	        		
//        	        		Chat.sendTitleMessage(player.getUniqueId(), ChatColor.WHITE + "Click the " + ChatColor.AQUA + ChatColor.BOLD + "[Checkout]" + ChatColor.WHITE + " sign again");
//        	        		
//        	        		confirmCheckout.put(player.getName(), shopID);
//        	        		
//    						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
//    							@Override
//    							public void run() {
//    							    Player player = event.getPlayer();
//    							    
//    							    if(confirmCheckout.containsKey(player.getName())) {
//    							    	player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.RED + "Confirmation session expired.");
//    							    	confirmCheckout.remove(player.getName());
//    							    }
//    							    
//    							}
//    						}, 300L);
//	    						
//        				} else {
//        					player.sendMessage(ChatColor.YELLOW + "[Market] " + ChatColor.WHITE + "Your cart for this shop is empty.");
//        				}
//        				
//					}
//				}
//				
//				
//				if(sign.getLine(0).equalsIgnoreCase("[Market]")) {
//					
//					int shopID = Integer.parseInt(sign.getLine(1).substring(9));
//					int productID = Integer.parseInt(sign.getLine(2).substring(12));
//					
//					Material item = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//					int buyPrice = DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Buy");
//					int sellPrice = DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Sell");
//					
//					String placeholder = Chat.getTitlePlaceholder(" " + MarketShop.values()[shopID-1].toString(), 38);
//					player.sendMessage("");
//					player.sendMessage(ChatColor.YELLOW + placeholder + " " + ChatColor.YELLOW + MarketShop.values()[shopID-1].toString().replace("_", " ") + " " + ChatColor.YELLOW + placeholder);
//					player.sendMessage("");
//					player.sendMessage(ChatColor.WHITE + "Price to " + ChatColor.BOLD + "BUY" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(buyPrice) + ChatColor.WHITE + " | Price to " + ChatColor.BOLD + "SELL" + ChatColor.WHITE + ": " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(sellPrice));
//					player.sendMessage("");
//					player.sendMessage(ChatColor.WHITE + "To sell " + ChatColor.AQUA + ChatColor.BOLD + item.toString().toLowerCase().replace("_", " ") + ChatColor.WHITE + " to this shop, use:");
//					player.sendMessage(ChatColor.GOLD + "    /sell " + ChatColor.GREEN + shopID + ChatColor.GOLD + "(shop ID) " + ChatColor.GREEN + productID + ChatColor.GOLD + "(product ID) <amount>/all");
//					player.sendMessage(ChatColor.WHITE + "To add this item to your cart, use:");
//					player.sendMessage(ChatColor.GOLD + "    /cart add " + ChatColor.GREEN + shopID + ChatColor.GOLD + "(shop ID) " + ChatColor.GREEN + productID + ChatColor.GOLD + "(product ID) <amount>");
//					player.sendMessage(ChatColor.YELLOW + "======================================");
//					player.sendMessage("");
//					
//					//TASK
//					if(shopID == 1 && (productID > 6 && productID < 13) && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.CLICK_LOG_SALE_SIGN))
//						TaskManager.stepTask(player.getUniqueId());
//					
//					//TASK
//					if(shopID == 9 && productID == 9 && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.CLICK_IRON_SWORD_SALE_SIGN))
//						TaskManager.stepTask(player.getUniqueId());
//					
//					
//					// 5 minutes to use shop sign:
//					
//					ArrayList<Integer> activeShops;
//					if(marketInfoSignsClicked.containsKey(player.getName()))
//						activeShops = marketInfoSignsClicked.get(player.getName());
//					else activeShops = new ArrayList<Integer>();
//					
//					if(!activeShops.contains((Object)shopID)) {
//						activeShops.add(shopID); //add shop to list
//						marketInfoSignsClicked.put(player.getName(), activeShops); //update list
//						
//						//remove list in 60 seconds if not used:
//						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
//							@Override public void run() {
//								
//								ArrayList<Integer> activeShops = marketInfoSignsClicked.get(player.getName());
//								if(activeShops.contains((Object)shopID)) {
//									activeShops.remove((Object)shopID);
//									marketInfoSignsClicked.put(player.getName(), activeShops); //update list
//								}
//								
//							}
//						}, 6000L); // 5 minutes
//						
//					}
//			
//					
//				}
//			}
//		}
//	}
//	
//	public void addItemToCart(UUID uuid, int shopID, int productID, int amount) {
//		
//		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
//
//		List<Integer> shops = new ArrayList<Integer>();
//		List<Integer> products = new ArrayList<Integer>();
//		
//		// update shop and product lists
//		if(playerConfig.getString("Carts") != null) //if player has ANY carts
//			shops = playerConfig.getIntegerList("Carts.Shops");
//		if(playerConfig.getString("Carts." + shopID) != null) //if player has a cart for specific shop (shopID)
//			products = playerConfig.getIntegerList("Carts." + shopID + ".Products");
//		else {
//			shops.add(shopID);
//			playerConfig.set("Carts.Shops", shops);
//		}
//		
//		// set amount:
//		if(playerConfig.getString("Carts." + shopID + "." + productID) != null) { //if product is in cart already
//			playerConfig.set("Carts." + shopID + "." + productID, playerConfig.getInt("Carts." + shopID + "." + productID) + amount);
//		} else {
//			products.add(productID);
//			playerConfig.set("Carts." + shopID + ".Products", products); //add product to list of products
//			
//			playerConfig.set("Carts." + shopID + "." + productID, amount); //set product amount
//		}
//		
//		playerConfig.save();
//		
//	}
//	
//	public int getCostOfCart(UUID uuid, int shopID) {
//		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
//
//		//check if cart is empty
//		if(playerConfig.getString("Carts." + shopID) == null)
//			return 0;
//		
//		//list of products in player cart for shop with ID 'shopID'
//		List<Integer> products = playerConfig.getIntegerList("Carts." + shopID + ".Products");
//		
//		int cost = 0;
//		for(int productID : products) {
//			int buyPrice = DeadMC.MarketFile.data().getInt(shopID + "." + productID + ".Buy");
//			int amount = playerConfig.getInt("Carts." + shopID + "." + productID);
//			int productCost = buyPrice*amount;
//			cost += productCost;
//		}
//		return cost;
//		
//	}
//	
//	public static int getNumberOfFreeSlotsRequiredForCartCheckout(UUID uuid, int shopID) {
//		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
//
//		List<Integer> productsInCart = playerConfig.getIntegerList("Carts." + shopID + ".Products");
//
//		int numberOfSlots = 0;
//		for(int productID : productsInCart) {
//			int amount = playerConfig.getInt("Carts." + shopID + "." + productID);
//			Material item = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//			
//			numberOfSlots += Math.ceil((double) amount / item.getMaxStackSize());
//		}
//		return numberOfSlots;
//		
//	}
//	
//
//	public static void addCartToInventory(Player player, int shopID) {
//		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
//
//		List<Integer> productsInCart = playerConfig.getIntegerList("Carts." + shopID + ".Products");
//		
//		for(int productID : productsInCart) {
//			
//			int amount = playerConfig.getInt("Carts." + shopID + "." + productID);
//			Material item = Material.getMaterial(DeadMC.MarketFile.data().getString(shopID + "." + productID + ".Material"));
//			
//			while(amount > item.getMaxStackSize()) {
//				player.getInventory().setItem(Players.getEmptyInventorySlot(player), new ItemStack(item, item.getMaxStackSize()));
//				amount -= item.getMaxStackSize();
//			}
//			if(amount > 0)
//				player.getInventory().setItem(Players.getEmptyInventorySlot(player), new ItemStack(item, amount));
//			
//		}
//		player.updateInventory();
//	}
//	
//	
//	
}
