package com.deadmc.DeadAPImaven;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

// expanded tool from https://github.com/espidev/ebackup/tree/master/src/main/java/dev/espi/ebackup

public class Backup implements CommandExecutor, Listener {
	private static DeadMC plugin;
	public Backup(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		Player player = null;
		if(sender instanceof Player) {
			player = (Player) sender;
		}
		
		if(commandLabel.equalsIgnoreCase("backup")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(player.isOp() || (playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= Admin.StaffRank.ADMINISTRATOR.ordinal())) {
				if(args.length == 0) {
					
					Player finalPlayer = player;
					Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
						finalPlayer.sendMessage("");
						finalPlayer.sendMessage(ChatColor.YELLOW + "=========================================");
						File[] smallBackupsDir = new File(Bukkit.getServer().getWorldContainer() + "/backups", "SMALL").listFiles();
						File[] bigBackupsDir = new File(Bukkit.getServer().getWorldContainer() + "/backups", "BIG").listFiles();
						finalPlayer.sendMessage(ChatColor.WHITE + "" + smallBackupsDir.length + " small backups saved | " + bigBackupsDir.length + " big backups saved");
						
						long totalSizeSmall = 0;
						for(File file : smallBackupsDir)
							totalSizeSmall += file.length();
						long totalSizeBig = 0;
						for(File file : bigBackupsDir)
							totalSizeBig += file.length();
						DecimalFormat df = new DecimalFormat("####0.00");
						double totalSizeSmallMB = totalSizeSmall / 1048576.0; //convert to MB
						double totalSizeBigGB = totalSizeBig / 1073741824.0; //convert to GB
						
						finalPlayer.sendMessage(ChatColor.WHITE + "Total size of small backups: " + ChatColor.BOLD + df.format(totalSizeSmallMB) + ChatColor.WHITE + "mb");
						finalPlayer.sendMessage(ChatColor.WHITE + "Total size of big backups: " + ChatColor.BOLD + df.format(totalSizeBigGB) + ChatColor.WHITE + "gb");
						finalPlayer.sendMessage("");
						finalPlayer.sendMessage(ChatColor.WHITE + "Last " + historyToKeep + " non-small backup attempts:");
						for(int backup = 0; backup <= historyToKeep; backup++) {
							if(DeadMC.RestartFile.data().getString("Backup.History." + backup) == null)
								break;
							finalPlayer.sendMessage(ChatColor.YELLOW + " > " + ChatColor.WHITE + DeadMC.RestartFile.data().getString("Backup.History." + backup));
						}
						finalPlayer.sendMessage(ChatColor.YELLOW + "=========================================");
						finalPlayer.sendMessage("");
					});
					
				} else {
					try {
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							Backup.createBackup(BackupType.valueOf(args[0].toUpperCase()), BackupReason.MANUAL);
						});
					} catch(IllegalArgumentException e) {
						player.sendMessage(ChatColor.YELLOW + "[Backup] " + ChatColor.RED + "Unknown backup type '" + args[0] + "'.");
					}
				}
			}
		}
		
		return true;
	}
	
	public static long startTime = 0; //if 0, there is no backup started. In epoch time (ms)
	public static AtomicBoolean isInBackup = new AtomicBoolean(false); //lock
	
	//in order of importance for automatic
	public enum BackupType {
		BIG,
		SMALL,
		PLUGINS
	}
	
	// Zip compression level (0-9)
	// Increase it to reduce file size, but backups will be more CPU intensive and take longer
	private static Map<BackupType, Integer> compressionLevel = new HashMap<BackupType, Integer>() {{
		put(BackupType.SMALL, 0);
		put(BackupType.BIG, 4);
		put(BackupType.PLUGINS, 2);
	}};
	private static Map<BackupType, Integer> maxBackups = new HashMap<BackupType, Integer>() {{
		put(BackupType.SMALL, 168); //every hour, make backup. Keep for 7 days.
		put(BackupType.BIG, 7); //every 16 hours, get full backup. Keep for 5 days
		put(BackupType.PLUGINS, 2); //only keep 2 from restarts
	}};
	//in hours, set to 0 to disable automatic backup
	private static Map<BackupType, Integer> timeBetweenBackups = new HashMap<BackupType, Integer>() {{
		put(BackupType.SMALL, 1);
		put(BackupType.BIG, 16);
		put(BackupType.PLUGINS, 0);
	}};
	private static Map<BackupType, Integer> timeSinceLastBackup = new HashMap<BackupType, Integer>() {{
		put(BackupType.SMALL, 0);
		put(BackupType.BIG, 0);
		put(BackupType.PLUGINS, 0);
	}};

	// delete old backups (when limit reached)
	private static void checkMaxBackups(BackupType type) {
		int backups = 0;
		SortedMap<Long, File> m = new TreeMap<>(); // oldest files to newest
		
		File backupPath = new File(Bukkit.getServer().getWorldContainer(), "backups");
		File backupPathType = new File(backupPath + "/" + type.toString());
		
		for(File file : backupPathType.listFiles()) {
			if(file.getName().endsWith(".zip")) {
				backups++;
				m.put(file.lastModified(), file);
			}
		}

		// delete old backups
		while (backups-- >= maxBackups.get(type)) {
			m.get(m.firstKey()).delete();
			m.remove(m.firstKey());
		}
	}
	
	private static String backupTime() {
		long finishTime = System.currentTimeMillis();
		DecimalFormat df = new DecimalFormat("####0.00");
		double timeInSeconds = (finishTime-startTime) / 1000.0; //convert to seconds
		return new String("" + df.format(timeInSeconds) + " seconds");
	}
	
	private static String getFileSize(File file, boolean gb) {
		long fileSize = file.length();
		DecimalFormat df = new DecimalFormat("####0.00");
		if(gb) {
			double sizeToGB = fileSize / 1073741824.0; //convert to GB
			return new String(df.format(sizeToGB) + "gb");
		} else {
			//mb
			double sizeToMB = fileSize / 1048576.0; //convert to MB
			return new String(df.format(sizeToMB) + "mb");
		}
	}
	
	private static 	int historyToKeep = 10;
	private static void addBackupHistory(String data) {
		for(int backup = historyToKeep; backup >= 0; backup--) {
			if(backup == 0)
				DeadMC.RestartFile.data().set("Backup.History." + backup, data);
			else {
				DeadMC.RestartFile.data().set("Backup.History." + backup, DeadMC.RestartFile.data().getString("Backup.History." + (backup-1)));
			}
		}
		DeadMC.RestartFile.save();
	}
	
	public enum BackupReason {
		SCHEDULED,
		MANUAL,
		PRE_RESTART
	}

	public static void loadTimer() {
		for(BackupType type : BackupType.values()) {
			File backupPath = new File(Bukkit.getServer().getWorldContainer(), "backups");
			if(!backupPath.exists())
				backupPath.mkdir();
			File backupPathType = new File(backupPath + "/" + type.toString());
			if(!backupPathType.exists())
				backupPathType.mkdir();
		}
		
		Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () -> {
			Chat.debug(ChatColor.YELLOW + "[Backup] " + ChatColor.WHITE + "Checking if backup is needed");
			if(!Restarter.timerHasStarted) { //dont backup if server is going to restart
				//update all times
				for(BackupType type : BackupType.values()) {
					if(timeBetweenBackups.get(type) > 0) {
						timeSinceLastBackup.replace(type, timeSinceLastBackup.get(type) + 1);
					}
				}
				//check for due backups
				boolean started = false;
				for(BackupType type : BackupType.values()) { //in order of importance
					if(timeBetweenBackups.get(type) > 0
							&& timeSinceLastBackup.get(type) == timeBetweenBackups.get(type)) {
						timeSinceLastBackup.replace(type, 0); //always replace for all
						if(!started) {
							started = true;
							createBackup(type, BackupReason.SCHEDULED);
						}
					}
				}
			}
		}, 60 * 60 * 20L,  60 * 60 * 20L); //every hour
	}
	
	// actually do the backup
	public static void createBackup(BackupType type, BackupReason reason) {
		startTime = System.currentTimeMillis();
		String reasonString = reason != BackupReason.SCHEDULED ? " (" + reason.toString().toLowerCase().replace("_", "-") + ")" : "";
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy @ HHmm");
		String fileName = formatter.format(new Date()) + reasonString;
		
		Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + "Starting " + type.toString() + " backup... (" + reason.toString().toLowerCase().replace("_", "-") + ")", true);
		if(isInBackup.get()) {
			//log error, backup is running!
			Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + ChatColor.RED + "Backup failed because there is already a backup running.", true);
			if(type != BackupType.SMALL) addBackupHistory(ChatColor.WHITE + fileName + ": " + ChatColor.DARK_RED + "FAILED" + ChatColor.WHITE + " - backup already running.");
			return;
		}
		
		// set true so you can handle multiple backups trying to start at once (log error)
		isInBackup.set(true);
		
		try {
			List<File> ignoredFiles = new ArrayList<File>();
			File currentWorkingDirectory = Bukkit.getServer().getWorldContainer();
			if(type == BackupType.SMALL) {
				currentWorkingDirectory = new File(Bukkit.getServer().getWorldContainer(), "plugins/DeadMC");
			} else if(type == BackupType.PLUGINS) {
				currentWorkingDirectory = new File(Bukkit.getServer().getWorldContainer(), "plugins");
			}
			
			//ignore files:
			List<String> filesToIgnore = DeadMC.RestartFile.data().getStringList("Backup.FilesToIgnore");
			for (String s : filesToIgnore) {
				ignoredFiles.add(new File(s));
			}

			// delete old backups
			checkMaxBackups(type);
			
			// zip
			int errors = 0; //number of skipped files
			File backupPath = new File(Bukkit.getServer().getWorldContainer(), "backups");
			FileOutputStream fos = new FileOutputStream(backupPath + "/" + type.toString() + "/" + fileName + ".zip");
			ZipOutputStream zipOut = new ZipOutputStream(fos);
			
			// set zip compression level
			zipOut.setLevel(compressionLevel.get(type));
			
			// backup worlds first
			if(type == BackupType.BIG) {
				for(World world : Bukkit.getWorlds()) {
					File worldFolder = world.getWorldFolder();
					
					String worldPath = Paths.get(currentWorkingDirectory.toURI()).relativize(Paths.get(worldFolder.toURI())).toString();
					if(worldPath.endsWith("/.")) {// 1.16 world folders end with /. for some reason
						worldPath = worldPath.substring(0, worldPath.length() - 2);
						worldFolder = new File(worldPath);
					}
					
					// check if world is in ignored list
					boolean skip = false;
					for(File file : ignoredFiles) {
						if(file.getCanonicalPath().equals(worldFolder.getCanonicalPath())) {
							skip = true;
							break;
						}
					}
					if(skip) continue;
					
					// manually trigger world save (needs to be run sync)
					AtomicBoolean saved = new AtomicBoolean(false);
					Bukkit.getScheduler().runTask(plugin, () -> {
						world.save();
						saved.set(true);
					});
					
					// wait until world save is finished
					while(!saved.get()) Thread.sleep(500);
					
					world.setAutoSave(false); // make sure autosave doesn't screw everything over
					
					Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + "Backing up world '" + world.getName() + "'...", true);
					errors += zipFile(worldFolder, worldPath, zipOut, ignoredFiles);
					Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + "  > " + ChatColor.GREEN + "'" + world.getName() + "' complete!", true);
					
					world.setAutoSave(true);
					
					// ignore in dfs
					ignoredFiles.add(worldFolder);
				}
			}
			
			// dfs all other files
			Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + "Backing up plugin data...", true);
			errors += zipFile(currentWorkingDirectory, "", zipOut, ignoredFiles);
			zipOut.close();
			fos.close();
			
			// upload to ftp/sftp
			//File file = new File(backupPath + "/" + type.toString() + "/" + fileName + ".zip");
			//Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + "Uploading to SFTP server...", true);
			//uploadSFTP(file, type);
			
			// if the upload is able to go smoothly, delete local backup
//			if(file.delete()) {
//				Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + " > Deleted local backup successfully.", true);
//			} else {
//				Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + ChatColor.RED + " > Unable to delete local backup!.", true);
//			}
			
			String fileSize = getFileSize(new File(backupPath + "/" + type.toString() + "/" + fileName + ".zip"), type == BackupType.BIG);
			Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + ChatColor.GREEN + "'" + type.toString() + "' backup complete!", true);
			Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + ChatColor.WHITE + fileName, true);
			Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + ChatColor.WHITE + fileSize + ChatColor.ITALIC + " in " + ChatColor.WHITE + backupTime(), true);
			if(type != BackupType.SMALL) {
				if(errors > 0) addBackupHistory(ChatColor.WHITE + fileName + ": " + ChatColor.RED + "SUCCESS w/ error" + ChatColor.WHITE + " - (" + fileSize + ChatColor.ITALIC + " in " + ChatColor.WHITE + backupTime()  + ") (" + ChatColor.RED + errors + " errors" + ChatColor.WHITE + ")");
				else addBackupHistory(ChatColor.WHITE + fileName + ": " + ChatColor.GREEN + "SUCCESS" + ChatColor.WHITE + " - (" + fileSize + ChatColor.ITALIC + " in " + ChatColor.WHITE + backupTime()  + ")");
			}
			
		} catch (Exception e) {
			Chat.logError(":head_bandage: **Error with " + type.toString() + " backup (" + reasonString + ")!** " + e.getMessage());
			Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + "  > " + ChatColor.RED + "Error! " + fileName + " - " + backupTime() + " - " + e.getMessage(), true);
			if(type != BackupType.SMALL) addBackupHistory(ChatColor.WHITE + fileName + ": " + ChatColor.DARK_RED + "FAILED" + ChatColor.WHITE + " - " + e.getMessage());
			e.printStackTrace();
		} finally {
			for(World world : Bukkit.getWorlds())
				world.setAutoSave(true); //turn auto save back on all worlds in case an error occurred
			
			// unlock
			isInBackup.set(false);
		}
	}
	
	// recursively compress files and directories
	// returns the number of errors
	private static int zipFile(File fileToZip, String fileName, ZipOutputStream zipOut, List<File> ignoredFiles) throws IOException {
		if(fileName.endsWith(".jar")) return 0;
		for(File file : ignoredFiles) { // return if it is ignored file
			if (file.getCanonicalPath().equals(fileToZip.getCanonicalPath())) return 0;
		}
		
		int errors = 0;
		
		// fix windows archivers not being able to see files because they don't support / (root) for zip files
		if (fileName.startsWith("/") || fileName.startsWith("\\")) {
			fileName = fileName.substring(1);
		}
		// make sure there won't be a "." folder
		if (fileName.startsWith("./") || fileName.startsWith(".\\")) {
			fileName = fileName.substring(2);
		}
		// truncate \. on windows (from the end of folder names)
		if (fileName.endsWith("/.") || fileName.endsWith("\\.")) {
			fileName = fileName.substring(0, fileName.length()-2);
		}
		
		if (fileToZip.isDirectory()) { // if it's a directory, recursively search
			if (fileName.endsWith("/")) {
				zipOut.putNextEntry(new ZipEntry(fileName));
			} else {
				zipOut.putNextEntry(new ZipEntry(fileName + "/"));
			}
			zipOut.closeEntry();
			File[] children = fileToZip.listFiles();
			for (File childFile : children) {
				errors += zipFile(childFile, fileName + "/" + childFile.getName(), zipOut, ignoredFiles);
			}
		} else { // if it's a file, store
			try {
				FileInputStream fis = new FileInputStream(fileToZip);
				ZipEntry zipEntry = new ZipEntry(fileName);
				zipOut.putNextEntry(zipEntry);
				byte[] bytes = new byte[1024];
				int length;
				while ((length = fis.read(bytes)) >= 0) {
					zipOut.write(bytes, 0, length);
				}
				fis.close();
			} catch (IOException e) {
				Chat.broadcastMessage(ChatColor.YELLOW + "[Backup] " + "  > " + ChatColor.RED + "Error backing up file '" + fileName + "' - backup will ignore this file - " + e.getMessage() + ".", true);
				errors++;
			}
		}
		
		return errors;
	}
	
//	private static void uploadSFTP(File file, BackupType type) throws JSchException, SftpException {
//		JSch jsch = new JSch();
//
//		String pass = "Zimaj1423";
//
//		String username = "";
//		String host = "";
//		int port = 22;
//		Session session = jsch.getSession(username, host, port);
//
//		// password auth (using password)
//		session.setPassword(pass);
//
//		session.setConfig("StrictHostKeyChecking", "no");
//		session.connect();
//
//		Channel channel = session.openChannel("sftp");
//		channel.connect();
//		ChannelSftp sftpChannel = (ChannelSftp) channel;
//		sftpChannel.put(file.getAbsolutePath(), "~/backups/"+ type.toString() + "/" + file.getName());
//		sftpChannel.exit();
//		session.disconnect();
//	}
}