package com.deadmc.DeadAPImaven;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Container;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.MultipleFacing;
import org.bukkit.block.data.Rail;
import org.bukkit.block.data.type.Bed;
import org.bukkit.block.data.type.Fence;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.block.data.type.Wall;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

public class Explosion implements Listener {
	private static DeadMC plugin;
	
	public Explosion(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static Boolean isExplodingProjectile(EntityType type) {
		return type == EntityType.ARROW
				|| type == EntityType.DRAGON_FIREBALL
				|| type == EntityType.FIREBALL
				|| type == EntityType.SPECTRAL_ARROW;
	}
	
	//blockstate contains all info like location, block type, rotation etc.
	public static HashMap<Location, Explosion> affectedBlocks = new HashMap<Location, Explosion>(); //all blocks that have been destroyed and are going to be restored
	public static HashMap<Location, Explosion> delayedBlocks = new HashMap<Location, Explosion>(); //the initial explosion has finished, although these blocks are waiting on surrounding explosions to finish
	public static HashMap<Location, TwoBlockTall> twoBlockTallBlocks = new HashMap<Location, TwoBlockTall>(); //always use bottom half for location
	
	//lists of all blocks affected by this explosion:
	private Location origin;
	private HashMap<Location, BlockState> nonDependentBlocks = new HashMap<Location, BlockState>();
	private HashMap<Location, BlockState> dependentBlocks = new HashMap<Location, BlockState>();
	private HashMap<Location, BlockState> blocksWaitingForCompletion = new HashMap<Location, BlockState>(); //delayed blocks that are waiting for this explosion to finish
	private Explosion(Location origin) {
		this.origin = origin;
	}
	
	private boolean canRemoveBlock(BlockState blockToTry) {
		//returns whether the block will be removed from explosion
		//for example, cannot remove a jukebox as it will pop out the disc inside
		//can not remove chests as they hold items
		//can not remove pistons as they could be moving
		
		int chanceOfBreaking = (int) (Math.random() * 100); // 0-99 (inclusive)
		return !affectedBlocks.containsKey(blockToTry.getLocation()) //if it hasn't already been affected
				&& !delayedBlocks.containsKey(blockToTry.getLocation()) //if it isn't waiting for another explosion
				&& !blockToTry.getType().isAir()
				&& ((blockToTry.getType() != Material.OBSIDIAN && blockToTry.getType() != Material.NETHERITE_BLOCK) || chanceOfBreaking < 25)
				&& blockToTry.getType() != Material.BEDROCK
				&& !blockToTry.getType().toString().contains("PISTON") //all pistons, moving etc.
				&& blockToTry.getType() != Material.NETHER_PORTAL
				&& !(blockToTry.getBlockData() instanceof Bed)
				&& !(blockToTry instanceof Container)
				//stair corners won't regenerate in right position if surrounding blocks have been destroyed from another explosion
				&& (!(blockToTry.getBlockData() instanceof Stairs stairs) || !hasSurroundingStairsThatHaveBeenAffected(blockToTry)); //therefore just don't remove stairs if has surrounding explosions
	}
	
	private static boolean isFallingBlock(Material type) {
		return type == Material.SAND
				|| type == Material.GRAVEL
				|| type.toString().contains("CONCRETE_POWDER");
	}
	
	public static boolean isDependentBlock(BlockState block) {
		Material type = block.getType();
		//non solid blocks, signs, pressure plates, doors, lanterns, bell,
		return !type.isAir()
				&& type != Material.FIRE
				&& (!type.isSolid()
				|| block instanceof org.bukkit.block.Sign
				|| type.toString().contains("ANVIL")
				|| type.toString().contains("CARPET")
				|| type.toString().contains("AMETHYST_BUD")
				|| type == Material.AMETHYST_CLUSTER
				|| type == Material.POINTED_DRIPSTONE
				|| isConnectingBlock(block.getBlockData()) //blocks like glass panes depend on surrounding blocks
				|| type == Material.LANTERN || type == Material.SOUL_LANTERN || type == Material.BELL
				|| (TwoBlockTall.isTwoBlocksTall(block) && TwoBlockTall.getTwoBlocksTall(block).bottomHalf.getLocation().equals(block.getLocation()))); //is bottom half of a 2-block-tall block
	}
	
	//ignores blocks if failed
	//use this only for low-stress things
	private HashMap<BlockFace, BlockState> getBlocksOnSide(BlockState state, boolean includeHeight) {
		HashMap<BlockFace, BlockState> surroundingBlocksToTry = new HashMap<BlockFace, BlockState>();
		if(includeHeight) {
			try { surroundingBlocksToTry.put(BlockFace.UP, state.getBlock().getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) {}
			try { surroundingBlocksToTry.put(BlockFace.DOWN, state.getBlock().getRelative(BlockFace.DOWN).getState()); } catch(RuntimeException e) {}
		}
		try { surroundingBlocksToTry.put(BlockFace.NORTH, state.getBlock().getRelative(BlockFace.NORTH).getState()); } catch(RuntimeException e) {}
		try { surroundingBlocksToTry.put(BlockFace.EAST, state.getBlock().getRelative(BlockFace.EAST).getState()); } catch(RuntimeException e) {}
		try { surroundingBlocksToTry.put(BlockFace.SOUTH, state.getBlock().getRelative(BlockFace.SOUTH).getState()); } catch(RuntimeException e) {}
		try { surroundingBlocksToTry.put(BlockFace.WEST, state.getBlock().getRelative(BlockFace.WEST).getState()); } catch(RuntimeException e) {}
		return surroundingBlocksToTry;
	}
	
	private List<BlockState> getSurroundingBlocksForced(BlockState state) throws RuntimeException {
		List<BlockState> surroundingBlocksToTry = new ArrayList<BlockState>();
		surroundingBlocksToTry.add(state.getBlock().getRelative(BlockFace.DOWN).getState());
		surroundingBlocksToTry.add(state.getBlock().getRelative(BlockFace.UP).getState());
		surroundingBlocksToTry.add(state.getBlock().getRelative(BlockFace.NORTH).getState());
		surroundingBlocksToTry.add(state.getBlock().getRelative(BlockFace.EAST).getState());
		surroundingBlocksToTry.add(state.getBlock().getRelative(BlockFace.SOUTH).getState());
		surroundingBlocksToTry.add(state.getBlock().getRelative(BlockFace.WEST).getState());
		return surroundingBlocksToTry;
	}
	private CompletableFuture<List<BlockState>> getSurroundingBlocks(BlockState state) {

//		//example of completable future:
		//https://www.callicoder.com/java-8-completablefuture-tutorial/
//		CompletableFuture<PlayerConfig> getConfigAsync = CompletableFuture.supplyAsync(() -> { //use supplyAsync() when you want to return something asynchronously
//			//get the playerConfig "some uuid" after 1 second asynchronously
//			try {
//				TimeUnit.SECONDS.sleep(1);
//			} catch (InterruptedException e) {
//				throw new IllegalStateException(e);
//			}
//			return PlayerConfig.getConfig(UUID.fromString("some uuid"));
////		}).thenApply(playerConfig -> { //use thenApply() to process the result from above in the same thread and return it. Use the Async method to perform the task in a new thread.
//			//Doing something with the Bukkit API? Make sure to run it with a RunTask (BukkitScheduler)
//			//Bukkit.getScheduler().runTask(plugin, () -> { }
//			//OR Bukkit.getScheduler().runTask(plugin, () -> doSomething());
//
////			return playerConfig; //return the player config
////		}).thenAcceptAsync(playerConfig -> { //or you can use thenAccept() to process the result from supplyAsync. Name the result what you like - eg. playerConfig is the result from supplyAsync
////			playerConfig.set("Test", 0):
////			playerConfig.save();
//		});
//
//		PlayerConfig playerConfig = getConfigAsync.get(); //waits for the
		
		CompletableFuture<List<BlockState>> cf = new CompletableFuture<List<BlockState>>();
		try {
			cf.complete(getSurroundingBlocksForced(state)); //complete async
		} catch(RuntimeException e) {
			//if the block fails to get async, get sync
			//Chat.debug("[Explosion] Failed to get surrounding block states for " + state.getType() + " asynchronously. Getting sync instead.");
			Bukkit.getScheduler().runTask(plugin, () -> { //NOTE: takes approx 40-50ms to start a task - a max of 500ms for entire explosion
				cf.complete(getSurroundingBlocksForced(state)); //complete sync
			});
		}
		return cf;
	}
	
	//gets the BlockState that is saved at the location
	private BlockState getSavedBlock(Location location) {
		if(nonDependentBlocks.containsKey(location)) return nonDependentBlocks.get(location);
		if(dependentBlocks.containsKey(location)) return dependentBlocks.get(location);
		if(blocksWaitingForCompletion.containsKey(location)) return blocksWaitingForCompletion.get(location);
		return null; //returns null if the explosion doesn't have a block saved at the given location
	}

	//gets if there were stairs around the block that have been destroyed by a different explosion
	private boolean hasSurroundingStairsThatHaveBeenAffected(BlockState block) {
		for(BlockState surroundingBlock : getBlocksOnSide(block, false).values()) {
			if(affectedBlocks.containsKey(surroundingBlock.getLocation()) || delayedBlocks.containsKey(surroundingBlock.getLocation())) {
				Explosion surroundingExplosion = affectedBlocks.containsKey(surroundingBlock.getLocation()) ? affectedBlocks.get(surroundingBlock.getLocation()) : delayedBlocks.get(surroundingBlock.getLocation());
				if(this != surroundingExplosion //explosions are different
						&& surroundingExplosion.getSavedBlock(surroundingBlock.getLocation()).getBlockData() instanceof Stairs) { //the surrounding block that is destroyed was a stair
					return true;
				}
			}
		}
		return false;
	}
	
	//run this async
	private void restoreLocation(BlockState stateBefore) {
		Location location = stateBefore.getLocation();
		
		//restore the top half at the same time
		if(twoBlockTallBlocks.containsKey(location)) {
			BlockState topHalf = twoBlockTallBlocks.get(location).topHalf;
			restoreLocation(topHalf);
			Bukkit.getScheduler().runTask(plugin, () -> affectedBlocks.remove(topHalf.getLocation())); //remove from the affectedBlocks list
			Bukkit.getScheduler().runTask(plugin, () -> twoBlockTallBlocks.remove(location));
		}
		
		if(isConnectingBlock(stateBefore.getBlockData())) {
			Bukkit.getScheduler().runTask(plugin, () -> {
				//just place the block so it automatically connects to other connecting blocks if around
				stateBefore.getBlock().setType(stateBefore.getType());
				connectBlock(stateBefore.getBlock());
			});
		} else {
			Bukkit.getScheduler().runTask(plugin, () -> stateBefore.update(true, !isFallingBlock(stateBefore.getType()))); //restore with the same block state data
		}

		Bukkit.getScheduler().runTask(plugin, () -> {
			location.getWorld().playEffect(location, Effect.CHORUS_FLOWER_GROW, 20, 20);
			location.getWorld().playEffect(location.add(0, 1, 0), Effect.SMOKE, 50, 20);
		});
	}
	
	private void connectBlock(Block block) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {

			if(block.getBlockData() instanceof MultipleFacing multipleFacing) { //fences, glass panes, iron bars
				HashMap<BlockFace, BlockState> blocksOnSide = getBlocksOnSide(block.getState(), false);
				for(BlockFace face : blocksOnSide.keySet()) {
					BlockState state = blocksOnSide.get(face);
					if(!state.getType().isSolid() || state.getBlockData() instanceof Wall) continue; //doesn't link to walls
					
					multipleFacing.setFace(face, true);
				}
				Bukkit.getScheduler().runTask(plugin, () -> block.setBlockData(multipleFacing)); //update the block data
			}
			
			if(block.getBlockData() instanceof Wall wall) { //fences, glass panes, iron bars
				HashMap<BlockFace, BlockState> blocksOnSide = getBlocksOnSide(block.getState(), false);
				for(BlockFace face : blocksOnSide.keySet()) {
					BlockState state = blocksOnSide.get(face);
					if(!state.getType().isSolid() || state.getBlockData() instanceof Fence) continue; //doesn't link to fences, although links to panes

					wall.setHeight(face, Wall.Height.TALL);
				}
				Bukkit.getScheduler().runTask(plugin, () -> block.setBlockData(wall)); //update the block data
			}
			
		});
	}
	//blocks that connect to other blocks and that don't need to save it's data. These blocks data will be reset on restore so they can connect again.
	public static boolean isConnectingBlock(BlockData block) {
		return block instanceof Fence //is instance of MultipleFacing
				|| block instanceof Rail
				|| block.getMaterial() == Material.IRON_BARS //is instance of MultipleFacing
				|| block.getMaterial().toString().contains("GLASS_PANE") //is instance of MultipleFacing
				|| block instanceof Wall;
	}

	public static void explode(Location origin, Boolean big, int effectSize, int timeUntilRestore) {
		Block block = origin.getBlock();
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			ArrayList<BlockState> blocksToTry = new ArrayList<BlockState>();
			try { blocksToTry.add(block.getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.DOWN).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.NORTH).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.EAST).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.SOUTH).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.WEST).getState()); } catch(RuntimeException e) { }
			
			try { blocksToTry.add(block.getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.UP).getRelative(BlockFace.NORTH).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.UP).getRelative(BlockFace.EAST).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.UP).getRelative(BlockFace.SOUTH).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.UP).getRelative(BlockFace.WEST).getState()); } catch(RuntimeException e) { }
			
			try { blocksToTry.add(block.getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.NORTH).getRelative(BlockFace.DOWN).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.EAST).getRelative(BlockFace.DOWN).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.SOUTH).getRelative(BlockFace.DOWN).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.WEST).getRelative(BlockFace.DOWN).getState()); } catch(RuntimeException e) { }
			
			try { blocksToTry.add(block.getRelative(BlockFace.NORTH).getRelative(BlockFace.NORTH).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.EAST).getRelative(BlockFace.EAST).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.SOUTH).getRelative(BlockFace.SOUTH).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.WEST).getRelative(BlockFace.WEST).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.NORTH).getRelative(BlockFace.NORTH).getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.EAST).getRelative(BlockFace.EAST).getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.SOUTH).getRelative(BlockFace.SOUTH).getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
			try { blocksToTry.add(block.getRelative(BlockFace.WEST).getRelative(BlockFace.WEST).getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
			
			if(big) {
				try { blocksToTry.add(block.getRelative(BlockFace.NORTH_EAST).getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
				try { blocksToTry.add(block.getRelative(BlockFace.NORTH_WEST).getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
				try { blocksToTry.add(block.getRelative(BlockFace.SOUTH_EAST).getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
				try { blocksToTry.add(block.getRelative(BlockFace.SOUTH_WEST).getRelative(BlockFace.UP).getState()); } catch(RuntimeException e) { }
				try { blocksToTry.add(block.getRelative(BlockFace.NORTH_EAST).getState()); } catch(RuntimeException e) { }
				try { blocksToTry.add(block.getRelative(BlockFace.NORTH_WEST).getState()); } catch(RuntimeException e) { }
				try { blocksToTry.add(block.getRelative(BlockFace.SOUTH_EAST).getState()); } catch(RuntimeException e) { }
				try { blocksToTry.add(block.getRelative(BlockFace.SOUTH_WEST).getState()); } catch(RuntimeException e) { }
			}
		
			explode(origin, blocksToTry, effectSize, timeUntilRestore);
		});
	}
	
	public static void explode(Location origin, List<BlockState> blocksToTry, int effectSize, int timeUntilRestore) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			long startTime = System.currentTimeMillis();
			
			//create new explosion
			Explosion explosion = new Explosion(origin);

			boolean debug = DeadMC.RestartFile.data().getString("DebugMessages.explosion") != null;
			
			for(BlockState blockToTry : blocksToTry) {
				
				//check each block to see if it should be destroyed, don't bother checking if can't remove
				//also checks if it's already been affected or is currently delayed
				if(!explosion.canRemoveBlock(blockToTry)) continue;
				//good to remove

				TwoBlockTall twoBlockTall = TwoBlockTall.getTwoBlocksTall(blockToTry);
				if(twoBlockTall != null) { //is 2-blocks-tall
					Bukkit.getScheduler().runTask(plugin, () -> explosion.twoBlockTallBlocks.put(twoBlockTall.bottomHalf.getLocation(), twoBlockTall));
					
					blockToTry = twoBlockTall.bottomHalf; //always use the bottom half
					//just add the top half as non-dependent block
					explosion.nonDependentBlocks.put(twoBlockTall.topHalf.getLocation(), twoBlockTall.topHalf);
					Bukkit.getScheduler().runTask(plugin, () -> affectedBlocks.put(twoBlockTall.topHalf.getLocation(), explosion));
				}
				
				//add a reference to the location and the block that will be affected
				BlockState finalBlockToTry = blockToTry;
				Bukkit.getScheduler().runTask(plugin, () -> affectedBlocks.put(finalBlockToTry.getLocation(), explosion));
				
				//check if the block depends on other blocks
				if(isDependentBlock(blockToTry)) {
					explosion.dependentBlocks.put(blockToTry.getLocation(), blockToTry); //add as a dependent block
				} else {
					explosion.nonDependentBlocks.put(blockToTry.getLocation(), blockToTry); //add as a non-dependent block
				}

				//check each surrounding block for depending blocks, we will update them after
				try {
					for(BlockState surroundingBlock : explosion.getSurroundingBlocks(blockToTry).get()) {
						if(!affectedBlocks.containsKey(surroundingBlock.getLocation()) //has not been affected, including this explosion
							&& !delayedBlocks.containsKey(surroundingBlock.getLocation()) //is not waiting
								&& !blocksToTry.contains(surroundingBlock) //not in this explosion
								&& isDependentBlock(surroundingBlock)) {
							
							TwoBlockTall twoBlockTallSurrounding = TwoBlockTall.getTwoBlocksTall(surroundingBlock);
							if(twoBlockTallSurrounding != null) { //is 2-blocks-tall
								Bukkit.getScheduler().runTask(plugin, () -> explosion.twoBlockTallBlocks.put(twoBlockTallSurrounding.bottomHalf.getLocation(), twoBlockTallSurrounding));
								
								surroundingBlock = twoBlockTallSurrounding.bottomHalf; //always use the bottom half
								//just add the top half as non-dependent block
								explosion.nonDependentBlocks.put(twoBlockTallSurrounding.topHalf.getLocation(), twoBlockTallSurrounding.topHalf);
								Bukkit.getScheduler().runTask(plugin, () -> affectedBlocks.put(twoBlockTallSurrounding.topHalf.getLocation(), explosion));
							}
							
							//the surrounding block could be depending on the block we're removing
							explosion.dependentBlocks.put(surroundingBlock.getLocation(), surroundingBlock); //add as a dependent block
							
							//add a reference to the location and the block that will be affected
							BlockState finalSurroundingBlock = surroundingBlock;
							Bukkit.getScheduler().runTask(plugin, () -> affectedBlocks.put(finalSurroundingBlock.getLocation(), explosion));
						}
					}
				} catch(InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}

			}
			
			//dont restore dependent blocks yet unless surrounding blocks have been restored or are apart of explosion
			
			//remove dependent blocks first
			for(BlockState state : explosion.dependentBlocks.values()) {
				Bukkit.getScheduler().runTask(plugin, () -> state.getBlock().setType(debug ? Material.YELLOW_STAINED_GLASS : Material.AIR));
			}
			//now remove all non-dependent blocks
			for(BlockState state : explosion.nonDependentBlocks.values()) {
				Bukkit.getScheduler().runTask(plugin, () -> state.getBlock().setType(debug ? Material.GLASS : Material.AIR));
			}
			
			//play effects (after blocks are removed)
			if(effectSize > 0) {
				Bukkit.getScheduler().runTask(plugin, () -> origin.getWorld().createExplosion(origin, effectSize, true, origin.getWorld().getName().contains("nether") ? true : false));
				if(origin.getWorld().getName().contains("nether")) return; //don't regenerate
			}
			
			//sort the non-dependent blocks based on y height
			List<BlockState> nonDependentSorted = new ArrayList<BlockState>(explosion.nonDependentBlocks.values());
			Collections.sort(nonDependentSorted, new Comparator<BlockState>() {
				@Override
				public int compare(BlockState block1, BlockState block2) {
					return Integer.compare(block1.getY(), block2.getY());
				}
			});
			
			Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
				//RESTORE BLOCKS
				
				float speed = 1300; //initial speed
				float modifier = 0.92f;
				float minSpeed = 90;
				
				//restore non-dependent blocks first
				for(BlockState block : nonDependentSorted) {
					if(TwoBlockTall.isTwoBlocksTall(block)) continue; //restore 2-block-tall blocks together
					
					explosion.restoreLocation(block);
					Bukkit.getScheduler().runTask(plugin, () -> affectedBlocks.remove(block.getLocation())); //remove from the affectedBlocks list
					
					if(speed <= minSpeed) speed = minSpeed;
					Tools.sleepThread(Math.round(speed));
					speed *= modifier;
				}
				
				//restore dependent blocks last
				for(BlockState block : explosion.dependentBlocks.values()) {
					//don't restore if surrounding blocks were involved in another explosion
					if(explosion.okayToRestore(block, debug)) {
						explosion.restoreLocation(block);
					}
					Bukkit.getScheduler().runTask(plugin, () -> affectedBlocks.remove(block.getLocation())); //remove from affectedBlocks, even if added to delayed
					
					if(speed <= minSpeed) speed = minSpeed;
					Tools.sleepThread(Math.round(speed));
					speed *= modifier;
				}
				
				//restore delayed (always dependent) blocks from other explosions
				for(BlockState block : explosion.blocksWaitingForCompletion.values()) {
					if(explosion.okayToRestore(block, debug)) { //check again in case more
						explosion.restoreLocation(block);
					}
					Bukkit.getScheduler().runTask(plugin, () -> delayedBlocks.remove(block.getLocation())); //remove from delayedBlocks
					
					if(speed <= minSpeed) speed = minSpeed;
					Tools.sleepThread(Math.round(speed));
					speed *= modifier;
				}

			}, timeUntilRestore);
			
			long endTime = System.currentTimeMillis();
			Chat.debug("Explosion took " + (endTime - startTime) + " ms.");
		});
	}
	
	private boolean okayToRestore(BlockState state, boolean debug) {
		//check if surrounding blocks are apart of another explosion
		try {
			for(BlockState surroundingBlock : getSurroundingBlocks(state).get()) {
				Location location = surroundingBlock.getLocation();
	
				if(affectedBlocks.containsKey(location)) { //surrounding block is a part of an explosion
					Explosion surroundingExplosion = affectedBlocks.get(location);
					if(!delayedBlocks.containsKey(state.getLocation()) //location isn't already delayed
							&& surroundingExplosion != this) { //surrounding explosion is not this explosion
						
						//delay the restoration as it could be depending on the other block
						surroundingExplosion.blocksWaitingForCompletion.put(state.getLocation(), state);
						//add location to delayed list and reference this explosion
						Bukkit.getScheduler().runTask(plugin, () -> delayedBlocks.put(state.getLocation(), surroundingExplosion)); //affecting a public static variable, run sync
	
						if(debug) Bukkit.getScheduler().runTask(plugin, () -> state.getBlock().setType(Material.CYAN_STAINED_GLASS));
						
						return false;
					}
				}
			}
		} catch(InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@EventHandler
	public void hangingEntityBreak(HangingBreakEvent event) {
		if(event.getCause() == HangingBreakEvent.RemoveCause.EXPLOSION || event.getCause() == HangingBreakEvent.RemoveCause.EXPLOSION || event.getCause() == HangingBreakEvent.RemoveCause.PHYSICS) {
			Entity entity = event.getEntity();
			if(entity.getType() == EntityType.PAINTING) {
				event.setCancelled(true);
				return;
			}
			if(entity instanceof ItemFrame itemFrame) {
				Location location = entity.getLocation().getBlock().getRelative(itemFrame.getAttachedFace()).getLocation();
				//Chat.debug("  " + location.getBlock().getType());
				//Chat.debug("  is in explosion - " + (affectedBlocks.containsKey(location) || delayedBlocks.containsKey(location)));
				if(affectedBlocks.containsKey(location) || delayedBlocks.containsKey(location)) {
					//Chat.debug("    > Cancelled");
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onFallBlock(EntityChangeBlockEvent event) {
		//cancel blocks falling if was due to explosion
		if(event.getEntity() instanceof FallingBlock fallingBlock) {
			Location locationToCheck = fallingBlock.getLocation().getBlock().getRelative(BlockFace.DOWN).getLocation();
			if(affectedBlocks.containsKey(locationToCheck)
			|| delayedBlocks.containsKey(locationToCheck)) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void armorStandDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof ArmorStand armorStand
		&& (event.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION
		|| event.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION
		|| event.getCause() == EntityDamageEvent.DamageCause.PROJECTILE)) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockExplode(BlockExplodeEvent event) {
		Location location = event.getBlock().getLocation();
		if(!location.getWorld().getName().contains("nether")) {
			event.setCancelled(true);
			List<BlockState> states = new ArrayList<BlockState>();
			for(Block block : event.blockList())
				states.add(block.getState());
			Explosion.explode(location, states, 1, 100);
		}
	}
	
	// TESTING only:
	@EventHandler
	public void onBlockBreak(PlayerInteractEvent event) {
		if(event.getPlayer().isOp() && event.getAction() == Action.LEFT_CLICK_BLOCK && event.getPlayer().getInventory().getItemInMainHand().getType() == Material.DIAMOND_AXE) {
			event.setCancelled(true);
			explode(event.getClickedBlock().getLocation(), false, 0, 100);
		}
	}
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if(player.isOp()) {
			if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block block = event.getClickedBlock();
				if(player.getInventory().getItemInMainHand() != null && player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_AXE && !block.getType().isAir()) {
					String isDependent = isDependentBlock(block.getState()) ? "is dependent!" : "non-dependent";
					String isConnecting = isConnectingBlock(block.getBlockData()) ? "is connecting!" : "non-connecting";
					String stairData = block.getBlockData() instanceof Stairs stairs ? " - " + stairs.getShape() : "";
					String multipleFacingData = block.getBlockData() instanceof MultipleFacing multipleFacing ? " - " + multipleFacing.getFaces() : "";
					TwoBlockTall twoBlockTall = TwoBlockTall.getTwoBlocksTall(block.getState());
					String twoBlockTallData = twoBlockTall != null ? "Top half Y: " + twoBlockTall.topHalf.getY() + " - Bottom half Y: " + twoBlockTall.bottomHalf.getY() : "";
					Chat.sendTitleMessage(player.getUniqueId(), "" + block.getType() + " - " + isDependent + " - " + isConnecting + stairData + multipleFacingData + twoBlockTallData);
					//Material preMaterial = block.getType();
					//block.setType(Material.AIR);
					//block.setType(preMaterial);
				}
			}
		}
	}
}
