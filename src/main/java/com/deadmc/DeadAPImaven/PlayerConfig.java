package com.deadmc.DeadAPImaven;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PlayerConfig extends YamlConfiguration {
	
	public static Map<UUID, PlayerConfig> configs = new HashMap<UUID, PlayerConfig>();
	
	public static PlayerConfig getConfig(Player player) {
		return getConfig(player.getUniqueId());
	}
	
	public static PlayerConfig getConfig(OfflinePlayer player) {
		UUID uuid = player.getUniqueId();
		if(!new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "players" + File.separator + uuid.toString() + ".yml").exists()) {
			return null;
		}
		return getConfig(uuid);
	}
	
	private boolean isCorrupt = false;
	private BufferedWriter logWriter = null;
	public BufferedWriter getLogFile() {
		if(logWriter == null) {
			ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin"));
			try {
				File directory = new File(Bukkit.getWorldContainer(), "player config logs" + File.separator + uuid.toString());
				directory.mkdirs();
				File logFile = new File(Bukkit.getWorldContainer(), "player config logs" + File.separator + uuid.toString() + File.separator + zonedDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE) + ".txt");
				logWriter = new BufferedWriter(new FileWriter(logFile, true));
			} catch(Exception e) {
				Chat.logError("(" + uuid.toString() + ")" + e.getMessage());
				return null;
			}
		}
		return logWriter;
	}
	
	public void writeToLog(String string) {
		if(DeadMC.RestartFile.data().getString("LogPlayerConfigActions") == null) return;
		
		try {
			ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin"));
			getLogFile().append("[" + zonedDateTime.format(DateTimeFormatter.ISO_LOCAL_TIME) + "] " + string + "\n");
		} catch(Exception e) {
			//Chat.logError("(" + uuid.toString() + ")" + e.getMessage());
		}
		
		checkIfFileCorrupted();
	}
	
	public void writeStackTraceToLog() {
		if(DeadMC.RestartFile.data().getString("LogPlayerConfigActions") == null) return;
		
		for(StackTraceElement stack : Thread.currentThread().getStackTrace()) {
			if(stack.toString().contains("DeadAPImaven") && !stack.toString().contains("PlayerConfig")) {
				writeToLog("   -> " + stack.toString());
			}
		}
		
	}
	
	@Override
	public void set(@NotNull String path, @Nullable Object value) {
		//check that config is not reloading (as it loads in all the data)
		//Chat.debug("Attempting to set " + path + " to " + value);
		if(!isReloading) writeToLog("Attempting to set '" + path + "' to '" + value + "'");
		try {
			super.set(path, value);
			//Chat.debug(" > Success!");
		} catch(Exception e) {
			writeToLog(" > ERROR SETTING!");
		}
	}
	
	private void closeLogWriter() {
		if(DeadMC.RestartFile.data().getString("LogPlayerConfigActions") == null) return;
		
		try {
			logWriter.close();
			logWriter = null;
		} catch(Exception e) {
			Chat.logError("(" + uuid.toString() + ")" + e.getMessage());
		}
		
		checkIfFileCorrupted();
	}
	
	private void checkIfFileCorrupted() {
		if(DeadMC.RestartFile.data().getString("LogPlayerConfigActions") == null) return;
		
		if(isCorrupt) return; //is already corrupt
		if(isReloading) return;

		File file = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "players" + File.separator + uuid.toString() + ".yml");
		
		if(getString("DonateRank") == null && DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
			isCorrupt = true;
			Chat.logError("PlayerConfig for " + Bukkit.getOfflinePlayer(uuid).getName() + " (" + uuid.toString() + ") looks corrupted! Check player config logs.");
			writeToLog("[!] Config is corrupt!");
		}
	}
	
	public static PlayerConfig getConfig(UUID uuid) {
		synchronized(configs) { //'synchronized' allows only one thread to access configs at a time
			
			//get stack trace to figure out what is calling it
			//Bukkit.getConsoleSender().sendMessage("" + Thread.currentThread().getStackTrace()[2]);
			
			if(configs.containsKey(uuid)) {
				configs.get(uuid).checkIfFileCorrupted();

				return configs.get(uuid);
			}
			
			PlayerConfig config = new PlayerConfig(uuid);
			configs.put(uuid, config);
			
			config.writeToLog("Saved the config to memory.");

			Chat.debug(ChatColor.LIGHT_PURPLE + "[PlayerConfig] " + ChatColor.RESET + "Added " + uuid.toString() + " - " + configs.size());
			
			return config;
			
		}
	}
	
	private File file = null;
	private Object saveLock = new Object();
	private UUID uuid;
	
	public PlayerConfig(UUID uuid) {
		super(); //'super' calls parent class constructor (YamlConfiguration)
		file = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "players" + File.separator + uuid.toString() + ".yml");
		this.uuid = uuid;
		reload();
	}
	
	@SuppressWarnings("unused")
	private PlayerConfig() {
		uuid = null;
	}
	
	private boolean isReloading = false;
	
	private void reload() {
		isReloading = true;
		writeToLog("Reloading YML");
		writeStackTraceToLog();
		synchronized(saveLock) {
			try {
				load(file);
				writeToLog(" > Successfully reloaded");
			} catch(Exception e) {
				writeToLog(" [!] Failed reload!");
			}
			closeLogWriter();
		}
		isReloading = false;
	}
	
	public void save() {
		writeToLog("Saving to file");
		writeStackTraceToLog();
		synchronized(saveLock) {
			try {
				save(file);
				writeToLog(" > Successfully saved");
				closeLogWriter();
			} catch(Exception e) {
				e.printStackTrace();
				Chat.logError("There was an error saving a player file (" + uuid.toString() + ")." + (e.getMessage() == null ? "No error message." : " Could not save data for: " + e.getMessage().replace("No JavaBean properties found in ", "")));
				writeToLog(" [!] Failed save! (see server log for full error)");
				closeLogWriter();
			}
		}
	}
	
	public void discard() {
		writeToLog("Discarding from memory");
		writeStackTraceToLog();
		synchronized(configs) {
			if(Bukkit.getPlayer(uuid) == null) { //don't remove online players
				discard(true);
				writeToLog(" > Successfully discarded");
			} else {
				writeToLog(" [!] Failed discard (player is online still)");
			}
		}
	}
	
	public void discard(boolean force) {
		synchronized(configs) {
			if(force) { //don't remove online players
				writeToLog("Force discarded the config");
				writeStackTraceToLog();
				closeLogWriter();
				configs.remove(uuid);
				Chat.debug(ChatColor.LIGHT_PURPLE + "[PlayerConfig] " + ChatColor.RESET + "Removed " + uuid.toString() + " - " + configs.size());
			}
		}
	}
	
}
