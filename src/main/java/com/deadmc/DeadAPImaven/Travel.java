package com.deadmc.DeadAPImaven;

import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutionException;

import com.deadmc.DeadAPImaven.Towns.Town;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.Directional;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.event.world.PortalCreateEvent.CreateReason;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import io.papermc.lib.PaperLib;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import com.deadmc.DeadAPImaven.Admin.StaffRank;
import com.deadmc.DeadAPImaven.Task.TaskStep;
import org.bukkit.util.Vector;

public class Travel implements CommandExecutor, Listener {
	
	// Get reference to main class
	private static DeadMC plugin;
	
	public Travel(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static HashMap<String, String> isTravelling = new HashMap<String, String>(); //player, travel point name

	public static HashMap<String, ZonedDateTime> recentlyTravelled = new HashMap<String, ZonedDateTime>();
	
	public static HashMap<String, Integer> travelEnabledPlayers = new HashMap<String, Integer>(); //name, random int
	
	public static HashMap<String, String> travelToPlayer = new HashMap<String, String>(); //player to travel to, player attempting the travel
	
	public enum TravelLocation {
		the_Market,
		the_wilderness,
		Longdale
	}
	
	public static void sendTravelRequest(Player player, String nickName) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			
			//get nick name
			String playerName = nickName;
			for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
				PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
				if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(nickName)) {
					//used nick name
					playerName = onlinePlayer.getName();
					break;
				}
			}
			
			final Player playerToTravelTo = Bukkit.getPlayerExact(playerName);
			if(playerToTravelTo == null) {
				player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + nickName + ChatColor.RED + " is not an online player.");
				return;
			}
			PlayerConfig receiverConfig = PlayerConfig.getConfig(playerToTravelTo);
			String name = receiverConfig.getString("Name") != null ? receiverConfig.getString("Name") : playerToTravelTo.getName();
			if(travelToPlayer.containsKey(playerToTravelTo.getName())) {
				player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Could not send request as " + ChatColor.translateAlternateColorCodes('&', name) + ChatColor.RED + " already has a pending request!");
				return;
			}
			if(player.getWorld().getName().equalsIgnoreCase("revamp")
					|| playerToTravelTo.getWorld().getName().equalsIgnoreCase("revamp")) {
				player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Could not send request. (in a whitelisted area)");
				return;
			}
			
			Bukkit.getScheduler().runTask(plugin, () -> travelToPlayer.put(playerToTravelTo.getName(), player.getName()));
			
			player.sendMessage("");
			player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Sent request to travel to " + ChatColor.translateAlternateColorCodes('&', name) + ChatColor.GREEN + ".");
			player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "They have 2 minutes to accept or decline.");
			player.sendMessage("");
			
			playerToTravelTo.sendMessage("");
			playerToTravelTo.sendMessage(ChatColor.YELLOW + "============== " + ChatColor.BOLD + "Travel Request" + ChatColor.YELLOW + " ==============");
			playerToTravelTo.sendMessage(ChatColor.BOLD + player.getName() + ChatColor.WHITE + " is requesting to travel to you!");
			playerToTravelTo.sendMessage(ChatColor.WHITE + "To accept, use: " + ChatColor.GOLD + "/travel " + ChatColor.GREEN + "accept");
			playerToTravelTo.sendMessage(ChatColor.WHITE + "To decline, use: " + ChatColor.GOLD + "/travel " + ChatColor.RED + "decline");
			playerToTravelTo.sendMessage(ChatColor.YELLOW + "=============================================");
			playerToTravelTo.sendMessage("");
			
			final String playerNameFinal = playerName;
			//player has 2 minutes to accept or decline
			Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
				if(travelToPlayer.containsKey(playerNameFinal)) {
					if(playerToTravelTo != null)
						playerToTravelTo.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + ChatColor.translateAlternateColorCodes('&', player.getName()) + ChatColor.RED + "'s travel request expired.");
					if(player != null)
						player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Travel request to " + ChatColor.translateAlternateColorCodes('&', name) + ChatColor.RED + " expired.");
					Bukkit.getScheduler().runTask(plugin, () -> travelToPlayer.remove(playerNameFinal));
				}
			}, 2400L); // 2 minutes
			
		});
	}
	
	public static boolean attemptPublicTravel(Player player, String pointName, String previousMenu) {
		return attemptPublicTravel(player, pointName, previousMenu, null);
	}
	//run async
	public static boolean attemptPublicTravel(Player player, String pointName, String previousMenu, Shop shop) {

		String publicNameCased = publicNameCased(pointName);
		if(publicNameCased == null) {
			Chat.debug(pointName + " doesn't exist.");
			return false;
		}

		//if is player's point, just travel
		PublicTP tpConfig = PublicTP.getConfig(publicNameCased);
		UUID ownerUUID = UUID.fromString(tpConfig.getString("Owner"));
		Boolean isOwner = ownerUUID.toString().equalsIgnoreCase(player.getUniqueId().toString());
		
		//check if has password or fee
		if(tpConfig.getString("Pass") != null && !isOwner) {
			//has password
			
			//if opening anvil menu, set player XP to 0 first or they can take the item.
			if(TravelGUI.resetXP.containsKey(player.getName()))
				TravelGUI.resetXP.remove(player.getName());
			TravelGUI.resetXP.put(player.getName(), player.getLevel());
			player.setLevel(0);
			
			Bukkit.getScheduler().runTask(plugin, () -> {
				new AnvilGUI.Builder()
						.onClose(p3 -> {
							//give original XP back
							if(TravelGUI.resetXP.containsKey(player.getName())) {
								player.setLevel(TravelGUI.resetXP.get(player.getName()));
								TravelGUI.resetXP.remove(player.getName());
							}
						})
						
						.onComplete((p3, pass) -> { //called when the inventory output slot is clicked
							
							if(!pass.equalsIgnoreCase(tpConfig.getString("Pass"))) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Password incorrect.");
								return AnvilGUI.Response.text("Incorrect!");
							}
							
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Password correct!");
							
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
								//check if has fee,
								if(tpConfig.getString("Cost") != null) {
									//has a fee, open confirm menu
									
									ItemStack confirmItem = TravelGUI.openConfirmMenu(player, TravelGUI.Icon.Confirm_public_travel, "public points", true);
									ItemMeta confirmMeta = confirmItem.getItemMeta();
									List<String> lore = new ArrayList<String>();
									lore.add(ChatColor.WHITE + "Travel to " + publicNameCased + "?");
									lore.add(ChatColor.RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Cost: " + ChatColor.GOLD + Economy.convertCoins(tpConfig.getInt("Cost")));
									confirmMeta.setLore(lore);
									confirmItem.setItemMeta(confirmMeta);
								} else {
									//no fee
									//travel
									if(shop != null) {
										travelToShop(player, shop);
									} else {
										Location location = LocationCode.Decode(tpConfig.getString("LocationCode"));
										travel(player, publicNameCased, location);
									}
									
									//track the travel
									trackPublicTravel(player.getUniqueId(), publicNameCased);
								}
							});
							
							return AnvilGUI.Response.close();
						})
						.text("Password") //starting text
						.itemLeft(new ItemStack(Material.NAME_TAG))
						.onLeftInputClick(p3 -> Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Click the output slot to continue."))
						
						.title(ChatColor.BOLD + "Enter password" + ChatColor.RESET + ":") //set the title of the GUI
						.plugin(plugin)
						.open(player);
			});
		} else if(tpConfig.getString("Cost") != null && !isOwner) {
			//no password, but has fee

			//open fee confirm menu:
			ItemStack confirmItem = TravelGUI.openConfirmMenu(player, TravelGUI.Icon.Confirm_public_travel, previousMenu, true);
			ItemMeta confirmMeta = confirmItem.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.WHITE + "Travel to " + publicNameCased + "?");
			lore.add(ChatColor.RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Cost: " + ChatColor.GOLD + Economy.convertCoins(tpConfig.getInt("Cost")));
			if(shop != null) lore.add(ChatColor.RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Shop: " + ChatColor.GOLD + LocationCode.Encode(shop.location, true));
			confirmMeta.setLore(lore);
			confirmItem.setItemMeta(confirmMeta);
		} else {
			//no password or fee
			//travel
			if(shop != null) {
				travelToShop(player, shop);
			} else {
				Location location = LocationCode.Decode(tpConfig.getString("LocationCode"));
				travel(player, publicNameCased, location);
			}
			
			//track the travel
			trackPublicTravel(player.getUniqueId(), publicNameCased);
			
			Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
		}
		
		return true;
	}
	
	//run async
	public static boolean attemptTownTravel(Player player, String potentialTownName, String previousMenu) {

		if(!Town.townNameIsTaken(potentialTownName)) {
			return false;
		}
		
		String townDisplayName = Town.getTownDisplayName(Town.townNameCased(potentialTownName));
		boolean isMostCurrentName = townDisplayName.equalsIgnoreCase(potentialTownName);
		if(!isMostCurrentName) {
			return false;
		}
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		String originalTownName = Town.getOriginalTownName(townDisplayName);
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		
		if(townConfig.getString("Travel.LocationCode") == null) {
			player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + townDisplayName + " hasn't set a travel point yet.");
			return false;
		}
		
		boolean isMemberOfTown = playerConfig.getString("Town") != null && playerConfig.getString("Town").equalsIgnoreCase(originalTownName);
		boolean playerIsAllowed = player.isOp() || townConfig.getBoolean("Travel.Public") == true || isMemberOfTown;
		if(!playerIsAllowed) {
			player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + townDisplayName + " doesn't allow you to travel to their town.");
			return false;
		}
		
		boolean isMayorOfTown = isMemberOfTown && playerConfig.getInt("TownRank") >= Town.Rank.Co_Mayor.ordinal();
		int cost = townConfig.getString("Travel.Fee") == null || isMayorOfTown ? 0 : townConfig.getInt("Travel.Fee"); //mayors don't pay for travel
		
		if(playerConfig.getInt("Coins") < cost) {
			player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "You don't have enough coins! The travel fee is " + ChatColor.GOLD + Economy.convertCoins(cost) + ChatColor.WHITE + ".");
			return false;
		}
		
		if(cost == 0) {
			//costs not involved, just travel
			Location location = LocationCode.Decode(townConfig.getString("Travel.LocationCode"));
			travel(player, townDisplayName, location);
			Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
		} else {
			ItemStack confirmItem = TravelGUI.openConfirmMenu(player, TravelGUI.Icon.Confirm_town_travel, previousMenu, true);
			ItemMeta confirmMeta = confirmItem.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.WHITE + "Travel to " + townDisplayName + "?");
			lore.add(ChatColor.RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Cost: " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Travel.Fee")));
			confirmMeta.setLore(lore);
			confirmItem.setItemMeta(confirmMeta);
		}
		
		return true;
	}
	
	@EventHandler
	public void teleportEvent(PlayerTeleportEvent event) {
		Player player = event.getPlayer();
		if(!event.getTo().getWorld().getName().equalsIgnoreCase("revamp")
				&& !player.isOp())
			player.setGameMode(GameMode.SURVIVAL);
		if(event.getFrom().getWorld().getName().equalsIgnoreCase("revamp") && !event.getTo().getWorld().getName().equalsIgnoreCase("revamp")) {
			player.getInventory().clear();
		}
	}
	
	public static void travelToShop(Player player, Shop shop) {
		//travel to a shop coords and face the shop
		Bukkit.getScheduler().runTask(plugin, () -> {
			try {
				Block signBlock = shop.location.getWorld().getBlockAt(shop.location);
				BlockFace oppositeFace = ((Directional) signBlock.getBlockData()).getFacing();
				Block blockBehind = signBlock.getRelative(oppositeFace.getOppositeFace());
				//Chat.debug("Block behind sign is: " + blockBehind.getType());
				
				Block blockInFront = signBlock.getRelative(oppositeFace);
				//Chat.debug("Block in front of sign is: " + blockInFront.getType());
				
				Location standingLocation = blockInFront.getLocation().add(0.5, -1, 0.5);
				
				//face the sign
				Vector dir = blockBehind.getLocation().clone().add(0.5, 0, 0.5).subtract(standingLocation.clone().add(0, 1, 0)).toVector();
				standingLocation.setDirection(dir);
				
				Travel.travel(player, "a " + shop.type.toString().toLowerCase() + " shop (in " + shop.travelPoint + ")", standingLocation);
			} catch(Exception e) {
				Block signBlock = shop.location.getWorld().getBlockAt(shop.location);
				Chat.logError("Error travelling to shop " + shop.travelPoint + "(" + LocationCode.Encode(shop.location) + ") (region: " + shop.regionName + ") - signBlock is " + signBlock.getType(), e);
				Travel.attemptPublicTravel(player, shop.travelPoint, "Quick Menu");
			}
		});
	}
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			try {
				final Player player = (Player) sender;
				if(commandLabel.equalsIgnoreCase("public")) {
					TravelGUI.openPublicMenu(player);
				}
				
				if(commandLabel.equalsIgnoreCase("travel") || commandLabel.equalsIgnoreCase("sethome") || commandLabel.equalsIgnoreCase("home") || commandLabel.equalsIgnoreCase("warp") || commandLabel.equalsIgnoreCase("tp") || commandLabel.equalsIgnoreCase("teleport")) {
					
					if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.OPEN_THE_TRAVEL_MENU)) TaskManager.stepTask(player.getUniqueId());
					
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					
					if(!Admin.spectating.contains(player.getName())) {
						List<String> privateTravelPoints = new ArrayList<String>();
						if(playerConfig.getStringList("Travel.Private") != null)
							privateTravelPoints = playerConfig.getStringList("Travel.Private");
						List<String> publicTravelPoints = new ArrayList<String>();
						if(playerConfig.getStringList("Travel.Public") != null)
							publicTravelPoints = playerConfig.getStringList("Travel.Public");
						
						List<String> publicPoints = DeadMC.TravelFile.data().getStringList("Public");
						
						if(args.length == 0) {
							TravelGUI.openQuickMenu(player);
							return;
						}
						
						if(args[0].equalsIgnoreCase("pos")) {
							if((playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal())
									|| player.getWorld().getName().equalsIgnoreCase("revamp")) {
								if(args.length == 3
										&& Chat.isInteger(args[1]) && Chat.isInteger(args[2])) {
									Location location = new Location(player.getWorld(), Integer.parseInt(args[1]), 5, Integer.parseInt(args[2]));
									location.setY(player.getWorld().getHighestBlockYAt(location));
									travel(player, "(" + args[1] + ", " + args[2] + ")", location);
								} else
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/travel pos <x> <z>" + ChatColor.WHITE + ".");
								return;
							}
							
						}
						
						if(args[0].equalsIgnoreCase("menu")) {
							Bukkit.getScheduler().runTask(plugin, () -> Bukkit.getServer().dispatchCommand(player, "travel"));
							return;
						}
						
						if(args[0].equalsIgnoreCase("market")) {
							Bukkit.getScheduler().runTask(plugin, () -> Bukkit.getServer().dispatchCommand(player, "market"));
							return;
						}
						
						if(args[0].equalsIgnoreCase("wilderness")) {
							Bukkit.getScheduler().runTask(plugin, () -> Bukkit.getServer().dispatchCommand(player, "wild"));
							return;
						}
						
						if(args[0].equalsIgnoreCase("AdminWorld")) {
							if(playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal()) {
								
								player.setAllowFlight(true);
								
								for(ItemStack it : player.getInventory().getContents()) {
									if(it != null && !(it.getItemMeta().getDisplayName().contains("Backpack"))) {
										player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You need to have an empty inventory to travel here.");
										return;
									}
								}
								
								Bukkit.getScheduler().runTask(plugin, () -> {
									Bukkit.createWorld(WorldCreator.name("revamp"));
									travelSafely(player, new Location(Bukkit.getWorld("revamp"), 250000, 60, 250000), "AdminWorld");
									player.setGameMode(GameMode.CREATIVE);
								});
								return;
							} else
								player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You are not whitelisted to travel here.");
							return;
						}
						
						if(args[0].equalsIgnoreCase("revamp")) {
							if(playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.JUNIOR_MOD.ordinal()) {
								
								player.setAllowFlight(true);
								
								for(ItemStack it : player.getInventory().getContents()) {
									if(it != null && !(it.getItemMeta().getDisplayName().contains("Backpack"))) {
										player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You need to have an empty inventory to travel here.");
										return;
									}
								}
								
								Bukkit.getScheduler().runTask(plugin, () -> {
									Bukkit.createWorld(WorldCreator.name("revamp"));
									PaperLib.teleportAsync(player, new Location(Bukkit.getWorld("revamp"), -409, 60, 48));
									player.setGameMode(GameMode.CREATIVE);
								});
								return;
							} else
								player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You are not whitelisted to travel here.");
							return;
						}
						
						if(args[0].equalsIgnoreCase("accept") || args[0].equalsIgnoreCase("decline")) {
							if(travelToPlayer.containsKey(player.getName())) {
								
								if(args[0].equalsIgnoreCase("accept")) {
									Player playerToTravel = Bukkit.getPlayer(travelToPlayer.get(player.getName()));
									
									if(playerToTravel == null) {
										player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + travelToPlayer.get(player.getName()) + " is no longer online.");
										Bukkit.getScheduler().runTask(plugin, () -> Bukkit.getServer().dispatchCommand(player, "travel decline"));
										return;
									}
									
									if(player.getWorld().getName().equalsIgnoreCase("revamp")
											|| playerToTravel.getWorld().getName().equalsIgnoreCase("revamp")) {
										player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Could not send request. (in a whitelisted area)");
										return;
									}
									
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + travelToPlayer.get(player.getName()) + " is travelling to you...");
									
									playerToTravel.sendMessage("");
									playerToTravel.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + player.getName() + " accepted your travel request!");
									travel(playerToTravel, player);
									playerToTravel.sendMessage("");
									
									travelToPlayer.remove(player.getName());
								}
								if(args[0].equalsIgnoreCase("decline")) {
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + travelToPlayer.get(player.getName()) + "'s travel request was cancelled.");
									travelToPlayer.remove(player.getName());
								}
							} else
								player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You have no pending travel requests.");
							return;
						}
						
						if(args[0].equalsIgnoreCase("create")) {
							TravelGUI.openCreateMenu(player);
							return;
						}
						
						if(args[0].equalsIgnoreCase("delete")) {
							if(args.length > 1) {
								String pointName = args[1];
								
								boolean isPrivate = privateTravelPoints.contains(pointName);
								String nameCased = publicNameCased(pointName);
								boolean isPublic = nameCased != null && publicTravelPoints.contains(nameCased);
								
								if(isPublic || isPrivate) {
									
									ItemStack confirmItem = isPublic ? TravelGUI.openConfirmMenu(player, TravelGUI.Icon.DELETE_PUBLIC_TP_CONFIRM, "manage public: " + pointName, true) : TravelGUI.openConfirmMenu(player, TravelGUI.Icon.DELETE_PRIVATE_TP_CONFIRM, "manage private: " + pointName, true);
									ItemMeta deletemeta = confirmItem.getItemMeta();
									List<String> lore = new ArrayList<String>();
									lore.add(" " + ChatColor.GRAY + "" + ChatColor.BOLD + ">" + ChatColor.GRAY + " Name: " + ChatColor.AQUA + ChatColor.BOLD + pointName);
									deletemeta.setLore(lore);
									confirmItem.setItemMeta(deletemeta);
									
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "You don't have a travel point named '" + pointName + "'!");
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/travel list" + ChatColor.WHITE + " to manage your points.");
									player.sendMessage("");
								}
								
							} else {
								TravelGUI.openManageMenu(player);
							}
							return;
						}
						
						if(args[0].equalsIgnoreCase("list")) {
							TravelGUI.openManageMenu(player);
							return;
						}
						
						//private
						if(privateTravelPoints.contains(args[0])) {
							String pointName = args[0];
							Location location = LocationCode.Decode(playerConfig.getString("Travel." + pointName + ".LocationCode"));
							travel(player, pointName, location);
							
							//track the travel
							playerConfig.set("Travel." + pointName + ".Uses", playerConfig.getString("Travel." + pointName + ".Uses") == null ? 1 : playerConfig.getInt("Travel." + pointName + ".Uses") + 1);
							playerConfig.save();
							
							return;
						}
						
						if(playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") == StaffRank.ADMINISTRATOR.ordinal()) {
							
							//travel to player
							String playerName = args[0];
							for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
								PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
								if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[0])) {
									//used nick name
									playerName = onlinePlayer.getName();
									break;
								}
							}
							if(Bukkit.getPlayerExact(playerName) != null) {
								
								Player playerToTravel = Bukkit.getPlayerExact(playerName);
								String name = player.getName();
								if(playerConfig.getString("Name") != null)
									name = playerConfig.getString("Name");
								playerToTravel.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + name + " is travelling to you...");
								
								travel(player, playerToTravel);
								
								return;
							}
							
						}
						
						//travel to player (original name only! (takes priority))
						if(args.length == 2 && args[1].contains("player")) {
							//run asynchronously as we search all online players for nick names
							sendTravelRequest(player, args[0]);
							return;
						}
						
						//town
						if(attemptTownTravel(player, args[0], "towns")) {
							//success
							return;
						}
						
						//public
						if(attemptPublicTravel(player, args[0], "public points")) {
							//success
							return;
						}
						
						//travel to player (player last priority))
						sendTravelRequest(player, args[0]);
						
						return;
					} else
						player.sendMessage(ChatColor.YELLOW + "[Spectate] " + ChatColor.WHITE + "You cannot use this while spectating.");
					
				}
				
				if(commandLabel.equalsIgnoreCase("market") || (commandLabel.equalsIgnoreCase("wilderness") || commandLabel.equalsIgnoreCase("wild"))) {
					
					if(commandLabel.equalsIgnoreCase("market"))
						travel(player, "the Market");
					else if(commandLabel.equalsIgnoreCase("wilderness") || commandLabel.equalsIgnoreCase("wild"))
						travel(player, "the wilderness");
					
					else {
						player.sendMessage("");
						player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE +"This is a donator feature!");
						player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "You can use the " + ChatColor.GOLD + "/tp" + ChatColor.WHITE + " menu to travel.");
						player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
						player.sendMessage("");
					}
					
				}
				
				if(commandLabel.equalsIgnoreCase("longdale") || commandLabel.equalsIgnoreCase("spawn"))
					travel(player, "Longdale");
				
				return;
				
			} catch(Exception e) {
				Chat.logError(e, sender instanceof Player player ? player : null);
			}
			
		});
		
		return true;
	}
	
	public static void cancelTravel(Player player) {
		Bukkit.getScheduler().runTask(plugin, () -> {
			//cancel travel
			if(!isTravelling.containsKey(player.getName())) return; //nothing to cancel
			
			travelEnabledPlayers.remove(player.getName());
			isTravelling.remove(player.getName());
			player.removePotionEffect(PotionEffectType.CONFUSION);
		});
	}
	
	@EventHandler
	public void onBoardSignClick(final PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		
		if(isTravelling.containsKey(player.getName())) {
			cancelTravel(player);
			player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You cancelled your travel request.");
			return;
		}
		
		if(event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(event.getClickedBlock().getState() instanceof Sign) {
				Sign sign = (Sign) event.getClickedBlock().getState();
				if(sign.getLine(0).contains("[Board]")) {
					
					if(sign.getLine(2).toLowerCase().contains("wilderness")) {
						travel(player, "the wilderness");
						return;
					}
					
					if(sign.getLine(2).toLowerCase().contains("market")) {
						travel(player, "the Market");
						return;
					}
					
					travel(player, ChatColor.stripColor(sign.getLine(2)));
				}
			}
		}
	}
	
	@EventHandler
	public void suffocationWhileTeleporting(EntityDamageEvent event) {
		
		if(event.getCause() == DamageCause.SUFFOCATION || event.getCause() == DamageCause.CRAMMING) {
			
			Entity entity = event.getEntity();
			if(entity instanceof Player) {
				Player player = (Player) entity;
				
				//travel pvp protection:
				if(recentlyTravelled.containsKey(player.getName())) {
					
					ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
					long protectionLeft = (PvP.travelProtection - ChronoUnit.SECONDS.between(recentlyTravelled.get(player.getName()), adelaideTime));
					
					if(protectionLeft < 1) {
						recentlyTravelled.remove(player.getName());
						return;
					}
					
					//still has protection
					event.setCancelled(true);
					
					Location highestLocation = new Location(player.getLocation().getWorld(), player.getLocation().getX(), player.getLocation().getY() + 2, player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
					PaperLib.teleportAsync(player, highestLocation); //teleport to top block
				}
			}
			
		}
	}
	
	private static void travelSafely(Player player, Location location, String travelPointName) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			Location checkLocation = location;
			
			//Chat.broadcastMessage("Checking location to travel...");
			int tries = 0;
			Boolean isUnsafe = false;
			while(isUnsafe || tries == 0) {
				//Chat.broadcastMessage("Tries = " + tries);
				
				tries++;
				if(tries > 250) {
					//there's apparently no free space
					Block highestBlock = checkLocation.getWorld().getHighestBlockAt(checkLocation);
					checkLocation = new Location(highestBlock.getWorld(), highestBlock.getX(), highestBlock.getY() + 2, highestBlock.getZ(), checkLocation.getYaw(), checkLocation.getPitch());
					break;
				}
				
				Block feet = checkLocation.getBlock();
				Block head = feet.getRelative(BlockFace.UP);
				Block ground = feet.getRelative(BlockFace.DOWN);
				
				if(feet.getType().isSolid()) {
					//feet blocked
					//Chat.broadcastMessage("Feet blocked");
					checkLocation = new Location(checkLocation.getWorld(), checkLocation.getX(), checkLocation.getY() + 1, checkLocation.getZ(), checkLocation.getYaw(), checkLocation.getPitch());
					isUnsafe = true;
				} else if(head.getType().isSolid()) {
					//head blocked
					//Chat.broadcastMessage("Head blocked");
					checkLocation = new Location(checkLocation.getWorld(), checkLocation.getX(), checkLocation.getY() + 1, checkLocation.getZ(), checkLocation.getYaw(), checkLocation.getPitch());
					isUnsafe = true;
				} else if(!ground.getType().isSolid()) {
					//no solid ground
					//Chat.broadcastMessage("No solid ground");
					checkLocation = new Location(checkLocation.getWorld(), checkLocation.getX(), checkLocation.getY() - 1, checkLocation.getZ(), checkLocation.getYaw(), checkLocation.getPitch());
					isUnsafe = true;
				} else
					isUnsafe = false;
			}
			
			final Location locationFinal = checkLocation;
			
			Bukkit.getScheduler().runTask(plugin, () -> {
				//despawn current pets
				PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
				if(playerConfig.getString("Pets.Active.Reference") != null)
					//despawn current pets
					Pets.DespawnPets(player, false);
				
				//Chat.broadcastMessage("Teleporting async");
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 200, 500000, false, false, false));
				PaperLib.teleportAsync(player, locationFinal).thenRun(() -> completeTravel(player, travelPointName, locationFinal));
			});
		});
	}
	
	private static void completeTravel(Player player, String travelPointName, Location location) {
		//Chat.broadcastMessage("COMPLETE!");
		player.removePotionEffect(PotionEffectType.BLINDNESS);
		player.removePotionEffect(PotionEffectType.CONFUSION);
		player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 60, 500000, false, false, false));
		PaperLib.teleportAsync(player, location); //teleport again asynchronously to put at right block
		
		if(isTravelling.containsKey(player.getName()) && isTravelling.get(player.getName()).equalsIgnoreCase(travelPointName)) {
			Bukkit.getScheduler().runTaskLater(plugin, () -> {
				player.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
				player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "After a long journey, you arrive at " + travelPointName + ChatColor.WHITE + ".");
				isTravelling.remove(player.getName());
				//despawn current pets
				PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
				if(playerConfig.getString("Pets.Active.Reference") != null)
					Pets.teleportPet(player, Pets.Pet.values()[playerConfig.getInt("Pets.Active.Ordinal")]);
				
				//TASK
				if(travelPointName.contains("The Longdale Market")
						&& (TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.TRAVEL_TO_THE_MARKET) || TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.FIND_THE_IRON_SWORD_SHOP)))
					TaskManager.stepTask(player.getUniqueId());
				if(travelPointName.equals(Town.getTownDisplayName(player.getUniqueId()))
						&& TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.TRAVEL_TO_TOWN))
					TaskManager.stepTask(player.getUniqueId());
			}, 20L);
		}
		
		//TASK
		if(travelPointName.equals("the wilderness") && (TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.TRAVEL_TO_WILDERNESS)
				|| TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.TRAVEL_TO_WILDERNESS_TO_CLAIM_LAND))) {
			TaskManager.stepTask(player.getUniqueId());
		}
		
		//TASK
		if(travelPointName.equals("the Market")
				&& TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.TRAVEL_TO_THE_MARKET)) {
			TaskManager.stepTask(player.getUniqueId());
		}
	}
	
	public static void travel(Player player, String globalPositionName) {
		travel(player, globalPositionName, getGlobalPosition(globalPositionName));
	}
	public void travel(Player playerTravelling, Player playerToTravelTo) {
		travel(playerTravelling, ChatColor.translateAlternateColorCodes('&', playerToTravelTo.getDisplayName()), playerToTravelTo.getLocation());
	}
	public static void travel(Player player, String travelPointName, Location location) {
		
		if(isTravelling.containsKey(player.getName())) return; //is already travelling
		
		player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "You begin a journey to " + travelPointName + ChatColor.WHITE + "...");
		
		Bukkit.getScheduler().runTask(plugin, () -> {
			isTravelling.put(player.getName(), travelPointName);
			player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 2000, 500000, false, false, false));
		});
		
		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			
			if(!isTravelling.containsKey(player.getName()) || !isTravelling.get(player.getName()).equals(travelPointName)) return; //not travelling somewhere else
			
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 500000, false, false, false));
			
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				if(travelPointName.equalsIgnoreCase("the wilderness")) travelSafely(player, getRandomWildLocation(player), travelPointName); //wait to get random location only when needed
				else travelSafely(player, location, travelPointName);
			});
			
			//anti tp protection:
			
			ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
			
			//reset if travelled within 30 seconds
			if(recentlyTravelled.containsKey(player.getName())) {
				//long protectionLeft = (PvP.travelProtection-ChronoUnit.SECONDS.between(recentlyTravelled.get(player.getName()), adelaideTime));
				//Chat.broadcastMessage(" - Updated time (" + protectionLeft + " seconds left)");
				recentlyTravelled.remove(player.getName());
			}
			
			recentlyTravelled.put(player.getName(), adelaideTime);
			
			Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
				ZonedDateTime adelaideTimeNew = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
				if(recentlyTravelled.containsKey(player.getName())
						&& adelaideTimeNew.minusSeconds(29).isAfter(recentlyTravelled.get(player.getName()))) {
					Bukkit.getScheduler().runTask(plugin, () -> recentlyTravelled.remove(player.getName()));
					//Chat.broadcastMessage("Protection worn off");
				}
			}, 600L);
			
		}, 60L);
		
		Bukkit.getScheduler().runTask(plugin, () -> {
			if(player.getAllowFlight() && !player.isOp()) {
				player.sendMessage(ChatColor.YELLOW + "[Fly] " + ChatColor.RED + "Flying mode disabled.");
				player.setFlying(false);
				player.setAllowFlight(false);
			}
		});
		
	}
	
	public static Location getGlobalPosition(String globalPosName) {
		if(globalPosName.equalsIgnoreCase("the wilderness")) return null;
		
		if(DeadMC.TravelFile.data().getString("Global Position." + globalPosName) == null) {
			Chat.logError("The global position '" + globalPosName + "' doesn't exist.");
			return DeadMC.TravelFile.data().getLocation("Global Position.Longdale");
		}
		return DeadMC.TravelFile.data().getLocation("Global Position." + globalPosName);
	}
	
	public static Location getRandomWildLocation(Player playerToSendMessageTo) {
		
		//int count = 0;
		
		long startTime = System.currentTimeMillis();
		//Bukkit.broadcastMessage("Getting random location...");
		
		Location randomLocation = null;
		while(randomLocation == null) {
			//Bukkit.broadcastMessage("1");
			//count++;
			
			int minimumDistance = 100000;
			int maximumDistance = 125000;
			int borderDistanceFromLongdale = minimumDistance + (int)(Math.random() * (maximumDistance - minimumDistance));
			
			Random rand = new Random();
			int randomSide = rand.nextInt(4) + 1; //random number of 1, 2, 3 or 4
			
			if(randomSide == 1) { //right side
				int z = borderDistanceFromLongdale;
				if(z <= 546) z = -z;
				else z -= 546;
				
				//z is a random number from -546 to 1454
				randomLocation = new Location(Bukkit.getWorld("world"), 743, 5, z, (float) -90.0, (float) 0.0);
			}
			
			if(randomSide == 2) { //left side
				int z = borderDistanceFromLongdale;
				if(z <= 546) z = -z;
				else z -= 546;
				
				//z is a random number from -546 to 1454
				randomLocation = new Location(Bukkit.getWorld("world"), -1257, 5, z, (float) 90.0, (float) 0.0);
			}
			
			if(randomSide == 3) { //top side
				int x = borderDistanceFromLongdale;
				if(x <= 1257) x = -x;
				else x -= 1257;
				
				//x is a random number from -1257 to 743
				randomLocation = new Location(Bukkit.getWorld("world"), x, 5, -546, (float) -180.0, (float) 0.0);
			}
			
			if(randomSide == 4) { //bottom side
				int x = borderDistanceFromLongdale;
				if(x <= 1257) x = -x;
				else x -= 1257;
				
				//x is a random number from -1257 to 743
				randomLocation = new Location(Bukkit.getWorld("world"), x, 5, 1454, (float) 0.0, (float) 0.0);
			}
			//Bukkit.broadcastMessage("2");
			Chat.sendTitleMessage(playerToSendMessageTo.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Searching for wild land...");
			try {
				PaperLib.getChunkAtAsync(randomLocation).get(); //load the chunk first async, otherwise getHighestBlockYAt will do it synchronously
			} catch(InterruptedException e) {
				e.printStackTrace();
			} catch(ExecutionException e) {
				e.printStackTrace();
			}
			//Bukkit.broadcastMessage("3");
			
			randomLocation.setY(Bukkit.getWorld("world").getHighestBlockYAt(randomLocation));
			//Bukkit.broadcastMessage("DOWN: " + randomLocation.getBlock().getRelative(BlockFace.DOWN).getType() + " - UP: " + randomLocation.getBlock().getRelative(BlockFace.UP).getType());
			
			//Bukkit.broadcastMessage("4");
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
			ApplicableRegionSet regionsAtLoc = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(randomLocation));
			
			if(((randomLocation.getBlock().getRelative(BlockFace.UP).getType() == Material.AIR
					|| randomLocation.getBlock().getRelative(BlockFace.UP).getType() == Material.CAVE_AIR
					|| randomLocation.getBlock().getRelative(BlockFace.UP).getType() == Material.VOID_AIR
					|| randomLocation.getBlock().getRelative(BlockFace.UP).getType() == Material.SNOW)
					&& (!(randomLocation.getBlock().getRelative(BlockFace.DOWN).getType() == Material.WATER
					|| randomLocation.getBlock().getRelative(BlockFace.DOWN).getType() == Material.LAVA
					|| randomLocation.getBlock().getRelative(BlockFace.DOWN).getType() == Material.CACTUS)))
					&& regionsAtLoc.size() == 0) {
				//Bukkit.broadcastMessage("5");
				break;
			}
			randomLocation = null;
		}
		
		//long finishTime = System.currentTimeMillis();
		
		//DecimalFormat df = new DecimalFormat("####0.00");
		//double timeInSeconds = (finishTime-startTime) / 1000.0; //convert to seconds
		
		//Bukkit.broadcastMessage("Finished in " + timeInSeconds + " seconds.");
		
		//Bukkit.broadcastMessage("Number of tries: " + count);
		
		Chat.sendTitleMessage(playerToSendMessageTo.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "You arrive in the wilderness...");
		
		return new Location(randomLocation.getWorld(), randomLocation.getX(), randomLocation.getY() + 2, randomLocation.getZ());
		
	}
	
	@EventHandler
	public void onNetherPortalGenerate(PortalCreateEvent event) {
		if(event.getReason() == CreateReason.NETHER_PAIR
				&& !event.getWorld().getName().contains("nether")
				&& event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			for(BlockState block : event.getBlocks()) {
				if(!Regions.playerCanBuild(player, block.getLocation(), true)) {
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Couldn't generate portal (land was claimed).");
					event.setCancelled(true);
					return;
				}
				
			}
		}
	}
	
	@EventHandler
	public void onBoardSignCreate(SignChangeEvent event) {
		Player player = event.getPlayer();
		if(event.getLine(0).equalsIgnoreCase("[board]")) {
			if(!player.isOp()) {
				player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.RED + "You cannot create boarding signs.");
				event.setCancelled(true);
			} else {
				event.setLine(0, ChatColor.BOLD + "[Board]");
				event.setLine(1, "Click for info");
				if(event.getLine(2).toLowerCase().contains("wild"))
					event.setLine(2, ChatColor.DARK_RED + "" + ChatColor.BOLD + "Wilderness");
				if(event.getLine(2).toLowerCase().contains("market"))
					event.setLine(2, ChatColor.GOLD + "" + ChatColor.BOLD + "The Market");
				if(event.getLine(2).toLowerCase().contains("rank"))
					event.setLine(2, ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Ranks");
				if(event.getLine(2).toLowerCase().contains("tutorial"))
					event.setLine(2, ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Tutorial");
			}
		}
	}
	
	public static boolean doesPointExist(Player player, String type, String point) {
		if(type.equalsIgnoreCase("public") || type.equalsIgnoreCase("shop")) {
			return publicNameIsTaken(point);
		} else if(type.equalsIgnoreCase("private")) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			return playerConfig.getString("Travel." + point + ".LocationCode") != null;
		} else if(type.equalsIgnoreCase("town")) {
			return Town.townNameIsTaken(point);
		} else {
			Chat.logError(type + " is not a valid travel point type.");
			return false;
		}
	}
	
	public static boolean publicNameIsTaken(String pointName) {
		for(String point : DeadMC.TravelFile.data().getStringList("Public")) {
			if(point.equalsIgnoreCase(pointName)) {
				return true;
			}
		}
		return false;
	}
	
	public static String publicNameCased(String pointName) {
		for(String point : DeadMC.TravelFile.data().getStringList("Public")) {
			if(point.equalsIgnoreCase(pointName)) {
				return point;
			}
		}
		return null;
	}
	
	public static void trackPublicTravel(UUID uuid, String pointName) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			PublicTP tpConfig = PublicTP.getConfig(pointName);
			
			int minutesSinceCreated = DateCode.getTimeSince(tpConfig.getString("Created"));
			double daySinceCreated = Math.floorDiv(minutesSinceCreated, 1440) + 1; //the day number since being created (eg. first day is 1)
			
			if(daySinceCreated > tpConfig.getInt("TodaysDay")) {
				//update day
				tpConfig.set("TodaysDay", daySinceCreated);
				
				//reset visits
				tpConfig.set("Visits", new ArrayList<String>());
				tpConfig.save();
			}
			
			//log unique visit straight away
			List<String> visits = tpConfig.getStringList("Visits");
			
			boolean alreadyVisitedToday = visits.contains(uuid.toString());
			if(!alreadyVisitedToday) {
				//track visit
				visits.add(uuid.toString());
				tpConfig.set("Visits", visits);
				
				//add unique visit
				tpConfig.set("UniqueVisits", tpConfig.getInt("UniqueVisits") + 1);
				tpConfig.save();
			}
		});
	}
	
	public float getPopularity(PublicTP tpConfig) {
		//track time
		float minutesSinceCreated = DateCode.getTimeSince(tpConfig.getString("Created"));
		float visits = tpConfig.getInt("UniqueVisits");
		
		return visits / minutesSinceCreated;
	}
	
	public static ShopData getShopsAtLocation(Location location) {
		//get lowest priority region
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
		ApplicableRegionSet regionsAtLoc = regionManager.getApplicableRegions(BukkitAdapter.asBlockVector(location));
		
		ProtectedRegion lowestPriorityRegion = null;
		for(ProtectedRegion region : regionsAtLoc) {
			if(region.getPriority() == 1) {
				lowestPriorityRegion = region;
			}
		}
		
		if(lowestPriorityRegion == null) return null; //return 0 if no shops around
		
		int numberOfShops = 0;
		int stocked = 0;
		
		ApplicableRegionSet regionsWithinRegion = regionManager.getApplicableRegions(lowestPriorityRegion);
		for(ProtectedRegion region : regionsWithinRegion) {
			//for each region within region
			//check if has shops
			
			List<String> shops = new ArrayList<String>();
			File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + region.getId() + ".yml");
			if(regionFile.exists()) {
				RegionConfig regionConfig = RegionConfig.getConfig(region.getId());
				shops = regionConfig.getStringList("Shops");
				
				numberOfShops += shops.size();
				
				//check if stocked:
				for(String shop : shops) {
					Block block = Shops.decodeCode(shop).getBlock();
					String shopCode = Shops.convertToCode(block.getLocation());
					//Sign sign = (Sign) block.getState();
					//if(!sign.getLine(1).equalsIgnoreCase(ChatColor.DARK_RED + "Out of Order")) {
					if(regionConfig.getString(shop + ".Item") != null) {
						//get total stock available:
						int totalStock = Shops.getTotalStock(regionConfig, shopCode);
						if(totalStock > 0) {
							stocked++;
						}
					}
				}
			}
		}
		
		if(numberOfShops == 0) {
			return null;
		}
		
		return new ShopData(numberOfShops, stocked);
	}
}