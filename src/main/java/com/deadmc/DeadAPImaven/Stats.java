package com.deadmc.DeadAPImaven;

import net.md_5.bungee.api.ChatColor;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class Stats implements CommandExecutor {
	private static DeadMC plugin;
	public Stats(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public enum Stat {
		NEW_PLAYERS,
		RETURNING_PLAYERS,
		VOTES,
		PLAYTIME,
		ACTIVE_STAFF,
		STAFF_PLAYTIME,
		OSAT,
		RESPONSES
	};
	
	// USER EXPERIENCE:
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		Player player = null;
		if(sender instanceof Player)
			player = (Player) sender;
		else {
			Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_RED + "[Stats] Cannot perform from console.");
			return true;
		}
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);

		if(commandLabel.equalsIgnoreCase("stat")) {
			if(player.isOp() || playerConfig.getString("StaffRank") != null) {
				
    			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
    			    @Override
    			    public void run() {
    			    	Player player = (Player) sender;
						
						if(args.length == 0) {
							//show last 7 days
							
							player.sendMessage("");
							
							player.sendMessage("Showing last 7 days...");
							
							String spacing = "     ";
							String days = "";
							for(int day = 0; day < DayOfWeek.values().length; day++) {
								String dayString = DayOfWeek.values()[day].toString().substring(0,3);
								
								if(day < (DayOfWeek.values().length-1))
									days += dayString + spacing;
								else days += dayString;
							}
							player.sendMessage("Day:" + spacing + ChatColor.GOLD + days);
							
							//for each stat
							// - add each day
							
							for(String stat : DeadMC.statsToTrack) {
								String stats = "";
								
								String previousStat = "0";
								for(int day = 0; day < DayOfWeek.values().length; day++) {
									int amount = DeadMC.StatsFile.data().getInt(day + "." + stat);
			
									String makeBold = "";
									if(amount > 0) makeBold = "" + ChatColor.BOLD;
									
									if(stat.equalsIgnoreCase("PT")) {
										amount /= 60;
									}
									if(stat.equalsIgnoreCase("OS")) {
										int responses = DeadMC.StatsFile.data().getInt(day + ".R");
										if(responses == 0) amount = 0;
										else amount /= responses;
									}
										
									String amountAsString = new String(makeBold + amount + ChatColor.WHITE);
									String amountAsString_NoFormatting = new String("" + amount);
									
		//							if(stat.equalsIgnoreCase("S") && amount > 0) {
		//								amountAsString += "$";
		//								amountAsString_NoFormatting += "$";
		//							}
									if(stat.equalsIgnoreCase("OS") && amount > 0) {
										amountAsString += "%";
										amountAsString_NoFormatting += "%";
									}
									
									//pre spacing amount
									String prespacing = "";
									int prespacingAmount = 3 - (previousStat.length()-2);
									for(int count = 0; count < prespacingAmount; count++)
										prespacing += " ";
									//post spacing amount
									String postspacing = "";
									int postspacingAmount = 3 - (amountAsString_NoFormatting.length()-2);
									for(int count = 0; count < postspacingAmount; count++)
										postspacing += " ";
									
									stats += prespacing + amountAsString + postspacing;
									
									previousStat = amountAsString_NoFormatting;
								}
								
								String extraSpace = "";
								if(stat.length() == 1) extraSpace = "  ";
								player.sendMessage(ChatColor.GOLD + stat + ChatColor.WHITE + ":   " + extraSpace + stats);
							}
							
							player.sendMessage("");
						
						} else if(Chat.isInteger(args[0])) {
							//compare weeks
		
							int weeksBackToTrack = Integer.parseInt(args[0]);
							if(weeksBackToTrack <= 4) {
							
							//start at lowest week, work way to current week (greatest week)
							int currentWeek = DeadMC.StatsFile.data().getInt("CurrentWeek");
							int weekToTrackTo = currentWeek - weeksBackToTrack;
							if(weekToTrackTo <= 0) weekToTrackTo = 1;
							
							player.sendMessage("");
							
							player.sendMessage("Comparing to last " + (currentWeek-weekToTrackTo) + " weeks...");
							
							List<String> stats = new ArrayList<String>(); //eg. stats[0] = Np: 0  5  12  3
							for(String stat : DeadMC.statsToTrack) { //for each stat
								if(stat.length() == 1) stats.add(ChatColor.GOLD + stat + ":       " + ChatColor.WHITE);
								else stats.add(ChatColor.GOLD + stat + ":    " + ChatColor.WHITE);
							}
							
							String spacing = "        ";
							String weeks = "";
							for(int week = weekToTrackTo; week <= currentWeek; week++) {
								
								//display week number:
								String weekString = "WK" + week;
								
								if(week < currentWeek)
									weeks += weekString + spacing;
								else {
									weekString = "cur";
									weeks += weekString;
								}
								
								//display stats:
								
								//for each week
								//add stats for each day
								
								int count = 0;
								for(String stat : DeadMC.statsToTrack) { //for each stat
									
									int total = 0;
									for(int day = 0; day < DayOfWeek.values().length; day++) { //for each day
										//add each day
										total += GetWeekFile(week).getInt(day + "." + stat);
									}
									
									String makeBold = "";
									if(total > 0) makeBold = "" + ChatColor.BOLD;
									
									if(stat.equalsIgnoreCase("PT")) {
										total /= 60; //in hours
									}
									if(stat.equalsIgnoreCase("OS")) {
										int responses = 0;
										for(int day = 0; day < DayOfWeek.values().length; day++) { //for each day
											//add each day
											responses += GetWeekFile(week).getInt(day + ".R");
										}
										if(responses == 0) total = 0;
										else total /= responses;
									}
									if(stat.equalsIgnoreCase("RP")) {
										if(week == currentWeek) {
											ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
											total /= (adelaideTime.getDayOfWeek().ordinal()+1);
										} else total /= 7;
											
									}
										
									String totalAsString = new String(makeBold + total + ChatColor.WHITE);
									String totalAsString_NoFormatting = new String("" + total);
									
									if(stat.equalsIgnoreCase("S") && total > 0) {
										totalAsString += "$";
										totalAsString_NoFormatting += "$";
									}
									if(stat.equalsIgnoreCase("OS")) {
										totalAsString += "%";
										totalAsString_NoFormatting += "%";
									}
									
									//pre spacing amount
									String prespacing = "";
									int prespacingAmount = 6 - (totalAsString_NoFormatting.length()-1);
									for(int i = 0; i < prespacingAmount; i++)
										prespacing += " ";
									//post spacing amount
									String postspacing = "";
									int postspacingAmount = 6 - (totalAsString_NoFormatting.length()-1);
									for(int i = 0; i < postspacingAmount; i++)
										postspacing += " ";
									
									String newString = stats.get(count) + prespacing + totalAsString + postspacing;
									stats.set(count, newString);
									
									count++;
								}
							}
							//display week header:
							player.sendMessage("Week:" + spacing + ChatColor.GOLD + weeks);
							//loop through stats and display:
							for(String stat : stats) {
								player.sendMessage(stat);
							}
							
							player.sendMessage("");
							
							} else {
								player.sendMessage("Maximum range of weeks is 4.");
							}
						} else {
							player.sendMessage("For current week: /stat");
							player.sendMessage("For X previous weeks: /stat <weeks back to compare>");
						}
    			    }
    			});
			} else 
				 player.sendMessage("Need permission.");
		}
		return true;
	}
	
	// TRACKING:
	
	public static void Add(Stat stat, int amount) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
		    @Override
		    public void run() {
				EndOfWeek(); //run end of day if need be first

				String statID = DeadMC.statsToTrack.get(stat.ordinal());
				ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
				
				//get this weeks file
				// - todays section
				//   - ++
				int newValue = (int)DeadMC.StatsFile.data().getInt(adelaideTime.getDayOfWeek().ordinal() + "." + statID) + amount;
				DeadMC.StatsFile.data().set(adelaideTime.getDayOfWeek().ordinal() + "." + statID, newValue);
				Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
				    @Override
				    public void run() {
				    	DeadMC.StatsFile.save();
				    }
				},0L);
		    }
		});
	}
	public static void Add(Stat stat) {
		Add(stat, 1);
	}
	
	// GATHERING:
	
	//get the stat for specific day
	public static int Get(Stat stat, int week, int day) {
		//get relevant week file
		// - get relevant day section
		//   - get relevant stat
		String statID = DeadMC.statsToTrack.get(stat.ordinal());
		return GetWeekFile(week).getInt(day + "." + statID);
	}
	
	//get the stat for entire week
	public static int Get(Stat stat, int week) {
		int amount = 0;
		
		//add the stat for each day
		for(int day = 0; day < DayOfWeek.values().length; day++)
			amount += Get(stat, week, day);
		
		return amount;
	}
	
	// TRACKING FILES:
	
	private static FileConfiguration GetWeekFile(int week) {
		if(week == DeadMC.StatsFile.data().getInt("CurrentWeek"))
			return DeadMC.StatsFile.data(); //if current week, use the stats file
		else {
			FileConfiguration statsYml = YamlConfiguration.loadConfiguration(new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder() + File.separator + "stats", "Week" + week + ".yml"));
			return statsYml;
		}
	}
	
	public static DayOfWeek startOfWeek = DayOfWeek.MONDAY; //the day to start a new week tracking
	public static void EndOfWeek() {
		//if actual day is Tuesday, and StartOfWeek was > 6 days ago
		ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		if(adelaideTime.getDayOfWeek() == startOfWeek && DateCode.getDaysSince(DeadMC.StatsFile.data().getString("StartOfWeek")) > 6) {

			// save complete stats.yml file into stats folder
			File source = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder() + "/" + "stats.yml");
			File destination = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder() + "/stats/Week" + DeadMC.StatsFile.data().getInt("CurrentWeek") + ".yml");
			try { FileUtils.copyFile(source, destination); } catch (IOException e) { e.printStackTrace(); }
			
			// reset all days
			for(int count = 0; count < DayOfWeek.values().length; count++) {
				for(String stat : DeadMC.statsToTrack) {
					DeadMC.StatsFile.data().set(count + "." + stat, 0); 
				}
			}
			
			// reset survey comments
			DeadMC.StatsFile.data().set("Survey.Positive", new ArrayList<String>()); 
			DeadMC.StatsFile.data().set("Survey.Negative", new ArrayList<String>()); 
			
			// current week ++
			DeadMC.StatsFile.data().set("CurrentWeek", (int)DeadMC.StatsFile.data().getInt("CurrentWeek")+1);
			// start of week = today
			DeadMC.StatsFile.data().set("StartOfWeek", DateCode.Encode(adelaideTime, true, false, false, true, false, false)); //just encode year and day of year
			DeadMC.StatsFile.save();
			
		}
	}

}
