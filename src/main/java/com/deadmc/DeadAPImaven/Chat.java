package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Admin.Punishment;
import com.deadmc.DeadAPImaven.Admin.StaffRank;
import com.deadmc.DeadAPImaven.Donator.Rank;
import com.deadmc.DeadAPImaven.Pets.Pet;
import com.deadmc.DeadAPImaven.Towns.Town;
import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import github.scarsz.discordsrv.dependencies.jda.api.entities.MessageEmbed;
import github.scarsz.discordsrv.util.DiscordUtil;
import github.scarsz.discordsrv.util.WebhookUtil;
import io.papermc.lib.PaperLib;
import me.clip.placeholderapi.PlaceholderAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.jetbrains.annotations.Nullable;

import java.text.DecimalFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.logging.LogRecord;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Chat implements Listener, CommandExecutor {
	private static DeadMC plugin;
	
	public Chat(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static List<String> townChatToggled = new ArrayList<String>(); //player
	public static List<String> nationChatToggled = new ArrayList<String>(); //player
	public static List<String> staffChatToggled = new ArrayList<String>(); //player
	public static Map<String, String> lastMessage = new HashMap<String, String>(); //player, message
	public static Map<String, String> replyToMessage = new HashMap<String, String>(); //receiver, sender
	
	public static List<String> loggedErrors = new ArrayList<String>();
	public static void logError(String message) {
		logError(message, Thread.currentThread().getStackTrace(), null);
	}
	public static void logError(String message, Player player) {
		logError(message, Thread.currentThread().getStackTrace(), player);
	}
	public static void logError(LogRecord record) {
		logError(record.getThrown().getMessage(), record.getThrown().getStackTrace(), null);
	}
	public static void logError(Exception e) {
		logError(e, null);
		e.printStackTrace();
	}
	public static void logError(Exception e, Player player) {
		logError(e.getMessage(), e.getStackTrace(), player);
		e.printStackTrace();
	}
	public static void logError(String message, Exception e) {
		logError(message + "\n" + e.getMessage(), e.getStackTrace(), null);
		e.printStackTrace();
	}
	public static void logError(String message, StackTraceElement[] stacktrace, Player player) {
		if(player != null) player.sendMessage(ChatColor.RED + "[DeadMC] " + org.bukkit.ChatColor.RED + "Woops! There was an error and we couldn't complete that request. " + ChatColor.WHITE + "An administrator has been notified.");
		if(stacktrace.length == 0 && !loggedErrors.contains(message)) {
			Chat.broadcastDiscord(":head_bandage: There was an error!\n**" + message + "**\n> No stack to show? Sorry!", true);
			loggedErrors.add(message);
		} else if(!loggedErrors.contains(message+stacktrace[0].toString())) {
			String stackToShow = "";
			boolean isFirst = true;
			for(int count = 0; count < stacktrace.length; count++) {
				if(stacktrace[count].toString().contains("DeadAPImaven") && !stacktrace[count].toString().contains("Chat.logError")) { //only show stack trace relating to DeadMC and not apart of the error logging
					stackToShow += isFirst ? "\n> " + stacktrace[count] : "\n\n> " + stacktrace[count];
					isFirst = false;
				}
			}
			if(DeadMC.RestartFile.data().getString("IsDevelopmentServer") == null) {
				Chat.broadcastDiscord(":head_bandage: There was an error!\n**" + message + "**" + stackToShow, true);
			} else {
				Chat.broadcastDiscord(":tools: (dev server) There was an error!\n**" + message + "**" + stackToShow, true);
			}
			loggedErrors.add(message+stacktrace[0].toString());
		}
	}
	
	public void lookupPlayer(Player sender, UUID uuid, int pageNumber) {
		String username = Bukkit.getOfflinePlayer(uuid).getName();
		String lookupName = username;
		String discordName = DiscordSRVListener.getDiscordName(uuid);
		String nameStripped = lookupName;
		PlayerConfig lookupConfig = PlayerConfig.getConfig(uuid);
		OfflinePlayer lookupPlayer = Bukkit.getOfflinePlayer(uuid);
		if(lookupConfig.getString("Name") != null) {
			lookupName = lookupConfig.getString("Name");
			nameStripped = Chat.stripColor(lookupName);
		}
		
		if(sender == null) {
			//looking up from console
			Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Username: " + ChatColor.WHITE + username + (lookupConfig.getString("Name") != null ? " (aka " + ChatColor.translateAlternateColorCodes('&', lookupConfig.getString("Name")) + ChatColor.WHITE + ")" : ""));
			Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "UUID: " + ChatColor.WHITE + uuid.toString());
			if(discordName != null) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Discord: " + ChatColor.WHITE + discordName);
			}
			if(lookupConfig.getString("Town") != null) {
				TownConfig townConfig = TownConfig.getConfig(lookupConfig.getString("Town"));
				Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "Town: " + ChatColor.WHITE + lookupConfig.getString("Town") + (townConfig.getString("Alias") != null ? " (aka " + townConfig.getString("Alias") : ""));
			}
			return;
		}
		
		PlayerConfig senderConfig = PlayerConfig.getConfig(sender);
		
		sender.sendMessage("");
		sender.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(nameStripped, 46) + " " + ChatColor.translateAlternateColorCodes('&', lookupName) + ChatColor.YELLOW + " " + Chat.getTitlePlaceholder(nameStripped, 46));
		if(lookupConfig.getString("Name") != null && lookupConfig.getString("StaffRank") == null)
			sender.sendMessage(" - Username: " + ChatColor.GOLD + lookupPlayer.getName());
		if(discordName != null)
			sender.sendMessage(" - Discord: " + ChatColor.WHITE + discordName);
		
		//first join
		String firstJoin = DateCode.Encode(ZonedDateTime.ofInstant(Instant.ofEpochMilli(lookupPlayer.getFirstPlayed()), ZoneId.of("Australia/Darwin")), true, true, true, true, true, true);
		sender.sendMessage(" - joined on " + ChatColor.GOLD +
				DateCode.Decode(firstJoin, DateCode.Value.DAY_OF_MONTH)
				+ "/" + DateCode.Decode(firstJoin, DateCode.Value.MONTH)
				+ "/" + DateCode.Decode(firstJoin, DateCode.Value.YEAR)
				+ ChatColor.WHITE + " (" + DateCode.getDaysSince(firstJoin) + ChatColor.WHITE + " days ago).");
		
		//playtime
		DecimalFormat df = new DecimalFormat("####0.00");
		double daysPlayed = (((lookupPlayer.getStatistic(Statistic.PLAY_ONE_MINUTE) / 20.0) / 60.0) / 60.0) / 24.0;
		String playtime = df.format(daysPlayed) + ChatColor.WHITE + " days";
		if(daysPlayed <= 1) //days
			playtime = df.format(daysPlayed * 24.0) + "" + ChatColor.WHITE + " hours";
		
		sender.sendMessage(" - has " + ChatColor.GOLD + playtime + ChatColor.WHITE + " playtime.");
		
		DecimalFormat formatter = new DecimalFormat("#,###");
		sender.sendMessage(" - has killed " + ChatColor.GOLD + formatter.format(lookupConfig.getInt("Kills")) + ChatColor.WHITE + " zombies.");
		sender.sendMessage(" - has " + ChatColor.GOLD + Economy.convertCoins(lookupConfig.getInt("Coins")) + ChatColor.WHITE + ".");
		if(lookupConfig.getString("TasksCompleted") != null)
			sender.sendMessage(" - has completed " + ChatColor.GOLD + lookupConfig.getInt("TasksCompleted") + ChatColor.WHITE + " tasks.");
		if(lookupConfig.getString("MaxStreak") != null && lookupConfig.getInt("MaxStreak") != lookupConfig.getInt("TasksCompleted"))
			sender.sendMessage(" - has had a max streak of " + ChatColor.GOLD + lookupConfig.getInt("MaxStreak") + ChatColor.WHITE + " tasks.");
		int pets = Pets.getObtainedPets(lookupConfig).size();
		if(pets > 0)
			sender.sendMessage(" - has obtained " + ChatColor.GOLD + pets + ChatColor.WHITE + " of " + ChatColor.GOLD + Pet.values().length + ChatColor.WHITE + " pets.");
		if(lookupConfig.getString("Welcome") != null)
			sender.sendMessage(" - has welcomed " + ChatColor.GOLD + lookupConfig.getInt("Welcome") + ChatColor.WHITE + " players.");
		if(Bukkit.getPlayer(uuid) != null) {
			int timeSinceLastLogout = DateCode.getTimeSince(lookupConfig.getString("LastLogin"));
			String lastSeen = timeSinceLastLogout + "" + ChatColor.WHITE + " minutes";
			if(timeSinceLastLogout > 60)
				lastSeen = Math.round(timeSinceLastLogout / 60.0) + "" + ChatColor.WHITE + " hours";
			if(timeSinceLastLogout > 2880)
				lastSeen = Math.round(timeSinceLastLogout / 1440.0) + "" + ChatColor.WHITE + " days";
			
			sender.sendMessage(" - is " + ChatColor.GREEN + "ONLINE!" + ChatColor.WHITE + " (logged in " + ChatColor.GOLD + lastSeen + " ago).");
		} else {
			int timeSinceLastLogout = DateCode.getTimeSince(lookupConfig.getString("LastLogout"));
			String lastSeen = timeSinceLastLogout + "" + ChatColor.WHITE + " minutes";
			if(timeSinceLastLogout > 60)
				lastSeen = Math.round(timeSinceLastLogout / 60.0) + "" + ChatColor.WHITE + " hours";
			if(timeSinceLastLogout > 2880)
				lastSeen = Math.round(timeSinceLastLogout / 1440.0) + "" + ChatColor.WHITE + " days";
			
			sender.sendMessage(" - is " + ChatColor.RED + "OFFLINE" + ChatColor.WHITE + " (last seen " + ChatColor.GOLD + lastSeen + " ago).");
		}
		
		if(lookupConfig.getString("Town") != null)
			sender.sendMessage(" - is a " + Town.Rank.values()[lookupConfig.getInt("TownRank")].toString().replace("_", "-") + " of " + ChatColor.AQUA + Town.getTownDisplayName(lookupConfig.getString("Town")) + ".");
		if(lookupConfig.getInt("DonateRank") > 0) {
			if(lookupConfig.getString("StaffRank") != null)
				sender.sendMessage(" - is a server donator! " + ChatColor.GRAY + "[" + ChatColor.GREEN + "$" + ChatColor.GRAY + "]");
			else
				sender.sendMessage(" - is a server donator! " + ChatColor.GRAY + "[" + ChatColor.GREEN + "$" + ChatColor.GRAY + "] " + Donator.getPrefix(uuid));
		}
		if(lookupConfig.getString("StaffRank") != null)
			sender.sendMessage(" - is a server staff member! " + Donator.getPrefix(uuid));
		if(senderConfig.getString("StaffRank") != null //is staff:
				&& (lookupConfig.getString("StaffRank") == null //and player looking up isn't a staff
				|| sender.isOp())) {
			sender.sendMessage("");
			sender.sendMessage("Staff Only:");
			
			//last known IP:
			if(senderConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal()) {
				if(lookupConfig.getString("LastKnownIp.IP") != null)
					sender.sendMessage(" - last known IP: " + ChatColor.GOLD + lookupConfig.getString("LastKnownIp.IP") + ChatColor.WHITE + ChatColor.ITALIC + " (changed " + DateCode.getDaysSince(lookupConfig.getString("LastKnownIp.Date")) + " days ago" + ")");
				else
					sender.sendMessage(" - last known IP: " + ChatColor.RED + "not yet tracked");
			}
			
			//display mute offences:
			String muteOffences = "no";
			if(lookupConfig.getString("MuteOffences") != null)
				muteOffences = ChatColor.BOLD + "" + lookupConfig.getInt("MuteOffences");
			sender.sendMessage(" - has " + muteOffences + ChatColor.WHITE + " chat offences.");
			
			//is banned?
			if(lookupConfig.getString("Banned.Reason") != null) {
				Boolean stillBanned = true;
				
				//minutes passed since ban:
				int timeSinceBan = DateCode.getTimeSince(lookupConfig.getString("Banned.Start"));
				
				if(lookupConfig.getString("Banned.Time") != null) {
					//temp ban:
					if(timeSinceBan > (lookupConfig.getInt("Banned.Time") * 60)) {
						//ban has passed:
						
						Admin.logBan(uuid, Punishment.valueOf(lookupConfig.getString("Banned.Reason").replace(" ", "_")), lookupConfig.getString("Banned.Start"), lookupConfig.getInt("Banned.Time"));
						
						lookupConfig.set("Banned", null);
						lookupConfig.save();
						stillBanned = false;
					}
				}
				
				if(stillBanned) {
					
					sender.sendMessage(ChatColor.DARK_RED + " - Is banned for " + ChatColor.BOLD + lookupConfig.getString("Banned.Reason") + ":");
					
					//comments
					if(lookupConfig.getString("Banned.Comments") != null) {
						if(lookupConfig.getString("Banned.Punished by").equalsIgnoreCase("CONSOLE")) {
							sender.sendMessage(ChatColor.DARK_RED + "     - " + ChatColor.RED + "AUTOMATIC" + ChatColor.WHITE + ": " + ChatColor.ITALIC + "\"" + lookupConfig.getString("Banned.Comments") + "\"");
						} else {
							UUID punisher = UUID.fromString(lookupConfig.getString("Banned.Punished by"));
							PlayerConfig punisherConfig = PlayerConfig.getConfig(punisher);
							
							String nick = Bukkit.getOfflinePlayer(punisher).getName();
							if(punisherConfig.getString("Name") != null)
								nick = ChatColor.translateAlternateColorCodes('&', punisherConfig.getString("Name"));
							
							sender.sendMessage(ChatColor.DARK_RED + "     - " + ChatColor.RED + nick + ChatColor.WHITE + ": " + ChatColor.ITALIC + "\"" + lookupConfig.getString("Banned.Comments") + "\"");
						}
					}
					
					//punish ID (if existing)
					if(lookupConfig.getString("Banned.ActivityID") != null) {
						sender.sendMessage(ChatColor.DARK_RED + "     - " + ChatColor.WHITE + "Unpunish ID: " + ChatColor.GOLD + "/unpunish " + ChatColor.BOLD + lookupConfig.getInt("Banned.ActivityID"));
					}
					
					//timed ban?
					if(lookupConfig.getString("Banned.Time") != null) {
						
						int timeSentence = lookupConfig.getInt("Banned.Time"); //in hours
						String timeSentence_Plural = "";
						if(timeSentence < 24) //less than a day
							timeSentence_Plural = timeSentence == 1 ? "1 hour" : timeSentence + " hours";
						else //less than a week
							timeSentence_Plural = timeSentence < 48 ? "1 day" : (timeSentence / 24) + " days";
						
						int timeServed = timeSinceBan / 60; //in hours
						String timeServed_Plural = "";
						if(timeServed < 24) //less than a day
							timeServed_Plural = timeServed == 1 ? "1 hour" : timeServed + " hours";
						else //less than a week
							timeServed_Plural = timeServed < 48 ? "1 day" : (timeServed / 24) + " days";
						
						sender.sendMessage(ChatColor.DARK_RED + "     - " + ChatColor.RED + ChatColor.BOLD + timeServed_Plural + ChatColor.WHITE + " / " + ChatColor.GREEN + ChatColor.BOLD + timeSentence_Plural + ChatColor.WHITE + " served");
						
					}
					
				}
			}
			
			//ban history:
			int numberOfBans = lookupConfig.getString("BanHistory.Number of bans") == null ? 0 : lookupConfig.getInt("BanHistory.Number of bans");
			if(numberOfBans > 0) {
				sender.sendMessage(ChatColor.WHITE + " - has served " + ChatColor.BOLD + numberOfBans + ChatColor.WHITE + " ban sentences:");
				for(int count = 1; count <= numberOfBans; count++) {
					//1. 6 days ago - served 8 hours for ALIAS ABUSE
					int daysAgo = DateCode.getDaysSince(lookupConfig.getString("BanHistory." + count + ".Date"));
					
					int timeServed = lookupConfig.getInt("BanHistory." + count + ".HoursServed");
					String timeServed_Plural = "";
					if(timeServed < 24) //less than a day
						timeServed_Plural = timeServed == 1 ? "1 hour" : timeServed + " hours";
					else //less than a week
						timeServed_Plural = timeServed < 48 ? "1 day" : (timeServed / 24) + " days";
					
					String reason = Admin.Punishment.values()[lookupConfig.getInt("BanHistory." + count + ".Reason")].toString().replace("_", " ");
					
					sender.sendMessage("     - " + daysAgo + ChatColor.WHITE + " days ago" + " | Served " + ChatColor.RED + timeServed_Plural + ChatColor.WHITE + " for " + reason);
				}
			}
			
			sender.sendMessage("");
			//display aliases
			if(lookupConfig.getString("Aliases") != null) {
				List<String> aliases = lookupConfig.getStringList("Aliases");
				sender.sendMessage(" - has " + ChatColor.BOLD + aliases.size() + ChatColor.WHITE + " known aliases:");
				
				//display 5 names
				
				int aliasesPerPage = 5;
				
				int maxAliasToInclude = (aliasesPerPage + (pageNumber * aliasesPerPage)) - 1; //start at page 0
				int minAliasToInclude = pageNumber * aliasesPerPage;
				//0 - 4
				// 5 - 9
				
				//get towns on page
				List<String> aliasesOnPage = new ArrayList<String>();
				int count = 0;
				for(String alias : aliases) {
					if(count >= minAliasToInclude && count <= maxAliasToInclude) {
						aliasesOnPage.add(alias);
					}
					if(count > maxAliasToInclude) break;
					count++;
				}
				
				int numberOfPages = (int) Math.ceil(new Double(aliases.size() / aliasesPerPage));
				if(pageNumber > numberOfPages)
					pageNumber = numberOfPages;
				
				for(String aliasUUID : aliasesOnPage) {
					PlayerConfig aliasConfig = PlayerConfig.getConfig(UUID.fromString(aliasUUID));
					if(aliasConfig.getString("Banned.Reason") != null)
						sender.sendMessage("     - " + ChatColor.RED + Bukkit.getOfflinePlayer(UUID.fromString(aliasUUID)).getName() + ChatColor.WHITE + " (" + aliasConfig.getString("Banned.Reason") + ")");
					else
						sender.sendMessage("     - " + ChatColor.GOLD + Bukkit.getOfflinePlayer(UUID.fromString(aliasUUID)).getName());
				}
				
				if(aliases.size() > 5) {
					sender.sendMessage("Displaying page " + (pageNumber + 1) + ChatColor.WHITE + " of " + (numberOfPages + 1) + ".");
					sender.sendMessage("Use: " + ChatColor.GOLD + "/lookup " + lookupName+ " <page>");
				}
				
			} else {
				sender.sendMessage(" - has no known alias accounts.");
			}
		}
		sender.sendMessage(ChatColor.YELLOW + "================================================");
		sender.sendMessage("");
	}
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		if(commandLabel.equalsIgnoreCase("g") && !(sender instanceof Player)) {
			String message = "";
			for(int count = 0; count < args.length; count++) {
				message += args[count] + " ";
			}
			Chat.broadcastMessage(getFullChatPrefix(Bukkit.getOfflinePlayer("Thunder_Waffe").getUniqueId(), true) + ChatColor.GRAY + ": " + ChatColor.WHITE + message);
			return true;
		}
		
		Player player = null;
		if(sender instanceof Player) {
			player = (Player) sender;
			final PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(commandLabel.equalsIgnoreCase("r") || commandLabel.equalsIgnoreCase("m") || commandLabel.equalsIgnoreCase("w") || commandLabel.equalsIgnoreCase("msg") || commandLabel.equalsIgnoreCase("message") || commandLabel.equalsIgnoreCase("mail")) {
				
				if(!commandLabel.equalsIgnoreCase("r") && args.length <= 1) {
					//use /m <player> <message>
					player.sendMessage("");
					player.sendMessage(ChatColor.YELLOW + "[Mail] " + ChatColor.WHITE + "Send a private message with:");
					player.sendMessage(ChatColor.YELLOW + "[Mail] " + ChatColor.GOLD + "/msg <player> <message>");
					player.sendMessage("");
					return true;
				}
				
				//run asynchronously as we search all online players for nick names
				Player finalPlayer1 = player;
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {

					Player recipient = null;
					if(!commandLabel.equalsIgnoreCase("r")) {
						//get recipient
						
						for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
							String recordedName = PlaceHolders.playersName.get(onlinePlayer.getUniqueId().toString());
							if(recordedName != null
									&& stripColor(recordedName).equalsIgnoreCase(args[0])) {
								//used nick name
								recipient = onlinePlayer;
								break;
							}
						}
						if(recipient == null) recipient = Bukkit.getPlayer(args[0]);
						if(recipient == null) {
							finalPlayer1.sendMessage(ChatColor.YELLOW + "[Mail] " + ChatColor.RED + "'" + args[0] + "' is not online.");
							return;
						}
						if(recipient.getUniqueId().toString().equalsIgnoreCase(finalPlayer1.getUniqueId().toString())) {
							finalPlayer1.sendMessage(ChatColor.YELLOW + "[Mail] " + ChatColor.RED + "Cannot send mail to yourself.");
							return;
						}
					} else {
						if(!replyToMessage.containsKey(finalPlayer1.getName())) {
							finalPlayer1.sendMessage("");
							finalPlayer1.sendMessage(ChatColor.YELLOW + "[Mail] " + ChatColor.WHITE + "Send a private message with:");
							finalPlayer1.sendMessage(ChatColor.YELLOW + "[Mail] " + ChatColor.GOLD + "/msg <player> <message>");
							finalPlayer1.sendMessage("");
							return;
						} else {
							recipient = Bukkit.getPlayer(replyToMessage.get(finalPlayer1.getName()));
						}
						if(recipient == null) {
							finalPlayer1.sendMessage(ChatColor.YELLOW + "[Mail] " + ChatColor.RED + "'" + args[0] + "' is not online.");
							return;
						}
					}
					
					
					String message = "";
					int startingCount = commandLabel.equalsIgnoreCase("r") ? 0 : 1;
					for(int count = startingCount; count < args.length; count++) {
						String space = " ";
						if(count == args.length - 1) space = "";
						message += args[count] + space;
					}
					
					
					//if even cancelled, send to console
					Bukkit.getServer().getConsoleSender().sendMessage(finalPlayer1.getName() + " to " + recipient.getName() + ": " + message);
					
					String checkedMessage = checkMessage(message, finalPlayer1.getUniqueId(), null);
					if(checkedMessage != null) {
						
						if(playerConfig.getString("Coins") != null) { //player not in tutorial
							if(DeadMC.PlayerFile.data().getStringList("Active").contains(recipient.getUniqueId().toString())) { //player not in tutorial
								//royals+ can use colour in messages
								if(playerConfig.getInt("DonateRank") >= Rank.Royal.ordinal())
									checkedMessage = ChatColor.translateAlternateColorCodes('&', checkedMessage);
								
								String playersName = finalPlayer1.getName();
								if(playerConfig.getString("Name") != null)
									playersName = playerConfig.getString("Name");
								
								String recipientsName = recipient.getName();
								PlayerConfig onlineConfig = PlayerConfig.getConfig(recipient.getUniqueId());
								if(onlineConfig.getString("Name") != null)
									recipientsName = onlineConfig.getString("Name");
								
								List<String> ignoredUUIDs = new ArrayList<String>();
								if(onlineConfig.getString("Ignored") != null)
									ignoredUUIDs = onlineConfig.getStringList("Ignored");
								if(ignoredUUIDs.contains(finalPlayer1.getUniqueId().toString())) {
									finalPlayer1.sendMessage(ChatColor.YELLOW + "[Mail] " + ChatColor.RED + ChatColor.BOLD + recipientsName + ChatColor.RED + " is not accepting messages.");
									return;
								}
								
								recipient.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " => " + ChatColor.GRAY + "(from " + ChatColor.translateAlternateColorCodes('&', playersName) + ChatColor.GRAY + "): " + checkedMessage);
								finalPlayer1.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " => " + ChatColor.GRAY + "(to " + ChatColor.translateAlternateColorCodes('&', recipientsName) + ChatColor.GRAY + "): " + checkedMessage);
								
								if(replyToMessage.containsKey(recipient.getName()))
									replyToMessage.replace(recipient.getName(), finalPlayer1.getName());
								else
									replyToMessage.put(recipient.getName(), finalPlayer1.getName());
							}
							
							//play noise whenever someone messages
							if(!recipient.getName().equalsIgnoreCase(finalPlayer1.getName()))
								recipient.playSound(recipient.getLocation(), Sound.BLOCK_BELL_USE, 1f, 8f);
							
						}
						
					}
				});
			}
			
			if(commandLabel.equalsIgnoreCase("help") || commandLabel.equalsIgnoreCase("?") || commandLabel.equalsIgnoreCase("deadmc:?") || commandLabel.equalsIgnoreCase("deadmc:help")
					|| commandLabel.equalsIgnoreCase("h")) {
				
				if(Tools.floodgateEnabled()) {
					//CustomItems.openHelpBook(player);
				}
				
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder("Help", 46) + " " + "Help" + " " + Chat.getTitlePlaceholder("Help", 46));
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Rules:");
				player.sendMessage(ChatColor.GOLD + "/rules" + ChatColor.WHITE + " - A list of rules.");
				player.sendMessage(ChatColor.GOLD + "/ignore" + ChatColor.WHITE + " - Ignore chat from players.");
				player.sendMessage(ChatColor.GOLD + "/report <player>" + ChatColor.WHITE + " - Report a player.");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Toggle options:");
				player.sendMessage(ChatColor.GOLD + "/info" + ChatColor.WHITE + " - Toggle the info menu.");
				player.sendMessage(ChatColor.GOLD + "/bar" + ChatColor.WHITE + " - Toggle the Blood Moon bar.");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Things to do:");
				player.sendMessage(ChatColor.GOLD + "/task" + ChatColor.WHITE + " - Complete tasks for rewards.");
				player.sendMessage(ChatColor.GOLD + "/pets" + ChatColor.WHITE + " - Obtain pets!");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Economy:");
				player.sendMessage(ChatColor.GOLD + "/shops" + ChatColor.WHITE + " - Create player owned shops.");
				player.sendMessage(ChatColor.GOLD + "/find" + ChatColor.WHITE + " - Finds items in shops.");
				player.sendMessage(ChatColor.GOLD + "/coins" + ChatColor.WHITE + " - Check the amount of coins you have.");
				player.sendMessage(ChatColor.GOLD + "/pay <player>" + ChatColor.WHITE + " - Send coins to a player.");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Travel points:");
				player.sendMessage(ChatColor.GOLD + "/travel" + ChatColor.WHITE + " - Opens the travel menu.");
				player.sendMessage(ChatColor.GOLD + "/public" + ChatColor.WHITE + " - A list of player-created travel points.");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Towns & Land:");
				player.sendMessage(ChatColor.GOLD + "/town" + ChatColor.WHITE + " - Create or Join a town.");
				player.sendMessage(ChatColor.GOLD + "/land help" + ChatColor.WHITE + " - Claim and Manage land.");
				player.sendMessage(ChatColor.GOLD + "/nation" + ChatColor.WHITE + " - Create or Join a nation.");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Players & Competition:");
				player.sendMessage(ChatColor.GOLD + "/lookup <player>" + ChatColor.WHITE + " - Lookup player info.");
				player.sendMessage(ChatColor.GOLD + "/truces" + ChatColor.WHITE + " - Truce players to disable PvP.");
				player.sendMessage(ChatColor.GOLD + "/leaderboards" + ChatColor.WHITE + " - Player leaderboards.");
				player.sendMessage("");
				player.sendMessage(ChatColor.WHITE + "Need help? Join our " + ChatColor.BOLD + "Discord" + ChatColor.WHITE + ": " + ChatColor.GOLD + "discord.gg/CMpyUrs");
				player.sendMessage(ChatColor.WHITE + "Or join our website: " + ChatColor.GOLD + "www.DeadMC.com");
				if(playerConfig.getString("StaffRank") != null) {
					player.sendMessage("");
					player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Staff & Management:");
					player.sendMessage(ChatColor.GOLD + "/log" + ChatColor.WHITE + " - A log of player reports and staff activity.");
					player.sendMessage(ChatColor.GOLD + "/punish" + ChatColor.WHITE + " - Punish a player.");
					player.sendMessage(ChatColor.GOLD + "/unpunish" + ChatColor.WHITE + " - Cancel a players punishment.");
					player.sendMessage(ChatColor.GOLD + "/invsee" + ChatColor.WHITE + " - View a player's inventory.");
					player.sendMessage(ChatColor.GOLD + "/spec" + ChatColor.WHITE + " - Spectate a player (become invisible).");
					player.sendMessage(ChatColor.GOLD + "/vanish" + ChatColor.WHITE + " - Toggle invisibility and silent login/logout.");
					player.sendMessage(ChatColor.GOLD + "/sc" + ChatColor.WHITE + " - Chat to all online staff.");
					player.sendMessage(ChatColor.GOLD + "/comments" + ChatColor.WHITE + " - View /feedback comments from week.");
					player.sendMessage(ChatColor.GOLD + "/stat" + ChatColor.WHITE + " - View server stats from current week.");
					player.sendMessage(ChatColor.GOLD + "/stat <weeks back>" + ChatColor.WHITE + " - Compare stats to previous weeks.");
					if(playerConfig.getInt("StaffRank") >= StaffRank.MODERATOR.ordinal()) {
						player.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Moderator+:");
						player.sendMessage(ChatColor.GOLD + "/setname" + ChatColor.WHITE + " - Force set a player's name.");
						player.sendMessage(ChatColor.GOLD + "/dmc townname" + ChatColor.WHITE + " - Force set a town name.");
						player.sendMessage(ChatColor.GOLD + "/dmc bypass_alias_bans <player>" + ChatColor.WHITE + " - Let a player bypass their aliases' bans.");
						player.sendMessage(ChatColor.GOLD + "/dmc user-search" + ChatColor.WHITE + " - Find user names, nick names and UUID.");
					}
					if(playerConfig.getInt("StaffRank") >= StaffRank.SENIOR_MOD.ordinal()) {
						player.sendMessage(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "Sr Moderator+:");
						player.sendMessage(ChatColor.GOLD + "/dmc sethelper" + ChatColor.WHITE + " - Set/remove helper roles.");
						player.sendMessage(ChatColor.GOLD + "/dmc setstaff" + ChatColor.WHITE + " - Set/remove staff roles.");
						player.sendMessage(ChatColor.GOLD + "/dmc delete_region" + ChatColor.WHITE + " - Force delete a region.");
						player.sendMessage(ChatColor.GOLD + "/dmc delete_public_tp" + ChatColor.WHITE + " - Force delete a public TP.");
					}
					if(playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal()) {
						player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Admin+:");
						player.sendMessage(ChatColor.GOLD + "/restarter" + ChatColor.WHITE + " - Details about server health or force run a restart.");
						player.sendMessage(ChatColor.GOLD + "/backup" + ChatColor.WHITE + " - Run small, medium, big backups (with history).");
						player.sendMessage(ChatColor.GOLD + "/dmc transfer-account" + ChatColor.WHITE + " - (use with caution!) Erases everything from B, then moves everything from A, then erases everything from A.");
					}
				}
				player.sendMessage(ChatColor.YELLOW + "=================================================");
				player.sendMessage("");
				
				//TASK
				//if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.USE_HELP_COMMAND)) TaskManager.stepTask(player.getUniqueId());
				
			}
			
			//nation chat
			if(commandLabel.equalsIgnoreCase("nc")) {
				
				final Player finalPlayer = player;
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					if(args.length == 0) {
						//town chat toggled on / off
						//use /tc to disable. (enable again)
						if(nationChatToggled.contains(finalPlayer.getName())) {
							nationChatToggled.remove(finalPlayer.getName());
							finalPlayer.sendMessage("");
							finalPlayer.sendMessage(ChatColor.LIGHT_PURPLE + "[/nc] " + ChatColor.RED + "Nation chat toggled off.");
							finalPlayer.sendMessage(ChatColor.LIGHT_PURPLE + "[/nc] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/nc" + ChatColor.WHITE + " to enable again.");
							finalPlayer.sendMessage("");
						} else {
							nationChatToggled.add(finalPlayer.getName());
							finalPlayer.sendMessage("");
							finalPlayer.sendMessage(ChatColor.LIGHT_PURPLE + "[/nc] " + ChatColor.GREEN + "Nation chat toggled on.");
							finalPlayer.sendMessage(ChatColor.LIGHT_PURPLE + "[/nc] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/nc" + ChatColor.WHITE + " to disable.");
							finalPlayer.sendMessage("");
						}
						
						return;
					}
					
					String message = "";
					int startingCount = 0;
					for(int count = startingCount; count < args.length; count++) {
						String space = " ";
						if(count == args.length - 1) space = "";
						message += args[count] + space;
					}
					
					sendNationChat(finalPlayer.getUniqueId(), message, false);
				});
			}
			
			//town chat
			if(commandLabel.equalsIgnoreCase("tc")) {
				Player finalPlayer = player;
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					
					if(args.length == 0) {
						//town chat toggled on / off
						//use /tc to disable. (enable again)
						if(townChatToggled.contains(finalPlayer.getName())) {
							townChatToggled.remove(finalPlayer.getName());
							finalPlayer.sendMessage("");
							finalPlayer.sendMessage(ChatColor.AQUA + "[/tc] " + ChatColor.RED + "Town chat toggled off.");
							finalPlayer.sendMessage(ChatColor.AQUA + "[/tc] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/tc" + ChatColor.WHITE + " to enable again.");
							finalPlayer.sendMessage("");
						} else {
							townChatToggled.add(finalPlayer.getName());
							finalPlayer.sendMessage("");
							finalPlayer.sendMessage(ChatColor.AQUA + "[/tc] " + ChatColor.GREEN + "Town chat toggled on.");
							finalPlayer.sendMessage(ChatColor.AQUA + "[/tc] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/tc" + ChatColor.WHITE + " to disable.");
							finalPlayer.sendMessage("");
						}
						
						return;
					}
					
					String message = "";
					int startingCount = 0;
					for(int count = startingCount; count < args.length; count++) {
						String space = " ";
						if(count == args.length - 1) space = "";
						message += args[count] + space;
					}
					
					sendTownChat(finalPlayer.getUniqueId(), message, false);
				});
			}
			
			//staff chat:
			if(commandLabel.equalsIgnoreCase("sc")
					&& playerConfig.getString("StaffRank") != null) {
				Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
					@Override
					public void run() {
						final Player player = (Player) sender;
						
						if(args.length == 0) {
							//staff chat toggled on / off
							//use /sc to disable. (enable again)
							if(staffChatToggled.contains(player.getName())) {
								staffChatToggled.remove(player.getName());
								player.sendMessage("");
								player.sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.RED + "Staff chat toggled off.");
								player.sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/sc" + ChatColor.WHITE + " to enable again.");
								player.sendMessage("");
							} else {
								staffChatToggled.add(player.getName());
								player.sendMessage("");
								player.sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.GREEN + "Staff chat toggled on.");
								player.sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/sc" + ChatColor.WHITE + " to disable.");
								player.sendMessage("");
							}
							
							return;
						}
						
						String message = "";
						int startingCount = 0;
						for(int count = startingCount; count < args.length; count++) {
							String space = " ";
							if(count == args.length - 1) space = "";
							message += args[count] + space;
						}
						
						String playersName = player.getName();
						if(playerConfig.getString("Name") != null)
							playersName = playerConfig.getString("Name");
						
						//send message to player
						player.sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.GRAY + ChatColor.ITALIC + "you" + ChatColor.DARK_GREEN + ChatColor.BOLD + " => " + ChatColor.GREEN + message);
						
						//send message to all staff
						for(Player recipient : Bukkit.getOnlinePlayers()) {
							if(recipient.hasPermission("tab.staff") && !recipient.getName().equalsIgnoreCase(player.getName())) {
								recipient.sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', playersName) + ChatColor.DARK_GREEN + ChatColor.BOLD + " => " + ChatColor.GREEN + message);
							}
						}
					}
				});
			}
			
			if(commandLabel.equalsIgnoreCase("ignore") || commandLabel.equalsIgnoreCase("unignore")) {
				if(args.length == 0) {
					//run asynchronously as we search all online players for nick names
					Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
						@Override
						public void run() {
							Player player = (Player) sender;
							
							List<String> ignoredUUIDs = new ArrayList<String>();
							if(playerConfig.getString("Ignored") != null)
								ignoredUUIDs = playerConfig.getStringList("Ignored");
							
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "============== Ignore List ==============");
							if(ignoredUUIDs.size() > 0)
								player.sendMessage(ChatColor.WHITE + "You are ignoring " + ChatColor.RED + ignoredUUIDs.size() + ChatColor.WHITE + " players.");
							else
								player.sendMessage(ChatColor.WHITE + "You are not ignoring any players.");
							for(String uuid : ignoredUUIDs) {
								String originalName = Bukkit.getOfflinePlayer(UUID.fromString(uuid)).getName();
								PlayerConfig ignoredConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
								String playerName = originalName;
								if(ignoredConfig.getString("Name") != null)
									playerName = ChatColor.translateAlternateColorCodes('&', ignoredConfig.getString("Name")) + ChatColor.WHITE + " (" + originalName + ")";
								player.sendMessage(" - " + playerName);
							}
							player.sendMessage("");
							player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/unignore <player>" + ChatColor.WHITE + " to undo.");
							player.sendMessage(ChatColor.YELLOW + "=======================================");
							player.sendMessage("");
						}
					});
				}
				
				if(args.length > 0) {
					
					//run asynchronously as we search all online players for nick names
					Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
						@Override
						public void run() {
							Player player = (Player) sender;
							
							String playerName = args[0];
							for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
								PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
								if(onlineConfig.getString("Name") != null && stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[0])) {
									//used nick name
									playerName = onlinePlayer.getName();
									break;
								}
							}
							UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
							
							PlayerConfig lookupConfig = PlayerConfig.getConfig(uuid);
							
							if(lookupConfig.getString("Coins") != null) { //player exists
								
								if(uuid.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
									player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "Cannot ignore yourself.");
									return;
								}
								
								//if already ignored, unignore
								List<String> ignoredUUIDs = new ArrayList<String>();
								if(playerConfig.getString("Ignored") != null)
									ignoredUUIDs = playerConfig.getStringList("Ignored");
								
								if(ignoredUUIDs.contains(uuid.toString())) {
									player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "No longer ignoring " + ChatColor.BOLD + Bukkit.getOfflinePlayer(uuid).getName() + ChatColor.WHITE + ".");
									ignoredUUIDs.remove(uuid.toString());
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Ignoring " + ChatColor.BOLD + Bukkit.getOfflinePlayer(uuid).getName() + ChatColor.WHITE + ".");
									player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/unignore " + Bukkit.getOfflinePlayer(uuid).getName() + ChatColor.WHITE + " to undo.");
									player.sendMessage("");
									ignoredUUIDs.add(uuid.toString());
								}
								
								playerConfig.set("Ignored", ignoredUUIDs);
								playerConfig.save();
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "'" + playerName + "' is not an existing player name.");
							
						}
					});
					
				}
			}
		}
		
		if(commandLabel.equalsIgnoreCase("lookup")) {
			
			if(args.length > 0) {
				Player finalPlayer = player;
				Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
					String keyword = args[0];
					
					int pageNumber = 0;
					if(args.length > 1 && Chat.isInteger(args[1])) {
						pageNumber = Integer.parseInt(args[1]) - 1;
					} else {
						if(args.length > 1) {
							finalPlayer.sendMessage(ChatColor.YELLOW + "[Lookup] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/lookup " + args[0] + " <page number>");
							return;
						}
					}
					
					List<UUID> potentialUsers = new ArrayList<UUID>();
					
					for(String uuidString : DeadMC.PlayerFile.data().getStringList("Active")) {
						UUID uuid = UUID.fromString(uuidString);
						PlayerConfig lookupConfig = PlayerConfig.getConfig(uuid);
						String username = Bukkit.getOfflinePlayer(uuid).getName();
						if(username == null) continue;
						String nickname = lookupConfig.getString("Name"); //can be null
						
						//1) is it a full username? break
						if(username.equalsIgnoreCase(keyword)) {
							lookupPlayer(finalPlayer, uuid, pageNumber);
							return;
						}
						
						//lookup has a nick name?
						if(nickname != null) {
							String stripped = Chat.stripColor(lookupConfig.getString("Name"));
							
							//2) is it a full nick name? break
							if(stripped.equalsIgnoreCase(keyword)) {
								lookupPlayer(finalPlayer, uuid, pageNumber);
								return;
							}
							
							//3) does the nickname contain the keyword?
							if(stripped.toLowerCase().contains(keyword)) {
								potentialUsers.add(uuid);
							}
						}
						
						//4) does the username contain the keyword?
						if(!potentialUsers.contains(uuid) && username.toLowerCase().contains(keyword)) {
							potentialUsers.add(uuid);
						}
					}
					
					//if got to here, there was no exact lookup
					
					if(potentialUsers.size() == 0) {
						if(sender instanceof Player) finalPlayer.sendMessage(ChatColor.YELLOW + "[Lookup] " + ChatColor.WHITE + "No users found matching '" + keyword + "'.");
						else Bukkit.getConsoleSender().sendMessage("No users found.");
						return;
					}
					if(potentialUsers.size() == 1) { //only 1 result, so look it up
						lookupPlayer(finalPlayer, potentialUsers.get(0), pageNumber);
						return;
					}
					
					//multiple results...
					
					if(sender instanceof Player) {
						finalPlayer.sendMessage("");
						finalPlayer.sendMessage(ChatColor.YELLOW + "[Lookup] " + ChatColor.WHITE + ChatColor.BOLD + potentialUsers.size() + ChatColor.WHITE + " players found:");
						
						int count = 0;
						for(UUID uuid : potentialUsers) {
							String name = Bukkit.getOfflinePlayer(uuid).getName();
							PlayerConfig resultConfig = PlayerConfig.getConfig(uuid);
							finalPlayer.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + name + (resultConfig.getString("Name") != null ? " (aka " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', resultConfig.getString("Name")) + ChatColor.WHITE + ")" : ""));
							count++;
							
							if(count >= 10) {
								finalPlayer.sendMessage(ChatColor.WHITE + "...and " + (potentialUsers.size()-10) + " more...");
								break;
							}
						}
						finalPlayer.sendMessage(ChatColor.YELLOW + "[Lookup] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/lookup <player>" + ChatColor.WHITE + " for more info.");
						finalPlayer.sendMessage("");
					} else {
						for(UUID uuid : potentialUsers) {
							String name = Bukkit.getOfflinePlayer(uuid).getName();
							PlayerConfig resultConfig = PlayerConfig.getConfig(uuid);
							Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " > " + ChatColor.WHITE + name + (resultConfig.getString("Name") != null ? ChatColor.BOLD + " | " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', resultConfig.getString("Name")) : ""));
						}
					}
					
				});
				
			} else player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/lookup <player>" + ChatColor.WHITE + ".");
			
		}
		
		if(commandLabel.equalsIgnoreCase("rules")) {
			player.sendMessage("");
			player.sendMessage(ChatColor.YELLOW + "================ DeadMC Rules ================");
			player.sendMessage(ChatColor.GOLD + "1. NO HACKS OR MODS");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "Please disable any modifications that will give a");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "benefit over vanilla players to avoid punishment.");
			player.sendMessage(ChatColor.GOLD + "2. NO ABUSE OF BUGS");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "Please report any potential bugs (of all types).");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "Abuse of bugs can lead to account rollback/reset/bans.");
			player.sendMessage(ChatColor.GOLD + "3. RESPECTFUL CHAT");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "Use chat features as if it were the real world.");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "  No offensive or threatening language.");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "  No spamming (caps spam, flooding etc.).");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "  Only supporters may share links. No advertising.");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "  English only in main chat (excluding private chats).");
			player.sendMessage(ChatColor.GOLD + "4. PUBLIC NUISANCE");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "Actions that intend to cause harm to the server,");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "or offense to other players, will NOT BE TOLERATED.");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "This includes TP killing and/or scamming.");
			player.sendMessage(ChatColor.GOLD + "5. ACCOUNT ALIAS ABUSE");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "Do not use multiple accounts in order to gain a");
			player.sendMessage(ChatColor.GOLD + "   " + ChatColor.WHITE + "benefit over other players to avoid punishment.");
			player.sendMessage(ChatColor.BOLD + "Flag/report a player with " + ChatColor.GOLD + "/report" + ChatColor.WHITE + ".");
			player.sendMessage(ChatColor.WHITE + "False reporting can lead to a ban.");
			player.sendMessage(ChatColor.YELLOW + "=============================================");
			player.sendMessage("");
		}
		
		if(commandLabel.equalsIgnoreCase("leaderboards") || commandLabel.equalsIgnoreCase("leaderboard") || commandLabel.equalsIgnoreCase("lb")) {
			if(args.length == 0) {
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + "=============== Leaderboards ===============");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "coins" + ChatColor.WHITE + " - The " + ChatColor.BOLD + "richest" + ChatColor.WHITE + " players.");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "kills" + ChatColor.WHITE + " - The " + ChatColor.BOLD + "strongest" + ChatColor.WHITE + " players.");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "playtime" + ChatColor.WHITE + " - The most " + ChatColor.BOLD + "dedicated" + ChatColor.WHITE + " players.");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "welcome" + ChatColor.WHITE + " - The " + ChatColor.BOLD + "friendliest" + ChatColor.WHITE + " players.");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "votes" + ChatColor.WHITE + " - The most " + ChatColor.BOLD + "rewarded" + ChatColor.WHITE + " players.");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "skills" + ChatColor.WHITE + " - The most " + ChatColor.BOLD + "skillful" + ChatColor.WHITE + " players.");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "towns" + ChatColor.WHITE + " - The " + ChatColor.BOLD + "greatest towns" + ChatColor.WHITE + ".");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "tasks" + ChatColor.WHITE + " - The most " + ChatColor.BOLD + "adventurous" + ChatColor.WHITE + " players.");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "streak" + ChatColor.WHITE + " - The most " + ChatColor.BOLD + "hard-working" + ChatColor.WHITE + " players.");
				player.sendMessage("");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "bmleader" + ChatColor.WHITE + " - All-time " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " leader.");
				player.sendMessage(ChatColor.GOLD + "/lb " + ChatColor.BOLD + "bmkills" + ChatColor.WHITE + " - The most " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + " kills.");
				player.sendMessage(ChatColor.YELLOW + "============================================");
				player.sendMessage("");
			} else {
				if(args[0].equalsIgnoreCase("skills")) {
					Bukkit.dispatchCommand(player, "mctop");
					return true;
				}
				if(args[0].equalsIgnoreCase("towns")) {
					Bukkit.dispatchCommand(player, "town list");
					return true;
				}
				if(args[0].equalsIgnoreCase("coins") || args[0].equalsIgnoreCase("kills") || args[0].equalsIgnoreCase("votes") || args[0].equalsIgnoreCase("votes_month") || args[0].equalsIgnoreCase("playtime") || args[0].equalsIgnoreCase("welcome") || args[0].equalsIgnoreCase("tasks") || args[0].equalsIgnoreCase("streak") || args[0].equalsIgnoreCase("bmleader") || args[0].equalsIgnoreCase("bmkills")) {
					Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
						@Override
						public void run() {
							try {
								
								Player player = (Player) sender;
								
								FileConfiguration leaderboard = DeadMC.LeaderboardFile.data();
								
								String type = args[0].toUpperCase();
								
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder("Top 10 - " + type, 46) + " Top 10 - " + type + " " + Chat.getTitlePlaceholder("Top 10 - " + type, 46));
								
								for(int count = 1; count <= 10; count++) {
									if(leaderboard.getString(type + "." + count + ".UUID") != null) {
										String amountString = "";
										if(type.equalsIgnoreCase("coins"))
											amountString = Economy.convertCoins(leaderboard.getInt(type + "." + count + ".Amount"));
										DecimalFormat formatter = new DecimalFormat("#,###");
										if(type.equalsIgnoreCase("kills") || type.equalsIgnoreCase("bmkills"))
											amountString = formatter.format(leaderboard.getInt(type + "." + count + ".Amount")) + " zombies";
										if(type.equalsIgnoreCase("playtime")) {
											DecimalFormat df = new DecimalFormat("####0.00");
											amountString = df.format((leaderboard.getInt(type + "." + count + ".Amount") / 60.0) / 24.0) + " days";
										}
										if(type.equalsIgnoreCase("tasks"))
											amountString = leaderboard.getInt(type + "." + count + ".Amount") + " tasks";
										if(type.equalsIgnoreCase("streak"))
											amountString = leaderboard.getInt(type + "." + count + ".Amount") + " tasks";
										if(type.equalsIgnoreCase("bmleader"))
											amountString = leaderboard.getInt(type + "." + count + ".Amount") + " nights";
										if(type.equalsIgnoreCase("welcome"))
											amountString = leaderboard.getInt(type + "." + count + ".Amount") + " players";
										if(type.equalsIgnoreCase("votes") || type.equalsIgnoreCase("votes_month"))
											amountString = leaderboard.getInt(type.toString() + "." + count + ".Amount") + " votes";
										
										OfflinePlayer leader = Bukkit.getOfflinePlayer(UUID.fromString(leaderboard.getString(type + "." + count + ".UUID")));
										PlayerConfig leaderConfig = PlayerConfig.getConfig(leader);
										String leaderName = leader.getName();
										if(leaderConfig.getString("Name") != null)
											leaderName = leaderConfig.getString("Name");
										player.sendMessage(ChatColor.BOLD + "  " + ChatColor.AQUA + count + ChatColor.WHITE + " -> " + Chat.stripColor(leaderName) + " (" + ChatColor.GOLD + amountString + ChatColor.WHITE + ")");
									}
								}
								
								player.sendMessage(ChatColor.YELLOW + "============================================");
								player.sendMessage("");
								
							} catch(Exception e) {
								Chat.logError("**Issue displaying leaderboard**: " + e.getMessage());
							}
						}
					});
				} else
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "There is no leaderboard for '" + args[0] + "'.");
			}
		}
		
		if(commandLabel.equalsIgnoreCase("tell") || commandLabel.equalsIgnoreCase("me")) {
			return true;
		}
		
		return true;
	}
	
	public enum Leaderboard {
		COINS,
		KILLS,
		PLAYTIME,
		TASKS,
		STREAK,
		BMLEADER,
		BMKILLS,
		CURRENTBLOODMOON,
		WELCOME,
		VOTES,
		VOTES_MONTH
	}
	
	//use updateLeaderHeads(type) to reload a leaderboard instead
	public static void initialiseLeaderboards(Leaderboard type) {
		if(DeadMC.RestartFile.data().getString("Maintenance") != null) return;
		
		//load leader board heads
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				try {
				
				FileConfiguration leaderboard = DeadMC.LeaderboardFile.data();

				for(int count = 1; count <= 10; count++) {
					String headLocation = DeadMC.LeaderboardFile.data().getString(type.toString() + "." + count + ".HeadLoc");

					if(headLocation != null) {
						Location hologramLocation = LocationCode.Decode(headLocation).add(0, 1.9, 0);

						UUID leader = UUID.fromString(leaderboard.getString(type.toString() + "." + count + ".UUID"));

						final int currentCount = count;

						//temporary fix to ensure chunk is loaded:
						Location headLoc = LocationCode.Decode(headLocation);
						Chunk chunk = null;
						try {
							chunk = PaperLib.getChunkAtAsync(headLoc).get();
						} catch(InterruptedException | ExecutionException e) {
							e.printStackTrace();
						}
						Chunk finalChunk = chunk;
						if(chunk == null) {
							Chat.debug("[Leaderhead] Could not get chunk for hologram!");
							return;
						}
						Bukkit.getScheduler().runTask(plugin, () -> {
							finalChunk.setForceLoaded(true);
							finalChunk.load();
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
								long timePassedMs = 0;
								while(!finalChunk.isLoaded()) {
									Chat.debug(org.bukkit.ChatColor.RED + "Chunk isn't loaded. Trying again in 500ms");
									Tools.sleepThread(500); //wait half a second to try again
									timePassedMs += 500;

									if(timePassedMs > 30000) { //30 seconds
										Chat.debug(org.bukkit.ChatColor.RED + "Cancelled trying to load chunk entities.");
										break;
									}
								}

								//update the head
								Bukkit.getScheduler().runTask(plugin, () -> {
									final Collection<Entity> nearbyEntities = headLoc.getWorld().getNearbyEntities(headLoc, 1.0, 1.0, 1.0);
									ArmorStand armorStand = null;
									for(Entity entity : nearbyEntities) {
										Chat.debug("Nearby entity " + entity.getLocation().toString(), "lbhead");
										if(LocationCode.Encode(entity.getLocation(), true).equalsIgnoreCase(LocationCode.Encode(headLoc, true))) {
											Chat.debug("Found!", "lbhead");
											armorStand = (ArmorStand) entity;
											break;
										}
									}
									if(armorStand == null) {
										Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "[" + type.toString() + "] " + ChatColor.RED + "There is no armor stand at " + headLocation + " for #" + currentCount + ".");
										return;
									}
									ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
									SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
									skullMeta.setOwningPlayer(Bukkit.getOfflinePlayer(leader));
									skull.setItemMeta(skullMeta);
									armorStand.getEquipment().setHelmet(skull);

									Location blockBehind = armorStand.getWorld().getBlockAt(armorStand.getLocation()).getRelative(armorStand.getFacing().getOppositeFace()).getLocation();
									//setup leaderboard hologram
									if(currentCount == 1) {

										final Hologram title = HologramsAPI.createHologram(plugin, blockBehind.add(0.5, 5.5, 0.5));

										String hologramTitle = leaderboard.getString(type.toString() + "." + currentCount + ".HologramTitle");
										if(hologramTitle != null)
											title.appendTextLine(ChatColor.GOLD + "" + ChatColor.ITALIC + "Top 10 - " + ChatColor.GOLD + ChatColor.BOLD + hologramTitle.replace("_", " "));
										else
											title.appendTextLine(ChatColor.GOLD + "" + ChatColor.ITALIC + "Top 10 - " + ChatColor.GOLD + ChatColor.BOLD + "set title");

										title.appendTextLine("Use " + ChatColor.GOLD + ChatColor.BOLD + "/lb " + type.toString().toLowerCase());
										title.appendTextLine(ChatColor.YELLOW + "=============================");
										for(int lb = 1; lb <= 10; lb++) {
											if(leaderboard.getString(type.toString() + "." + lb + ".UUID") != null) {
												UUID uuid = UUID.fromString(leaderboard.getString(type.toString() + "." + lb + ".UUID"));
												OfflinePlayer playerInPos = Bukkit.getOfflinePlayer(uuid);
												PlayerConfig posConfig = PlayerConfig.getConfig(uuid);
												String playerInPosName = playerInPos.getName();
												if(posConfig.getString("Name") != null)
													playerInPosName = posConfig.getString("Name");

												String amountString = "";
												if(type.toString().equalsIgnoreCase("coins"))
													amountString = Economy.convertCoins(leaderboard.getInt(type.toString() + "." + lb + ".Amount"));
												DecimalFormat formatter = new DecimalFormat("#,###");
												if(type.toString().equalsIgnoreCase("kills") || type.toString().equalsIgnoreCase("bmkills"))
													amountString = formatter.format(leaderboard.getInt(type.toString() + "." + lb + ".Amount")) + " zombies";
												if(type.toString().equalsIgnoreCase("playtime"))
													amountString = Math.round(leaderboard.getInt(type.toString() + "." + lb + ".Amount") / 60.0) + " hours";
												if(type.toString().equalsIgnoreCase("tasks"))
													amountString = leaderboard.getInt(type.toString() + "." + lb + ".Amount") + " tasks";
												if(type.toString().equalsIgnoreCase("streak"))
													amountString = leaderboard.getInt(type.toString() + "." + lb + ".Amount") + " tasks";
												if(type.toString().equalsIgnoreCase("bmleader"))
													amountString = leaderboard.getInt(type.toString() + "." + lb + ".Amount") + " nights";
												if(type.toString().equalsIgnoreCase("welcome"))
													amountString = leaderboard.getInt(type.toString() + "." + lb + ".Amount") + " players";
												if(type.toString().equalsIgnoreCase("votes") || type.toString().equalsIgnoreCase("votes_month"))
													amountString = leaderboard.getInt(type.toString() + "." + lb + ".Amount") + " votes";

												String number = ChatColor.GRAY + "" + ChatColor.BOLD + lb + ". ";
												if(lb == 1)
													number = ChatColor.AQUA + "" + ChatColor.BOLD + lb + ChatColor.GRAY + ". ";
												if(lb == 2)
													number = ChatColor.GOLD + "" + ChatColor.BOLD + lb + ChatColor.GRAY + ". ";
												if(lb == 3)
													number = ChatColor.WHITE + "" + ChatColor.BOLD + lb + ChatColor.GRAY + ". ";

												String finalPlayerInPosName = playerInPosName;
												String finalNumber = number;
												String finalAmountString = amountString;
												title.appendTextLine(finalNumber + ChatColor.WHITE + Chat.stripColor(finalPlayerInPosName) + " (" + ChatColor.GOLD + finalAmountString + ChatColor.WHITE + ")");
											}
										}
										title.appendTextLine(ChatColor.YELLOW + "=============================");
									}
								});
							});
						});

						//create new hologram
						PlayerConfig leaderConfig = PlayerConfig.getConfig(leader);
						String townRank = leaderConfig.getString("Town") == null ? "" : Town.getTownRankPrefix(leader);
						String townName = leaderConfig.getString("Town") == null ? "" : Town.getTownDisplayName(leader);
						String name = leaderConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(leader).getName() : leaderConfig.getString("Name");

						String place = ChatColor.AQUA + "" + ChatColor.BOLD + "1st";
						if(count == 2) place = ChatColor.GOLD + "" + ChatColor.BOLD + "2nd";
						if(count == 3) place = ChatColor.WHITE + "" + ChatColor.BOLD + "3rd";
						final String placeString = place;

						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
							@Override
							public void run() {
								Hologram newHologram = HologramsAPI.createHologram(plugin, hologramLocation);
								newHologram.appendTextLine(placeString);
								if(leaderConfig.getString("Town") != null)
									newHologram.appendTextLine(townRank + townName);
								newHologram.appendTextLine(name);
							}
						}, 100L);

					} else return;
				}
				
				} catch(Exception e) {
					Chat.logError("**Issue initialising leaderboard**: " + e.getMessage());
				}
			}
		});
		
	}
	
	//update leader board heads
	public static void updateLeaderHeads(Leaderboard type) {
		if(DeadMC.RestartFile.data().getString("Maintenance") != null) return;
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				
				try {
				FileConfiguration leaderboard = DeadMC.LeaderboardFile.data();

				for(int count = 1; count <= 10; count++) {
					String headLocationString = leaderboard.getString(type.toString() + "." + count + ".HeadLoc");

					if(headLocationString != null) { //does the leaderboard position have a leader head?
						Location headLocation = LocationCode.Decode(headLocationString);
						Location hologramLocation = headLocation.clone().add(0, 1.9, 0);

						//loop all active holograms made by DeadMC (will only be leaderboard ones)
						for(Hologram hologram : HologramsAPI.getHolograms(plugin)) {
							if(LocationCode.Encode(hologram.getLocation(), true).equalsIgnoreCase(LocationCode.Encode(hologramLocation, true))) {
								//this is the hologram

								UUID leader = UUID.fromString(leaderboard.getString(type.toString() + "." + count + ".UUID"));

								//update the head
								int finalCount = count;
								Bukkit.getScheduler().runTask(plugin, new Runnable() {
									@Override
									public void run() {

										ArmorStand armorStand = null;
										for(Entity entity : headLocation.getWorld().getNearbyEntities(headLocation, 1.0, 1.0, 1.0)) {
											if(LocationCode.Encode(entity.getLocation(), true).equalsIgnoreCase(LocationCode.Encode(headLocation, true))) {
												armorStand = (ArmorStand) entity;
												break;
											}
										}
										if(armorStand == null) {
											Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "[" + type.toString() + "] " + ChatColor.RED + "There is no armor stand at " + headLocation + " for #" + finalCount + ".");
											return;
										}

										ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
										SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
										skullMeta.setOwningPlayer(Bukkit.getOfflinePlayer(leader));
										skull.setItemMeta(skullMeta);
										armorStand.getEquipment().setHelmet(skull);
									}
								});

								//create new hologram
								PlayerConfig leaderConfig = PlayerConfig.getConfig(leader);
								String townRank = leaderConfig.getString("Town") == null ? "" : Town.getTownRankPrefix(leader);
								String townName = leaderConfig.getString("Town") == null ? "" : Town.getTownDisplayName(leader);
								String name = leaderConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(leader).getName() : ChatColor.translateAlternateColorCodes('&', leaderConfig.getString("Name"));

								String place = ChatColor.AQUA + "" + ChatColor.BOLD + "1st";
								if(count == 2)
									place = ChatColor.GOLD + "" + ChatColor.BOLD + "2nd";
								if(count == 3)
									place = ChatColor.WHITE + "" + ChatColor.BOLD + "3rd";
								final String placeString = place;

								Bukkit.getScheduler().runTask(plugin, new Runnable() {
									@Override
									public void run() {
										Hologram newHologram = HologramsAPI.createHologram(plugin, hologramLocation);
										newHologram.appendTextLine(ChatColor.AQUA + "" + ChatColor.BOLD + placeString);
										if(leaderConfig.getString("Town") != null)
											newHologram.appendTextLine(townRank + townName);
										newHologram.appendTextLine(name);

										//just delete old one
										hologram.delete();
									}
								});

								break;
							}
						}
					} else return;
				}
				
				} catch(Exception e) {
					Chat.logError("**Issue updating leaderheads**: " + e.getMessage());
				}
			}
		});
	}
	
	public static void updateHologramBoard(Leaderboard type) {
		if(DeadMC.RestartFile.data().getString("Maintenance") != null) return;
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				
				try {
					FileConfiguration leaderboard = DeadMC.LeaderboardFile.data();
					String headLocationString = leaderboard.getString(type.toString() + "." + 1 + ".HeadLoc"); //always relative to first position
					
					if(headLocationString != null) {
						Location headLocation = LocationCode.Decode(headLocationString);
						
						//loop all active holograms made by DeadMC (will only be leaderboard ones)
						Bukkit.getScheduler().runTask(plugin, new Runnable() {
							@Override
							public void run() {
								ArmorStand armorStand = null;
								for(Entity entity : headLocation.getWorld().getNearbyEntities(headLocation, 1.0, 1.0, 1.0)) {
									if(LocationCode.Encode(entity.getLocation(), true).equalsIgnoreCase(LocationCode.Encode(headLocation, true))) {
										armorStand = (ArmorStand) entity;
										break;
									}
								}
								if(armorStand == null) {
									Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "[" + type.toString() + "] " + ChatColor.RED + "There is no armor stand at " + headLocation + " for #1.");
									return;
								}
								
								Location blockBehind = armorStand.getWorld().getBlockAt(armorStand.getLocation()).getRelative(armorStand.getFacing().getOppositeFace()).getLocation();
								Location titleLocation = blockBehind.clone().add(0.5, 5.5, 0.5);
								
								for(Hologram hologram : HologramsAPI.getHolograms(plugin)) {
									
									if(LocationCode.Encode(hologram.getLocation(), true).equalsIgnoreCase(LocationCode.Encode(titleLocation, true))) {
										//this is the title hologram
										
										hologram.delete(); //delete old one
										
										final Hologram title = HologramsAPI.createHologram(plugin, titleLocation);
										
										String hologramTitle = leaderboard.getString(type.toString() + "." + 1 + ".HologramTitle");
										if(hologramTitle != null)
											title.appendTextLine(ChatColor.GOLD + "" + ChatColor.ITALIC + "Top 10 - " + ChatColor.GOLD + ChatColor.BOLD + hologramTitle.replace("_", " "));
										else
											title.appendTextLine(ChatColor.GOLD + "" + ChatColor.ITALIC + "Top 10 - " + ChatColor.GOLD + ChatColor.BOLD + "set title");
										
										title.appendTextLine("Use " + ChatColor.GOLD + ChatColor.BOLD + "/lb " + type.toString().toLowerCase());
										title.appendTextLine(ChatColor.YELLOW + "=============================");
										for(int count = 1; count <= 10; count++) {
											if(leaderboard.getString(type.toString() + "." + count + ".UUID") != null) {
												UUID uuid = UUID.fromString(leaderboard.getString(type.toString() + "." + count + ".UUID"));
												OfflinePlayer playerInPos = Bukkit.getOfflinePlayer(uuid);
												PlayerConfig posConfig = PlayerConfig.getConfig(uuid);
												String playerInPosName = playerInPos.getName();
												if(posConfig.getString("Name") != null)
													playerInPosName = posConfig.getString("Name");
												
												String amountString = "";
												if(type.toString().equalsIgnoreCase("coins"))
													amountString = Economy.convertCoins(leaderboard.getInt(type.toString() + "." + count + ".Amount"));
												DecimalFormat formatter = new DecimalFormat("#,###");
												if(type.toString().equalsIgnoreCase("kills") || type.toString().equalsIgnoreCase("bmkills"))
													amountString = formatter.format(leaderboard.getInt(type.toString() + "." + count + ".Amount")) + " zombies";
												if(type.toString().equalsIgnoreCase("playtime"))
													amountString = Math.round(leaderboard.getInt(type.toString() + "." + count + ".Amount") / 60.0) + " hours";
												if(type.toString().equalsIgnoreCase("tasks"))
													amountString = leaderboard.getInt(type.toString() + "." + count + ".Amount") + " tasks";
												if(type.toString().equalsIgnoreCase("streak"))
													amountString = leaderboard.getInt(type.toString() + "." + count + ".Amount") + " tasks";
												if(type.toString().equalsIgnoreCase("bmleader"))
													amountString = leaderboard.getInt(type.toString() + "." + count + ".Amount") + " nights";
												if(type.toString().equalsIgnoreCase("welcome"))
													amountString = leaderboard.getInt(type.toString() + "." + count + ".Amount") + " players";
												if(type.toString().equalsIgnoreCase("votes") || type.toString().equalsIgnoreCase("votes_month"))
													amountString = leaderboard.getInt(type.toString() + "." + count + ".Amount") + " votes";
												
												String number = ChatColor.GRAY + "" + ChatColor.BOLD + count + ". ";
												if(count == 1)
													number = ChatColor.AQUA + "" + ChatColor.BOLD + count + ChatColor.GRAY + ". ";
												if(count == 2)
													number = ChatColor.GOLD + "" + ChatColor.BOLD + count + ChatColor.GRAY + ". ";
												if(count == 3)
													number = ChatColor.WHITE + "" + ChatColor.BOLD + count + ChatColor.GRAY + ". ";
												
												title.appendTextLine(number + ChatColor.WHITE + Chat.stripColor(playerInPosName) + " (" + ChatColor.GOLD + amountString + ChatColor.WHITE + ")");
											}
										}
										title.appendTextLine(ChatColor.YELLOW + "=============================");
										return;
									}
									
								}
								
							}
						});
						
					} else return;
					
				} catch(Exception e) {
					Chat.logError("**Issue updating hologram board for leaderboard**: " + e.getMessage());
				}
			}
		});
	}
	
	//force update all of a players details
	public static void updateUserLeaderboardPrefix(UUID uuid) {
		if(DeadMC.RestartFile.data().getString("Maintenance") != null) return;
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				FileConfiguration leaderboard = DeadMC.LeaderboardFile.data();
				for(Leaderboard type : Leaderboard.values()) {
					for(int count = 1; count <= 3; count++) {
						if(leaderboard.getString(type.toString() + "." + count + ".HeadLoc") == null)
							break; //no head for first place
						if(leaderboard.getString(type.toString() + "." + count + ".UUID").equalsIgnoreCase(uuid.toString())) {
							//update head
							updateLeaderHeads(type);
							updateHologramBoard(type);
						}
					}
				}
			}
		});
	}
	
	public static void updateLeaderBoard(Leaderboard type, UUID uuid) {
		if(DeadMC.RestartFile.data().getString("Maintenance") != null) return;
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				
				try {
				PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
				
				if(Bukkit.getOfflinePlayer(uuid).isOp() && DeadMC.RestartFile.data().getString("IsDevelopmentServer") == null) return;
				
				FileConfiguration leaderboard = DeadMC.LeaderboardFile.data();
				
				int value = 0;
				if(type == Leaderboard.COINS)
					value = playerConfig.getInt("Coins");
				if(type == Leaderboard.KILLS)
					value = playerConfig.getInt("Kills");
				if(type == Leaderboard.PLAYTIME) {
					OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
					value = (int)Math.round((offlinePlayer.getStatistic(Statistic.PLAY_ONE_MINUTE) / 20.0) / 60.0);
				} if(type == Leaderboard.WELCOME)
					value = playerConfig.getInt("Welcome");
				if(type == Leaderboard.TASKS)
					value = playerConfig.getInt("TasksCompleted");
				if(type == Leaderboard.STREAK)
					value = playerConfig.getInt("MaxStreak");
				if(type == Leaderboard.BMKILLS)
					value = playerConfig.getInt("BloodMoonKills");
				if(type == Leaderboard.BMLEADER)
					value = playerConfig.getInt("BloodMoonLeader");
				if(type == Leaderboard.CURRENTBLOODMOON)
					value = playerConfig.getInt("BM_CurrentKills");
				if(type == Leaderboard.VOTES) {
					String votes = "%VotingPlugin_Total_AllTime%";
					votes = PlaceholderAPI.setPlaceholders(Bukkit.getOfflinePlayer(uuid), votes);
					value = Integer.parseInt(votes);
				}
				if(type == Leaderboard.VOTES_MONTH) {
					String votes = "%VotingPlugin_Total_Monthly%";
					votes = PlaceholderAPI.setPlaceholders(Bukkit.getOfflinePlayer(uuid), votes);
					value = Integer.parseInt(votes);
				}
				
				int currentPlace = 0;
				for(int count = 10; count > 0; count--) {
					try {
						if(leaderboard.getString(type.toString() + "." + count + ".UUID") != null
								&& leaderboard.getString(type.toString() + "." + count + ".UUID").equalsIgnoreCase(uuid.toString())) {
							currentPlace = count;
						}
					} catch(Exception e) {
						Chat.logError("**Issue updating leaderboard** '" + type.toString() + "' at place " + count + ": " + e.getMessage());
					}
				}
				
				int newPlace = 0;
				for(int count = 10; count > 0; count--) {
					if(value > leaderboard.getInt(type.toString() + "." + count + ".Amount"))
						newPlace = count;
					else break; //not higher than this
				}
				
				//Chat.broadcastMessage("Current place: " + currentPlace);
				//Chat.broadcastMessage("New place: " + newPlace);
				
				//if in leaderboard but no longer should be
				if(currentPlace != 0 && newPlace == 0) {
					//is now leaving leaderboard
					//kick out leaderboard and REORGANISE:
					int steps = 10 - currentPlace;
					
					//move each one below up
					for(int count = 0; count < steps; count++) {
						int currentStep = currentPlace + count;
						//Chat.broadcastMessage("Moving " + (currentStep+1) + " to " + currentStep);
						leaderboard.set(type.toString() + "." + currentStep + ".Amount", leaderboard.getInt(type.toString() + "." + (currentStep + 1) + ".Amount"));
						leaderboard.set(type.toString() + "." + currentStep + ".UUID", leaderboard.getString(type.toString() + "." + (currentStep + 1) + ".UUID"));
					}
					leaderboard.set(type.toString() + "." + 10 + ".Amount", 0);
					leaderboard.set(type.toString() + "." + 10 + ".UUID", null);
					
					Bukkit.getScheduler().runTask(plugin, new Runnable() {
						@Override
						public void run() {
							DeadMC.LeaderboardFile.save();
						}
					});
					
					updateLeaderHeads(type);
					updateHologramBoard(type);
					
					return;
				}
				
				//BASICALLY: if player can move position
				//if new place is different to current place
				//and current place can go up, and new value is greater than the value below it
				//OR
				//if current place can go down, and new value is less than the value below
				if(currentPlace != newPlace
						&& ((currentPlace != 1 && value > leaderboard.getInt(type.toString() + "." + new Integer(currentPlace - 1) + ".Amount"))
						|| ((currentPlace != 10 && value < leaderboard.getInt(type.toString() + "." + new Integer(currentPlace + 1) + ".Amount"))))) {
					
					if(currentPlace != 0) {
						//was already in leaderboard
						
						if(newPlace < currentPlace) { //is moving up
							//Chat.broadcastMessage("Move up");
							int steps = currentPlace - newPlace;
							
							//move each one below down
							for(int count = 0; count < steps; count++) {
								int currentStep = currentPlace - count;
								//Chat.broadcastMessage("Moving " + (currentStep-1) + " to " + currentStep);
								leaderboard.set(type.toString() + "." + currentStep + ".Amount", leaderboard.getInt(type.toString() + "." + (currentStep - 1) + ".Amount"));
								leaderboard.set(type.toString() + "." + currentStep + ".UUID", leaderboard.getString(type.toString() + "." + (currentStep - 1) + ".UUID"));
							}
							//finally, put in new value at newPlace
							leaderboard.set(type.toString() + "." + newPlace + ".Amount", value);
							leaderboard.set(type.toString() + "." + newPlace + ".UUID", uuid.toString());
						} else { //is moving down
							//Chat.broadcastMessage("Move down");
							int steps = newPlace - currentPlace;
							
							newPlace--;
							
							//move each one below up
							for(int count = 0; count < steps; count++) {
								int currentStep = currentPlace + count;
								//Chat.broadcastMessage("Moving " + (currentStep+1) + " to " + currentStep);
								leaderboard.set(type.toString() + "." + currentStep + ".Amount", leaderboard.getInt(type.toString() + "." + (currentStep + 1) + ".Amount"));
								leaderboard.set(type.toString() + "." + currentStep + ".UUID", leaderboard.getString(type.toString() + "." + (currentStep + 1) + ".UUID"));
							}
						}
						//finally, put in new value at newPlace
						leaderboard.set(type.toString() + "." + newPlace + ".Amount", value);
						leaderboard.set(type.toString() + "." + newPlace + ".UUID", uuid.toString());
						
						updateLeaderHeads(type);
						updateHologramBoard(type);
						
					} else if(newPlace != 0) {
						//is now entering leaderboard...
						//REORGANISE:
						//Chat.broadcastMessage("Entered leaderboard");
						
						//push all down,
						for(int count = 10; count >= newPlace; count--) {
							leaderboard.set(type.toString() + "." + count + ".Amount", leaderboard.getInt(type.toString() + "." + (count - 1) + ".Amount"));
							leaderboard.set(type.toString() + "." + count + ".UUID", leaderboard.getString(type.toString() + "." + (count - 1) + ".UUID"));
						}
						//enter new place
						leaderboard.set(type.toString() + "." + newPlace + ".Amount", value);
						leaderboard.set(type.toString() + "." + newPlace + ".UUID", uuid.toString());
						
						updateLeaderHeads(type);
						updateHologramBoard(type);
					}
					Bukkit.getScheduler().runTask(plugin, new Runnable() {
						@Override
						public void run() {
							DeadMC.LeaderboardFile.save();
						}
					});
				} else if(currentPlace != 0) {
					//update if not moving
					leaderboard.set(type.toString() + "." + currentPlace + ".Amount", value);
					updateHologramBoard(type);
					
					Bukkit.getScheduler().runTask(plugin, new Runnable() {
						@Override
						public void run() {
							DeadMC.LeaderboardFile.save();
						}
					});
				}
				
			} catch(Exception e) {
				Chat.logError("**Issue updating leaderboard**: " + e.getMessage());
			}
			}
		});
	}
	
	@EventHandler
	public void onTalkEvent(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		if(playerConfig.getString("Coins") != null) { //player not in tutorial
			
			//royals+ can use colour in messages
			String message = event.getMessage();
			if(playerConfig.getInt("DonateRank") >= Rank.Royal.ordinal())
				message = ChatColor.translateAlternateColorCodes('&', event.getMessage());
			
			if(townChatToggled.contains(player.getName())) {
				
				sendTownChat(player.getUniqueId(), message, false);
				
				event.setCancelled(true);
				return;
			}
			
			if(nationChatToggled.contains(player.getName())) {
				
				sendNationChat(player.getUniqueId(), message, false);
				
				event.setCancelled(true);
				return;
			}
			
			if(staffChatToggled.contains(player.getName())) {
				
				if(playerConfig.getString("StaffRank") != null) {
					
					String playersName = player.getName();
					if(playerConfig.getString("Name") != null)
						playersName = playerConfig.getString("Name");
					
					//send message to player
					player.sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.GRAY + ChatColor.ITALIC + "you" + ChatColor.DARK_GREEN + ChatColor.BOLD + " => " + ChatColor.GREEN + message);
					
					//send message to all staff
					for(Player recipient : Bukkit.getOnlinePlayers()) {
						if(recipient.hasPermission("tab.staff") && !recipient.getName().equalsIgnoreCase(player.getName())) {
							recipient.sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', playersName) + ChatColor.DARK_GREEN + ChatColor.BOLD + " => " + ChatColor.GREEN + message);
						}
					}
					
					//send message to console
					Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "[/sc] " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', playersName) + ChatColor.DARK_GREEN + ChatColor.BOLD + " => " + ChatColor.GREEN + message);
					
				} else {
					staffChatToggled.remove(player.getName());
				}
				
				event.setCancelled(true);
				return;
			}
			
			String checkedMessage = checkMessage(event.getMessage(), player.getUniqueId(), null);
			if(checkedMessage != null) {
				for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
					PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer.getUniqueId());
					if(onlineConfig.getString("Coins") != null) { //player not in tutorial
						
						List<String> ignoredUUIDs = new ArrayList<String>();
						if(onlineConfig.getString("Ignored") != null)
							ignoredUUIDs = onlineConfig.getStringList("Ignored");
						if(!ignoredUUIDs.contains(player.getUniqueId().toString())) {
							
							String playersName = onlinePlayer.getName();
							if(onlineConfig.getString("Name") != null)
								playersName = stripColor(onlineConfig.getString("Name"));
							
							if(event.getMessage().toLowerCase().contains(onlinePlayer.getName().toLowerCase()) || event.getMessage().toLowerCase().contains(playersName.toLowerCase()))
								onlinePlayer.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " => " + ChatColor.RESET + getFullChatPrefix(player.getUniqueId(), false) + ChatColor.GRAY + ": " + ChatColor.WHITE + /*event.getMessage().substring(0, 1).toUpperCase() +*/ message/*.substring(1)*/);
							else
								onlinePlayer.sendMessage(getFullChatPrefix(player.getUniqueId(), false) + ChatColor.GRAY + ": " + ChatColor.WHITE + /*event.getMessage().substring(0, 1).toUpperCase() +*/ message/*.substring(1)*/);
							
						}
						
					}
					
					//play noise whenever someone talks
					if(onlinePlayer.isOp() && !onlinePlayer.getName().equalsIgnoreCase(event.getPlayer().getName()))
						onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.BLOCK_BELL_USE, 1f, 8f);
				}
				
				//send discord message
				String playerName = playerConfig.getString("Name") == null ? player.getName() : Chat.stripColor(playerConfig.getString("Name"));
				String avatarUrl = "https://www.mc-heads.net/avatar/" + player.getUniqueId().toString();
				//add staff rank, or [Supporter]
				String prefix = playerConfig.getInt("DonateRank") > 0 ? " (Supporter)" : "";
				if(playerConfig.getString("StaffRank") != null) {
					Admin.StaffRank staff = Admin.StaffRank.values()[playerConfig.getInt("StaffRank")];
					if(staff == Admin.StaffRank.JUNIOR_MOD)
						prefix =  " (Junior Mod)";
					if(staff == Admin.StaffRank.MODERATOR)
						prefix =  " (Moderator)";
					if(staff == Admin.StaffRank.SENIOR_MOD)
						prefix =  " (Senior Mod)";
					if(staff == Admin.StaffRank.ADMINISTRATOR)
						prefix =  " (Admin)";
					if(staff == Admin.StaffRank.OWNER)
						prefix =  " (Owner)";
				}
				WebhookUtil.deliverMessage(DiscordSRV.getPlugin().getMainTextChannel(), playerName + prefix, avatarUrl, checkedMessage, (MessageEmbed) null);
				
				if(message.toLowerCase().contains("welcome")) {
					String target = null;
					for(String newPlayer : Players.newPlayers) {
						if(message.toLowerCase().contains(newPlayer.toLowerCase())) {
							target = newPlayer;
							break;
						}
					}
					for(String newPlayer : Players.newPlayers) {
						//check all new players if welcomed
						if(target == null || target.equalsIgnoreCase(newPlayer)) {
							List<String> welcomedBy = Players.newPlayers_welcomedBy.get(newPlayer);
							if(!welcomedBy.contains(player.getName())) {
								//hasn't been welcomed by player
								
								int reward = (int) (Math.random() * 30) + 30;
								Economy.giveCoins(player.getUniqueId(), reward);
								
								player.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.WHITE + "Thanks for welcoming " + ChatColor.ITALIC + newPlayer + "!" + ChatColor.GREEN + " +" + ChatColor.GOLD + Economy.convertCoins(reward) + ChatColor.WHITE + "");
								
								welcomedBy.add(player.getName());
								Players.newPlayers_welcomedBy.replace(newPlayer, welcomedBy);
								
								int totalWelcomed = playerConfig.getString("Welcome") == null ? 1 : playerConfig.getInt("Welcome") + 1;
								playerConfig.set("Welcome", totalWelcomed);
								playerConfig.save();
								
							}
						}
					}
					//update leaderboard
					Chat.updateLeaderBoard(Leaderboard.WELCOME, player.getUniqueId());
				}
				
			}
			
		}
		
		//if even cancelled, send to console
		if(DeadMC.RestartFile.data().getString("DebugMessages.chat") != null)
			Bukkit.getServer().getConsoleSender().sendMessage("Original: " + event.getMessage());
		else Bukkit.getServer().getConsoleSender().sendMessage(player.getName() + ": " + event.getMessage());
		event.setCancelled(true);
	}
	
	public static void sendTownChat(UUID uuid, String message, boolean discord) {
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
		Player onlinePlayer = Bukkit.getPlayer(uuid);
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		String originalTownName = playerConfig.getString("Town");
		if(originalTownName != null) {
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			String townDisplayName = Town.getTownDisplayName(originalTownName);
			
			String playersName = offlinePlayer.getName();
			if(playerConfig.getString("Name") != null)
				playersName = playerConfig.getString("Name");
			
			//player can swear / spam / send links in town chat
			if(playerConfig.getInt("DonateRank") >= Rank.Royal.ordinal())
				message = ChatColor.translateAlternateColorCodes('&', message);
			
			String discordPrefix = discord ? ChatColor.GRAY + "[" + ChatColor.BLUE + "D" + ChatColor.GRAY + "] " : "";
			
			//send message to player
			if(onlinePlayer != null) {
				onlinePlayer.sendMessage(ChatColor.AQUA + "[" + townDisplayName + "] " + discordPrefix + ChatColor.GRAY + ChatColor.ITALIC + "you" + ChatColor.DARK_AQUA + ChatColor.BOLD + " => " + ChatColor.AQUA + message);
			}
			
			//send message to all town members
			for(String memberUUIDstring : townConfig.getStringList("Members")) {
				UUID memberUUID = UUID.fromString(memberUUIDstring);
				Player member = Bukkit.getPlayer(memberUUID);
				if(member != null && !uuid.equals(memberUUID)) {
					member.sendMessage(ChatColor.AQUA + "[" + townDisplayName + "] " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', playersName) + ChatColor.DARK_AQUA + ChatColor.BOLD + " => " + ChatColor.AQUA + message);
				}
			}
			
			//send message to console
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "[" + townDisplayName + "] " + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', playersName) + ChatColor.DARK_AQUA + ChatColor.BOLD + " => " + ChatColor.AQUA + message);

			//send message to discord
			Guild guild = DiscordSRV.getPlugin().getMainGuild();
			if(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).size() > 0) {
				//has discord channel
				String playerName = playerConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(uuid).getName() : Chat.stripColor(playerConfig.getString("Name"));
				String avatarUrl = "https://www.mc-heads.net/avatar/" + uuid.toString();
				String rankPrefix = Town.Rank.values()[playerConfig.getInt("TownRank")].toString().replace("_", "-");
				WebhookUtil.deliverMessage(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).get(0), playerName + " (" + rankPrefix + ")", avatarUrl, message, (MessageEmbed) null);
			}
		} else {
			townChatToggled.remove(offlinePlayer.getName());
			if(onlinePlayer != null) {
				onlinePlayer.sendMessage(ChatColor.YELLOW + "[/tc] " + ChatColor.RED + "You are not a member of a town.");
			}
		}
	}
	
	public static void sendNationChat(UUID uuid, String message, boolean discord) {
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
		Player onlinePlayer = Bukkit.getPlayer(uuid);
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		String originalTownName = playerConfig.getString("Town");
		if(originalTownName != null) {
			//is member of town
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			String townDisplayName = Town.getTownDisplayName(originalTownName);
			
			//if town is in a nation
			String nationName = townConfig.getString("Nation");
			if(nationName != null) {
				NationConfig nationConfig = NationConfig.getConfig(nationName);
				
				String playersName = offlinePlayer.getName();
				if(playerConfig.getString("Name") != null)
					playersName = playerConfig.getString("Name");
				
				String discordPrefix = discord ? ChatColor.GRAY + "[" + ChatColor.BLUE + "D" + ChatColor.GRAY + "] " : "";
				
				//send message to player
				if(onlinePlayer != null) {
					onlinePlayer.sendMessage(ChatColor.LIGHT_PURPLE + "[" + nationName + "] " + discordPrefix + ChatColor.GRAY + ChatColor.ITALIC + "you" + ChatColor.DARK_PURPLE + ChatColor.BOLD + " => " + ChatColor.LIGHT_PURPLE + message);
				}
				
				for(UUID memberUUID : Nation.getMembers(nationName)) {
					Player member = Bukkit.getPlayer(memberUUID);
					if(member != null && !uuid.equals(memberUUID)) {
						member.sendMessage(ChatColor.LIGHT_PURPLE + "[" + nationName + "] " + discordPrefix + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', playersName) + ChatColor.DARK_PURPLE + ChatColor.BOLD + " => " + ChatColor.LIGHT_PURPLE + message);
					}
				}
				
				//send message to console
				Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + "[" + nationName + "] " + discordPrefix + ChatColor.GRAY + ChatColor.translateAlternateColorCodes('&', playersName) + ChatColor.DARK_PURPLE + ChatColor.BOLD + " => " + ChatColor.LIGHT_PURPLE + message);
				
				//send message to discord
				Guild guild = DiscordSRV.getPlugin().getMainGuild();
				if(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).size() > 0) {
					//has discord channel
					String playerName = playerConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(uuid).getName() : Chat.stripColor(playerConfig.getString("Name"));
					String avatarUrl = "https://www.mc-heads.net/avatar/" + uuid.toString();
					String rankPrefix = Town.Rank.values()[playerConfig.getInt("TownRank")].toString().replace("_", "-") + " of " + townDisplayName;
					WebhookUtil.deliverMessage(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).get(0), playerName + " (" + rankPrefix + ")", avatarUrl, message, (MessageEmbed) null);
				}
				
			} else {
				nationChatToggled.remove(offlinePlayer.getName());
				if(onlinePlayer != null)
					onlinePlayer.sendMessage(ChatColor.YELLOW + "[/nc] " + ChatColor.WHITE + "You are not a member of a nation.");
			}
		} else {
			nationChatToggled.remove(offlinePlayer.getName());
			if(onlinePlayer != null)
				onlinePlayer.sendMessage(ChatColor.YELLOW + "[/nc] " + ChatColor.WHITE + "You are not a member of a nation.");
		}
	}
	
	//debug messages to log if enabled
	public static void debug(String message) {
		if(DeadMC.RestartFile.data().getString("DebugMessages") != null)
			Bukkit.getConsoleSender().sendMessage(message);
	}
	public static void debug(String message, String type) {
		if(DeadMC.RestartFile.data().getString("DebugMessages." + type) != null)
			Bukkit.getConsoleSender().sendMessage(message);
	}
	
	public static void broadcastDiscord(String message) {
		DiscordUtil.sendMessage(DiscordSRV.getPlugin().getMainTextChannel(), message);
	}
	public static void broadcastDiscord(String message, boolean snicko) {
		broadcastDiscord(message, snicko, false);
	}
	public static void broadcastDiscord(String message, boolean snicko, boolean aces) {
		List<String> discordIDs = Arrays.asList(
				"464217924593385472", //Snicko
				"263041879333928964" //Aces
		);
		for(String discordID : discordIDs) {
			boolean allowed = (snicko && discordID.equalsIgnoreCase("464217924593385472"))
				|| (aces && discordID.equalsIgnoreCase("263041879333928964"));
			if(!allowed) continue;
			
			try {
				UUID uuid = DiscordSRV.getPlugin().getAccountLinkManager().getUuid(discordID);
				Chat.debug(message); //also send to log
				DiscordSRVListener.sendPrivateMessage(discordID, message);
			} catch(Exception e) {
				Chat.debug("There was an error sending the Discord message (trying again in 5 seconds): " + e.getMessage());
				Bukkit.getScheduler().runTaskAsynchronously(plugin, ()-> {
					Tools.sleepThread(5000);
					broadcastDiscord(message, snicko, aces); //continue every 5 seconds until it doesn't catch an error
				});
			}
		}
	}
	
	//a workaround for the Bukkit broadcast
	public static void broadcastMessage(String message, boolean opsOnly, boolean staffOnly, boolean async) {
		if(async) {
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				broadcastMessageFunctionality(message, opsOnly, staffOnly);
			});
		} else {
			broadcastMessageFunctionality(message, opsOnly, staffOnly);
		}
	}
	public static void broadcastMessage(String message, boolean opsOnly, boolean staffOnly) {
		broadcastMessage(message, opsOnly, staffOnly, true);
	}
	public static void broadcastMessage(String message, boolean opsOnly) {
		broadcastMessage(message, opsOnly, false, true);
	}
	public static void broadcastMessage(String message) {
		broadcastMessage(message, false, false, true);
	}
	
	private static void broadcastMessageFunctionality(String message, boolean opsOnly, boolean staffOnly) {
		Bukkit.getConsoleSender().sendMessage(message);
		for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			if(PlayerConfig.configs.containsKey(onlinePlayer.getUniqueId())) { //only if config is saved still (player is online)
				PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer.getUniqueId());
				if((!opsOnly || onlinePlayer.isOp()) && (!staffOnly || onlineConfig.getString("StaffRank") != null)) {
					if(onlineConfig.getString("Coins") != null) { //player not in tutorial
						onlinePlayer.sendMessage(message);
					}
				}
			}
		}
	}
	
	public static String getFullChatPrefix(UUID uuid, boolean discord) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		String playersName = player.getName();
		if(playerConfig.getString("Name") != null)
			playersName = playerConfig.getString("Name");
		
		String donatorTag = playerConfig.getInt("DonateRank") > Donator.Rank.Citizen.ordinal() ? new String(ChatColor.GRAY + "[" + ChatColor.GREEN + "$" + ChatColor.GRAY + "] ") : "";
		String namePrefix = new String(ChatColor.WHITE + playersName);
		String rankPrefix = Donator.getPrefix(player.getUniqueId());
		String townPrefix = Town.getTownRankPrefix(player.getUniqueId());
		String discordPrefix = discord ? ChatColor.GRAY + "[" + ChatColor.BLUE + "D" + ChatColor.GRAY + "] " : "";

		//donator name colour
		int donateRank = playerConfig.getInt("DonateRank");
		if(donateRank == Donator.Rank.Royal.ordinal())
			namePrefix = new String(ChatColor.LIGHT_PURPLE + playersName);
		if(donateRank == Donator.Rank.Emperor.ordinal()) {
			namePrefix = new String(ChatColor.GOLD + ChatColor.translateAlternateColorCodes('&', playersName));
		}
		
		if(playerConfig.getString("StaffRank") != null) {
			StaffRank rank = StaffRank.values()[playerConfig.getInt("StaffRank")];
			if(rank == StaffRank.JUNIOR_MOD)
				namePrefix = new String(ChatColor.GREEN + ChatColor.translateAlternateColorCodes('&', playersName));
			if(rank == StaffRank.MODERATOR)
				namePrefix = new String(ChatColor.DARK_GREEN + ChatColor.translateAlternateColorCodes('&', playersName));
			if(rank == StaffRank.SENIOR_MOD)
				namePrefix = new String(ChatColor.DARK_GREEN + ChatColor.translateAlternateColorCodes('&', playersName));
			if(rank == StaffRank.ADMINISTRATOR)
				namePrefix = new String(ChatColor.DARK_RED + ChatColor.translateAlternateColorCodes('&', playersName));
			if(rank == StaffRank.OWNER)
				namePrefix = new String(ChatColor.DARK_RED + ChatColor.translateAlternateColorCodes('&', playersName));
		}
		
		return new String(donatorTag + rankPrefix + " " + townPrefix + discordPrefix + namePrefix);
	}
	
	public static String stripColor(String string) {
		List<String> colourCodes = new ArrayList<String>();
		for(int count = 0; count <= 9; count++) //add 0-9
			colourCodes.add("&" + count);
		colourCodes.add("&a");
		colourCodes.add("&b");
		colourCodes.add("&c");
		colourCodes.add("&d");
		colourCodes.add("&e");
		colourCodes.add("&f");
		colourCodes.add("&k");
		colourCodes.add("&m");
		colourCodes.add("&o");
		colourCodes.add("&l");
		colourCodes.add("&n");
		colourCodes.add("&r");
		
		String prefixNoColor = "";
		for(int count = 0; count <= string.length(); count++) {
			try {
				String charactersToTry = string.substring(count, count + 2);
				
				if(colourCodes.contains(charactersToTry.toLowerCase())) {
					count++;
				} else {
					prefixNoColor += string.substring(count, count + 1);
				}
			} catch(StringIndexOutOfBoundsException e) {
				prefixNoColor += string.substring(count);
			}
		}
		return prefixNoColor;
	}
	
	public static int numberOfAmpersands(String someString, char searchedChar, int index) {
		if(index >= someString.length()) {
			return 0;
		}
		int count = someString.charAt(index) == searchedChar ? 1 : 0;
		return count + numberOfAmpersands(someString, searchedChar, index + 1);
	}
	
	public static Boolean playerIsMuted(UUID uuid) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		if(playerConfig.getString("Muted.Start") != null) {
			int timeSinceBan = DateCode.getTimeSince(playerConfig.getString("Muted.Start"));
			if(timeSinceBan >= playerConfig.getInt("Muted.Time")) {
				playerConfig.set("Muted", null);
				playerConfig.save();
				return false;
			} else return true;
		}
		return false;
	}
	
	public static String getTitlePlaceholder(String title, int sizeToMatch) {
		
		int titleStringLength = title.length() - 2;
		
		int numberOfPlaceholders = (sizeToMatch - titleStringLength) / 2;
		if(numberOfPlaceholders < 0) numberOfPlaceholders = 0;
		String placeholder = new String();
		for(int count = 0; count < numberOfPlaceholders; count++)
			placeholder = placeholder + "=";
		
		return placeholder;
	}
	
	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch(NumberFormatException e) {
			return false;
		} catch(NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}
	
	
	public static void sendBigTitle(UUID uuid, String messageBig, String messageSmall) {
		final Player onlinePlayer = Bukkit.getPlayer(uuid);
		if(onlinePlayer != null) {
			Bukkit.getScheduler().runTask(plugin, () -> {
				onlinePlayer.sendTitle(ChatColor.translateAlternateColorCodes('&', messageBig), ChatColor.translateAlternateColorCodes('&', messageSmall), 20, 100, 20);
			});
		}
	}
	
	public static void sendTitleMessage(UUID uuid, String message) {
		final Player onlinePlayer = Bukkit.getPlayer(uuid);
		if(onlinePlayer != null) {
			Bukkit.getScheduler().runTask(plugin, () -> {
				onlinePlayer.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(message));
			});
		}
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		event.setRespawnLocation(Travel.getGlobalPosition("Longdale"));
	}
	
	String killer = new String("creature");
	boolean killerIsPlayer = false;
	
	@EventHandler
	public void deathByEntityAttack(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if(player.getHealth() <= 3) {
				if(event.getDamager().getType() == EntityType.PLAYER) {
					String killerName = Bukkit.getOfflinePlayer(event.getDamager().getUniqueId()).getName();
					PlayerConfig enemyConfig = PlayerConfig.getConfig(player.getLastDamageCause().getEntity().getUniqueId());
					if(enemyConfig.getString("Name") != null)
						killerName = ChatColor.translateAlternateColorCodes('&', enemyConfig.getString("Name"));
					
					killer = killerName;
					killerIsPlayer = true;
				} else {
					killer = event.getDamager().getType().toString().toLowerCase().replace("_", " ");
					killerIsPlayer = false;
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		final Player player = event.getEntity();
		DamageCause reason = player.getLastDamageCause().getCause();
		
		event.setDeathMessage(null);
		
		final String killer = this.killer;
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			String playersName = player.getName();
			if(playerConfig.getString("Name") != null)
				playersName = ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Name"));
			
			String message = "";
			if(reason == DamageCause.BLOCK_EXPLOSION)
				message = ChatColor.RED + "[Death] A block exploded, and " + playersName + " was killed.";
			else if(reason == DamageCause.CONTACT)
				message = ChatColor.RED + "[Death] " + playersName + " was killed. Shouldn't have touched that block!";
			else if(reason == DamageCause.CRAMMING)
				message = ChatColor.RED + "[Death] " + playersName + " was crammed to death.";
			else if(reason == DamageCause.DROWNING)
				message = ChatColor.RED + "[Death] " + playersName + " took a long walk off a short pier.";
			else if(reason == DamageCause.DRAGON_BREATH)
				message = ChatColor.RED + "[Death] " + playersName + " was burnt to a crisp by the Ender Dragon.";
			else if(reason == DamageCause.ENTITY_ATTACK) {
				if(killerIsPlayer) {
					message = ChatColor.RED + "[Death] " + playersName + ChatColor.RED + " lost a battle to " + killer + ChatColor.RED + ".";
				} else {
					String killerName = killer;
					if(killer.equalsIgnoreCase("cave_spider")) killerName = new String("spooky spider");
					if(killer.equalsIgnoreCase("spider")) killerName = new String("climber");
					if(killer.equalsIgnoreCase("skeleton")) killerName = new String("destuctor");
					if(killer.equalsIgnoreCase("creeper")) killerName = new String("bomber");
					message = ChatColor.RED + "[Death] " + playersName + " was slain by a " + killerName + ".";
				}
			}
			else if(reason == DamageCause.ENTITY_SWEEP_ATTACK)
				message = ChatColor.RED + "[Death] " + playersName + " was killed by a swooping enemy.";
			else if(reason == DamageCause.ENTITY_EXPLOSION)
				message = ChatColor.RED + "[Death] " + playersName + " exploded.";
			else if(reason == DamageCause.FALL)
				message = ChatColor.RED + "[Death] " + playersName + " succumbed to gravity.";
			else if(reason == DamageCause.FALLING_BLOCK)
				message = ChatColor.RED + "A block fell on " + playersName + ", and they died.";
			else if(reason == DamageCause.FIRE || reason == DamageCause.FIRE_TICK)
				message = ChatColor.RED + "[Death] " + playersName + " burned to their death.";
			else if(reason == DamageCause.FLY_INTO_WALL)
				message = ChatColor.RED + "[Death] " + playersName + " flew themself into a wall, and was killed.";
			else if(reason == DamageCause.HOT_FLOOR)
				message = ChatColor.RED + "[Death] " + playersName + " stood on a burning floor, and was killed.";
			else if(reason == DamageCause.LAVA)
				message = ChatColor.RED + "[Death] " + playersName + " melted in lava.";
			else if(reason == DamageCause.LIGHTNING)
				message = ChatColor.RED + "[Death] " + playersName + " was struck by lightning, and was killed.";
			else if(reason == DamageCause.MAGIC)
				message = ChatColor.RED + "[Death] " + playersName + " was killed by a magical force.";
			else if(reason == DamageCause.MELTING)
				message = ChatColor.RED + "[Death] " + playersName + " melted to death.";
			else if(reason == DamageCause.POISON)
				message = ChatColor.RED + "[Death] " + playersName + " was poisoned.";
			else if(reason == DamageCause.PROJECTILE)
				message = ChatColor.RED + "[Death] " + playersName + " was struck by a projectile, and was killed.";
			else if(reason == DamageCause.STARVATION)
				message = ChatColor.RED + "[Death] " + playersName + " starved to death.";
			else if(reason == DamageCause.SUFFOCATION)
				message = ChatColor.RED + "[Death] " + playersName + " suffocated to death.";
			else if(reason == DamageCause.SUICIDE)
				message = ChatColor.RED + "[Death] " + playersName + " ended their own life.";
			else if(reason == DamageCause.VOID)
				message = ChatColor.RED + "[Death] " + playersName + " fell into the void, and was killed.";
			else if(reason == DamageCause.WITHER)
				message = ChatColor.RED + "[Death] " + playersName + " was slain by a wither.";
			else message = ChatColor.RED + "[Death] " + playersName + " died.";
			
			Bukkit.getConsoleSender().sendMessage(message);
			for(Player p : Bukkit.getOnlinePlayers()) {
				if(!p.getName().equalsIgnoreCase(player.getName()))
					p.sendMessage(message);
			}
			player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Oh dear! You died!");
		});
	}
	
	// check message:

	private static List<String> swearWords = Arrays.asList("fuck", "shit", "bitch", "penis", "cunt", "pussy", "faggot", "nigga", "nigger", "asshole");
	private static List<String> domainWords = Arrays.asList("com", "org", "net", "co", "xyz", "ly", "nu", "tk", "minehut.gg", "discord.gg");
	private static List<String> whitelistedDomains = Arrays.asList("deadmc.com", "discord.gg/cmpyurs");

	public static HashMap<String, Long> lastMessageTime = new HashMap<String, Long>();
	
	//run async!
	//returns the converted message, otherwise if blocked, returns null
	public static String checkMessage(String message, UUID uuid, String discordID) {
		long startTime = System.currentTimeMillis();
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		@Nullable Player player = discordID == null ? Bukkit.getPlayer(uuid) : null; //player will be null if using discord ID
		
		Chat.debug("Checking message...", "chat");
		boolean isStaff = playerConfig.getString("StaffRank") != null;
		boolean isSupporter = playerConfig.getInt("DonateRank") > 0;
		boolean isMuted = playerIsMuted(uuid);
		Chat.debug(" is staff: " + isStaff, "chat");
		Chat.debug(" is supporter: " + isSupporter, "chat");
		Chat.debug(" is muted: " + isMuted, "chat");
		if(isStaff) return message; //ignore for staff
		if(isMuted) {
			int timeInMinutes = playerConfig.getInt("Muted.Time") - DateCode.getTimeSince(playerConfig.getString("Muted.Start"));
			String timeString = timeInMinutes + " minutes";
			if(timeInMinutes > 60 && timeInMinutes < 1440) timeString = (int)Math.ceil(timeInMinutes/60.0) + " hours";
			else if(timeInMinutes > 1440) timeString = (int)Math.ceil(timeInMinutes/1440.0) + " days";
			
			if(player == null) //on discord
				DiscordSRVListener.sendPrivateMessage(discordID, "You attempted to chat in the Discord although you are muted for " + Punishment.values()[playerConfig.getInt("Muted.Reason")].toString().toLowerCase().replace("_", " ") + ". (" + timeString + ")");
			else player.sendMessage(ChatColor.DARK_RED + "[DeadMC] " + ChatColor.WHITE + "You are muted for " + Punishment.values()[playerConfig.getInt("Muted.Reason")].toString().toLowerCase().replace("_", " ") + ". (" + timeString + ")");
			return null;
		}
		
		//repeated message check:
		if(lastMessage.containsKey(uuid.toString())
				&& lastMessage.get(uuid.toString()).equalsIgnoreCase(message)) {
			if(player == null) //on discord
				DiscordSRVListener.sendPrivateMessage(discordID, "You attempted to chat in the Discord although you cannot send the same message twice.");
			else player.sendMessage(ChatColor.DARK_RED + "[DeadMC] " + ChatColor.RED + "You cannot send the same message twice.");
			return null;
		}
		
		//must wait X seconds between chat - else warn for spamming
		if(!isSupporter //supporters can bypass
				&& lastMessageTime.containsKey(uuid.toString())) {
			Long timeSinceLast = System.currentTimeMillis() - lastMessageTime.get(uuid.toString());
			if(timeSinceLast < 750) {
				if(player == null) //on discord
					DiscordSRVListener.sendPrivateMessage(discordID, "You attempted to chat in the Discord although you cannot send messages that fast.");
				else Chat.sendTitleMessage(player.getUniqueId(), ChatColor.DARK_RED + "[DeadMC] " + ChatColor.RED + "You cannot send messages that fast.");
				giveWarning(uuid, message, Punishment.SPAMMING, " (too fast)", discordID);
				return null;
			}
		}
		
		//add last message here
		if(lastMessage.containsKey(uuid.toString())) {
			lastMessage.replace(uuid.toString(), message);
			lastMessageTime.replace(uuid.toString(), System.currentTimeMillis());
		} else {
			lastMessage.put(uuid.toString(), message);
			lastMessageTime.put(uuid.toString(), System.currentTimeMillis());
		}
		
		//swears check
		if(containsBlacklistWord(message.toLowerCase())) {
			//warn player for offensive language
			giveWarning(uuid, message, Punishment.OFFENSIVE_LANGUAGE, "", discordID);
			return null;
		}
		
		//domain check:
		boolean containsDomain = containsDomain(player, message.toLowerCase());
		String containsIP = extractIP(message);
		if(!isSupporter //supporters can send links
		&& (containsIP != null //contains an IP
			|| (containsDomain //contains a domain
			&& !whitelistedDomains.stream().anyMatch(message.toLowerCase()::contains)))) { //ignore whitelisted domains

			if(containsIP != null) {
				Chat.debug("Contains an IP! " + containsIP, "chat");
			}
			
			//warn player for advertising
			giveWarning(uuid, message, Punishment.SHARING_LINKS, containsIP != null  ? " (sharing IP address)" : " (sharing web domain)", discordID);
			return null;
		}
		
		//flood check:
		int maxRepeatedLetters = 6; //the max amount of repeated letters allowed
		Pattern pattern = Pattern.compile("(.)\\1{" + maxRepeatedLetters + ",}");
		Matcher matcher = pattern.matcher(message);
		int groupNumber = 1;
		while(matcher.find()) {
			if(groupNumber == 1) {
				//only warn on first group
				giveWarning(uuid, message, Punishment.SPAMMING, " (flooding)", discordID);
			}
			String group = matcher.group();
			message = message.replace(group, group.substring(0, groupNumber > 1 ? 1 : 3)); //reduce message to just 3 (or 1 if multiple groups)
			matcher = pattern.matcher(message);
			groupNumber++;
		}
		
		if(message.length() == 1) {
			if(player == null) //on discord
				DiscordSRVListener.sendPrivateMessage(discordID, "You attempted to chat in the Discord although you cannot send messages that small.");
			else player.sendMessage(ChatColor.DARK_RED + "[DeadMC] " + ChatColor.RED + "You cannot send messages that small.");
			return null; //don't send if 1 character or less
		}
		
		//can't contain more than 17 capital letters in message - will set entire message to lowercase otherwise
		long capitalLetters = message.codePoints().filter(c-> c>='A' && c<='Z').count();
		if((message.length() < 18 && capitalLetters > 12)
			|| capitalLetters > 20) {
			message = message.toLowerCase();
			giveWarning(uuid, message, Punishment.SPAMMING, " (excessive caps)", discordID);
		}

		Chat.debug("Total (async) time taken to check message: " + (System.currentTimeMillis()-startTime) + "ms");
		
		return message;
	}
	
	private static HashMap<Punishment, Integer> maxWarnings = new HashMap<Punishment, Integer>() {{
		put(Punishment.OFFENSIVE_LANGUAGE, 2);
		put(Punishment.SPAMMING, 3);
		put(Punishment.SHARING_LINKS, 2);
	}};
	
	public static ConcurrentHashMap<String, Integer> warnings = new ConcurrentHashMap<String, Integer>(); //player, number of warnings
	
	public static void giveWarning(UUID uuid, String message, Punishment type, String moreDetails, String discordID) { //warn for using blacklisted words, or
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			@Nullable Player player = discordID == null ? Bukkit.getPlayer(uuid) : null; //player will be null if using discord ID
			
			String warningCode = uuid.toString() + "-" + type.toString();
			if(warnings.getOrDefault(warningCode, 0) + 1 == maxWarnings.getOrDefault(type, 2)) {
				//punish player
				
				if(player == null) { //on discord
					DiscordSRVListener.sendPrivateMessage(discordID, "**You have been punished for " + type.toString().replace("_", " ").toLowerCase() + ".**");
					if(moreDetails.length() > 0)
						DiscordSRVListener.sendPrivateMessage(discordID, " > Specific:*" + moreDetails + "*");
				} else {
					player.sendMessage("");
					player.sendMessage(ChatColor.DARK_RED + "[DeadMC] " + ChatColor.RED + "You have been punished for " + type.toString().replace("_", " ").toLowerCase() + ".");
					if(moreDetails.length() > 0)
						player.sendMessage(ChatColor.DARK_RED + "[DeadMC] " + ChatColor.WHITE + "Specific:" + moreDetails);
					player.sendMessage(ChatColor.DARK_RED + "[DeadMC] " + ChatColor.WHITE + "Please read the " + ChatColor.GOLD + "/rules" + ChatColor.WHITE + ".");
					player.sendMessage("");
				}
				
				if(moreDetails.contains("(too fast)")) lastMessage.replace(uuid.toString(), "sent message too fast"); //send to log
				Bukkit.getScheduler().runTask(plugin, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "punish " + Bukkit.getOfflinePlayer(uuid).getName() + " " + type.ordinal() + " AUTOMATIC"));
				
				//remove warnings
				warnings.remove(warningCode);
			} else {
				PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
				String nickName = playerConfig.getString("Name") != null ? playerConfig.getString("Name") : Bukkit.getOfflinePlayer(uuid).getName();
				WebhookUtil.deliverMessage(DiscordUtil.getJda().getTextChannelById(JDAListener.warningsChannel), player, Chat.stripColor(nickName), ":speaking_head: **" + Chat.stripColor(nickName) + "** was automatically warned for **" + type.toString().replace("_", " ").toLowerCase() + "**" + moreDetails + ": '" + lastMessage.get(uuid.toString()) + "'", (MessageEmbed) null);
				
				Chat.broadcastMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + ChatColor.translateAlternateColorCodes('&', nickName) + " was automatically warned for " + type.toString().replace("_", " ").toLowerCase() + moreDetails + ": '" + ChatColor.ITALIC + lastMessage.get(uuid.toString()) + "'", true, true);
				
				//add warning
				if(warnings.containsKey(warningCode))
					warnings.replace(warningCode, warnings.get(warningCode) + 1);
				else warnings.put(warningCode, 1);
				
				if(player == null) { //on discord
					DiscordSRVListener.sendPrivateMessage(discordID, "You have been **warned** for " + type.toString().replace("_", " ").toLowerCase() + ". (" + warnings.get(warningCode) + "/" + maxWarnings.getOrDefault(type, 2) + ")");
					if(moreDetails.length() > 0)
						DiscordSRVListener.sendPrivateMessage(discordID, " > Specific:*" + moreDetails + "*");
				} else {
					player.sendMessage("");
					player.sendMessage(ChatColor.RED + "[WARNING] " + ChatColor.RED + "You have been warned for " + type.toString().replace("_", " ").toLowerCase() + ".");
					if(moreDetails.length() > 0)
						player.sendMessage(ChatColor.RED + "[WARNING] " + ChatColor.WHITE + "Specific:" + moreDetails);
					player.sendMessage(ChatColor.RED + "[WARNING] " + ChatColor.WHITE + "Please read the " + ChatColor.GOLD + "/rules" + ChatColor.WHITE + "." + ChatColor.YELLOW + " (" + warnings.get(warningCode) + "/" + maxWarnings.getOrDefault(type, 2) + ")");
					player.sendMessage("");
				}
				
				Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
					if(warnings.getOrDefault(warningCode, 1) == 1) {
						//remove
						warnings.remove(warningCode);
					} else {
						//remove 1
						warnings.replace(warningCode, warnings.get(warningCode) - 1);
					}
				}, 36000L); //30 mins
			}
		});
	}
	
	private static boolean containsDomain(Player player, String message) {

		if(message.length() < 10) return false; //anything less than google.com

		Chat.debug("Checking '" + message + "' for domain", "chat");

		for(String domainWord : domainWords) {
			int index = 0;
			while(message.toLowerCase().indexOf(domainWord, index) >= 0) {
				index = message.toLowerCase().indexOf(domainWord, index) + domainWord.length();

				Chat.debug("Found possible domain " + domainWord + " at " + index);

				for(int originalStart = message.length() - domainWord.length(); originalStart >= 0; originalStart--) {
					String remainingStripped = domainWord.contains(".") ? message.substring(originalStart).toLowerCase().replaceAll("[^a-zA-Z.]+", "") : message.substring(originalStart).toLowerCase().replaceAll("[^a-zA-Z]+", "");
					
					if(remainingStripped.length() < domainWord.length()) continue;

					Chat.debug("Trying index " + originalStart + " - " + remainingStripped + " - is domain? " + remainingStripped.substring(0, domainWord.length()), "chat");
					if(remainingStripped.substring(0, domainWord.length()).equals(domainWord)) {
						//this is the start
						Chat.debug(" > Found original start @ " + originalStart, "chat");
						
						//if player has played more than 1 hour, don't be so strict (exclude spaces)
						int playtime = player == null ? 0 : (int) Math.round(((player.getStatistic(Statistic.PLAY_ONE_MINUTE) / 20.0) / 60.0));
						String regex = playtime > 60 ? "[^a-zA-Z ]" : "[^a-zA-Z]";
						
						boolean letterBeforeIsSpecial = (originalStart - 1) > 0 && message.substring(originalStart - 1, originalStart).matches(regex);
						//Chat.debug("Letter before = " + message.substring(originalStart - 1, originalStart), "chat");
						boolean letterAfterIsSpecial = (originalStart + domainWord.length() + 1) >= message.length()
								|| ((originalStart + domainWord.length() + 1) < message.length() && message.substring(originalStart + domainWord.length(), originalStart + domainWord.length() + 1).matches(regex));
						//Chat.debug("Letter after = " + message.substring(originalStart + domainWord.length(), originalStart + domainWord.length() + 1), "chat");
						
						Chat.debug("Contains domain word " + domainWord + " at " + originalStart + " - letter before is special? " + letterBeforeIsSpecial + " - letter after is special ? " + letterAfterIsSpecial, "chat");
						
						if(domainWord.equalsIgnoreCase("co") && message.toLowerCase().contains("mayor")) {
							break;
						}
						if(letterBeforeIsSpecial && letterAfterIsSpecial) {
							//has non-character before and after it
							Chat.debug("Contains domain!", "chat");
							return true;
						} else {
							Chat.debug(" > Has normal character before or after it. Is safe.", "chat");
							break;
						}
					}
				}

			}
		}

		return false;
	}
	
	private static boolean containsBlacklistWord(String message) {
		//exact match check
		boolean originalMessageContainsSwear = swearWords.stream().anyMatch(message.toLowerCase()::contains);
		
		if(originalMessageContainsSwear) return true;
		
		//remove duplicate letters
		int maxRepeatedLetters = 1; //the max amount of repeated letters allowed
		Pattern pattern = Pattern.compile("(.)\\1{" + maxRepeatedLetters + ",}");
		Matcher matcher = pattern.matcher(message);
		int groupNumber = 1;
		while(matcher.find()) {
			String group = matcher.group();
			message = message.replace(group, group.substring(0,1)); //reduce message to just 1
			Chat.debug("(swear check) Found duplicate group " + group + " - new message: " + message, "chat");
			matcher = pattern.matcher(message);
			groupNumber++;
		}
		
		//advanced check
		String stripped = message.toLowerCase().replaceAll("[^a-zA-Z0-9]+","");
		Chat.debug("Checking '" + stripped + "' for swear", "chat");
		
		//if there's a letter before the start of the word, ignore (eg. finish it)
		for(String swear : swearWords) {
			int index = 0;
			while(stripped.toLowerCase().indexOf(swear, index) >= 0) {
				int start = stripped.toLowerCase().indexOf(swear, index);
				index = stripped.toLowerCase().indexOf(swear, index) + swear.length();

				Chat.debug("Found possible swear at [" + start + ", " + index + "] - " + stripped.substring(start, index), "chat");
				
				for(int originalStart = message.length() - swear.length(); originalStart >= 0; originalStart--) {
					String remainingStripped = message.substring(originalStart).toLowerCase().replaceAll("[^a-zA-Z0-9]+","");
					if(remainingStripped.length() < swear.length()) continue;
					
					Chat.debug("Trying index " + originalStart + " - " + remainingStripped + " - is swear? " + remainingStripped.substring(0, swear.length()), "chat");
					if(remainingStripped.substring(0, swear.length()).equals(swear)) {
						//this is the start
						Chat.debug(" > Found original start @ " + originalStart, "chat");
						if(originalStart > 0 && message.substring(originalStart - 1, originalStart).matches("[a-zA-Z]")) {
							//has letter before it
							Chat.debug(" > Has letter before it. Is safe.", "chat");
							break;
						} else {
							Chat.debug("Contains swear!", "chat");
							return true;
						}
					}
				}
			}
		}
		
		return false;
	}
	
	private static String extractIP(String s) {
		java.util.regex.Matcher m = java.util.regex.Pattern.compile(
				"(?<!\\d|\\d\\.)" +
						"(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
						"(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
						"(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
						"(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])" +
						"(?!\\d|\\.\\d)").matcher(s);
		return m.find() ? m.group() : null;
	}
}
