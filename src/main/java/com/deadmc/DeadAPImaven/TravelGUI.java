package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Task.TaskStep;
import com.deadmc.DeadAPImaven.Towns.Town;
import com.deadmc.DeadAPImaven.Towns.TownGUI;
import com.deadmc.DeadAPImaven.Travel.TravelLocation;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;

import java.io.File;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

public class TravelGUI implements Listener {
	
	// Get reference to main class
	private static DeadMC plugin;
	
	public TravelGUI(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public enum Icon {
		MANAGE_MENU,
		CREATE_MENU,
		
		CREATE_PRIVATE,
		CREATE_PUBLIC,
		
		HEADER_LINE,
		
		WILD_TP,
		MARKET_TP,
		LONGDALE_TP,
		TOWN_TP,
		QUICK_SLOT_1,
		QUICK_SLOT_2,
		QUICK_SLOT_3,
		QUICK_SLOT_4,
		QUICK_SLOT_5,
		QUICK_SLOT_6,
		
		PLAYERS_MENU,
		
		PUBLIC_MENU,
		TOWNS_MENU,
		ITEM_FINDER,
		
		Confirm_public_travel,
		Confirm_town_travel,
		Confirm_set_town_point,
		Create_private_TP,
		Create_public_TP,
		
		PUBLIC_SET_PASS,
		PUBLIC_SET_FEE,
		
		DELETE_PUBLIC_TP,
		DELETE_PUBLIC_TP_CONFIRM,
		DELETE_PRIVATE_TP,
		DELETE_PRIVATE_TP_CONFIRM,
		
		BACK
	}
	
	public static ItemStack getIcon(Player player, Icon icon) {
		ItemStack finalIcon = null;
		String displayName = null;
		List<String> lore = new ArrayList<String>();
		
		if(icon == Icon.BACK) {
			finalIcon = new ItemStack(Material.BARRIER);
			displayName = ChatColor.DARK_RED + "" + ChatColor.BOLD + "Cancel";
		}
		if(icon == Icon.HEADER_LINE) {
			finalIcon = new ItemStack(Material.GRAY_STAINED_GLASS_PANE);
			displayName = " ";
		}
		if(icon == Icon.MANAGE_MENU) {
			finalIcon = new ItemStack(Material.BOOKSHELF);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Manage " + ChatColor.YELLOW + ChatColor.BOLD + "<<";
			
			lore.add(ChatColor.WHITE + "Travel to, or delete a TP.");
			lore.add(ChatColor.GRAY + " > Opens the manager menu.");
		}
		if(icon == Icon.CREATE_MENU) {
			finalIcon = new ItemStack(Material.WRITABLE_BOOK);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Create " + ChatColor.YELLOW + ChatColor.BOLD + "<<";
			
			lore.add(ChatColor.WHITE + "Create your own TP.");
			lore.add(ChatColor.GRAY + " > Opens the creator menu.");
		}
		
		if(icon == Icon.WILD_TP) {
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Wilderness";
			lore.add(ChatColor.WHITE + "Travel to the wild.");
			
			finalIcon = new ItemStack(Material.WOODEN_AXE);
		}
		if(icon == Icon.MARKET_TP) {
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "The Market";
			lore.add(ChatColor.WHITE + "Travel to Longdale Market.");

			finalIcon = new ItemStack(Material.BREAD);
		}
		if(icon == Icon.TOWN_TP) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			
			if(playerConfig.getString("Town") != null) {
				String originalTownName = playerConfig.getString("Town");
				String townDisplayName = Town.getTownDisplayName(player.getUniqueId());
				TownConfig townConfig = TownConfig.getConfig(originalTownName);
				int cost = townConfig.getString("Travel.Fee") == null ? 0 : townConfig.getInt("Travel.Fee");
				
				finalIcon = new ItemStack(Material.SHIELD);
				displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Your Town";
				
				if(townConfig.getString("Travel.LocationCode") != null) {
					
					String costString = cost > 0 ? " (Cost: " + ChatColor.GOLD + Economy.convertCoins(cost) + ChatColor.WHITE + ")" : "";
					lore.add(ChatColor.WHITE + "Travel to " + townDisplayName + "." + costString);
					lore.add(ChatColor.GRAY + " > Left click to travel.");
					
				} else
					lore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.RED + " " + "No travel point set!");
				
				//if mayor: 
				if(playerConfig.getInt("TownRank") >= Town.Rank.Co_Mayor.ordinal())
					lore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Right click to manage.");
				
			} else {
				finalIcon = new ItemStack(Material.RED_STAINED_GLASS_PANE);
				displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Your Town";
				
				lore.add(ChatColor.RED + "You are not in a town.");
				lore.add(ChatColor.GRAY + " >" + ChatColor.GRAY + " Opens the Town menu.");
			}
		}
		if(icon == Icon.LONGDALE_TP) {
			finalIcon = new ItemStack(Material.OAK_BOAT);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Longdale";
			
			lore.add(ChatColor.WHITE + "Travel to Longdale.");
		}
		if(icon.toString().contains("QUICK_SLOT")) {
			int slot = Integer.parseInt(icon.toString().substring(11));
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Quick Slot " + slot;
			
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			if(slot > DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.QuickSlots")) {
				//player hasn't unlocked the slot
				finalIcon = new ItemStack(Material.RED_STAINED_GLASS_PANE);
				lore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.RED + " This is a donor feature.");
				lore.add(ChatColor.DARK_RED + "  " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Visit " + ChatColor.GOLD + "deadmc.com/ranks" + ChatColor.GRAY + ".");
			} else if(playerConfig.getString("Travel.QuickSlot" + slot + ".Name") != null) {
				//quick point exists:
				String type = playerConfig.getString("Travel.QuickSlot" + slot + ".Type");
				String point = playerConfig.getString("Travel.QuickSlot" + slot + ".Name");
				
				if(Travel.doesPointExist(player, type, point)) {
					ItemStack item = getTravelPointItem(point, PageType.valueOf(type.toUpperCase()), player);
					ItemMeta meta = item.getItemMeta();
					lore.add(ChatColor.WHITE + "Travel to " + ChatColor.GOLD + point + ChatColor.WHITE + ".");
					lore.add(ChatColor.GRAY + " > " + type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase());
					meta.setLore(lore);
					meta.setDisplayName(displayName);
					item.setItemMeta(meta);
					return item;
				} else {
					//no longer exists:
					
					//remove from config
					playerConfig.set("Travel.QuickSlot" + slot, null);
					playerConfig.save();
					
					finalIcon = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
					lore.add(ChatColor.GRAY + " > Nothing set.");
				}
				
			} else {
				//nothing set (eg. just upgraded rank, deleted point)
				finalIcon = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
				lore.add(ChatColor.GRAY + " > Nothing set.");
			}
		}
		
		if(icon == Icon.PLAYERS_MENU) {
			finalIcon = new ItemStack(Material.PLAYER_HEAD);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Players";
			
			lore.add(ChatColor.WHITE + "Travel to online players.");
			lore.add(ChatColor.GRAY + " > Opens the player menu.");
		}
		if(icon == Icon.ITEM_FINDER) {
			finalIcon = new ItemStack(Material.NAME_TAG);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Item Finder";
			
			lore.add(ChatColor.WHITE + "Find shops that buy and sell items.");
			lore.add(ChatColor.GRAY + " > Opens the item finder menu.");
		}
		
		if(icon == Icon.TOWNS_MENU) {
			finalIcon = new ItemStack(Material.YELLOW_BANNER);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Towns";
			
			lore.add(ChatColor.WHITE + "Travel to other towns.");
			lore.add(ChatColor.GRAY + " > Opens the towns menu.");
		}
		if(icon == Icon.PUBLIC_MENU) {
			finalIcon = new ItemStack(Material.SKULL_BANNER_PATTERN);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Public Points";
			
			lore.add(ChatColor.WHITE + "Find popular locations.");
			lore.add(ChatColor.GRAY + " > Opens the public menu.");
		}
		
		if(icon == Icon.CREATE_PRIVATE) {
			finalIcon = new ItemStack(Material.CHEST_MINECART);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Private";
			
			lore.add(ChatColor.WHITE + "Create a private TP.");
			lore.add(ChatColor.GRAY + " > Only you can access.");
			
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			List<String> privateTravelPoints = new ArrayList<String>();
			if(playerConfig.getStringList("Travel.Private") != null)
				privateTravelPoints = playerConfig.getStringList("Travel.Private");
			int limit = DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.Travel.Private");
			
			lore.add(ChatColor.GRAY + " > " + privateTravelPoints.size() + " / " + limit + " created.");
		}
		if(icon == Icon.CREATE_PUBLIC) {
			finalIcon = new ItemStack(Material.HOPPER_MINECART);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Public";
			
			lore.add(ChatColor.WHITE + "Create a public TP.");
			lore.add(ChatColor.GRAY + " > Anyone can access.");
			
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			List<String> publicTravelPoints = new ArrayList<String>();
			if(playerConfig.getStringList("Travel.Public") != null)
				publicTravelPoints = playerConfig.getStringList("Travel.Public");
			int limit = playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") == Admin.StaffRank.ADMINISTRATOR.ordinal() ? 999999 : DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.Travel.Public");
			
			if(limit > 0) {
				lore.add(ChatColor.GRAY + " > " + publicTravelPoints.size() + " / " + limit + " created");
			} else {
				lore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.RED + " This is a donor feature.");
				lore.add(ChatColor.DARK_RED + "  " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Visit " + ChatColor.GOLD + "deadmc.com/ranks" + ChatColor.GRAY + ".");
			}
		}
		
		if(icon == Icon.PUBLIC_SET_PASS) {
			finalIcon = new ItemStack(Material.ENDER_CHEST);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Set Password";
			
			lore.add(ChatColor.WHITE + "Require password to travel.");
		}
		if(icon == Icon.PUBLIC_SET_FEE) {
			finalIcon = new ItemStack(Material.GOLD_INGOT);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + "Set Fee";
			
			lore.add(ChatColor.WHITE + "Require a fee to travel.");
		}
		
		if(icon == Icon.DELETE_PUBLIC_TP) {
			finalIcon = new ItemStack(Material.TNT_MINECART);
			displayName = ChatColor.DARK_RED + "" + ChatColor.BOLD + "Delete";
			
			lore.add(ChatColor.WHITE + "Remove your public TP.");
		}
		if(icon == Icon.DELETE_PUBLIC_TP_CONFIRM) {
			finalIcon = new ItemStack(Material.TNT);
			displayName = ChatColor.DARK_RED + "" + ChatColor.BOLD + "Delete" + ChatColor.RED + " (public)";
			
			lore.add(ChatColor.WHITE + "Click to confirm.");
		}
		if(icon == Icon.DELETE_PRIVATE_TP) {
			finalIcon = new ItemStack(Material.TNT_MINECART);
			displayName = ChatColor.DARK_RED + "" + ChatColor.BOLD + "Delete";
			
			lore.add(ChatColor.WHITE + "Remove your private TP.");
		}
		if(icon == Icon.DELETE_PRIVATE_TP_CONFIRM) {
			finalIcon = new ItemStack(Material.TNT);
			displayName = ChatColor.DARK_RED + "" + ChatColor.BOLD + "Delete" + ChatColor.RED + " (private)";
			
			lore.add(ChatColor.WHITE + "Click to confirm.");
		}
		
		//confirmation menus:
		if(icon == Icon.Confirm_set_town_point || icon == Icon.Create_private_TP || icon == Icon.Create_public_TP || icon == Icon.Confirm_public_travel || icon == Icon.Confirm_town_travel) {
			finalIcon = new ItemStack(Material.JUNGLE_SIGN);
			displayName = ChatColor.YELLOW + "" + ChatColor.BOLD + icon.toString().replace("_", " ") + "?";
		}
		if(icon == Icon.Confirm_set_town_point) {
			lore.add(ChatColor.WHITE + "Set your town TP to your location.");
			lore.add(" " + ChatColor.DARK_RED + "" + ChatColor.BOLD + ">" + ChatColor.WHITE + " Cost: " + ChatColor.GOLD + "50 coins");
		}
		
		if(displayName != null || lore.size() > 0) {
			ItemMeta meta = finalIcon.getItemMeta();
			meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			meta.setDisplayName(displayName);
			meta.setLore(lore);
			finalIcon.setItemMeta(meta);
			return finalIcon;
		} else return null;
	}
	
	public static String getItemFinderNameFromGUI(String itemGUIname) {
		String itemFinderName = ChatColor.stripColor(itemGUIname);
		if(itemFinderName.contains("(special)") || itemFinderName.contains("(enchant)")) {
			itemFinderName = itemFinderName.toLowerCase();
		}
		return itemFinderName.replace("(special) ", "SPECIAL:").replace("(enchant) ", "ENCHANT:").replace(" ", "_");
	}
	
	public static void openQuickMenu(Player player) {
		boolean usingJavaVersion = Tools.playerIsJava(player);
		Inventory inventory = Bukkit.createInventory(null, usingJavaVersion ? 54 : 36, ChatColor.BOLD + "Travel" + ChatColor.DARK_GRAY + ": Quick Menu");
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			//top:
			inventory.setItem(usingJavaVersion ? 5 : 4, getIcon(player, Icon.MANAGE_MENU));
			inventory.setItem(usingJavaVersion ? 3 : 1, getIcon(player, Icon.CREATE_MENU));
			
			if(usingJavaVersion) {
				createHeader(player, inventory, 1);
				createHeader(player, inventory, 4);
			}
			
			//left side:
			inventory.setItem(usingJavaVersion ? 18 : 12, getIcon(player, Icon.WILD_TP));
			inventory.setItem(usingJavaVersion ? 19 : 13, getIcon(player, Icon.MARKET_TP));
			inventory.setItem(usingJavaVersion ? 27 : 18, getIcon(player, Icon.PLAYERS_MENU));
			
			//right side:
			inventory.setItem(usingJavaVersion ? 26 : 17, getIcon(player, Icon.TOWNS_MENU));
			inventory.setItem(usingJavaVersion ? 25 : 16, getIcon(player, Icon.PUBLIC_MENU));
			inventory.setItem(usingJavaVersion ? 35 : 23, getIcon(player, Icon.ITEM_FINDER));
			
			//middle:
			inventory.setItem(usingJavaVersion ? 22 : 30, getIcon(player, Icon.TOWN_TP));
			inventory.setItem(usingJavaVersion ? 30 : 20, getIcon(player, Icon.QUICK_SLOT_1));
			inventory.setItem(usingJavaVersion ? 31 : 21, getIcon(player, Icon.QUICK_SLOT_2));
			inventory.setItem(usingJavaVersion ? 32 : 26, getIcon(player, Icon.QUICK_SLOT_3));
			inventory.setItem(usingJavaVersion ? 39 : 27, getIcon(player, Icon.QUICK_SLOT_4));
			inventory.setItem(usingJavaVersion ? 40 : 32, getIcon(player, Icon.QUICK_SLOT_5));
			inventory.setItem(usingJavaVersion ? 41 : 33, getIcon(player, Icon.QUICK_SLOT_6));

			//bottom:
			inventory.setItem(usingJavaVersion ? 49 : 35, getIcon(player, Icon.LONGDALE_TP));
			
			//quick private tps:
			// if has 1 or QuickMenuSlot1 is null - show in centre
			// if knight+, player can set private or public point in each slot by right clicking
			// default is red stained square
			
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
		});
	}
	
	public static void openCreateMenu(Player player) {
		Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.BOLD + "Travel" + ChatColor.DARK_GRAY + ": Creation Menu");
		
		inventory.setItem(3, getIcon(player, Icon.CREATE_PRIVATE));
		inventory.setItem(5, getIcon(player, Icon.CREATE_PUBLIC));
		
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta meta = backButton.getItemMeta();
		List<String> lore = meta.getLore() == null ? new ArrayList<String>() : meta.getLore();
		lore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + "Quick Menu" + ChatColor.GRAY + ".");
		meta.setLore(lore);
		backButton.setItemMeta(meta);
		inventory.setItem(0, backButton);
		
		Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
	}
	
	private static int createHeader(Player player, Inventory inventory, int line) {
		return createHeader(player, inventory, line, false);
	}
	
	private static int createHeader(Player player, Inventory inventory, int line, boolean excludeMiddle) {
		int inventorySize = Tools.playerIsJava(player) ? 9 : 6;
		
		int starting = line * inventorySize;
		int ending = ((line + 1) * inventorySize) - 1;
		int middle = starting + 4;
		for(int count = starting; count <= ending; count++) {
			if(excludeMiddle && count == middle) continue;
			int finalCount = count;
			inventory.setItem(finalCount, getIcon(player, Icon.HEADER_LINE));
		}
		return middle;
	}
	
	private enum SortType {
		Time_Created,
		Alphabetical,
		Most_Used,
		Town_Rank,
		Popularity,
		Minecraft_Order,
		Buy_Price,
		Sell_Price,
		Stock
	}
	
	private static ItemStack getSortItem(Player player, SortType type, MenuName menuName) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		//set defaults ?
		if(playerConfig.getString("Travel.Selected." + menuName.toString()) == null) {
			if(menuName == MenuName.Manage_Points)
				playerConfig.set("Travel.Selected." + menuName.toString(), SortType.Time_Created.ordinal());
			if(menuName == MenuName.Online_Players)
				playerConfig.set("Travel.Selected." + menuName.toString(), SortType.Alphabetical.ordinal());
			if(menuName == MenuName.Public_Points)
				playerConfig.set("Travel.Selected." + menuName.toString(), SortType.Most_Used.ordinal());
			if(menuName == MenuName.Towns)
				playerConfig.set("Travel.Selected." + menuName.toString(), SortType.Town_Rank.ordinal());
			if(menuName == MenuName.Item_Finder)
				playerConfig.set("Travel.Selected." + menuName.toString(), SortType.Popularity.ordinal());
			if(menuName == MenuName.Item_Finder_Results)
				playerConfig.set("Travel.Selected." + menuName.toString(), SortType.Buy_Price.ordinal());
			playerConfig.save();
		}
		
		Boolean isSelected = playerConfig.getInt("Travel.Selected." + menuName.toString()) == type.ordinal();
		
		ItemStack item = new ItemStack(isSelected ? Material.MUSIC_DISC_CAT : Material.MUSIC_DISC_STAL);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.AQUA + type.toString().replace("_", " "));
		List<String> lore = new ArrayList<String>();
		if(type == SortType.Time_Created)
			lore.add(ChatColor.WHITE + "Sort in order of " + ChatColor.ITALIC + "time since created" + ChatColor.WHITE + ".");
		if(type == SortType.Alphabetical)
			lore.add(ChatColor.WHITE + "Sort in " + ChatColor.ITALIC + "alphabetical" + ChatColor.WHITE + " order.");
		if(type == SortType.Most_Used)
			lore.add(ChatColor.WHITE + "Sort in order of " + ChatColor.ITALIC + "most used" + ChatColor.WHITE + ".");
		if(type == SortType.Town_Rank)
			lore.add(ChatColor.WHITE + "Sort in order of " + ChatColor.ITALIC + "town rank" + ChatColor.WHITE + ".");
		if(type == SortType.Popularity)
			lore.add(ChatColor.WHITE + "Sort in order of " + ChatColor.ITALIC + "most searched" + ChatColor.WHITE + ".");
		if(type == SortType.Buy_Price)
			lore.add(ChatColor.WHITE + "Sort in order of " + ChatColor.ITALIC + "buy price" + ChatColor.WHITE + ".");
		if(type == SortType.Sell_Price)
			lore.add(ChatColor.WHITE + "Sort in order of " + ChatColor.ITALIC + "sell price" + ChatColor.WHITE + ".");
		if(type == SortType.Stock)
			lore.add(ChatColor.WHITE + "Sort in order of " + ChatColor.ITALIC + "stock available" + ChatColor.WHITE + ".");
		if(type == SortType.Minecraft_Order)
			lore.add(ChatColor.WHITE + "Sort in Minecraft order.");
		
		if(isSelected) {
			Boolean descending = playerConfig.getString("Travel.DescendingOrder." + menuName.toString()) != null;
			
			if(descending)
				lore.add(ChatColor.GRAY + " > Left click for " + ChatColor.BOLD + "normal" + ChatColor.GRAY + " order.");
			else
				lore.add(ChatColor.GRAY + " > Right click for " + ChatColor.BOLD + "opposite" + ChatColor.GRAY + " order.");
		}
		
		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		
		return item;
	}
	
	private enum ToggleType {
		Points_With_Password,
		Points_With_Fee,
		Hidden_Towns
	}
	
	private static ItemStack getToggleItem(Player player, ToggleType type) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		Boolean isHidden = playerConfig.getString("Travel.Toggle." + type.toString()) != null;
		
		ItemStack item = new ItemStack(isHidden ? Material.MUSIC_DISC_STAL : Material.MUSIC_DISC_STRAD);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.AQUA + type.toString().replace("_", " "));
		List<String> lore = new ArrayList<String>();
		if(type == ToggleType.Points_With_Password)
			lore.add(ChatColor.WHITE + "Show points with passwords?");
		if(type == ToggleType.Points_With_Fee)
			lore.add(ChatColor.WHITE + "Show points with fees?");
		if(type == ToggleType.Hidden_Towns)
			lore.add(ChatColor.WHITE + "Show towns with private points?");
		
		if(isHidden)
			lore.add(ChatColor.GRAY + " > Click to " + ChatColor.BOLD + "turn on" + ChatColor.GRAY + ".");
		else lore.add(ChatColor.GRAY + " > Click to " + ChatColor.BOLD + "turn off" + ChatColor.GRAY + ".");
		
		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		
		return item;
	}
	
	private enum PageType {
		DEFAULT, //none set
		PRIVATE,
		PUBLIC,
		SHOP,
		MISCELLANEOUS,
		PLAYER,
		TOWN,
		ITEM_FINDER,
		RESULTS
	}
	
	private enum MenuName {
		Manage_Points,
		Public_Points,
		Online_Players,
		Towns,
		Item_Finder,
		Item_Finder_Results
	}
	
	private static ItemStack getPageItem(PageType type, String direction, int currentPage, int numberOfPages) {
		Boolean isForward = direction.toLowerCase().contains("forward");
		
		//item:
		ItemStack item = new ItemStack(Material.ARROW);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.AQUA + (isForward ? "Next Page" : "Previous Page"));
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.WHITE + (isForward ? "Next" : "Previous") + " " + type.toString().toLowerCase().replace("_", " ") + (type == PageType.ITEM_FINDER || type == PageType.PLAYER ? " page." : " points."));
		
		lore.add(ChatColor.GRAY + " > Showing page " + currentPage + " of " + numberOfPages);
		
		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		item.setItemMeta(meta);
		
		return item;
	}
	
	private static List<String> pointsOrdered(Player player, MenuName menuName, List<String> points) {
		return pointsOrdered(player, menuName, PageType.DEFAULT, points);
	}
	private static List<String> pointsOrdered(Player player, MenuName menuName, PageType pageType, List<String> points) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		SortType order = SortType.values()[playerConfig.getInt("Travel.Selected." + menuName.toString())];
		Boolean descending = playerConfig.getString("Travel.DescendingOrder." + menuName.toString()) != null;
		
		if(order == SortType.Alphabetical) {
			if(descending) {
				Collections.sort(points, String.CASE_INSENSITIVE_ORDER.reversed());
			} else Collections.sort(points, String.CASE_INSENSITIVE_ORDER);
		}
		
		if(order == SortType.Most_Used) {
			if(pageType == PageType.PRIVATE) {
				Collections.sort(points, new Comparator<String>() {
					@Override
					public int compare(String point1, String point2) {
						int uses1 = playerConfig.getString("Travel." + point1 + ".Uses") == null ? 0 : playerConfig.getInt("Travel." + point1 + ".Uses");
						int uses2 = playerConfig.getString("Travel." + point2 + ".Uses") == null ? 0 : playerConfig.getInt("Travel." + point2 + ".Uses");
						if(descending) return Integer.compare(uses1, uses2);
						else return Integer.compare(uses2, uses1);
					}
				});
			}
			if(pageType == PageType.PUBLIC || pageType == PageType.MISCELLANEOUS || pageType == PageType.SHOP) {
				Collections.sort(points, new Comparator<String>() {
					@Override
					public int compare(String point1, String point2) {
						PublicTP tpConfig1 = PublicTP.getConfig(point1);
						int minutesSinceCreated1 = DateCode.getTimeSince(tpConfig1.getString("Created"));
						int daysSinceCreated1 = Math.floorDiv(minutesSinceCreated1, 1440); //the days passed since being created (eg. first day is 0)
						float visits1 = tpConfig1.getInt("UniqueVisits");
						
						float uniqueVisits1 = visits1 / (daysSinceCreated1 + 1);
						
						PublicTP tpConfig2 = PublicTP.getConfig(point2);
						int minutesSinceCreated2 = DateCode.getTimeSince(tpConfig2.getString("Created"));
						int daysSinceCreated2 = Math.floorDiv(minutesSinceCreated2, 1440); //the days passed since being created (eg. first day is 0)
						float visits2 = tpConfig2.getInt("UniqueVisits");
						
						float uniqueVisits2 = visits2 / (daysSinceCreated2 + 1);
						
						if(descending) return Float.compare(uniqueVisits1, uniqueVisits2);
						else return Float.compare(uniqueVisits2, uniqueVisits1);
					}
				});
			}
		}
		
		if(order == SortType.Time_Created) {
			//dont sort unless descending
			if(descending) {
				Collections.reverse(points);
			}
		}
		
		return points;
	}
	
	private static ArrayList<Integer> quickSlots = new ArrayList<Integer>(Arrays.asList(
			3,4,5,12,13,14));
	private static ArrayList<Integer> privateSlots = new ArrayList<Integer>(Arrays.asList(
			19, 20, 21, 22, 23, 24, 25,
			28, 29, 30, 31, 32, 33, 34));
	private static ArrayList<Integer> publicSlots = new ArrayList<Integer>(Arrays.asList(
			46, 47, 48, 49, 50, 51, 52));
	private static ArrayList<Integer> playerSlots = new ArrayList<Integer>(Arrays.asList(
			18, 19, 20, 21, 22, 23, 24, 25, 26,
			27, 28, 29, 30, 31, 32, 33, 34, 35,
			36, 37, 38, 39, 40, 41, 42, 43, 44));
	private static ArrayList<Integer> townSlots = new ArrayList<Integer>(Arrays.asList(
			18, 19, 20, 21, 22, 23, 24, 25, 26));
	private static ArrayList<Integer> itemFinderResultsSlots = new ArrayList<Integer>(Arrays.asList(
			18, 19, 20, 21, 22, 23, 24, 25, 26));
	private static ArrayList<Integer> itemFinderSlots = new ArrayList<Integer>(Arrays.asList(
			9, 10, 11, 12, 13, 14, 15, 16, 17,
			18, 19, 20, 21, 22, 23, 24, 25, 26,
			27, 28, 29, 30, 31, 32, 33, 34, 35,
			36, 37, 38, 39, 40, 41, 42, 43, 44));
	
	//set playerConfig to null if not private
	private static ItemStack getTravelPointItem(String pointName, PageType type, Player player) {
		return getTravelPointItem(pointName, type, player, null);
	}
	private static ItemStack getTravelPointItem(String pointName, PageType type, Player player, Shop shop) {
		
		Material material = Material.RAIL;
		if(type == PageType.PUBLIC || type == PageType.MISCELLANEOUS || type == PageType.ITEM_FINDER) material = Material.POWERED_RAIL;
		
		ItemStack item = new ItemStack(material);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.RESET + "" + ChatColor.YELLOW + ChatColor.BOLD + pointName);
		List<String> lore = new ArrayList<String>();
		if(type == PageType.SHOP) {
			PublicTP tpConfig = PublicTP.getConfig(pointName);
			ShopData shopData = Travel.getShopsAtLocation(LocationCode.Decode(tpConfig.getString("LocationCode")));
			lore.add(ChatColor.WHITE + "Has " + ChatColor.BOLD + shopData.shops + ChatColor.WHITE + " shops (" + shopData.percent + ChatColor.WHITE + " with stock)");
		}
		if(type == PageType.ITEM_FINDER) {
			if(shop.type != Shop.ShopType.MARKET) lore.add(ChatColor.WHITE + "Stock: " + ChatColor.ITALIC + shop.stock);
			lore.add(ChatColor.WHITE + "Buy: " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(shop.buyPrice) + (shop.sellPrice != 0 ? ChatColor.WHITE + " | Sell: " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(shop.sellPrice) : ""));
		}
		if(type == PageType.PRIVATE) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			int uses = playerConfig.getString("Travel." + pointName + ".Uses") == null ? 0 : playerConfig.getInt("Travel." + pointName + ".Uses");
			lore.add(ChatColor.WHITE + "" + uses + " total visits.");
			
			if(playerConfig.getString("Travel." + pointName + ".Icon") != null)
				item.setType(Material.valueOf(playerConfig.getString("Travel." + pointName + ".Icon")));
		}
		if(type == PageType.PUBLIC || type == PageType.MISCELLANEOUS || type == PageType.SHOP || type == PageType.ITEM_FINDER) {
			if(shop != null && shop.type == Shop.ShopType.MARKET) {
				//is a market shop
				meta.setDisplayName(ChatColor.RESET + "" + ChatColor.YELLOW + ChatColor.BOLD + "The Longdale Market");
				item.setType(Material.BREAD);
			} else {
				PublicTP tpConfig = PublicTP.getConfig(pointName);
				if(tpConfig.getString("Created") == null) {
					Chat.logError("Public TP '" + pointName + "' is trying to be accessed but no longer exists.");
					return new ItemStack(Material.AIR);
				}
				int minutesSinceCreated = DateCode.getTimeSince(tpConfig.getString("Created"));
				int daysSinceCreated = Math.floorDiv(minutesSinceCreated, 1440); //the days passed since being created (eg. first day is 0)
				float visits = tpConfig.getInt("UniqueVisits");
				
				float uniqueVisits = visits / (daysSinceCreated + 1);
				if(type != PageType.ITEM_FINDER)
					lore.add(ChatColor.WHITE + String.format("%.1f", uniqueVisits) + " average daily visits.");
				
				if(tpConfig.getString("Icon") != null)
					item.setType(Material.valueOf(tpConfig.getString("Icon")));
				if(tpConfig.getString("Pass") != null)
					lore.add(ChatColor.RED + "Requires password.");
				if(tpConfig.getString("Cost") != null)
					lore.add(ChatColor.WHITE + "Travel cost: " + ChatColor.GOLD + Economy.convertCoins(tpConfig.getInt("Cost")));
			}
		}
		lore.add(ChatColor.GRAY + " > Left click to travel.");
		if(type != PageType.ITEM_FINDER) lore.add(ChatColor.GRAY + " > Middle click to set Quick Slot.");
		if(type == PageType.PRIVATE || type == PageType.PUBLIC) {
			lore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Right click to manage.");
		}
		if(type == PageType.TOWN) {
			item = Town.getBanner(Town.getOriginalTownName(pointName)).clone();
			//use the banner meta instead of normal meta:
			BannerMeta bannerMeta = (BannerMeta) item.getItemMeta();
			
			bannerMeta.setLore(lore);
			bannerMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			item.setItemMeta(bannerMeta);
			return item;
		}
		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		meta.addItemFlags(ItemFlag.HIDE_DYE);
		meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
		meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		item.setItemMeta(meta);
		
		return item;
	}
	
	private static void clearPoints(Inventory inventory, Player player, PageType type) {
		if(type == PageType.PRIVATE || type == PageType.SHOP) {
			for(int slot : privateSlots) {
				inventory.setItem(slot, null);
			}
		}
		if(type == PageType.PUBLIC || type == PageType.MISCELLANEOUS) {
			for(int slot : publicSlots) {
				inventory.setItem(slot, null);
			}
		}
		if(type == PageType.PLAYER) {
			for(int slot : playerSlots) {
				inventory.setItem(slot, null);
			}
		}
		if(type == PageType.TOWN) {
			for(int slot : townSlots) {
				inventory.setItem(slot, null);
			}
		}
		if(type == PageType.RESULTS) {
			for(int slot : itemFinderResultsSlots) {
				inventory.setItem(slot, null);
			}
		}
		if(type == PageType.ITEM_FINDER) {
			for(int slot : itemFinderSlots) {
				inventory.setItem(slot, null);
			}
		}
	}
	
	private static void populatePoints(Player player, Inventory inventory, MenuName menuName, PageType type, List<String> points, int page) {
		
		int itemsOnPage;
		ArrayList<Integer> slots;
		
		int numberOfPages;
		int middleItem;
		
		if(type == PageType.PRIVATE || type == PageType.SHOP) {
			//populate private:
			itemsOnPage = 14;
			slots = privateSlots;
			
			//reset arrows and middle item
			numberOfPages = (int) (Math.ceil(points.size() / 14.0));
			
			inventory.setItem(27, getPageItem(type, "back", 1, numberOfPages));
			inventory.setItem(35, getPageItem(type, "forward", 1, numberOfPages));
			
			middleItem = 13;
		} else {
			//populate public and misc:
			itemsOnPage = 7;
			slots = publicSlots;
			
			//reset arrows and middle item
			numberOfPages = (int) (Math.ceil(points.size() / 7.0));
			
			inventory.setItem(45, getPageItem(type, "back", 1, numberOfPages));
			inventory.setItem(53, getPageItem(type, "forward", 1, numberOfPages));
			
			middleItem = 40;
		}
		
		//set middle item:
		ItemStack mid = inventory.getItem(middleItem);
		ItemMeta meta = mid.getItemMeta();
		List<String> lore = meta.getLore();
		lore.remove(2);
		lore.add(ChatColor.GRAY + " > Showing page 1 of " + numberOfPages + ".");
		meta.setLore(lore);
		mid.setItemMeta(meta);
		
		int start = (itemsOnPage * page) - itemsOnPage;
		List<String> pointsOrdered = pointsOrdered(player, menuName, type, points);
		for(int count = 0; count < slots.size(); count++) {
			if((count + start) >= pointsOrdered.size()) break;
			
			String point = pointsOrdered.get(count + start);
			int slot = slots.get(count);
			
			inventory.setItem(slot, getTravelPointItem(point, type, player));
		}
	}
	
	private static List<Player> getOnlinePlayersAsEditableList() {
		//Bukkit.getOnlinePlayers() returns a non-modifiable list
		List<Player> onlinePlayers = new ArrayList<Player>();
		for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			onlinePlayers.add(onlinePlayer);
		}
		return onlinePlayers;
	}
	
	private static List<Player> onlinePlayersOrdered(Player playerToOrderFor) {
		List<Player> players = getOnlinePlayersAsEditableList();
		PlayerConfig playerConfig = PlayerConfig.getConfig(playerToOrderFor);
		
		SortType order = SortType.values()[playerConfig.getInt("Travel.Selected." + MenuName.Online_Players.toString())];
		Boolean descending = playerConfig.getString("Travel.DescendingOrder." + MenuName.Online_Players.toString()) != null;
		
		if(order == SortType.Alphabetical) {
			Collections.sort(players, new Comparator<Player>() {
				@Override
				public int compare(Player player1, Player player2) {
					PlayerConfig playerConfig1 = PlayerConfig.getConfig(player1);
					String nick1 = playerConfig1.getString("Name") != null ? Chat.stripColor(playerConfig1.getString("Name")) : player1.getName();
					
					PlayerConfig playerConfig2 = PlayerConfig.getConfig(player2);
					String nick2 = playerConfig2.getString("Name") != null ? Chat.stripColor(playerConfig2.getString("Name")) : player2.getName();

					if(descending) return nick2.compareTo(nick1);
					else return nick1.compareTo(nick2);
				}
			});
		}
		
		return players;
	}
	
	private static void populatePlayers(Player player, Inventory inventory, int page) {
		
		int startingSlot = playerSlots.get(0);
		int endingSlot = playerSlots.get(playerSlots.size() - 1);
		int amountOnPage = playerSlots.size();
		
		int startingPlayer = amountOnPage * (page - 1);
		int endingPlayer = amountOnPage * page;
		
		//get online players, excluding this player
		List<Player> onlinePlayers = onlinePlayersOrdered(player);
		onlinePlayers.remove(player);
		
		if(startingPlayer >= onlinePlayers.size()) {
			//page number too great!
			return;
		}
		
		int slotCount = 0;
		for(int count = startingPlayer; count < onlinePlayers.size(); count++) {
			if(count >= endingPlayer) {
				//no more players!
				return;
			}
			Player onlinePlayer = onlinePlayers.get(count);
			
			PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
			String name = onlineConfig.getString("Name") != null ? onlineConfig.getString("Name") : onlinePlayer.getName();
			
			ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
			SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
			skullMeta.setOwningPlayer(onlinePlayer);
			
			//set name and town/nation names
			skullMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + ChatColor.translateAlternateColorCodes('&', name));
			List<String> lore = new ArrayList<String>();
			String originalTownName = onlineConfig.getString("Town");
			if(originalTownName != null) {
				lore.add(ChatColor.WHITE + "" + ChatColor.ITALIC + "Member of " + ChatColor.WHITE + Town.getTownDisplayName(originalTownName));
				
				//check if in nation
				String nationName = TownConfig.getConfig(originalTownName).getString("Nation");
				if(nationName != null) {
					lore.add(ChatColor.WHITE + nationName + ChatColor.WHITE + ChatColor.ITALIC + " nation");
				}
			}
			lore.add(ChatColor.GRAY + " > Click to request travel.");
			skullMeta.setLore(lore);
			skull.setItemMeta(skullMeta);
			
			inventory.setItem(startingSlot + slotCount, skull);
			slotCount++;
		}
	}
	
	//run async
	private static void populateTownPoints(Player player, Inventory inventory, List<String> points, int page) {
		
		int startingSlot = townSlots.get(0);
		int endingSlot = townSlots.get(townSlots.size() - 1);
		int amountOnPage = townSlots.size();
		
		int startingTown = amountOnPage * (page - 1);
		int endingTown = amountOnPage * page;
		
		if(startingTown >= points.size()) {
			//page number too great!
			return;
		}
		
		List<String> townsRanked = Town.getTownsRanked();
		int slotCount = 0;
		for(int count = startingTown; count < points.size(); count++) {
			if(count >= endingTown) {
				break;
			}
			String originalTownName = points.get(count);
			try {
				String townDisplayName = Town.getTownDisplayName(originalTownName);
				TownConfig townConfig = TownConfig.getConfig(originalTownName);
				boolean isPublic = townConfig.getBoolean("Travel.Public");
				int rank = townsRanked.indexOf(originalTownName) + 1;
				
				ItemStack banner = Town.getBanner(originalTownName).clone();
				BannerMeta itemMeta = (BannerMeta) banner.getItemMeta();
				
				//set name and town/nation names
				itemMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + townDisplayName);
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.WHITE + "Town rank: " + ChatColor.GOLD + "#" + ChatColor.BOLD + rank);
				
				boolean hasFee = townConfig.getString("Travel.Fee") != null && townConfig.getInt("Travel.Fee") > 0;
				if(hasFee) {
					lore.add(ChatColor.WHITE + "Travel cost: " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Travel.Fee")));
				}
				
				if(isPublic) {
					lore.add(ChatColor.GRAY + " > Left click to travel.");
				} else {
					lore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.RED + " This town is private.");
				}
				lore.add(ChatColor.GRAY + " > Middle click to set Quick Slot.");
				itemMeta.setLore(lore);
				itemMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
				banner.setItemMeta(itemMeta);
				
				inventory.setItem(startingSlot + slotCount, banner);
			} catch(Exception e) {
				Chat.logError("Error with town banner for /tp menu! **" + originalTownName + "**", e);
			}
			slotCount++;
		}
	}
	
	private static List<String> getTownsWithPointsOrdered(Player playerToOrderFor, boolean showPrivateTowns, boolean showTownsWithFee) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(playerToOrderFor);
		
		SortType order = SortType.values()[playerConfig.getInt("Travel.Selected." + MenuName.Towns.toString())];
		Boolean descending = playerConfig.getString("Travel.DescendingOrder." + MenuName.Towns.toString()) != null;
		
		List<String> townsOrdered = Town.getTownsRanked(); //get all the towns
		if(descending) Collections.reverse(townsOrdered);
		List<String> townsWithPoints = new ArrayList<String>();
		
		for(String originalTownName : townsOrdered) {
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			
			boolean hasTravelPoint = townConfig.getString("Travel.LocationCode") != null;
			if(hasTravelPoint) {
				
				boolean isPublic = townConfig.getBoolean("Travel.Public");
				boolean hasFee = townConfig.getString("Travel.Fee") != null && townConfig.getInt("Travel.Fee") > 0;
				if((isPublic || showPrivateTowns) && (!hasFee || showTownsWithFee)) {
					townsWithPoints.add(originalTownName);
				}
				
			}
		}
		
		return townsWithPoints;
	}
	
	public static void openTownsMenu(Player player) {
		openTownsMenu(player, null);
	}
	public static void openTownsMenu(Player player, Inventory inventory) {
		
		boolean inventoryAlreadyOpen = inventory != null;
		if(!inventoryAlreadyOpen) {
			inventory = Bukkit.createInventory(null, 36, ChatColor.BOLD + "Travel" + ChatColor.DARK_GRAY + ": Towns");
		}
		Inventory finalInventory = inventory;
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		List<String> townsOrdered = getTownsWithPointsOrdered(player, playerConfig.getString("Travel.Toggle." + ToggleType.Hidden_Towns.toString()) == null, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Fee.toString()) == null);
		
		int numberOfPages = (int) (Math.ceil(townsOrdered.size() / 9.0));
		
		//set sorting options:
		inventory.setItem(4, getSortItem(player, SortType.Town_Rank, MenuName.Towns));
		
		inventory.setItem(7, getToggleItem(player, ToggleType.Hidden_Towns));
		inventory.setItem(8, getToggleItem(player, ToggleType.Points_With_Fee));
		
		//arrows:
		inventory.setItem(27, getPageItem(PageType.TOWN, "back", 1, numberOfPages));
		inventory.setItem(35, getPageItem(PageType.TOWN, "forward", 1, numberOfPages));
		
		//header:
		int middleItemSlot = createHeader(player, inventory, 1, true);
		ItemStack middleItem = new ItemStack(Material.SHIELD);
		ItemMeta meta = middleItem.getItemMeta();
		meta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Towns " + ChatColor.YELLOW + ChatColor.BOLD + "<<");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.WHITE + "Player owned towns.");
		lore.add(ChatColor.GRAY + " > There are " + DeadMC.TownFile.data().getStringList("Active").size() + " active towns.");
		lore.add(ChatColor.GRAY + " > Showing page 1 of " + numberOfPages + ".");
		meta.setLore(lore);
		middleItem.setItemMeta(meta);
		inventory.setItem(middleItemSlot, middleItem);
		
		//back button:
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = new ArrayList<String>();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + "Quick Menu" + ChatColor.WHITE + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			//clear points:
			if(inventoryAlreadyOpen) {
				clearPoints(finalInventory, player, PageType.TOWN);
			}
			
			//populate points:
			populateTownPoints(player, finalInventory, townsOrdered, 1);
		});
		
		if(!inventoryAlreadyOpen) {
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(finalInventory));
		}
	}
	
	public static void openPlayerMenu(Player player) {
		openPlayerMenu(player, null);
	}
	public static void openPlayerMenu(Player player, Inventory inventory) {
		
		boolean inventoryAlreadyOpen = inventory != null;
		if(!inventoryAlreadyOpen) {
			inventory = Bukkit.createInventory(null, 54, ChatColor.BOLD + "Travel" + ChatColor.DARK_GRAY + ": Online Players");
		}
		Inventory finalInventory = inventory;
		
		//back button:
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = new ArrayList<String>();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + "Quick Menu" + ChatColor.WHITE + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		//set sorting options:
		inventory.setItem(4, getSortItem(player, SortType.Alphabetical, MenuName.Online_Players));
		
		//header
		createHeader(player, inventory, 1);
		
		//arrows:
		int numberOfPages = (int) Math.ceil(Bukkit.getOnlinePlayers().size() / 27.0);
		inventory.setItem(45, getPageItem(PageType.PLAYER, "back", 1, numberOfPages));
		inventory.setItem(53, getPageItem(PageType.PLAYER, "forward", 1, numberOfPages));
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			//clear
			clearPoints(finalInventory, player, PageType.PLAYER);
			
			//populate
			populatePlayers(player, finalInventory, 1);
		});
		
		if(!inventoryAlreadyOpen) {
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(finalInventory));
		}
	}
	
	public static void openManageMenu(Player player) {
		openManageMenu(player, null);
	}
	public static void openManageMenu(Player player, Inventory inventory) {
		boolean inventoryAlreadyOpen = inventory != null;
		if(!inventoryAlreadyOpen) {
			inventory = Bukkit.createInventory(null, 54, ChatColor.BOLD + "Travel" + ChatColor.DARK_GRAY + ": Manage Points");
		}
		Inventory finalInventory = inventory;
		
		//set sorting options:
		inventory.setItem(3, getSortItem(player, SortType.Time_Created, MenuName.Manage_Points));
		inventory.setItem(4, getSortItem(player, SortType.Alphabetical, MenuName.Manage_Points));
		inventory.setItem(5, getSortItem(player, SortType.Most_Used, MenuName.Manage_Points));
		
		//arrows
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		List<String> privateTravelPoints = playerConfig.getStringList("Travel.Private") != null ? playerConfig.getStringList("Travel.Private") : new ArrayList<String>();
		List<String> publicTravelPoints = playerConfig.getStringList("Travel.Public") != null ? playerConfig.getStringList("Travel.Public") : new ArrayList<String>();

		int numberOfPages_Private = (int) (Math.ceil(privateTravelPoints.size() / 14.0));
		int numberOfPages_Public = (int) (Math.ceil(publicTravelPoints.size() / 7.0));
		
		inventory.setItem(27, getPageItem(PageType.PRIVATE, "back", 1, numberOfPages_Private));
		inventory.setItem(35, getPageItem(PageType.PRIVATE, "forward", 1, numberOfPages_Private));
		
		inventory.setItem(45, getPageItem(PageType.PUBLIC, "back", 1, numberOfPages_Public));
		inventory.setItem(53, getPageItem(PageType.PUBLIC, "forward", 1, numberOfPages_Public));
		
		//private header:
		int middleItemSlot = createHeader(player, inventory, 1, true);
		ItemStack middleItem = new ItemStack(Material.CHEST_MINECART);
		ItemMeta meta = middleItem.getItemMeta();
		meta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Private " + ChatColor.YELLOW + ChatColor.BOLD + "<<");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.WHITE + "Manage your " + ChatColor.BOLD + "private" + ChatColor.WHITE + " points below.");
		lore.add(ChatColor.GRAY + " > You have " + privateTravelPoints.size() + " private points.");
		lore.add(ChatColor.GRAY + " > Showing page 1 of " + numberOfPages_Private + ".");
		meta.setLore(lore);
		middleItem.setItemMeta(meta);
		inventory.setItem(middleItemSlot, middleItem);
		
		//public header
		int middleItemSlot2 = createHeader(player, inventory, 4, true);
		ItemStack middleItem2 = new ItemStack(Material.HOPPER_MINECART);
		ItemMeta meta2 = middleItem2.getItemMeta();
		meta2.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Public " + ChatColor.YELLOW + ChatColor.BOLD + "<<");
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.WHITE + "Manage your " + ChatColor.BOLD + "public" + ChatColor.WHITE + " points below.");
		lore2.add(ChatColor.GRAY + " > You have " + publicTravelPoints.size() + " public points.");
		lore2.add(ChatColor.GRAY + " > Showing page 1 of " + numberOfPages_Public + ".");
		meta2.setLore(lore2);
		middleItem2.setItemMeta(meta2);
		inventory.setItem(middleItemSlot2, middleItem2);
		
		//side bits
		inventory.setItem(18, getIcon(player, Icon.HEADER_LINE));
		inventory.setItem(26, getIcon(player, Icon.HEADER_LINE));
		
		//back button:
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = new ArrayList<String>();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + "Quick Menu" + ChatColor.WHITE + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			//clear points?
			if(inventoryAlreadyOpen) {
				clearPoints(finalInventory, player, PageType.PRIVATE);
				clearPoints(finalInventory, player, PageType.PUBLIC);
			}
			
			//populate points:
			populatePoints(player, finalInventory, MenuName.Manage_Points, PageType.PRIVATE, privateTravelPoints, 1);
			populatePoints(player, finalInventory, MenuName.Manage_Points, PageType.PUBLIC, publicTravelPoints, 1);
		});
		
		if(!inventoryAlreadyOpen) {
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(finalInventory));
		}
	}
	
	public static HashMap<String, Integer> resetXP = new HashMap<String, Integer>();
	@EventHandler
	public void gainExp(PlayerExpChangeEvent event) {
		Player player = event.getPlayer();
		if(resetXP.containsKey(player.getName())) {
			event.setAmount(0); //cant gain exp while anvil menu open
		}
	}
	
	public static void openManagePrivateTP(Player player, String pointName) {
		Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.BOLD + "Manage" + ChatColor.DARK_GRAY + ": Private TP");
		
		ItemStack deleteItem = getIcon(player, Icon.DELETE_PRIVATE_TP);
		ItemMeta meta = deleteItem.getItemMeta();
		List<String> lore = meta.getLore() == null ? new ArrayList<String>() : meta.getLore();
		lore.add(ChatColor.GRAY + " > Remove " + pointName);
		meta.setLore(lore);
		deleteItem.setItemMeta(meta);
		inventory.setItem(8, deleteItem);
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		ItemStack iconItem = new ItemStack(playerConfig.getString("Travel." + pointName + ".Icon") != null ? Material.valueOf(playerConfig.getString("Travel." + pointName + ".Icon")) : Material.ITEM_FRAME);
		ItemMeta iconmeta = iconItem.getItemMeta();
		iconmeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Set Icon");
		List<String> iconlore = new ArrayList<String>();
		iconlore.add(ChatColor.WHITE + "Choose the icon for " + pointName + ".");
		if(playerConfig.getString("Travel." + pointName + ".Icon") != null) {
			iconlore.add(ChatColor.GRAY + " > Current: " + ChatColor.AQUA + playerConfig.getString("Travel." + pointName + ".Icon").replace("_", " "));
		}
		iconmeta.setLore(iconlore);
		iconItem.setItemMeta(iconmeta);

		inventory.setItem(4, iconItem);
		
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = backmeta.getLore() == null ? new ArrayList<String>() : backmeta.getLore();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + "Manage Points" + ChatColor.GRAY + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
	}
	
	public static void openManagePublicTP(Player player, String pointName) {
		openManagePublicTP(player, pointName, true);
	}
	public static void openManagePublicTP(Player player, String pointName, boolean openedAsync) {
		Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.BOLD + "Manage" + ChatColor.DARK_GRAY + ": Public TP");
		
		ItemStack deleteItem = getIcon(player, Icon.DELETE_PUBLIC_TP);
		ItemMeta meta = deleteItem.getItemMeta();
		List<String> lore = meta.getLore() == null ? new ArrayList<String>() : meta.getLore();
		lore.add(ChatColor.GRAY + " > Remove " + pointName);
		meta.setLore(lore);
		deleteItem.setItemMeta(meta);
		
		PublicTP tpConfig = PublicTP.getConfig(pointName);
		
		ItemStack passItem = getIcon(player, Icon.PUBLIC_SET_PASS);
		ItemMeta passmeta = passItem.getItemMeta();
		List<String> passlore = passmeta.getLore() == null ? new ArrayList<String>() : passmeta.getLore();
		passlore.add(ChatColor.GRAY + " > Set the password for " + pointName);
		if(tpConfig.getString("Pass") != null) {
			String pass = tpConfig.getString("Pass");
			String hidePassword = "";
			for(int count = 1; count < (pass.length() - 1); count++)
				hidePassword = hidePassword + "*";
			passlore.add(ChatColor.GRAY + " > Current: " + ChatColor.AQUA + pass.substring(0, 1) + hidePassword + pass.substring(pass.length() - 1, pass.length()));
			passlore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Right click to remove.");
		}
		passmeta.setLore(passlore);
		passItem.setItemMeta(passmeta);
		
		ItemStack feeItem = getIcon(player, Icon.PUBLIC_SET_FEE);
		ItemMeta feemeta = feeItem.getItemMeta();
		List<String> feelore = feemeta.getLore() == null ? new ArrayList<String>() : feemeta.getLore();
		feelore.add(ChatColor.GRAY + " > Set the fee for " + pointName);
		if(tpConfig.getString("Cost") != null) {
			feelore.add(ChatColor.GRAY + " > Current: " + ChatColor.GOLD + Economy.convertCoins(tpConfig.getInt("Cost")));
			feelore.add(ChatColor.DARK_RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Right click to remove.");
		}
		feemeta.setLore(feelore);
		feeItem.setItemMeta(feemeta);
		
		ItemStack iconItem = new ItemStack(tpConfig.getString("Icon") != null ? Material.valueOf(tpConfig.getString("Icon")) : Material.ITEM_FRAME);
		ItemMeta iconmeta = iconItem.getItemMeta();
		iconmeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Set Icon");
		List<String> iconlore = new ArrayList<String>();
		iconlore.add(ChatColor.WHITE + "Choose the icon for " + pointName + ".");
		if(tpConfig.getString("Icon") != null) {
			iconlore.add(ChatColor.GRAY + " > Current: " + ChatColor.AQUA + tpConfig.getString("Icon").replace("_", " "));
		}
		iconmeta.setLore(iconlore);
		iconItem.setItemMeta(iconmeta);
		
		inventory.setItem(3, passItem);
		inventory.setItem(4, iconItem);
		inventory.setItem(5, feeItem);
		inventory.setItem(8, deleteItem);
		
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = backmeta.getLore() == null ? new ArrayList<String>() : backmeta.getLore();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + "Manage Points" + ChatColor.GRAY + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		if(openedAsync) {
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
		} else {
			player.openInventory(inventory);
		}
	}
	
	public static void openQuickSlotMenu(Player player, String pointName, String type, String previousMenu) {
		Inventory inventory = Bukkit.createInventory(null, 18, ChatColor.BOLD + "Travel" + ChatColor.DARK_GRAY + ": Quick Slots");
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		List<ItemStack> slots = new ArrayList<>(Arrays.asList(
				getIcon(player, Icon.QUICK_SLOT_1),
				getIcon(player, Icon.QUICK_SLOT_2),
				getIcon(player, Icon.QUICK_SLOT_3),
				getIcon(player, Icon.QUICK_SLOT_4),
				getIcon(player, Icon.QUICK_SLOT_5),
				getIcon(player, Icon.QUICK_SLOT_6)
		));
		
		int count = 1;
		for(ItemStack slot : slots) {
			if(slot.getType() != Material.RED_STAINED_GLASS_PANE) {
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.WHITE + "Add " + ChatColor.GOLD + pointName + ChatColor.WHITE + " here?");
				if(!slot.getType().toString().toLowerCase().contains("stained_glass_pane")) {
					String replacingName = playerConfig.getString("Travel.QuickSlot" + count + ".Name");
					lore.add(ChatColor.WHITE + "Replace: " + ChatColor.GOLD + replacingName);
				}
				lore.add(ChatColor.GRAY + " > " + type + " point.");
				lore.add(ChatColor.GRAY + " > Click to swap.");
				
				ItemMeta meta = slot.getItemMeta();
				meta.setLore(lore);
				slot.setItemMeta(meta);
			}
			
			inventory.setItem(quickSlots.get(count-1), slot);
			count++;
		}
		
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backMeta = backButton.getItemMeta();
		List<String> backLore = backMeta.getLore() == null ? new ArrayList<String>() : backMeta.getLore();
		backLore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + previousMenu + ChatColor.GRAY + ".");
		backMeta.setLore(backLore);
		backButton.setItemMeta(backMeta);
		inventory.setItem(0, backButton);
		
		Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
	}
	
	
	//run this async! searches all public points
	public static PublicPointsGrouped publicPointsGrouped(boolean showPointsWithPassword, boolean showPointsWithFee) {
		List<String> publicPoints = DeadMC.TravelFile.data().getStringList("Public");
		
		List<String> shops = new ArrayList<String>();
		List<String> misc = new ArrayList<String>();
		
		int stockedShopsNeeded = 3;
		for(String point : publicPoints) {
			PublicTP tpConfig = PublicTP.getConfig(point);
			if(!showPointsWithPassword && tpConfig.getString("Pass") != null) continue;
			if(!showPointsWithFee && tpConfig.getString("Cost") != null) continue;
			if(tpConfig.getString("LocationCode") == null) continue;
			
			ShopData shopData = Travel.getShopsAtLocation(LocationCode.Decode(tpConfig.getString("LocationCode")));
			if(shopData == null || shopData.stocked < stockedShopsNeeded) {
				misc.add(point);
			}
			if(shopData != null && shopData.stocked >= stockedShopsNeeded) {
				shops.add(point);
			}
		}
		return new PublicPointsGrouped(shops, misc);
	}
	
	public static String getItemFinderNameFromItem(ItemStack itemStack) {
		
		Material material = itemStack.getType();
		if(!material.isItem() || material.isAir() || material == Material.DEBUG_STICK || (material == Material.PLAYER_HEAD && itemStack.hasItemMeta() && itemStack.getItemMeta().getDisplayName().contains("Backpack"))) {
			// item is not applicable for an item finder name
			return null;
		}
		
		String customItemName = CustomItems.getCustomItemName(itemStack);
		Chat.debug(customItemName);
		boolean isCustomItem = customItemName != null;
		if(isCustomItem) {
			return "SPECIAL:" + customItemName;
		}
		
		boolean isEnchantedBook = itemStack.hasItemMeta() && itemStack.getItemMeta() instanceof EnchantmentStorageMeta enchantedBook;
		if(isEnchantedBook) {
			EnchantmentStorageMeta enchantedBook = (EnchantmentStorageMeta) itemStack.getItemMeta();
			for(Enchantment enchantment : enchantedBook.getStoredEnchants().keySet()) {
				return "ENCHANT:" + enchantment.toString().toLowerCase();
			}
		}
		
		return material.toString();
		
	}
	
	private static List<String> itemFinderItemNames = new ArrayList<String>();
	public static List<String> getItemFinderItemNames() {
		if(itemFinderItemNames.size() == 0) { //only get reference for items once on start (never changes except when new items are released)
			List<String> blocks = new ArrayList<String>();
			for(Material block : Material.values()) {
				if(block.isItem() && !block.isAir() && block != Material.DEBUG_STICK)
					blocks.add(block.toString());
			}
			
			for(String block : blocks) {
				itemFinderItemNames.add(block);
			}
			
			for(Enchantment enchantment : Enchantment.values()) {
				String name = enchantment.getKey().toString().substring(10);
				String fullName = "ENCHANT:" + name;
				itemFinderItemNames.add(fullName);
			}
			for(CustomItems.CustomItem customItem : CustomItems.CustomItem.values()) {
				String name = customItem.toString().toLowerCase();
				String fullName = "SPECIAL:" + name;
				itemFinderItemNames.add(fullName);
			}
		}
		
		return itemFinderItemNames;
	}

	private static List<String> getItemsSorted(Player player) {
		return getItemsSorted(player, "");
	}
	private static List<String> getItemsSorted(Player player, String search) {
		
		List<String> applicableNames = new ArrayList<String>();
		if(search.equalsIgnoreCase("")) {
			applicableNames.addAll(getItemFinderItemNames());
		} else {
			for(String itemFinderItemName : getItemFinderItemNames()) {
				if(itemFinderItemName.toLowerCase().contains(search.toLowerCase())) {
					applicableNames.add(itemFinderItemName);
				}
			}
		}
		
		//sort:
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		SortType order = SortType.values()[playerConfig.getInt("Travel.Selected." + MenuName.Item_Finder.toString())];
		Boolean descending = playerConfig.getString("Travel.DescendingOrder." + MenuName.Item_Finder.toString()) != null;
		
		if(order == SortType.Minecraft_Order) {
			if(descending) {
				Collections.reverse(applicableNames);
			}
		}
		
		if(order == SortType.Alphabetical) {
			if(descending) {
				Collections.sort(applicableNames, String.CASE_INSENSITIVE_ORDER.reversed());
			} else Collections.sort(applicableNames, String.CASE_INSENSITIVE_ORDER);
		}
		
		if(order == SortType.Popularity) {
			Collections.sort(applicableNames, new Comparator<String>() {
				@Override
				public int compare(String item1, String item2) {
					int uses1 = DeadMC.MarketFile.data().getString("ItemFinder." + item1) == null ? 0 : DeadMC.MarketFile.data().getInt("ItemFinder." + item1);
					int uses2 = DeadMC.MarketFile.data().getString("ItemFinder." + item2) == null ? 0 : DeadMC.MarketFile.data().getInt("ItemFinder." + item2);
					if(descending) return Integer.compare(uses1, uses2);
					else return Integer.compare(uses2, uses1);
				}
			});
		}
		
		return applicableNames;
	}
	
	private static ItemStack getItemFinderIcon(String itemFinderName) {
		ItemStack itemStack = null;
		if(itemFinderName.contains("SPECIAL:")) {
			CustomItems.CustomItem customItem = CustomItems.CustomItem.valueOf(ChatColor.stripColor(itemFinderName).replace("SPECIAL:", "").toUpperCase().replace(" ", "_"));
			itemStack = CustomItems.getCustomItem(customItem, false);
			itemStack.setAmount(1);
			if(itemStack.getItemMeta() instanceof Damageable damageable) {
				damageable.setDamage(0);
			}
		} else {
			itemStack = itemFinderName.contains("ENCHANT:") ? new ItemStack(Material.ENCHANTED_BOOK) : new ItemStack(Material.valueOf(ChatColor.stripColor(itemFinderName)));
		}
		ItemMeta itemMeta = itemStack.getItemMeta();
		itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		itemMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		itemMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + itemFinderName.toUpperCase().replace("_", " ").replace("ENCHANT:", "(enchant) ").replace("SPECIAL:", "(special) "));
		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}
	
	private static void populateItemFinder(Player player, Inventory inventory, List<String> itemNamesSorted, int page) {
		int startingSlot = itemFinderSlots.get(0);
		int endingSlot = itemFinderSlots.get(itemFinderSlots.size() - 1);
		int amountOnPage = itemFinderSlots.size();
		
		int startingItem = amountOnPage * (page - 1);
		int endingItem = amountOnPage * page;
		
		if(startingItem >= itemNamesSorted.size()) {
			//page number too great!
			return;
		}
		
		int slotCount = 0;
		for(int count = startingItem; count < itemNamesSorted.size(); count++) {
			if(count >= endingItem) {
				break;
			}
			
			//if it contains enchant, show enchant book
			//if it contains special, show special item
			//else show item
			ItemStack material = getItemFinderIcon(itemNamesSorted.get(count));
			ItemMeta itemMeta = material.getItemMeta();
			
			//set name and description
			itemMeta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + itemNamesSorted.get(count).toUpperCase().replace("_", " ").replace("ENCHANT:", "(enchant) ").replace("SPECIAL:", "(special) "));
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + " > Click to browse shops.");
			itemMeta.setLore(lore);
			material.setItemMeta(itemMeta);
			
			inventory.setItem(startingSlot + slotCount, material);
			slotCount++;
		}
	}
	
	private static void populateItemFinderResults(Player player, Inventory inventory, List<Shop> shopsWithStock, int page) {
		int startingSlot = townSlots.get(0);
		int endingSlot = townSlots.get(townSlots.size() - 1);
		int amountOnPage = townSlots.size();
		
		int startingPoint = amountOnPage * (page - 1);
		int endingPoint = amountOnPage * page;
		
		if(startingPoint >= shopsWithStock.size()) {
			//page number too great!
			return;
		}
		
		int slotCount = 0;
		for(int count = startingPoint; count < shopsWithStock.size(); count++) {
			if(count >= endingPoint) {
				break;
			}

			Shop shop = shopsWithStock.get(count);
			inventory.setItem(startingSlot + slotCount, getTravelPointItem(shop.travelPoint, PageType.ITEM_FINDER, player, shop));
			slotCount++;
		}
	}
	
	private static List<Shop> sortShopsForGUI(Player player, List<Shop> shops) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		SortType order = SortType.values()[playerConfig.getInt("Travel.Selected." + MenuName.Item_Finder_Results.toString())];
		Boolean descending = playerConfig.getString("Travel.DescendingOrder." + MenuName.Item_Finder_Results.toString()) != null;
		
		if(order == SortType.Buy_Price) {
			Collections.sort(shops, new Comparator<Shop>() {
				@Override
				public int compare(Shop shop1, Shop shop2) {
					if(descending) return Integer.compare(shop2.buyPrice, shop1.buyPrice);
					else return Integer.compare(shop1.buyPrice, shop2.buyPrice);
				}
			});
		}
		if(order == SortType.Sell_Price) {
			Collections.sort(shops, new Comparator<Shop>() {
				@Override
				public int compare(Shop shop1, Shop shop2) {
					if(descending) return Integer.compare(shop1.sellPrice, shop2.sellPrice);
					else return Integer.compare(shop2.sellPrice, shop1.sellPrice);
				}
			});
		}
		if(order == SortType.Stock) {
			Collections.sort(shops, new Comparator<Shop>() {
				@Override
				public int compare(Shop shop1, Shop shop2) {
					if(descending) return Integer.compare(shop1.stock, shop2.stock);
					else return Integer.compare(shop2.stock, shop1.stock);
				}
			});
		}
		
		return shops;
	}
	
	public static void openItemFinderResultsMenu(Player player, String itemFinderName) {
		openItemFinderResultsMenu(player, itemFinderName, null, "Item Finder");
	}
	public static void openItemFinderResultsMenu(Player player, String itemFinderName, Inventory inventory, String previousMenu) {
		
		if(itemFinderName.toUpperCase().contains("LOG") && TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.OPEN_THE_SHOP_FINDER_MENU)) {
			TaskManager.stepTask(player.getUniqueId());
		}
		
		boolean inventoryAlreadyOpen = inventory != null;
		if(!inventoryAlreadyOpen) {
			inventory = Bukkit.createInventory(null, 36, ChatColor.BOLD + "Item Finder" + ChatColor.DARK_GRAY + ": Results");
		}
		Inventory finalInventory = inventory;

		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		List<Shop> shopsWithStock = Shops.getShopsWithStock(itemFinderName, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Password.toString()) == null, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Fee.toString()) == null);
		List<Shop> shopsSorted = sortShopsForGUI(player, shopsWithStock);
		int numberOfPages = (int) (Math.ceil(shopsWithStock.size() / 9.0));
		
		//set sorting options:
		inventory.setItem(3, getSortItem(player, SortType.Buy_Price, MenuName.Item_Finder_Results));
		inventory.setItem(4, getSortItem(player, SortType.Sell_Price, MenuName.Item_Finder_Results));
		inventory.setItem(5, getSortItem(player, SortType.Stock, MenuName.Item_Finder_Results));
		
		inventory.setItem(7, getToggleItem(player, ToggleType.Points_With_Password));
		inventory.setItem(8, getToggleItem(player, ToggleType.Points_With_Fee));
		
		//arrows:
		inventory.setItem(27, getPageItem(PageType.RESULTS, "back", 1, numberOfPages));
		inventory.setItem(35, getPageItem(PageType.RESULTS, "forward", 1, numberOfPages));
		
		//header:
		int middleItemSlot = createHeader(player, inventory, 1, true);
		ItemStack middleItem = getItemFinderIcon(itemFinderName);
		ItemMeta meta = middleItem.getItemMeta();
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + " > There are " + shopsWithStock.size() + " shops with stock.");
		lore.add(ChatColor.GRAY + " > Showing page 1 of " + numberOfPages + ".");
		meta.setLore(lore);
		middleItem.setItemMeta(meta);
		inventory.setItem(middleItemSlot, middleItem);
		
		//back button:
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = new ArrayList<String>();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + previousMenu + ChatColor.WHITE + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			//populate points:
			populateItemFinderResults(player, finalInventory, shopsSorted, 1);
		});
		
		if(!inventoryAlreadyOpen) {
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(finalInventory));
		}
	}
	
	public static void openItemFinderSearchMenu(Player player) {
		//if opening anvil menu, set player XP to 0 first or they can take the item.
		if(resetXP.containsKey(player.getName()))
			resetXP.remove(player.getName());
		resetXP.put(player.getName(), player.getLevel());
		player.setLevel(0);
		
		new AnvilGUI.Builder()
				.onClose(p -> {
					//give original XP back
					if(resetXP.containsKey(player.getName())) {
						player.setLevel(resetXP.get(player.getName()));
						resetXP.remove(player.getName());
					}
				})
				
				.onComplete((p, search) -> { //called when the inventory output slot is clicked
					openItemFinderMenu(player, search, false);
					return AnvilGUI.Response.close();
				})
				.text("iron") //starting text
				.itemLeft(new ItemStack(Material.NAME_TAG))
				.onLeftInputClick(p -> Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Click the output slot to continue."))
				
				.title(ChatColor.BOLD + "Item search" + ChatColor.RESET + ":") //set the title of the GUI
				.plugin(plugin)
				.open(player);
	}
	
	public static void openItemFinderMenu(Player player) {
		openItemFinderMenu(player, "", null, true);
	}
	public static void openItemFinderMenu(Player player, String search, boolean openingAsync) {
		openItemFinderMenu(player, search, null, openingAsync);
	}
	public static void openItemFinderMenu(Player player, Inventory inventory) {
		openItemFinderMenu(player, "", inventory, true);
	}
	public static void openItemFinderMenu(Player player, String search, Inventory inventory, boolean openingAsync) {
		
		String name = search.length() > 0 ? "\"" + search + "\"" : "Select Item";
		
		boolean inventoryAlreadyOpen = inventory != null;
		if(!inventoryAlreadyOpen) {
			inventory = Bukkit.createInventory(null, 54, ChatColor.BOLD + "Item Finder" + ChatColor.DARK_GRAY + ": " + name);
		}
		Inventory finalInventory = inventory;
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		//back button:
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = new ArrayList<String>();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + (search.length() > 0 ? "Item Finder" : "Quick Menu") + ChatColor.WHITE + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		List<String> itemsSorted = getItemsSorted(player, search);
		int numberOfPages = (int) (Math.ceil(itemsSorted.size() / 36.0));
		
		//set sorting options:
		inventory.setItem(3, getSortItem(player, SortType.Popularity, MenuName.Item_Finder));
		inventory.setItem(4, getSortItem(player, SortType.Minecraft_Order, MenuName.Item_Finder));
		inventory.setItem(5, getSortItem(player, SortType.Alphabetical, MenuName.Item_Finder));
		
		//arrows:
		inventory.setItem(45, getPageItem(PageType.ITEM_FINDER, "back", 1, numberOfPages));
		inventory.setItem(53, getPageItem(PageType.ITEM_FINDER, "forward", 1, numberOfPages));
		
		//search button:
		ItemStack searchButton = new ItemStack(Material.COMPASS);
		ItemMeta meta = searchButton.getItemMeta();
		meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "SEARCH");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.WHITE + "Use keywords to find specific items.");
		lore.add(ChatColor.GRAY + " > Click to open.");
		meta.setLore(lore);
		searchButton.setItemMeta(meta);
		inventory.setItem(49, searchButton);
		
		//help info:
		ItemStack help = new ItemStack(Material.KNOWLEDGE_BOOK);
		ItemMeta helpMeta = help.getItemMeta();
		helpMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "HELP");
		List<String> helpLore = new ArrayList<String>();
		helpLore.add(ChatColor.WHITE + "Want to list your own shop and items?");
		helpLore.add(ChatColor.GRAY + " (1) Create a shop with /shop.");
		helpLore.add(ChatColor.GRAY + " (2) Set a " + ChatColor.UNDERLINE + "public" + ChatColor.GRAY + " travel point (donator).");
		helpLore.add(ChatColor.GRAY + " > Make sure the TP is on the same land as the shop.");
		helpMeta.setLore(helpLore);
		help.setItemMeta(helpMeta);
		inventory.setItem(8, help);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			//clear?
			if(inventoryAlreadyOpen) {
				clearPoints(finalInventory, player, PageType.ITEM_FINDER);
			}
			
			//populate:
			populateItemFinder(player, finalInventory, itemsSorted, 1); //in case opening sync (ie. from anvil menu), run this async
		});
		
		if(!inventoryAlreadyOpen) {
			if(openingAsync) {
				Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(finalInventory));
			} else {
				player.openInventory(finalInventory);
			}
		}
	}
	
	public static void openPublicMenu(Player player) {
		openPublicMenu(player, null);
	}
	public static void openPublicMenu(Player player, Inventory inventory) {
		
		boolean inventoryAlreadyOpen = inventory != null;
		if(!inventoryAlreadyOpen) {
			inventory = Bukkit.createInventory(null, 54, ChatColor.BOLD + "Travel" + ChatColor.DARK_GRAY + ": Public Points");
		}
		Inventory finalInventory = inventory;
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		PublicPointsGrouped publicPoints = publicPointsGrouped(playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Password.toString()) == null, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Fee.toString()) == null);
		
		int numberOfPages_Shops = (int) (Math.ceil(publicPoints.shops.size() / 14.0));
		int numberOfPages_Misc = (int) (Math.ceil(publicPoints.misc.size() / 7.0));
		
		//set sorting options:
		inventory.setItem(3, getSortItem(player, SortType.Time_Created, MenuName.Public_Points));
		inventory.setItem(4, getSortItem(player, SortType.Alphabetical, MenuName.Public_Points));
		inventory.setItem(5, getSortItem(player, SortType.Most_Used, MenuName.Public_Points));
		
		inventory.setItem(7, getToggleItem(player, ToggleType.Points_With_Password));
		inventory.setItem(8, getToggleItem(player, ToggleType.Points_With_Fee));
		
		//arrows:
		inventory.setItem(27, getPageItem(PageType.SHOP, "back", 1, numberOfPages_Shops));
		inventory.setItem(35, getPageItem(PageType.SHOP, "forward", 1, numberOfPages_Shops));
		
		inventory.setItem(45, getPageItem(PageType.MISCELLANEOUS, "back", 1, numberOfPages_Misc));
		inventory.setItem(53, getPageItem(PageType.MISCELLANEOUS, "forward", 1, numberOfPages_Misc));
		
		//private header:
		int middleItemSlot = createHeader(player, inventory, 1, true);
		ItemStack middleItem = new ItemStack(Material.CHEST);
		ItemMeta meta = middleItem.getItemMeta();
		meta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Shops " + ChatColor.YELLOW + ChatColor.BOLD + "<<");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.WHITE + "Player owned shops.");
		lore.add(ChatColor.GRAY + " > There are " + publicPoints.shops.size() + " shop points.");
		lore.add(ChatColor.GRAY + " > Showing page 1 of " + numberOfPages_Shops + ".");
		meta.setLore(lore);
		middleItem.setItemMeta(meta);
		inventory.setItem(middleItemSlot, middleItem);
		
		//public header
		int middleItemSlot2 = createHeader(player, inventory, 4, true);
		ItemStack middleItem2 = new ItemStack(Material.MINECART);
		ItemMeta meta2 = middleItem2.getItemMeta();
		meta2.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + ">>" + ChatColor.GOLD + ChatColor.BOLD + " Miscellaneous " + ChatColor.YELLOW + ChatColor.BOLD + "<<");
		List<String> lore2 = new ArrayList<String>();
		lore2.add(ChatColor.WHITE + "Player owned travel points.");
		lore2.add(ChatColor.GRAY + " > There are " + publicPoints.misc.size() + " non-shop points.");
		lore2.add(ChatColor.GRAY + " > Showing page 1 of " + numberOfPages_Misc + ".");
		meta2.setLore(lore2);
		middleItem2.setItemMeta(meta2);
		inventory.setItem(middleItemSlot2, middleItem2);
		
		//side bits
		inventory.setItem(18, getIcon(player, Icon.HEADER_LINE));
		inventory.setItem(26, getIcon(player, Icon.HEADER_LINE));
		
		//back button:
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta backmeta = backButton.getItemMeta();
		List<String> backlore = new ArrayList<String>();
		backlore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + "Quick Menu" + ChatColor.WHITE + ".");
		backmeta.setLore(backlore);
		backButton.setItemMeta(backmeta);
		inventory.setItem(0, backButton);
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			//clear points
			if(inventoryAlreadyOpen) {
				clearPoints(finalInventory, player, PageType.SHOP);
				clearPoints(finalInventory, player, PageType.MISCELLANEOUS);
			}
			
			//populate points:
			populatePoints(player, finalInventory, MenuName.Public_Points, PageType.SHOP, publicPoints.shops, 1);
			populatePoints(player, finalInventory, MenuName.Public_Points, PageType.MISCELLANEOUS, publicPoints.misc, 1);
		});
		
		if(!inventoryAlreadyOpen) {
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(finalInventory));
		}
	}
	
	public static void openTPCreateMenu(Player player, String type) {
		
		//if opening anvil menu, set player XP to 0 first or they can take the item.
		if(resetXP.containsKey(player.getName())) resetXP.remove(player.getName());
		resetXP.put(player.getName(), player.getLevel());
		player.setLevel(0);
		
		Bukkit.getScheduler().runTask(plugin, () -> {
			
			new AnvilGUI.Builder()
					.onClose(p -> {
						//give original XP back
						if(resetXP.containsKey(player.getName())) {
							player.setLevel(resetXP.get(player.getName()));
							resetXP.remove(player.getName());
						}
					})
					
					.onComplete((p, text) -> { //called when the inventory output slot is clicked
						PlayerConfig playerConfig = PlayerConfig.getConfig(player);
						if(type.equalsIgnoreCase("private")) {
							//private:
							
							//errors in name:
							if(text.contains(" ")) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Name cannot contain spaces.");
								return AnvilGUI.Response.text("Contains spaces!");
							}
							if(text.length() <= 1) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Name must be more than 1 character.");
								return AnvilGUI.Response.text("Enter name");
							}
							if(playerConfig.getString("Travel." + text + ".LocationCode") != null) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Name is already used.");
								return AnvilGUI.Response.text("Name taken.");
							}
							
							ItemStack confirmItem = openConfirmMenu(player, Icon.Create_private_TP, "Creation Menu", false);
							ItemMeta meta = confirmItem.getItemMeta();
							List<String> lore = new ArrayList<String>();
							lore.add(ChatColor.WHITE + "Set a travel point at your location.");
							lore.add(" " + ChatColor.GRAY + "" + ChatColor.BOLD + ">" + ChatColor.GRAY + " Name: " + ChatColor.AQUA + ChatColor.BOLD + text);
							lore.add(" " + ChatColor.GRAY + "" + ChatColor.BOLD + ">" + ChatColor.GRAY + " Only you can access.");
							meta.setLore(lore);
							confirmItem.setItemMeta(meta);
							
							return AnvilGUI.Response.close();
						}
						if(type.equalsIgnoreCase("public")) {
							//public:
							
							//errors in name:
							if(text.contains(" ")) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Name cannot contain spaces.");
								return AnvilGUI.Response.text("Contains spaces!");
							}
							if(text.length() > 16) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Name must be less than 16 characters.");
								return AnvilGUI.Response.text("Too long!");
							}
							if(text.length() <= 1) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Name must be more than 1 character.");
								return AnvilGUI.Response.text("Enter name");
							}
							if(Travel.publicNameIsTaken(text) || Town.townNameIsTaken(text)) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "That name is already taken.");
								return AnvilGUI.Response.text("Name taken.");
							}
							try {
								ProtectedCuboidRegion region = new ProtectedCuboidRegion(text, BlockVector3.ONE, BlockVector3.ONE);
							} catch(IllegalArgumentException e) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "The point name cannot contain special characters.");
								return AnvilGUI.Response.text("Invalid characters.");
							}
							if(text.toLowerCase().contains("fuck")
									|| text.toLowerCase().contains("shit")
									|| text.toLowerCase().contains("cunt")
									|| text.toLowerCase().contains("pussy")
									|| text.toLowerCase().contains("nigga")
									|| text.toLowerCase().contains("nigger")) {
								
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Name cannot contain inappropriate language.");
								return AnvilGUI.Response.text("Inappropriate.");
							}
							RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
							if(regionManager.getApplicableRegions(BlockVector3.at(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())).size() > 0
									&& !Regions.playerCanBuild(player, player.getLocation(), false)) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You do not have rights to this land.");
								return AnvilGUI.Response.close();
							}
							
							ItemStack confirmItem = openConfirmMenu(player, Icon.Create_public_TP, "Creation Menu", false);
							ItemMeta meta = confirmItem.getItemMeta();
							List<String> lore = new ArrayList<String>();
							lore.add(ChatColor.WHITE + "Set a travel point at your location.");
							lore.add(" " + ChatColor.GRAY + "" + ChatColor.BOLD + ">" + ChatColor.GRAY + " Anyone can travel here.");
							lore.add(" " + ChatColor.GRAY + "" + ChatColor.BOLD + ">" + ChatColor.GRAY + " Name: " + ChatColor.AQUA + ChatColor.BOLD + text);
							
							meta.setLore(lore);
							confirmItem.setItemMeta(meta);
							
							return AnvilGUI.Response.close();
						}
						return AnvilGUI.Response.close();
					})
					.text("Enter name") //starting text
					.itemLeft(new ItemStack(Material.NAME_TAG))
					.onLeftInputClick(p -> Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Click the output slot to continue."))
					
					.title(ChatColor.BOLD + type + ChatColor.DARK_GRAY + ": Set Name:") //set the title of the GUI
					.plugin(plugin)
					.open(player);
			
		});
	}
	
	//returns the confirmation icon
	public static ItemStack openConfirmMenu(Player player, Icon confirmIcon, String previousMenu, boolean openingAsync) {
		
		Inventory inventory = Bukkit.createInventory(null, 9, ChatColor.BOLD + "Travel" + ChatColor.DARK_GRAY + ": Confirm?");
		
		ItemStack confirmItem = getIcon(player, confirmIcon);
		inventory.setItem(4, confirmItem);
		
		ItemStack backButton = getIcon(player, Icon.BACK);
		ItemMeta meta = backButton.getItemMeta();
		List<String> lore = meta.getLore() == null ? new ArrayList<String>() : meta.getLore();
		lore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + previousMenu + ChatColor.GRAY + ".");
		meta.setLore(lore);
		backButton.setItemMeta(meta);
		inventory.setItem(0, backButton);
		
		if(openingAsync) {
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
		} else {
			player.openInventory(inventory);
		}
		return inventory.getItem(4);
	}
	
	private static void openSetIconMenu(Player player, String pointName, boolean isPublic, int page) {

		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			Inventory inventory = Bukkit.createInventory(null, 54, ChatColor.BOLD + "Set Icon" + ChatColor.DARK_GRAY + ": " + pointName);
			
			int leftArrow = 45;
			int rightArrow = 53;
			int itemsOnPage = 44; //Ignore lower slot
			Material[] materials = Material.values();
			for(int count = 0; count <= itemsOnPage; count++) {
				int materialIndex = count + (itemsOnPage * page);
				if(materialIndex >= materials.length) break;
				
				//populate each slot
				ItemStack item = new ItemStack(materials[materialIndex]);
				if(page == 0 && count == 0) item = new ItemStack(Material.POWERED_RAIL);
				inventory.setItem(count, item);
			}
			int pages = (int) Math.ceil(materials.length / 52.0);
			
			ItemStack arrow = new ItemStack(Material.ARROW);
			ItemMeta meta = arrow.getItemMeta();
			meta.setDisplayName(ChatColor.AQUA + "Previous Page");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.WHITE + "Previous icon page.");
			lore.add(ChatColor.GRAY + " > Showing page " + page + " of " + pages);
			meta.setLore(lore);
			meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			arrow.setItemMeta(meta);
			
			inventory.setItem(leftArrow, arrow);
			
			meta.setDisplayName(ChatColor.AQUA + "Next Page");
			List<String> lore2 = new ArrayList<String>();
			lore2.add(ChatColor.WHITE + "Next icon page.");
			lore2.add(ChatColor.GRAY + " > Showing page " + page + " of " + pages);
			meta.setLore(lore2);
			arrow.setItemMeta(meta);
			
			inventory.setItem(rightArrow, arrow);
			
			ItemStack backButton = getIcon(player, Icon.BACK);
			ItemMeta backMeta = backButton.getItemMeta();
			List<String> backLore = backMeta.getLore() == null ? new ArrayList<String>() : backMeta.getLore();
			backLore.add(ChatColor.GRAY + " > Back to " + ChatColor.BOLD + "manage " + (isPublic ? "public" : "private") + ": " + pointName + ChatColor.GRAY + ".");
			backMeta.setLore(backLore);
			backButton.setItemMeta(backMeta);
			inventory.setItem(49, backButton);
			
			Bukkit.getScheduler().runTask(plugin, () -> player.openInventory(inventory));
		});
	}
	
	private static int getPageNumberFromArrow(ItemStack arrow) {
		String pageString = ChatColor.stripColor(arrow.getItemMeta().getLore().get(1));
		int start = 16;
		int end = 17;
		int currentPage = Integer.parseInt(pageString.substring(start, end));
		while(Chat.isInteger(pageString.substring(start, end))) {
			currentPage = Integer.parseInt(pageString.substring(start, end));
			end++;
		}
		return currentPage;
	}
	
	private MenuName getMenuName(String menuTitle) {
		if(menuTitle.contains("Item Finder")) {
			if(menuTitle.contains("Results")) return MenuName.Item_Finder_Results;
			else return MenuName.Item_Finder;
		}
		return MenuName.valueOf(ChatColor.stripColor(menuTitle).replace("Travel: ", "").replace(" ", "_"));
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void travelGUIclick(InventoryClickEvent event) {
		try {
			if(event.getClickedInventory() == null) {
				//clicked outside of inventory
				return;
			}
			
			if(event.getWhoClicked() instanceof Player) {
				Player player = (Player) event.getWhoClicked();
				
				if(event.getView().getTitle().contains("Set Icon")) {
					ItemStack cancelButton = event.getClickedInventory().getItem(49);
					boolean isPublic = cancelButton.getItemMeta().getLore().get(0).contains("public");
					
					String title = ChatColor.stripColor(event.getView().getTitle());
					String pointName = title.substring(10,title.length());
					if(event.getCurrentItem() != null && event.getCurrentItem().getItemMeta() != null && event.getCurrentItem().getItemMeta().hasDisplayName()) {
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							String name = event.getCurrentItem().getItemMeta().getDisplayName();
							if(name.toLowerCase().contains("next page") || name.toLowerCase().contains("previous page")) {
								
								int pages = (int) Math.ceil(Material.values().length / 52.0);
								int currentPage = getPageNumberFromArrow(event.getClickedInventory().getItem(45));

								if(name.toLowerCase().contains("next page")) {
									currentPage++;
								}
								if(name.toLowerCase().contains("previous page")) {
									currentPage--;
								}
								if(currentPage >= 0 && currentPage <= pages) {
									openSetIconMenu(player, pointName, isPublic, currentPage);
								} else
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages!");
								
							}
							
							if(name.toLowerCase().contains("cancel")) {
								if(isPublic) {
									openManagePublicTP(player, pointName);
								} else {
									openManagePrivateTP(player, pointName);
								}
							}
						});
						
					} else if(event.getCurrentItem() != null && event.getCurrentItem().getType() != Material.AIR && event.getSlot() < 45) {
						//set icon as clicked item
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							Material material = event.getCurrentItem().getType();
							
							if(isPublic) {
								PublicTP tpConfig = PublicTP.getConfig(pointName);
								tpConfig.set("Icon", material.toString());
								tpConfig.save();

								openManagePublicTP(player, pointName);
							} else {
								PlayerConfig playerConfig = PlayerConfig.getConfig(player);
								playerConfig.set("Travel." + pointName + ".Icon", material.toString());
								playerConfig.save();
								
								openManagePrivateTP(player, pointName);
							}
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Icon for " + pointName + " has been updated!");
						});
					}
					
					event.setCancelled(true);
					return;
				}
				
				if(event.getView().getTitle().contains("Travel") || event.getView().getTitle().contains("Manage") || event.getView().getTitle().contains("Set Icon") || event.getView().getTitle().contains("Item Finder")) {
					if(event.getCurrentItem() == null || event.getCurrentItem().getItemMeta() == null || event.getCurrentItem().getItemMeta().getLore() == null || event.getCurrentItem().getItemMeta().getLore().size() == 0 || !event.getCurrentItem().getItemMeta().hasDisplayName()) {
						//didn't click a custom item
						event.setCancelled(true);
						return;
					}

					ItemMeta meta = event.getCurrentItem().getItemMeta();
					String name = meta.getDisplayName();
					
					//per item here:
					
					if(event.getView().getTitle().contains("Item Finder")) {
						boolean isSearchMenu = event.getView().getTitle().contains("\"");
						if(event.getView().getTitle().contains("Select Item") || isSearchMenu) {
							if(name.toLowerCase().contains("search")) {
								openItemFinderSearchMenu(player);
							}
							if(itemFinderSlots.contains(event.getSlot())) {
								//is an item
								
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									//track the find usage
									String itemFinderName = getItemFinderNameFromGUI(name);
									int uses = DeadMC.MarketFile.data().getString("ItemFinder." + itemFinderName) == null ? 0 : DeadMC.MarketFile.data().getInt("ItemFinder." + itemFinderName);
									DeadMC.MarketFile.data().set("ItemFinder." + itemFinderName, uses + 1);
									DeadMC.MarketFile.save();
									
									openItemFinderResultsMenu(player, itemFinderName, null, isSearchMenu ? "Item Finder: " + ChatColor.stripColor(event.getView().getTitle()).substring(14, ChatColor.stripColor(event.getView().getTitle()).length()-1) : "Item Finder");
								});
							}
						}
						if(event.getView().getTitle().contains("Results")) {
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
								if(itemFinderResultsSlots.contains(event.getSlot())) {
									//clicked point
									
									//everyone can travel directly to market shops
									//only emperors can travel directly to public shops
									
									PlayerConfig playerConfig = PlayerConfig.getConfig(player);
									String itemGUIname = event.getClickedInventory().getItem(13).getItemMeta().getDisplayName();
									String itemFinderName = getItemFinderNameFromGUI(itemGUIname);
									
									//travel directly to shop location:
									Shop shop = getShopClickedInResultsMenu(player, event.getClickedInventory(), itemFinderName, event.getSlot());
									if(shop.type == Shop.ShopType.MARKET) {
										Travel.travelToShop(player, shop);
										Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
									} else {
										//is a public point
										Travel.attemptPublicTravel(player, shop.travelPoint, "results: " + itemFinderName, playerConfig.getInt("DonateRank") >= Donator.Rank.Emperor.ordinal() ? shop : null);
									}
								}
							});
						}
					}
					
					if(event.getView().getTitle().contains("Towns")) {
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							if(event.getCurrentItem().getType().toString().contains("BANNER")) {
								String pointName = ChatColor.stripColor(name);
								
								if(event.isLeftClick()) {
									//travel
									Travel.attemptTownTravel(player, pointName, "towns");
								}
								if(event.getClick() == ClickType.MIDDLE) {
									//quick slot
									openQuickSlotMenu(player, pointName, "Town", "towns");
								}
							}
						});
					}
					
					if(name.toLowerCase().contains("cancel")) {
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							String description = meta.getLore().get(0);
							
							if(description.toLowerCase().contains("quick menu")) {
								openQuickMenu(player);
							}
							if(description.toLowerCase().contains("creation menu")) {
								openCreateMenu(player);
							}
							if(description.contains("manage public:")) {
								String descriptionStripped = ChatColor.stripColor(description);
								String pointName = descriptionStripped.substring(26, descriptionStripped.length() - 1);
								openManagePublicTP(player, pointName);
							}
							if(description.toLowerCase().contains("manage points") || description.toLowerCase().contains("manage menu")) {
								openManageMenu(player);
							}
							if(description.toLowerCase().contains("public points")) {
								openPublicMenu(player);
							}
							if(description.toLowerCase().contains("manage tp")) {
								TownGUI.openTPMenu(player, "Quick Menu", true);
							}
							if(description.toLowerCase().contains("towns")) {
								openTownsMenu(player);
							}
							if(description.toLowerCase().contains("item finder")) {
								openItemFinderMenu(player);
							}
							if(description.toLowerCase().contains("item finder: ")) { //search
								String descriptionStripped = ChatColor.stripColor(description);
								String itemFinderName = descriptionStripped.substring(24, descriptionStripped.length() - 1);
								openItemFinderMenu(player, itemFinderName, true);
							}
							if(description.toLowerCase().contains("results:")) {
								String descriptionStripped = ChatColor.stripColor(description);
								String itemFinderName = descriptionStripped.substring(20, descriptionStripped.length() - 1);
								openItemFinderResultsMenu(player, itemFinderName);
							}
						});
						event.setCancelled(true);
						return;
					}
					
					if(event.getView().getTitle().contains("Confirm?")) {
						
						if(name.toLowerCase().contains(Icon.Confirm_town_travel.toString().toLowerCase().replace("_", " "))) {
							
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
								//travel to the town
								
								List<String> lore = event.getCurrentItem().getItemMeta().getLore();
								String description = ChatColor.stripColor(lore.get(0));
								String townDisplayName = description.substring(10, description.length() - 1);
								String originalTownName = Town.getOriginalTownName(townDisplayName);
								
								PlayerConfig playerConfig = PlayerConfig.getConfig(player);
								TownConfig townConfig = TownConfig.getConfig(originalTownName);
								int cost = townConfig.getInt("Travel.Fee");
								
								if(playerConfig.getInt("Coins") >= cost) {
									Economy.takeCoins(player.getUniqueId(), cost);
									Town.addFunds(originalTownName, player, cost);
									
									//travel:
									Location location = LocationCode.Decode(townConfig.getString("Travel.LocationCode"));
									Travel.travel(player, townDisplayName, location);
								} else {
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "You don't have enough coins! The travel fee is " + ChatColor.GOLD + Economy.convertCoins(cost) + ChatColor.WHITE + ".");
								}
								
								Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
								
							});
						}
						
						if(name.toLowerCase().contains(Icon.Confirm_public_travel.toString().toLowerCase().replace("_", " "))) {
							
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
								List<String> lore = event.getCurrentItem().getItemMeta().getLore();
								String description = ChatColor.stripColor(lore.get(0));
								String pointName = description.substring(10, description.length() - 1);
								
								PlayerConfig playerConfig = PlayerConfig.getConfig(player);
								PublicTP tpConfig = PublicTP.getConfig(pointName);
								int cost = tpConfig.getInt("Cost");
								UUID ownerUUID = UUID.fromString(tpConfig.getString("Owner"));
								
								if(playerConfig.getInt("Coins") >= cost) {
									Economy.takeCoins(player.getUniqueId(), cost);
									Economy.giveCoins(ownerUUID, cost);
									
									//travel
									if(lore.size() > 2) {
										//travelling to a shop directly
										String shopDescriptionStripped = ChatColor.stripColor(lore.get(2));
										Location differentLocation = LocationCode.Decode(shopDescriptionStripped.substring(9));
										Travel.travelToShop(player, new Shop(Shop.ShopType.PLAYER, pointName, differentLocation));
									} else {
										Location location = LocationCode.Decode(tpConfig.getString("LocationCode"));
										Travel.travel(player, pointName, location);
									}
									
									//track
									Travel.trackPublicTravel(player.getUniqueId(), pointName);
								} else {
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "You don't have enough coins! The travel fee is " + ChatColor.GOLD + Economy.convertCoins(cost) + ChatColor.WHITE + ".");
								}
								
								Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
							});
						}
						
						if(name.toLowerCase().contains("delete")) {
							if(name.toLowerCase().contains("public")) {
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									String pointName = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getLore().get(0)).substring(9);
									
									PlayerConfig playerConfig = PlayerConfig.getConfig(player);
									List<String> publicTravelPoints = new ArrayList<String>();
									if(playerConfig.getStringList("Travel.Public") != null)
										publicTravelPoints = playerConfig.getStringList("Travel.Public");
									
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "'" + pointName + "' has been deleted.");
									
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "'" + pointName + "' has been deleted.");
									
									//update global data
									List<String> publicPoints = DeadMC.TravelFile.data().getStringList("Public");
									
									//remove from player file
									publicTravelPoints.remove(pointName);
									playerConfig.set("Travel.Public", publicTravelPoints);
									playerConfig.save();
									
									//remove from global list:
									publicPoints.remove(pointName);
									DeadMC.TravelFile.data().set("Public", publicPoints);
									DeadMC.TravelFile.save();
									
									//remove public point file:
									File publicFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "travel" + File.separator + pointName + ".yml");
									publicFile.delete();
									
									openManageMenu(player);
								});
							}
							if(name.toLowerCase().contains("private")) {
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									String pointName = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getLore().get(0)).substring(9);
									
									PlayerConfig playerConfig = PlayerConfig.getConfig(player);
									List<String> privateTravelPoints = new ArrayList<String>();
									if(playerConfig.getStringList("Travel.Private") != null)
										privateTravelPoints = playerConfig.getStringList("Travel.Private");
									
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "'" + pointName + "' has been deleted.");
									
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "'" + pointName + "' has been deleted.");
									
									privateTravelPoints.remove(pointName);
									playerConfig.set("Travel.Private", privateTravelPoints);
									
									playerConfig.set("Travel." + pointName, null);
									playerConfig.save();
									
									openManageMenu(player);
								});
							}
						}
					}
					
					if(event.getView().getTitle().contains("Online Players")) {
						if(meta instanceof SkullMeta skullMeta) {
							//clicked player
							OfflinePlayer playerToTeleportTo = skullMeta.getOwningPlayer();
							Travel.sendTravelRequest(player, playerToTeleportTo.getName());
							player.closeInventory();
						}
					}
					
					if(event.getView().getTitle().contains("Quick Menu")) {
						
						if(name.toLowerCase().contains("longdale")) {
							Travel.travel(player, "Longdale");
							player.closeInventory();
						}
						
						if(name.toLowerCase().contains("wilderness")) {
							
							Travel.travel(player, "the wilderness");
							player.closeInventory();
						}
						
						if(name.toLowerCase().contains("market")) {
							Travel.travel(player, "the Market");
							player.closeInventory();
						}
						
						if(name.toLowerCase().contains("players")) {
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> openPlayerMenu(player));
						}
						
						if(name.toLowerCase().contains("towns")) {
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> openTownsMenu(player));
						}
						
						if(name.toLowerCase().contains("item finder")) {
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> openItemFinderMenu(player));
						}
						
						if(name.toLowerCase().contains("create")) {
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> openCreateMenu(player));
						}
						
						if(name.toLowerCase().contains("manage")) {
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> openManageMenu(player));
						}
						
						if(name.toLowerCase().contains("quick slot")) {
							Material material = event.getCurrentItem().getType();
							if(material.toString().toLowerCase().contains("stained_glass_pane")) {
								event.setCancelled(true);
								return;
							}
							
							PlayerConfig playerConfig = PlayerConfig.getConfig(player);
							String lore0 = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getLore().get(0));
							String lore1 = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getLore().get(1));
							String pointName = lore0.substring(10, lore0.length() - 1); //remove the space at the end
							
							if(lore1.toLowerCase().contains("private")) {
								//clicked private
								
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									List<String> privateTravelPoints = new ArrayList<String>();
									if(playerConfig.getStringList("Travel.Private") != null)
										privateTravelPoints = playerConfig.getStringList("Travel.Private");
									
									if(privateTravelPoints.contains(pointName)) { //check it still exists
										Location location = LocationCode.Decode(playerConfig.getString("Travel." + pointName + ".LocationCode"));
										Travel.travel(player, pointName, location);
										
										//track the travel
										playerConfig.set("Travel." + pointName + ".Uses", playerConfig.getString("Travel." + pointName + ".Uses") == null ? 1 : playerConfig.getInt("Travel." + pointName + ".Uses") + 1);
										playerConfig.save();
										
										Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
									} else {
										//no longer exists:
										
										//remove from config
										int slot = Integer.parseInt(ChatColor.stripColor(name).substring(11));
										playerConfig.set("Travel.QuickSlot" + slot, null);
										playerConfig.save();
										
										//reload menu
										openQuickMenu(player);
										
										Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "This point no longer exists.");
									}
								});
							}
							
							if(lore1.toLowerCase().contains("public") || lore1.toLowerCase().contains("shop")) {
								//clicked public

								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									boolean travelAttemptSucceeded = Travel.attemptPublicTravel(player, pointName, "quick menu"); //check it still exists
									if(!travelAttemptSucceeded) {
										//no longer exists:
										
										//remove from config
										int slot = Integer.parseInt(ChatColor.stripColor(name).substring(11));
										playerConfig.set("Travel.QuickSlot" + slot, null);
										playerConfig.save();
										
										//reload menu
										openQuickMenu(player);
										
										Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "This point no longer exists.");
									}
								});
							}
							
							if(lore1.toLowerCase().contains("town")) {
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									//clicked town
									if(Town.townNameIsTaken(pointName)
											&& Town.getTownDisplayName(pointName).equalsIgnoreCase(pointName)) {
										//pointName is the town's display name
										Travel.attemptTownTravel(player, pointName, "quick menu");
									} else {
										//no longer exists:
										
										//remove from config
										int slot = Integer.parseInt(ChatColor.stripColor(name).substring(11));
										playerConfig.set("Travel.QuickSlot" + slot, null);
										playerConfig.save();
										
										//reload menu
										openQuickMenu(player);
										
										Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "This point no longer exists.");
									}
								});
							}
							
						}
						
						if(name.toLowerCase().contains("your town")) {
							
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
								PlayerConfig playerConfig = PlayerConfig.getConfig(player);
								
								String originalTownName = playerConfig.getString("Town");
								String townDisplayName = Town.getTownDisplayName(originalTownName);
								TownConfig townConfig = TownConfig.getConfig(originalTownName);
								
								Boolean mayor = playerConfig.getInt("TownRank") >= Town.Rank.Co_Mayor.ordinal();
								if(event.isLeftClick() || !mayor) {
									
									Travel.attemptTownTravel(player, townDisplayName, "quick menu");
									
								} else if(event.isRightClick()) {
									TownGUI.openTPMenu(player, "Quick Menu", true);
								}
								
							});
						}
						
						//public : runs public command
						if(name.toLowerCase().contains("public points")) {
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
								openPublicMenu(player);
							});
						}
						
					}
					
					if(name.toLowerCase().contains(Icon.Confirm_set_town_point.toString().replace("_", " ").toLowerCase())) {
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							PlayerConfig playerConfig = PlayerConfig.getConfig(player);
							
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							int cost = 50;
							
							boolean canUseFunds = townConfig.getInt("Funds") >= cost;
							boolean canUseCoins = playerConfig.getInt("Coins") >= cost;
							if(canUseFunds || canUseCoins) {
								
								if(canUseFunds) {
									//use funds first if available
									Town.sendMessage(originalTownName, new String(ChatColor.GOLD + Economy.convertCoins(cost) + ChatColor.RED + " deducted from " + townDisplayName + "'s funds."), true, true);
									townConfig.set("Funds", townConfig.getInt("Funds") - cost);
								} else {
									Economy.takeCoins(player.getUniqueId(), cost);
									playerConfig.set("TownFundsAdded", playerConfig.getInt("TownFundsAdded") + cost);
									playerConfig.save();
								}
								
								Location travelLocation = player.getLocation();
								
								townConfig.set("Travel.LocationCode", LocationCode.Encode(travelLocation));
								townConfig.set("Travel.Public", false);
								townConfig.save();
								
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Travel point updated.");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + townDisplayName + "'s travel point has been updated.");
								
								//TASK
								if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.CREATE_TOWN_TRAVEL_POINT)) {
									TaskManager.stepTask(player.getUniqueId());
									Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
								} else {
									TownGUI.openTPMenu(player, "Quick Menu", true);
								}
								
							} else {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Town] " + ChatColor.RED + "Not enough funds!");
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.RED + townDisplayName + " doesn't have enough funds! " + ChatColor.GOLD + Economy.convertCoins(townConfig.getInt("Funds")) + ChatColor.WHITE + " / " + ChatColor.GOLD + Economy.convertCoins(cost));
								player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/town funds add <coins>");
								player.sendMessage("");
								Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
							}
						});
					}
					
					//creating TP:
					if(event.getView().getTitle().contains("Creation Menu")
							&& (name.toLowerCase().contains("private") || name.toLowerCase().contains("public"))) {
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							
							if(player.getWorld().getName().equalsIgnoreCase("revamp") && !player.isOp()) {
								player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Cannot create travel point here.");
								return;
							}
							
							PlayerConfig playerConfig = PlayerConfig.getConfig(player);
							
							if(name.toLowerCase().contains("private")) {
								
								List<String> privateTravelPoints = new ArrayList<String>();
								if(playerConfig.getStringList("Travel.Private") != null)
									privateTravelPoints = playerConfig.getStringList("Travel.Private");
								int limit = DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.Travel.Private");
								
								if(privateTravelPoints.size() >= limit) {
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Private limit reached.");
									player.sendMessage("");
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You've reached the " + Donator.getPrefix(player.getUniqueId()) + ChatColor.AQUA + " private travel point" + ChatColor.WHITE + " limit.");
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to upgrade.");
									player.sendMessage("");
									return;
								}
								
								RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
								if(regionManager.getApplicableRegions(BlockVector3.at(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())).size() > 0
										&& !Regions.playerCanBuild(player, player.getLocation(), true)) {
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You do not have rights to this land.");
									if(!Tools.playerIsJava(player)) {
										Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
									}
									return;
								}
								
								openTPCreateMenu(player, "private");
							}
							
							if(name.toLowerCase().contains("public")) {
								
								List<String> publicTravelPoints = new ArrayList<String>();
								if(playerConfig.getStringList("Travel.Public") != null)
									publicTravelPoints = playerConfig.getStringList("Travel.Public");
								int limit = playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") == Admin.StaffRank.ADMINISTRATOR.ordinal() ? 999999 : DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.Travel.Public");
								
								if(publicTravelPoints.size() >= limit) {
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Public limit reached.");
									player.sendMessage("");
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "You've reached the " + Donator.getPrefix(player.getUniqueId()) + ChatColor.AQUA + " public travel point" + ChatColor.WHITE + " limit.");
									player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to upgrade.");
									player.sendMessage("");
									return;
								}
								
								RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
								if(regionManager.getApplicableRegions(BlockVector3.at(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())).size() > 0
										&& !Regions.playerCanBuild(player, player.getLocation(), true)) {
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "You do not have rights to this land.");
									if(!Tools.playerIsJava(player)) {
										Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
									}
									return;
								}
								
								openTPCreateMenu(player, "public");
							}
							
						});
					}
					
					if(name.toLowerCase().contains(Icon.Create_private_TP.toString().replace("_", " ").toLowerCase())) {
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							
							if(player.getWorld().getName().equalsIgnoreCase("revamp") && !player.isOp()) {
								player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Cannot create travel point here.");
								return;
							}
							
							PlayerConfig playerConfig = PlayerConfig.getConfig(player);
							
							String text = ChatColor.stripColor(meta.getLore().get(1)).substring(9); //the travel point name
							
							//success
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "'" + text + "' has been created!");
							
							//update players data
							List<String> privateTravelPoints = new ArrayList<String>();
							if(playerConfig.getStringList("Travel.Private") != null)
								privateTravelPoints = playerConfig.getStringList("Travel.Private");
							
							privateTravelPoints.add(text);
							playerConfig.set("Travel.Private", privateTravelPoints);
							playerConfig.set("Travel." + text + ".LocationCode", LocationCode.Encode(player.getLocation()));
							playerConfig.save();

							player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Private TP '" + ChatColor.AQUA + ChatColor.BOLD + text + ChatColor.GREEN + "' has been created!");

							openManagePrivateTP(player, text);
							
							//check if free quick slot
							int freeQuickSlot = getFreeQuickSlot(playerConfig);
							boolean hasFreeQuickSlot = freeQuickSlot != -1;
							if(hasFreeQuickSlot) {
								//add to the quick slot
								playerConfig.set("Travel.QuickSlot" + freeQuickSlot + ".Name", text);
								playerConfig.set("Travel.QuickSlot" + freeQuickSlot + ".Type", "private");
								playerConfig.save();
							}
							
						});
					}
					
					if(name.toLowerCase().contains(Icon.Create_public_TP.toString().replace("_", " ").toLowerCase())) {
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							
							if(player.getWorld().getName().equalsIgnoreCase("revamp") && !player.isOp()) {
								player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Cannot create travel point here.");
								return;
							}
							
							PlayerConfig playerConfig = PlayerConfig.getConfig(player);
							
							String text = ChatColor.stripColor(meta.getLore().get(2)).substring(9); //the travel point name
							
							//success
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "'" + text + "' has been created!");
							
							List<String> publicTravelPoints = new ArrayList<String>();
							if(playerConfig.getStringList("Travel.Public") != null)
								publicTravelPoints = playerConfig.getStringList("Travel.Public");
							
							//update players data
							publicTravelPoints.add(text);
							playerConfig.set("Travel.Public", publicTravelPoints);
							playerConfig.save();
							
							//update global data
							List<String> publicPoints = DeadMC.TravelFile.data().getStringList("Public");
							publicPoints.add(text);
							DeadMC.TravelFile.data().set("Public", publicPoints);
							DeadMC.TravelFile.save();
							
							//create public travel file
							PublicTP tpConfig = PublicTP.getConfig(text);
							tpConfig.set("LocationCode", LocationCode.Encode(player.getLocation()));
							tpConfig.set("Owner", player.getUniqueId().toString());
							
							//public rankings:
							ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
							tpConfig.set("Created", DateCode.Encode(adelaideTime, true, false, false, true, true, true));
							tpConfig.set("UniqueVisits", 0); //the total daily unique visits
							tpConfig.set("TodaysDay", 1); //reset list when new day
							tpConfig.set("Visits", new ArrayList<String>());
							tpConfig.save();
							
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Public TP '" + ChatColor.AQUA + ChatColor.BOLD + text + ChatColor.GREEN + "' has been created!");
							player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Use the " + ChatColor.GOLD + "/tp" + ChatColor.WHITE + " menu to manage your points.");
							player.sendMessage("");
							
							openManagePublicTP(player, text);
							
							//check if free quick slot
							int freeQuickSlot = getFreeQuickSlot(playerConfig);
							boolean hasFreeQuickSlot = freeQuickSlot != -1;
							if(hasFreeQuickSlot) {
								//add to the quick slot
								playerConfig.set("Travel.QuickSlot" + freeQuickSlot + ".Name", text);
								playerConfig.set("Travel.QuickSlot" + freeQuickSlot + ".Type", "public");
								playerConfig.save();
							}
							
						});
					}
					
					if(event.getView().getTitle().contains("Quick Slots")) {
						if(event.getCurrentItem().getItemMeta().hasDisplayName() && event.getCurrentItem().getItemMeta().getDisplayName().toLowerCase().contains("Quick Slot")) {
							event.setCancelled(true);
							return;
						}
						
						Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
							int slot = Integer.parseInt(ChatColor.stripColor(name).substring(11));
							List<String> lore = event.getCurrentItem().getItemMeta().getLore();
							if(lore.size() == 2) {
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "This is a donor feature.");
							} else {
								String pointName = ChatColor.stripColor(lore.get(0)).replace("Add ", "").replace(" here?", "");
								String type = lore.size() == 3 ? ChatColor.stripColor(lore.get(1)).replace(" > ", "").replace(" point.", "").toLowerCase() : ChatColor.stripColor(lore.get(2)).replace(" > ", "").replace(" point.", "").toLowerCase();
								
								PlayerConfig playerConfig = PlayerConfig.getConfig(player);
								playerConfig.set("Travel.QuickSlot" + slot + ".Name", pointName);
								playerConfig.set("Travel.QuickSlot" + slot + ".Type", type);
								playerConfig.save();
								
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Updated " + ChatColor.YELLOW + ChatColor.BOLD + "Quick Slot " + slot + ChatColor.GREEN + "!");
								openQuickMenu(player);
							}
						});
					}
					
					//sorting:
					if(event.getView().getTitle().contains("Manage Points")
							|| event.getView().getTitle().contains("Public Points")
							|| event.getView().getTitle().contains("Towns")
							|| event.getView().getTitle().contains("Online Players")
							|| event.getView().getTitle().contains("Item Finder")) {
						MenuName menuName = getMenuName(event.getView().getTitle());
						if(event.getCurrentItem().getType().toString().toLowerCase().contains("disc")) {
							
							if(event.getSlot() == 7 || event.getSlot() == 8) {
								//clicked toggle options
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Toggled!");
									
									//set the new toggle type:
									PlayerConfig playerConfig = PlayerConfig.getConfig(player);
									ToggleType type = ToggleType.valueOf(ChatColor.stripColor(name).replace(" ", "_"));
									playerConfig.set("Travel.Toggle." + type.toString(), playerConfig.getString("Travel.Toggle." + type.toString()) != null ? null : true); //remove
									playerConfig.save();
									
									//populate points
									if(menuName == MenuName.Public_Points) {
										openPublicMenu(player, event.getClickedInventory());
									}
									if(menuName == MenuName.Towns) {
										openTownsMenu(player, event.getClickedInventory());
									}
									if(menuName == MenuName.Item_Finder_Results) {
										String itemGUIname = event.getClickedInventory().getItem(13).getItemMeta().getDisplayName();
										String itemFinderName = getItemFinderNameFromGUI(itemGUIname);
										boolean isSearchMenu = event.getView().getTitle().contains("\"");
										openItemFinderResultsMenu(player, itemFinderName, event.getClickedInventory(), isSearchMenu ? "Item Finder: " + ChatColor.stripColor(event.getView().getTitle()).substring(14, ChatColor.stripColor(event.getView().getTitle()).length()-1) : "Item Finder");
									}
								});
								
							} else if(event.getSlot() == 3 || event.getSlot() == 4 || event.getSlot() == 5) {
								//clicked sorting option
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									PlayerConfig playerConfig = PlayerConfig.getConfig(player);
									
									SortType order = SortType.valueOf(ChatColor.stripColor(meta.getDisplayName().replace(" ", "_")));
									
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Sorted!");
									
									//set the new sort type:
									playerConfig.set("Travel.DescendingOrder." + menuName.toString(), event.isRightClick() ? true : null);
									playerConfig.set("Travel.Selected." + menuName.toString(), order.ordinal());
									playerConfig.save();
									
									//populate points:
									if(menuName == MenuName.Manage_Points) {
										openManageMenu(player, event.getClickedInventory());
									}
									if(menuName == MenuName.Public_Points) {
										openPublicMenu(player, event.getClickedInventory());
									}
									if(menuName == MenuName.Online_Players) {
										openPlayerMenu(player, event.getClickedInventory());
									}
									if(menuName == MenuName.Towns) {
										openTownsMenu(player, event.getClickedInventory());
									}
									if(menuName == MenuName.Item_Finder) {
										if(event.getView().getTitle().contains("\"")) {
											String titleStripped = ChatColor.stripColor(event.getView().getTitle());
											String search = titleStripped.substring(14, titleStripped.length() - 1);
											openItemFinderMenu(player, search, event.getClickedInventory(), true);
											
										} else {
											openItemFinderMenu(player, event.getClickedInventory());
										}
									}
									if(menuName == MenuName.Item_Finder_Results) {
										String itemFinderName = getItemFinderNameFromGUI(event.getClickedInventory().getItem(13).getItemMeta().getDisplayName());
										boolean isSearchMenu = event.getView().getTitle().contains("\"");
										openItemFinderResultsMenu(player, itemFinderName, event.getClickedInventory(), isSearchMenu ? "Item Finder: " + ChatColor.stripColor(event.getView().getTitle()).substring(14, ChatColor.stripColor(event.getView().getTitle()).length()-1) : "Item Finder");
									}
								});
							}
						}
						
						if(event.getCurrentItem().getType() == Material.ARROW && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasLore()) {
							//change page:
							
							Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
								PlayerConfig playerConfig = PlayerConfig.getConfig(player);
								
								String type = event.getCurrentItem().getItemMeta().getLore().get(0);
								Chat.debug(type);
								
								//get current page number:
								int arrow = 0;
								if(type.contains("private") || type.contains("shop") || type.contains("results") || type.contains("town"))
									arrow = 27;
								if(type.contains("public") || type.contains("miscellaneous") || type.contains("player") || type.contains("item finder"))
									arrow = 45;
								
								if(arrow == 0 || (!type.contains("Next") && !type.contains("Previous"))) return; //is not a page arrow
								
								int currentPage = getPageNumberFromArrow(event.getInventory().getItem(arrow));
								
								if(type.contains("private")) {
									
									List<String> privateTravelPoints = new ArrayList<String>();
									if(playerConfig.getStringList("Travel.Private") != null)
										privateTravelPoints = playerConfig.getStringList("Travel.Private");
									int numberOfPages_Private = (int) (Math.ceil(privateTravelPoints.size() / 14.0));
									
									if(name.contains("Next")) {
										if((currentPage + 1) <= numberOfPages_Private)
											currentPage++;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									if(name.contains("Previous")) {
										if((currentPage - 1) >= 1)
											currentPage--;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									clearPoints(event.getClickedInventory(), player, PageType.PRIVATE);
									populatePoints(player, event.getClickedInventory(), menuName, PageType.PRIVATE, privateTravelPoints, currentPage);
									
									//update the inventory:
									event.getClickedInventory().setItem(27, getPageItem(PageType.PRIVATE, "back", currentPage, numberOfPages_Private));
									event.getClickedInventory().setItem(35, getPageItem(PageType.PRIVATE, "forward", currentPage, numberOfPages_Private));
									
									ItemStack middleItem = event.getClickedInventory().getItem(13);
									ItemMeta middleItemMeta = middleItem.getItemMeta();
									List<String> lore = new ArrayList<String>();
									lore.add(ChatColor.WHITE + "Manage your " + ChatColor.BOLD + "private" + ChatColor.WHITE + " points below.");
									lore.add(ChatColor.GRAY + " > You have " + privateTravelPoints.size() + " private points.");
									lore.add(ChatColor.GRAY + " > Showing page " + currentPage + " of " + numberOfPages_Private + ".");
									middleItemMeta.setLore(lore);
									middleItem.setItemMeta(middleItemMeta);
								}
								
								if(type.contains("public")) {
									
									List<String> publicTravelPoints = new ArrayList<String>();
									if(playerConfig.getStringList("Travel.Public") != null)
										publicTravelPoints = playerConfig.getStringList("Travel.Public");
									int numberOfPages_Public = (int) (Math.ceil(publicTravelPoints.size() / 7.0));
									
									if(name.contains("Next")) {
										if((currentPage + 1) <= numberOfPages_Public)
											currentPage++;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									if(name.contains("Previous")) {
										if((currentPage - 1) >= 1)
											currentPage--;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									clearPoints(event.getClickedInventory(), player, PageType.PUBLIC);
									populatePoints(player, event.getClickedInventory(), menuName, PageType.PUBLIC, publicTravelPoints, currentPage);
									
									//update the inventory:
									event.getClickedInventory().setItem(45, getPageItem(PageType.PUBLIC, "back", currentPage, numberOfPages_Public));
									event.getClickedInventory().setItem(53, getPageItem(PageType.PUBLIC, "forward", currentPage, numberOfPages_Public));
									
									ItemStack middleItem = event.getClickedInventory().getItem(40);
									ItemMeta middleItemMeta = middleItem.getItemMeta();
									List<String> lore = new ArrayList<String>();
									lore.add(ChatColor.WHITE + "Manage your " + ChatColor.BOLD + "public" + ChatColor.WHITE + " points below.");
									lore.add(ChatColor.GRAY + " > You have " + publicTravelPoints.size() + " private points.");
									lore.add(ChatColor.GRAY + " > Showing page " + currentPage + " of " + numberOfPages_Public + ".");
									middleItemMeta.setLore(lore);
									middleItem.setItemMeta(middleItemMeta);
								}
								
								if(type.contains("shop")) {
									
									PublicPointsGrouped publicPoints = publicPointsGrouped(playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Password.toString()) == null, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Fee.toString()) == null);
									int numberOfPages_Shops = (int) (Math.ceil(publicPoints.shops.size() / 14.0));
									
									if(name.contains("Next")) {
										if((currentPage + 1) <= numberOfPages_Shops)
											currentPage++;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									if(name.contains("Previous")) {
										if((currentPage - 1) >= 1)
											currentPage--;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									clearPoints(event.getClickedInventory(), player, PageType.SHOP);
									populatePoints(player, event.getClickedInventory(), menuName, PageType.SHOP, publicPoints.shops, currentPage);
									
									//update the inventory:
									event.getClickedInventory().setItem(27, getPageItem(PageType.SHOP, "back", currentPage, numberOfPages_Shops));
									event.getClickedInventory().setItem(35, getPageItem(PageType.SHOP, "forward", currentPage, numberOfPages_Shops));
									
									ItemStack middleItem = event.getClickedInventory().getItem(13);
									ItemMeta middleItemMeta = middleItem.getItemMeta();
									List<String> lore = new ArrayList<String>();
									lore.add(ChatColor.WHITE + "Player owned shops.");
									lore.add(ChatColor.GRAY + " > There are " + publicPoints.shops.size() + " shop travel points.");
									lore.add(ChatColor.GRAY + " > Showing page " + currentPage + " of " + numberOfPages_Shops + ".");
									middleItemMeta.setLore(lore);
									middleItem.setItemMeta(middleItemMeta);
								}
								
								if(type.contains("miscellaneous")) {
									
									PublicPointsGrouped publicPoints = publicPointsGrouped(playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Password.toString()) == null, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Fee.toString()) == null);
									int numberOfPages_Misc = (int) (Math.ceil(publicPoints.misc.size() / 7.0));
									
									if(name.contains("Next")) {
										if((currentPage + 1) <= numberOfPages_Misc)
											currentPage++;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									if(name.contains("Previous")) {
										if((currentPage - 1) >= 1)
											currentPage--;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									clearPoints(event.getClickedInventory(), player, PageType.MISCELLANEOUS);
									populatePoints(player, event.getClickedInventory(), menuName, PageType.MISCELLANEOUS, publicPoints.misc, currentPage);
									
									//update the inventory:
									event.getClickedInventory().setItem(45, getPageItem(PageType.MISCELLANEOUS, "back", currentPage, numberOfPages_Misc));
									event.getClickedInventory().setItem(53, getPageItem(PageType.MISCELLANEOUS, "forward", currentPage, numberOfPages_Misc));
									
									ItemStack middleItem = event.getClickedInventory().getItem(40);
									ItemMeta middleItemMeta = middleItem.getItemMeta();
									List<String> lore = new ArrayList<String>();
									lore.add(ChatColor.WHITE + "Player owned travel points.");
									lore.add(ChatColor.GRAY + " > There are " + publicPoints.misc.size() + " non-shop points.");
									lore.add(ChatColor.GRAY + " > Showing page " + currentPage + " of " + numberOfPages_Misc + ".");
									middleItemMeta.setLore(lore);
									middleItem.setItemMeta(middleItemMeta);
								}
								
								if(type.contains("players")) {
									
									int numberOfPages_Players = (int) (Math.ceil(Bukkit.getOnlinePlayers().size() / 27.0));
									
									if(name.contains("Next")) {
										if((currentPage + 1) <= numberOfPages_Players)
											currentPage++;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									if(name.contains("Previous")) {
										if((currentPage - 1) >= 1)
											currentPage--;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									clearPoints(event.getClickedInventory(), player, PageType.PLAYER);
									populatePlayers(player, event.getClickedInventory(), currentPage);
									
									//update the inventory:
									event.getClickedInventory().setItem(45, getPageItem(PageType.PLAYER, "back", currentPage, numberOfPages_Players));
									event.getClickedInventory().setItem(53, getPageItem(PageType.PLAYER, "forward", currentPage, numberOfPages_Players));
								}
								
								if(type.contains("town")) {
									List<String> townsOrdered = getTownsWithPointsOrdered(player, playerConfig.getString("Travel.Toggle." + ToggleType.Hidden_Towns.toString()) == null, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Fee.toString()) == null);
									int numberOfPages_Towns = (int) (Math.ceil(townsOrdered.size() / 9.0));
									
									if(name.contains("Next")) {
										if((currentPage + 1) <= numberOfPages_Towns)
											currentPage++;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									if(name.contains("Previous")) {
										if((currentPage - 1) >= 1)
											currentPage--;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									clearPoints(event.getClickedInventory(), player, PageType.TOWN);
									populateTownPoints(player, event.getClickedInventory(), townsOrdered, currentPage);

									//update the inventory:
									event.getClickedInventory().setItem(27, getPageItem(PageType.TOWN, "back", currentPage, numberOfPages_Towns));
									event.getClickedInventory().setItem(35, getPageItem(PageType.TOWN, "forward", currentPage, numberOfPages_Towns));
								}
								
								if(type.contains("item finder")) {
									
									boolean isSearching = !event.getView().getTitle().contains("Select Item");
									List<String> itemsSorted = isSearching ? getItemsSorted(player, event.getView().getTitle().substring(12)) : getItemsSorted(player);
									int numberOfPages_Items = (int) (Math.ceil(itemsSorted.size() / 36.0));
									
									if(name.contains("Next")) {
										if((currentPage + 1) <= numberOfPages_Items)
											currentPage++;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									if(name.contains("Previous")) {
										if((currentPage - 1) >= 1)
											currentPage--;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									clearPoints(event.getClickedInventory(), player, PageType.ITEM_FINDER);
									populateItemFinder(player, event.getClickedInventory(), itemsSorted, currentPage);
									
									//update the inventory:
									event.getClickedInventory().setItem(45, getPageItem(PageType.ITEM_FINDER, "back", currentPage, numberOfPages_Items));
									event.getClickedInventory().setItem(53, getPageItem(PageType.ITEM_FINDER, "forward", currentPage, numberOfPages_Items));
								}
								
								if(type.contains("results")) {
									
									ItemStack middleItem = event.getClickedInventory().getItem(13);
									ItemMeta middleItemMeta = middleItem.getItemMeta();
									
									String itemFinderName =getItemFinderNameFromGUI(middleItemMeta.getDisplayName());
									
									List<Shop> shopsWithStock = Shops.getShopsWithStock(ChatColor.stripColor(itemFinderName), playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Password.toString()) == null, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Fee.toString()) == null);
									List<Shop> shopsSorted = sortShopsForGUI(player, shopsWithStock);
									int numberOfPages = (int) (Math.ceil(shopsWithStock.size() / 9.0));
									
									if(name.contains("Next")) {
										if((currentPage + 1) <= numberOfPages)
											currentPage++;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									if(name.contains("Previous")) {
										if((currentPage - 1) >= 1)
											currentPage--;
										else {
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "No more pages.");
											event.setCancelled(true);
											return;
										}
									}
									clearPoints(event.getClickedInventory(), player, PageType.RESULTS);
									populateItemFinderResults(player, event.getClickedInventory(), shopsSorted, currentPage);
									
									//update the inventory:
									event.getClickedInventory().setItem(27, getPageItem(PageType.RESULTS, "back", currentPage, numberOfPages));
									event.getClickedInventory().setItem(35, getPageItem(PageType.RESULTS, "forward", currentPage, numberOfPages));
									
									List<String> lore = new ArrayList<String>();
									lore.add(ChatColor.GRAY + " > There are " + shopsWithStock.size() + " shops with stock.");
									lore.add(ChatColor.GRAY + " > Showing page " + currentPage + " of " + numberOfPages + ".");
									middleItemMeta.setLore(lore);
									middleItem.setItemMeta(middleItemMeta);
								}
							});
						}
						
						if(event.getCurrentItem().getItemMeta().getLore().get(0) != null
								&& (privateSlots.contains(event.getSlot()) || publicSlots.contains(event.getSlot()))) {
							String description = event.getCurrentItem().getItemMeta().getLore().get(0);
							
							//travel in manage menu:
							if(event.getView().getTitle().contains("Manage Points")) {
								
								Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
									if(description.contains("total visits")) {
										//clicked private point
										
										String pointName = ChatColor.stripColor(name);
										if(event.isLeftClick()) {
											//travel
											PlayerConfig playerConfig = PlayerConfig.getConfig(player);
											Location location = LocationCode.Decode(playerConfig.getString("Travel." + pointName + ".LocationCode"));
											Travel.travel(player, pointName, location);
											
											//track the travel
											playerConfig.set("Travel." + pointName + ".Uses", playerConfig.getString("Travel." + pointName + ".Uses") == null ? 1 : playerConfig.getInt("Travel." + pointName + ".Uses") + 1);
											playerConfig.save();
											
											Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
										}
										if(event.getClick() == ClickType.MIDDLE) {
											//quick slot
											openQuickSlotMenu(player, pointName, "Private", "Manage Points");
										}
										if(event.isRightClick()) {
											openManagePrivateTP(player, pointName);
										}
									}
									if(description.contains("average daily visits") || description.contains("with stock")) {
										//clicked public point or shop
										
										String pointName = ChatColor.stripColor(name);
										if(event.isLeftClick()) {
											//travel
											PublicTP tpConfig = PublicTP.getConfig(pointName);
											Location location = LocationCode.Decode(tpConfig.getString("LocationCode"));
											Travel.travel(player, pointName, location);
											
											//track the travel
											Travel.trackPublicTravel(player.getUniqueId(), pointName);
											
											Bukkit.getScheduler().runTask(plugin, () -> player.closeInventory());
										}
										if(event.getClick() == ClickType.MIDDLE) {
											//quick slot
											
											if(description.contains("with stock"))
												openQuickSlotMenu(player, pointName, "Shop", "Manage Points");
											else
												openQuickSlotMenu(player, pointName, "Public", "Manage Points");
										}
										if(event.isRightClick()) {
											//manage only from manage menu
											openManagePublicTP(player, pointName);
										}
									}
								});
							}
							
							//travel in public menu:
							if(event.getView().getTitle().contains("Public Points")) {
								//clicked public point or shop
								
								String pointName = ChatColor.stripColor(name);
								if(event.isLeftClick()) {
									PublicTP tpConfig = PublicTP.getConfig(pointName);
									
									//check if has password or fee
									if(tpConfig.getString("Pass") != null) {
										//has password
										
										//if opening anvil menu, set player XP to 0 first or they can take the item.
										if(resetXP.containsKey(player.getName()))
											resetXP.remove(player.getName());
										resetXP.put(player.getName(), player.getLevel());
										player.setLevel(0);
										
										new AnvilGUI.Builder()
												.onClose(p3 -> {
													//give original XP back
													if(resetXP.containsKey(player.getName())) {
														player.setLevel(resetXP.get(player.getName()));
														resetXP.remove(player.getName());
													}
												})
												
												.onComplete((p3, pass) -> { //called when the inventory output slot is clicked
													
													if(!pass.equalsIgnoreCase(tpConfig.getString("Pass"))) {
														Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Password incorrect.");
														return AnvilGUI.Response.text("Incorrect!");
													}
													
													Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Password correct!");
													
													//check if has fee,
													if(tpConfig.getString("Cost") != null) {
														//has a fee, open confirm menu
														
														ItemStack confirmItem = openConfirmMenu(player, Icon.Confirm_public_travel, "public points", false);
														ItemMeta confirmMeta = confirmItem.getItemMeta();
														List<String> lore = new ArrayList<String>();
														lore.add(ChatColor.WHITE + "Travel to " + pointName + "?");
														lore.add(ChatColor.RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Cost: " + ChatColor.GOLD + Economy.convertCoins(tpConfig.getInt("Cost")));
														confirmMeta.setLore(lore);
														confirmItem.setItemMeta(confirmMeta);
													} else {
														//no fee
														//travel
														Location location = LocationCode.Decode(tpConfig.getString("LocationCode"));
														Travel.travel(player, pointName, location);
														
														//track the travel
														Travel.trackPublicTravel(player.getUniqueId(), pointName);
													}
													
													return AnvilGUI.Response.close();
												})
												.text("Password") //starting text
												.itemLeft(new ItemStack(Material.NAME_TAG))
												.onLeftInputClick(p3 -> Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Click the output slot to continue."))
												
												.title(ChatColor.BOLD + "Enter password" + ChatColor.RESET + ":") //set the title of the GUI
												.plugin(plugin)
												.open(player);
									} else if(tpConfig.getString("Cost") != null) {
										//no password, but has fee
										
										//open fee confirm menu:
										ItemStack confirmItem = openConfirmMenu(player, Icon.Confirm_public_travel, "public points", false);
										ItemMeta confirmMeta = confirmItem.getItemMeta();
										List<String> lore = new ArrayList<String>();
										lore.add(ChatColor.WHITE + "Travel to " + pointName + "?");
										lore.add(ChatColor.RED + " " + ChatColor.BOLD + ">" + ChatColor.GRAY + " Cost: " + ChatColor.GOLD + Economy.convertCoins(tpConfig.getInt("Cost")));
										confirmMeta.setLore(lore);
										confirmItem.setItemMeta(confirmMeta);
									} else {
										//no password or fee
										//travel
										Location location = LocationCode.Decode(tpConfig.getString("LocationCode"));
										Travel.travel(player, pointName, location);
										
										//track the travel
										Travel.trackPublicTravel(player.getUniqueId(), pointName);
										
										player.closeInventory();
									}
								}
								if(event.getClick() == ClickType.MIDDLE) {
									//quick slot
									Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> openQuickSlotMenu(player, pointName, description.contains("with stock") ? "Shop" : "Public", "public points"));
								}
								if(event.isRightClick() && player.isOp()) {
									//manage only from manage menu
									Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> openManagePublicTP(player, pointName));
								}
								
							}
							
						}
						
					}
					
					if(event.getView().getTitle().contains("Manage") && (event.getView().getTitle().contains("Public") || event.getView().getTitle().contains("Private"))) {
						
						boolean isPublic = event.getView().getTitle().contains("Public");
						
						if(name.toLowerCase().contains("set icon")) {
							String description = event.getCurrentItem().getItemMeta().getLore().get(0);
							String pointName = ChatColor.stripColor(description).substring(20, ChatColor.stripColor(description).length()-1);
							openSetIconMenu(player, pointName, isPublic, 0);
						}
						
						if(name.toLowerCase().contains("delete")) {
							String pointName = ChatColor.stripColor(meta.getLore().get(1)).substring(10); //the travel point name
							
							ItemStack confirmItem = isPublic ? openConfirmMenu(player, Icon.DELETE_PUBLIC_TP_CONFIRM, "manage public: " + pointName, false) : openConfirmMenu(player, Icon.DELETE_PRIVATE_TP_CONFIRM, "manage private: " + pointName, false);
							ItemMeta deletemeta = confirmItem.getItemMeta();
							List<String> lore = new ArrayList<String>();
							lore.add(" " + ChatColor.GRAY + "" + ChatColor.BOLD + ">" + ChatColor.GRAY + " Name: " + ChatColor.AQUA + ChatColor.BOLD + pointName);
							deletemeta.setLore(lore);
							confirmItem.setItemMeta(deletemeta);
						}
						
						if(name.toLowerCase().contains("fee")) {
							String pointName = ChatColor.stripColor(meta.getLore().get(1)).substring(19); //the travel point name
							
							if(event.isLeftClick()) {
								//if opening anvil menu, set player XP to 0 first or they can take the item.
								if(resetXP.containsKey(player.getName()))
									resetXP.remove(player.getName());
								resetXP.put(player.getName(), player.getLevel());
								player.setLevel(0);
								
								new AnvilGUI.Builder()
										.onClose(p3 -> {
											//give original XP back
											if(resetXP.containsKey(player.getName())) {
												player.setLevel(resetXP.get(player.getName()));
												resetXP.remove(player.getName());
											}
										})
										
										.onComplete((p3, fee) -> { //called when the inventory output slot is clicked
											if(!Chat.isInteger(fee) || Integer.parseInt(fee) < 0) {
												Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Enter a positive number.");
												return AnvilGUI.Response.text("Enter number.");
											}
											
											PublicTP tpConfig = PublicTP.getConfig(pointName);
											if(Integer.parseInt(fee) == 0)
												tpConfig.set("Cost", null);
											else
												tpConfig.set("Cost", Integer.parseInt(fee));
											tpConfig.save();
											
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + pointName + " has been updated!");
											
											player.sendMessage("");
											player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + pointName + " has been updated!");
											player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "New travel cost: " + ChatColor.GOLD + Economy.convertCoins(Integer.parseInt(fee)));
											player.sendMessage("");
											
											openManagePublicTP(player, pointName, false);
											
											return AnvilGUI.Response.close();
										})
										.text("0") //starting text
										.itemLeft(new ItemStack(Material.NAME_TAG))
										.onLeftInputClick(p3 -> Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Click the output slot to continue."))
										
										.title(ChatColor.DARK_GRAY + "Set: " + ChatColor.RESET + ChatColor.BOLD + "Fee" + ChatColor.RESET + " ?") //set the title of the GUI
										.plugin(plugin)
										.open(player);
							}
							
							if(event.isRightClick()) {
								//remove the fee
								PublicTP tpConfig = PublicTP.getConfig(pointName);
								if(tpConfig.getString("Cost") != null) {
									tpConfig.set("Cost", null);
									tpConfig.save();
									
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + "Travel fee removed!");
									
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + pointName + "'s travel fee removed!");
									
									//just update the item
									ItemStack feeItem = event.getCurrentItem();
									ItemMeta feemeta = feeItem.getItemMeta();
									List<String> feelore = new ArrayList<String>();
									feelore.add(ChatColor.GRAY + " > Set the fee for " + pointName);
									feemeta.setLore(feelore);
									feeItem.setItemMeta(feemeta);
								}
							}
							
						}
						
						if(name.toLowerCase().contains("pass")) {
							
							String pointName = ChatColor.stripColor(meta.getLore().get(1)).substring(24); //the travel point name
							
							if(event.isLeftClick()) {
								//if opening anvil menu, set player XP to 0 first or they can take the item.
								if(resetXP.containsKey(player.getName()))
									resetXP.remove(player.getName());
								resetXP.put(player.getName(), player.getLevel());
								player.setLevel(0);
								
								new AnvilGUI.Builder()
										.onClose(p3 -> {
											//give original XP back
											if(resetXP.containsKey(player.getName())) {
												player.setLevel(resetXP.get(player.getName()));
												resetXP.remove(player.getName());
											}
										})
										
										.onComplete((p3, pass) -> { //called when the inventory output slot is clicked
											
											if(pass.contains(" ")) {
												Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Password cannot contain spaces.");
												return AnvilGUI.Response.text("Contains spaces!");
											}
											
											//create public travel file
											PublicTP tpConfig = PublicTP.getConfig(pointName);
											tpConfig.set("Pass", pass);
											tpConfig.save();
											
											Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + pointName + " has been updated!");
											
											player.sendMessage("");
											player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + pointName + " has been updated!");
											String hidePassword = "";
											for(int count = 1; count < (pass.length() - 1); count++)
												hidePassword = hidePassword + "*";
											player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Password: " + ChatColor.AQUA + pass.substring(0, 1) + hidePassword + pass.substring(pass.length() - 1, pass.length()));
											player.sendMessage("");
											
											openManagePublicTP(player, pointName, false);
											
											return AnvilGUI.Response.close();
										})
										.text("Password") //starting text
										.itemLeft(new ItemStack(Material.NAME_TAG))
										.onLeftInputClick(p3 -> Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.RED + "Click the output slot to continue."))
										
										.title(ChatColor.DARK_GRAY + "Set: " + ChatColor.RESET + ChatColor.BOLD + "Password" + ChatColor.RESET + " ?") //set the title of the GUI
										.plugin(plugin)
										.open(player);
							}
							
							if(event.isRightClick()) {
								//remove the pass
								PublicTP tpConfig = PublicTP.getConfig(pointName);
								if(tpConfig.getString("Pass") != null) {
									tpConfig.set("Pass", null);
									tpConfig.save();
									
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + pointName + " has been updated!");
									
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.GREEN + pointName + " has been updated!");
									player.sendMessage(ChatColor.YELLOW + "[Travel] " + ChatColor.WHITE + "Password was removed.");
									player.sendMessage("");
									
									//just update the item
									ItemStack passItem = event.getCurrentItem();
									ItemMeta passmeta = passItem.getItemMeta();
									List<String> passlore = new ArrayList<String>();
									passlore.add(ChatColor.GRAY + " > Set the password for " + pointName);
									passmeta.setLore(passlore);
									passItem.setItemMeta(passmeta);
								}
							}
						}
					}
					
					event.setCancelled(true);
				}
				
			}
			
		} catch(Exception e) {
			event.setCancelled(true);
			Chat.logError(e, event.getWhoClicked() instanceof Player player ? player : null);
		}
	}
	
	private int getFreeQuickSlot(PlayerConfig playerConfig) {
		//loop quick slots
		int maxSlotsAllowed = DeadMC.DonatorFile.data().getInt(playerConfig.getInt("DonateRank") + ".Upgrades.QuickSlots");
		for(int slot = 1; slot <= maxSlotsAllowed; slot++) {
			if(playerConfig.getString("Travel.QuickSlot" + slot + ".Name") == null) {
				return slot;
			}
		}
		return -1;
	}
	
	private Shop getShopClickedInResultsMenu(Player player, Inventory inventory, String itemFinderName, int slotClicked) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);

		List<Shop> shopsWithStock = Shops.getShopsWithStock(itemFinderName, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Password.toString()) == null, playerConfig.getString("Travel.Toggle." + ToggleType.Points_With_Fee.toString()) == null);
		List<Shop> shopsSorted = sortShopsForGUI(player, shopsWithStock);
		int currentPage = getPageNumberFromArrow(inventory.getItem(27));
		
		int startingIndex = (currentPage-1) * itemFinderResultsSlots.size();
		int slot = slotClicked - itemFinderResultsSlots.get(0);
		int shopIndex = startingIndex + slot;
		
		return shopsSorted.get(shopIndex);
	}
	
	@EventHandler
	public void travelGUIdrag(InventoryDragEvent event) {
		if(event.getWhoClicked() instanceof Player) {
			if(event.getView().getTitle().contains("Travel") || event.getView().getTitle().contains("Manage") || event.getView().getTitle().contains("Set Icon") || event.getView().getTitle().contains("Item Finder")) {
				event.setCancelled(true);
			}
		}
	}
	
}

class PublicPointsGrouped {
	List<String> shops;
	List<String> misc;
	
	PublicPointsGrouped(List<String> shops, List<String> misc) {
		this.shops = shops;
		this.misc = misc;
	}
}
