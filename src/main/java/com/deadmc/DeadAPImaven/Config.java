package com.deadmc.DeadAPImaven;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {
	private DeadMC plugin;
	
	private FileConfiguration data = null;
	private File dataFile = null;
	public String fileName;
	
	public Config(DeadMC plugin, String fileName) {
		this.plugin = plugin;
		this.fileName = fileName;
	}
	
	public void reload() {
		
		if(dataFile == null)
			dataFile = new File(plugin.getDataFolder(), fileName);
		
		data = YamlConfiguration.loadConfiguration(dataFile);
		
		File defConfigStream = new File(fileName);
		if(defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			data.setDefaults(defConfig);
		}
		
	}
	
	public FileConfiguration data() {
		
		if(data == null)
			reload();
		
		return data;
	}
	
	public void save() {
		
		if(data == null || dataFile == null)
			return;
		
		try {
			data().save(dataFile);
		} catch(IOException ex) {
			plugin.getLogger().log(Level.SEVERE, "Could not save config to " + dataFile, ex);
		}
		
	}
	
	public void saveDefault(Boolean force) {
		
		if(dataFile == null)
			dataFile = new File(plugin.getDataFolder(), fileName);
		
		if(!dataFile.exists() || force == true)
			plugin.saveResource(fileName, force);
		
	}
	
}
