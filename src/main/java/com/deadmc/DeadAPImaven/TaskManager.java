package com.deadmc.DeadAPImaven;

import static java.util.stream.Collectors.toMap;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.deadmc.DeadAPImaven.Chat.Leaderboard;
import com.deadmc.DeadAPImaven.Task.TaskStep;

public class TaskManager implements CommandExecutor {
	private static DeadMC plugin;
	public TaskManager(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public enum Difficulty {
		EASY,
		MEDIUM,
		HARD,
		ELITE
	};
	
	public enum TaskType {
		KILL,
		HARVEST,
		BREAK, //can't place this block until task complete (have high amounts to suggest farms eg. 1000x cactus)
		PLACE,
		TRAVEL, //a travel event (travelling to wilderness, market, nether, end)
		WALK, //walking distance
		ENCHANT,
		BREED,
		BREW,
		MILK,
		SHEAR,
		CATCH, //fish
		TAME,
		SELL //can't buy this item until task complete
	}
	
	public static List<String> confirmSkipTask = new ArrayList<String>();
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		final Player player = (Player) sender;

	    if(commandLabel.equalsIgnoreCase("task")) {
	    	
			boolean inDevelopment = false;
			if(inDevelopment) {
				player.sendMessage("");
				player.sendMessage(ChatColor.DARK_RED + "[DeadMC] " + ChatColor.WHITE + ChatColor.BOLD + "PLEASE NOTE: " + ChatColor.WHITE + "Tasks are currently in development. Please see this post for info:");
				player.sendMessage(ChatColor.WHITE + "Read about it here: " + ChatColor.GOLD + "https://bit.ly/3bzbxjh");
				Chat.sendTitleMessage(player.getUniqueId(), "" + ChatColor.DARK_RED + ChatColor.BOLD + "PLEASE NOTE: " + ChatColor.WHITE + "Tasks are currently in development. See the chat for more info.");
			}
			
	    	PlayerConfig playerConfig = PlayerConfig.getConfig(player);
	    	
	    	if(args.length == 0) {
	    		
    			Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
    			    @Override
    			    public void run() {

			    		player.sendMessage("");
			    		player.sendMessage(ChatColor.YELLOW + "==================== Task Help ====================");
			    		
			    		//if streak > 0
			    		if(playerConfig.getString("Streak") != null) {
							player.sendMessage(ChatColor.WHITE + "Current streak: " + ChatColor.BOLD + playerConfig.getInt("Streak"));
							player.sendMessage(ChatColor.WHITE + "The higher your streak, the greater the reward!");
							DecimalFormat formatter = new DecimalFormat("#.##");
							formatter.setRoundingMode(RoundingMode.UP);
							player.sendMessage(ChatColor.WHITE + "  Current reward multiplier: " + ChatColor.BOLD + formatter.format(GetStreakMultiplier(playerConfig.getInt("Streak"))) + ChatColor.WHITE + "x");
							player.sendMessage("");
			    			//eg. Current streak: 1
				    		//Reward multiplier = 1.15 (0.15*streak)
			    		}
		
				    	if(playerConfig.getString("Task") == null) {
				    		
				    		player.sendMessage(ChatColor.RED + "You currently don't have a task assigned.");
				    		player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/task new" + ChatColor.WHITE + " to assign and skip tasks.");
				    		
					    	player.sendMessage("");
					    	player.sendMessage(ChatColor.BOLD + "Complete tasks for rewards.");
					    	player.sendMessage("The bigger your streak: the greater the reward!");
					    	
				    	} else {
				    		
				    		int ID = playerConfig.getInt("Task");
				    		int step = playerConfig.getInt("Step");
				    		
				    		if(ID == 0) { //is in tutorial
					    		player.sendMessage(ChatColor.WHITE + "You currently have a task assigned:");
					    		player.sendMessage(ChatColor.YELLOW + "  - Task name: " + ChatColor.AQUA + ChatColor.BOLD + TaskManager.Tasks.get(ID).name);
					    		player.sendMessage(ChatColor.YELLOW + "  - Current step: " + ChatColor.AQUA + ChatColor.BOLD + TaskStep.values()[step].toString().charAt(0) + TaskStep.values()[step].toString().toLowerCase().replace("_", " ").substring(1,TaskStep.values()[step].toString().length()) + ChatColor.WHITE + " (Step " + ChatColor.BOLD + (step+1) + ChatColor.WHITE + " of " + ChatColor.BOLD + TaskManager.Tasks.get(0).steps.size() + ChatColor.WHITE + ")");
								player.sendMessage(ChatColor.YELLOW + "  - Tips: " + ChatColor.WHITE + TaskManager.Tasks.get(ID).steps.get(TaskStep.values()[step]));
								player.sendMessage("");
								player.sendMessage(ChatColor.RED + "" + ChatColor.ITALIC + ChatColor.BOLD + "Skip" + ChatColor.RED + " the tutorial with " + ChatColor.GOLD + "/task new" + ChatColor.RED + ".");
				    		} else {
								Difficulty difficulty = GetDifficulty(ID);
								
								String taskName = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + ".Task name");
								int numberOfSteps = DeadMC.TaskFile.data().getInt("Tasks." + difficulty.ordinal() + "." + ID + ".Step count");
								String stepDescription = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Step description");
								
								String difficultyString = ChatColor.WHITE + "an " + ChatColor.GREEN + "EASY";
								if(difficulty == Difficulty.MEDIUM) difficultyString = ChatColor.WHITE + "a " + ChatColor.YELLOW + "MEDIUM";
								if(difficulty == Difficulty.HARD) difficultyString = ChatColor.WHITE + "a " + ChatColor.RED + "HARD";
								if(difficulty == Difficulty.ELITE) difficultyString = ChatColor.WHITE + "an " + ChatColor.DARK_RED + "ELITE";
		
								player.sendMessage(ChatColor.WHITE + "You currently have " + difficultyString + ChatColor.WHITE + " task assigned:");
								player.sendMessage(ChatColor.YELLOW + "  - Task name: " + ChatColor.AQUA + ChatColor.BOLD + taskName);
								player.sendMessage(ChatColor.YELLOW + "  - Current step: " + ChatColor.AQUA + stepDescription + "." + ChatColor.WHITE + " (Step " + ChatColor.BOLD + (step+1) + ChatColor.WHITE + " of " + ChatColor.BOLD + (numberOfSteps+1) + ChatColor.WHITE + ")");
								if(playerConfig.getString("Track") != null)
								player.sendMessage(ChatColor.YELLOW + "  - Progress: " + ChatColor.WHITE + ChatColor.BOLD + playerConfig.getInt("Track") + ChatColor.WHITE + " / " + ChatColor.BOLD + DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Amount"));
								if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Tips") != null)
								player.sendMessage(ChatColor.YELLOW + "  - Tips: " + ChatColor.WHITE + DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Tips"));
								player.sendMessage("");
								player.sendMessage(ChatColor.WHITE + "Complete the task for a reward!");
								player.sendMessage("Or " + ChatColor.ITALIC + "skip" + ChatColor.WHITE + " the task with " + ChatColor.GOLD + "/task new" + ChatColor.WHITE + ".");
				    		}
				    	}
			    		player.sendMessage(ChatColor.YELLOW + "=================================================");
			    		player.sendMessage("");
	    		
    			    }
    			});
	    		
	    	} else if(args.length > 0) {
	    		
	    		
    			//player commands
	    		if(args[0].equalsIgnoreCase("new")) {
	    			if(playerConfig.getString("Task") != null) {
	    				//player has a task already
	    				
	    				int ID = playerConfig.getInt("Task");
	    				int step = playerConfig.getInt("Step");
	    				if(ID == 0) { //if is in tutorial
		    				// - can skip tutorial, but warn and confirm:
		    				// - complete the tutorial to unlock tasks and for a reward
	    					
	    					if(!confirmSkipTask.contains(player.getName())) {
	    						
	    						confirmSkipTask.add(player.getName());
	    						//confirm session expired
	    						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
	    							@Override
	    							public void run() {
	    								confirmSkipTask.remove(player.getName());
	    							}
	    						}, 600L); // 30 seconds
	    						
	    			    		player.sendMessage("");
	    			    		player.sendMessage(ChatColor.YELLOW + "==================== Task Help ====================");
	    			    		player.sendMessage(ChatColor.RED + "Are you sure you want to " + ChatColor.BOLD + "SKIP THE TUTORIAL" + ChatColor.RED + "?");
	    			    		player.sendMessage("");
	    			    		player.sendMessage(ChatColor.WHITE + "Complete the tutorial to unlock tasks and for a reward!");
	    			    		player.sendMessage("Or " + ChatColor.ITALIC + ChatColor.BOLD + "skip" + ChatColor.WHITE + " the tutorial with " + ChatColor.GOLD + "/task new" + ChatColor.WHITE + ".");
	    			    		player.sendMessage(ChatColor.YELLOW + "=================================================");
	    			    		player.sendMessage("");
	    						
	    					} else {
	    						assignNewTask(player.getUniqueId());
	    						confirmSkipTask.remove(player.getName());
	    					}
	    					
	    					return true;
	    					
	    				}
	    				
	    				if(playerConfig.getString("Streak") != null) {
	    					//player has streak >= 1
	    					//are you sure you want to cancel your streak of 1?
		    				// the higher your streak, the greater the reward
	    					
	    					if(!confirmSkipTask.contains(player.getName())) {
	    						
	    						confirmSkipTask.add(player.getName());
	    						//confirm session expired
	    						Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
	    							@Override
	    							public void run() {
	    								confirmSkipTask.remove(player.getName());
	    							}
	    						}, 600L); // 30 seconds
	    						
	    						Difficulty difficulty = GetDifficulty(ID);
	    						
	    						String taskName = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + ".Task name");
	    						int numberOfSteps = DeadMC.TaskFile.data().getInt("Tasks." + difficulty.ordinal() + "." + ID + ".Step count");
	    						
	    						String placeholder = Chat.getTitlePlaceholder(new String(taskName + " | Step " + (step+1) + " of " + (numberOfSteps+1)), 44);
	    						
	    						player.sendMessage("");
	    						player.sendMessage(ChatColor.YELLOW + placeholder + " " + ChatColor.WHITE + "" + ChatColor.BOLD + taskName + ChatColor.WHITE + " | Step " + ChatColor.BOLD + (step+1) + ChatColor.WHITE + " of " + ChatColor.BOLD + (numberOfSteps+1) + ChatColor.YELLOW + " " + placeholder);
	    						player.sendMessage(ChatColor.RED + "Confirm to skip your " + ChatColor.RED + ChatColor.BOLD + difficulty.toString() + ChatColor.RED + " task and " + ChatColor.RED + ChatColor.BOLD + "reset your streak" + ChatColor.RED + "?");
	    						player.sendMessage(ChatColor.WHITE + "  Current streak: " + ChatColor.BOLD + playerConfig.getInt("Streak"));
	    						player.sendMessage("");
	    						player.sendMessage(ChatColor.WHITE + "The higher your streak, the greater the reward!");
	    						DecimalFormat formatter = new DecimalFormat("#.##");
	    						formatter.setRoundingMode(RoundingMode.UP);
	    						player.sendMessage(ChatColor.WHITE + "  Current reward multiplier: " + ChatColor.BOLD + formatter.format(GetStreakMultiplier(playerConfig.getInt("Streak"))) + ChatColor.WHITE + "x");
	    						player.sendMessage("Confirm to " + ChatColor.ITALIC + "" + ChatColor.BOLD + "skip" + ChatColor.WHITE + " the task by using " + ChatColor.GOLD + "/task new" + ChatColor.WHITE + " again.");
	    						player.sendMessage(ChatColor.YELLOW + "============================================");
	    						player.sendMessage("");
	    						
	    					} else {
	    						//just skip the task (confirmed)
		    					//confirm contains:

	    						playerConfig.set("Streak", null); //reset streak
	    						playerConfig.save();
	    						assignNewTask(player.getUniqueId());
	    						confirmSkipTask.remove(player.getName());
	    					}
	    					
	    				} else {
	    					assignNewTask(player.getUniqueId());
	    					//just skip the task (streak is 0)
	    				}
	    				
	    			} else {
	    				assignNewTask(player.getUniqueId());
	    			}
	    		} else if(args[0].equalsIgnoreCase("list")) {
	    				
    				Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
        			    @Override
        			    public void run() {
        			    	
		    				List<Integer> tasksToShow = new ArrayList<Integer>(); //list of easy task IDs
		    					
		    				
		    				if(args.length <= 2) {
		    					
		    					//add all tasks from difficulty lists
		    					for(int difficulty = 0; difficulty < Difficulty.values().length; difficulty++) {
		    						tasksToShow.addAll(DeadMC.TaskFile.data().getIntegerList("List." + difficulty));
		    					}
		    					
		    				} else {
		    					
		    					Difficulty searchDifficultyOnly = null;
		    					if(args.length > 3) {
									if(getDifficultyFromString(args[3]) != null) {
			    						searchDifficultyOnly = getDifficultyFromString(args[3]);
			    					} else {
			    						player.sendMessage("");
			    						player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + "'" + args[3] + "' is not a difficulty.");
			    						for(int typeID = 0; typeID < Difficulty.values().length; typeID++) {
			    							player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + " - " + Difficulty.values()[typeID]);
			    						}
			    						player.sendMessage("");
			    						return;
			    					}
								}
		    					
		    					if(args.length > 2) {
			    					//add only certain types
			    					TaskType type = null;
			    					if(!args[2].equalsIgnoreCase("all")) {
				    					try {
				    						//is arg 1 a real type?
				    						type = TaskType.valueOf(args[2].toUpperCase());
				    					} catch(IllegalArgumentException e) {
				    						//not real, display options
				    						player.sendMessage("");
				    						player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + "'" + args[2] + "' is not a valid type:");
				    						player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + " - all");
				    						for(int typeID = 0; typeID < TaskType.values().length; typeID++) {
				    							player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + " - " + TaskType.values()[typeID]);
				    						}
				    						player.sendMessage("");
				    						return;
				    					}
			    					}
			    					//loop all tasks, check if it matches type
			    					if(searchDifficultyOnly == null) {
				    					for(int difficulty = 0; difficulty < Difficulty.values().length; difficulty++) {
				    						for(int task : DeadMC.TaskFile.data().getIntegerList("List." + difficulty)) {
				    							if(type == null) {
				    								tasksToShow.add(task);
				    							} else {
					    							//for each step
					    							int step = 0;
					    							while(DeadMC.TaskFile.data().getString("Tasks." + difficulty + "." + task + "." + step + ".Type") != null) {
						    							if(DeadMC.TaskFile.data().getInt("Tasks." + difficulty + "." + task + "." + step + ".Type") == type.ordinal()) {
						    								tasksToShow.add(task);
						    							}
						    							step++;
					    							}
				    							}
				    						}
				    					}
			    					} else {
			    						for(int task : DeadMC.TaskFile.data().getIntegerList("List." + searchDifficultyOnly.ordinal())) {
			    							if(type == null) {
			    								tasksToShow.add(task);
			    							} else {
				    							//for each step
				    							int step = 0;
				    							while(DeadMC.TaskFile.data().getString("Tasks." + searchDifficultyOnly.ordinal() + "." + task + "." + step + ".Type") != null) {
					    							if(DeadMC.TaskFile.data().getInt("Tasks." + searchDifficultyOnly.ordinal() + "." + task + "." + step + ".Type") == type.ordinal()) {
					    								tasksToShow.add(task);
					    							}
					    							step++;
				    							}
			    							}
			    						}
			    					}
			    					
			    					//task list <page> <type> <difficulty as string or int>
		    					}
		    				}
		    				
							final int pageNumber = (args.length > 1 && Chat.isInteger(args[1])) ? Integer.parseInt(args[1]) - 1 : 0;
		        			    	
							int tasksPerPage = 5;
	    			    	
							int maxTaskToInclude = (tasksPerPage + (pageNumber * tasksPerPage)) - 1; //start at page 0
							int minTaskToInclude = pageNumber * tasksPerPage;
							//0 - 9 
							// 10 - 19
							
							//get towns on page
							List<Integer> tasksOnPage = new ArrayList<Integer>();
							int count = 0;
							for(int task : tasksToShow) {
								if(count >= minTaskToInclude && count <= maxTaskToInclude) {
									tasksOnPage.add(task);
								}
								if(count > maxTaskToInclude) break;
								count++;
							}
							
							int numberOfPages = (int) Math.ceil(new Double(tasksToShow.size() / tasksPerPage));
							
			    			player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder("List of tasks", 46) + " List of tasks " + Chat.getTitlePlaceholder("List of tasks", 46));
							player.sendMessage(ChatColor.WHITE + "There are " + ChatColor.GOLD + ChatColor.BOLD + tasksToShow.size() + ChatColor.WHITE + " tasks to show.");
							player.sendMessage("");
							if(pageNumber > numberOfPages) player.sendMessage(ChatColor.RED + "   Page #" + pageNumber + " does not exist.");
							for(int task : tasksOnPage) {
								
								String difficultyString = ChatColor.GREEN + "EASY";
								Difficulty difficulty = GetDifficulty(task);
								if(difficulty == Difficulty.MEDIUM) difficultyString = ChatColor.YELLOW + "MEDIUM";
								if(difficulty == Difficulty.HARD) difficultyString = ChatColor.RED + "HARD";
								if(difficulty == Difficulty.ELITE) difficultyString = ChatColor.DARK_RED + "ELITE";
								
								String taskName = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + task + ".Task name");
								
								player.sendMessage(ChatColor.WHITE + "(" + difficultyString + ChatColor.WHITE + ") " + ChatColor.BOLD + taskName + ChatColor.WHITE + " / " + ChatColor.BOLD + task);
								
								int numberOfSteps = DeadMC.TaskFile.data().getInt("Tasks." + difficulty.ordinal() + "." + task + ".Step count");
								for(int step = 0; step <= numberOfSteps; step++) {
									String stepDescription = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + task + "." + step + ".Step description");
	    							player.sendMessage(ChatColor.WHITE + " - " + stepDescription);
								}
								//(EASY) Burning Up / 38
								// - Kill 5 blaze
							}
							player.sendMessage("");
							player.sendMessage(ChatColor.WHITE + "Displaying page " + (pageNumber+1) + ChatColor.WHITE + " of " + (numberOfPages+1) + ".");
							player.sendMessage(ChatColor.YELLOW + "==============================================");
							player.sendMessage("");
        			    }
    				});
	    				
	    		} else {
	    			player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + "Unknown command. Use " + ChatColor.GOLD + "/task" + ChatColor.WHITE + " for help.");
	    		}
	    		
	    		//admin only commands:
	    		if(player.isOp() 
	    				/*|| (playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") == StaffRank.ADMINISTRATOR.ordinal())*/) {
	    			
	    			
	    			//create a easy, medium, hard task etc.
		    		if(args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("c")) {
		    			
		    			//green = adding-step? dependent
		    			//red = specifics
		    			//gold = descriptives - requirement
		    			//yellow = descriptives - optional
		    			
		    			//task create ID difficulty taskName type what amount stepDescription (using) (tips) 
		    			
		    			
		    			// ID - use "new" to create new task
		    			
		    			// difficulty 0-3 (green) - if adding step, this will be ignored (can be anything)
		    			// task name - if adding step, this will be ignored (can be anything)
		    			
		    			// type (must exist in task types) (red)
		    			// what (must exist in list) (red)
		    			// amount (red)
		    			
		    			// step description (gold)
		    			
		    			// using (yellow)
		    			// tips (yellow)
		    			
		    			if(args.length >= 10) {
		    				
		    				// CREATE AND RELAY DETAILS:
		    				
		    				player.sendMessage("");
		    				player.sendMessage("Attempting to create...");
		    				
			    			Difficulty difficulty = null;
			    			String taskName = null;
			    			TaskType type = null;
			    			String what = null;
			    			int amount = 0;
			    			String stepDescription = null;
			    			String using = null;
			    			String tips = null;
			    			
		    				boolean addStep = false;
		    				int stepCount = 0; //the number of steps in the task
		    				
		    				// ID:
		    				int taskID = DeadMC.TaskFile.data().getInt("Task ID tracker") + 1;
			    			if(args[1].equalsIgnoreCase("new")) {
			    				DeadMC.TaskFile.data().set("Task ID tracker", taskID);
			    			} else if(Chat.isInteger(args[1])) {
			    				
			    				taskID = Integer.parseInt(args[1]);
			    				if(TaskExists(taskID)) {
			    					//add step to task
			    					addStep = true;
			    					
			    					difficulty = GetDifficulty(taskID);
			    					taskName = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + taskID + ".Task name");
			    					stepCount = DeadMC.TaskFile.data().getInt("Tasks." + difficulty.ordinal() + "." + taskID + ".Step count") + 1;
			    				} else {
			    					player.sendMessage(ChatColor.RED + "Error: " + ChatColor.WHITE + "That ID doesn't exist.");
				    				return true; //break
			    				}
			    				
			    			} else {
			    				player.sendMessage(ChatColor.RED + "Error: " + ChatColor.WHITE + "/task c " + ChatColor.GOLD + "new" + ChatColor.WHITE + " or " + ChatColor.GOLD + "<ID>");
			    				return true; //break
			    			}
			    			
			    			// DIFFICULTY

			    			if(!addStep) {
			    				//attempting to create new task
			    				if(getDifficultyFromString(args[2]) != null) {
		    						difficulty = getDifficultyFromString(args[2]);
		    					} else {
		    						player.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.GOLD + "<difficulty>");
		    						for(int count = 0; count < Difficulty.values().length; count++)
		    							player.sendMessage(ChatColor.WHITE + "  - " + ChatColor.GOLD + count + ChatColor.WHITE + ", " + Difficulty.values()[count].toString());
		    						
		    						return true; //break
			    				}
			    			}
			    			
			    			// TASK NAME
			    			
			    			if(!addStep) {
			    				//attempting to create new task
			    				taskName = args[3].replace("_", " ");
			    			}
			    			
			    			// TYPE:
			    			
			    			try {
			    				type = TaskType.valueOf(args[4]);
			    			} catch (IllegalArgumentException e) {
			    				player.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.GOLD + "<type>");
			    				for(int count = 0; count < TaskType.values().length; count++)
		    						player.sendMessage(ChatColor.WHITE + "  - " + ChatColor.GOLD + count + ChatColor.WHITE + ", " + TaskType.values()[count].toString());
			    				
			    				return true; //break
			    			}
			    			
			    			// WHAT:

			    			//use item in hand for placing with '.' - If item is air, what is any
				    		//otherwise type to have it contain characters - eg. all logs, use '_log'
			    			if(type == TaskType.PLACE || type == TaskType.BREAK || type == TaskType.HARVEST) {
			    				
			    				if(args[5].equalsIgnoreCase(".")) {
				    				if(player.getInventory().getItemInMainHand().getType() == Material.AIR)
				    					what = null;
				    				else what = player.getInventory().getItemInMainHand().getType().toString();
			    				} else {
			    					//contain characters in type
			    					what = args[5].toLowerCase();
			    				}
			    				
			    			} else if(type == TaskType.KILL || type == TaskType.BREED) {
			    				
			    				if(args[5].equalsIgnoreCase(".")) {
				    				what = null;
			    				} else {
			    					//contain characters in type
			    					what = args[5].toLowerCase();
			    				}
			    				
			    			} else {
				    			List<String> whats = DeadMC.TaskFile.data().getStringList("What." + type.ordinal());
				    			if((whats == null || whats.size() == 0)
				    					&& !args[5].equalsIgnoreCase(".")) {
				    				player.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.GOLD + "<what>");
				    				player.sendMessage(ChatColor.WHITE + "  - ADD: " + ChatColor.GOLD + "." + ChatColor.WHITE + " (no what option, this arg will be ignored)"); 
				    				
				    				return true; //break
				    			} else
				    				//whats list exists, it must be an int between range:
				    			if(Chat.isInteger(args[5]) && Integer.parseInt(args[5]) < whats.size() && Integer.parseInt(args[5]) >= 0) {
				    				
				    				what = whats.get(Integer.parseInt(args[5]));
				    				
				    			} else 
				    				
				    			if(args[5].equalsIgnoreCase(".")) {
				    				
				    				what = null;
				    				
				    			
				    			} else {
				    				player.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.GOLD + "<what>");
				    				for(int count = 0; count < whats.size(); count++)
			    						player.sendMessage(ChatColor.WHITE + "  - " + ChatColor.GOLD + count + ChatColor.WHITE + ", " + whats.get(count));
				    				
				    				return true; //break
				    			}
		    				}
			    			
			    			// AMOUNT:
			    			
			    			if(!Chat.isInteger(args[6])) {
			    				player.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.GOLD + "<amount>" + ChatColor.WHITE + " (must be int)");
			    				return true; //break
			    			} else {
			    				amount = Integer.parseInt(args[6]);
			    			}
			    			
			    			// USING:

			    			if(!args[7].equalsIgnoreCase(".")) {
			    				using = args[7].toUpperCase();
			    			}
			    			
			    			// STEP DESCRIPTION:
			    			
			    			if(args[8].equalsIgnoreCase(".")) {
			    				if(what == null) {
			    					player.sendMessage(ChatColor.RED + "ERROR: " + ChatColor.GOLD + "<step description>");
			    					player.sendMessage(ChatColor.WHITE + "  - Cannot auto generate with no <what>");
			    					return true; //break
			    				}
			    				
			    				//auto generate
			    				String typeString = type.toString().substring(0, 1).toUpperCase() + type.toString().substring(1, type.toString().length()).toLowerCase();
			    				
			    				String usingString = "";
			    				if(using != null) usingString = " using a " + using.toLowerCase().replace("_", " ");
			    				
			    				String plural = "";
			    				if(amount > 1) plural = "s";
			    				
			    				String whatString = what.toLowerCase().replace("_", " ") + plural;
			    				
			    				stepDescription = typeString + " " + amount + " " + whatString + usingString;
			    			} else {
			    				stepDescription = args[8].replace("_", " ");
			    			}
			    			
			    			// TIPS:
			    			
			    			if(!args[9].equalsIgnoreCase(".")) {
			    				tips = "";
				    			int argCount = 9;
				    			while(argCount < args.length) {
				    				if(argCount == (args.length-1))
				    					tips += args[argCount];
				    				else tips += args[argCount] + " ";
				    				argCount++;
				    			}
			    			}
			    			
			    			//confirm? - creating NEW/adding step
			    			
			    			if(!addStep) {
			    				player.sendMessage(ChatColor.WHITE + "Creating " + ChatColor.GOLD + "NEW" + ChatColor.WHITE + " task!");
			    			}
			    			else { 
			    				player.sendMessage(ChatColor.GOLD + "ADDING STEP" + ChatColor.WHITE + " to " + ChatColor.GOLD + taskName + ChatColor.WHITE + "!");
			    				player.sendMessage(ChatColor.WHITE + "  - Previous step: " + ChatColor.GREEN + (int)(stepCount-1) + ChatColor.WHITE + ", (" + ChatColor.GREEN + DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + taskID + "." + (int)(stepCount-1) + ".Step description") + ChatColor.WHITE + ")");
			    			}
			    			player.sendMessage(ChatColor.WHITE + "  - Task Name: " + ChatColor.GREEN + taskName);
			    			player.sendMessage(ChatColor.WHITE + "  - Difficulty: " + ChatColor.GREEN + difficulty.toString());
			    			player.sendMessage("");
			    			
			    			player.sendMessage(ChatColor.WHITE + "  - Type: " + ChatColor.RED + type.toString());
			    			if(what != null) player.sendMessage(ChatColor.WHITE + "  - What: " + ChatColor.RED + what);
			    			else player.sendMessage(ChatColor.WHITE + "  - What: " + ChatColor.WHITE + "any");
			    			player.sendMessage(ChatColor.WHITE + "  - Amount: " + ChatColor.RED + amount);
			    			player.sendMessage("");
			    			
			    			player.sendMessage(ChatColor.WHITE + "  - Step description: " + ChatColor.GOLD + stepDescription);
			    			player.sendMessage("");
			    			
			    			if(using != null) player.sendMessage(ChatColor.WHITE + "  - Using: " + ChatColor.YELLOW + using);
			    			else player.sendMessage(ChatColor.WHITE + "  - Using: " + ChatColor.WHITE + "any");
			    			if(tips != null) player.sendMessage(ChatColor.WHITE + "  - Tips: " + ChatColor.YELLOW + tips);
			    			else player.sendMessage(ChatColor.WHITE + "  - Tips: " + ChatColor.WHITE + "none");
			    			
			    			player.sendMessage("");
			    			
			    			//return ID. Add another step with /task c <ID>
			    			player.sendMessage(ChatColor.BOLD + "Add another step with " + ChatColor.GOLD + "/task c " + taskID + " . . " + ChatColor.RED + "<type> <what> <amount>" + ChatColor.YELLOW + "<using> <step description> (<tips>)");
			    			
			    			player.sendMessage("");
			    			
			    			//add to list of tasks:
			    			List<Integer> tasksInList = DeadMC.TaskFile.data().getIntegerList("List." + difficulty.ordinal());
			    			if(!tasksInList.contains(taskID)) {
				    			tasksInList.add(taskID);
				    			DeadMC.TaskFile.data().set("List." + difficulty.ordinal(), tasksInList);
			    			}
			    			
			    			//add data:
			    			if(!addStep) {
			    				DeadMC.TaskFile.data().set("Tasks." + difficulty.ordinal() + "." + taskID + ".Task name", taskName);
			    			}
			    			DeadMC.TaskFile.data().set("Tasks." + difficulty.ordinal() + "." + taskID + ".Step count", stepCount);
			    			
			    			DeadMC.TaskFile.data().set("Tasks." + difficulty.ordinal() + "." + taskID + "." + stepCount + ".Step description", stepDescription);
			    			DeadMC.TaskFile.data().set("Tasks." + difficulty.ordinal() + "." + taskID + "." + stepCount + ".Tips", tips);
			    			DeadMC.TaskFile.data().set("Tasks." + difficulty.ordinal() + "." + taskID + "." + stepCount + ".Type", type.ordinal());
			    			DeadMC.TaskFile.data().set("Tasks." + difficulty.ordinal() + "." + taskID + "." + stepCount + ".What", what);
			    			DeadMC.TaskFile.data().set("Tasks." + difficulty.ordinal() + "." + taskID + "." + stepCount + ".Amount", amount);
			    			DeadMC.TaskFile.data().set("Tasks." + difficulty.ordinal() + "." + taskID + "." + stepCount + ".Using", using);
			    			
			    			DeadMC.TaskFile.save();
			    			
		    			} else {
		    				player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.RED + "Missing arguments.");
		    			}
		    		
		    		}
		    		
	    		}
	    		
	    	}
    		
	    }
	    return true;
	}
	
	public static double GetStreakMultiplier(int streak) {
		//uses a decaying exponential function
		// visualise with https://www.desmos.com/calculator/zu4v4qt6bz
		
		double startingMultiplier = 0.1; //the maximum multiplier to start with, gets smaller from there
		double modifier = 0.02; //the bigger, the quicker is decays
		
		//y = a(1-b)^x
		double multiplier = 1;
		for(int count = 1; count <= streak; count++) {
			multiplier += startingMultiplier * Math.pow((1-modifier), count);
		}

		return multiplier;
	}
	
	private boolean TaskExists(int ID) {
		for(int count = 0; count < Difficulty.values().length; count++) {
			if(DeadMC.TaskFile.data().getString("Tasks." + count + "." + ID + ".Task name") != null)
				return true;
		}
		return false;
	}
	
	public static Difficulty GetDifficulty(int ID) {
		for(int count = 0; count < Difficulty.values().length; count++) {
			if(DeadMC.TaskFile.data().getString("Tasks." + count + "." + ID + ".Task name") != null)
				return Difficulty.values()[count];
		}
		return null;
	}
	
	
	public static HashMap<Integer, Task> Tasks;
	
	public static void assignNewTask(UUID uuid) {
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			
			PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
			
			//chance of difficulty changes depending on streak
			int streak = 0;
			if(playerConfig.getString("Streak") != null)
				streak = playerConfig.getInt("Streak");
			
			int chance_medium = 0;
			int chance_hard = 0;
			int chance_elite = 0;
			
			if(streak >= 1 && streak < 5) {
				//1,2
				//first task is easy, 2nd and 3rd tasks, should get 1-2 medium
				//45% for easy
				chance_medium = 55; //55% for medium
				
			}
			if(streak >= 5 && streak < 10) {
				//6,7,8,9 tasks complete:
				//30% for easy
				chance_medium = 70; //50% for medium
				chance_hard = 20; //20% for hard
				
			} else if(streak >= 10) {
				//20% chance for easy
				chance_medium = 80; //36% chance for medium
				chance_hard = 44; //32% chance for hard
				chance_elite = 12; //12% chance for elite
			}
			
			Difficulty difficulty = Difficulty.EASY;
			
			//ensure task isn't repeated until at least another 'numberOfTasksBeforeRepeating' tasks complete
			List<Integer> lastCompletedTasks = new ArrayList<Integer>();
			if(playerConfig.getIntegerList("LastCompletedTasks") != null && playerConfig.getIntegerList("LastCompletedTasks").size() > 0)
				lastCompletedTasks = playerConfig.getIntegerList("LastCompletedTasks");
			
			int ID = -1;
			while(ID < 0 || lastCompletedTasks.contains(ID)) {
				List<Integer> tasksToChooseFrom = new ArrayList<Integer>();
				while(tasksToChooseFrom.size() == 0) { //in case no tasks exist, (eg. when testing, there are no hard and elite tasks)
					int random = (int) (Math.random() * 100) + 1; // 1-100
					if(random <= chance_elite) difficulty = Difficulty.ELITE;
					else if(random <= chance_hard) difficulty = Difficulty.HARD;
					else if(random <= chance_medium) difficulty = Difficulty.MEDIUM;
					else difficulty = Difficulty.EASY;
					
					tasksToChooseFrom = DeadMC.TaskFile.data().getIntegerList("List." + difficulty.ordinal());
				}
				
				int randomIntInList = (int) (Math.random() * tasksToChooseFrom.size()); //get random int from 0->size of list
				ID = tasksToChooseFrom.get(randomIntInList); //the new task ID to assign
			}
			
			playerConfig.set("Task", ID);
			playerConfig.set("Step", 0);
			playerConfig.set("Track", 0);
			playerConfig.save();
			
			//send chat message:
			Player player = Bukkit.getPlayer(uuid);
			if(player != null) {
				String taskName = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + ".Task name");
				int numberOfSteps = DeadMC.TaskFile.data().getInt("Tasks." + difficulty.ordinal() + "." + ID + ".Step count");
				String stepDescription = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + 0 + ".Step description");
				
				String difficultyString = ChatColor.GREEN + "EASY";
				if(difficulty == Difficulty.MEDIUM) difficultyString = ChatColor.YELLOW + "MEDIUM";
				if(difficulty == Difficulty.HARD) difficultyString = ChatColor.RED + "HARD";
				if(difficulty == Difficulty.ELITE) difficultyString = ChatColor.DARK_RED + "ELITE";
				
				String placeholder = Chat.getTitlePlaceholder(new String(taskName + " | Step " + 1 + " of " + (numberOfSteps + 1)), 44);
				
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + placeholder + " " + ChatColor.WHITE + "" + ChatColor.BOLD + taskName + ChatColor.WHITE + " | Step " + ChatColor.BOLD + 1 + ChatColor.WHITE + " of " + ChatColor.BOLD + (numberOfSteps + 1) + ChatColor.YELLOW + " " + placeholder);
				player.sendMessage(ChatColor.WHITE + "You have been assigned a new " + difficultyString + ChatColor.WHITE + " task!");
				player.sendMessage(ChatColor.BOLD + "NEW TASK: " + ChatColor.AQUA + stepDescription + ".");
				if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + 0 + ".Tips") != null)
					player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.WHITE + DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + 0 + ".Tips"));
				player.sendMessage("");
				if(numberOfSteps >= 1) {
					player.sendMessage(ChatColor.WHITE + " This task has " + ChatColor.BOLD + numberOfSteps + " more " + ChatColor.WHITE + "steps.");
					for(int step = 1; step <= numberOfSteps; step++)
						player.sendMessage(ChatColor.BLUE + "  Step " + ChatColor.BOLD + (step + 1) + ". " + ChatColor.WHITE + DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Step description"));
					player.sendMessage("");
				}
				player.sendMessage(ChatColor.WHITE + "Complete the task for a reward!");
				player.sendMessage("Or " + ChatColor.ITALIC + "" + ChatColor.BOLD + "skip" + ChatColor.WHITE + " the task with " + ChatColor.GOLD + "/task new" + ChatColor.WHITE + ".");
				player.sendMessage(ChatColor.YELLOW + "============================================");
				player.sendMessage("");
				
				String colourString = "&a";
				if(difficulty == Difficulty.MEDIUM) colourString = "&e";
				if(difficulty == Difficulty.HARD) colourString = "&c";
				if(difficulty == Difficulty.ELITE) colourString = "&4";
				
				Chat.sendBigTitle(uuid, "&6New task!", colourString + taskName + "&f | See the chat for info.");
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.BOLD + "NEW TASK: " + ChatColor.GREEN + stepDescription);
				
				//update placeholder
				if(PlaceHolders.player_TaskStreak.containsKey(uuid.toString()))
					PlaceHolders.player_TaskStreak.remove(uuid.toString());
				if(PlaceHolders.player_TaskString.containsKey(uuid.toString()))
					PlaceHolders.player_TaskString.remove(uuid.toString());
			}
		});
	}
	
	public static void track(UUID uuid, int amount) {

			PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
			
			if(playerConfig.getString("Task") == null) return;
			
			int ID = playerConfig.getInt("Task");
			int step = playerConfig.getInt("Step");
			
			int goal = DeadMC.TaskFile.data().getInt("Tasks." + GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".Amount");
			
			int newAmount = amount;
			if(playerConfig.getString("Track") != null)
				newAmount += playerConfig.getInt("Track");
			
			if(newAmount >= goal)
				stepTask(uuid);
			else {
				Bukkit.getScheduler().runTask(plugin, () -> {
					Player player = Bukkit.getPlayer(uuid);
					if(player != null)
						player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_CHIME, 1f, 8f);
				});
				playerConfig.set("Track", newAmount);
				playerConfig.save();
				Chat.sendTitleMessage(uuid, ChatColor.YELLOW + "[Task] " + ChatColor.WHITE + "Progress: " + ChatColor.GOLD + ChatColor.BOLD + newAmount + ChatColor.WHITE + " / " + ChatColor.BOLD + goal);
			}
			
			//update placeholder
			if(PlaceHolders.player_TaskStreak.containsKey(uuid.toString()))
				PlaceHolders.player_TaskStreak.remove(uuid.toString());

	}
	
	public static void stepTask(UUID uuid) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
			
			int ID = playerConfig.getInt("Task");
			int newStep = playerConfig.getInt("Step") + 1;
			
			playerConfig.set("Step", newStep);
			playerConfig.set("Track", null);
			//clear placed blocks
			playerConfig.set("TaskBlocksPlaced", null);
			playerConfig.save();
			
			if(ID == 0) { //is in tutorial
				if(newStep >= TaskManager.Tasks.get(ID).steps.size()) {
					completeTask(uuid);
				} else {
					//send message for step
					Player player = Bukkit.getPlayer(uuid);
					if(player != null) {
						Bukkit.getScheduler().runTask(plugin, () -> player.playSound(player.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 1f, 8f));
						
						String placeholder = Chat.getTitlePlaceholder(new String(TaskManager.Tasks.get(ID).name + " | Step " + (newStep + 1) + " of " + TaskManager.Tasks.get(ID).steps.size()), 44);
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + placeholder + " " + ChatColor.WHITE + "" + ChatColor.BOLD + TaskManager.Tasks.get(ID).name + ChatColor.WHITE + " | Step " + ChatColor.BOLD + (newStep + 1) + ChatColor.WHITE + " of " + ChatColor.BOLD + TaskManager.Tasks.get(0).steps.size() + ChatColor.YELLOW + " " + placeholder);
						player.sendMessage(ChatColor.BOLD + "NEW STEP: " + ChatColor.GREEN + TaskStep.values()[newStep].toString().charAt(0) + TaskStep.values()[newStep].toString().toLowerCase().replace("_", " ").substring(1, TaskStep.values()[newStep].toString().length()));
						player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.WHITE + TaskManager.Tasks.get(ID).steps.get(TaskStep.values()[newStep]));
						player.sendMessage("");
						player.sendMessage(ChatColor.ITALIC + "Complete the task for a reward!");
						player.sendMessage(ChatColor.YELLOW + "============================================");
						player.sendMessage("");
						
						Chat.sendBigTitle(uuid, "&6New task!", "&fSee the chat for info.");
						Chat.sendTitleMessage(uuid, ChatColor.BOLD + "NEW STEP: " + ChatColor.GREEN + TaskStep.values()[newStep].toString().charAt(0) + TaskStep.values()[newStep].toString().toLowerCase().replace("_", " ").substring(1, TaskStep.values()[newStep].toString().length()));
						
					}
				}
			} else if(playerConfig.getString("Task") != null) {
				Difficulty difficulty = GetDifficulty(ID);
				String taskName = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + ".Task name");
				int numberOfSteps = DeadMC.TaskFile.data().getInt("Tasks." + difficulty.ordinal() + "." + ID + ".Step count");
				
				if(newStep > numberOfSteps) {
					completeTask(uuid);
				} else {
					//send message for step
					Player player = Bukkit.getPlayer(uuid);
					if(player != null) {
						Bukkit.getScheduler().runTask(plugin, () -> player.playSound(player.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 1f, 8f));
						
						String stepDescription = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + newStep + ".Step description");
						
						String placeholder = Chat.getTitlePlaceholder(new String(taskName + " | Step " + (newStep + 1) + " of " + (numberOfSteps + 1)), 44);
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + placeholder + " " + ChatColor.WHITE + "" + ChatColor.BOLD + taskName + ChatColor.WHITE + " | Step " + ChatColor.BOLD + (newStep + 1) + ChatColor.WHITE + " of " + ChatColor.BOLD + (numberOfSteps + 1) + ChatColor.YELLOW + " " + placeholder);
						player.sendMessage(ChatColor.BOLD + "NEW STEP: " + ChatColor.AQUA + stepDescription + ".");
						if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + newStep + ".Tips") != null)
							player.sendMessage(ChatColor.YELLOW + " - " + ChatColor.WHITE + DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + newStep + ".Tips"));
						player.sendMessage("");
						player.sendMessage(ChatColor.WHITE + "Complete the task for a reward!");
						player.sendMessage("Or " + ChatColor.ITALIC + "skip" + ChatColor.WHITE + " the task with " + ChatColor.GOLD + "/task new" + ChatColor.WHITE + ".");
						player.sendMessage(ChatColor.YELLOW + "============================================");
						player.sendMessage("");
						
						Chat.sendBigTitle(uuid, "&aNew step!", "&fSee the chat for info.");
						Chat.sendTitleMessage(uuid, ChatColor.BOLD + "NEW STEP: " + ChatColor.GREEN + stepDescription);
						
					}
				}
			}
			
			//update placeholders
			if(PlaceHolders.player_TaskStreak.containsKey(uuid.toString()))
				PlaceHolders.player_TaskStreak.remove(uuid.toString());
			if(PlaceHolders.player_TaskString.containsKey(uuid.toString()))
				PlaceHolders.player_TaskString.remove(uuid.toString());
	}

	private static int numberOfTasksBeforeRepeating = 15;
	
	public static void completeTask(UUID uuid) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
			if(playerConfig.getString("Task") != null) {
				
				Player player = Bukkit.getPlayer(uuid);
				Bukkit.getScheduler().runTask(plugin, () -> player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1f, 8f));
				
				if(playerConfig.getInt("Task") == 0) {
					//send message for end task
					Chat.sendBigTitle(uuid, "&aTutorial complete!", "&fSee the chat for info.");
					Chat.sendTitleMessage(uuid, ChatColor.GREEN + "" + ChatColor.BOLD + "TASK COMPLETE!");
					
					Chat.broadcastMessage(ChatColor.YELLOW + "[Achievement] " + ChatColor.AQUA + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " completed the tutorial!");
					player.sendMessage("");
					player.sendMessage(ChatColor.YELLOW + "[Task] " + ChatColor.GREEN + "Tutorial complete! Congratulations!");
					player.sendMessage(ChatColor.YELLOW + "[Task] " + ChatColor.WHITE + "To continue learning, explore the " + ChatColor.GOLD + "/help" + ChatColor.WHITE + " command.");
					player.sendMessage("");
					Bukkit.getScheduler().runTask(plugin, () -> {
						for(int count = 0; count < 5; count++) {
							CustomItems.giveItemSafely(uuid, CustomItems.votingKey());
						}
						player.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.GOLD + "You have been rewarded with 5 crate keys!");
						player.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.WHITE + "Redeem these at the Voting Crate in " + ChatColor.GOLD + "/longdale" + ChatColor.WHITE + ".");
						player.sendMessage("");
						
						player.closeInventory();
					});
				} else {
					
					int newStreak = 1;
					if(playerConfig.getString("Streak") != null)
						newStreak += playerConfig.getInt("Streak");
					
					int ID = playerConfig.getInt("Task");
					Difficulty difficulty = GetDifficulty(ID);
					String taskName = DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + ".Task name");
					
					//send message for end task
					String colourString = "&2";
					if(difficulty == Difficulty.MEDIUM) colourString = "&e";
					if(difficulty == Difficulty.HARD) colourString = "&c";
					if(difficulty == Difficulty.ELITE) colourString = "&4";
					
					Chat.sendBigTitle(uuid, "&aTask complete!", colourString + taskName + "&f | See the chat for info.");
					Chat.sendTitleMessage(uuid, ChatColor.GREEN + "" + ChatColor.BOLD + "TASK COMPLETE!");
					
					player.sendMessage("");
					player.sendMessage(ChatColor.YELLOW + "============================================");
					player.sendMessage(ChatColor.GREEN + "Task " + ChatColor.AQUA + ChatColor.BOLD + taskName + ChatColor.GREEN + " complete! Congratulations!");
					player.sendMessage(ChatColor.WHITE + "  > You have a streak of " + ChatColor.BOLD + newStreak + ChatColor.WHITE + " tasks complete.");
					player.sendMessage("");
					player.sendMessage(ChatColor.WHITE + "Start a new task with " + ChatColor.GOLD + "/task new" + ChatColor.WHITE + ".");
					player.sendMessage("");
					
					int streak = newStreak - 1;
					double multiplier = 1;
					if(streak > 0) {
						//apply multiplier
						multiplier = GetStreakMultiplier(streak);
						
						DecimalFormat formatter = new DecimalFormat("#.##");
						formatter.setRoundingMode(RoundingMode.UP);
						player.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.WHITE + "Applied streak multiplier of " + ChatColor.BOLD + formatter.format(multiplier) + ChatColor.WHITE + "x.");
					}
					
					int reward = 0;
					if(difficulty == Difficulty.EASY)
						reward = (int) (Math.random() * ((125 - 75) + 1)) + 75; //75-125
					if(difficulty == Difficulty.MEDIUM)
						reward = (int) (Math.random() * ((250 - 150) + 1)) + 150; //150-250
					if(difficulty == Difficulty.HARD)
						reward = (int) (Math.random() * ((650 - 400) + 1)) + 400; //400-650
					if(difficulty == Difficulty.ELITE)
						reward = (int) (Math.random() * ((1000 - 700) + 1)) + 700; //700-1000
					reward *= (int) multiplier;
					
					Economy.giveCoins(uuid, reward);
					
					player.sendMessage(ChatColor.YELLOW + "[Reward] " + ChatColor.GOLD + "You have been rewarded with " + ChatColor.BOLD + Economy.convertCoins(reward) + ChatColor.GOLD + "!");
					player.sendMessage(ChatColor.YELLOW + "============================================");
					player.sendMessage("");
					
					//send global message
					if(difficulty == Difficulty.HARD) {
						Chat.broadcastMessage(ChatColor.YELLOW + "[Achievement] " + ChatColor.AQUA + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " completed a " + ChatColor.RED + ChatColor.BOLD + "HARD" + ChatColor.WHITE + " task and received " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(reward) + ChatColor.WHITE + "!");
					}
					if(difficulty == Difficulty.ELITE) {
						Chat.broadcastMessage(ChatColor.YELLOW + "[Achievement] " + ChatColor.AQUA + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " completed an " + ChatColor.RED + ChatColor.BOLD + "ELITE" + ChatColor.WHITE + " task and received " + ChatColor.GOLD + ChatColor.BOLD + Economy.convertCoins(reward) + ChatColor.WHITE + "!");
					}
					if(newStreak == 3 || newStreak % 5 == 0) {
						Chat.broadcastMessage(ChatColor.YELLOW + "[Achievement] " + ChatColor.AQUA + ChatColor.BOLD + player.getName() + ChatColor.WHITE + " has a " + ChatColor.ITALIC + "task streak" + ChatColor.WHITE + " of " + ChatColor.GOLD + ChatColor.BOLD + newStreak + ChatColor.WHITE + "!");
					}
					
					if(newStreak == 1) {
						player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + "You have unlocked " + ChatColor.YELLOW + ChatColor.BOLD + "MEDIUM" + ChatColor.WHITE + " tasks.");
					}
					if(newStreak == 5) {
						player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + "You have unlocked " + ChatColor.RED + ChatColor.BOLD + "HARD" + ChatColor.WHITE + " tasks.");
					}
					if(newStreak == 10) {
						player.sendMessage(ChatColor.YELLOW + "[Tasks] " + ChatColor.WHITE + "You have unlocked " + ChatColor.DARK_RED + ChatColor.BOLD + "ELITE" + ChatColor.WHITE + " tasks.");
					}
					
					//ensure task isn't repeated until at least another 'numberOfTasksBeforeRepeating' tasks complete
					List<Integer> lastCompletedTasks = new ArrayList<Integer>();
					if(playerConfig.getIntegerList("LastCompletedTasks") != null && playerConfig.getIntegerList("LastCompletedTasks").size() > 0)
						lastCompletedTasks = playerConfig.getIntegerList("LastCompletedTasks");
					//pop on task
					lastCompletedTasks.add(0, ID);
					//if size of list > numberOfTasksBeforeRepeating, remove fifth element
					if(lastCompletedTasks.size() > numberOfTasksBeforeRepeating)
						lastCompletedTasks.remove(numberOfTasksBeforeRepeating);
					playerConfig.set("LastCompletedTasks", lastCompletedTasks);
					
					playerConfig.set("Streak", newStreak);
					
					//stats tracking for /lookup and leaderboards:
					//max streak:
					if(playerConfig.getString("MaxStreak") == null
							|| newStreak > playerConfig.getInt("MaxStreak")) {
						playerConfig.set("MaxStreak", newStreak);
						Chat.updateLeaderBoard(Leaderboard.STREAK, uuid);
					}
					//tasks completed:
					int tasksComplete = 0;
					if(playerConfig.getString("TasksCompleted") != null)
						tasksComplete = playerConfig.getInt("TasksCompleted");
					playerConfig.set("TasksCompleted", new Integer(tasksComplete + 1));
					Chat.updateLeaderBoard(Leaderboard.TASKS, uuid);
				}
				
				//clear placed blocks
				playerConfig.set("TaskBlocksPlaced", null);
				playerConfig.set("Task", null);
				playerConfig.set("Step", null);
				playerConfig.set("Track", null);
				playerConfig.save();
				
				//update placeholder
				if(PlaceHolders.player_TaskString.containsKey(uuid.toString()))
					PlaceHolders.player_TaskString.remove(uuid.toString());
				
			}
	}
	
	public static Boolean playerHasStep(UUID uuid, TaskType type) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		if(playerConfig.getString("Task") != null && playerConfig.getInt("Task") != 0) {
			int ID = playerConfig.getInt("Task");
			int step = playerConfig.getInt("Step");
			if(DeadMC.TaskFile.data().getInt("Tasks." + GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".Type") == type.ordinal())
				return true;
		}
		
		return false;		 
	}
	
	public static Boolean playerHasStep_TUTORIAL(UUID uuid, TaskStep step) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		
		if(playerConfig.getString("Task") != null
				&& playerConfig.getInt("Task") == 0
				&& playerConfig.getInt("Step") == step.ordinal())
			return true;
		
		return false;		 
	}
	
	private Difficulty getDifficultyFromString(String string) {
		if(Chat.isInteger(string) && Integer.parseInt(string) <= 3) {
			return Difficulty.values()[Integer.parseInt(string)];
		} else {
			try {
				return Difficulty.valueOf(string.toUpperCase());
			} catch(IllegalArgumentException e) { }
		}
		return null; //return null if not a difficulty
	}

}
