package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Admin.StaffRank;
import com.deadmc.DeadAPImaven.Towns.Town;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Restarter implements CommandExecutor, Listener {
	private static DeadMC plugin;
	public Restarter(DeadMC p) {
		plugin = p;
	}

	public static int CalculateMinimumPlayerTrigger() {
		int total = 0;
		for(int day = 1; day <= 7; day++) {
			//1-7
			try {
				total += DeadMC.RestartFile.data().getInt("History." + day);
				Chat.debug("Adding " + DeadMC.RestartFile.data().getInt("History." + day) + " (total = " + total + ")");
			} catch(NullPointerException e) {
				return 10; //not enough data
			}
		}
		
		Chat.debug("Final total = " + total + " / 7.0 = " + (total/7.0) + " * 1.4 = " + ((total/7.0) * 1.4) + " rounded = " + (int) Math.ceil((total/7.0) * 1.4));
		return (int) Math.ceil((total/7.0) * 1.4); //40% buffer from average of least players online
	}

	public static int minimumPlayerTrigger = 10;
	public static boolean inMaintenance = false;
	public static long LAST_START_TIME;
	private int minHoursBetweenRestarts = 30; //the min amount of time between restarts
	
	private static int timerMax = 300; //in seconds, the max warning time - 5 minutes
	public static boolean timerHasStarted = false;
	private static int timeUntilRestart = 0; //in seconds
	
	private static ArrayList<Integer> warningTimes = new ArrayList<Integer>(Arrays.asList(
			900, 600, 300, 120, 60, 30, 10, 5, 4, 3, 2, 1)); //in seconds
	private static ArrayList<Integer> discordWarningTimes = new ArrayList<Integer>(Arrays.asList(
			900, 600, 300, 120, 60, 30, 10)); //in seconds
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		if(commandLabel.equalsIgnoreCase("restarter")) {
			if(args.length == 0) {
				if(sender instanceof Player) {
					Player player = (Player) sender;
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					if(player.isOp() || (playerConfig.getString("StaffRank") != null && playerConfig.getInt("StaffRank") >= StaffRank.ADMINISTRATOR.ordinal())) {
						DecimalFormat df = new DecimalFormat("####0.00");
						double oneHour = 3600000.0;
						long uptime = System.currentTimeMillis() - LAST_START_TIME; // in milliseconds
						double timeInHours = uptime / oneHour; //convert to hours
						
						long lastClean = System.currentTimeMillis() - DeadMC.RestartFile.data().getLong("DataCleaner.Previous.Time"); // in milliseconds
						double lastClean_timeInHours = lastClean / oneHour; //convert to hours
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "=========================================");
						player.sendMessage(ChatColor.GOLD + "/restarter force-restart <warning-time>" + ChatColor.WHITE + " - This will restart the server and run the data-cleaner to clear old players and chunks.");
						player.sendMessage(ChatColor.GOLD + "/restarter force-maintenance <warning-time>" + ChatColor.WHITE + " - This will kick all players off until the server is restarted again. It will also run the data-cleaner.");
						
						player.sendMessage("");
						if(DeadMC.RestartFile.data().getString("DataCleaner.Previous.NullRegions") != null) {
							player.sendMessage(ChatColor.RED + "[DataCleaner] " + "Previous clean: (" + df.format(timeInHours) + " hours ago)");
							player.sendMessage(" - Null files removed: " + ChatColor.BOLD + DeadMC.RestartFile.data().getString("DataCleaner.Previous.NullFiles"));
							player.sendMessage(" - Null regions removed: " + ChatColor.BOLD + DeadMC.RestartFile.data().getString("DataCleaner.Previous.NullRegions"));
							player.sendMessage(" - Old players turned: " + ChatColor.BOLD + DeadMC.RestartFile.data().getString("DataCleaner.Previous.Players"));
							player.sendMessage(" - Old IPs untracked: " + ChatColor.BOLD + DeadMC.RestartFile.data().getString("DataCleaner.Previous.IPs"));
							player.sendMessage(" - Old world regions removed: " + ChatColor.BOLD + DeadMC.RestartFile.data().getString("DataCleaner.Previous.Chunks"));
						}
						player.sendMessage("");
						player.sendMessage(ChatColor.RED + "[Restarter] " + ChatColor.WHITE + "Time since last restart: " + ChatColor.BOLD + df.format(timeInHours) + ChatColor.WHITE + " hours");
						player.sendMessage(ChatColor.RED + "[Restarter] " + ChatColor.WHITE + "Minimum player trigger: " + ChatColor.BOLD + minimumPlayerTrigger);
						player.sendMessage(ChatColor.YELLOW + "=========================================");
						player.sendMessage("");
					}
				} else {
					Chat.broadcastMessage("[Restarter] Use /restarter force-restart/force-maintenance <warning time in seconds> <leave null to not backup>", true);
					return true;
				}
			} else {
				if(args[0].equalsIgnoreCase("force-restart") || args[0].equalsIgnoreCase("force-maintenance")) {
					boolean backup = false;
					int time = timerMax;
					if(args.length == 2 && Chat.isInteger(args[1]))
						time = Integer.parseInt(args[1]);
					if(args.length == 3) backup = true;
					if(!timerHasStarted) {
						boolean restart = args[0].equalsIgnoreCase("force-restart");
						StartTimer(backup, time, restart);
					} else {
						Chat.broadcastMessage(ChatColor.RED + "[DeadMC] There is already a restart scheduled.", true);
					}
				}
				if(args[0].equalsIgnoreCase("cancel")) {
					if(!timerHasStarted) {
						Chat.broadcastMessage(ChatColor.RED + "[DeadMC] Nothing to cancel.");
					} else {
						CancelTimer();
					}
				}
			}
		}
		return true;
	}
	
	//on player login:
	@EventHandler
	public void onPlayerLogin(AsyncPlayerPreLoginEvent event) {
		if(inMaintenance) {
			event.disallow(Result.KICK_OTHER, "Server undergoing maintenance. We'll be back shortly!");
		}
	}
	//on player logout:
	@EventHandler
	public void onPlayerLogout(PlayerQuitEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			long oneHour = 3600000L;
			long uptime = System.currentTimeMillis() - LAST_START_TIME; // in milliseconds
			
			//track minimum player history
			if(uptime > (oneHour*2)) {
				if(DeadMC.RestartFile.data().getString("Last tracked") == null
						|| DateCode.getDaysSince(DeadMC.RestartFile.data().getString("Last tracked")) >= 1) {
					//new day, or never been tracked
					ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
					DeadMC.RestartFile.data().set("Last tracked", DateCode.Encode(adelaideTime, true, false, false, true, false, false));
					
					for(int day = 7; day >= 0; day--) {
						//0 is current
						//7 goes null
						//6 goes to 7
						//...
						
						if(day == 0)
							DeadMC.RestartFile.data().set("History." + (int)day, Bukkit.getOnlinePlayers().size());
						else {
							DeadMC.RestartFile.data().set("History." + (int)day, DeadMC.RestartFile.data().getInt("History." + (day-1)));
						}
					}
					DeadMC.RestartFile.save();
					
				} else if(Bukkit.getOnlinePlayers().size() < DeadMC.RestartFile.data().getInt("History." + 0)) {
					DeadMC.RestartFile.data().set("History." + (int)0, Bukkit.getOnlinePlayers().size());
					DeadMC.RestartFile.save();
				}
				
			}
			
			long time = (Bukkit.getWorld("world").getTime() + (timerMax*20));
			boolean isDayAtEndOfTimer = time < BloodMoon.nightStartTime || time > BloodMoon.lengthOfDay;
			if(!timerHasStarted
					&& uptime > (oneHour*minHoursBetweenRestarts)
					&& Bukkit.getOnlinePlayers().size() <= minimumPlayerTrigger
					&& isDayAtEndOfTimer) {
				//also check if it will be day time in timerMax time
				
				StartTimer(true, timerMax, true);
			}
		});
	}
	
	public static void StartTimer(boolean backupFirst, int time, boolean runRestartProcess) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			String type = runRestartProcess ? "reload" : "undergo maintenance";
			
			if(backupFirst) {
				//do backup first, wait for completion
				Backup.createBackup(Backup.BackupType.PLUGINS, Backup.BackupReason.PRE_RESTART);
				
				// wait until backup is finished and will be day time
				while(Backup.isInBackup.get() || ((Bukkit.getWorld("world").getTime() + (time*20)) > BloodMoon.nightStartTime && (Bukkit.getWorld("world").getTime() + (time*20)) < (BloodMoon.lengthOfDay+1000))) {
					Chat.debug("Delaying " + Backup.isInBackup.get() + " " + ((Bukkit.getWorld("world").getTime() + (time*20)) > BloodMoon.nightStartTime) + " " + ((Bukkit.getWorld("world").getTime() + (time*20)) < (BloodMoon.lengthOfDay+1000)), "restarter");
					Tools.sleepThread(3000); //wait 3 seconds to check again
				}
			}
			if(!timerHasStarted) {
				//dont run if already running
				timerHasStarted = true;
				timeUntilRestart = time;
				
				//send an initial message:
				int timeUntilRestartMinutes = (int)Math.ceil(timeUntilRestart/60.0);
				String timeUntilRestartString = timeUntilRestart < 60 ? timeUntilRestart + " second" + (timeUntilRestart == 1 ? "" : "s") : timeUntilRestartMinutes + " minute" + (timeUntilRestartMinutes == 1 ? "" : "s");
				if(!warningTimes.contains(timeUntilRestart)) {
					//display warning to all players
					Chat.broadcastMessage(ChatColor.RED + "[Warning] The server will " + type + " in " + timeUntilRestartString + ".");
				}
				if(!discordWarningTimes.contains(timeUntilRestart)) {
					String icon = runRestartProcess ? ":arrows_counterclockwise:" : ":tools:";
					Chat.broadcastDiscord( icon + " **The server will " + type + " in " + timeUntilRestartString + ".**"); //send the message to discord
				}
				
				while(timerHasStarted && timeUntilRestart >= 0) {
					//end
					if(timeUntilRestart == 0) {
						Chat.debug(ChatColor.RED + "Removing Discord chat permissions!");
						DiscordSRVListener.allowChat(false);
						
						if(runRestartProcess) BeginRestartProcess();
						else {
							Chat.broadcastDiscord( ":no_entry_sign: **The server is offline for maintenance.**"); //send the message to discord
							//kick all players
							for(Player player : Bukkit.getOnlinePlayers()) {
								if(player != null && !player.isOp()) {
									Bukkit.getScheduler().runTask(plugin, () -> player.kickPlayer("Server undergoing maintenance. We'll be back shortly!"));
								}
							}
							Chat.broadcastMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "All players kicked! Stop the server when ready.", true);
							//set in maintenance mode to stop players coming on:
							inMaintenance = true;
						}
					}
					
					timeUntilRestartMinutes = (int)Math.ceil(timeUntilRestart/60.0);
					timeUntilRestartString = timeUntilRestart < 60 ? timeUntilRestart + " second" + (timeUntilRestart == 1 ? "" : "s") : timeUntilRestartMinutes + " minute" + (timeUntilRestartMinutes == 1 ? "" : "s");
					
					if(warningTimes.contains(timeUntilRestart)) {
						//display warning to all players
						Chat.broadcastMessage(ChatColor.RED + "[Warning] The server will " + type + " in " + timeUntilRestartString + ".");
					}
					if(discordWarningTimes.contains(timeUntilRestart)) {
						String icon = runRestartProcess ? ":arrows_counterclockwise:" : ":tools:";
						Chat.broadcastDiscord( icon + " **The server will " + type + " in " + timeUntilRestartString + ".**"); //send the message to discord
					}
					
					//update bar for everyone
					for(Player player : Bukkit.getOnlinePlayers()) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.RED + "[Maintenance] Server " + type + "ing in " + timeUntilRestartString + ".");
					}
					
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					timeUntilRestart--;
				}
			}
		});
	}
	
	public static void CancelTimer() {
		timerHasStarted = false;
		timeUntilRestart = 0;
		Chat.broadcastMessage(ChatColor.RED + "[DeadMC] The restart has been cancelled.");
	}
	
	private static void BeginRestartProcess() {
		Chat.broadcastDiscord( ":no_entry_sign: **The server is reloading.** We'll be back shortly!"); //send the message to discord
		
		Chat.broadcastDiscord(":arrows_counterclockwise: **The server is running the clean...**", true);
		
		//update files if neccessary
		try {
			File updatesDirectory = new File(Bukkit.getWorldContainer(), "plugins" + File.separator + "updates");
			Restarter.UpdateFiles(updatesDirectory);
		} catch(Exception e) {
			Chat.logError("[FileUpdater] There was an error! " + e.getMessage());
		}
		
		//set in maintenance mode
		inMaintenance = true;
		DeadMC.RestartFile.data().set("Maintenance", true);
		DeadMC.RestartFile.save();
		
		Bukkit.spigot().restart();
	}
	
	public static void UpdateFiles(File directory) {
		if(directory == null) {
			Chat.debug("[FileUpdater] No needed file updates found in " + directory.getName() + ".");
			return;
		}
		
		File[] updateFiles = directory.listFiles();
		if(updateFiles == null) {
			Chat.debug("[FileUpdater] No needed file updates found in " + directory.getName() + ".");
			return;
		}
		
		Chat.debug("[FileUpdater] " + updateFiles.length + " files found in " + directory.getName() + ".");
		
		for(File updateFile : updateFiles) {
			Chat.debug("[FileUpdater] Checking " + updateFile.getAbsolutePath() + "...");
			if(updateFile.isDirectory()) {
				Chat.debug("Is directory");
				UpdateFiles(updateFile);
				Chat.debug("Finished " + updateFile.getName());
				updateFile.delete(); //delete empty folder
			} else {
				Chat.debug("Is file");
				File oldFile = new File(updateFile.getAbsolutePath().replace("updates" + File.separator, ""));
				if(updateFile.getName().contains(".jar")) {
					Chat.broadcastDiscord(":floppy_disk: [FileUpdater] Updating **" + updateFile.getName() + "** through Paper's methods (is a JAR).", true);
					continue;
				}
				try {
					Files.move(updateFile.toPath(), oldFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
					Chat.debug("Moved successfully");
					Chat.broadcastDiscord(":floppy_disk: Successfully updated " + updateFile.getName() + ".", true);
				} catch(Exception e) {
					Chat.logError("[FileUpdater] There was an error **moving** " + updateFile.getAbsolutePath() + "!\n" + e.getMessage());
				}
			}
		}
	}
	
	public static BufferedWriter logWriter = null;
	
	public static void writeToLog(String string) {
		if(logWriter == null) return;
		try {
			logWriter.append(string + "\n");
		} catch(Exception e) {
			Chat.logError(e);
		}
	}
	
	public static void ClearOldData() {
		//all of this can be run async, and outputs messages in console
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			Bukkit.getConsoleSender().sendMessage("");
			
			long startTime = System.currentTimeMillis();
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy @ HHmm");
			String date = formatter.format(new Date());
			try {
				File logFile = new File(Bukkit.getWorldContainer(), "data clean logs" + File.separator + date + ".txt");
				logWriter = new BufferedWriter(new FileWriter(logFile, true));
			} catch(Exception e) {
				Chat.logError(e);
			}
			
			try {
				Players.turnIPs();
			} catch(Exception e) { Chat.logError(":head_bandage: **Error turning IPs!** " + e.getMessage(), e.getStackTrace(), null); }
			try {
				Players.turnPlayers();
			} catch(Exception e) { Chat.logError(":head_bandage: **Error turning players!** " + e.getMessage(), e.getStackTrace(), null); }
			try {
				Admin.RemoveNullData();
			} catch(Exception e) { Chat.logError(":head_bandage: **Error removing null data!** " + e.getMessage(), e.getStackTrace(), null); }
			try {
				Admin.RemoveNullRegions();
			} catch(Exception e) { Chat.logError(":head_bandage: **Error removing null regions!** " + e.getMessage(), e.getStackTrace(), null); }
			try {
				Admin.ClearOldChunks(); //will also remove undefined regions
			} catch(Exception e) { Chat.logError(":head_bandage: **Error cleaning old chunks!** " + e.getMessage(), e.getStackTrace(), null); }
			
			//only turn discord channels on main server (in case of testing etc.)
			if(DeadMC.RestartFile.data().getString("IsDevelopmentServer") == null) {
				try {
					Town.turnDiscordChannels();
				} catch(Exception e) { Chat.logError(":head_bandage: **Error turning town discords!** " + e.getMessage(), e.getStackTrace(), null); }
				try {
					Nation.turnDiscordChannels();
				} catch(Exception e) { Chat.logError(":head_bandage: **Error turning nation discords!** " + e.getMessage(), e.getStackTrace(), null); }
			}
			
			try {
				logWriter.close();
			} catch(Exception e) {
				Chat.logError(e);
			}
			
			long finishTime = System.currentTimeMillis();
			
			DecimalFormat df = new DecimalFormat("####0.00");
			double timeInSeconds = (finishTime-startTime) / 1000.0; //convert to seconds
			Bukkit.getConsoleSender().sendMessage("[Data-Clean] Total data-clean time taken: " + df.format(timeInSeconds) + " seconds");
			Bukkit.getConsoleSender().sendMessage("");
			Chat.broadcastDiscord(":tools: Finished! Clean took " + df.format(timeInSeconds) + " seconds.", true);
			
			DeadMC.RestartFile.data().set("DataCleaner.Previous.Time", System.currentTimeMillis());
			
			//wait 60 seconds to ensure server will restart from 'crashed' state
			int waiting = 65 - (int)Math.floor(timeInSeconds); //70 seconds - total time taken
			if(waiting > 1) {
				Bukkit.getConsoleSender().sendMessage("[Restarter] Waiting " + df.format(waiting) + " until restart");
				try {
					TimeUnit.SECONDS.sleep(waiting);
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			DeadMC.RestartFile.data().set("Maintenance", null);
			DeadMC.RestartFile.save();
			
			try {
				Chat.broadcastDiscord( ":pinching_hand: **Nearly there! Coming back online now.**"); //send the message to discord
				TimeUnit.SECONDS.sleep(5);
				//ensure message sends first
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			Bukkit.spigot().restart();
		});
	}
	
}
