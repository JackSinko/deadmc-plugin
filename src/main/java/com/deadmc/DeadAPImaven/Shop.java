package com.deadmc.DeadAPImaven;

import org.bukkit.Location;
import org.bukkit.Material;

public class Shop {

	public enum ShopType {
		PLAYER,
		MARKET
	}
	
	public ShopType type;
	public int stock;
	public Location location;
	public String travelPoint;
	public int buyPrice;
	public int sellPrice;
	public String regionName;
	public Material itemType = null;
	public boolean isDonatorItem = false;
	
	public Shop(ShopType type, String pointName, Location location) {
		this.type = type;
		this.location = location;
		this.travelPoint = pointName;
	}
	
	public Shop(ShopType type, Location location, int stock, String pointName, int buyPrice, int sellPrice) {
		this.type = type;
		this.location = location;
		this.stock = stock;
		this.travelPoint = pointName;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	public Shop(ShopType type, Location location, int stock, String pointName, int buyPrice, int sellPrice, String regionName) {
		this.type = type;
		this.location = location;
		this.stock = stock;
		this.travelPoint = pointName;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
		this.regionName = regionName;
	}
	
	public Shop(ShopType type, Location location, int stock, String pointName, int buyPrice, int sellPrice, Material itemType, boolean isDonatorItem) {
		this.type = type;
		this.location = location;
		this.stock = stock;
		this.travelPoint = pointName;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
		this.itemType = itemType;
		this.isDonatorItem = isDonatorItem;
	}
	
}
