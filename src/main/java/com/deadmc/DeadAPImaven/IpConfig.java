package com.deadmc.DeadAPImaven;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class IpConfig extends YamlConfiguration {
	
	public static Map<String, IpConfig> configs = new HashMap<String, IpConfig>();
	
	public static IpConfig getConfig(String ip) {
		synchronized(configs) { //'synchronized' allows only one thread to access configs at a time
			
			if(configs.containsKey(ip))
				return configs.get(ip);

			IpConfig config = new IpConfig(ip);
			configs.put(ip, config);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[IpConfig] " + ChatColor.RESET + "Added " + ip + " - " + configs.size());
			
			return config;
			
		}
	}
	
	private File file = null;
	private Object saveLock = new Object();
	private String ip;
	
	public IpConfig(String ip) {
		super(); //'super' calls parent class constructor (YamlConfiguration)
		file = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "ips" + File.separator + ip + ".yml");
		this.ip = ip;
		reload();
	}
	
	@SuppressWarnings("unused")
	private IpConfig() {
		ip = null;
	}
	
	private void reload() {
		synchronized(saveLock) {
			try {
				load(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void save() {
		synchronized(saveLock) {
			try {
				save(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void discard() {
		synchronized(configs) {
			configs.remove(ip);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[IpConfig] " + ChatColor.RESET + "Removed " + ip + " - " + configs.size());
		}
	}
	
}