package com.deadmc.DeadAPImaven;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import com.deadmc.DeadAPImaven.Chat.Leaderboard;

public class Economy implements CommandExecutor {
	private DeadMC plugin;
	
	public Economy(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	List<String> confirmPay = new ArrayList<String>();
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		Player player = null;
		if(sender instanceof Player)
			player = (Player) sender;
		
		if(commandLabel.equalsIgnoreCase("money") || commandLabel.equalsIgnoreCase("coins") || commandLabel.equalsIgnoreCase("bars") || commandLabel.equalsIgnoreCase("balance") || commandLabel.equalsIgnoreCase("bal")) {
			String playerName = player.getName();
			UUID playerUUID = player.getUniqueId();
			if(args.length > 0) {
				OfflinePlayer playerToLookup = Bukkit.getOfflinePlayer(args[0]);
				if(DeadMC.PlayerFile.data().getStringList("Active").contains(playerToLookup.getUniqueId().toString())) {
					playerUUID = playerToLookup.getUniqueId();
					playerName = playerToLookup.getName();
				} else {
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GOLD + args[0] + ChatColor.WHITE + " is not an existing player.");
					return true;
				}
			}
			PlayerConfig playerConfig = PlayerConfig.getConfig(playerUUID);
			player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.AQUA + playerName + ChatColor.WHITE + " has " + ChatColor.GOLD + convertCoins(playerConfig.getInt("Coins")) + ChatColor.WHITE + ".");
		}
		
		if(commandLabel.equalsIgnoreCase("pay")) {
			if(args.length > 1) {
				if(Chat.isInteger(args[1]) && Integer.parseInt(args[1]) > 0) {
					
					if(DeadMC.PlayerFile.data().getStringList("Active").contains(Bukkit.getOfflinePlayer(args[0]).getUniqueId().toString())) {
						if(!player.getName().equalsIgnoreCase(args[0])) {
							String playerName = args[0];
							
							PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
							int coins = Integer.parseInt(args[1]);
							
							if(coins <= playerConfig.getInt("Coins")) {
								
								if(confirmPay.contains(player.getName())) {
									confirmPay.remove(player.getName());
									
									Economy.takeCoins(player.getUniqueId(), coins);
									Economy.giveCoins(Bukkit.getOfflinePlayer(playerName).getUniqueId(), coins);
									
									player.sendMessage(ChatColor.YELLOW + "[Pay] " + ChatColor.GREEN + "Gave " + ChatColor.GOLD + convertCoins(coins) + ChatColor.GREEN + " to " + playerName + "'s funds.");
									
									if(Bukkit.getPlayer(playerName) != null) {
										Bukkit.getPlayer(playerName).sendMessage(ChatColor.YELLOW + "[Pay] " + ChatColor.GREEN + player.getName() + " paid " + ChatColor.GOLD + convertCoins(coins) + ChatColor.GREEN + " to your funds.");
									}
								} else {
									
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Pay] " + ChatColor.RED + "Are you sure you want to give " + ChatColor.GOLD + convertCoins(coins) + ChatColor.RED + " to " + playerName + "?");
									player.sendMessage(ChatColor.YELLOW + "[Pay] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/pay " + playerName + " " + coins + ChatColor.WHITE + " again to confirm.");
									player.sendMessage(ChatColor.YELLOW + "[Pay] " + ChatColor.WHITE + "Confirmation session will expire in 15 seconds");
									player.sendMessage("");
									
									confirmPay.add(player.getName());
									Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
										@Override
										public void run() {
											Player player = null;
											if((sender instanceof Player)) {
												player = (Player) sender;
											}
											
											if(confirmPay.contains(player.getName())) {
												player.sendMessage(ChatColor.YELLOW + "[Pay] " + ChatColor.RED + "Confirmation session expired.");
												confirmPay.remove(player.getName());
											}
											
										}
									}, 300L);
									
								}
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "You don't have " + ChatColor.GOLD + convertCoins(coins) + ChatColor.WHITE + "!");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.RED + "You can't pay yourself.");
					} else
						player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "'" + args[0] + "' is not an existing player.");
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/pay <player> <AMOUNT OF COINS>" + ChatColor.WHITE + ".");
			} else
				player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/pay <player> <amount>" + ChatColor.WHITE + ".");
		}
		
		return true;
	}
	
	public static String convertCoins(int coins) {
		//int goldBars = new Integer(coins/100);
		//int goldCoins = new Integer(coins - (100*goldBars));
		//return new String(goldBars + "b" + goldCoins + "c");
		DecimalFormat formatter = new DecimalFormat("#,###");
		return new String(formatter.format(coins) + " coins");
	}
	
	public static void giveCoins(UUID uuid, int amount) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(uuid).getUniqueId());
		int newBalance = new Integer(playerConfig.getInt("Coins") + amount);
		playerConfig.set("Coins", newBalance);
		playerConfig.save();
		
		//update placeholder data
		if(PlaceHolders.playersCoins.containsKey(uuid.toString()))
			PlaceHolders.playersCoins.replace(uuid.toString(), newBalance);
		else PlaceHolders.playersCoins.put(uuid.toString(), newBalance);
		
		Chat.updateLeaderBoard(Leaderboard.COINS, uuid);
		
		Chat.sendTitleMessage(uuid, ChatColor.GREEN + "Awarded " + ChatColor.GOLD + "" + ChatColor.BOLD + convertCoins(amount));
		
		PlaceHolders.totalCoinsInCirculation += amount; //update stat
	}
	
	public static void takeCoins(UUID uuid, int amount) {
		PlayerConfig playerConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(uuid).getUniqueId());
		int newBalance = new Integer(playerConfig.getInt("Coins") - amount);
		playerConfig.set("Coins", newBalance);
		playerConfig.save();
		
		//update placeholder data
		if(PlaceHolders.playersCoins.containsKey(uuid.toString()))
			PlaceHolders.playersCoins.replace(uuid.toString(), newBalance);
		else PlaceHolders.playersCoins.put(uuid.toString(), newBalance);
		
		Chat.updateLeaderBoard(Leaderboard.COINS, uuid);
		
		Chat.sendTitleMessage(uuid, ChatColor.RED + "Withdrawn " + ChatColor.GOLD + "" + ChatColor.BOLD + convertCoins(amount));
		
		PlaceHolders.totalCoinsInCirculation -= amount; //update stat
	}
	
}
