package com.deadmc.DeadAPImaven;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class PublicTP extends YamlConfiguration {
	
	public static Map<String, PublicTP> configs = new HashMap<String, PublicTP>();
	
	public static PublicTP getConfig(String pointName) {
		synchronized(configs) { //'synchronized' allows only one thread to access configs at a time
			
			if(configs.containsKey(pointName))
				return configs.get(pointName);
			
			PublicTP config = new PublicTP(pointName);
			configs.put(pointName, config);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[PublicTP] " + ChatColor.RESET + "Added " + pointName + " - " + configs.size());
			
			return config;
			
		}
	}
	
	private File file = null;
	private Object saveLock = new Object();
	private String pointName;
	
	public PublicTP(String pointName) {
		super(); //'super' calls parent class constructor (YamlConfiguration)
		file = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "travel" + File.separator + pointName + ".yml");
		this.pointName = pointName;
		reload();
	}
	
	@SuppressWarnings("unused")
	private PublicTP() {
		pointName = null;
	}
	
	private void reload() {
		synchronized(saveLock) {
			try {
				load(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void save() {
		synchronized(saveLock) {
			try {
				save(file);
			} catch(Exception ignore) {
			
			}
		}
	}
	
	public void discard() {
		synchronized(configs) {
			configs.remove(pointName);
			Chat.debug(ChatColor.LIGHT_PURPLE + "[PublicTP] " + ChatColor.RESET + "Removed " + pointName + " - " + configs.size());
		}
	}
	
}