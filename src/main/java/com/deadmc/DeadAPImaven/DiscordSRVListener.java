package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Towns.Town;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.api.ListenerPriority;
import github.scarsz.discordsrv.api.Subscribe;
import github.scarsz.discordsrv.api.events.*;
import github.scarsz.discordsrv.dependencies.jda.api.Permission;
import github.scarsz.discordsrv.dependencies.jda.api.entities.*;
import github.scarsz.discordsrv.dependencies.jda.api.events.guild.member.GuildMemberJoinEvent;
import github.scarsz.discordsrv.dependencies.jda.api.interactions.commands.OptionType;
import github.scarsz.discordsrv.dependencies.jda.api.interactions.commands.build.CommandData;
import github.scarsz.discordsrv.dependencies.jda.api.interactions.commands.build.OptionData;
import github.scarsz.discordsrv.util.DiscordUtil;
import github.scarsz.discordsrv.util.WebhookUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class DiscordSRVListener {
	
	private final DeadMC plugin;
	
	public DiscordSRVListener(DeadMC plugin) {
		this.plugin = plugin;
	}

	public static void allowChat(boolean allow) {
		if(DeadMC.RestartFile.data().getString("IsDevelopmentServer") == null) {
			List<Permission> allowPerms = new ArrayList<Permission>();
			List<Permission> denyPerms = new ArrayList<Permission>();
			
			allowPerms.add(Permission.VIEW_CHANNEL);
			if(allow) {
				allowPerms.add(Permission.MESSAGE_WRITE);
				Chat.debug(ChatColor.GREEN + "Allowing discord chat for role: " + DiscordSRV.getPlugin().getMainGuild().getRoleById("600297881961889804"));
			} else {
				denyPerms.add(Permission.MESSAGE_WRITE);
				Chat.debug(ChatColor.RED + "Denying discord chat for role: " + DiscordSRV.getPlugin().getMainGuild().getRoleById("600297881961889804"));
			}
			DiscordSRV.getPlugin().getMainTextChannel().getParent().putPermissionOverride(DiscordSRV.getPlugin().getMainGuild().getRoleById("600297881961889804")).setPermissions(allowPerms, denyPerms).complete();
		}
	}
	
	public static String getDiscordName(UUID uuid) {
		String discordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(uuid);
		if(discordID == null) return null;
		User linkedUser = DiscordUtil.getJda().getUserById(discordID);
		if(linkedUser == null) return null;
		Member member = DiscordSRV.getPlugin().getMainGuild().getMember(linkedUser);
		if(member == null) return null;
		return member.getNickname();
	}
	
	@Subscribe
	public void discordReadyEvent(DiscordReadyEvent event) {
		
		if(DeadMC.RestartFile.data().getString("IsDevelopmentServer") == null) {
			// Example of using JDA's events
			// We need to wait until DiscordSRV has initialized JDA, thus we're doing this inside DiscordReadyEvent
			DiscordUtil.getJda().addEventListener(new JDAListener(plugin));
			
			List<CommandData> cmds = new ArrayList<CommandData>();
			
			cmds.add(new CommandData("vanish", "Toggle invisibility and silent login/logout."));
			
			cmds.add(new CommandData("voice", "Create a private town or nation voice channel."));
			
			cmds.add(new CommandData("unpunish", "Unpunish a player on Discord and Minecraft.")
					.addOptions(new OptionData(OptionType.STRING, "punish_id", "The punishment ID. This can be found in the staff log.").setRequired(true))
					.addOptions(new OptionData(OptionType.STRING, "reason", "The reason why the player is being unpunished.").setRequired(true)));
			
			cmds.add(new CommandData("punish", "Punish a player on Discord and Minecraft.")
					.addOptions(new OptionData(OptionType.STRING, "player", "The player's Minecraft username or donator nickname.").setRequired(true))
					.addOptions(new OptionData(OptionType.STRING, "punishment", "BUG_ABUSE, HACKING, OFFENSIVE_LANGUAGE, PUBLIC_NUISANCE, SHARING_LINKS, SPAMMING").setRequired(true))
					.addOptions(new OptionData(OptionType.STRING, "comments", "Evidence, notes, or other comments. This will appear to the player.").setRequired(true))
					.addOptions(new OptionData(OptionType.STRING, "time", "The punishment length. (eg. 1day, 2weeks) (only applicable for bans)").setRequired(false)));
			
			DiscordSRV.getPlugin().getMainGuild().updateCommands().addCommands(cmds).queue();
			
			
			// ... we can also do anything other than listen for events with JDA now,
			plugin.getLogger().info("Chatting on Discord with " + DiscordUtil.getJda().getUsers().size() + " users!");
			// see https://ci.dv8tion.net/job/JDA/javadoc/ for JDA's javadoc
			// see https://github.com/DV8FromTheWorld/JDA/wiki for JDA's wiki
			
			if(DeadMC.RestartFile.data().getString("Maintenance") == null) {
				Chat.broadcastDiscord(":ok_hand: **The server is back online!** <@&816520549001068574>"); //send the message to discord
				DiscordSRVListener.allowChat(true);
				
				DecimalFormat df = new DecimalFormat("####0.00");
				if(DeadMC.RestartFile.data().getString("LastDisable") != null) {
					double timeSinceDisable = (System.currentTimeMillis() - DeadMC.RestartFile.data().getLong("LastDisable")) / 1000.0; //convert to seconds
					double timeInMinutes = timeSinceDisable / 60.0;
					
					//if the time since last safe disable is more than 60 minutes, it is likely the server didn't shut down properly and call onDisable last time.
					if(timeInMinutes > 60) { //server crashed ?
						DeadMC.RestartFile.data().set("LastDisable", System.currentTimeMillis());
						DeadMC.RestartFile.save();
						Chat.broadcastDiscord(":head_bandage: **The server didn't shut down properly.** The server didn't register the onDisable previously correctly and therefore may have crashed. If server was intended to be down for more than 1 hour, you can ignore this.", true); //send the message to ops
					}
				}
			}
		}
	}

	//automatic responses:
	// strip the string of any non alphabet characters and lowercase
	//eg. 'I can't join' - becomes 'i cant join'
	
	//only works in the help, bug-reports and longdale channel
	//take a list of strings and send a message if it contains it
	// cant, log | cant, join | cannot, join | ip, incorrect | verify, username | unknown, host - show 'The server is currently ONLINE. Having trouble connecting? Make sure you on are the PC/java version of Minecraft, have a genuine account. If still not working, try our direct IP X'

	public List<List<String>> troubleConnectingResponseTriggers = (List<List<String>>) Arrays.asList(
			Arrays.asList("cant", "log"),
			Arrays.asList("cant", "join"),
			Arrays.asList("cannot", "log"),
			Arrays.asList("cannot", "join"),
			Arrays.asList("ip", "incorrect"),
			Arrays.asList("username", "verify"),
			Arrays.asList("unknown", "host")
	);

	public boolean messageContainsPhrases(String message, List<List<String>> triggers) {
		
		String messageStrippedToJustAlphabet = message.replaceAll("[^a-zA-Z ]", "").toLowerCase();
		
		//Bukkit.broadcastMessage("Testing '" + messageStrippedToJustAlphabet + "'");
		
		for(List<String> phrases : triggers) {
			//Bukkit.broadcastMessage("Checking phrases '" + phrases + "'");
			boolean phrasesAreGood = true;
			for(String phrase : phrases) {
				if(!messageStrippedToJustAlphabet.contains(phrase)) {
					//Bukkit.broadcastMessage(" > Didn't contain '" + phrase + "' - breaking");
					phrasesAreGood = false;
					break;
				}
				//Bukkit.broadcastMessage(" > Contains '" + phrase + "'");
			}
			if(phrasesAreGood) {
				//Bukkit.broadcastMessage(" > Match!");
				return true;
			}
		}
		
		return false;
	}
	
	@Subscribe(priority = ListenerPriority.MONITOR)
	public void discordMessageReceived(DiscordGuildMessageReceivedEvent event) {
		// Example of logging a message sent in Discord
		
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		String discordID = event.getMember().getId();
		UUID uuid = DiscordSRV.getPlugin().getAccountLinkManager().getUuid(discordID);
		if(uuid == null) return; //user doesn't have account linked
		
		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
		if(Chat.playerIsMuted(uuid)
				|| playerConfig.getString("Banned.Start") != null) {
			//player is muted or banned
			event.getMessage().delete().complete();
			String type = playerConfig.getString("Banned.Start") != null ? "banned" : "muted";
			sendPrivateMessage(discordID, "You attempted to talk in the Discord, although you are currently " + type + ".\nYou can appeal on our forums at www.DeadMC.com/forum.");
			return;
		}
		
		//process the message sent in discord
		final String message = event.getMessage().getContentStripped().length() > 256 ? event.getMessage().getContentStripped().substring(0, 256) : event.getMessage().getContentStripped();
		
		//send help messages:
		if(event.getChannel().getName().equals("longdale")
				|| event.getChannel().getName().equals("bug-reports")
				|| event.getChannel().getName().equals("help")
				|| event.getChannel().getName().equals("minecraft-chat-dev")) {
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				if(messageContainsPhrases(message, troubleConnectingResponseTriggers)) {
					//send message
					DiscordUtil.sendMessage(event.getChannel(), "> The server is currently **ONLINE**." +
							"\n\n*Having trouble connecting?* Make sure you are using the PC/Java version of Minecraft and have a genuine Minecraft account." +
							"\nIf still not working, try our direct IP: **161.129.154.214**");
				}
			});
		}
		
		if(event.getChannel().getName().contains("-town-chat")) {
			//if player is in town chat channel, they are in a town and have their account linked
			event.getMessage().delete().complete();
			
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> Chat.sendTownChat(player.getUniqueId(), message, true));
		}
		
		if(event.getChannel().getName().contains("-nation-chat")) {
			//if player is in nation chat channel, they are in a nation and have their account linked
			event.getMessage().delete().complete();
			
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> Chat.sendNationChat(player.getUniqueId(), message, true));
		}
		
		if(event.getChannel() == DiscordSRV.getPlugin().getMainTextChannel()) {
			//was in main text channel
			event.getMessage().delete().complete();
			
			String finalMessage = message;
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				String checkedMessage = Chat.checkMessage(finalMessage, uuid, discordID);
				if(checkedMessage != null) {
					Chat.lastMessage.put(uuid.toString(), finalMessage);
					Chat.lastMessageTime.put(uuid.toString(), System.currentTimeMillis());
					
					//send discord message
					String playerName = playerConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(uuid).getName() : Chat.stripColor(playerConfig.getString("Name"));
					String avatarUrl = "https://www.mc-heads.net/avatar/" + uuid.toString();
					//add staff rank, or [Supporter]
					String prefix = playerConfig.getInt("DonateRank") > 0 ? " (Supporter)" : "";
					if(playerConfig.getString("StaffRank") != null) {
						Admin.StaffRank staff = Admin.StaffRank.values()[playerConfig.getInt("StaffRank")];
						if(staff == Admin.StaffRank.JUNIOR_MOD)
							prefix =  " (Junior Mod)";
						if(staff == Admin.StaffRank.MODERATOR)
							prefix =  " (Moderator)";
						if(staff == Admin.StaffRank.SENIOR_MOD)
							prefix =  " (Senior Mod)";
						if(staff == Admin.StaffRank.ADMINISTRATOR)
							prefix =  " (Admin)";
						if(staff == Admin.StaffRank.OWNER)
							prefix =  " (Owner)";
					}
					WebhookUtil.deliverMessage(DiscordSRV.getPlugin().getMainTextChannel(), playerName + prefix, avatarUrl, checkedMessage, (MessageEmbed) null);
					
					//send in game message
					for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
						PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer.getUniqueId());
						if(onlineConfig.getString("Coins") != null) { //player not in tutorial
							
							List<String> ignoredUUIDs = new ArrayList<String>();
							if(onlineConfig.getString("Ignored") != null)
								ignoredUUIDs = onlineConfig.getStringList("Ignored");
							if(!ignoredUUIDs.contains(uuid.toString())) {
								
								String playersName = onlinePlayer.getName();
								if(onlineConfig.getString("Name") != null)
									playersName = Chat.stripColor(onlineConfig.getString("Name"));
								
								if(checkedMessage.toLowerCase().contains(onlinePlayer.getName().toLowerCase()) || checkedMessage.toLowerCase().contains(playersName.toLowerCase()))
									onlinePlayer.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " => " + ChatColor.RESET + Chat.getFullChatPrefix(uuid, true) + ChatColor.GRAY + ": " + ChatColor.WHITE + /*event.getMessage().substring(0, 1).toUpperCase() +*/ checkedMessage/*.substring(1)*/);
								else
									onlinePlayer.sendMessage(Chat.getFullChatPrefix(uuid, true) + ChatColor.GRAY + ": " + ChatColor.WHITE + /*event.getMessage().substring(0, 1).toUpperCase() +*/ checkedMessage/*.substring(1)*/);
								
							}
						}
					}
				}
			});
		}
		
	}

	@Subscribe(priority = ListenerPriority.MONITOR)
	public void aMessageWasSentInADiscordGuildByTheBot(DiscordGuildMessageSentEvent event) {
		// Example of logging a message sent in Minecraft (being sent to Discord)
		
		Chat.debug("A message was sent to Discord: " + event.getMessage(), "discord");
	}

	@Subscribe
	public void accountsLinked(AccountLinkedEvent event) {
		// Example of broadcasting a message when a new account link has been made
		Chat.debug(event.getPlayer().getName() + " just linked their MC account to their Discord user " + event.getUser() + "!");
		
		//add to town discord?
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		PlayerConfig playerConfig = PlayerConfig.getConfig(event.getPlayer());
		String originalTownName = playerConfig.getString("Town");
		if(originalTownName != null) {
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			String townDisplayName = Town.getTownDisplayName(originalTownName);
			
			if(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).size() == 0) {
				if(Town.qualifiesForDiscordChannel(originalTownName)) {
					Town.createDiscordChannel(originalTownName); //this will assign all the roles
				}
			} else {
				//has a discord channel
				
				//add the role
				String townRoleName = "Member of " + townDisplayName;
				Role townRole = guild.getRolesByName(townRoleName, false).get(0);
				DiscordUtil.addRolesToMember(DiscordUtil.getMemberById(event.getUser().getId()), townRole);
			}
			
			//check if nation now qualifies
			String nationName = townConfig.getString("Nation");
			if(nationName != null) {
				if(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).size() == 0) {
					if(Nation.qualifiesForDiscordChannel(nationName)) {
						Nation.createDiscordChannel(nationName); //this will assign all the roles
					}
				} else {
					//has a discord channel
					
					//add the role
					String nationRoleName = nationName + " nation";
					Role nationRole = guild.getRolesByName(nationRoleName, false).get(0);
					DiscordUtil.addRolesToMember(DiscordUtil.getMemberById(event.getUser().getId()), nationRole);
				}
			}
		}
	}
	
	@Subscribe
	public void accountUnlinked(AccountUnlinkedEvent event) {
		// Example of DM:ing user on unlink
		OfflinePlayer player = event.getPlayer();
		String discordID = event.getDiscordId();
		sendPrivateMessage(event.getDiscordId(), "Your account has been unlinked.");
		
		Chat.debug(player.getName() + " just unlinked their MC account.");
		
		//remove from town discord?
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		String originalTownName = playerConfig.getString("Town");
		if(originalTownName != null) {
			TownConfig townConfig = TownConfig.getConfig(originalTownName);
			String townDisplayName = Town.getTownDisplayName(originalTownName);
			
			//remove from nation discord?
			String nationName = townConfig.getString("Nation");
			if(nationName != null) {
				if(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).size() > 0) {
					if(Nation.getLeader(nationName).equals(player.getUniqueId())) {
						//is nation leader, delete the channel
						Nation.deleteDiscordChannel(nationName);
					} else {
						//not the leader, remove their role
						String nationRoleName = nationName + " nation";
						Role nationRole = guild.getRolesByName(nationRoleName, false).size() == 0 ? null : guild.getRolesByName(nationRoleName, false).get(0);
						
						DiscordUtil.removeRolesFromMember(DiscordUtil.getMemberById(discordID), nationRole);
					}
				}
			}
			
			if(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).size() > 0) {
				//has a discord channel
				
				if(playerConfig.getInt("TownRank") == Town.Rank.Mayor.ordinal()) {
					//if mayor, delete the channel
					Town.deleteDiscordChannel(originalTownName);
				} else {
					//if not mayor, remove their role
					String townRoleName = "Member of " + townDisplayName;
					Role townRole = guild.getRolesByName(townRoleName, false).size() == 0 ? null : guild.getRolesByName(townRoleName, false).get(0);
					
					DiscordUtil.removeRolesFromMember(DiscordUtil.getMemberById(discordID), townRole);
				}
			}
		}
	}
	
	public static void sendPrivateMessage(String discordID, String message) {
		User user = DiscordUtil.getJda().getUserById(discordID);
		
		// will be null if the bot isn't in a Discord server with the user (eg. they left the main Discord server)
		if (user != null) {
			
			// opens/retrieves the private channel for the user & sends a message to it (if retrieving the private channel was successful)
			user.openPrivateChannel().queue(privateChannel -> privateChannel.sendMessage(message).queue());
		}
	}

	@Subscribe
	public void discordMessageProcessed(DiscordGuildMessagePostProcessEvent event) {
		// Example of modifying a Discord -> Minecraft message
		event.setCancelled(true); //cancel all default messages
	}
	
	@Subscribe
	public void onUserJoinGuild(GuildMemberJoinEvent event) {
		sendPrivateMessage(event.getUser().getId(), ":wave: Welcome to DeadMC!" +
				"\n\nTo connect to the server, you must have a *genuine* Minecraft **Java Edition** account *(no bedrock edition*, sorry!).\n> You can connect with our IP **play.DeadMC.com**." +
				"\n\nRemember to **link your Discord** account in game with **/discord link** for some cool extra features!." +
				"\n\nBe sure to also join our website @ **www.DeadMC.com** for access to the forums, donation perks, and more!" +
				"\n\n*Having troubles connecting?*\n" +
				"Try our direct IP: **161.129.154.214**");
	}
	
}

