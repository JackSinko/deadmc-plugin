package com.deadmc.DeadAPImaven;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Dictionary;
import java.util.concurrent.TimeUnit;

public class Tools {
	
	public static void sleepThread(int milliseconds) {
		//sleeps the thread it's called from in milliseconds
		try {
			TimeUnit.MILLISECONDS.sleep(milliseconds);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}

	//stack level starts at 1
	public static void printStackTrace() {
		printStackTrace(1);
	}
	public static void printStackTraceAll() {
		printStackTrace(Integer.MAX_VALUE);
	}
	public static void printStackTrace(int levels) {
		for(int level = 0; level < levels; level++) { //start at 2
			try {
				Chat.debug("" + Thread.currentThread().getStackTrace()[2 + level]); //start at 2
			} catch(IndexOutOfBoundsException e) {
				Chat.debug("-- End of stack --");
				return;
			}
		}
	}
	
	public static boolean floodgateEnabled() {
		return false;
		//File floodgate = new File(Bukkit.getWorldContainer(), "plugins" + File.separator + "floodgate-spigot.jar");
		//return floodgate.exists() && floodgate != null;
	}
	
	public static boolean playerIsJava(Player player) {
		if(!floodgateEnabled()) return true;
		
		String floodgateDevice = PlaceholderAPI.setPlaceholders(player, "%floodgate_device%");
		return floodgateDevice.equalsIgnoreCase("Java");
	}
	
}
