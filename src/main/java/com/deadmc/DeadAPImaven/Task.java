package com.deadmc.DeadAPImaven;

import java.util.HashMap;

import org.bukkit.ChatColor;

import com.deadmc.DeadAPImaven.Task.TaskStep;

public class Task {
	
	public enum TaskStep {
		//TUTORIAL:
		OPEN_THE_TRAVEL_MENU,
		TRAVEL_TO_WILDERNESS,
		GATHER_LOGS,
		OPEN_THE_SHOP_FINDER_MENU,
		TRAVEL_TO_THE_MARKET,
		CLICK_LOG_SALE_SIGN,
		SELL_LOGS,
		FIND_THE_IRON_SWORD_SHOP,
		CLICK_IRON_SWORD_SALE_SIGN,
		BUY_SWORD,
		CREATE_A_TOWN,
		TRAVEL_TO_WILDERNESS_TO_CLAIM_LAND,
		OBTAIN_CLAIM_TOOL,
		SELECT_LAND_TO_CLAIM,
		CLAIM_LAND,
		//USE_LAND_COMMAND,
		USE_LAND_HELP_COMMAND,
		//USE_TOWN_COMMAND,
		OPEN_TOWN_TRAVEL_MENU,
		CREATE_TOWN_TRAVEL_POINT,
		TRAVEL_TO_TOWN,
		USE_TOWN_COMMAND
		//USE_HELP_COMMAND
	};

	public int ID;
	public String name;
	public String description;
	public HashMap<TaskStep, String> steps;
	public int reward;
	
	public Task(int ID, String name, String description, HashMap<TaskStep, String> steps, int reward) {
		this.ID = ID;
		this.name = name;
		this.description = description;
		this.steps = steps;
		this.reward = reward;
	}
	
}
