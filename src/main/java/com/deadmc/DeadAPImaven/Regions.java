package com.deadmc.DeadAPImaven;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.deadmc.DeadAPImaven.Towns.Town;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.regions.RegionOperationException;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;
import net.raidstone.wgevents.events.RegionEnteredEvent;
import net.raidstone.wgevents.events.RegionLeftEvent;
import com.deadmc.DeadAPImaven.Admin.StaffRank;
import com.deadmc.DeadAPImaven.Task.TaskStep;
import com.deadmc.DeadAPImaven.Towns.Town.Rank;

public class Regions implements Listener, CommandExecutor {
	private static DeadMC plugin;
	
	public Regions(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static List<String> confirmUnclaimRegion = new ArrayList<String>();
	
	public static Map<String, String> playersWhoCouldHaveLeftTown = new HashMap<String, String>(); //player, town
	public static List<String> playersInTownRegions = new ArrayList<String>();
	
	public static Map<String, Integer> confirmBuyRegion = new HashMap<String, Integer>(); //player, saleID
	public static Map<String, String> setupSellRegion = new HashMap<String, String>(); //player, regionID
	public static Map<String, Integer> setupSellRegionCost = new HashMap<String, Integer>(); //player, cost
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		final Player player = (Player) sender;
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			try {
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
				
				if(commandLabel.equalsIgnoreCase("claimtool")) {
					if(playerConfig.getString("Town") != null) {
						
						if(!player.getInventory().contains(Material.WOODEN_AXE)) {
							
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Added 1x claim tool (wooden axe) to inventory.");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.YELLOW + "Claims the area from bedrock to max height.");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Left click" + ChatColor.WHITE + " to select the first corner");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Right click" + ChatColor.WHITE + " to select the opposite corner");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Claim wild land with: " + ChatColor.GOLD + "/land claim");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Divide a region with: " + ChatColor.GOLD + "/land define");
							player.sendMessage("");
							
							Bukkit.getScheduler().runTask(plugin, () -> {
								ItemStack claimTool = new ItemStack(Material.WOODEN_AXE, 1);
								
								ItemMeta meta = claimTool.getItemMeta();
								meta.setDisplayName(ChatColor.GOLD + "Claim Tool");
								ArrayList<String> lore = new ArrayList<String>();
								lore.add(ChatColor.WHITE + "Use to claim land");
								lore.add(" ");
								lore.add(ChatColor.YELLOW + "Claims the area from bedrock to max height");
								lore.add(ChatColor.RED + "Left click" + ChatColor.WHITE + " to select the first corner");
								lore.add(ChatColor.RED + "Right click" + ChatColor.WHITE + " to select the opposite corner");
								meta.setLore(lore);
								claimTool.setItemMeta(meta);
								
								player.getInventory().addItem(claimTool);
								
							});
							
							//TASK
							if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.OBTAIN_CLAIM_TOOL))
								TaskManager.stepTask(player.getUniqueId());
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You already have a claim tool in your inventory.");
						
					} else
						player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You must be a member of a town to claim land.");
				}
				
				
				if(commandLabel.equalsIgnoreCase("land")) {
					
					ApplicableRegionSet regionsAtLoc = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world"))).getApplicableRegions(BukkitAdapter.asBlockVector(player.getLocation()));
					
					if(args.length == 0) {
						
						if(regionsAtLoc.size() == 0) {
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Standing in wilderness; this land hasn't been claimed.");
							//if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.USE_LAND_COMMAND))
							//player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Stand in your region and try again to complete the task.");
							
							return;
						}
						
						ProtectedRegion townRegion = (ProtectedRegion) regionsAtLoc.getRegions().toArray()[0];
						
						if(townRegion.getPriority() == 0) {
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You are in " + ChatColor.YELLOW + ChatColor.BOLD + "Longdale" + ChatColor.WHITE + " territory.");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Get to the wilderness with " + ChatColor.GOLD + "/wild" + ChatColor.WHITE + ".");
							player.sendMessage("");
							
							return;
						}
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "============= Regions at your location: =============");
						player.sendMessage("You are in " + ChatColor.YELLOW + ChatColor.BOLD + Town.getTownDisplayName(getOriginalTownName(townRegion.getId())) + ChatColor.WHITE + " territory.");
						player.sendMessage("");
						player.sendMessage(ChatColor.WHITE + "Priority: " + ChatColor.AQUA + "Region ID" + ChatColor.WHITE + " (owner)");
						for(int count = 0; count < regionsAtLoc.size(); count++) {
							
							ProtectedRegion region = (ProtectedRegion) regionsAtLoc.getRegions().toArray()[count];
							
							String regionName = getRegionID(region.getId());
							
							String owner = "";
							if(!Chat.isInteger(getRegionID(region.getId()))) {
								for(UUID uuid : region.getOwners().getUniqueIds())
									owner = Bukkit.getOfflinePlayer(uuid).getName();
								owner = ChatColor.WHITE + " (" + owner + ")";
							} else
								regionName = regionName + ChatColor.WHITE + ChatColor.ITALIC + " (belongs to town)";
							
							if(highestPriorityLand(player.getLocation()) == region.getPriority()) {
								Bukkit.getScheduler().runTask(plugin, new Runnable() {
									@Override
									public void run() {
										Bukkit.dispatchCommand(player, "svis wg " + region.getId());
									}
								});
								player.sendMessage("    - Priority " + region.getPriority() + ChatColor.WHITE + ": " + ChatColor.AQUA + ChatColor.BOLD + regionName + owner + ChatColor.WHITE + " (" + ChatColor.DARK_GREEN + ChatColor.BOLD + "***" + ChatColor.WHITE + ")");
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Land] " + ChatColor.AQUA + ChatColor.BOLD + regionName + ChatColor.WHITE + " (" + ChatColor.GOLD + (region.volume() / 256) + ChatColor.WHITE + " blocks)");
							} else
								player.sendMessage("    - Priority " + region.getPriority() + ChatColor.WHITE + ": " + ChatColor.AQUA + regionName + owner);
							
						}
						player.sendMessage("");
						player.sendMessage("(" + ChatColor.DARK_GREEN + ChatColor.BOLD + "***" + ChatColor.WHITE + ") - has building rights at your location");
						player.sendMessage("Use " + ChatColor.GOLD + "/land info <town name> <region ID>" + ChatColor.WHITE + " for info.");
						player.sendMessage(ChatColor.YELLOW + "================================================");
						player.sendMessage("");
						
						//TASK
						//if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.USE_LAND_COMMAND)) TaskManager.stepTask(player.getUniqueId());
						
						return;
					} else if(args[0].equalsIgnoreCase("help")) {
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "================== Land Commands ==================");
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Research land:");
						player.sendMessage(ChatColor.GOLD + "/land" + ChatColor.WHITE + " - View regions at your location.");
						if(playerConfig.getString("Town") != null)
							player.sendMessage(ChatColor.GOLD + "/land info" + ChatColor.WHITE + " - A list of regions in " + Town.getTownDisplayName(playerConfig.getString("Town")) + ".");
						player.sendMessage(ChatColor.GOLD + "/land info <town>" + ChatColor.WHITE + " - A list of regions in a town.");
						player.sendMessage(ChatColor.GOLD + "/land info <town> <region>" + ChatColor.WHITE + " - Details of a specific region.");
						if(playerConfig.getString("Town") != null) {
							player.sendMessage("");
							player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Claim and divide:");
							player.sendMessage(ChatColor.GOLD + "/claimtool" + ChatColor.WHITE + " - Obtain the tool to select regions.");
							if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
								player.sendMessage(ChatColor.GOLD + "/land claim" + ChatColor.WHITE + " - Claim wild land.");
							}
							player.sendMessage(ChatColor.GOLD + "/land define" + ChatColor.WHITE + " - Divide your claimed land.");
							player.sendMessage(ChatColor.GOLD + "/land unclaim" + ChatColor.WHITE + " - Delete a region.");
							
							player.sendMessage("");
							player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Building rights:");
							player.sendMessage(ChatColor.GOLD + "/land share" + ChatColor.WHITE + " - Give town members building rights to a region.");
							player.sendMessage(ChatColor.GOLD + "/land unshare" + ChatColor.WHITE + " - Remove members building rights.");
							player.sendMessage(ChatColor.GOLD + "/land sell" + ChatColor.WHITE + " - Create a [FOR SALE] sign on your region.");
							player.sendMessage(ChatColor.GOLD + "/land buy" + ChatColor.WHITE + " - Buy a region.");
							
							player.sendMessage("");
							player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Other:");
							player.sendMessage(ChatColor.GOLD + "/land spawning" + ChatColor.WHITE + " - Toggle zombie spawning in region.");
						}
						player.sendMessage(ChatColor.YELLOW + "==================================================");
						player.sendMessage("");
						
						//TASK
						if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.USE_LAND_HELP_COMMAND)) {
							Bukkit.getScheduler().runTask(plugin, new Runnable() {
								@Override
								public void run() {
									TaskManager.stepTask(player.getUniqueId());
								}
							});
						}
						
					} else if(args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("lookup")) {
						if(args.length > 1) {
							if(Town.townNameIsTaken(args[1])) {
								String originalTownName = Town.getOriginalTownName(Town.townNameCased(args[1].toLowerCase()));
								String townDisplayName = Town.getTownDisplayName(originalTownName);
								TownConfig townConfig = TownConfig.getConfig(originalTownName);
								
								List<String> regions = townConfig.getStringList("Regions");
								
								if(args.length == 2 || !regions.contains(args[2])) {
									
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(townDisplayName + "'s regions", 46) + " " + townDisplayName + "'s regions " + Chat.getTitlePlaceholder(townDisplayName + "'s regions", 46));
									if(regions.size() == 0)
										player.sendMessage(ChatColor.RED + townDisplayName + " doesn't have any claimed land.");
									else
										player.sendMessage(ChatColor.WHITE + "Priority " + ChatColor.WHITE + "/ " + ChatColor.AQUA + "Region ID" + ChatColor.WHITE + " (owner)");
									player.sendMessage("");
									for(int count = 0; count < regions.size(); count++) {
										
										try {
											ProtectedRegion region = (ProtectedRegion) regionManager.getRegion(originalTownName + "/" + regions.get(count));
											String regionID = getRegionID(region.getId());
											
											String owner = "";
											if(!Chat.isInteger(getRegionID(region.getId()))) {
												for(UUID uuid : region.getOwners().getUniqueIds())
													owner = Bukkit.getOfflinePlayer(uuid).getName();
												owner = ChatColor.WHITE + " (" + owner + ")";
											} else
												regionID = regionID + ChatColor.WHITE + ChatColor.ITALIC + " (belongs to town)";
											
											player.sendMessage("    - Priority " + region.getPriority() + ChatColor.WHITE + ": " + ChatColor.AQUA + ChatColor.BOLD + regionID + owner);
											
										} catch(Exception e) {
											Chat.logError("Town region no longer exists: " + originalTownName + "/" + regions.get(count));
										}
									}
									player.sendMessage("");
									player.sendMessage("Use " + ChatColor.GOLD + "/land info <town name> <region ID>" + ChatColor.WHITE + " for info.");
									player.sendMessage(ChatColor.YELLOW + "================================================");
									player.sendMessage("");
								} else if(regions.contains(args[2])) {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(townDisplayName + ": " + args[2].toLowerCase(), 46) + " " + townDisplayName + ": " + args[2].toLowerCase() + " " + Chat.getTitlePlaceholder(townDisplayName + ": " + args[2].toLowerCase(), 46));
									ProtectedRegion region = (ProtectedRegion) regionManager.getRegion(originalTownName + "/" + args[2]);
									player.sendMessage("Priority: " + ChatColor.GOLD + region.getPriority());
									if(region.getPriority() == 1) {
										player.sendMessage("Owner: " + ChatColor.ITALIC + "(belongs to town)");
										player.sendMessage("Members:");
										for(UUID owner : region.getOwners().getUniqueIds())
											player.sendMessage("  - " + ChatColor.GOLD + Bukkit.getOfflinePlayer(owner).getName());
										for(UUID member : region.getMembers().getUniqueIds())
											player.sendMessage("  - " + ChatColor.GOLD + Bukkit.getOfflinePlayer(member).getName());
									} else {
										for(UUID owner : region.getOwners().getUniqueIds())
											player.sendMessage("Owner: " + ChatColor.GOLD + Bukkit.getOfflinePlayer(owner).getName());
										if(region.getMembers().size() > 0) {
											player.sendMessage("Members:");
											for(UUID member : region.getMembers().getUniqueIds())
												player.sendMessage("  - " + ChatColor.GOLD + Bukkit.getOfflinePlayer(member).getName());
										}
									}
									player.sendMessage("Land size: " + ChatColor.GOLD + (region.volume() / 256) + ChatColor.WHITE + " blocks");
									if(playerConfig.getString("Town") != null && playerConfig.getString("Town").equalsIgnoreCase(originalTownName)) {
										player.sendMessage("Coords: " + ChatColor.AQUA + ChatColor.ITALIC + "(" + region.getMinimumPoint().getBlockX() + ", " + region.getMinimumPoint().getBlockZ() + ")" + ChatColor.WHITE + " -> " + ChatColor.AQUA + ChatColor.ITALIC + "(" + region.getMaximumPoint().getBlockX() + ", " + region.getMaximumPoint().getBlockZ() + ")");
									}
									player.sendMessage(ChatColor.YELLOW + "================================================");
									player.sendMessage("");
								}
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "'" + args[1] + "' is not an existing town.");
						} else if(playerConfig.getString("Town") != null) {
							Bukkit.getScheduler().runTask(plugin, new Runnable() {
								@Override
								public void run() {
									Bukkit.getServer().dispatchCommand(player, "land info " + playerConfig.getString("Town"));
								}
							});
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/land info <town>");
					} else if(args[0].equalsIgnoreCase("claim")) {
						if(playerConfig.getString("Town") != null
								&& playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
							
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							//if player has selection
							int regionID = townConfig.getInt("CurrentLandID") + 1;
							String potentialRegionName = new String(originalTownName.toLowerCase() + "/" + regionID);
							ProtectedCuboidRegion selection = getSelection(player, potentialRegionName);
							if(selection != null) {
								
								int blocksClaimable = (townConfig.getInt("Limit.Claim") * townConfig.getInt("Limit.Claim")) - townConfig.getInt("ClaimedBlocks");
								int size = selection.volume() / 256;
								
								if(size <= blocksClaimable) {
									
									//if selection doesn't include claimed regions
									if(regionManager.getApplicableRegions(selection).getRegions().size() == 0) {
										//claim wild land
										
										//set player who claimed it as owner
										DefaultDomain owners = new DefaultDomain();
										owners.addPlayer(player.getUniqueId());
										
										selection.setOwners(owners);
										
										//set any other mayors as member
										DefaultDomain members = new DefaultDomain();
										for(UUID mayor : Town.getMayors(originalTownName)) {
											if(!(mayor.toString().equalsIgnoreCase(player.getUniqueId().toString()))) {
												members.addPlayer(mayor);
											}
										}
										
										selection.setMembers(members);
										
										selection.setPriority(1);
										
										regionManager.addRegion(selection);
										try {
											regionManager.save();
										} catch(StorageException e) {
											e.printStackTrace();
										}
										
										Bukkit.getScheduler().runTask(plugin, new Runnable() {
											@Override
											public void run() {
												Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + potentialRegionName + " deny-message -w " + player.getWorld().getName() + " &e[Land] &cThis is claimed land! &fUse &6/land &ffor info.");
												Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + potentialRegionName + " deny-spawn -w " + player.getWorld().getName() + " zombie,zombie_villager,husk,drowned,spider,cave_spider,skeleton,stray,creeper,phantom,ghast");
												Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + potentialRegionName + " chorus-fruit-teleport -w " + player.getWorld().getName() + " deny");
												Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + potentialRegionName + " enderpearl -w " + player.getWorld().getName() + " deny");
											}
										});
										
										List<String> regions = townConfig.getStringList("Regions");
										
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + "The land now belongs to " + townDisplayName + "! " + ChatColor.WHITE + "(ID: " + ChatColor.YELLOW + regionID + ChatColor.WHITE + ")");
										
										if(regions.size() == 0) {
											Location travelLocation = player.getLocation();
											
											townConfig.set("Travel.LocationCode", LocationCode.Encode(travelLocation));
											townConfig.set("Travel.Public", false);
											townConfig.save();
											
											player.sendMessage("");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.GREEN + "Town travel point created.");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "You can use " + ChatColor.GOLD + "/town set travel-point" + ChatColor.WHITE + " to update this.");
											player.sendMessage(ChatColor.YELLOW + "[Town] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/travel " + townDisplayName + ChatColor.WHITE + " to travel to " + townDisplayName + ".");
											player.sendMessage("");
										}
										regions.add("" + regionID);
										townConfig.set("Regions", regions);
										
										townConfig.set("ClaimedBlocks", townConfig.getInt("ClaimedBlocks") + size);
										townConfig.set("CurrentLandID", regionID);
										townConfig.save();
										
										//TASK
										if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.CLAIM_LAND)) {
											Bukkit.getScheduler().runTask(plugin, new Runnable() {
												@Override
												public void run() {
													TaskManager.stepTask(player.getUniqueId());
												}
											});
										}
										
									} else {
										player.sendMessage("");
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Ensure there isn't any claimed land in your selection.");
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "To split the land, use " + ChatColor.GOLD + "/land define" + ChatColor.WHITE + ".");
										player.sendMessage("");
									}
									
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "The selection size exceeds your claim limit! (" + ChatColor.BOLD + size + ChatColor.RED + " blocks selected / " + ChatColor.BOLD + blocksClaimable + ChatColor.RED + " claimable blocks)");
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/town upgrade" + ChatColor.WHITE + " to upgrade limit.");
									player.sendMessage("");
								}
								
							} else {
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You don't have a selection active to claim!");
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use the " + ChatColor.GOLD + "/claimtool" + ChatColor.WHITE + " to make a selection");
								player.sendMessage("");
							}
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You must be a town " + ChatColor.YELLOW + "mayor" + ChatColor.WHITE + "/" + ChatColor.YELLOW + "co-mayor" + ChatColor.WHITE + " to claim wild land.");
					} else if(args[0].equalsIgnoreCase("unclaim") || args[0].equalsIgnoreCase("delete")) {
						String originalTownName = playerConfig.getString("Town");
						String townDisplayName = Town.getTownDisplayName(originalTownName);
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						
						if(args.length > 1) {
							String regionName = originalTownName.toLowerCase() + "/" + args[1].toLowerCase();
							ProtectedRegion region = regionManager.getRegion(regionName);
							
							if(playerConfig.getString("Town") != null && regionManager.hasRegion(regionName)
									&& (region.getOwners().getUniqueIds().contains(player.getUniqueId()) || region.getOwners().getUniqueIds().size() == 0 || !Town.isMember(originalTownName, (UUID) region.getOwners().getUniqueIds().toArray()[0]))) {
								
								ApplicableRegionSet regionsWithinRegion = regionManager.getApplicableRegions(region);
								if(highestPriorityLand(regionsWithinRegion.getRegions()) == region.getPriority()) {
									
									List<String> shops = new ArrayList<String>();
									File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionName + ".yml");
									RegionConfig regionConfig = null;
									if(regionFile.exists()) {
										regionConfig = RegionConfig.getConfig(regionName);
										shops = regionConfig.getStringList("Shops");
									}
									
									//if shops exist in region:
									if(shops.size() > 0 && regionsWithinRegion.size() == 1) {
										if(!confirmUnclaimRegion.contains(player.getName())) {
											
											confirmUnclaimRegion.add(player.getName());
											
											player.sendMessage("");
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Are you sure you want to remove " + args[1].toLowerCase() + "?");
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "There are " + ChatColor.BOLD + shops.size() + " shops " + ChatColor.WHITE + "that will be removed.");
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/land unclaim " + args[1].toLowerCase() + ChatColor.WHITE + " again to confirm.");
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Confirmation session will expire in 15 seconds");
											player.sendMessage("");
											
											Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {
												@Override
												public void run() {
													if(confirmUnclaimRegion.contains(player.getName())) {
														player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Confirmation session expired.");
														confirmUnclaimRegion.remove(player.getName());
													}
													
												}
											}, 300L);
											
											return;
										}
									}
									
									//give back claimed blocks (only for level 1 land)
									if(region.getPriority() == 1)
										townConfig.set("ClaimedBlocks", townConfig.getInt("ClaimedBlocks") - (region.volume() / 256));
									
									//update regions in town config
									List<String> regions = townConfig.getStringList("Regions");
									regions.remove(args[1].toLowerCase());
									townConfig.set("Regions", regions);
									townConfig.save();
									
									//update shops
									if(shops.size() > 0) {
										//remove shops if no more regions left
										if(regionsWithinRegion.size() == 1) {
											//track number of shops player has:
											for(UUID owner : regionManager.getRegion(regionName).getOwners().getUniqueIds()) {
												PlayerConfig ownerConfig = PlayerConfig.getConfig(owner);
												int newAmount = ownerConfig.getInt("Shops") - shops.size();
												if(newAmount == 0)
													ownerConfig.set("Shops", null);
												else
													ownerConfig.set("Shops", newAmount);
												ownerConfig.save();
											}
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + ChatColor.BOLD + shops.size() + " shops " + ChatColor.RED + "removed!");
											//clear signs
											for(String shopCode : shops) {
												Location location = Shops.decodeCode(shopCode);
												Bukkit.getScheduler().runTask(plugin, () -> {
													Sign sign = (Sign) location.getBlock().getState();
													for(int line = 0; line <= 3; line++)
														sign.setLine(line, "");
													sign.update();
												});
											}
											//remove region file:
											if(regionConfig != null)
												regionConfig.discard();
											regionFile.delete();
											File townFolder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + Regions.getOriginalTownName(regionName).toLowerCase());
											if(townFolder.isDirectory() && townFolder.list().length == 0)
												townFolder.delete();
										} else {
											//else transfer to the next highest priority region
											for(String shopCode : shops) {
												Shops.copyShop(regionName, getHighestPriorityLand(Shops.decodeCode(shopCode)).getId(), shopCode);
											}
										}
									}
									
									//delete the region
									regionManager.removeRegion(regionName);
									
									confirmUnclaimRegion.remove(player.getName());
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + "Region " + ChatColor.YELLOW + args[1].toLowerCase() + ChatColor.GREEN + " was deleted.");
									
								} else {
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You cannot unclaim " + ChatColor.BOLD + args[1].toLowerCase() + ChatColor.WHITE + " because the following regions are within this region (with a higher priority):");
									for(ProtectedRegion r : regionManager.getApplicableRegions(region).getRegions())
										if(!r.getId().equalsIgnoreCase(regionName) && r.getPriority() > region.getPriority())
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + " - " + ChatColor.RED + getRegionID(r.getId()));
									player.sendMessage("");
								}
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You don't own a region named " + ChatColor.YELLOW + args[1].toLowerCase() + ".");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/land unclaim <region ID>");
						
					} else if(args[0].equalsIgnoreCase("define")) {
						if(playerConfig.getString("Town") != null) {
							String originalTownName = playerConfig.getString("Town");
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							if(args.length > 1) {
								String potentialRegionName = new String(originalTownName.toLowerCase() + "/" + args[1].toLowerCase());
								if(!regionManager.hasRegion(potentialRegionName)) {
									if(!Chat.isInteger(args[1])) {
										if(args[1].length() <= 12) {
											if(!(args[1].toLowerCase().contains("fuck")
													|| args[1].toLowerCase().contains("shit")
													|| args[1].toLowerCase().contains("cunt")
													|| args[1].toLowerCase().contains("pussy")
													|| args[1].toLowerCase().contains("nigga")
													|| args[1].toLowerCase().contains("nigger"))) {
												
												//if player has selection
												try {
													ProtectedCuboidRegion region = new ProtectedCuboidRegion(potentialRegionName, BlockVector3.ONE, BlockVector3.ONE);
												} catch(IllegalArgumentException e) {
													player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "The region name cannot contain special characters.");
													return;
												}
												
												ProtectedCuboidRegion selection = getSelection(player, potentialRegionName);
												if(selection != null) {
													
													ApplicableRegionSet regionsWithinSelection = regionManager.getApplicableRegions(selection);
													//if selection doesn't include claimed regions
													if(regionsWithinSelection.size() > 0 &&
															playerOwnsLand(selection, player.getUniqueId(), false)) {
														
														//COPY SHOPS WITHIN SELECTION to new region file:
														//loop through all regions within selection
														for(ProtectedRegion region : regionsWithinSelection.getRegions()) {
															String regionName = region.getId();
															
															File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionName + ".yml");
															if(regionFile.exists()) {
																RegionConfig oldRegionConfig = RegionConfig.getConfig(regionName);
																
																//for each shop:
																for(String shopCode : oldRegionConfig.getStringList("Shops")) {
																	Location location = Shops.decodeCode(shopCode);
																	//if shop is in selection
																	if(selection.contains(BlockVector3.at(location.getBlockX(), location.getBlockY(), location.getBlockZ()))) {
																		Shops.copyShop(regionName, potentialRegionName, shopCode);
																	}
																}
															}
														}
														
														DefaultDomain owner = new DefaultDomain();
														owner.addPlayer(player.getUniqueId());
														selection.setOwners(owner);
														selection.setPriority(highestPriorityLand(regionManager.getApplicableRegions(selection).getRegions()) + 1);
														
														List<String> regions = townConfig.getStringList("Regions");
														regions.add(args[1].toLowerCase());
														townConfig.set("Regions", regions);
														townConfig.save();
														
														regionManager.addRegion(selection);
														try {
															regionManager.save();
														} catch(StorageException e) {
															e.printStackTrace();
														}
														
														Bukkit.getScheduler().runTask(plugin, new Runnable() {
															@Override
															public void run() {
																Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + potentialRegionName + " deny-message -w " + player.getWorld().getName() + " &e[Land] &cYou do not own this land!");
																Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + potentialRegionName + " deny-spawn -w " + player.getWorld().getName() + " zombie,zombie_villager,husk,drowned,spider,cave_spider,skeleton,stray,creeper,phantom,ghast");
																Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + potentialRegionName + " chorus-fruit-teleport -w " + player.getWorld().getName() + " deny");
																Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + potentialRegionName + " enderpearl -w " + player.getWorld().getName() + " deny");
															}
														});
														
														//claim region
														player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + "The land was successfully divided! " + ChatColor.WHITE + "(ID: " + ChatColor.YELLOW + args[1].toLowerCase() + ChatColor.WHITE + ")");
														
													} else {
														player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You must own all the land in the selection to do this.");
													}
													
												} else {
													player.sendMessage("");
													player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You don't have a selection active to claim!");
													player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use the " + ChatColor.GOLD + "/claimtool" + ChatColor.WHITE + " to make a selection.");
													player.sendMessage("");
												}
												
											} else
												player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Name cannot contain offensive language.");
										} else
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Name must be a maximum of 12 characters!");
									} else
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Name must contain " + ChatColor.ITALIC + "at least 1 " + ChatColor.RED + ChatColor.BOLD + "non-numeric" + ChatColor.RED + " character.");
								} else
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "There is already a region in your town named '" + args[1].toLowerCase() + "'.");
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/land define <region ID>");
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You must be a member of a town to define land.");
					} else if(args[0].equalsIgnoreCase("share")) {
						if(args.length > 2) {
							
							String playerName = args[2];
							for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
								PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
								if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[2])) {
									//used nick name
									playerName = onlinePlayer.getName();
									break;
								}
							}
							UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
							
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							String regionName = originalTownName + "/" + args[1].toLowerCase();
							
							if(originalTownName != null && regionManager.hasRegion(regionName) && regionManager.getRegion(regionName).getOwners().getUniqueIds().contains(player.getUniqueId())) {
								
								//players no longer need to be a member of the town to share land
								if(DeadMC.PlayerFile.data().getStringList("Active").contains(uuid.toString())) {
									
									ProtectedRegion region = regionManager.getRegion(regionName);
									if(!region.getMembers().getUniqueIds().contains(uuid) && !regionManager.getRegion(regionName).getOwners().getUniqueIds().contains(uuid)) {
										
										//add player as member
										DefaultDomain members = region.getMembers();
										members.addPlayer(uuid);
										region.setMembers(members);
										
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + ChatColor.BOLD + playerName + ChatColor.GREEN + " now has building rights to " + args[1].toLowerCase() + ".");
										if(Bukkit.getPlayer(uuid) != null)
											Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You've been given building rights to '" + ChatColor.YELLOW + ChatColor.BOLD + args[1].toLowerCase() + ChatColor.WHITE + "' in " + townDisplayName + " by " + player.getName() + ".");
										
									} else
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + Bukkit.getOfflinePlayer(uuid).getName() + ChatColor.WHITE + " already has buildings rights to " + ChatColor.YELLOW + args[1].toLowerCase() + ".");
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + args[2] + ChatColor.WHITE + " is not an existing player.");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You don't own a region named " + ChatColor.YELLOW + args[1].toLowerCase() + ".");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/land share <region ID> <player name>");
					} else if(args[0].equalsIgnoreCase("unshare")) {
						if(args.length > 2) {
							
							String playerName = args[2];
							for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
								PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer);
								if(onlineConfig.getString("Name") != null && Chat.stripColor(onlineConfig.getString("Name")).equalsIgnoreCase(args[2])) {
									//used nick name
									playerName = onlinePlayer.getName();
									break;
								}
							}
							UUID uuid = Bukkit.getOfflinePlayer(playerName).getUniqueId();
							
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							
							String regionName = originalTownName + "/" + args[1].toLowerCase();
							
							if(originalTownName != null && regionManager.hasRegion(regionName) && regionManager.getRegion(regionName).getOwners().getUniqueIds().contains(player.getUniqueId())) {
								
								if(regionManager.getRegion(regionName).getOwners().getUniqueIds().contains(uuid)) {
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You are the land owner.");
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Sell the land to remove building rights.");
									return;
								}
								
								ProtectedRegion region = regionManager.getRegion(regionName);
								if(region.getMembers().getUniqueIds().contains(uuid)) {
									
									//remove as member
									DefaultDomain members = region.getMembers();
									members.removePlayer(uuid);
									region.setMembers(members);
									
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + ChatColor.BOLD + Bukkit.getOfflinePlayer(uuid).getName() + ChatColor.GREEN + " no longer has building rights to " + args[1].toLowerCase() + ".");
									if(Bukkit.getPlayer(uuid) != null)
										Bukkit.getPlayer(uuid).sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You no longer have building rights to " + ChatColor.YELLOW + ChatColor.BOLD + args[1].toLowerCase() + ChatColor.WHITE + " in " + townDisplayName + ".");
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + args[2] + ChatColor.WHITE + " doesn't have building rights to " + ChatColor.YELLOW + args[1].toLowerCase() + ChatColor.WHITE + ".");
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "You don't own a region named " + ChatColor.YELLOW + args[1].toLowerCase() + ".");
							
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/land unshare <region ID> <player name>");
					} else if(args[0].equalsIgnoreCase("sell")) {
						if(args.length > 2) {
							String regionID = args[1].toLowerCase();
							
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							String regionName = originalTownName + "/" + args[1].toLowerCase();
							if(originalTownName != null && regionManager.hasRegion(regionName) && regionManager.getRegion(regionName).getOwners().getUniqueIds().contains(player.getUniqueId())) {
								if(Chat.isInteger(args[2]) && Integer.parseInt(args[2]) >= 0) {
									
									List<String> shops = new ArrayList<String>();
									File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionName + ".yml");
									if(regionFile.exists()) {
										RegionConfig regionConfig = RegionConfig.getConfig(regionName);
										shops = regionConfig.getStringList("Shops");
									}
									
									if(!setupSellRegion.containsKey(player.getName())) {
										
										setupSellRegion.put(player.getName(), regionID);
										int price = Integer.parseInt(args[2]);
										setupSellRegionCost.put(player.getName(), price);
										
										player.sendMessage("");
										//if shops exist in region:
										if(shops.size() > 0) {
											player.sendMessage(ChatColor.DARK_RED + "NOTE: " + ChatColor.WHITE + "There are " + ChatColor.BOLD + shops.size() + " shops" + ChatColor.WHITE + " in this region.");
											player.sendMessage(ChatColor.DARK_RED + "NOTE: " + ChatColor.WHITE + "You will lose ownership of these shops.");
										}
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Are you ready to put " + ChatColor.GOLD + regionID + ChatColor.WHITE + " up for sale?");
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GOLD + ChatColor.BOLD + " 1. " + ChatColor.WHITE + "Place a sign with " + ChatColor.AQUA + "[SELL]" + ChatColor.WHITE + " on the first line.");
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GOLD + ChatColor.BOLD + " 2. " + ChatColor.WHITE + "Click the sign.");
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "This sale setup session will expire in 30 seconds.");
										player.sendMessage("");
										
										Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {
											@Override
											public void run() {
												if(setupSellRegion.containsKey(player.getName())) {
													player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Sale setup session expired.");
													setupSellRegion.remove(player.getName());
													setupSellRegionCost.remove(player.getName());
												}
											}
										}, 600L);
										
									} else
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Please wait for the current session to expire.");
								} else
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Price must be the sale price in " + ChatColor.ITALIC + "coins" + ChatColor.RED + "!");
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You don't own a region named " + ChatColor.YELLOW + regionID.toLowerCase() + ChatColor.RED + ".");
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/land sell <region ID> <price>");
					} else if(args[0].equalsIgnoreCase("buy")) {
						if(args.length > 1) {
							if(Chat.isInteger(args[1])) {
								int saleID = Integer.parseInt(args[1]);
								if(DeadMC.RegionFile.data().getString(saleID + ".Region") != null) {
									int cost = DeadMC.RegionFile.data().getInt(saleID + ".Cost");
									String regionName = DeadMC.RegionFile.data().getString(saleID + ".Region");
									String originalTownName = getOriginalTownName(regionName);
									String townDisplayName = Town.getTownDisplayName(originalTownName);
									String regionID = getRegionID(regionName);
									ProtectedRegion region = regionManager.getRegion(regionName);
									
									if(playerConfig.getString("Town") != null &&
											playerConfig.getString("Town").equalsIgnoreCase(originalTownName)) {
										
										if(regionManager.hasRegion(regionName)) {
											if(!region.getOwners().getUniqueIds().contains(player.getUniqueId())) {
												if(playerConfig.getInt("Coins") >= cost) {
													
													List<String> shops = new ArrayList<String>();
													File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionName + ".yml");
													if(regionFile.exists()) {
														RegionConfig regionConfig = RegionConfig.getConfig(regionName);
														shops = regionConfig.getStringList("Shops");
													}
													
													if(confirmBuyRegion.containsKey(player.getName()) && saleID == confirmBuyRegion.get(player.getName())) {
														
														//make sure player doesn't go over shop limit:
														PlayerConfig newOwnerConfig = PlayerConfig.getConfig(player.getUniqueId());
														int shopLimit = DeadMC.DonatorFile.data().getInt(newOwnerConfig.getInt("DonateRank") + ".Upgrades.Shops");
														int currentShops = 0;
														if(newOwnerConfig.getString("Shops") != null)
															currentShops = newOwnerConfig.getInt("Shops");
														int newAmount1 = currentShops + shops.size();
														if(newAmount1 <= shopLimit) {
															
															confirmBuyRegion.remove(player.getName());
															
															//send notifications
															UUID owner = null;
															for(UUID uuid : region.getOwners().getUniqueIds())
																owner = uuid;
															Town.sendMessage(playerConfig.getString("Town"), "" + ChatColor.GREEN + ChatColor.BOLD + player.getName() + ChatColor.GREEN + " just bought region " + ChatColor.BOLD + regionID + ChatColor.GREEN + " from " + Bukkit.getOfflinePlayer(owner).getName() + " for " + Economy.convertCoins(cost) + ChatColor.GREEN + "!", false, false);
															Town.sendMessage(playerConfig.getString("Town"), ChatColor.GREEN + "Region '" + ChatColor.BOLD + regionID + ChatColor.GREEN + "' was sold!", false, true);
															
															Economy.takeCoins(player.getUniqueId(), cost);
															Economy.giveCoins(owner, cost);
															
															DefaultDomain owners = new DefaultDomain();
															owners.addPlayer(player.getUniqueId());
															region.setOwners(owners); //update owner
															DefaultDomain members = new DefaultDomain();
															region.setMembers(members); //reset members
															
															if(shops.size() > 0) {
																//track number of shops old owner has:
																PlayerConfig oldOwnerConfig = PlayerConfig.getConfig(owner);
																int newAmount = oldOwnerConfig.getInt("Shops") - shops.size();
																if(newAmount == 0)
																	oldOwnerConfig.set("Shops", null);
																else
																	oldOwnerConfig.set("Shops", newAmount);
																oldOwnerConfig.save();
																
																//track number of shops new owner has:
																newOwnerConfig.set("Shops", newAmount1);
																newOwnerConfig.save();
																
																player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.WHITE + "Gained ownership of " + ChatColor.BOLD + shops.size() + ChatColor.WHITE + " shops.");
															}
															player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + "Successfully bought " + regionID + "!");
															
															DeadMC.RegionFile.data().set("" + saleID, null);
															DeadMC.RegionFile.save();
															
														} else {
															//contains X shops
															player.sendMessage("");
															player.sendMessage(ChatColor.YELLOW + "[Shops] " + ChatColor.WHITE + "There are " + ChatColor.BOLD + shops.size() + " shops" + ChatColor.WHITE + " in this region.");
															player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You cannot buy " + regionID + " as it will put you above your shops limit. " + ChatColor.WHITE + ChatColor.BOLD + newAmount1 + ChatColor.WHITE + " / " + ChatColor.BOLD + shopLimit);
															player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to upgrade.");
															player.sendMessage("");
															//you cannot buy this region as it will put you over your shop limit of X shops.
														}
													} else {
														if(confirmBuyRegion.containsKey(player.getName())) {
															player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Please wait for the current session to expire.");
															return;
														}
														confirmBuyRegion.put(player.getName(), saleID);
														
														player.sendMessage("");
														player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Are you sure you want to buy " + ChatColor.AQUA + regionID + ChatColor.WHITE + " (" + ChatColor.AQUA + (region.volume() / 256) + " blocks" + ChatColor.WHITE + ") for " + ChatColor.GOLD + Economy.convertCoins(cost) + ChatColor.WHITE + "?");
														if(shops.size() > 0) {
															player.sendMessage(ChatColor.DARK_RED + "NOTE: " + ChatColor.WHITE + "There are " + ChatColor.BOLD + shops.size() + " shops" + ChatColor.WHITE + " in this region.");
															player.sendMessage(ChatColor.DARK_RED + "NOTE: " + ChatColor.WHITE + "You will gain ownership of these shops.");
														}
														List<String> higherPriorityRegions = higherPriorityRegionsWithin(region);
														if(higherPriorityRegions.size() > 0) {
															player.sendMessage(ChatColor.DARK_RED + "NOTE: There are " + higherPriorityRegions.size() + " regions within this region with a higher building priority. This means you won't have entire control over this region!");
															for(String r : higherPriorityRegions)
																player.sendMessage("  - " + ChatColor.YELLOW + getRegionID(r));
														} else
															player.sendMessage(ChatColor.GREEN + "NOTE: Currently has ALL buildings rights!");
														player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/land buy " + saleID + ChatColor.WHITE + " again to confirm.");
														player.sendMessage("");
														
														Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {
															@Override
															public void run() {
																if(confirmBuyRegion.containsKey(player.getName())) {
																	player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Land buy session expired.");
																	confirmBuyRegion.remove(player.getName());
																}
															}
														}, 300L);
													}
												} else
													player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You don't have " + ChatColor.GOLD + Economy.convertCoins(cost) + ChatColor.WHITE + "!");
											} else
												player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You can't buy your own region.");
										} else
											player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Oh no! This region no longer exists...");
									} else
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You must be a member of " + townDisplayName + " to buy this land.");
								} else
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "'" + saleID + "' is not a valid sale ID.");
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "'" + args[1] + "' is not a valid sale ID.");
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/land buy <Sale ID>" + ChatColor.WHITE + " (find a " + ChatColor.AQUA + "[FOR SALE]" + ChatColor.WHITE + " sign)");
					} else if(args[0].equalsIgnoreCase("spawning")) {
						if(args.length > 2 && (args[2].equalsIgnoreCase("on") || args[2].equalsIgnoreCase("off"))) {
							String regionID = args[1].toLowerCase();
							
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							String regionName = originalTownName + "/" + args[1].toLowerCase();
							if(originalTownName != null && regionManager.hasRegion(regionName) && regionManager.getRegion(regionName).getOwners().getUniqueIds().contains(player.getUniqueId())) {
								
								if(args[2].equalsIgnoreCase("off")) {
									Bukkit.getScheduler().runTask(plugin, new Runnable() {
										@Override
										public void run() {
											Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + regionName + " deny-spawn -w world zombie,zombie_villager,husk,drowned,spider,cave_spider,skeleton,stray,creeper,phantom,ghast");
										}
									});
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Toggled zombie spawning " + ChatColor.RED + ChatColor.BOLD + "OFF" + ChatColor.WHITE + " in '" + regionID + "'.");
								}
								if(args[2].equalsIgnoreCase("on")) {
									Bukkit.getScheduler().runTask(plugin, new Runnable() {
										@Override
										public void run() {
											Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "rg flag " + regionName + " deny-spawn -w world");
										}
									});
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Toggled zombie spawning " + ChatColor.GREEN + ChatColor.BOLD + "ON" + ChatColor.WHITE + " in '" + regionID + "'.");
								}
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You don't own a region named " + ChatColor.YELLOW + regionID.toLowerCase() + ChatColor.RED + ".");
						} else
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/land spawning <region ID> <on/off>");
						
					} else
						player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Unknown command. Try: " + ChatColor.GOLD + "/land help");
					
				}
				
			} catch(Exception e) {
				Chat.logError(e, player);
			}
			
		});
		
		return true;
	}
	
	@EventHandler
	public void onSignUpdate(SignChangeEvent event) {
		Player player = event.getPlayer();
		
		if(event.getLine(0).equalsIgnoreCase("[FOR SALE]") || event.getLine(0).equalsIgnoreCase("[LAND FOR SALE]") || event.getLine(0).equalsIgnoreCase("[SALE]") || event.getLine(0).equalsIgnoreCase("[SELL]")) {
			if(playerOwnsLand(event.getBlock().getLocation(), player.getUniqueId(), false)) {
				
				event.setLine(0, ChatColor.DARK_GREEN + "[LAND FOR SALE]");
				event.setLine(1, "TO BE CONFIGURED");
				event.setLine(2, "Ready to sell?");
				event.setLine(3, "Use: /land sell");
				Sign sign = (Sign) event.getBlock().getState();
				sign.update();
				
				player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Configure the sale sign with " + ChatColor.GOLD + "/land sell" + ChatColor.WHITE + ".");
				
			} else {
				player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You must own the land to do that.");
				event.setCancelled(true);
			}
		}
		
	}
	
	@EventHandler
	public void playerBreakSign(BlockBreakEvent event) {
		if(event.getBlock().getState() instanceof Sign) {
			Sign sign = (Sign) event.getBlock().getState();
			if(sign.getLine(0).equalsIgnoreCase(ChatColor.DARK_GREEN + "[LAND FOR SALE]")) {
				if(!event.isCancelled()) {
					int saleID = Integer.parseInt(sign.getLine(1).substring(9));
					event.getPlayer().sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + getRegionID(DeadMC.RegionFile.data().getString(saleID + ".Region")) + " is no longer for sale!");
					
					DeadMC.RegionFile.data().set("" + saleID, null);
					DeadMC.RegionFile.save();
				}
			}
		}
	}
	
	@EventHandler
	public void playerClickSign(PlayerInteractEvent event) {
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
		
		Player player = event.getPlayer();
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK
				|| event.getAction() == Action.LEFT_CLICK_BLOCK) {
			if(event.getClickedBlock().getState() instanceof Sign) {
				Sign sign = (Sign) event.getClickedBlock().getState();
				if(sign.getLine(0).equalsIgnoreCase(ChatColor.DARK_GREEN + "[LAND FOR SALE]")) {
					if(sign.getLine(1).equalsIgnoreCase("TO BE CONFIGURED")) {
						//configure here
						if(setupSellRegion.containsKey(player.getName())) {
							
							String originalTownName = playerConfig.getString("Town");
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							
							String regionName = originalTownName + "/" + setupSellRegion.get(player.getName());
							ProtectedRegion region = regionManager.getRegion(regionName);
							
							if(regionManager.hasRegion(regionName) && region.getPriority() > 1) {
								if(region.getOwners().getUniqueIds().contains(player.getUniqueId())) {
									if(regionIsAtLocation(regionName, event.getClickedBlock().getLocation())) {
										
										int saleID = DeadMC.RegionFile.data().getInt("SignIDcount") + 1;
										DeadMC.RegionFile.data().set("SignIDcount", saleID);
										DeadMC.RegionFile.save();
										sign.setLine(0, ChatColor.DARK_GREEN + "[LAND FOR SALE]");
										sign.setLine(1, "Sale ID: " + saleID);
										sign.setLine(2, "");
										sign.setLine(3, "CLICK FOR INFO!");
										sign.update();
										
										DeadMC.RegionFile.data().set(saleID + ".Region", regionName);
										DeadMC.RegionFile.data().set(saleID + ".Cost", setupSellRegionCost.get(player.getName()));
										DeadMC.RegionFile.save();
										
										setupSellRegion.remove(player.getName());
										setupSellRegionCost.remove(player.getName());
										
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + getRegionID(regionName) + " is now for sale!");
										
									} else
										player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "The sign must be placed INSIDE region '" + setupSellRegion.get(player.getName()) + "'.");
								} else
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You don't own region '" + regionName + "'.");
							} else
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Cannot sell level 1 priority regions - these belong to the town! " + ChatColor.WHITE + "Define regions within this region to sell.");
							
						} else {
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "This land sale sign is not configured.");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "Are you attempting to configure? Use: " + ChatColor.GOLD + "/land sell");
							player.sendMessage("");
						}
						
					} else {
						//get info
						
						int saleID = Integer.parseInt(sign.getLine(1).substring(9));
						if(DeadMC.RegionFile.data().getString(saleID + ".Region") != null) {
							
							String regionName = DeadMC.RegionFile.data().getString(saleID + ".Region");
							String originalTownName = getOriginalTownName(regionName);
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							String regionID = getRegionID(regionName);
							int cost = DeadMC.RegionFile.data().getInt(saleID + ".Cost");
							
							ProtectedRegion region = regionManager.getRegion(regionName);
							if(regionManager.hasRegion(regionName)) {
								
								if(playerConfig.getString("Town") != null &&
										playerConfig.getString("Town").equalsIgnoreCase(originalTownName)) {
									
									List<String> shops = new ArrayList<String>();
									File regionFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + regionName + ".yml");
									if(regionFile.exists()) {
										RegionConfig regionConfig = RegionConfig.getConfig(regionName);
										shops = regionConfig.getStringList("Shops");
									}
									
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder("FOR SALE: " + regionID, 46) + " FOR SALE: " + regionID + " " + Chat.getTitlePlaceholder("FOR SALE: " + regionID, 46));
									player.sendMessage("Region ID: " + ChatColor.AQUA + regionID);
									player.sendMessage("Region area: " + ChatColor.AQUA + (region.volume() / 256) + " blocks");
									player.sendMessage("Regions within region: " + ChatColor.AQUA + (regionManager.getApplicableRegions(region).size() - 1));
									player.sendMessage("Shops within region: " + ChatColor.AQUA + shops.size());
									for(UUID owner : region.getOwners().getUniqueIds())
										player.sendMessage("Owner: " + ChatColor.AQUA + Bukkit.getOfflinePlayer(owner).getName());
									if(region.getMembers().size() > 0) {
										player.sendMessage("Members:");
										for(UUID member : region.getMembers().getUniqueIds())
											player.sendMessage("  - " + ChatColor.AQUA + Bukkit.getOfflinePlayer(member).getName());
									}
									player.sendMessage("Price to buy: " + ChatColor.GOLD + Economy.convertCoins(cost));
									player.sendMessage("");
									List<String> higherPriorityRegions = higherPriorityRegionsWithin(region);
									if(higherPriorityRegions.size() > 0) {
										player.sendMessage(ChatColor.DARK_RED + "NOTE: There are " + higherPriorityRegions.size() + " regions within this region with a higher building priority. This means you won't have entire control over this region!");
										for(String r : higherPriorityRegions)
											player.sendMessage("  - " + ChatColor.YELLOW + getRegionID(r));
									} else
										player.sendMessage(ChatColor.GREEN + "NOTE: Currently has ALL buildings rights!");
									player.sendMessage("Use " + ChatColor.GOLD + "/land buy " + saleID + ChatColor.WHITE + " (the sale ID) to buy.");
									player.sendMessage(ChatColor.YELLOW + "===============================================");
									player.sendMessage("");
									
								} else
									player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "You must be a member of " + townDisplayName + " to buy this land.");
								
							} else {
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Oh no! This region no longer exists...");
								/* clear sign */
								sign.setLine(0, "");
								sign.setLine(1, "");
								sign.setLine(2, "");
								sign.setLine(3, "");
								sign.update();
								DeadMC.RegionFile.data().set(saleID + "", null);
								DeadMC.RegionFile.save();
							}
						} else {
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "This land is no longer for sale.");
							/* clear sign */
							sign.setLine(0, "");
							sign.setLine(1, "");
							sign.setLine(2, "");
							sign.setLine(3, "");
							sign.update();
						}
						
					}
				}
			}
		}
	}
	
	
	public static boolean regionIsAtLocation(String regionName, Location location) {
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(location.getWorld()));
		for(ProtectedRegion region : regionManager.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()))) {
			if(regionName.equalsIgnoreCase(region.getId())) return true;
		}
		return false;
	}
	
	public ProtectedCuboidRegion getSelection(Player player, String potentialRegionName) {
		WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
		
		try {
			LocalSession session = worldEdit.getSession(player);
			Region selection = session.getSelection(session.getSelectionWorld());
			
			if(!session.getSelectionWorld().getName().equalsIgnoreCase("world")) return null;
			
			selection.expand(BlockVector3.UNIT_Y.multiply(448));
			selection.expand(BlockVector3.UNIT_MINUS_Y.multiply(448));
			
			return new ProtectedCuboidRegion(potentialRegionName, selection.getMinimumPoint(), selection.getMaximumPoint());
			
		} catch(NullPointerException | IncompleteRegionException | RegionOperationException e) {
			return null;
		}
		
	}
	
	//check if player has building rights at location
	public static Boolean playerCanBuild(Player player, Location location, boolean includeWildLand) {
		ProtectedRegion highestPriority = Regions.getHighestPriorityLand(location);
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
		Boolean canBuild = (includeWildLand && highestPriority == null) //is only wild land (no regions)
				|| (highestPriority != null && (regionManager.getRegion(highestPriority.getId()).getMembers().getUniqueIds().contains(player.getUniqueId()) || regionManager.getRegion(highestPriority.getId()).getOwners().getUniqueIds().contains(player.getUniqueId())));
		return canBuild;
	}
	
	//deprecated as only searches regions in players town
	// searches for regions in player's town owned by player or is member of
//	public static List<ProtectedRegion> getRegionsPlayerCanBuildAt(UUID uuid) {
//		
//		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
//		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
//		String originalTownName = playerConfig.getString("Town");
//		String townDisplayName = Town.getTownDisplayName(originalTownName);
//		TownConfig townConfig = TownConfig.getConfig(originalTownName);
//		
//		List<ProtectedRegion> regions = new ArrayList<ProtectedRegion>();
//
//		for(String regionID : townConfig.getStringList("Regions")) {
//			
//			String regionName = originalTownName + "/" + regionID;
//			ProtectedRegion region = regionManager.getRegion(regionName);
//			
//			Boolean canBuild = regionManager.getRegion(regionID).getMembers().getUniqueIds().contains(uuid) || regionManager.getRegion(regionID).getOwners().getUniqueIds().contains(uuid) ? true : false;
//			if(canBuild) {
//				regions.add(region);
//			}
//			
//		}
//		
//		Chat.broadcastMessage("" + regions);
//		return regions;
//	}
//	public static List<String> getRegionIDsPlayerCanBuildAt(UUID uuid) {
//		PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
//		
//		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
//		List<String> regionIDs = new ArrayList<String>();
//		
//		String originalTownName = playerConfig.getString("Town");
//		TownConfig townConfig = TownConfig.getConfig(originalTownName);
//		for(String regionID : townConfig.getStringList("Regions")) {
//			
//			String regionName = originalTownName + "/" + regionID;
//			ProtectedRegion region = regionManager.getRegion(regionName);
//			
//			for (UUID ownerUUID : region.getOwners().getUniqueIds()) {
//	            if(ownerUUID.toString().equalsIgnoreCase(uuid.toString()) && !regionIDs.contains(regionID)) {
//	            	regionIDs.add(regionID); break;
//	            }
//	        }
//			
//			for (UUID memberUUID : region.getMembers().getUniqueIds()) {
//	            if(memberUUID.toString().equalsIgnoreCase(uuid.toString()) && !regionIDs.contains(regionID)) {
//	            	regionIDs.add(regionID); break;
//	            }
//	        }
//			
//		}
//			
//		return regionIDs;
//	}
	
	public static boolean playerOwnsLand(ProtectedCuboidRegion selection, UUID uuid, boolean includeWildLand) {
		
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
		ApplicableRegionSet regionsWithinSelection = regionManager.getApplicableRegions(selection);
		
		if(!includeWildLand && regionsWithinSelection.size() == 0) return false;
		
		//get highest priority land
		int highestPriority = 0;
		List<ProtectedRegion> highestPriorityRegions = new ArrayList<ProtectedRegion>();
		
		for(ProtectedRegion region : regionsWithinSelection) {
			
			//longdale land:
			if(region.getPriority() == 0) {
				return false;
			}
			
			if(region.getPriority() > highestPriority) {
				highestPriorityRegions.clear();
				highestPriorityRegions.add(region);
				highestPriority = region.getPriority();
				continue;
			}
			
			if(region.getPriority() == highestPriority)
				highestPriorityRegions.add(region);
			
		}
		
		for(ProtectedRegion region : highestPriorityRegions) {
			if(region.getPriority() > 0 && !region.getOwners().getUniqueIds().contains(uuid))
				return false;
		}
		
		return true;
		
	}
	
	public static boolean townOwnsLand(Location location, String originalTownName) {
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		List<String> townRegionIDs = townConfig.getString("Regions") != null ? townConfig.getStringList("Regions") : new ArrayList<String>();
		
		ApplicableRegionSet regionsAtLoc = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world"))).getApplicableRegions(BukkitAdapter.asBlockVector(location));
		if(regionsAtLoc.size() == 0) return false;
		
		for(ProtectedRegion region : regionsAtLoc) {
			if(region.getPriority() == 0 || !townRegionIDs.contains(getRegionID(region.getId()))) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean playerOwnsLand(Location location, UUID uuid, boolean includeWildLand) {
		
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(location.getWorld()));
		ApplicableRegionSet regionsAtLocation = regionManager.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()));
		
		if(includeWildLand && regionsAtLocation.size() == 0) return true;
		
		//get highest priority land
		int highestPriority = 0;
		ProtectedRegion highestPriorityRegion = null;
		
		for(ProtectedRegion region : regionsAtLocation) {
			
			if(region.getPriority() >= highestPriority) {
				highestPriorityRegion = region;
				highestPriority = region.getPriority();
			}
			
		}
		
		if(highestPriorityRegion == null) { //there's no regions
			if(includeWildLand)
				return true;
			else
				return false;
		} else if(highestPriorityRegion.getPriority() == 0 || !highestPriorityRegion.getOwners().getUniqueIds().contains(uuid))
			return false;
		else
			return true;
	}
	
	public List<String> higherPriorityRegionsWithin(ProtectedRegion regionToCheck) {
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
		
		//get higher priority land
		List<String> higherRegions = new ArrayList<String>();
		for(ProtectedRegion region : regionManager.getApplicableRegions(regionToCheck)) {
			if(region.getPriority() > regionToCheck.getPriority())
				higherRegions.add(region.getId());
		}
		
		return higherRegions;
	}
	
	public static int highestPriorityLand(Set<ProtectedRegion> regionsToCheck) {
		
		//get highest priority land
		int highestPriority = 0;
		for(ProtectedRegion region : regionsToCheck) {
			
			if(region.getPriority() > highestPriority)
				highestPriority = region.getPriority();
		}
		
		return highestPriority;
	}
	
	
	public static int highestPriorityLand(Location location) {
		
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(location.getWorld()));
		ApplicableRegionSet regionsAtLocation = regionManager.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()));
		
		//get highest priority land
		int highestPriority = 0;
		for(ProtectedRegion region : regionsAtLocation) {
			
			if(region.getPriority() > highestPriority) {
				highestPriority = region.getPriority();
			}
			
		}
		
		return highestPriority;
	}
	
	public static ProtectedRegion getHighestPriorityLand(Location location) {
		
		RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(location.getWorld()));
		ApplicableRegionSet regionsAtLocation = regionManager.getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()));
		
		//get highest priority land
		ProtectedRegion highestPriority = null;
		for(ProtectedRegion region : regionsAtLocation) {
			
			if(highestPriority == null || region.getPriority() > highestPriority.getPriority()) {
				highestPriority = region;
			}
			
		}
		
		return highestPriority;
	}
	
	public static OfflinePlayer getOwnerAtLocation(Location location) {
		ProtectedRegion highestPriorityLand = Regions.getHighestPriorityLand(location);
		if(highestPriorityLand != null) { //if land exists at location
			for(UUID uuid : highestPriorityLand.getOwners().getUniqueIds())
				return Bukkit.getOfflinePlayer(uuid);
		}
		return null;
	}
	
	//get town name from region name
	public static String getOriginalTownName(String regionName) {
		int end = 0;
		
		String townName = "";
		for(int count = 0; count < regionName.length(); count++) {
			townName = regionName.substring(0, count);
			if(townName.contains("/")) {
				end = count - 1;
				break;
			}
		}
		
		return Town.townNameCased(regionName.substring(0, end));
	}
	
	//get town name from region name
	public static String getRegionID(String regionName) {
		int start = 0;
		
		String townName = "";
		for(int count = 0; count < regionName.length(); count++) {
			townName = regionName.substring(0, count);
			if(townName.contains("/")) {
				start = count;
				break;
			}
		}
		
		return regionName.substring(start, regionName.length());
	}
	
	
	//region event messages
	
	@EventHandler
	public void onEnterRegion(RegionEnteredEvent event) {
		Player player = event.getPlayer();
		ProtectedRegion region = event.getRegion();
		
		//set flying mode:
		Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				if(playerConfig.getInt("DonateRank") >= Donator.Rank.Royal.ordinal() && !player.getAllowFlight() && !player.isOp() && !Admin.spectating.contains(player.getName()) && player.getGameMode() == GameMode.SURVIVAL
						&& Regions.playerCanBuild(player, player.getLocation(), false)) {
					player.setAllowFlight(true);
				}
			}
		}, 5L);
		
		if(region.getPriority() > 1)
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GREEN + "Entered region " + ChatColor.YELLOW + ChatColor.BOLD + getRegionID(region.getId()) + ChatColor.GREEN + ".");
		else if(region.getPriority() == 1 && !playersInTownRegions.contains(player.getName())) {
			playersInTownRegions.add(player.getName());
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.GREEN + "Entered " + ChatColor.YELLOW + ChatColor.BOLD + Town.getTownDisplayName(getOriginalTownName(region.getId())) + ChatColor.GREEN + " territory.");
		} else if(event.getRegion().getId().equalsIgnoreCase("longdale"))
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "Entered Longdale territory.");
		else if(event.getRegion().getId().equalsIgnoreCase("market"))
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "" + ChatColor.ITALIC + "Welcome!" + ChatColor.YELLOW + " from the Longdale market.");
	}
	
	private List<String> worldeditMessageTimer = new ArrayList<String>();
	
	@EventHandler
	public void onPlayerUseClaimTool(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		if((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK)
				&& player.getInventory().getItemInMainHand().getType() == Material.WOODEN_AXE
				&& playerConfig.getString("Town") != null
				&& !player.isOp()
				&& !player.getWorld().getName().equalsIgnoreCase("revamp")) {
			
			try {
				WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
				LocalSession session = worldEdit.getSession(player);
				Region selection = session.getSelection(session.getSelectionWorld());
				
				//update the selection visulizer
				selection.expand(BlockVector3.UNIT_Y.multiply(448));
				selection.expand(BlockVector3.UNIT_MINUS_Y.multiply(448));
				
				if(!worldeditMessageTimer.contains(player.getName())) {
					
					worldeditMessageTimer.add(player.getName());
					//ensure message doesn't send multiple times
					Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
						@Override
						public void run() {
							if(worldeditMessageTimer.contains(player.getName())) {
								worldeditMessageTimer.remove(player.getName());
							}
						}
					}, 1); // 1 tick
					
					//claiming land, show how many free blocks
					RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(Bukkit.getWorld("world")));
					
					if(playerConfig.getString("Town") != null
							&& playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()
							&& regionManager.getApplicableRegions(getSelection(player, player.getName())).getRegions().size() == 0) {
						//claiming land
						TownConfig townConfig = TownConfig.getConfig(playerConfig.getString("Town"));
						int blocksClaimable = (townConfig.getInt("Limit.Claim") * townConfig.getInt("Limit.Claim")) - townConfig.getInt("ClaimedBlocks");
						long size = selection.getVolume() / 256;
						if(size <= blocksClaimable) {
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + "" + ChatColor.BOLD + size + " blocks" + ChatColor.WHITE + " / " + ChatColor.GREEN + blocksClaimable + " blocks");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + "Use " + ChatColor.GOLD + "/land claim" + ChatColor.GREEN + " to claim this land.");
							
							//TASK
							if(TaskManager.playerHasStep_TUTORIAL(player.getUniqueId(), TaskStep.SELECT_LAND_TO_CLAIM))
								TaskManager.stepTask(player.getUniqueId());
						} else {
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Land] " + ChatColor.RED + "" + ChatColor.BOLD + size + " blocks" + ChatColor.WHITE + " / " + ChatColor.RED + blocksClaimable + " blocks");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Selection too big! Use " + ChatColor.GOLD + "/town upgrade claim-limit" + ChatColor.RED + ".");
						}
					} else if(playerConfig.getString("Town") != null) {
						
						//defining land
						long size = selection.getVolume() / 256;
						if(playerOwnsLand(getSelection(player, player.getName()), player.getUniqueId(), false)) {
							//show green if player owns the land
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + "" + ChatColor.BOLD + size + " blocks");
							player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.GREEN + "Use " + ChatColor.GOLD + "/land " + ChatColor.BOLD + "define" + ChatColor.GOLD + " <name>" + ChatColor.GREEN + " to split this land.");
						} else {
							Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Land] " + ChatColor.RED + ChatColor.BOLD + "You don't own all this land!");
							if(playerConfig.getInt("TownRank") >= Rank.Co_Mayor.ordinal()) {
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "To " + ChatColor.BOLD + "claim" + ChatColor.WHITE + ", ensure the selection only contains wild land.");
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.WHITE + "To " + ChatColor.BOLD + "define" + ChatColor.WHITE + ", ensure you own all the land in the selection.");
							} else {
								//show red if player doesn't own the land
								player.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "Ensure you own all the land in the selection.");
							}
						}
					}
				}
				
			} catch(NullPointerException | IncompleteRegionException | RegionOperationException e) {
			
			}
		}
	}
	
	@EventHandler
	public void onExitRegion(RegionLeftEvent event) {
		Player player = event.getPlayer();
		ProtectedRegion region = event.getRegion();
		
		if(region.getPriority() == 1) {
			if(!playersWhoCouldHaveLeftTown.containsKey(player.getName()))
				playersWhoCouldHaveLeftTown.put(player.getName(), Town.getTownDisplayName(getOriginalTownName(region.getId())));
			
		} else if(region.getPriority() > 1)
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.RED + "Exited region " + ChatColor.YELLOW + ChatColor.BOLD + getRegionID(region.getId()) + ChatColor.RED + ".");
		
		else if(event.getRegion().getId().equalsIgnoreCase("longdale"))
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "Exited Longdale territory.");
		else if(event.getRegion().getId().equalsIgnoreCase("market"))
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "" + ChatColor.ITALIC + "Farewell!" + ChatColor.YELLOW + " from the Longdale market.");
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		
		if(player.getWorld().getEnvironment() == org.bukkit.World.Environment.NETHER && player.getLocation().getY() >= 128.0) {
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.DARK_RED + "[DeadMC] " + ChatColor.RED + "Cannot go on nether roof!");
			
			event.setCancelled(true);
		}
		
		//check if left territory
		if(playersWhoCouldHaveLeftTown.containsKey(player.getName()) && playersWhoCouldHaveLeftTown.get(player.getName()) != null && !(playersWhoCouldHaveLeftTown.get(player.getName()).equalsIgnoreCase("undefined"))) {
			RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
			ApplicableRegionSet regionsAtLocation = regionManager.getApplicableRegions(BlockVector3.at(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ()));
			if(regionsAtLocation.size() == 0) {
				//left territory
				Chat.sendTitleMessage(player.getUniqueId(), ChatColor.RED + "Exited " + ChatColor.YELLOW + ChatColor.BOLD + playersWhoCouldHaveLeftTown.get(player.getName()) + ChatColor.RED + " territory.");
				playersInTownRegions.remove(player.getName());
				playersWhoCouldHaveLeftTown.remove(player.getName());
			}
		}
		
		//run async:
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				if(player.getAllowFlight() && !player.isOp() && !Admin.spectating.contains(player.getName()) && player.getGameMode() == GameMode.SURVIVAL) {
					
					//player no longer member of highest priority land
					if(!Regions.playerCanBuild(player, player.getLocation(), false)) {
						
						if(player.isFlying()) {
							Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
								@Override
								public void run() {
									Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Fly] " + ChatColor.RED + "Exited owned land - Flying mode disabled.");
								}
							}, 5L);
							
							player.setFlying(false);
						}
						
						player.setAllowFlight(false);
					}
					
				}
			}
		});
		
	}
	
	
}
