package com.deadmc.DeadAPImaven;

import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.deadmc.DeadAPImaven.Towns.Town;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.md_5.bungee.api.ChatColor;
import com.deadmc.DeadAPImaven.Task.TaskStep;

public class PlaceHolders extends PlaceholderExpansion {
	private DeadMC plugin;
	
	public PlaceHolders(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	public static HashMap<String, String> playersName = new HashMap<String, String>();
	public static HashMap<String, Integer> playersCoins = new HashMap<String, Integer>();
	public static HashMap<String, Integer> player_BMKills = new HashMap<String, Integer>();
	public static HashMap<String, String> player_TaskStreak = new HashMap<String, String>();
	public static HashMap<String, String> player_TaskString = new HashMap<String, String>();
	public static HashMap<String, String> player_TownString1 = new HashMap<String, String>();
	public static HashMap<String, String> player_TownString2 = new HashMap<String, String>();
	public static HashMap<String, String> player_TownName = new HashMap<String, String>();
	public static HashMap<String, String> player_TownRank = new HashMap<String, String>();
	public static String totalZombieKills = null;
	public static String totalTowns = null;
	public static int totalDeaths = 0;
	public static int totalCoinsInCirculation = 0;
	
	@Override
	public boolean persist() {
		return true;
	}
	
	@Override
	public boolean canRegister() {
		return true;
	}
	
	@Override
	public String getAuthor() {
		return plugin.getDescription().getAuthors().toString();
	}
	
	@Override
	public String getIdentifier() {
		return "deadmc";
	}
	
	@Override
	public String getVersion() {
		return plugin.getDescription().getVersion();
	}
	
	DecimalFormat formatter = new DecimalFormat("#,###");
	
	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		
		String uuid = "";
		if(player != null)
			uuid = player.getUniqueId().toString();
		
		// %deadmc_name%
		if(identifier.equals("name")) {
			if(!playersName.containsKey(uuid)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
				if(playerConfig.getString("Name") == null)
					playersName.put(player.getUniqueId().toString(), player.getName());
				else playersName.put(player.getUniqueId().toString(), playerConfig.getString("Name"));
			}
			return playersName.get(uuid);
		}
		
		if(identifier.equals("coins")) {
			if(!playersCoins.containsKey(uuid)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
				if(playerConfig.getString("Coins") == null) return "" + Economy.convertCoins(0);
				else playersCoins.put(uuid, playerConfig.getInt("Coins"));
			}
			return "" + Economy.convertCoins(playersCoins.get(uuid));
		}
		
		if(identifier.equals("townstring1")) {
			if(!player_TownString1.containsKey(uuid)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
				if(playerConfig.getString("Town") != null) {
					if(playerConfig.getString("TownRank") != null)
						player_TownString1.put(uuid.toString(), "You are a " + Town.Rank.values()[playerConfig.getInt("TownRank")].toString().replace("_", "") + " of " + ChatColor.AQUA + Town.getTownDisplayName(player.getUniqueId()) + ChatColor.WHITE + ".");
					else
						player_TownString1.put(uuid.toString(), "You are a Citizen of " + Town.getTownDisplayName(player.getUniqueId()) + ".");
				} else player_TownString1.put(uuid.toString(), "You are not a member of a town.");
			}
			
			return player_TownString1.get(uuid);
		}
		if(identifier.equals("townstring2")) {
			if(!player_TownString2.containsKey(uuid)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
				if(playerConfig.getString("Town") != null) {
					TownConfig townConfig = TownConfig.getConfig(playerConfig.getString("Town"));
					int tax = townConfig.getInt("Tax");
					
					if(tax > 0) {
						int taxDue = playerConfig.getInt("TownFundsOwed") - playerConfig.getInt("TownFundsAdded");
						if(taxDue > 0 && Town.Rank.values()[playerConfig.getInt("TownRank")] != Town.Rank.Mayor)
							player_TownString2.put(uuid.toString(), ChatColor.RED + "(!) " + ChatColor.WHITE + "Tax due: " + ChatColor.GOLD + Economy.convertCoins(taxDue));
						else
							player_TownString2.put(uuid.toString(), ChatColor.WHITE + "Tax due: " + ChatColor.GREEN + "All paid!");
						
					} else player_TownString2.put(uuid.toString(), "Tax due: NA (no tax)");
				} else
					player_TownString2.put(uuid.toString(), "Use " + ChatColor.GOLD + "/town" + ChatColor.WHITE + " for help.");
			}
			
			return player_TownString2.get(uuid);
		}
		
		if(identifier.equals("taskstring")) {
			if(!player_TaskString.containsKey(uuid)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
				if(playerConfig.getString("Task") == null) {
					player_TaskString.put(uuid, "No task assigned.");
				} else {
					if(playerConfig.getInt("Task") == 0) {
						int step = playerConfig.getInt("Step");
						String name = TaskStep.values()[step].toString().charAt(0) + TaskStep.values()[step].toString().toLowerCase().replace("_", " ").substring(1, TaskStep.values()[step].toString().length());
						player_TaskString.put(uuid, ChatColor.AQUA + name);
					} else {
						
						int ID = playerConfig.getInt("Task");
						String name = DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + ".Task name");
						player_TaskString.put(uuid, ChatColor.AQUA + name);
					}
				}
			}
			
			return player_TaskString.get(uuid);
			
		}
		if(identifier.equals("taskstreak")) {
			if(!player_TaskStreak.containsKey(uuid)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
				if(playerConfig.getString("Task") != null) {
					
					if(playerConfig.getInt("Task") == 0) {
						player_TaskStreak.put(uuid, "Complete the tutorial for a reward!");
					} else {
						int track = 0;
						if(playerConfig.getString("Track") != null)
							track += playerConfig.getInt("Track");
						
						int ID = playerConfig.getInt("Task");
						int step = playerConfig.getInt("Step");
						int goal = DeadMC.TaskFile.data().getInt("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".Amount");
						
						player_TaskStreak.put(uuid, "Progress: " + ChatColor.GREEN + ChatColor.BOLD + track + ChatColor.WHITE + " / " + ChatColor.BOLD + goal);
					}
				} else {
					if(playerConfig.getString("Streak") == null) {
						player_TaskStreak.put(uuid, "Streak: 0");
					} else
						player_TaskStreak.put(uuid, "Streak: " + ChatColor.BOLD + playerConfig.getInt("Streak"));
				}
			}
			
			return player_TaskStreak.get(uuid);
			
		}
		if(identifier.equals("townrank")) {
			if(!player_TownRank.containsKey(uuid)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
				if(playerConfig.getString("Town") != null)
					player_TownRank.put(uuid, Town.getTownRankPrefix(player.getUniqueId()));
				else player_TownRank.put(uuid, "");
			}
			
			return player_TownRank.get(uuid);
		}
		
		if(identifier.equals("townname")) {
			if(!player_TownName.containsKey(uuid)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(uuid));
				if(playerConfig.getString("Town") != null)
					player_TownName.put(uuid, Town.getTownDisplayName(player.getUniqueId()));
				else player_TownName.put(uuid, "");
			}
			
			return player_TownName.get(uuid);
		}
		
		if(identifier.equals("isbloodmoon")) {
			return new String("" + BloodMoon.isBloodMoon());
		}
		if(identifier.equals("timeuntilbloodmoon")) {
			return new String("" + BloodMoon.timeUntilBloodMoonFormatted());
		}
		if(identifier.equals("motd_ending")) {
			if(BloodMoon.isBloodMoon)
				return new String("&4&lBlood Moon");
			return new String("to &4&lBM");
		}
		if(identifier.equals("timeleftinnight")) {
			return new String("" + BloodMoon.timeLeftInNight());
		}
		if(identifier.equals("bloodmoonprogress")) {
			return new String("" + BloodMoon.bloodMoonProgress());
		}
		if(identifier.equals("bloodmooncurrentkills")) {
			if(BloodMoon.isBloodMoon) {
				if(player_BMKills.containsKey(uuid)) return "" + player_BMKills.get(uuid);
				else return "0";
			} else return "0";
		}
		if(identifier.equals("leadingkiller1")) {
			if(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 1) == null) return "";
			
			int amount = DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 1 + ".Amount");
			OfflinePlayer leader = Bukkit.getOfflinePlayer(UUID.fromString(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 1 + ".UUID")));
			String leaderName = leader.getName();
			
			PlayerConfig leaderConfig = PlayerConfig.getConfig(leader);
			if(leaderConfig.getString("Name") != null) leaderName = leaderConfig.getString("Name");
			
			//if(leader.getName().equalsIgnoreCase(player.getName())) return new String(ChatColor.GOLD + "" + ChatColor.BOLD + "Me" + ChatColor.WHITE + ChatColor.BOLD + " <--");
			return new String(ChatColor.GOLD + leaderName + ChatColor.WHITE + " (" + ChatColor.AQUA + amount + ChatColor.WHITE + (Tools.playerIsJava(player) ? " kills" : "") + ")");
		}
		if(identifier.equals("leadingkiller2")) {
			if(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 2) == null) return "";
			
			int amount = DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 2 + ".Amount");
			OfflinePlayer leader = Bukkit.getOfflinePlayer(UUID.fromString(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 2 + ".UUID")));
			String leaderName = leader.getName();
			
			PlayerConfig leaderConfig = PlayerConfig.getConfig(leader);
			if(leaderConfig.getString("Name") != null) leaderName = leaderConfig.getString("Name");
			
			//if(leader.getName().equalsIgnoreCase(player.getName())) return new String(ChatColor.WHITE + "" + ChatColor.BOLD + "Me" + ChatColor.WHITE + ChatColor.BOLD + " <--");
			return new String(leaderName + ChatColor.WHITE + " (" + ChatColor.AQUA + amount + ChatColor.WHITE + (Tools.playerIsJava(player) ? " kills" : "") + ")");
		}
		if(identifier.equals("leadingkiller3")) {
			if(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 3) == null) return "";
			
			int amount = DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 3 + ".Amount");
			OfflinePlayer leader = Bukkit.getOfflinePlayer(UUID.fromString(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 3 + ".UUID")));
			String leaderName = leader.getName();
			
			PlayerConfig leaderConfig = PlayerConfig.getConfig(leader);
			if(leaderConfig.getString("Name") != null) leaderName = leaderConfig.getString("Name");
			
			//if(leader.getName().equalsIgnoreCase(player.getName())) return new String(ChatColor.WHITE + "" + ChatColor.BOLD + "Me" + ChatColor.WHITE + ChatColor.BOLD + " <--");
			return new String(leaderName + ChatColor.WHITE + " (" + ChatColor.AQUA + amount + ChatColor.WHITE + (Tools.playerIsJava(player) ? " kills" : "") + ")");
		}
		
		if(identifier.equals("bloodmoonloot")) {
			if(DeadMC.LeaderboardFile.data().getString("CURRENTBLOODMOON." + 1) == null
					|| DeadMC.LeaderboardFile.data().getString("CurrentTotalKills") == null)
				return "0 coins";
			
			//calculate rewards:
			int totalKills = DeadMC.LeaderboardFile.data().getInt("CurrentTotalKills");
			int top3kills = DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 1 + ".Amount") + DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 2 + ".Amount") + DeadMC.LeaderboardFile.data().getInt("CURRENTBLOODMOON." + 3 + ".Amount");
			double multiplier = 1 + ((totalKills - top3kills) / totalKills);
			int endLoot = (int) (totalKills * multiplier);
			
			return new String("" + Economy.convertCoins(endLoot));
		}
		if(identifier.equals("bartext")) {
			if(BloodMoon.isBloodMoon)
				return new String(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + ": " + BloodMoon.timeLeftInNight());
			return new String(BloodMoon.timeUntilBloodMoonFormatted() + " until " + ChatColor.DARK_RED + ChatColor.BOLD + "Blood Moon" + ChatColor.WHITE + ".");
		}
		if(identifier.equals("barcolour")) {
			if(BloodMoon.isBloodMoon) return "RED";
			return "WHITE";
		}
		
		if(identifier.equalsIgnoreCase("totalzombiekills")) {
			if(totalZombieKills == null) {
				int totalKills = DeadMC.LeaderboardFile.data().getInt("TotalServerKills");
				return formatter.format(totalKills);
			} else {
				return totalZombieKills;
			}
		}
		
		if(identifier.equalsIgnoreCase("totaltowns")) {
			if(totalTowns == null) {
				int totalTowns = DeadMC.TownFile.data().getStringList("Active").size();
				return formatter.format(totalTowns);
			} else {
				return "" + totalTowns;
			}
		}
		
		if(identifier.equalsIgnoreCase("totalcurrency")) {
			return new String(formatter.format(totalCoinsInCirculation));
		}
		
		if(identifier.equalsIgnoreCase("totaldeaths")) {
			return new String(formatter.format(totalDeaths));
		}
		
		if(identifier.equalsIgnoreCase("secretsanta_time")) {
			ZonedDateTime chicagoTime = ZonedDateTime.now(ZoneId.of("America/Chicago"));
			ZonedDateTime christmasDay = ZonedDateTime.of(2021, 12, 25, 0, 0, 0, 0, ZoneId.of("America/Chicago"));

			if(chicagoTime.isAfter(christmasDay)) {
				//is past christmas eve
				return new String(ChatColor.WHITE + "" + ChatColor.GOLD + ChatColor.BOLD + "Unlocked!");
			} else {
				int days = (int) ChronoUnit.DAYS.between(chicagoTime, christmasDay);
				int dayHours = days*24;
				int hours = (int) ChronoUnit.HOURS.between(chicagoTime, christmasDay) - dayHours;

				return new String(ChatColor.WHITE + "Unlocks in " + ChatColor.BOLD + days + " days " + hours + " hours");
			}
		}
		if(identifier.equalsIgnoreCase("secretsanta_presents")) {
	                PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
	                List<ItemStack> gifts = new ArrayList<ItemStack>();
	                if(playerConfig.getString("Gifts") != null) gifts = (List<ItemStack>) playerConfig.get("Gifts");
	
	                if(gifts.size() > 0) {
	                        return new String(ChatColor.WHITE + "You have " + ChatColor.GOLD + ChatColor.BOLD + gifts.size() + ChatColor.WHITE + " presents waiting!");
	                } else {
				return new String(ChatColor.BOLD + "Happy Holidays!");
	                }
		}
		
		// We return null if an invalid placeholder (f.e. %someplugin_placeholder3%)
		// was provided
		return null;
	}
	
}
