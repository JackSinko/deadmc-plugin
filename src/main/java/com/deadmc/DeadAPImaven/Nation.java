package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Towns.Town;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.Permission;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Role;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import github.scarsz.discordsrv.util.DiscordUtil;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.awt.*;
import java.io.File;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.List;

public class Nation implements CommandExecutor {
	private static DeadMC plugin;
	public Nation(DeadMC plugin) {
		this.plugin = plugin;
	}

	//to create nation discord, get allow each town role access to the channel
	
	private List<String> confirmLeave = new ArrayList<String>();
	private List<String> confirmTransfer = new ArrayList<String>();
	Map<String, List<String>> invites = new HashMap<String, List<String>>(); // nationName, list of player names with invites
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		final Player player = (Player) sender;
		try {
			//run async
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				if(commandLabel.equalsIgnoreCase("nation") || commandLabel.equalsIgnoreCase("n")) {
					
					if(args.length == 0) {
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "================ Nation Commands ================");
						player.sendMessage(ChatColor.WHITE + "Nations allow mayors to ally with other towns.");
						player.sendMessage("");
						player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Existing nations:");
						player.sendMessage(ChatColor.GOLD + "/nation list" + ChatColor.WHITE + " - A ranking list of all nations.");
						player.sendMessage(ChatColor.GOLD + "/nation lookup <nation>" + ChatColor.WHITE + " - Lookup nation stats.");
						
						String originalTownName = playerConfig.getString("Town");
						if(originalTownName != null) {
							TownConfig townConfig = TownConfig.getConfig(originalTownName);
							String townDisplayName = Town.getTownDisplayName(originalTownName);
							boolean isMayor = playerConfig.getInt("TownRank") >= Town.Rank.Mayor.ordinal();
							
							String nationName = townConfig.getString("Nation");
							if(nationName != null) {
								//is a part of a nation
								NationConfig nationConfig = NationConfig.getConfig(nationName);
								
								player.sendMessage("");
								player.sendMessage(ChatColor.AQUA + townDisplayName + " is a member of the " + nationName + " nation.");
								player.sendMessage(ChatColor.GOLD + "/nc <message>" + ChatColor.WHITE + " - Nation chat.");
								
								if(isMayor) {
									player.sendMessage(ChatColor.GOLD + "/nation leave" + ChatColor.WHITE + " - Leave the " + nationName + " nation.");
									
									boolean isCapitalTown = nationConfig.getString("Capital").equalsIgnoreCase(originalTownName);
									if(isCapitalTown) {
										//is the nation leader (mayor of the capital town)
										player.sendMessage(ChatColor.GOLD + "/nation invite" + ChatColor.WHITE + " - Invite a town to join " + nationName + ".");
										player.sendMessage(ChatColor.GOLD + "/nation banish" + ChatColor.WHITE + " - Banish a town from " + nationName + ".");
										player.sendMessage(ChatColor.GOLD + "/nation set description" + ChatColor.WHITE + " - A brief description of your nation");
										player.sendMessage(ChatColor.GOLD + "/nation set recruiting" + ChatColor.WHITE + " - Manage how towns can join " + nationName + ".");
										player.sendMessage(ChatColor.GOLD + "/nation set capital" + ChatColor.WHITE + " - Set the capital town.");
									}
								}
							} else {
								//not in a nation
								if(isMayor) {
									player.sendMessage("");
									player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Join or Create a nation:");
									player.sendMessage(ChatColor.GOLD + "/nation create" + ChatColor.WHITE + " - Create a nation.");
									player.sendMessage(ChatColor.GOLD + "/nation join" + ChatColor.WHITE + " - Join a nation.");
								}
							}
						}
						player.sendMessage(ChatColor.YELLOW + "================================================");
						player.sendMessage("");
						return;
					}
					
					if(args[0].equalsIgnoreCase("list")) {
						int nationsPerPage = 10;
						int pageNumber = 0;
						if(args.length > 1 && Chat.isInteger(args[1])) {
							pageNumber = Integer.parseInt(args[1]) - 1;
						} else {
							if(args.length > 1) {
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/nation list <page number>");
								return;
							}
						}
						
						List<String> nationsRanked = getNationsRanked();
						
						int maxTownToInclude = (nationsPerPage + (pageNumber * nationsPerPage)) - 1; //start at page 0
						int minTownToInclude = pageNumber * nationsPerPage;
						//0 - 9
						// 10 - 19
						
						int numberOfPages = (int) Math.ceil(new Double(nationsRanked.size() / nationsPerPage));
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder("List of active nations", 46) + " List of active nations " + Chat.getTitlePlaceholder("List of active nations", 46));
						player.sendMessage(ChatColor.WHITE + "There are " + ChatColor.GOLD + ChatColor.BOLD + nationsRanked.size() + ChatColor.WHITE + " active DeadMC nations!");
						player.sendMessage("");
						if(pageNumber > numberOfPages)
							player.sendMessage(ChatColor.RED + "   Page #" + (pageNumber + 1) + " does not exist.");
						int rank = 1;
						for(int count = minTownToInclude; count <= maxTownToInclude; count++) {
							if(nationsRanked.size() <= count) break;
							String nationName = nationsRanked.get(count);
							int actualRank = rank + (pageNumber * nationsPerPage);
							
							NationConfig nationConfig = NationConfig.getConfig(nationName);
							
							String recruitStatus = ChatColor.GREEN + "OPEN";
							Town.RecruitStatus status = Town.RecruitStatus.values()[nationConfig.getInt("Recruiting")];
							if(status == Town.RecruitStatus.CLOSED)
								recruitStatus = ChatColor.RED + "CLOSED";
							if(status == Town.RecruitStatus.INVITE_ONLY)
								recruitStatus = ChatColor.YELLOW + "INVITE-ONLY";
							
							List<UUID> members = getMembers(nationName);
							player.sendMessage("" + ChatColor.WHITE + ChatColor.BOLD + actualRank + ChatColor.WHITE + ". " + ChatColor.GOLD + ChatColor.BOLD + nationName + ChatColor.WHITE + " | " + ChatColor.BOLD + getDaysOld(nationName) + ChatColor.WHITE + " days old | " + recruitStatus + ChatColor.WHITE + " | " + members.size() + " members");
							rank++;
						}
						player.sendMessage("");
						player.sendMessage(ChatColor.WHITE + "Displaying page " + (pageNumber + 1) + ChatColor.WHITE + " of " + (numberOfPages + 1) + ".");
						player.sendMessage(ChatColor.WHITE + "Lookup a nation with: " + ChatColor.GOLD + "/nation lookup <nation>");
						player.sendMessage(ChatColor.YELLOW + "==============================================");
						player.sendMessage("");
						
						return;
					}
					
					if(args[0].equalsIgnoreCase("lookup")) {
						
						//check that nation exists with name args[1]
						String nationName = getNameCased(args[1]);
						if(nationName == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "'" + args[1] + "' is not an existing nation.");
							return;
						}
						NationConfig nationConfig = NationConfig.getConfig(nationName);
						
						String originalCapitalTownName = nationConfig.getString("Capital");
						String capitalTownDisplayName = Town.getTownDisplayName(originalCapitalTownName);
						
						UUID leaderUUID = getLeader(nationName);
						PlayerConfig leaderConfig = PlayerConfig.getConfig(leaderUUID);
						String leaderName = leaderConfig.getString("Name") == null ? Bukkit.getOfflinePlayer(leaderUUID).getName() : ChatColor.translateAlternateColorCodes('&', leaderConfig.getString("Name"));
						
						int nationRank = getNationsRanked().indexOf(nationName) + 1;
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder(nationName, 46) + " " + nationName + " " + Chat.getTitlePlaceholder(nationName, 46));
						player.sendMessage(ChatColor.YELLOW + "Description: " + ChatColor.WHITE + nationConfig.getString("Description"));
						player.sendMessage(ChatColor.YELLOW + "Reigning since: " + ChatColor.WHITE + DateCode.Decode(nationConfig.getString("CreationDate"), DateCode.Value.DAY_OF_MONTH) + "/" + DateCode.Decode(nationConfig.getString("CreationDate"), DateCode.Value.MONTH) + "/" + DateCode.Decode(nationConfig.getString("CreationDate"), DateCode.Value.YEAR) + " (" + getDaysOld(nationName) + " days old)");
						player.sendMessage(ChatColor.YELLOW + "Recruiting: " + ChatColor.WHITE + Town.RecruitStatus.values()[nationConfig.getInt("Recruiting")].toString().replace("_", " "));
						player.sendMessage(ChatColor.YELLOW + "Nation rank: " + ChatColor.GOLD + "#" + ChatColor.BOLD + nationRank + ChatColor.WHITE + " out of " + DeadMC.NationFile.data().getStringList("Active").size() + " (" + ChatColor.GOLD + "/nation list" + ChatColor.WHITE + ")");
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "Capital town: " + ChatColor.WHITE + capitalTownDisplayName);
						player.sendMessage(ChatColor.YELLOW + "Nation leader: " + ChatColor.WHITE + leaderName);
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "Towns (" + nationConfig.getStringList("Towns").size() + " total):");
						//the towns in the nation (with pages) in order of town scores
						
						List<String> townsRanked = Town.getTownsRanked();
						
						//sort the towns based on score (highest to lowest)
						List<String> townsInNationSorted = new ArrayList<String>(nationConfig.getStringList("Towns"));
						Collections.sort(townsInNationSorted, new Comparator<String>() {
							@Override
							public int compare(String town1, String town2) {
								return Double.compare(Town.getScore(town2), Town.getScore(town1));
							}
						});
						
						for(String townInNation : townsInNationSorted) {
							player.sendMessage(ChatColor.YELLOW + "  > " + ChatColor.WHITE + Town.getTownDisplayName(townInNation) + " (rank " + ChatColor.GOLD + "#" + (townsRanked.indexOf(townInNation) + 1) + ChatColor.WHITE + ")");
						}
						
						player.sendMessage(ChatColor.YELLOW + "Total members: " + ChatColor.WHITE + getMembers(nationName).size());
						
						player.sendMessage(ChatColor.YELLOW + "=================================================");
						player.sendMessage("");
						
						return;
					}
					
					if(args[0].equalsIgnoreCase("create")) {
						String originalTownName = playerConfig.getString("Town");
						if(originalTownName == null || playerConfig.getInt("TownRank") < Town.Rank.Mayor.ordinal()) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You must be a town mayor to create a nation.");
							return;
						}
						
						//must be a donator to create a nation
						if(playerConfig.getInt("DonateRank") == 0) {
							//not a donator
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You must be a donator to create a nation.");
							player.sendMessage(ChatColor.RED + "[DeadMC] " + ChatColor.WHITE + "Visit " + ChatColor.GOLD + "www.DeadMC.com/ranks" + ChatColor.WHITE + " to unlock this command.");
							player.sendMessage("");
							return;
						}
						
						//is mayor of town
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						String townDisplayName = Town.getTownDisplayName(originalTownName);
						
						String exsitingNationName = townConfig.getString("Nation");
						if(exsitingNationName != null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You are already a member of a nation (" + exsitingNationName + ").");
							return;
						}
						//is not a part of a nation
						
						if(args.length == 1) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/nation create <name>");
							return;
						}
						//args greater than 1
						
						String potentialNationName = args[1];
						//check it qualifies
						if(potentialNationName.length() > 21) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Your nation name must be less than 21 characters. (is " + potentialNationName.length() + ")");
							return;
						}
						
						try {
							ProtectedCuboidRegion region = new ProtectedCuboidRegion(potentialNationName, BlockVector3.ONE, BlockVector3.ONE);
						} catch(IllegalArgumentException e) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Your nation name cannot contain special characters.");
							return;
						}
						
						if(potentialNationName.toLowerCase().contains("fuck")
								|| potentialNationName.toLowerCase().contains("cunt")
								|| potentialNationName.toLowerCase().contains("nigger")
								|| potentialNationName.toLowerCase().contains("nigga")
								|| potentialNationName.toLowerCase().contains("faggot")) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Your nation name cannot contain offensive language.");
							return;
						}
						
						//check doesn't already exist
						if(checkExistsIgnoreCase(potentialNationName)) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "A nation already exists with that name. Try another name.");
							return;
						}
						
						//good to create
						
						//add to the nation file list
						List<String> nations = DeadMC.NationFile.data().getStringList("Active");
						nations.add(potentialNationName);
						DeadMC.NationFile.data().set("Active", nations);
						DeadMC.NationFile.save();
						
						//create the config and add the towns
						NationConfig nationConfig = NationConfig.getConfig(potentialNationName);
						List<String> towns = new ArrayList<String>();
						towns.add(originalTownName);
						
						//set initial values
						ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
						nationConfig.set("CreationDate", DateCode.Encode(adelaideTime, true, true, true, true, false, false));
						nationConfig.set("Description", "A DeadMC nation.");
						nationConfig.set("Recruiting", Town.RecruitStatus.OPEN.ordinal()); //all nations start with open recruiting
						nationConfig.set("Capital", originalTownName);
						nationConfig.set("Towns", towns);
						nationConfig.save();
						
						//update the town
						townConfig.set("Nation", potentialNationName);
						townConfig.save();
						
						Town.sendMessage(originalTownName, townDisplayName + " has formed the " + potentialNationName + " nation.");
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.GREEN + "You have successfully formed the " + potentialNationName + " nation!");
						player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Get started by using the " + ChatColor.GOLD + "/nation" + ChatColor.WHITE + " command.");
						player.sendMessage("");
						
						return;
					}
					
					if(args[0].equalsIgnoreCase("leave")) {
						String originalTownName = playerConfig.getString("Town");
						if(originalTownName == null || playerConfig.getInt("TownRank") < Town.Rank.Mayor.ordinal()) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You must be a town mayor to leave the nation.");
							return;
						}
						//is mayor of town
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						String townDisplayName = Town.getTownDisplayName(originalTownName);
						
						//check if town is in nation
						String nationName = townConfig.getString("Nation");
						if(nationName == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + townDisplayName + " is not in a nation.");
							return;
						}
						//is in nation
						NationConfig nationConfig = NationConfig.getConfig(nationName);
						
						//if town is capital of nation
						if(nationConfig.getString("Capital").equalsIgnoreCase(originalTownName)) {
							if(!confirmLeave.contains(player.getUniqueId().toString())) {
								// give warning: 'Town'
								confirmLeave.add(player.getUniqueId().toString());
								
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + townDisplayName + " is the nation's capital. If you leave, the nation will be disbanded.");
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You can set another town as the capital before leaving with " + ChatColor.GOLD + "/nation set capital <town>" + ChatColor.WHITE + ".");
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + ChatColor.BOLD + "You can use " + ChatColor.GOLD + ChatColor.BOLD + "/nation leave" + ChatColor.WHITE + ChatColor.BOLD + " to confirm.");
								player.sendMessage("");
								
								Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
									if(confirmLeave.contains(player.getUniqueId().toString())) {
										confirmLeave.remove(player.getUniqueId().toString());
									}
								}, 2400L);
								return;
							} else {
								// disband the nation on confirm
								confirmLeave.remove(player.getUniqueId().toString());
								disband(nationName);
								return;
							}
						} else {
							//remove the town from the nation
							removeTown(nationName, originalTownName);
							return;
						}
					}
					
					if(args[0].equalsIgnoreCase("set")) {
						
						//check that player is a mayor
						String nationName = isLeader(player.getUniqueId());
						if(nationName == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You must be a nation leader to use this.");
							return;
						}
						//is nation leader
						NationConfig nationConfig = NationConfig.getConfig(nationName);
						
						if(args.length == 1) {
							//nation set help
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/nation set " + ChatColor.AQUA + "<one of these:>");
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "description" + ChatColor.WHITE + ": a brief description of your nation.");
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "recruiting" + ChatColor.WHITE + ": control how (and if) towns can join " + nationName);
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "  - " + ChatColor.AQUA + "capital" + ChatColor.WHITE + ": give leadership of the nation to another mayor");
							player.sendMessage("");
							return;
						}
						//has 2 args
						
						if(args[1].equalsIgnoreCase("recruiting")) {
							//set recruiting to open/invite-only/closed
							if(args.length > 2 && args[2].equalsIgnoreCase("open")) {
								nationConfig.set("Recruiting", Town.RecruitStatus.OPEN.ordinal());
								nationConfig.save();
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.GREEN + "Any town may now join " + nationName + ".");
								return;
							} else if(args.length > 2 && args[2].equalsIgnoreCase("invite-only")) {
								nationConfig.set("Recruiting", Town.RecruitStatus.INVITE_ONLY.ordinal());
								nationConfig.save();
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.YELLOW + "Only towns who are sent an invitation may join " + nationName + ".");
								return;
							} else if(args.length > 2 && args[2].equalsIgnoreCase("closed")) {
								nationConfig.set("Recruiting", Town.RecruitStatus.CLOSED.ordinal());
								nationConfig.save();
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "No towns can now join " + nationName + ".");
								return;
							} else {
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/nation set recruiting " + ChatColor.AQUA + "public" + ChatColor.WHITE + "/" + ChatColor.AQUA + "invite-only" + ChatColor.WHITE + "/" + ChatColor.AQUA + "private");
								return;
							}
						}
						
						if(args[1].equalsIgnoreCase("description")) {
							if(args.length == 2) {
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/nation set description <description>");
								return;
							}
							
							int count = 3;
							String description = "";
							while(count <= args.length) {
								description = description + args[count - 1] + " ";
								count++;
							}
							description = description.substring(0, description.length() - 1); //remove the last space
							
							if(description.length() > 128) {
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "Description must be less than 128 characters.");
								return;
							}
							
							//update discord topic if channel exists
							List<TextChannel> channels = DiscordSRV.getPlugin().getMainGuild().getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false);
							if(channels.size() > 0) {
								channels.get(0).getManager().setTopic(description).queue();
							}
							
							nationConfig.set("Description", description);
							nationConfig.save();
							
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.GREEN + nationName + "'s description has been updated.");
							
							return;
						}
						
						if(args[1].equalsIgnoreCase("capital")) {
							if(args.length == 2) {
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/nation set capital <town>");
								return;
							}
							
							//check that args[2] is a town
							String townNameCased = Town.townNameCased(args[2]);
							if(townNameCased == null) {
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "'" + args[2] + "' is not an existing town.");
								return;
							}
							String originalTownName = Town.getOriginalTownName(townNameCased);
							
							//check town is a member of the nation
							if(!nationConfig.getStringList("Towns").contains(originalTownName)) {
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + townNameCased + " is not a member of " + nationName + ".");
								return;
							}
							
							//can set capital:
							
							//if town is capital of nation
							if(!nationConfig.getString("Capital").equalsIgnoreCase(originalTownName)) {
								if(!confirmTransfer.contains(player.getUniqueId().toString())) {
									// give warning: 'Town'
									confirmTransfer.add(player.getUniqueId().toString());
									
									player.sendMessage("");
									player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "Are you sure you to transfer ownership of " + nationName + "?");
									player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "This will remove " + Town.getTownDisplayName(nationConfig.getString("Capital")) + " as the capital, and therefore you will no longer be the leader of the nation.");
									player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You can use " + ChatColor.GOLD + "/nation set capital " + townNameCased + ChatColor.WHITE + " again to confirm.");
									player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Confirmation session will expire in 30 seconds.");
									player.sendMessage("");
									
									Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
										if(confirmTransfer.contains(player.getUniqueId().toString())) {
											confirmTransfer.remove(player.getUniqueId().toString());
											player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "Confirmation session expired.");
										}
									}, 600L);
									return;
								} else {
									// change the nation capital on confirm
									confirmTransfer.remove(player.getUniqueId().toString());
									nationConfig.set("Capital", originalTownName);
									nationConfig.save();
									
									sendMessage(nationName, townNameCased + " has become the capital town of " + nationName + ".");
									return;
								}
							} else {
								//remove the town from the nation
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + townNameCased + " is already the capital of " + nationName + ".");
								return;
							}
						}
						
					}
					
					if(args[0].equalsIgnoreCase("join")) {
						
						if(args.length == 1) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/nation join <nation name>" + ChatColor.WHITE + ".");
							return;
						}
						
						//if player is in a town and a mayor
						String originalTownName = playerConfig.getString("Town");
						if(originalTownName == null || playerConfig.getInt("TownRank") < Town.Rank.Mayor.ordinal()) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You must be a town mayor to use this.");
							return;
						}
						//is mayor of a town
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						String townDisplayName = Town.getTownDisplayName(originalTownName);
						
						//check if town is in nation
						String exsitingNationName = townConfig.getString("Nation");
						if(exsitingNationName != null) {
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + townDisplayName + " can only be a member of one nation.");
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use " + ChatColor.GOLD + "/nation leave" + ChatColor.WHITE + " to leave " + exsitingNationName + ".");
							player.sendMessage("");
							return;
						}
						//is not in nation
						
						//if arg 1 is a nation
						String nationNameCased = getNameCased(args[1]);
						if(nationNameCased == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + args[1] + " is not an existing nation.");
							return;
						}
						//nation exists
						NationConfig nationConfig = NationConfig.getConfig(nationNameCased);
						
						//if the nation is closed ?
						if(nationConfig.getInt("Recruiting") == Town.RecruitStatus.CLOSED.ordinal()) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "The " + nationNameCased + " nation are not recruiting at this time.");
							return;
						}
						
						//if the nation is invite-only, and they don't have an invite
						if(nationConfig.getInt("Recruiting") == Town.RecruitStatus.INVITE_ONLY.ordinal()
								&& (!invites.containsKey(nationNameCased) || !invites.get(nationNameCased).contains(player.getName()))) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "An invite is required to join " + nationNameCased + ".");
							return;
						}
						//is open or player has invite
						
						//remove the invite:
						removeInvite(player.getName(), nationNameCased);
						
						//add town to nation
						addTown(nationNameCased, originalTownName);
						
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.GREEN + townDisplayName + " has joined the " + nationNameCased + " nation!");
						player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Get started by using the " + ChatColor.GOLD + "/nation" + ChatColor.WHITE + " command.");
						player.sendMessage("");
						return;
					}
					
					if(args[0].equalsIgnoreCase("invite")) {
						if(args.length == 1) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/nation invite <town>");
							return;
						}
						
						//check that player is a mayor
						String nationName = isLeader(player.getUniqueId());
						if(nationName == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "You must be a nation leader to use this.");
							return;
						}
						//is nation leader
						NationConfig nationConfig = NationConfig.getConfig(nationName);
						
						if(nationConfig.getInt("Recruiting") == Town.RecruitStatus.CLOSED.ordinal()) {
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + nationName + "'s recruitment status is closed.");
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You can change this with " + ChatColor.GOLD + "/nation set recruiting" + ChatColor.WHITE + ".");
							player.sendMessage("");
							return;
						}
						
						//check args[1] is a town
						String townNameCased = Town.townNameCased(args[1]);
						if(townNameCased == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "'" + args[1] + "' is not an existing town.");
							return;
						}
						//is a town name or alias
						String originalTownName = Town.getOriginalTownName(townNameCased);
						TownConfig townConfig = TownConfig.getConfig(originalTownName);
						String townDisplayName = Town.getTownDisplayName(originalTownName);
						
						//is the mayor already in a nation?
						String existingNationName = townConfig.getString("Nation");
						if(existingNationName != null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + townDisplayName + " is a member of the " + existingNationName + " nation.");
							return;
						}
						
						//check if the mayor is online
						UUID mayorUUID = UUID.fromString(townConfig.getString("Mayor"));
						Player mayor = Bukkit.getPlayer(mayorUUID);
						if(mayor == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "The Mayor of " + townDisplayName + " (" + Bukkit.getOfflinePlayer(mayorUUID).getName() + ") must be online to receive the invite.");
							return;
						}
						//mayor is online
						
						//invites: nation name is key, list of players are in list
						List<String> nationsInvites = invites.containsKey(nationName) ? invites.get(nationName) : new ArrayList<String>();
						//don't send invites again
						if(nationsInvites.contains(mayor.getName())) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "You have already sent an invite to Mayor " + mayor.getName() + ".");
							return;
						}
						//can send invite
						
						//add invite to mayor
						nationsInvites.add(mayor.getName());
						if(invites.containsKey(nationName)) {
							invites.replace(nationName, nationsInvites);
						} else {
							invites.put(nationName, nationsInvites);
						}
						
						player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.GREEN + "You have sent an invite to " + mayor.getName() + " (Mayor of " + townDisplayName + ").");
						
						mayor.sendMessage("");
						mayor.sendMessage(ChatColor.YELLOW + "========== " + ChatColor.BOLD + "Invitation to join nation" + ChatColor.YELLOW + " ==========");
						mayor.sendMessage(ChatColor.GREEN + "The " + ChatColor.BOLD + nationName + ChatColor.GREEN + " nation has sent you an invitation to join!");
						mayor.sendMessage(ChatColor.WHITE + "Lookup the nation's info with " + ChatColor.GOLD + "/nation lookup " + nationName + ChatColor.WHITE + ".");
						mayor.sendMessage(ChatColor.WHITE + "To " + ChatColor.GREEN + "accept" + ChatColor.WHITE + ", use: " + ChatColor.GOLD + "/nation join " + nationName + ChatColor.WHITE + ".");
						mayor.sendMessage(ChatColor.YELLOW + "===========================================");
						mayor.sendMessage("");
						
						//remove invite in 5 minutes
						Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
							removeInvite(mayor.getName(), nationName);
						}, 6000L);
						
						return;
					}
					
					if(args[0].equalsIgnoreCase("banish")) {
						if(args.length == 1) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/nation banish <town>");
							return;
						}
						
						//check that player is a mayor
						String nationName = isLeader(player.getUniqueId());
						if(nationName == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "You must be a nation leader to use this.");
							return;
						}
						//is nation leader
						NationConfig nationConfig = NationConfig.getConfig(nationName);
						
						//check that args[1] is a town
						String townNameCased = Town.townNameCased(args[1]);
						if(townNameCased == null) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + "'" + args[1] + "' is not an existing town.");
							return;
						}
						String originalTownName = Town.getOriginalTownName(townNameCased);
						
						//check town is a member of the nation
						if(!nationConfig.getStringList("Towns").contains(originalTownName)) {
							player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.RED + townNameCased + " is not a member of " + nationName + ".");
							return;
						}
						
						//can banish:
						
						//if town is capital of nation
						if(nationConfig.getString("Capital").equalsIgnoreCase(originalTownName)) {
							if(!confirmLeave.contains(player.getUniqueId().toString())) {
								// give warning: 'Town'
								confirmLeave.add(player.getUniqueId().toString());
								
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + townNameCased + " is the nation's capital. If you leave, the nation will be disbanded.");
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "You can set another town as the capital before leaving with " + ChatColor.GOLD + "/nation set capital <town>" + ChatColor.WHITE + ".");
								player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + ChatColor.BOLD + "You can use " + ChatColor.GOLD + ChatColor.BOLD + "/nation leave" + ChatColor.WHITE + ChatColor.BOLD + " to confirm.");
								player.sendMessage("");
								
								Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
									if(confirmLeave.contains(player.getUniqueId().toString())) {
										confirmLeave.remove(player.getUniqueId().toString());
									}
								}, 2400L);
								return;
							} else {
								// disband the nation on confirm
								confirmLeave.remove(player.getUniqueId().toString());
								disband(nationName);
								return;
							}
						} else {
							//remove the town from the nation
							removeTown(nationName, originalTownName);
							return;
						}
					}
					
					//on /capital, the new town mayor must be a donator
				}
				
				//if at this point, they've entered an invalid command
				player.sendMessage(ChatColor.YELLOW + "[Nation] " + ChatColor.WHITE + "Unknown command. Use " + ChatColor.GOLD + "/nation" + ChatColor.WHITE + " for help.");
				return;
			});
			
		} catch(Exception e) {
			Chat.logError(e, player);
		}
		
		return true;
	}
	
	private void removeInvite(String playerName, String nationName) {
		if(invites.containsKey(nationName)) {
			List<String> nationsInvites = invites.get(nationName);
			if(nationsInvites.contains(playerName)) {
				nationsInvites.remove(playerName);
				if(nationsInvites.size() == 0) {
					invites.remove(nationName);
				} else {
					invites.replace(nationName, nationsInvites);
				}
			}
		}
	}
	
	public static void addTown(String nationName, String originalTownName) {
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		String townDisplayName = Town.getTownDisplayName(originalTownName);
		
		sendMessage(nationName, townDisplayName + " has joined the " + nationName + " nation.");
		
		//add the town to the nation
		List<String> towns = nationConfig.getStringList("Towns");
		towns.add(originalTownName);
		nationConfig.set("Towns", towns);
		nationConfig.save();
		
		//update the town
		townConfig.set("Nation", nationName);
		townConfig.save();
		
		//check if any members are on discord, give them the role if exists
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		String nationRoleName = nationName + " nation";
		Role nationRole = guild.getRolesByName(nationRoleName, false).size() == 0 ? null : guild.getRolesByName(nationRoleName, false).get(0);
		if(nationRole != null) {
			//nation is on discord
			for(String memberUUID : townConfig.getStringList("Members")) {
				String discordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(UUID.fromString(memberUUID));
				if(discordID != null) {
					//add the role
					DiscordUtil.addRolesToMember(DiscordUtil.getMemberById(discordID), nationRole);
				}
			}
		} else {
			if(qualifiesForDiscordChannel(nationName)) {
				createDiscordChannel(nationName);
			}
		}
		
		Town.sendMessage(originalTownName, townDisplayName + " has joined the " + nationName + " nation.");
	}
	
	//on add member to nation, check if the nation qualifies for discord
	//on remove member from nation,
	public static void removeTown(String nationName, String originalTownName) {
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		TownConfig townConfig = TownConfig.getConfig(originalTownName);
		String townDisplayName = Town.getTownDisplayName(originalTownName);
		
		// leave the nation
		List<String> towns = nationConfig.getStringList("Towns");
		towns.remove(originalTownName);
		nationConfig.set("Towns", towns);
		nationConfig.save();
		
		//update the town
		townConfig.set("Nation", null);
		townConfig.save();
		
		//check if any members are on discord, remove the role if exists
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		String nationRoleName = nationName + " nation";
		Role nationRole = guild.getRolesByName(nationRoleName, false).size() == 0 ? null : guild.getRolesByName(nationRoleName, false).get(0);
		if(nationRole != null) {
			//nation is on discord
			if(!qualifiesForDiscordChannel(nationName)) {
				//no longer qualifies for channel
				deleteDiscordChannel(nationName);
			} else {
				//remove role from members in the town
				for(String memberUUID : townConfig.getStringList("Members")) {
					String discordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(UUID.fromString(memberUUID));
					if(discordID != null) {
						//remove the role
						DiscordUtil.removeRolesFromMember(DiscordUtil.getMemberById(discordID), nationRole);
					}
				}
			}
		}
		
		sendMessage(nationName, townDisplayName + " has left the " + nationName + " nation.");
		Town.sendMessage(originalTownName, townDisplayName + " has left the " + nationName + " nation.");
	}
	
	public static void disband(String nationName) {
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		
		//remove from the nation file list
		List<String> nations = DeadMC.NationFile.data().getStringList("Active");
		nations.remove(nationName);
		DeadMC.NationFile.data().set("Active", nations);
		DeadMC.NationFile.save();
		
		//update each town
		for(String townInNation : nationConfig.getStringList("Towns")) {
			TownConfig townConfig = TownConfig.getConfig(townInNation);
			townConfig.set("Nation", null);
			townConfig.save();
			
			//send message to towns saying the nation was disbanded
			Town.sendMessage(townInNation, "The " + nationName + " nation has been disbanded!");
			
			if(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).size() > 0) {
				deleteDiscordChannel(nationName);
			}
		}
		
		//delete the config
		File nationFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "nations" + File.separator + nationName + ".yml");
		nationConfig.discard();
		nationFile.delete();
	}

	public static boolean checkExistsIgnoreCase(String nationName) {
		return getNameCased(nationName) != null;
	}
	public static String getNameCased(String nationName) {
		for(String nation : DeadMC.NationFile.data().getStringList("Active")) {
			if(nation.toLowerCase().equalsIgnoreCase(nationName.toLowerCase())) {
				return nation;
			}
		}
		return null; //will return null if nation doesn't exist
	}
	
	public static UUID getLeader(String nationName) {
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		String capitalTown = nationConfig.getString("Capital");
		TownConfig capitalTownConfig = TownConfig.getConfig(capitalTown);
		
		return UUID.fromString(capitalTownConfig.getString("Mayor"));
	}
	
	//check if player is a nation leader
	//returns the nation name if true, else null if false
	public static String isLeader(UUID playerUUID) {
		//check that player is a mayor
		PlayerConfig playerConfig = PlayerConfig.getConfig(playerUUID);
		String playersTownName = playerConfig.getString("Town");
		if(playersTownName == null || playerConfig.getInt("TownRank") < Town.Rank.Mayor.ordinal()) {
			return null;
		}
		//is mayor of town
		TownConfig playersTownConfig = TownConfig.getConfig(playersTownName);
		String playersTownDisplayName = Town.getTownDisplayName(playersTownName);
		
		//check if town is in nation
		String nationName = playersTownConfig.getString("Nation");
		if(nationName == null) {
			return null;
		}
		//is in nation
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		
		//check if town is capital of nation
		if(!nationConfig.getString("Capital").equalsIgnoreCase(playersTownName)) {
			return null;
		}
		
		return nationName;
	}
	
	public static List<UUID> getMembers(String nationName) {
		List<UUID> members = new ArrayList<UUID>();
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		for(String townInNation : nationConfig.getStringList("Towns")) {
			TownConfig townInNationConfig = TownConfig.getConfig(townInNation);
			for(String memberUUIDstring : townInNationConfig.getStringList("Members")) {
				members.add(UUID.fromString(memberUUIDstring));
			}
		}
		return members;
	}
	
	public static double getScore(String nationName) {
		//add the scores of all the towns
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		
		double score = 0;
		for(String townInNation : nationConfig.getStringList("Towns")) {
			score += Town.getScore(townInNation);
		}
		
		return score;
	}
	
	public static List<String> getNationsRanked() {
		//sort the towns based on score (highest to lowest)
		List<String> nationsSorted = new ArrayList<String>(DeadMC.NationFile.data().getStringList("Active"));
		Collections.sort(nationsSorted, new Comparator<String>() {
			@Override
			public int compare(String nation1, String nation2) {
				return Double.compare(Nation.getScore(nation2), Nation.getScore(nation1));
			}
		});
		return nationsSorted;
	}
	
	public static int getDaysOld(String nationName) {
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		if(nationConfig.getString("CreationDate") == null) return 0; //town is null, eg. if testing
		
		ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		
		int creationDate_Year = DateCode.Decode(nationConfig.getString("CreationDate"), DateCode.Value.YEAR);
		int creationDate_DayOfYear = DateCode.Decode(nationConfig.getString("CreationDate"), DateCode.Value.DAY_OF_YEAR);
		
		int currentTimeDay;
		if(adelaideTime.getYear() > creationDate_Year)
			currentTimeDay = (365 * (adelaideTime.getYear() - creationDate_Year) + adelaideTime.getDayOfYear());
		else currentTimeDay = adelaideTime.getDayOfYear();
		
		int dayDifference = currentTimeDay - creationDate_DayOfYear;
		
		return dayDifference;
	}
	
	public static void sendMessage(String nationName, String message) {
		sendMessage(nationName, message, false, false);
	}
	public static void sendMessage(String nationName, String message, Boolean mayorsOnly, Boolean subtitle) {
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		
		for(String townInNation : nationConfig.getStringList("Towns")) {
			TownConfig townInNationConfig = TownConfig.getConfig(townInNation);
			for(String memberUUID : townInNationConfig.getStringList("Members")) {
				Player member = Bukkit.getPlayer(UUID.fromString(memberUUID));
				if(member != null) {
					if(!mayorsOnly || (mayorsOnly && PlayerConfig.getConfig(member.getUniqueId()).getInt("TownRank") >= Town.Rank.Co_Mayor.ordinal())) {
						if(!subtitle)
							member.sendMessage(ChatColor.YELLOW + "[Nation] " + message);
						else {
							Bukkit.getScheduler().runTask(plugin, () -> {
								Chat.sendTitleMessage(member.getUniqueId(), message);
							});
						}
					}
				}
			}
		}
		
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		if(!subtitle && !mayorsOnly && guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).size() > 0) {
			//send to discord
			DiscordUtil.sendMessage(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).get(0), message);
		}
	}
	
	public static void turnDiscordChannels() {
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		
		for(String nationName : DeadMC.NationFile.data().getStringList("Active")) {
			if(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).size() > 0
					&& !qualifiesForDiscordChannel(nationName)) {
				//no longer qualifies
				deleteDiscordChannel(nationName);
			}
		}
	}
	
	public static boolean qualifiesForDiscordChannel(String nationName) {
		Chat.debug("Checking if " + nationName + " qualifies for nation Discord chat. See stack below:");
		Chat.debug("" + Thread.currentThread().getStackTrace()[2]);
		
		NationConfig nationConfig = NationConfig.getConfig(nationName);
		
		int townsNeeded = 2;
		if(nationConfig.getStringList("Towns").size() < townsNeeded) {
			//not enough towns
			Chat.debug("Not enough towns");
			return false;
		}
		
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		
		//get discord ID of mayor. If null, return false
		UUID nationLeaderUUID = getLeader(nationName);
		String leaderDiscordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(nationLeaderUUID);
		if(leaderDiscordID == null) {
			//mayor not on discord
			Chat.debug("The leader isn't on discord");
			return false;
		}
		
		int activePlayersNeeded = 2; //at least 2 players needed
		int activePlayers = 0;
		//search all towns and all members
		for(String townInNation : nationConfig.getStringList("Towns")) {
			TownConfig townInNationConfig = TownConfig.getConfig(townInNation);
			for(String member : townInNationConfig.getStringList("Members")) {
				//an active player is a player whose last login is not more than 7 days ago
				PlayerConfig playerConfig = PlayerConfig.getConfig(UUID.fromString(member));
				int daysSinceLastSeen = playerConfig.getString("LastLogin") == null ? 0 : DateCode.getDaysSince(playerConfig.getString("LastLogin"));
				if(daysSinceLastSeen < 7.0) {
					activePlayers++;
					if(activePlayers >= activePlayersNeeded) break;
				}
			}
		}
		
		if(activePlayers < activePlayersNeeded) {
			//not enough active players
			Chat.debug("Not enough active players (there are " + activePlayers + ")");
			return false;
		}
		
		Chat.debug("Qualifies!");
		return true;
	}
	
	public static void createDiscordChannel(String nationName) {
		Chat.debug("Creating nation discord channel for " + nationName);
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		NationConfig nationConfig = NationConfig.getConfig(nationName);

		//create the role
		String nationRoleName = nationName + " nation";
		guild.createRole().setName(nationRoleName).setColor(Color.PINK).setPermissions(Permission.USE_SLASH_COMMANDS).complete();
		Role townRole = guild.getRolesByName(nationRoleName, false).get(0);
		
		//add roles to each member
		for(String townInNation : nationConfig.getStringList("Towns")) {
			TownConfig townInNationConfig = TownConfig.getConfig(townInNation);
			for(String member : townInNationConfig.getStringList("Members")) {
				String discordID = DiscordSRV.getPlugin().getAccountLinkManager().getDiscordId(UUID.fromString(member));
				if(discordID != null) {
					DiscordUtil.addRolesToMember(DiscordUtil.getMemberById(discordID), townRole);
				}
			}
		}
		
		//create the channel with role permissions
		long viewPermission = Permission.VIEW_CHANNEL.getRawValue();
		guild.createTextChannel(nationName.toLowerCase() + "-nation-chat", guild.getCategoryById("859961704456257546")) //assign to the 'Minecraft' category
				.addRolePermissionOverride(guild.getPublicRole().getIdLong(), 0, viewPermission) //deny viewing
				.addRolePermissionOverride(townRole.getIdLong(), viewPermission, 0) //allow viewing
				.setTopic(nationConfig.getString("Description")).complete(); //set the description
		
		//send intro message to all members
		DiscordUtil.sendMessage(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).get(0), "Welcome to the official " + nationName + " nation chat! :smiley:\n\nHere is where members of your town and members from other towns in your nation can communicate.\nThe channel keeps history of your in-game chats, and allows you the ability to chat to your allies remotely through Discord.\n\n> :loud_sound: You can also create a private voice channel at any time with **/voice**.");
	}
	
	public static void deleteDiscordChannel(String nationName) {
		Chat.debug("Deleting the " + nationName + " nation discord channel");
		Guild guild = DiscordSRV.getPlugin().getMainGuild();
		
		//delete the role
		String nationRoleName = nationName + " nation";
		Role nationRole = guild.getRolesByName(nationRoleName, false).get(0);
		nationRole.delete().queue();
		
		//delete the channel
		guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).get(0).delete().queue();
	}
}
