package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Chat.Leaderboard;
import com.deadmc.DeadAPImaven.Entities.LootRarity;
import com.deadmc.DeadAPImaven.Stats.Stat;
import com.deadmc.DeadAPImaven.Task.TaskStep;
import com.deadmc.DeadAPImaven.Towns.Town;
import com.deadmc.DeadAPImaven.Towns.TownGUI;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import github.scarsz.discordsrv.DiscordSRV;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;


//build with "clean install"
//located in 'target' folder


//main class
public class DeadMC extends JavaPlugin implements Listener {
	
	// files: 
	public static Config PlayerFile;
	public static Config MarketFile;
	public static Config TravelFile;
	public static Config TownFile;
	public static Config NationFile;
	public static Config DonatorFile;
	public static Config LootFile;
	public static Config RegionFile;
	public static Config ReportsFile;
	public static Config LeaderboardFile;
	public static Config TaskFile;
	public static Config ShopsFile;
	public static Config StatsFile;
	public static Config IpsFile;
	public static Config PetsFile;
	public static Config SantaFile;
	public static Config RestartFile;
	
	public static HashMap<String, Config> configs = new HashMap<String, Config>();
	
	public static List<String> statsToTrack = new ArrayList<String>();
	
	public static List<ItemStack> commonItems;
	public static List<ItemStack> uncommonItems;
	public static List<ItemStack> rareItems;
	public static List<ItemStack> veryRareItems;
	
	public static int emeraldCost = 30; //the conversion cost for emeralds to coins
	
	private DiscordSRVListener discordsrvListener = new DiscordSRVListener(this);
	
	//example of completable future:
//	CompletableFuture<Player> getPlayerAsync = CompletableFuture.supplyAsync(() -> {
//		return Bukkit.getPlayer("some player"); //get the player "some player" asynchronously
//	}).thenApply(player -> {
//		return player; //do this synchronously
//	});
//
//	Chat.debug("Hello " + getPlayerAsync.get().getName());
	
	
	//using task chain:
	
	//DeadMC.newChain()
	//.asyncFirst(() -> /* find and return the right player object*/)
	//.syncLast((wizard) -> /*now you're sync, modify the player in-game*/)
	//.execute(() -> log("Successfully loaded wizard for ..."));
	
	//task chain
//	private static TaskChainFactory taskChainFactory;
//	public static <T> TaskChain<T> newChain() {
//		return taskChainFactory.newChain();
//	}
//	public static <T> TaskChain<T> newSharedChain(String name) {
//		return taskChainFactory.newSharedChain(name);
//	}

	@Override
	public void onDisable() {
		
		DeadMC.RestartFile.data().set("LastDisable", System.currentTimeMillis());
		DeadMC.RestartFile.save();
		
		long uptime = System.currentTimeMillis() - Restarter.LAST_START_TIME; // in milliseconds
		if(DeadMC.RestartFile.data().getString("IsDevelopmentServer") == null && DeadMC.RestartFile.data().getString("Maintenance") == null && uptime > 100000) {
			Chat.broadcastDiscord( ":head_bandage: **The server stopped!** <@&630729683654148127>"); //send the message to discord
			Chat.broadcastDiscord( ":head_bandage: **The server stopped unexpectedly!** The server may have crashed - note that the DeadAPI shut down safely and called onDisable. If the server was force stopped, you can ignore this message.", true, true); //send the message to ops
			DiscordSRVListener.allowChat(false);
		}
		
		//save player playtime:
		ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		String currentDateCode = DateCode.Encode(adelaideTime, true, true, true, true, true, true);
		
		for(Player player : Bukkit.getOnlinePlayers()) {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player);
			int playTime = DateCode.getTimeSince(playerConfig.getString("LastLogin"));

			String statID = "";
			if(playerConfig.getString("StaffRank") == null) {
				//add play time stat
				statID = DeadMC.statsToTrack.get(Stat.PLAYTIME.ordinal());
			} else {
				//add staff play time
				statID = DeadMC.statsToTrack.get(Stat.STAFF_PLAYTIME.ordinal());
			}
			int newValue = (int) DeadMC.StatsFile.data().getInt(adelaideTime.getDayOfWeek().ordinal() + "." + statID) + playTime;
			DeadMC.StatsFile.data().set(adelaideTime.getDayOfWeek().ordinal() + "." + statID, newValue);
			
			playerConfig.set("LastLogout", currentDateCode); //used for getting unread reports, last seen
			playerConfig.save();
			
			playerConfig.discard(true);
			
			getServer().getConsoleSender().sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.WHITE + "Added " + playTime + " minutes for " + player.getName());
		}
		DeadMC.StatsFile.save();
		
		//remove any entities - eg. spiders
		int clearedEntities = 0;
		for(World world : Bukkit.getWorlds()) {
			for(Entity entity : world.getEntities()) {
				ApplicableRegionSet regionsAtLoc = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(world)).getApplicableRegions(BukkitAdapter.asBlockVector(entity.getLocation()));
				if(Entities.isZombie(entity)
						|| Entities.typesToAlwaysRemove.contains(entity.getType())
						|| (Entities.typesToRemoveIfUnclaimed.contains(entity.getType()) && regionsAtLoc.size() == 0 && entity.getCustomName() == null)) {
					
					entity.remove();
					clearedEntities++;
				}
			}
		}
		
		getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "[DeadMC] " + clearedEntities + " entities removed on disable.");
		
		int clearedPets = 0;
		for(World world : Bukkit.getWorlds()) {
			for(String entityUUID : Pets.allPetUUIDs) {
				Entity entity = Bukkit.getEntity(UUID.fromString(entityUUID));
				if(entity != null) {
					entity.remove();
					clearedPets++;
				}
			}
		}
		
		getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "[DeadMC] " + clearedPets + " pets removed on disable.");
		
		try {
			DiscordSRV.api.unsubscribe(discordsrvListener);
		} catch (Exception | NoClassDefFoundError e) {
			Chat.debug(ChatColor.RED + "Discord integration disabled. Could not find DiscordSRV.");
		}
	}
	
	private void setExceptionHandler() {
		Handler handler = new Handler() {
			@Override
			public void publish(LogRecord record) {
				if(record.getMessage().toLowerCase().contains("exception")) {
					Chat.logError(record);
				}
			}
			
			@Override
			public void flush() {
			
			}
			
			@Override
			public void close() throws SecurityException {
			
			}
		};
		
		
		getServer().getLogger().setUseParentHandlers(true);
		getServer().getLogger().setLevel(Level.ALL);
		
		getServer().getLogger().addHandler(handler);
	}
	
	private void launchMemoryLeakerDetector() {
		//temporary solution to clear old GC:
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
			long maxMemory = Runtime.getRuntime().maxMemory();
			long freeMemory = Runtime.getRuntime().freeMemory();
			
			long usedMemory = Runtime.getRuntime().totalMemory() - freeMemory;
			long finalFreeMemory = maxMemory - usedMemory;
			double freeMemoryPercent = (double)finalFreeMemory / maxMemory;
			
			Chat.debug("Free memory percent: " + freeMemoryPercent + " - " + finalFreeMemory + " - " + maxMemory, "memory");
			
			if(freeMemoryPercent < 0.2) {
				DecimalFormat formatter = new DecimalFormat("#.##");
				formatter.setRoundingMode(RoundingMode.UP);
				String usedString = formatter.format((double)(usedMemory / 1073741824.0));
				String maxString = formatter.format((double)(maxMemory / 1073741824.0));
				String finalPercent = formatter.format(freeMemoryPercent * 100.0);
				
				Chat.broadcastDiscord(":persevere: The server was low on free memory and forced the GC: " + usedString + "gb / " + maxString + "gb (**" + finalPercent + "**% free) - Most likely a memory leak" + (DeadMC.RestartFile.data().getString("IsDevelopmentServer") != null ? " (development server)" : ""), true);
				Bukkit.getScheduler().runTask(this, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spark heapsummary"));
			}
		}, 30 * 20L,  30 * 20L); //every 30 seconds
		
		//if memory leak present, run cleaner every 20 minutes:
//		Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
//			Bukkit.getScheduler().runTask(this, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spark heapsummary"));
//		}, 20 * 60 * 20L,  20 * 60 * 20L); //every 20 minute
	}
	
	private void launchNewYearDetector() {
		Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
			//if time is past first new years eve && before end of last new years day
			ZonedDateTime minTime = ZonedDateTime.now(ZoneOffset.MIN);
			ZonedDateTime maxTime = ZonedDateTime.now(ZoneOffset.MAX);
			ZonedDateTime startOfNewYearsEve = ZonedDateTime.of(2021, 12, 31, 0, 0, 0, 0, ZoneOffset.MAX);
			ZonedDateTime endOfNewYearsDay = ZonedDateTime.of(2022, 1, 2, 0, 0, 0, 0, ZoneOffset.MAX);
			
			if(maxTime.isAfter(startOfNewYearsEve) && minTime.isBefore(endOfNewYearsDay)) {
				Bukkit.getScheduler().runTask(this, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "newyear start 298"));
			}
		}, 5 * 60 * 20L,  5 * 60 * 20L); //every 5 minutes
	}
	
	@Override
	public void onEnable() {

		Pets.halloweenTreats.add(Material.PUMPKIN);
		Pets.halloweenTreats.add(Material.COBWEB);
		Pets.halloweenTreats.add(Material.CRIMSON_ROOTS);
		Pets.halloweenTreats.add(Material.SWEET_BERRIES);
		Pets.halloweenTreats.add(Material.COOKIE);
		Pets.halloweenTreats.add(Material.PUMPKIN_PIE);
		
		// register commands:
		
		getCommand("setname").setExecutor(new Admin(this));
		getCommand("gm").setExecutor(new Admin(this));
		getCommand("log").setExecutor(new Admin(this));
		getCommand("unpunish").setExecutor(new Admin(this));
		getCommand("punish").setExecutor(new Admin(this));
		getCommand("report").setExecutor(new Admin(this));
		getCommand("spectate").setExecutor(new Admin(this));
		getCommand("vanish").setExecutor(new Admin(this));
		getCommand("dmc").setExecutor(new Admin(this));
		
		getCommand("yes").setExecutor(new Admin(this));
		getCommand("no").setExecutor(new Admin(this));
		getCommand("feedback").setExecutor(new Admin(this));
		getCommand("comments").setExecutor(new Admin(this));
		
		getCommand("restarter").setExecutor(new Restarter(this));
		
		getCommand("backup").setExecutor(new Backup(this));
		
		getCommand("stat").setExecutor(new Stats(this));
		
		getCommand("nation").setExecutor(new Nation(this));
		
		getCommand("town").setExecutor(new Town(this));
		
		getCommand("claimtool").setExecutor(new Regions(this));
		getCommand("land").setExecutor(new Regions(this));
		
		getCommand("travel").setExecutor(new Travel(this));
		getCommand("longdale").setExecutor(new Travel(this));
		getCommand("wilderness").setExecutor(new Travel(this));
		getCommand("market").setExecutor(new Travel(this));
		getCommand("public").setExecutor(new Travel(this));
		
		getCommand("speak").setExecutor(new Chat(this));
		getCommand("help").setExecutor(new Chat(this));
		getCommand("message").setExecutor(new Chat(this));
		getCommand("rules").setExecutor(new Chat(this));
		getCommand("leaderboards").setExecutor(new Chat(this));
		getCommand("tell").setExecutor(new Chat(this)); //banned commands
		getCommand("lookup").setExecutor(new Chat(this));
		getCommand("ignore").setExecutor(new Chat(this));
		getCommand("sc").setExecutor(new Chat(this));
		getCommand("tc").setExecutor(new Chat(this));
		getCommand("nc").setExecutor(new Chat(this));
		
		getCommand("gift").setExecutor(new SecretSanta(this));
		
		getCommand("task").setExecutor(new TaskManager(this));
		
		getCommand("santa").setExecutor(new Donator(this));
		getCommand("hat").setExecutor(new Donator(this));
		getCommand("invsee").setExecutor(new Donator(this));
		getCommand("workbench").setExecutor(new Donator(this));
		getCommand("kit").setExecutor(new Donator(this));
		getCommand("disguise").setExecutor(new Donator(this));
		getCommand("zombie").setExecutor(new Donator(this));
		getCommand("name").setExecutor(new Donator(this));
		getCommand("royal").setExecutor(new Donator(this));
		getCommand("deathpoint").setExecutor(new Donator(this));
		getCommand("fly").setExecutor(new Donator(this));
		getCommand("banner").setExecutor(new Donator(this));
		getCommand("prefix").setExecutor(new Donator(this));
		
		getCommand("find").setExecutor(new Shops(this));
		getCommand("sell").setExecutor(new Shops(this));
		getCommand("buy").setExecutor(new Shops(this));
		getCommand("shop").setExecutor(new Shops(this));
		
		getCommand("pay").setExecutor(new Economy(this));
		getCommand("money").setExecutor(new Economy(this));
		
		getCommand("truces").setExecutor(new PvP(this));
		getCommand("truce").setExecutor(new PvP(this));
		getCommand("untruce").setExecutor(new PvP(this));
		getCommand("pvp").setExecutor(new PvP(this));
		
		getCommand("pet").setExecutor(new Pets(this));
		
		getCommand("giveitem").setExecutor(new CustomItems(this));
		
		// register auto tab completion
		getCommand("nation").setTabCompleter(new AutoTab(this));
		getCommand("n").setTabCompleter(new AutoTab(this));
		getCommand("giveitem").setTabCompleter(new AutoTab(this));
		getCommand("backup").setTabCompleter(new AutoTab(this));
		getCommand("setname").setTabCompleter(new AutoTab(this));
		getCommand("town").setTabCompleter(new AutoTab(this));
		getCommand("land").setTabCompleter(new AutoTab(this));
		getCommand("t").setTabCompleter(new AutoTab(this));
		getCommand("towns").setTabCompleter(new AutoTab(this));
		getCommand("f").setTabCompleter(new AutoTab(this));
		getCommand("factions").setTabCompleter(new AutoTab(this));
		getCommand("towny").setTabCompleter(new AutoTab(this));
		getCommand("travel").setTabCompleter(new AutoTab(this));
		getCommand("warp").setTabCompleter(new AutoTab(this));
		getCommand("tp").setTabCompleter(new AutoTab(this));
		getCommand("teleport").setTabCompleter(new AutoTab(this));
		getCommand("sethome").setTabCompleter(new AutoTab(this));
		getCommand("home").setTabCompleter(new AutoTab(this));
		getCommand("leaderboards").setTabCompleter(new AutoTab(this));
		getCommand("leaderboard").setTabCompleter(new AutoTab(this));
		getCommand("lb").setTabCompleter(new AutoTab(this));
		getCommand("cart").setTabCompleter(new AutoTab(this));
		getCommand("sell").setTabCompleter(new AutoTab(this));
		getCommand("buy").setTabCompleter(new AutoTab(this));
		getCommand("s").setTabCompleter(new AutoTab(this));
		getCommand("shop").setTabCompleter(new AutoTab(this));
		getCommand("shops").setTabCompleter(new AutoTab(this));
		getCommand("banner").setTabCompleter(new AutoTab(this));
		getCommand("banners").setTabCompleter(new AutoTab(this));
		getCommand("kit").setTabCompleter(new AutoTab(this));
		getCommand("mail").setTabCompleter(new AutoTab(this));
		getCommand("tc").setTabCompleter(new AutoTab(this));
		getCommand("sc").setTabCompleter(new AutoTab(this));
		getCommand("nc").setTabCompleter(new AutoTab(this));
		getCommand("message").setTabCompleter(new AutoTab(this));
		getCommand("r").setTabCompleter(new AutoTab(this));
		getCommand("msg").setTabCompleter(new AutoTab(this));
		getCommand("m").setTabCompleter(new AutoTab(this));
		getCommand("w").setTabCompleter(new AutoTab(this));
		getCommand("lookup").setTabCompleter(new AutoTab(this));
		getCommand("task").setTabCompleter(new AutoTab(this));
		getCommand("report").setTabCompleter(new AutoTab(this));
		getCommand("punish").setTabCompleter(new AutoTab(this));
		getCommand("pun").setTabCompleter(new AutoTab(this));
		getCommand("unpunish").setTabCompleter(new AutoTab(this));
		getCommand("dmc").setTabCompleter(new AutoTab(this));
		getCommand("pet").setTabCompleter(new AutoTab(this));
		getCommand("pets").setTabCompleter(new AutoTab(this));
		getCommand("gift").setTabCompleter(new AutoTab(this));
		getCommand("disguise").setTabCompleter(new AutoTab(this));
		getCommand("dis").setTabCompleter(new AutoTab(this));
		getCommand("ignore").setTabCompleter(new AutoTab(this));
		getCommand("unignore").setTabCompleter(new AutoTab(this));
		getCommand("find").setTabCompleter(new AutoTab(this));
		getCommand("restarter").setTabCompleter(new AutoTab(this));
		
		// register event listener classes:
		
		getServer().getPluginManager().registerEvents(new Players(this), this);
		getServer().getPluginManager().registerEvents(new Chat(this), this);
		getServer().getPluginManager().registerEvents(new Travel(this), this);
		getServer().getPluginManager().registerEvents(new TravelGUI(this), this);
		getServer().getPluginManager().registerEvents(new TaskEvents(this), this);
		getServer().getPluginManager().registerEvents(new Regions(this), this);
		getServer().getPluginManager().registerEvents(new BloodMoon(this), this);
		getServer().getPluginManager().registerEvents(new Entities(this), this);
		getServer().getPluginManager().registerEvents(new Explosion(this), this);
		getServer().getPluginManager().registerEvents(new PvP(this), this);
		getServer().getPluginManager().registerEvents(new Donator(this), this);
		getServer().getPluginManager().registerEvents(new Shops(this), this);
		getServer().getPluginManager().registerEvents(new Pets(this), this);
		getServer().getPluginManager().registerEvents(new SecretSanta(this), this);
		getServer().getPluginManager().registerEvents(new TownWars(this), this);
		getServer().getPluginManager().registerEvents(new TownGUI(this), this);
		getServer().getPluginManager().registerEvents(new Restarter(this), this);
		getServer().getPluginManager().registerEvents(new CustomItems(this), this);
		getServer().getPluginManager().registerEvents(new Backup(this), this);
		
		// load data files:
		
		loadFiles();
		
		// load tasks:
		
		registerTasks();
		
		//start backup timer:
		
		Backup.loadTimer();
		
		//launchMemoryLeakerDetector();
		
		//launchNewYearDetector();
		
		//start pickup checks
		CustomItems.pickupItemsWithFullInventory();

		// other:
		
		//a list of stats to track
		statsToTrack.add("NP");
		statsToTrack.add("RP");
		statsToTrack.add("V");
		statsToTrack.add("PT");
		statsToTrack.add("AS");
		statsToTrack.add("SPT");
		statsToTrack.add("OS");
		statsToTrack.add("R");
		
		//set loot
		commonItems = (List<ItemStack>) DeadMC.LootFile.data().getList(LootRarity.COMMON.toString());
		uncommonItems = (List<ItemStack>) DeadMC.LootFile.data().getList(LootRarity.UNCOMMON.toString());
		rareItems = (List<ItemStack>) DeadMC.LootFile.data().getList(LootRarity.RARE.toString());
		veryRareItems = (List<ItemStack>) DeadMC.LootFile.data().getList(LootRarity.VERY_RARE.toString());
		
		getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "[DeadMC] Enabled");
		
		//register plugins:
		
		if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
			new PlaceHolders(this).register();
		}
		
		try {
			DiscordSRV.api.subscribe(discordsrvListener);
		} catch (Exception | NoClassDefFoundError e) {
			Chat.debug(ChatColor.RED + "Discord integration disabled. Could not find DiscordSRV.");
		}
		
		//reload leader heads:
		for(Leaderboard type : Leaderboard.values()) {
			Chat.initialiseLeaderboards(type);
		}
		
		//update stats:
		Bukkit.getScheduler().runTaskAsynchronously(this, new Runnable() {
			@Override
			public void run() {
				
				if(DeadMC.RestartFile.data().getString("Maintenance") != null) return;
				
				Restarter.minimumPlayerTrigger = Restarter.CalculateMinimumPlayerTrigger();
				
				//get total player coins in circulation
				for(String uuid : DeadMC.PlayerFile.data().getStringList("Active")) {
					OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
					
					//update currency stat
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					if(playerConfig != null && playerConfig.getString("Coins") != null)
						PlaceHolders.totalCoinsInCirculation += playerConfig.getInt("Coins");

					playerConfig.discard();
				}
				//loop each town
				for(String town : DeadMC.TownFile.data().getStringList("Active")) {
					TownConfig townConfig = TownConfig.getConfig(town); //load in towns
					if(townConfig != null && townConfig.getString("Funds") != null)
						PlaceHolders.totalCoinsInCirculation += townConfig.getInt("Funds");
				}
				
				//load in public tp's
				for(String publicTP : DeadMC.TravelFile.data().getStringList("Public")) {
					PublicTP tpConfig = PublicTP.getConfig(publicTP);
				}
				
//				File shopsFolder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops");
//				File[] files = shopsFolder.listFiles();
//				for(int town = 0; town < files.length; town++) {
//					if(!files[town].isFile()) { //this line weeds out other directories/folders
//						//is a town folder
//						File townFolder = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "shops" + File.separator + files[town].getName());
//						Chat.debug("Trying " + files[town].getName());
//						File[] regionFiles = townFolder.listFiles();
//						for(File regionFile : regionFiles) {
//							Chat.debug(" - " + regionFile.getName().replace(".yml", ""));
//							RegionConfig regionConfig = RegionConfig.getConfig(files[town].getName() + "/" + regionFile.getName().replace(".yml", ""));
//							for(String shopCode : regionConfig.getStringList("Shops")) {
//								Block block = Shops.decodeCode(shopCode).getBlock();
//								if(regionConfig.getItemStack(shopCode + ".Item") != null) {
//									ItemStack shopItem = new ItemStack(regionConfig.getItemStack(shopCode + ".Item")).clone();
//									Bukkit.getScheduler().runTask(DeadMC.this, new Runnable() {
//										@Override
//										public void run() {
//											List<Chest> connectedChests = Shops.getConnectedChests(block);
//											HashMap<Chest, List<ItemStack>> stockStacks = Shops.getStockStacks(connectedChests, shopItem);
//											int totalStock = Shops.getTotalStock(connectedChests, stockStacks, shopItem);
//											regionConfig.set(shopCode + ".TotalStock", totalStock);
//											Chat.debug("   - " + shopCode + " (" + totalStock + ")");
//										}
//									});
//								}
//							}
//							regionConfig.save();
//						}
//					}
//				}
			}
		});
		
		//restarter:
		Restarter.LAST_START_TIME = System.currentTimeMillis();
		if(DeadMC.RestartFile.data().getString("Maintenance") != null) {
			//middle phase
			Restarter.inMaintenance = true;
			Restarter.ClearOldData();
		}
		
		setExceptionHandler();
		
		Chat.debug("" + WorldGuard.getInstance());
		Chat.debug("" + WorldGuard.getInstance().getPlatform());
		
	}
	
	public void loadFiles() {
		
		// Player file:
		PlayerFile = new Config(this, "players.yml");
		configs.put("players.yml", PlayerFile);
		
		PlayerFile.saveDefault(false);
		PlayerFile.save();
		
		// Market file:
		MarketFile = new Config(this, "market.yml");
		configs.put("market.yml", MarketFile);
		
		MarketFile.saveDefault(false);
		MarketFile.save();
		
		// Travel file:
		TravelFile = new Config(this, "travel.yml");
		configs.put("travel.yml", TravelFile);
		
		TravelFile.saveDefault(false);
		TravelFile.save();
		
		// Town file:
		TownFile = new Config(this, "towns.yml");
		configs.put("towns.yml", TownFile);
		
		TownFile.saveDefault(false);
		TownFile.save();
		
		// Nation file:
		NationFile = new Config(this, "nations.yml");
		configs.put("nations.yml", NationFile);
		
		NationFile.saveDefault(false);
		NationFile.save();
		
		// Donator file:
		DonatorFile = new Config(this, "donator.yml");
		configs.put("donator.yml", DonatorFile);
		
		DonatorFile.saveDefault(false);
		DonatorFile.save();
		
		// Loot file:
		LootFile = new Config(this, "loot.yml");
		configs.put("loot.yml", LootFile);
		
		LootFile.saveDefault(false);
		LootFile.save();
		
		// Region file:
		RegionFile = new Config(this, "regions.yml");
		configs.put("regions.yml", RegionFile);
		
		RegionFile.saveDefault(false);
		RegionFile.save();
		
		// Reports file:
		ReportsFile = new Config(this, "reports.yml");
		configs.put("reports.yml", ReportsFile);
		
		ReportsFile.saveDefault(false);
		ReportsFile.save();
		
		// Leaderboards file:
		LeaderboardFile = new Config(this, "leaderboards.yml");
		configs.put("leaderboards.yml", LeaderboardFile);
		
		LeaderboardFile.saveDefault(false);
		LeaderboardFile.save();
		
		// Tasks file:
		TaskFile = new Config(this, "tasks.yml");
		configs.put("tasks.yml", TaskFile);
		
		TaskFile.saveDefault(false);
		TaskFile.save();
		
		// Shops file:
		ShopsFile = new Config(this, "shops.yml");
		configs.put("shops.yml", ShopsFile);
		
		ShopsFile.saveDefault(false);
		ShopsFile.save();
		
		// Stats file:
		StatsFile = new Config(this, "stats.yml");
		configs.put("stats.yml", StatsFile);
		
		StatsFile.saveDefault(false);
		StatsFile.save();
		
		// IPs file:
		IpsFile = new Config(this, "ips.yml");
		configs.put("ips.yml", IpsFile);
		
		IpsFile.saveDefault(false);
		IpsFile.save();
		
		// Pets file:
		PetsFile = new Config(this, "pets.yml");
		configs.put("pets.yml", PetsFile);
		
		PetsFile.saveDefault(false);
		PetsFile.save();
		
		// Secret Santa file:
		SantaFile = new Config(this, "santa.yml");
		configs.put("santa.yml", MarketFile);
		
		SantaFile.saveDefault(false);
		SantaFile.save();
		
		// Secret Santa file:
		RestartFile = new Config(this, "restarter.yml");
		configs.put("restarter.yml", RestartFile);
		
		RestartFile.saveDefault(false);
		RestartFile.save();
		
		//first time running stats
		if(StatsFile.data().getString("StartOfWeek") == null) {
			
			//get last tuesday
			ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
			while(adelaideTime.getDayOfWeek() != Stats.startOfWeek) {
				adelaideTime = adelaideTime.minusDays(1);
			}
			StatsFile.data().set("StartOfWeek", DateCode.Encode(adelaideTime, true, false, false, true, false, false)); //just encode year and day of year
			StatsFile.save();
		}
		
	}
	
	
	public void registerTasks() {
		
		// Collection of tasks linked to their ID
		TaskManager.Tasks = new HashMap<Integer, Task>();
		
		// Register tutorial task
		HashMap<TaskStep, String> tutorial_steps = new HashMap<TaskStep, String>();
		tutorial_steps.put(TaskStep.OPEN_THE_TRAVEL_MENU, "Open the " + ChatColor.GOLD + "/travel" + ChatColor.WHITE + " menu to begin exploring.");
		tutorial_steps.put(TaskStep.TRAVEL_TO_WILDERNESS, "Travel to the " + ChatColor.AQUA + "wilderness" + ChatColor.WHITE + " by clicking the wooden axe in the " + ChatColor.GOLD + "/tp" + ChatColor.WHITE + " menu.");
		tutorial_steps.put(TaskStep.GATHER_LOGS, "Gather some " + ChatColor.AQUA + "logs" + ChatColor.WHITE + " to sell at the market.");
		tutorial_steps.put(TaskStep.OPEN_THE_SHOP_FINDER_MENU, "To sell your logs, use " + ChatColor.GOLD + "/find" + ChatColor.WHITE + " to explore shops to sell to. (" + ChatColor.ITALIC + "TIP 1" + ChatColor.WHITE + ": Hold the log when using /find) (" + ChatColor.ITALIC + "TIP 2" + ChatColor.WHITE + ": Also try keywords, eg. " + ChatColor.GOLD + "/find log" + ChatColor.WHITE + ")");
		tutorial_steps.put(TaskStep.TRAVEL_TO_THE_MARKET, "Click " + ChatColor.AQUA + ChatColor.BOLD + "The Longdale Market" + ChatColor.WHITE + " (bread icon) in the " + ChatColor.GOLD + "/find" + ChatColor.WHITE + " menu to travel to the official market to sell your logs.");
		tutorial_steps.put(TaskStep.CLICK_LOG_SALE_SIGN, "Click the " + ChatColor.AQUA + "[Market]" + ChatColor.WHITE + " sign for the " + ChatColor.BOLD + "specific log" + ChatColor.WHITE + " you want to sell.");
		tutorial_steps.put(TaskStep.SELL_LOGS, "Use " + ChatColor.GOLD + "/sell" + ChatColor.WHITE + " to sell your logs for coins.");
		tutorial_steps.put(TaskStep.FIND_THE_IRON_SWORD_SHOP, "Now let's buy an iron sword. Like selling, try using " + ChatColor.GOLD + "/find IRON_SWORD" + ChatColor.WHITE + " and click " + ChatColor.AQUA + "The Longdale Market" + ChatColor.WHITE + ". (" + ChatColor.ITALIC + "TIP" + ChatColor.WHITE + ": Try keywords, eg. " + ChatColor.GOLD + "/find sword" + ChatColor.WHITE + ")");
		tutorial_steps.put(TaskStep.CLICK_IRON_SWORD_SALE_SIGN, "Click the " + ChatColor.AQUA + "[Market]" + ChatColor.WHITE + " sign.");
		tutorial_steps.put(TaskStep.BUY_SWORD, "Use " + ChatColor.GOLD + "/buy" + ChatColor.WHITE + " to purchase an iron sword.");
		tutorial_steps.put(TaskStep.CREATE_A_TOWN, "Great work! Now let's create a town so we can claim land and recruit members in the future. For now, use " + ChatColor.GOLD + "/town create <town name>" + ChatColor.WHITE + ". (" + ChatColor.ITALIC + "TIP" + ChatColor.WHITE + ": Choose a town name that you'll be proud of. You'll have to pay coins to change it in the future if you wish)");
		tutorial_steps.put(TaskStep.TRAVEL_TO_WILDERNESS_TO_CLAIM_LAND, "Now let's " + ChatColor.AQUA + "travel back to the wilderness" + ChatColor.WHITE + " with the " + ChatColor.GOLD + "/tp" + ChatColor.WHITE + " menu to claim some land. Claiming land has many benefits including: locks " + ChatColor.UNDERLINE + "chests" + ChatColor.WHITE + " & " + ChatColor.UNDERLINE + "doors" + ChatColor.WHITE + ", prevents " + ChatColor.UNDERLINE + "griefing" + ChatColor.WHITE + ", denies " + ChatColor.UNDERLINE + "zombie spawning" + ChatColor.WHITE + ", and disallows " + ChatColor.UNDERLINE + "pvp" + ChatColor.WHITE + ".");
		tutorial_steps.put(TaskStep.OBTAIN_CLAIM_TOOL, "Use " + ChatColor.GOLD + "/claimtool" + ChatColor.WHITE + " to obtain the land claiming tool, and then look for some land that you want to claim for your town. (" + ChatColor.ITALIC + "TIP" + ChatColor.WHITE + ": We can only claim a maximum of 100 blocks right now, so let's " + ChatColor.AQUA + "find a 10x10 area" + ChatColor.WHITE + ").");
		tutorial_steps.put(TaskStep.SELECT_LAND_TO_CLAIM, "Found some wild land to claim? While holding the " + ChatColor.GOLD + "claimtool" + ChatColor.WHITE + " (wooden axe), " + ChatColor.RED + "left click" + ChatColor.WHITE + " one of the corners to select it, then " + ChatColor.RED + "right click" + ChatColor.WHITE + " the opposite corner. (" + ChatColor.ITALIC + "TIP" + ChatColor.WHITE + ": It will automatically select from bedrock to max height - look at the white particles for reference)");
		tutorial_steps.put(TaskStep.CLAIM_LAND, "Perfect! You have selected a region less than your claim limit. Use " + ChatColor.GOLD + "/land claim" + ChatColor.WHITE + " to claim it as your own!");
		tutorial_steps.put(TaskStep.USE_LAND_HELP_COMMAND, "Congrats! This land is now yours. There are many things you can do with your land. Refer to the " + ChatColor.GOLD + "/land help" + ChatColor.WHITE + " command. Let's check that out!");
		tutorial_steps.put(TaskStep.OPEN_TOWN_TRAVEL_MENU, "Check out the land commands above ^ - Now let's " + ChatColor.AQUA + "set the town's travel point" + ChatColor.WHITE + ". Open the " + ChatColor.GOLD + "/tp" + ChatColor.WHITE + " menu and " + ChatColor.RED + "right click" + ChatColor.WHITE + " the shield to manage the town travel point.");
		tutorial_steps.put(TaskStep.CREATE_TOWN_TRAVEL_POINT, "In the " + ChatColor.AQUA + "town travel management menu" + ChatColor.WHITE + " (in the " + ChatColor.GOLD + "/tp" + ChatColor.WHITE + " menu), click " + ChatColor.AQUA + ChatColor.BOLD + "Set TP" + ChatColor.WHITE + " and confirm. (" + ChatColor.ITALIC + "TIP" + ChatColor.WHITE + ": You may also want to change the other options here).");
		tutorial_steps.put(TaskStep.TRAVEL_TO_TOWN, "Nice work! You can now travel to your town in the " + ChatColor.GOLD + "/tp" + ChatColor.WHITE + " menu by " + ChatColor.RED + "left clicking" + ChatColor.WHITE + " the shield. Give it a go!");
		tutorial_steps.put(TaskStep.USE_TOWN_COMMAND, "Lastly, let's check out the " + ChatColor.GOLD + "/town" + ChatColor.WHITE + " command to see what else we can do with our town.");
		TaskManager.Tasks.put(0, new Task(0, "Tutorial", "Complete the tutorial to unlock tasks for rewards.", tutorial_steps, 0));
		
	}
	
}
