package com.deadmc.DeadAPImaven;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationCode {

	public enum Value {
		X,
		Y,
		Z,
		YAW,
		PITCH,
		WORLD_NAME
	};
	
	public static String Encode(Location location) {
		return new String(location.getX() + "|" + location.getY() + "|" + location.getZ() + "|" + (int)location.getYaw() + "|" + (int)location.getPitch() + "|" + location.getWorld().getName());
	}
	
	public static String Encode(Location location, Boolean ignoreRotation) {
		return new String(location.getX() + "|" + location.getY() + "|" + location.getZ() + "|" + (int)0 + "|" + (int)0 + "|" + location.getWorld().getName());
	}
	
	public static Location Decode(String code) {
		double x = 0; double y = 0; double z = 0; int yaw = 0; int pitch = 0; String world = null;
		
		for(int ordinal = 0; ordinal <= Value.WORLD_NAME.ordinal(); ordinal++) {
			//for each ordinal
			
			int numberOfDash = 0;
			int valueStart = -1;
			int valueEnd = -1;
			for(int count = 0; count < code.length(); count++) {
				
				if(numberOfDash == ordinal && valueStart == -1)
					valueStart = count;
				
				if(code.charAt(count) == '|')
					numberOfDash++;
				
				if(numberOfDash > ordinal && valueEnd == -1) {
					valueEnd = count-1; break;
				}
	
			}
			
			if(Value.values()[ordinal] == Value.X) x = Double.parseDouble(code.substring(valueStart, valueEnd + 1));
			if(Value.values()[ordinal] == Value.Y) y = Double.parseDouble(code.substring(valueStart, valueEnd + 1));
			if(Value.values()[ordinal] == Value.Z) z = Double.parseDouble(code.substring(valueStart, valueEnd + 1));
			if(Value.values()[ordinal] == Value.YAW) yaw = Integer.parseInt(code.substring(valueStart, valueEnd + 1));
			if(Value.values()[ordinal] == Value.PITCH) pitch = Integer.parseInt(code.substring(valueStart, valueEnd + 1));
			if(Value.values()[ordinal] == Value.WORLD_NAME) world = code.substring(valueStart, code.length());
		}
		
		return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
	}
	
}
