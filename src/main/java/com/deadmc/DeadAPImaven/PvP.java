package com.deadmc.DeadAPImaven;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;

public class PvP implements CommandExecutor, Listener {
	private DeadMC plugin;
	
	public PvP(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	Map<String, List<String>> friendInvites = new HashMap<String, List<String>>(); // playerToFriend, playerWhoInvited
	Map<String, List<String>> unfriendInvites = new HashMap<String, List<String>>(); // playerToFriend, playerWhoInvited
	
	public boolean onCommand(final CommandSender sender, Command cmd, String commandLabel, final String[] args) {
		
		Player player = null;
		if(sender instanceof Player)
			player = (Player) sender;
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		
		if(commandLabel.equalsIgnoreCase("pvp")) {
			
		}
		
		if(commandLabel.equalsIgnoreCase("truce")) {
			if(args.length > 0) {
				List<String> friends1 = new ArrayList<String>();
				if(playerConfig.getStringList("Friends") != null)
					friends1 = playerConfig.getStringList("Friends");
				
				String friend = args[0];
				PlayerConfig friendConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(friend).getUniqueId());
				List<String> friends2 = new ArrayList<String>();
				if(friendConfig.getStringList("Friends") != null)
					friends2 = friendConfig.getStringList("Friends");
				
				if(!friends1.contains(Bukkit.getOfflinePlayer(friend).getUniqueId().toString())) {
					
					String playerNameCased = Bukkit.getPlayer(friend).getName();
					
					if(friendInvites.containsKey(player.getName())
							&& friendInvites.get(player.getName()).contains(friend)) {
						//accept friendship
						
						friends1.add(Bukkit.getOfflinePlayer(friend).getUniqueId().toString());
						friends2.add(player.getUniqueId().toString());
						friendConfig.set("Friends", friends2);
						playerConfig.set("Friends", friends1);
						
						playerConfig.save();
						friendConfig.save();
						
						if(Bukkit.getPlayer(friend) != null)
							Bukkit.getPlayer(friend).sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.GREEN + player.getName() + " has accepted your truce!");
						player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.GREEN + "You have truced with " + playerNameCased + "!");
						
						List<String> invites = friendInvites.get(player.getName());
						invites.remove(playerNameCased);
						friendInvites.put(playerNameCased, invites);
						return true;
					}
					
					if(DeadMC.PlayerFile.data().getStringList("Active").contains(Bukkit.getOfflinePlayer(friend).getUniqueId().toString())) {
						if(Bukkit.getPlayer(friend) != null) {
							
							if(!friend.equalsIgnoreCase(player.getName())
									&& (!friendInvites.containsKey(playerNameCased) || !friendInvites.get(playerNameCased).contains(player.getName()))) {
								
								List<String> invites = new ArrayList<String>();
								if(friendInvites.containsKey(playerNameCased))
									invites = friendInvites.get(playerNameCased);
								
								invites.add(player.getName());
								friendInvites.put(playerNameCased, invites);
								
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.GREEN + "Sent truce invitation to " + playerNameCased + ".");
								player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.WHITE + "They have 5 minutes to accept or decline.");
								player.sendMessage("");
								
								Bukkit.getPlayer(friend).sendMessage("");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.YELLOW + "========== " + ChatColor.BOLD + "Invitation to truce player" + ChatColor.YELLOW + " ==========");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + player.getName() + ChatColor.GREEN + " has sent you an invitation to truce!");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.WHITE + "Accepting the truce disables PvP with " + player.getName() + ".");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.WHITE + "PvP can only be enabled again when both players agree to untruce.");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.WHITE + "To accept, use: " + ChatColor.GOLD + "/truce " + player.getName());
								Bukkit.getPlayer(friend).sendMessage(ChatColor.YELLOW + "================================================");
								Bukkit.getPlayer(friend).sendMessage("");
								
								Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
									@Override
									public void run() {
										Player player = null;
										if((sender instanceof Player)) {
											player = (Player) sender;
										}
										
										if(friendInvites.containsKey(playerNameCased) && friendInvites.get(playerNameCased).contains(player.getName())) {
											if(Bukkit.getPlayer(friend) != null)
												Bukkit.getPlayer(friend).sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.RED + "Invitation from " + player.getName() + " expired.");
											if(player != null)
												player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.RED + "Invitation to " + playerNameCased + " expired.");
											
											List<String> invites = friendInvites.get(playerNameCased);
											invites.remove(player.getName());
											friendInvites.put(playerNameCased, invites);
										}
										
									}
								}, 6000L); // 5 minutes
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.WHITE + playerNameCased + " already has a pending invitation.");
						} else
							player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.RED + friend + " must be online to do this!");
					} else
						player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.RED + "'" + friend + "' is not a valid player.");
					
				} else
					player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.WHITE + "You already have a truce with " + friend + ".");
			} else
				player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/truce <player>");
		}
		
		if(commandLabel.equalsIgnoreCase("untruce")) {
			if(args.length > 0) {
				
				List<String> friends1 = new ArrayList<String>();
				if(playerConfig.getStringList("Friends") != null)
					friends1 = playerConfig.getStringList("Friends");
				
				String friend = args[0];
				PlayerConfig friendConfig = PlayerConfig.getConfig(Bukkit.getOfflinePlayer(friend).getUniqueId());
				List<String> friends2 = new ArrayList<String>();
				if(friendConfig.getStringList("Friends") != null)
					friends2 = friendConfig.getStringList("Friends");
				
				if(unfriendInvites.containsKey(player.getName())
						&& unfriendInvites.get(player.getName()).contains(friend)) {
					//remove friendship
					
					friends1.remove(Bukkit.getOfflinePlayer(friend).getUniqueId().toString());
					friends2.remove(player.getUniqueId().toString());
					
					if(friends2.size() == 0) friendConfig.set("Friends", null);
					else friendConfig.set("Friends", friends2);
					
					if(friends1.size() == 0) playerConfig.set("Friends", null);
					else friendConfig.set("Friends", friends1);
					
					playerConfig.save();
					friendConfig.save();
					
					if(Bukkit.getPlayer(friend) != null)
						Bukkit.getPlayer(friend).sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.GREEN + player.getName() + " has agreed to untrucing!");
					player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.GREEN + "You have untruced " + friend + "!");
					
					List<String> invites = unfriendInvites.get(player.getName());
					invites.remove(friend);
					unfriendInvites.put(friend, invites);
					return true;
				}
				
				if(DeadMC.PlayerFile.data().getStringList("Active").contains(Bukkit.getOfflinePlayer(friend).getUniqueId().toString())) {
					if(friends1.contains(Bukkit.getOfflinePlayer(friend).getUniqueId().toString())) {
						if(Bukkit.getPlayer(friend) != null) {
							
							if(!friend.equalsIgnoreCase(player.getName())
									&& (!unfriendInvites.containsKey(friend) || !unfriendInvites.get(friend).contains(player.getName()))) {
								
								List<String> invites = new ArrayList<String>();
								if(unfriendInvites.containsKey(friend))
									invites = unfriendInvites.get(friend);
								
								invites.add(player.getName());
								unfriendInvites.put(friend, invites);
								
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.GREEN + "Sent untruce invitation to " + friend + ".");
								player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.WHITE + "They have 5 minutes to accept or decline.");
								player.sendMessage("");
								
								Bukkit.getPlayer(friend).sendMessage("");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.YELLOW + "========= " + ChatColor.BOLD + "Invitation to untruce player" + ChatColor.YELLOW + " =========");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + player.getName() + ChatColor.GREEN + " wants to untruce you!");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.WHITE + "Accepting will enable PvP with " + player.getName() + ".");
								Bukkit.getPlayer(friend).sendMessage(ChatColor.WHITE + "To accept, use: " + ChatColor.GOLD + "/untruce " + player.getName());
								Bukkit.getPlayer(friend).sendMessage(ChatColor.YELLOW + "================================================");
								Bukkit.getPlayer(friend).sendMessage("");
								
								Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
									@Override
									public void run() {
										Player player = null;
										if((sender instanceof Player)) {
											player = (Player) sender;
										}
										
										if(unfriendInvites.containsKey(friend) && unfriendInvites.get(friend).contains(player.getName())) {
											if(Bukkit.getPlayer(friend) != null)
												Bukkit.getPlayer(friend).sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.RED + "Invitation from " + player.getName() + " expired.");
											if(player != null)
												player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.RED + "Invitation to " + friend + " expired.");
											
											List<String> invites = unfriendInvites.get(friend);
											invites.remove(player.getName());
											unfriendInvites.put(friend, invites);
										}
										
									}
								}, 6000L); // 5 minutes
								
							} else
								player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.WHITE + friend + " already has a pending invitation.");
						} else
							player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.RED + friend + " must be online to do this!");
					} else
						player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.WHITE + friend + " is not truced.");
				} else
					player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.RED + "'" + friend + "' is not a valid player.");
				
			} else
				player.sendMessage(ChatColor.YELLOW + "[PvP] " + ChatColor.WHITE + "Use: " + ChatColor.GOLD + "/untruce <player>");
		}
		
		if(commandLabel.equalsIgnoreCase("truces")) {
			if(playerConfig.getString("Friends") != null) {
				List<String> friends = playerConfig.getStringList("Friends");
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder("Friends", 44) + " " + "Friends" + " " + Chat.getTitlePlaceholder("Friends", 44));
				player.sendMessage(ChatColor.BOLD + "You have " + friends.size() + " truces:");
				for(String friend : friends)
					player.sendMessage(ChatColor.WHITE + "  - " + ChatColor.AQUA + Bukkit.getOfflinePlayer(UUID.fromString(friend)).getName());
				player.sendMessage(ChatColor.WHITE + "Request to untruce a player with " + ChatColor.GOLD + "/untruce <player>" + ChatColor.WHITE + ".");
				player.sendMessage(ChatColor.YELLOW + "=============================================");
				player.sendMessage("");
			} else {
				player.sendMessage("");
				player.sendMessage(ChatColor.YELLOW + Chat.getTitlePlaceholder("Truces", 44) + " " + "Truces" + " " + Chat.getTitlePlaceholder("Truces", 44));
				player.sendMessage(ChatColor.BOLD + "Looks like you don't have any truces!");
				player.sendMessage(ChatColor.WHITE + "  Truce a player with " + ChatColor.GOLD + "/truce <player>" + ChatColor.WHITE + " to disable PvP with them. Town peers are automatically truced.");
				player.sendMessage(ChatColor.YELLOW + "=============================================");
				player.sendMessage("");
			}
		}
		
		return true;
	}
	
	public static int travelProtection = 60;
	@EventHandler
	public void onPlayerDamage(EntityDamageByEntityEvent event) {
		
		//cannot attack friends:
		if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			
			Player damaged = (Player) event.getEntity();
			Player damager = (Player) event.getDamager();
			
			
			//travel pvp protection:
			if(Travel.recentlyTravelled.containsKey(damager.getName())) {
				
				ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
				long protectionLeft = (travelProtection - ChronoUnit.SECONDS.between(Travel.recentlyTravelled.get(damager.getName()), adelaideTime));
				
				if(protectionLeft < 1) {
					Travel.recentlyTravelled.remove(damager.getName());
					return;
				}
				
				//damager is protected
				event.setCancelled(true);
				
				Chat.sendTitleMessage(damager.getUniqueId(), "" + ChatColor.RED + "[Travel Protection] " + ChatColor.WHITE + "You cannot PvP for another " + ChatColor.BOLD + protectionLeft + ChatColor.WHITE + " seconds.");
				return;
				//send message to damager
				
			}
			if(Travel.recentlyTravelled.containsKey(damaged.getName())) {
				ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
				long protectionLeft = (travelProtection - ChronoUnit.SECONDS.between(Travel.recentlyTravelled.get(damaged.getName()), adelaideTime));
				
				if(protectionLeft < 1) {
					Travel.recentlyTravelled.remove(damaged.getName());
					return;
				}
				
				//damager is protected
				event.setCancelled(true);
				
				Chat.sendTitleMessage(damager.getUniqueId(), "" + ChatColor.RED + "[Travel Protection] " + ChatColor.WHITE + "You cannot PvP " + damaged.getName() + " for another " + ChatColor.BOLD + protectionLeft + ChatColor.WHITE + " seconds.");
				return;
				//send message to damager
				
			}
			
			boolean inSameTown = PlayerConfig.getConfig(damaged).getString("Town") != null && PlayerConfig.getConfig(damager).getString("Town") != null && PlayerConfig.getConfig(damaged).getString("Town").equalsIgnoreCase(PlayerConfig.getConfig(damager).getString("Town"));
			boolean areTruced = PlayerConfig.getConfig(damaged).getStringList("Friends").contains(damager.getUniqueId().toString());
			if(inSameTown || areTruced) {
				//same town or friended
				event.setCancelled(true);
				Chat.sendTitleMessage(damager.getUniqueId(), "" + ChatColor.YELLOW + (areTruced ? "[Truce] " : "[Town] ") + ChatColor.WHITE + "You cannot attack " + damaged.getName());
				return;
			}
		}
		
		//cannot shoot players on 
		if(event.getEntity() instanceof Player && event.getCause() == DamageCause.PROJECTILE) {
			Projectile projectile = (Projectile) event.getDamager();
			if(projectile.getShooter() instanceof Player && event.getEntity() instanceof Player) { //shooter is a player, and damaged is a player
				Player damager = (Player) projectile.getShooter();
				Player damaged = (Player) event.getEntity();
				
				//travel pvp protection:
				if(Travel.recentlyTravelled.containsKey(damager.getName())) {
					
					ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
					long protectionLeft = (travelProtection - ChronoUnit.SECONDS.between(Travel.recentlyTravelled.get(damager.getName()), adelaideTime));
					
					if(protectionLeft < 1) {
						Travel.recentlyTravelled.remove(damager.getName());
						return;
					}
					
					//damager is protected
					event.setCancelled(true);
					
					Chat.sendTitleMessage(damager.getUniqueId(), "" + ChatColor.RED + "[Travel Protection] " + ChatColor.WHITE + "You cannot PvP for another " + ChatColor.BOLD + protectionLeft + ChatColor.WHITE + " seconds.");
					return;
					//send message to damager
					
				}
				if(Travel.recentlyTravelled.containsKey(damaged.getName())) {
					ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
					long protectionLeft = (travelProtection - ChronoUnit.SECONDS.between(Travel.recentlyTravelled.get(damaged.getName()), adelaideTime));
					
					if(protectionLeft < 1) {
						Travel.recentlyTravelled.remove(damaged.getName());
						return;
					}
					
					//damager is protected
					event.setCancelled(true);
					
					Chat.sendTitleMessage(damager.getUniqueId(), "" + ChatColor.RED + "[Travel Protection] " + ChatColor.WHITE + "You cannot PvP " + damaged.getName() + " for another " + ChatColor.BOLD + protectionLeft + ChatColor.WHITE + " seconds.");
					return;
					//send message to damager
					
				}
				
				if((PlayerConfig.getConfig(damaged).getString("Town") != null && PlayerConfig.getConfig(damager).getString("Town") != null) && PlayerConfig.getConfig(damaged).getString("Town").equalsIgnoreCase(PlayerConfig.getConfig(damager).getString("Town"))
						|| PlayerConfig.getConfig(damaged).getStringList("Friends").contains(damager.getUniqueId().toString())) {
					//same town or friended
					event.setCancelled(true);
					Chat.sendTitleMessage(damager.getUniqueId(), "" + ChatColor.YELLOW + "[Truce] " + ChatColor.WHITE + "You cannot shoot " + damaged.getName());
					return;
				}
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(damaged.getUniqueId());
				String townName = playerConfig.getString("Town");
				if(Regions.townOwnsLand(damaged.getLocation(), townName)) {
					//shooting a player but they're in their town land
					event.setCancelled(true);
					damager.sendMessage(ChatColor.YELLOW + "[Land] " + ChatColor.RED + "This is claimed land!" + ChatColor.WHITE + " Use " + ChatColor.GOLD + "/land" + ChatColor.WHITE + " for info.");
				}
			}
			
		}
		
	}
}
