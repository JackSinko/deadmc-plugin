package com.deadmc.DeadAPImaven;

import com.deadmc.DeadAPImaven.Admin.ActivityType;
import com.deadmc.DeadAPImaven.Admin.Punishment;
import com.deadmc.DeadAPImaven.Chat.Leaderboard;
import com.deadmc.DeadAPImaven.DateCode.Value;
import com.deadmc.DeadAPImaven.Stats.Stat;
import com.deadmc.DeadAPImaven.Task.TaskStep;
import com.deadmc.DeadAPImaven.Towns.Town;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import io.papermc.lib.PaperLib;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.CrossbowMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.io.File;
import java.time.DayOfWeek;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

public class Players implements Listener {
	public static DeadMC plugin;
	
	public Players(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	// setup player
	@EventHandler
	public void onPlayerFirstJoin(PlayerJoinEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			try {
				Player player = event.getPlayer();
				
				if(!DeadMC.PlayerFile.data().getStringList("Active").contains(player.getUniqueId().toString())) {
					player.sendMessage("");
					player.sendMessage(ChatColor.GOLD + "Welcome to DeadMC!");
					Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
						if(player != null) {
							player.sendMessage("");
							player.sendMessage("Be sure to check over the " + ChatColor.GOLD + "/rules" + ChatColor.WHITE + " and " + ChatColor.GOLD + "/help" + ChatColor.WHITE + " if you get stuck.");
						}
					}, 100L);
					Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
						if(player != null) {
							player.sendMessage("");
							player.sendMessage("Join our " + ChatColor.BOLD + "Discord" + ChatColor.WHITE + " for additional help and other cool things! " + ChatColor.GOLD + "discord.gg/CMpyUrs");
						}
					}, 160L);
					Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
						if(player != null) {
							player.sendMessage("");
							player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "Begin your journey with the " + ChatColor.GOLD + ChatColor.BOLD + "/travel" + ChatColor.GREEN + ChatColor.BOLD + " menu!");
						}
					}, 220L);
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, () -> {
						try {
							if(player != null) {
								player.sendMessage("");

								// Initialise player file:
								PlayerConfig playerConfig = PlayerConfig.getConfig(player);
								playerConfig.set("Coins", 0);
								playerConfig.set("Task", 0);
								playerConfig.set("Step", 0);
								playerConfig.set("Kills", 0);
								playerConfig.set("DonateRank", 0);
								playerConfig.set("FreeDeathPointTravel", 3);
								playerConfig.set("FreeInventorySaveOnDeath", 3);
								
								ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
								playerConfig.set("LastLogin", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
								playerConfig.save();
								
								List<String> activePlayers = DeadMC.PlayerFile.data().getStringList("Active");
								activePlayers.add(player.getUniqueId().toString());
								DeadMC.PlayerFile.data().set("Active", activePlayers);
								DeadMC.PlayerFile.save();
								
								Economy.giveCoins(player.getUniqueId(), 500);
								
								//send join message:
								for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
									if(PlayerConfig.configs.containsKey(onlinePlayer.getUniqueId())) { //only if config is saved still (player is online)
										PlayerConfig onlineConfig = PlayerConfig.getConfig(onlinePlayer.getUniqueId());
										if(onlineConfig.getString("Coins") != null) { //player not in tutorial
											if(!onlinePlayer.getName().equals(player.getName())) {
												onlinePlayer.sendMessage(ChatColor.YELLOW + "[Login] " + ChatColor.GRAY + "[" + ChatColor.WHITE + "Citizen" + ChatColor.GRAY + "] " + ChatColor.WHITE + player.getName() + ChatColor.YELLOW + " has logged in.");
												onlinePlayer.sendMessage(ChatColor.GOLD + "A " + ChatColor.BOLD + ChatColor.ITALIC + "BIG WELCOME" + ChatColor.RESET + ChatColor.GOLD + " to our newest player, " + player.getName() + "!");
											}
										}
									}
								}
								
								// display tutorial task to player:
								
								String titleString = new String(ChatColor.WHITE + "" + ChatColor.BOLD + TaskManager.Tasks.get(playerConfig.getInt("Task")).name + ChatColor.WHITE + " | Step " + ChatColor.BOLD + "1" + ChatColor.WHITE + " of " + ChatColor.BOLD + TaskManager.Tasks.get(0).steps.size());
								int titleStringLength = new String(TaskManager.Tasks.get(playerConfig.getInt(player.getName() + ".Task")).name + " | Step " + "1" + " of " + TaskManager.Tasks.get(0).steps.size()).length() - 2;
								
								int numberOfPlaceholders = (44 - titleStringLength) / 2;
								String placeholder = new String();
								for(int count = 0; count < numberOfPlaceholders; count++)
									placeholder = placeholder + "=";
								
								player.sendMessage("");
								player.sendMessage(ChatColor.YELLOW + placeholder + " " + titleString + ChatColor.YELLOW + " " + placeholder);
								player.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "NEW TASK: " + ChatColor.GREEN + TaskStep.values()[0].toString().charAt(0) + TaskStep.values()[0].toString().toLowerCase().replace("_", " ").substring(1, TaskStep.values()[0].toString().length()));
								player.sendMessage(ChatColor.WHITE + TaskManager.Tasks.get(0).steps.get(TaskStep.OPEN_THE_TRAVEL_MENU));
								player.sendMessage("");
								player.sendMessage(ChatColor.ITALIC + "Complete the task for a reward!");
								player.sendMessage(ChatColor.WHITE + "Use " + ChatColor.GOLD + "/task" + ChatColor.WHITE + " for help.");
								player.sendMessage(ChatColor.YELLOW + "============================================"); //44 long
								player.sendMessage("");
								
								Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Task] " + ChatColor.WHITE + "You have a new task! See " + ChatColor.GOLD + "/task" + ChatColor.WHITE + ".");
								
								//update placeholders
								UUID uuid = player.getUniqueId();
								if(PlaceHolders.player_TaskStreak.containsKey(uuid.toString()))
									PlaceHolders.player_TaskStreak.remove(uuid.toString());
								if(PlaceHolders.player_TaskString.containsKey(uuid.toString()))
									PlaceHolders.player_TaskString.remove(uuid.toString());
								
								Stats.Add(Stat.NEW_PLAYERS);
								
								Bukkit.getScheduler().runTask(plugin, () -> {
									//add to new player list so players can welcome
									newPlayers.add(player.getName());
									newPlayers_welcomedBy.put(player.getName(), new ArrayList<String>());
									Bukkit.getScheduler().runTaskLater(plugin, () -> {
										if(newPlayers.contains(player.getName())) {
											newPlayers.remove(player.getName());
											newPlayers_welcomedBy.remove(player.getName());
										}
									}, 600L);
								});
								
							}
						} catch(Exception e) {
							Chat.logError("**Error setting up new player!** This is not good :(", e);
						}
					}, 280L);
					
					Bukkit.getScheduler().runTaskLater(plugin, () -> {
						try {
							if(player.isOnline()) {
								player.getInventory().clear();
								player.getInventory().addItem(new ItemStack(Material.STONE_SWORD, 1));
								
								ItemStack quiver = CustomItems.createNewQuiver(CustomItems.QuiverTier.Basic);
								player.getInventory().addItem(quiver);
								CustomItems.setQuiverStock(player.getInventory().getItem(1), 256);
								
								player.getInventory().addItem(CustomItems.quickBow(CustomItems.QuickBowType.SINGLE, false, 75));
								player.getInventory().addItem(CustomItems.quickBow(CustomItems.QuickBowType.MULTI, false, 264));
								//set charged:
								List<ItemStack> arrow = new ArrayList<ItemStack>();
								arrow.add(new ItemStack(Material.ARROW, 1));
								CrossbowMeta singleBowMeta = (CrossbowMeta) player.getInventory().getItem(2).getItemMeta();
								CrossbowMeta multiBowMeta = (CrossbowMeta) player.getInventory().getItem(3).getItemMeta();
								singleBowMeta.setChargedProjectiles(arrow);
								multiBowMeta.setChargedProjectiles(arrow);
								player.getInventory().getItem(2).setItemMeta(singleBowMeta);
								player.getInventory().getItem(3).setItemMeta(multiBowMeta);
								
								player.getInventory().addItem(new ItemStack(Material.OAK_BOAT, 1));
								player.getInventory().addItem(new ItemStack(Material.APPLE, 64));
								player.getInventory().addItem(new ItemStack(Material.CRAFTING_TABLE, 1));
								player.getInventory().addItem(new ItemStack(Material.STONE_AXE, 1));
								player.getInventory().addItem(new ItemStack(Material.STONE_PICKAXE, 1));
								
								ItemStack[] armor = new ItemStack[4];
								armor[0] = new ItemStack(Material.IRON_BOOTS, 1);
								armor[1] = new ItemStack(Material.IRON_LEGGINGS, 1);
								armor[2] = new ItemStack(Material.IRON_CHESTPLATE, 1);
								armor[3] = new ItemStack(Material.IRON_HELMET, 1);
								
								player.getInventory().setArmorContents(armor);
								player.getEquipment().setItemInOffHand(new ItemStack(Material.SHIELD));
								
								player.updateInventory();
							}
						} catch(Exception e) {
							Chat.logError("**Error setting up new player!** This is not good :(");
						}
					}, 320L);
				}
			} catch(Exception e) {
				Chat.logError("**Error setting up new player!** This is not good :(");
			}
		});
	}
	
	@EventHandler
	public void PreJoin(AsyncPlayerPreLoginEvent event) {
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(event.getUniqueId());
		
		//track ip address:
		String ip = event.getAddress().getHostAddress();
		IpConfig ipConfig = IpConfig.getConfig(ip);
		
		if(!DeadMC.IpsFile.data().getStringList("Active").contains(ip)) {
			List<String> ips = DeadMC.IpsFile.data().getStringList("Active");
			ips.add(ip);
			DeadMC.IpsFile.data().set("Active", ips);
			DeadMC.IpsFile.save();
		}
		
		List<String> playersUsingIp = new ArrayList<String>();
		if(ipConfig.getString("UUID") != null)
			playersUsingIp = ipConfig.getStringList("UUID");
		if(!playersUsingIp.contains(event.getUniqueId().toString()))
			playersUsingIp.add(event.getUniqueId().toString());
		ipConfig.set("UUID", playersUsingIp); //add new player connection
		ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
		ipConfig.set(event.getUniqueId().toString(), DateCode.Encode(adelaideTime, true, false, false, true, false, false)); //update last seen
		ipConfig.save();
		
		//add aliases to players file:
		if(playersUsingIp.size() > 1) {
			//more than 1 player using the ip
			
			List<String> aliases = new ArrayList<String>(); //a list of all aliases
			aliases.addAll(playersUsingIp); //add all players using the current IP
			//add all previous aliases from players
			for(String playerUsingIP_UUID : playersUsingIp) {
				PlayerConfig theirConfig = PlayerConfig.getConfig(UUID.fromString(playerUsingIP_UUID));
				
				if(theirConfig.getString("Aliases") != null) {
					for(String alias : theirConfig.getStringList("Aliases")) {
						if(!aliases.contains(alias)) aliases.add(alias);
					}
				}
			}
			//set aliases for everyone
			for(String alias : aliases) {
				PlayerConfig theirConfig = PlayerConfig.getConfig(UUID.fromString(alias));
				
				List<String> aliasesMinusPlayer = new ArrayList<String>();
				aliasesMinusPlayer.addAll(aliases);
				aliasesMinusPlayer.remove(alias);
				
				theirConfig.set("Aliases", aliasesMinusPlayer);
				theirConfig.save();
			}
			
		}
		
		
		//check if banned:
		if(playerConfig != null) {
			
			//check if any of the aliases are banned:
			List<String> playersToCheck = new ArrayList<String>();
			if(playerConfig.getString("Aliases") != null)
				playersToCheck.addAll(playerConfig.getStringList("Aliases"));
			playersToCheck.add(event.getUniqueId().toString());
			
			boolean canBypassAliasBan = playerConfig.getString("Banned.Start") == null && playerConfig.getString("Bypass_Alias_Bans") != null;
			
			for(String player : playersToCheck) {
				PlayerConfig aliasConfig = PlayerConfig.getConfig(UUID.fromString(player));
				
				if(aliasConfig.getString("Banned.Start") != null) {
					
					if(!player.equals(event.getUniqueId().toString())) {
						if(canBypassAliasBan) {
							Chat.broadcastMessage(ChatColor.YELLOW + " > " + ChatColor.WHITE + event.getName() + " is bypassing " + Bukkit.getOfflinePlayer(UUID.fromString(player)).getName() + "'s ban.", true, true);
							continue;
						}
					}
					
					int timeSinceBan = DateCode.getTimeSince(aliasConfig.getString("Banned.Start"));
					if(aliasConfig.getString("Banned.Time") != null) {
						//temp ban:
						if(timeSinceBan > (aliasConfig.getInt("Banned.Time") * 60)) {
							//ban has passed:
							
							Admin.logBan(UUID.fromString(player), Punishment.valueOf(aliasConfig.getString("Banned.Reason").replace(" ", "_")), aliasConfig.getString("Banned.Start"), aliasConfig.getInt("Banned.Time"));
							
							aliasConfig.set("Banned", null);
							aliasConfig.save();
							continue;
						} else {
							//still banned
							String plural = "";
							
							int remaining = (aliasConfig.getInt("Banned.Time") * 60) - timeSinceBan; //minutes remaining
							//Chat.broadcastMessage("Remaining = " + remaining + " minutes");
							
							if(remaining < 60)
								plural = remaining == 1 ? "1 minute" : remaining + " minutes";
							else if(remaining < 1440) //less than 24 hours
								plural = (remaining / 60) > 1 ? (remaining / 60) + " hours" : "1 hour";
							else //less than 7 days
								plural = (remaining / 1440) > 1 ? (remaining / 1440) + " days" : "1 day";
							
							String comments = "";
							if(aliasConfig.getString("Banned.Comments") != null)
								comments = "\"" + ChatColor.ITALIC + aliasConfig.getString("Banned.Comments") + ChatColor.WHITE + "\"";
							
							event.disallow(Result.KICK_BANNED, ChatColor.RED + "" + ChatColor.BOLD + "Banned " + ChatColor.WHITE + "for " + ChatColor.BOLD + aliasConfig.getString("Banned.Reason") + ChatColor.WHITE + " - " + ChatColor.GOLD + ChatColor.BOLD + plural + ChatColor.WHITE + " remaining. \n\n" + comments.replace("AUTOMATIC: ", "") + "\nPlease follow the rules.");
							break;
						}
					} else {
						//perm ban
						
						String comments = "";
						if(aliasConfig.getString("Banned.Comments") != null)
							comments = "\"" + ChatColor.ITALIC + aliasConfig.getString("Banned.Comments") + ChatColor.WHITE + "\"";
						
						event.disallow(Result.KICK_BANNED, ChatColor.RED + "" + ChatColor.BOLD + "Pending ban" + ChatColor.WHITE + " for " + ChatColor.BOLD + aliasConfig.getString("Banned.Reason") + ChatColor.WHITE + "\nVisit " + ChatColor.GOLD + "www.DeadMC.com/forums " + ChatColor.WHITE + "to lodge an appeal. \n\n" + comments.replace("AUTOMATIC: ", "") + "\nPlease follow the rules.");
						break;
					}
				}
				
			}
			
		}
		
		if(playerConfig.getString("LastKnownIp.IP") == null
				|| !playerConfig.getString("LastKnownIp.IP").equalsIgnoreCase(ip)) {
			//IP has changed or isn't tracked yet
			playerConfig.set("LastKnownIp.IP", ip); //track last known IP
			playerConfig.set("LastKnownIp.Date", DateCode.Encode(adelaideTime, true, true, true, true, false, false)); //set date of last known
			playerConfig.save();
		}
		
	}
	
	public static List<String> newPlayers = new ArrayList<String>();
	public static HashMap<String, List<String>> newPlayers_welcomedBy = new HashMap<String, List<String>>();
	public static List<String> loginProtection = new ArrayList<String>(); //if damaged within 5 seconds of logging in, cancel and teleport to top
	
	@EventHandler
	public void JoinMessages(PlayerJoinEvent event) {
		
		Player player = event.getPlayer();
		
		//cannot run async:
		
		for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			//hide players who are spectating/vanished to the player who logged in
			if(Admin.spectating.contains(onlinePlayer.getName()) || Admin.vanished.contains(onlinePlayer.getName())) {
				Chat.debug("Hiding " + onlinePlayer.getName() + " from " + player.getName());
				player.hidePlayer(plugin, onlinePlayer);
			}
		}
		
		for(Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			if(Admin.spectating.contains(player.getName()) || Admin.vanished.contains(player.getName())) {
				Chat.debug("Hiding " + player.getName() + " from " + onlinePlayer.getName());
				onlinePlayer.hidePlayer(plugin, player);
			}
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			PlayerConfig playerConfig = PlayerConfig.getConfig(player.getUniqueId());
			
			//check to add custom items that couldn't be added
			if(playerConfig.getString("ItemsToAdd") != null) {
				List<ItemStack> remainingItems = (List<ItemStack>) new ArrayList<ItemStack>((Collection<? extends ItemStack>) playerConfig.getList("ItemsToAdd"));
				for(ItemStack itemStack : (List<ItemStack>) playerConfig.getList("ItemsToAdd")) {
					if(Shops.getItemsCanFit(player, itemStack) < itemStack.getAmount()) continue;
					remainingItems.remove(itemStack);
					Bukkit.getScheduler().runTask(plugin, () -> player.getInventory().addItem(itemStack));
					player.sendMessage(ChatColor.YELLOW + "[DeadMC] " + ChatColor.GREEN + "Added missing item to your inventory.");
				}
				playerConfig.set("ItemsToAdd", remainingItems.size() == 0 ? null : remainingItems);
				playerConfig.save();
			}
			
			String originalTownName = playerConfig.getString("Town");
			
			//add login protection
			Bukkit.getScheduler().runTask(plugin, () -> loginProtection.add(player.getName()));
			Bukkit.getScheduler().runTaskLater(plugin, () -> {
				loginProtection.remove(player.getName());
			}, 200L); //10 seconds
			
			//update actual username for DiscordSRV
			if(playerConfig.getString("Name") != null)
				player.setDisplayName(playerConfig.getString("Name"));
			
			Player finalPlayer = player;
			Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
				while(Admin.spectating.contains(finalPlayer.getName()) && finalPlayer != null) {
					Chat.sendTitleMessage(finalPlayer.getUniqueId(), ChatColor.YELLOW + "[/spectate] " + ChatColor.RED + "You are currently hidden.");
					Tools.sleepThread(2000);
				}
				while(Admin.vanished.contains(finalPlayer.getName()) && finalPlayer != null) {
					Chat.sendTitleMessage(finalPlayer.getUniqueId(), ChatColor.YELLOW + "[/vanish] " + ChatColor.RED + "You are currently hidden.");
					Tools.sleepThread(2000);
				}
			});
			
			if(DeadMC.PlayerFile.data().getStringList("Active").contains(player.getUniqueId().toString())) {
				int playtimeMinutes = (int) Math.round(((player.getStatistic(Statistic.PLAY_ONE_MINUTE) / 20.0) / 60.0));
				
				//not first time playing (join message will be played after tutorial for first time)
				
				if(!Admin.vanished.contains(player.getName())) {
					Chat.broadcastMessage(ChatColor.YELLOW + "[Login] " + Chat.getFullChatPrefix(player.getUniqueId(), false) + ChatColor.YELLOW + " has logged in.");
				} else {
					player.sendMessage(ChatColor.YELLOW + "[/vanish] " + ChatColor.RED + "You logged in silently.");
					Chat.broadcastMessage(ChatColor.YELLOW + "[Login] " + Chat.getFullChatPrefix(player.getUniqueId(), false) + ChatColor.YELLOW + " logged in silently.", true);
				}
				
				ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
				if(playerConfig.getString("LastLogin") != null && DateCode.Decode(playerConfig.getString("LastLogin"), Value.DAY_OF_YEAR) != adelaideTime.getDayOfYear()) { //different day
					if(playtimeMinutes > 120 && playerConfig.getString("StaffRank") == null) //dont include staff
						Stats.Add(Stat.RETURNING_PLAYERS);
					if(playerConfig.getString("StaffRank") != null)
						Stats.Add(Stat.ACTIVE_STAFF);
				}
				
				playerConfig.set("LastLogin", DateCode.Encode(adelaideTime, true, true, true, true, true, true));
				
				//ANNOUNCEMENTS:
				
				//			if(playerConfig.getInt("PlayTime") > 60) {
				//				if(playerConfig.getInt("DonateRank") < 3) {
				//					player.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "[SALE]" + ChatColor.WHITE + " ENJOY " + ChatColor.AQUA + ChatColor.BOLD + "25%" + ChatColor.WHITE + " OFF ALL RANKS!" + ChatColor.DARK_RED + ChatColor.BOLD + "[SALE]");
				//					player.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "[SALE] " + ChatColor.GOLD + ChatColor.ITALIC + ChatColor.UNDERLINE + "www.DeadMC.com/ranks" + ChatColor.DARK_RED + ChatColor.BOLD + " [SALE]");
				//					player.sendMessage("");
				//				}
				//				//player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "[TASKS ANNOUNCEMENT]");
				//				//player.sendMessage(ChatColor.GREEN + "We have exciting news, " + player.getName() + "! ");
				//				//player.sendMessage(ChatColor.WHITE + "Tasks " + ChatColor.WHITE + "are right around the corner!");
				//				//player.sendMessage(ChatColor.ITALIC + "We are looking for your feedback and suggestions.");
				//				//player.sendMessage(ChatColor.WHITE + "Read about it here: " + ChatColor.GOLD + "https://bit.ly/3bzbxjh");
				//				//player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "[TASKS ANNOUNCEMENT]");
				//				//player.sendMessage("");
				//			}
				//
				
				//secret santa:
//				if(playerConfig.getString("ReceivedAdminGifts2021") == null) {
//					playerConfig.set("ReceivedAdminGifts2021", true);
//					for(int count = 0; count <= 4; count++) {
//						List<ItemStack> gifts = new ArrayList<ItemStack>();
//						if(playerConfig.getString("Gifts") != null) gifts = (List<ItemStack>) playerConfig.get("Gifts");
//
//						int index = (int)(Math.random() * DeadMC.veryRareItems.size()); // 0-size (inclusive)
//						ItemStack gift = DeadMC.veryRareItems.get(index).clone();
//
//						ItemMeta meta = gift.getItemMeta();
//						List<String> lore = new ArrayList<String>();
//						lore.add(ChatColor.WHITE + "Hope you enjoy this gift! From the DeadMC staff team :)");
//						meta.setLore(lore);
//						gift.setItemMeta(meta);
//
//						gifts.add(gift);
//						playerConfig.set("Gifts", gifts);
//						playerConfig.save();
//					}
//				}
//				player.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "[Happy Holidays]");
//				player.sendMessage(ChatColor.WHITE + "Happy Holidays from the DeadMC staff team!");
//				player.sendMessage(ChatColor.WHITE + "Visit the " + ChatColor.BOLD + "Holiday Tree" + ChatColor.WHITE + " at " + ChatColor.GOLD + "/longdale" + ChatColor.WHITE + " for presents!");
//				player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "[Happy Holidays]");
//				player.sendMessage("");
				
				if(playerConfig.getString("ShopTransactionsWhileOffline") != null) {
					player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_CHIME, .3f, 2f);
					List<String> transactions = playerConfig.getStringList("ShopTransactionsWhileOffline");
					player.sendMessage("");
					player.sendMessage(ChatColor.YELLOW + "================= Shop Transactions =================");
					player.sendMessage(ChatColor.RED + "(!)" + ChatColor.WHITE + " There were " + ChatColor.BOLD + transactions.size() + ChatColor.WHITE + " transactions while you were offline.");
					for(String transaction : transactions) {
						player.sendMessage(ChatColor.WHITE + " - " + transaction);
					}
					player.sendMessage(ChatColor.YELLOW + "===================================================");
					player.sendMessage("");
					playerConfig.set("ShopTransactionsWhileOffline", null);
					playerConfig.save();
				}
				
				if(playerConfig.getString("StaffRank") == null
						&& playtimeMinutes > 60) {
					//SURVEY:
					if(playerConfig.getString("LastSurveyVote") == null || DateCode.getDaysSince(playerConfig.getString("LastSurveyVote")) >= 14) {
						player.sendMessage(ChatColor.YELLOW + "[Survey] " + ChatColor.WHITE + "Do you " + ChatColor.ITALIC + "love" + ChatColor.WHITE + " playing DeadMC? " + ChatColor.GOLD + "/yes" + ChatColor.WHITE + " or " + ChatColor.GOLD + "/no");
						;
					}
				}
				
				if(player.isOp() || playerConfig.getString("StaffRank") != null) {

					// On login, give OP survey update
					// this week: 7 responses, 66%
					// comments:
					//  - (negative in red) there's no staff on
					//  - (postive in green) love it
					
					int responses = 0;
					int score = 0;
					//calculate responses:
					for(int day = 0; day < DayOfWeek.values().length; day++) { //for each day
						//add each day
						responses += DeadMC.StatsFile.data().getInt(day + ".R");
					}
					//calculate score:
					if(responses > 0) {
						for(int day = 0; day < DayOfWeek.values().length; day++) { //for each day
							//add each day
							score += DeadMC.StatsFile.data().getInt(day + ".OS");
						}
						score /= responses;
					}
					
					player.sendMessage("");
					player.sendMessage(ChatColor.YELLOW + "=======================================");
					player.sendMessage(ChatColor.YELLOW + "SURVEY UPDATE WEEK " + ChatColor.GOLD + DeadMC.StatsFile.data().getInt("CurrentWeek") + ChatColor.YELLOW + " (less than " + (int) (7 - adelaideTime.getDayOfWeek().ordinal()) + " days to go)");
					player.sendMessage("Number of responses: " + ChatColor.BOLD + responses);
					player.sendMessage("Player satisfaction " + ChatColor.BOLD + score + ChatColor.WHITE + "%");
					player.sendMessage("");
					
					int positiveComments = DeadMC.StatsFile.data().getStringList("Survey.Positive").size();
					int negativeComments = DeadMC.StatsFile.data().getStringList("Survey.Negative").size();
					
					if(positiveComments > 0)
						player.sendMessage("Positive comments: " + ChatColor.GREEN + positiveComments);
					else player.sendMessage("Positive comments: 0");
					
					if(negativeComments > 0)
						player.sendMessage("Negative comments: " + ChatColor.RED + negativeComments);
					else player.sendMessage("Negative comments: 0");
					player.sendMessage("/comments");
					player.sendMessage(ChatColor.YELLOW + "=======================================");
					player.sendMessage("");
				}
				
				// TAX:
				if(playerConfig.getString("Town") != null &&
						(playerConfig.getString("TownRank") == null ||
								playerConfig.getInt("TownRank") < Town.Rank.Co_Mayor.ordinal())) {
					
					String townDisplayName = Town.getTownDisplayName(player.getUniqueId());
					TownConfig townConfig = TownConfig.getConfig(originalTownName);
					
					if(townConfig.getInt("Tax") > 0) {
						
						int timeSinceLastChargedTax = DateCode.getTimeSince(playerConfig.getString("LastTax")); //in minutes
						if(timeSinceLastChargedTax >= 720) { //720 minutes = 12 hours
							
							//player owes tax
							
							int tax = townConfig.getInt("Tax");
							
							playerConfig.set("LastTax", DateCode.Encode(adelaideTime, true, false, false, true, true, true));
							
							playerConfig.set("TownFundsOwed", playerConfig.getInt("TownFundsOwed") + tax);
							int percentagePaid = (playerConfig.getInt("TownFundsAdded") * 100) / playerConfig.getInt("TownFundsOwed");
							
							player.sendMessage("");
							player.sendMessage(ChatColor.YELLOW + "===================== Town Tax =====================");
							player.sendMessage(ChatColor.RED + "(!)" + ChatColor.WHITE + " You have been charged " + ChatColor.GOLD + Economy.convertCoins(tax) + ChatColor.WHITE + ".");
							player.sendMessage(ChatColor.WHITE + "Your next tax is due when you login after 12 hours.");
							player.sendMessage("");
							
							if(percentagePaid >= 100) {
								player.sendMessage(ChatColor.GREEN + "You have a more-than-healthy tax with " + percentagePaid + "% paid.");
							} else if(percentagePaid >= 80) {
								player.sendMessage(ChatColor.GREEN + "You have a healthy tax with " + percentagePaid + "% paid.");
							} else if(percentagePaid >= 65) {
								player.sendMessage(ChatColor.YELLOW + "You have a standard tax with " + percentagePaid + "% paid.");
								player.sendMessage(ChatColor.WHITE + "NOTE: The mayor of your town can banish you if you reach below 50%.");
							} else if(percentagePaid >= 50) {
								player.sendMessage(ChatColor.RED + "You have an unsafe tax with " + percentagePaid + "% paid.");
								player.sendMessage(ChatColor.DARK_RED + "[WARNING] " + ChatColor.WHITE + "The mayor of your town can banish you if you reach below 50%.");
							} else if(percentagePaid < 50) {
								player.sendMessage(ChatColor.DARK_RED + "You have a dangerous tax with " + percentagePaid + "% paid.");
								player.sendMessage(ChatColor.DARK_RED + "[WARNING] " + ChatColor.RED + "The mayor has the ability to banish you.");
							}
							
							if(percentagePaid < 100) {
								player.sendMessage("");
								player.sendMessage(ChatColor.WHITE + "Total tax due: " + ChatColor.GOLD + Economy.convertCoins(playerConfig.getInt("TownFundsOwed") - playerConfig.getInt("TownFundsAdded")));
								player.sendMessage(ChatColor.WHITE + "To pay, use: " + ChatColor.GOLD + "/town funds add <amount>");
							}
							
							player.sendMessage(ChatColor.YELLOW + "===================================================");
							player.sendMessage("");
							
						}
					}
					
				}
				
				Bukkit.getScheduler().runTask(plugin, () -> playerConfig.save());
			}
			
			//check if qualifies for town discord channel
			Guild guild = DiscordSRV.getPlugin().getMainGuild();
			if(originalTownName != null) { //has town
				TownConfig townConfig = TownConfig.getConfig(originalTownName);
				String townDisplayName = Town.getTownDisplayName(originalTownName);
				if(guild.getTextChannelsByName(townDisplayName.toLowerCase() + "-town-chat", false).size() == 0
						&& Town.qualifiesForDiscordChannel(originalTownName)) {
					Town.createDiscordChannel(originalTownName);
				}
				
				//check if nation qualifies for channel
				String nationName = townConfig.getString("Nation");
				if(nationName != null) {
					//nation exists
					if(guild.getTextChannelsByName(nationName.toLowerCase() + "-nation-chat", false).size() == 0
							&& Nation.qualifiesForDiscordChannel(nationName)) {
						Nation.createDiscordChannel(nationName);
					}
				}
			}
			
			if(playerConfig.getString("Aliases") != null && playerConfig.getString("StaffRank") == null && !player.isOp()) {
				
				
				List<String> aliases = playerConfig.getStringList("Aliases");
				
				String name = player.getName();
				if(playerConfig.getString("Name") != null)
					name = ChatColor.translateAlternateColorCodes('&', playerConfig.getString("Name"));
				
				for(Player staff : Bukkit.getOnlinePlayers()) {
					if(PlayerConfig.getConfig(staff.getUniqueId()).getString("StaffRank") != null) {
						staff.sendMessage(ChatColor.YELLOW + " > " + ChatColor.WHITE + name + ChatColor.WHITE + " has " + ChatColor.RED + ChatColor.BOLD + aliases.size() + ChatColor.WHITE + " potential aliases.");
					}
				}
				
				
			}
			
			//unread staff reports
			if(playerConfig.getString("StaffRank") != null) {
				int lastReportMade = DeadMC.ReportsFile.data().getInt("Activity.CurrentID");
				
				int unread = 0;
				for(int count = lastReportMade; count > 0; count--) {
					
					ActivityType type = ActivityType.values()[DeadMC.ReportsFile.data().getInt("Activity." + count + ".Type")];
					int ID = DeadMC.ReportsFile.data().getInt("Activity." + count + ".ID");
					
					String timeOfReport = DeadMC.ReportsFile.data().getString(type.toString() + "." + ID + ".Date/Time");
					
					if(playerConfig.getString("LastLogout") != null && DateCode.getTimeSince(playerConfig.getString("LastLogout")) > DateCode.getTimeSince(timeOfReport)) {
						unread++;
					} else break;
				}
				
				if(unread > 0)
					player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "[Reports] " + ChatColor.WHITE + "There are " + ChatColor.AQUA + ChatColor.BOLD + unread + ChatColor.WHITE + " unread reports. Use: " + ChatColor.GOLD + "/log");
			}

		});
		
		event.setJoinMessage(null);
	}
	
	@EventHandler
	public void PlayerLogout(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		Player player = event.getPlayer();
		
		if(Admin.spectating.contains(player.getName())) {
			Admin.exitSpectateMode(player);
		}
		
		PlayerConfig playerConfig = PlayerConfig.getConfig(player);
		if(playerConfig.getString("Pets.Active.IsParrot") != null) {
			player.setShoulderEntityLeft(null);
			player.setShoulderEntityRight(null);
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			@Override
			public void run() {
				String uuid = player.getUniqueId().toString();
				
				if(newPlayers.contains(player.getName())) {
					newPlayers.remove(player.getName());
					newPlayers_welcomedBy.remove(player.getName());
				}
				
				//helmet contains flag?
				PlayerInventory inventory = player.getInventory();
				if(inventory.getHelmet() != null && inventory.getHelmet().getItemMeta() != null) {
					ItemStack helmet = inventory.getHelmet();
					ItemMeta helmetMeta = helmet.getItemMeta();
					if(helmetMeta.getLore() != null && helmetMeta.getLore().contains(ChatColor.GRAY + "This item will go back to the flag point on death or logout.")) {
						//place back at flag spawn point
						player.getInventory().setHelmet(new ItemStack(Material.AIR));
						player.updateInventory();
						
						Location location = LocationCode.Decode(DeadMC.DonatorFile.data().getString("TownWars.Flag.Location"));
						BlockData blockData = Bukkit.getServer().createBlockData(DeadMC.DonatorFile.data().getString("TownWars.Flag.BlockData"));
						List<Pattern> patterns = (List<Pattern>) DeadMC.DonatorFile.data().getList("TownWars.Flag.Patterns");
						
						Banner banner = (Banner) Bukkit.getWorld("world").getBlockAt(location).getState();
						banner.setPatterns(patterns);
						banner.setBlockData(blockData);
						banner.update();
					}
				}
				
				//remove placeholder data from memory
				if(PlaceHolders.playersCoins.containsKey(uuid))
					PlaceHolders.playersCoins.remove(uuid);
				if(PlaceHolders.playersName.containsKey(uuid))
					PlaceHolders.playersName.remove(uuid);
				if(PlaceHolders.player_TaskStreak.containsKey(uuid))
					PlaceHolders.player_TaskStreak.remove(uuid);
				if(PlaceHolders.player_TaskString.containsKey(uuid))
					PlaceHolders.player_TaskStreak.remove(uuid);
				if(PlaceHolders.player_BMKills.containsKey(uuid))
					PlaceHolders.player_BMKills.remove(uuid);
				
				if(PlaceHolders.player_TownString1.containsKey(uuid))
					PlaceHolders.player_TownString1.remove(uuid);
				if(PlaceHolders.player_TownString2.containsKey(uuid))
					PlaceHolders.player_TownString2.remove(uuid);
				if(PlaceHolders.player_TownName.containsKey(uuid))
					PlaceHolders.player_TownName.remove(uuid);
				if(PlaceHolders.player_TownRank.containsKey(uuid))
					PlaceHolders.player_TownRank.remove(uuid);
				
				if(Chat.townChatToggled.contains(player.getName()))
					Chat.townChatToggled.remove(player.getName());
				if(Chat.nationChatToggled.contains(player.getName()))
					Chat.nationChatToggled.remove(player.getName());
				if(Chat.staffChatToggled.contains(player.getName()))
					Chat.staffChatToggled.remove(player.getName());
				
				if(Chat.replyToMessage.containsKey(player.getName()))
					Chat.replyToMessage.remove(player.getName());
				
				if(Chat.lastMessageTime.containsKey(player.getName()))
					Chat.lastMessageTime.remove(player.getName());
				
				if(Chat.lastMessage.containsKey(player.getName()))
					Chat.lastMessage.remove(player.getName());
				
				if(playerConfig.getString("LastLogin") != null) {
					int playTime = DateCode.getTimeSince(playerConfig.getString("LastLogin"));
					if(playerConfig.getString("StaffRank") == null) {
						//add play time stat
						Stats.Add(Stat.PLAYTIME, playTime);
					} else {
						//add staff play time
						Stats.Add(Stat.STAFF_PLAYTIME, playTime);
					}
				}
				
				Chat.updateLeaderBoard(Leaderboard.PLAYTIME, player.getUniqueId());
				
				ZonedDateTime adelaideTime = ZonedDateTime.now(ZoneId.of("Australia/Darwin")); //to adelaide time
				playerConfig.set("LastLogout", DateCode.Encode(adelaideTime, true, true, true, true, true, true)); //used for getting unread reports, last seen
				
				if(!Admin.vanished.contains(player.getName())) {
					Chat.broadcastMessage(ChatColor.YELLOW + "[Logout] " + Chat.getFullChatPrefix(player.getUniqueId(), false) + ChatColor.YELLOW + " has logged out.");
				} else {
					Chat.broadcastMessage(ChatColor.YELLOW + "[Login] " + Chat.getFullChatPrefix(player.getUniqueId(), false) + ChatColor.YELLOW + " logged out silently.", true);
				}
				
				//if spectating player logs out, exit spectating mode
				
				//despawn current pets
				Pets.DespawnPets(event.getPlayer(), true);
				
				Bukkit.getScheduler().runTask(plugin, () -> playerConfig.save());
			}
		});
	}
	
	//be sure to run this async
	public static void turnPlayers() {
		List<String> activePlayers = DeadMC.PlayerFile.data().getStringList("Active");
		Restarter.writeToLog("Analysing " + activePlayers.size() + " players...");
		
		List<String> playersToTurn = new ArrayList<String>();
		
		for(String player : activePlayers) {
			UUID uuid = UUID.fromString(player);
			PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
			OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
			
			int playtimeMinutes = (int) Math.round(((offlinePlayer.getStatistic(Statistic.PLAY_ONE_MINUTE) / 20.0) / 60.0));
			if(playerConfig.getString("LastLogin") != null
					&& (playerConfig.getString("DonateRank") != null && playerConfig.getInt("DonateRank") == 0)) {
				//remove player if inactive for 3 days and < 10 minutes playtime
				//remove player if inactive for 14 days and < 2 hours playtime
				//remove player if inactive for 45 days and < 20 hours playtime
				//remove player if inactive for 90 days and < 40 hours playtime
				int daysPassedSinceLogin = DateCode.getDaysSince(playerConfig.getString("LastLogin"));
				
				if((playtimeMinutes < 10 && daysPassedSinceLogin > 3)
						|| (playtimeMinutes < 120 && daysPassedSinceLogin > 14)
						|| (playtimeMinutes < 1200 && daysPassedSinceLogin > 45)
						|| (playtimeMinutes < 2400 && daysPassedSinceLogin > 90)) {
					playersToTurn.add(uuid.toString());
					
					Restarter.writeToLog(" - Turning " + offlinePlayer.getName() + "(" + uuid.toString() + ") last seen " + daysPassedSinceLogin + " days ago (" + playtimeMinutes + " minutes played)");
				}
			}
			
		}
		
		Restarter.writeToLog("Turned " + playersToTurn.size() + " players...");
		for(String player : playersToTurn) {
			UUID uuid = UUID.fromString(player);
			PlayerConfig playerConfig = PlayerConfig.getConfig(uuid);
			
			if(playerConfig.getString("Town") != null) {
				Town.removeMember(uuid);
			}
			
			File playerFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "players" + File.separator + uuid.toString() + ".yml");
			if(playerFile.exists()) playerFile.delete();
			
			//also remove player files
			File datFile = new File(Bukkit.getWorldContainer(), "world" + File.separator + "playerdata" + File.separator + uuid.toString() + ".dat");
			if(datFile.exists()) datFile.delete();
			File datOldFile = new File(Bukkit.getWorldContainer(), "world" + File.separator + "playerdata" + File.separator + uuid.toString() + ".dat_old");
			if(datOldFile.exists()) datOldFile.delete();
			File statFile = new File(Bukkit.getWorldContainer(), "world" + File.separator + "stats" + File.separator + uuid.toString() + ".json");
			if(datOldFile.exists()) datOldFile.delete();
			
			activePlayers.remove(uuid.toString());
		}
		DeadMC.PlayerFile.data().set("Active", activePlayers);
		DeadMC.PlayerFile.save();
		
		Bukkit.getConsoleSender().sendMessage("[Data-Clean] " + ChatColor.GREEN + "" + playersToTurn.size() + " players removed.");
		DeadMC.RestartFile.data().set("DataCleaner.Previous.Players", playersToTurn.size());
		DeadMC.RestartFile.save();
	}
	
	//be sure to run this async
	public static void turnIPs() {
		//ips:
		List<String> activeIPs = DeadMC.IpsFile.data().getStringList("Active");
		Restarter.writeToLog("Analysing " + activeIPs.size() + " ips...");
		List<String> ipsToTurn = new ArrayList<String>();
		
		for(String ip : activeIPs) {
			IpConfig ipConfig = IpConfig.getConfig(ip);
			
			List<String> uuids = ipConfig.getStringList("UUID");
			for(String uuid : ipConfig.getStringList("UUID")) {
				int lastSeen = DateCode.getDaysSince(ipConfig.getString(uuid));
				if(lastSeen > 28) { //turn IP connection if last seen 28 days ago
					Restarter.writeToLog(" - Turning " + ip + " last used " + lastSeen + " days ago");
					uuids.remove(uuid);
				}
			}
			if(uuids.size() == 0) {
				//just remove the config file as no longer used
				ipsToTurn.add(ip);
				
			} else {
				//update the list
				ipConfig.set("UUID", uuids);
				ipConfig.save();
			}
			
		}
		
		Restarter.writeToLog("Turned " + ipsToTurn.size() + " ips...");
		for(String ip : ipsToTurn) {
			IpConfig ipConfig = IpConfig.getConfig(ip);
			File ipFile = new File(Bukkit.getPluginManager().getPlugin("DeadMC").getDataFolder(), "ips" + File.separator + ip + ".yml");
			ipFile.delete();
			
			activeIPs.remove(ip);
		}
		DeadMC.IpsFile.data().set("Active", activeIPs);
		DeadMC.IpsFile.save();
		
		Bukkit.getConsoleSender().sendMessage("[Data-Clean] " + ChatColor.GREEN + "" + ipsToTurn.size() + " IPs removed.");
		DeadMC.RestartFile.data().set("DataCleaner.Previous.IPs", ipsToTurn.size());
		DeadMC.RestartFile.save();
	}
	
}