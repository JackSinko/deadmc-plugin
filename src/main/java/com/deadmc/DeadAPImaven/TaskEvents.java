package com.deadmc.DeadAPImaven;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.Rotatable;
import org.bukkit.entity.Cat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityBreedEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.deadmc.DeadAPImaven.Admin.StaffRank;
import com.deadmc.DeadAPImaven.Pets.Pet;
import com.deadmc.DeadAPImaven.Task.TaskStep;
import com.deadmc.DeadAPImaven.TaskManager.Difficulty;
import com.deadmc.DeadAPImaven.TaskManager.TaskType;

public class TaskEvents implements Listener {
	private DeadMC plugin;
	
	public TaskEvents(DeadMC plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDropItem(PlayerDropItemEvent event) {
		Player player = event.getPlayer();
		if(Admin.spectating.contains(player.getName())) {
			Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Spectate] " + ChatColor.WHITE + "Cannot interact while spectating.");
			event.setCancelled(true);
		}
		
		//temp fix for ViaVersion
		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			player.updateInventory();
		}, 0L);
	}
	
	@EventHandler
	public void onPickupItem(EntityPickupItemEvent event) {
		ItemStack item = event.getItem().getItemStack();
		
		if(event.getEntityType() == EntityType.PLAYER) {
			if(Admin.spectating.contains(((Player) event.getEntity()).getName()))
				event.setCancelled(true);
		}
		
		if(event.getEntityType() == EntityType.PLAYER &&
				(item.getType().toString().contains("LOG"))) {
			if(TaskManager.playerHasStep_TUTORIAL(((Player) event.getEntity()).getUniqueId(), TaskStep.GATHER_LOGS))
				TaskManager.stepTask(((Player) event.getEntity()).getUniqueId());
		}
		
	}
	
	@EventHandler
	public void onEntityBreed(EntityBreedEvent event) {
		Entity entity = event.getEntity();
		
		if(event.getBreeder() instanceof Player) {
			Player player = (Player) event.getBreeder();

						if(TaskManager.playerHasStep(player.getUniqueId(), TaskType.BREED)) {
							
							PlayerConfig playerConfig = PlayerConfig.getConfig(player);
							
							//if what exists && matches
							int ID = playerConfig.getInt("Task");
							int step = playerConfig.getInt("Step");
							
							Difficulty difficulty = TaskManager.GetDifficulty(ID);
							
							if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".What") == null
									|| DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".What").equalsIgnoreCase(entity.getName())) {
								TaskManager.track(player.getUniqueId(), 1);
							}
							
						}
			
			//pets:
			if(DeadMC.PetsFile.data().getString("Pets." + entity.getName() + ".Description") != null
					&& DeadMC.PetsFile.data().getString("Pets." + entity.getName() + ".Description").contains("breeding")) {
				//if pet can be obtained through breeding
				
				int chance = DeadMC.PetsFile.data().getInt("Pets." + entity.getName() + ".Chance");
				int random = (int) (Math.random() * chance) + 1; // 1-chance (inclusive)
				if(random == chance) { //1 in X
					Pet pet = Pet.valueOf(entity.getName());
					Pets.Obtain(player, pet);
				}
			}
			
		}
	}
	
	@EventHandler
	public void onEntityTame(EntityTameEvent event) {
		Entity entity = event.getEntity();
		if(event.getOwner() instanceof Player) {
			Player player = (Player) event.getOwner();
			
			//pets:
			if(DeadMC.PetsFile.data().getString("Pets." + entity.getName() + ".Description") != null
					&& DeadMC.PetsFile.data().getString("Pets." + entity.getName() + ".Description").contains("taming")) {
				//if pet can be obtained through taming
				
				int chance = DeadMC.PetsFile.data().getInt("Pets." + entity.getName() + ".Chance");
				int random = (int) (Math.random() * chance) + 1; // 1-chance (inclusive)
				if(random == chance) { //1 in X
					Pet pet = Pet.valueOf(entity.getName());
					Pets.Obtain(player, pet);
					
					if(pet == Pet.Cat) {
						//update the cat variant
						PlayerConfig playerConfig = PlayerConfig.getConfig(player);
						playerConfig.set("Pets." + pet.ordinal() + ".Variant", ((Cat) entity).getCatType().toString());
						playerConfig.save();
						
						String entityNick = DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") != null ? DeadMC.PetsFile.data().getString("Pets." + pet.toString() + ".Nickname") : pet.toString().replace("_", " ");
						String petName = playerConfig.getString("Pets." + pet.ordinal() + ".Name") != null ? playerConfig.getString("Pets." + pet.ordinal() + ".Name") : "Pet " + entityNick;
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Pets] " + ChatColor.GREEN + ChatColor.translateAlternateColorCodes('&', petName) + "'s variant has been updated :)");
						
						Pets.Spawn(player, pet);
					}
				}
			}
		}
	}
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event) {
		
		if(!event.isCancelled()) { //cant break blocks on claimed land for task
			Player player = event.getPlayer();
			Block block = event.getBlock();
			
			if(TaskManager.playerHasStep(player.getUniqueId(), TaskType.PLACE)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				List<String> placedTaskBlocks = new ArrayList<String>();
				if(playerConfig.getString("TaskBlocksPlaced") != null) {
					placedTaskBlocks = playerConfig.getStringList("TaskBlocksPlaced");
				}
				
				if(placedTaskBlocks.contains(LocationCode.Encode(block.getLocation()))) {
					//block has already been placed this task
					
					//instead of dropping normal item, drop new item with players UUID as lore
					for(ItemStack item : block.getDrops()) {
						ItemMeta meta = item.getItemMeta();
						List<String> lore = new ArrayList<String>();
						lore.add("" + ChatColor.WHITE + player.getUniqueId().toString().substring(0, 5));
						meta.setLore(lore);
						item.setItemMeta(meta);
						Bukkit.getWorld(player.getWorld().getName()).dropItemNaturally(block.getLocation(), item);
					}
					
					placedTaskBlocks.remove(LocationCode.Encode(block.getLocation()));
					playerConfig.set("TaskBlocksPlaced", placedTaskBlocks);
					playerConfig.save();
					
					event.setDropItems(false);
				}
			}
			
			if(TaskManager.playerHasStep(player.getUniqueId(), TaskType.BREAK)) {
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				List<String> placedTaskBlocks = new ArrayList<String>();
				if(playerConfig.getString("TaskBlocksPlaced") != null) {
					placedTaskBlocks = playerConfig.getStringList("TaskBlocksPlaced");
				}
				
				//if what exists && matches
				int ID = playerConfig.getInt("Task");
				int step = playerConfig.getInt("Step");
				
				Difficulty difficulty = TaskManager.GetDifficulty(ID);
				
				String itemInHand = player.getInventory().getItemInMainHand().getType().toString();
				if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Using") == null //no specified item needed
						|| itemInHand.contains(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Using"))) {
					if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".What") == null) {
						//track any
						if(!placedTaskBlocks.contains(LocationCode.Encode(block.getLocation()))) {
							TaskManager.track(player.getUniqueId(), 1);
						} else {
							player.sendMessage(ChatColor.YELLOW + "[Task] " + ChatColor.RED + "This block won't be counted to your task as it was placed after starting your task.");
							placedTaskBlocks.remove(LocationCode.Encode(block.getLocation()));
							playerConfig.set("TaskBlocksPlaced", placedTaskBlocks);
							playerConfig.save();
						}
						
					} else if(block.getType().toString().toLowerCase().contains((DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".What")).toLowerCase())) {
						//block type contains WHAT
						
						if(!placedTaskBlocks.contains(LocationCode.Encode(block.getLocation()))) {
							TaskManager.track(player.getUniqueId(), 1);
						} else {
							player.sendMessage(ChatColor.YELLOW + "[Task] " + ChatColor.RED + "This block won't be counted to your task as it was placed after starting your task.");
							placedTaskBlocks.remove(LocationCode.Encode(block.getLocation()));
							playerConfig.set("TaskBlocksPlaced", placedTaskBlocks);
							playerConfig.save();
						}
						
						//track if harvesting sugar cane and cactus:
						if(block.getType() == Material.SUGAR_CANE || block.getType() == Material.CACTUS) {
							if(block.getType().toString().toLowerCase().contains((DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".What")).toLowerCase())
									&& (block.getRelative(BlockFace.UP).getType() == Material.SUGAR_CANE
									|| block.getRelative(BlockFace.UP).getType() == Material.CACTUS)
									&& !placedTaskBlocks.contains(LocationCode.Encode(block.getRelative(BlockFace.UP).getLocation())))
								TaskManager.track(player.getUniqueId(), 1); //track one more if grown on top
							if(block.getType().toString().toLowerCase().contains((DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".What")).toLowerCase())
									&& (block.getRelative(BlockFace.UP).getRelative(BlockFace.UP).getType() == Material.SUGAR_CANE
									|| block.getRelative(BlockFace.UP).getRelative(BlockFace.UP).getType() == Material.CACTUS)
									&& !placedTaskBlocks.contains(LocationCode.Encode(block.getRelative(BlockFace.UP).getRelative(BlockFace.UP).getLocation())))
								TaskManager.track(player.getUniqueId(), 1); //track one more if grown on top
						}
					}
				}
				
			}
			
			if(TaskManager.playerHasStep(player.getUniqueId(), TaskType.HARVEST)) {
				Block harvest = null;
				
				//check if broke a fully grown crop
				//Eg. beetroot, wheet
				BlockData data = block.getBlockData();
				if(data instanceof Ageable) {
					Ageable ag = (Ageable) data;
					if(ag.getAge() == ag.getMaximumAge()) {
						harvest = block;
					}
				}
				
				if(harvest == null) { //only check this if the block wasn't a fully grown crop
					if(block.getType() == Material.PUMPKIN || block.getType() == Material.MELON) {
						ArrayList<BlockFace> directions = new ArrayList<BlockFace>();
						directions.add(BlockFace.NORTH);
						directions.add(BlockFace.EAST);
						directions.add(BlockFace.SOUTH);
						directions.add(BlockFace.WEST);
						for(BlockFace direction : directions) {
							Block relative = block.getRelative(direction);
							
							//using the block data, get the direction the stem is facing
							String stemDirection = "";
							String stemData = relative.getBlockData().getAsString(true);
							if(relative.getType() == Material.ATTACHED_MELON_STEM)
								stemDirection = stemData.substring(37, stemData.length() - 1);
							if(relative.getType() == Material.ATTACHED_PUMPKIN_STEM)
								stemDirection = stemData.substring(39, stemData.length() - 1);
							
							if(stemDirection.equalsIgnoreCase(direction.getOppositeFace().toString())) {
								//block is connected!
								harvest = block;
								break;
							}
						}
					}
				}
				
				if(harvest != null) { //player harvested a block, and player has harvest task. Let's check if the task matches the block that was harvested.
					
					PlayerConfig playerConfig = PlayerConfig.getConfig(player);
					
					//if what exists && matches
					int ID = playerConfig.getInt("Task");
					int step = playerConfig.getInt("Step");
					
					Difficulty difficulty = TaskManager.GetDifficulty(ID);
					
					if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".What") == null) {
						//track any
						TaskManager.track(player.getUniqueId(), 1);
					} else if(block.getType().toString().toLowerCase().contains((DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".What")).toLowerCase())) {
						//block type contains WHAT
						TaskManager.track(player.getUniqueId(), 1);
						
					}
					
				}
				
			}
			
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockPlace(BlockPlaceEvent event) {
		if(!event.isCancelled()) {
			Player player = event.getPlayer();
			Block block = event.getBlock();
			
			if(TaskManager.playerHasStep(player.getUniqueId(), TaskType.PLACE)) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				//if what exists && matches
				int ID = playerConfig.getInt("Task");
				int step = playerConfig.getInt("Step");
				
				Difficulty difficulty = TaskManager.GetDifficulty(ID);
				
				String itemInHand = player.getInventory().getItemInMainHand().getType().toString();
				if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Using") == null //no specified item needed
						|| itemInHand.contains(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Using"))) {
					if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".What") == null
							|| block.getType().toString().toLowerCase().contains((DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".What")).toLowerCase())) {
						//track any && block type contains WHAT
						
						List<String> placedTaskBlocks = new ArrayList<String>();
						if(playerConfig.getString("TaskBlocksPlaced") != null) {
							placedTaskBlocks = playerConfig.getStringList("TaskBlocksPlaced");
						}
						placedTaskBlocks.add(LocationCode.Encode(block.getLocation()));
						playerConfig.set("TaskBlocksPlaced", placedTaskBlocks);
						playerConfig.save();
						
						if(player.getInventory().getItemInMainHand().hasItemMeta()
								&& player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.WHITE + player.getUniqueId().toString().substring(0, 5))) {
							player.sendMessage(ChatColor.YELLOW + "[Task] " + ChatColor.RED + "This block won't count to your task as it has already been placed since starting your task.");
							return;
						}
						TaskManager.track(player.getUniqueId(), 1);
						
					}
				}
			}
			
			//stop player from placing blocks with breaking task
			if(TaskManager.playerHasStep(player.getUniqueId(), TaskType.BREAK)) {
				
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				//if what exists && matches
				int ID = playerConfig.getInt("Task");
				int step = playerConfig.getInt("Step");
				
				Difficulty difficulty = TaskManager.GetDifficulty(ID);
				
				List<String> placedTaskBlocks = new ArrayList<String>();
				if(playerConfig.getString("TaskBlocksPlaced") != null) {
					placedTaskBlocks = playerConfig.getStringList("TaskBlocksPlaced");
				}
				
				if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".What") == null
						|| block.getType().toString().toLowerCase().contains((DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".What")).toLowerCase())) {
					
					if(placedTaskBlocks.size() > 100) {
						Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Task] " + ChatColor.RED + "Cannot place block.");
						player.sendMessage("");
						player.sendMessage(ChatColor.YELLOW + "[Task] " + ChatColor.RED + "Complete your task to place more blocks.");
						player.sendMessage(ChatColor.YELLOW + "[Task] " + ChatColor.WHITE + "Skip the task with " + ChatColor.GOLD + "/task new" + ChatColor.WHITE + ".");
						player.sendMessage("");
						event.setCancelled(true);
						return;
					}
					
					placedTaskBlocks.add(LocationCode.Encode(block.getLocation()));
					playerConfig.set("TaskBlocksPlaced", placedTaskBlocks);
					playerConfig.save();
					Chat.sendTitleMessage(player.getUniqueId(), ChatColor.YELLOW + "[Task] " + ChatColor.RED + "NOTE: This block won't be counted to your task.");
				}
				
			}
		}
	}
	
	@EventHandler
	public void onKill(EntityDeathEvent event) {
		Entity entity = event.getEntity();
		
		//get player who last hit entity
		if(entity.getLastDamageCause() instanceof EntityDamageByEntityEvent
				&& entity.getType().isAlive() && entity.getType() != EntityType.ARMOR_STAND) {
			EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent) entity.getLastDamageCause();
			
			Player player = null;
			//killed by projectile:
			if(damageEvent.getCause() == DamageCause.PROJECTILE) {
				Projectile projectile = (Projectile) damageEvent.getDamager();
				if(projectile.getShooter() instanceof Player) { //shooter is a player
					player = (Player) projectile.getShooter();
				}
			}
			//killed by player:
			if(damageEvent.getDamager() instanceof Player) {
				player = (Player) damageEvent.getDamager();
			}
			if(entity.getType() == EntityType.WITHER) {
				//get nearby player
				Collection<Entity> nearbyPlayers = entity.getLocation().getWorld().getNearbyEntities(entity.getLocation(), 50, 50, 50);
				double closestDistance = 100000;
				for(Entity e : nearbyPlayers) {
					if(e instanceof Player p && (player == null || p.getLocation().distance(entity.getLocation()) < closestDistance)) {
						closestDistance = p.getLocation().distance(entity.getLocation());
						player = (Player) e;
					}
				}
			}
			
			if(player != null) {
				PlayerConfig playerConfig = PlayerConfig.getConfig(player);
				
				if(TaskManager.playerHasStep(player.getUniqueId(), TaskType.KILL)) {
					//if what exists && matches
					int ID = playerConfig.getInt("Task");
					int step = playerConfig.getInt("Step");
					
					Difficulty difficulty = TaskManager.GetDifficulty(ID);
					
					String itemInHand = player.getInventory().getItemInMainHand().getType().toString();
					if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Using") == null //no specified item needed
							|| itemInHand.contains(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".Using"))) {
						if(DeadMC.TaskFile.data().getString("Tasks." + difficulty.ordinal() + "." + ID + "." + step + ".What") == null) {
							//track any
							TaskManager.track(player.getUniqueId(), 1);
						} else if(entity.getType().toString().equalsIgnoreCase(DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".What"))) {
							//entity type == WHAT
							TaskManager.track(player.getUniqueId(), 1);
							
						} else if((entity.getType() == EntityType.ZOMBIE
								|| entity.getType() == EntityType.HUSK
								|| entity.getType() == EntityType.ZOMBIE_VILLAGER
								|| entity.getType() == EntityType.DROWNED
								|| entity.getType() == EntityType.ZOMBIFIED_PIGLIN)
								&& DeadMC.TaskFile.data().getString("Tasks." + TaskManager.GetDifficulty(ID).ordinal() + "." + ID + "." + step + ".What").equalsIgnoreCase("ZOMBIE")) {
							//track all zombies
							TaskManager.track(player.getUniqueId(), 1);
						}
					}
					
				}
			}
		}
		
	}
	
}
